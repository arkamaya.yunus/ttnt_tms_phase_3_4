TRUNCATE TABLE tb_m_account;
TRUNCATE TABLE tb_m_company;
TRUNCATE TABLE tb_m_company_function;
TRUNCATE TABLE tb_m_company_prefferences;
TRUNCATE TABLE tb_m_customer;
TRUNCATE TABLE tb_m_holiday_cal;
TRUNCATE TABLE tb_m_items;
TRUNCATE TABLE tb_m_supplier;
TRUNCATE TABLE tb_m_tax;
TRUNCATE TABLE tb_m_users;
TRUNCATE TABLE tb_m_user_group;
TRUNCATE TABLE tb_m_user_group_auth;
TRUNCATE TABLE tb_m_warehouse;
TRUNCATE TABLE tb_r_balance_sheet;
TRUNCATE TABLE tb_r_balance_sheet_detail;
TRUNCATE TABLE tb_r_journal;
TRUNCATE TABLE tb_r_journal_items;
TRUNCATE TABLE tb_r_profit_loss;
TRUNCATE TABLE tb_r_profit_loss_detail;
TRUNCATE TABLE tb_r_purchase_order;
TRUNCATE TABLE tb_r_purchase_order_items;
TRUNCATE TABLE tb_r_purchase_payment;
TRUNCATE TABLE tb_r_purchase_payment_items;
TRUNCATE TABLE tb_r_purchase_request;
TRUNCATE TABLE tb_r_purchase_request_items;
TRUNCATE TABLE tb_r_purchase_return;
TRUNCATE TABLE tb_r_purchase_return_items;
TRUNCATE TABLE tb_r_reconcile;
TRUNCATE TABLE tb_r_reconcile_entries;
TRUNCATE TABLE tb_r_register;
TRUNCATE TABLE tb_r_sales_delivery;
TRUNCATE TABLE tb_r_sales_delivery_items;
TRUNCATE TABLE tb_r_sales_invoice;
TRUNCATE TABLE tb_r_sales_invoice_items;
TRUNCATE TABLE tb_r_sales_order;
TRUNCATE TABLE tb_r_sales_order_items;
TRUNCATE TABLE tb_r_sales_payment;
TRUNCATE TABLE tb_r_sales_quotation;
TRUNCATE TABLE tb_r_sales_quotation_items;