-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.6-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for moadrpcd_corporate
DROP DATABASE IF EXISTS `moadrpcd_corporate`;
CREATE DATABASE IF NOT EXISTS `moadrpcd_corporate` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `moadrpcd_corporate`;

-- Dumping structure for table moadrpcd_corporate.ci_sessions
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_account
CREATE TABLE IF NOT EXISTS `tb_m_account` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `account_code` varchar(50) DEFAULT NULL,
  `account_name` varchar(100) DEFAULT NULL,
  `account_description` varchar(255) DEFAULT NULL,
  `account_category` varchar(50) DEFAULT NULL,
  `account_type` enum('assets','equity','expenses','income','liability') DEFAULT NULL COMMENT 'tb_m_system',
  `account_parent` int(11) DEFAULT NULL,
  `account_balance` double DEFAULT 0,
  `used` char(1) DEFAULT '0',
  `level` tinyint(4) DEFAULT 0,
  `account_cashbank` char(1) DEFAULT '0',
  `account_lock` char(1) DEFAULT '0',
  `initial_journal_id` bigint(20) DEFAULT 0,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`account_id`),
  KEY `FK_tb_m_account_tb_m_company` (`company_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_app
CREATE TABLE IF NOT EXISTS `tb_m_app` (
  `app_id` int(11) NOT NULL,
  `app_name` varchar(100) DEFAULT NULL,
  `app_label` varchar(100) DEFAULT NULL,
  `app_default_function_id` int(11) DEFAULT NULL,
  `app_description` varchar(255) DEFAULT NULL,
  `app_active` char(1) DEFAULT '1',
  `app_order` int(11) DEFAULT 1,
  `app_icon` varchar(30) DEFAULT NULL,
  `app_bgcolor` varchar(10) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_company
CREATE TABLE IF NOT EXISTS `tb_m_company` (
  `company_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `company_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `company_industry` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `logo` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `signature` varchar(100) DEFAULT NULL,
  `show_logo_in_report` varchar(1) CHARACTER SET latin1 DEFAULT '1',
  `address` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `shipping_address` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `phone` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `fax` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `tax_number` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `website` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `currency` varchar(100) CHARACTER SET latin1 DEFAULT 'idr',
  `default_language` varchar(10) CHARACTER SET latin1 DEFAULT 'id',
  `bank_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `bank_branch` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `bank_address` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `bank_account_number` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `bank_account_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `swift_code` varchar(5) CHARACTER SET latin1 DEFAULT NULL,
  `accounting_period_start` date DEFAULT NULL,
  `accounting_period_end` date DEFAULT NULL,
  `sales_open_invoices` double DEFAULT NULL,
  `sales_overdue_invoices` double DEFAULT NULL,
  `sales_invoices_paid_last30` double DEFAULT NULL,
  `trial_expired` date DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `activation_expired` datetime DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `created_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`company_id`,`user_email`),
  KEY `company_id` (`company_id`) USING BTREE,
  KEY `FK_tb_m_company_tb_m_package` (`package_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_company_function
CREATE TABLE IF NOT EXISTS `tb_m_company_function` (
  `company_id` int(11) NOT NULL,
  `function_id` int(11) NOT NULL,
  `created_by` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`company_id`,`function_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_company_prefferences
CREATE TABLE IF NOT EXISTS `tb_m_company_prefferences` (
  `preff_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `system_type` varchar(100) CHARACTER SET latin1 NOT NULL,
  `system_code` varchar(100) CHARACTER SET latin1 NOT NULL,
  `system_value_txt` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `system_value_num` double DEFAULT NULL,
  `created_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`preff_id`,`company_id`,`system_type`,`system_code`),
  KEY `FK_tb_m_company_prefferences_tb_m_company` (`company_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_customer
CREATE TABLE IF NOT EXISTS `tb_m_customer` (
  `customer_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL DEFAULT 0,
  `customer_code` varchar(50) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_address_billing` varchar(255) DEFAULT NULL,
  `customer_city` varchar(255) DEFAULT NULL,
  `customer_prov` varchar(255) DEFAULT NULL,
  `customer_zip_code` varchar(10) DEFAULT NULL,
  `customer_country` varchar(255) DEFAULT NULL,
  `customer_phone1` varchar(15) DEFAULT NULL,
  `customer_phone2` varchar(15) DEFAULT NULL,
  `customer_phone3` varchar(15) DEFAULT NULL,
  `npwp_number` varchar(50) DEFAULT NULL,
  `npwp_address` varchar(255) DEFAULT NULL,
  `ar_id` bigint(20) DEFAULT NULL,
  `ap_id` bigint(20) DEFAULT NULL,
  `default_term` varchar(50) DEFAULT NULL,
  `opening_balance` double DEFAULT NULL,
  `opening_dt` date DEFAULT NULL,
  `ar_remaining` double DEFAULT NULL,
  `pos_flag` char(1) DEFAULT '0',
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `contact_person` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `FK_tb_m_customer_tb_m_company` (`company_id`) USING BTREE,
  KEY `FK_tb_m_customer_tb_m_account` (`ar_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_function
CREATE TABLE IF NOT EXISTS `tb_m_function` (
  `function_id` char(3) NOT NULL DEFAULT '',
  `app_id` int(11) NOT NULL,
  `function_parent` char(3) DEFAULT NULL,
  `function_name` varchar(100) DEFAULT NULL,
  `function_name_id` varchar(100) DEFAULT NULL,
  `function_controller` varchar(100) DEFAULT NULL,
  `function_description` varchar(255) DEFAULT NULL,
  `function_active` char(1) DEFAULT '1',
  `function_order` int(11) DEFAULT 1,
  `function_icon` varchar(50) DEFAULT NULL,
  `function_link_visible` char(1) DEFAULT '1',
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`function_id`),
  KEY `app_id` (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_function_feature
CREATE TABLE IF NOT EXISTS `tb_m_function_feature` (
  `feature_id` char(5) NOT NULL,
  `function_id` char(3) NOT NULL,
  `feature_name` varchar(100) DEFAULT NULL,
  `feature_description` varchar(255) DEFAULT NULL,
  `feature_element_id` varchar(100) DEFAULT NULL,
  `feature_element_class` varchar(100) DEFAULT NULL,
  `feature_element_type` varchar(100) DEFAULT NULL,
  `feature_element_icon` varchar(100) DEFAULT NULL,
  `feature_action` varchar(100) DEFAULT NULL,
  `feature_position` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`feature_id`),
  KEY `FK_tb_m_function_feature_tb_m_function` (`function_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_holiday_cal
CREATE TABLE IF NOT EXISTS `tb_m_holiday_cal` (
  `holiday_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `holiday_dt` date NOT NULL,
  `holiday_type` enum('LIBURNASIONAL','CUTIBERSAMA') NOT NULL DEFAULT 'LIBURNASIONAL',
  `description` text DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`holiday_id`,`holiday_dt`,`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_items
CREATE TABLE IF NOT EXISTS `tb_m_items` (
  `item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL DEFAULT 0,
  `item_code` varchar(50) DEFAULT NULL,
  `item_category_id` varchar(100) DEFAULT NULL,
  `item_category` varchar(100) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `item_price` double DEFAULT NULL,
  `item_price_buy` double DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `warehouse_id` bigint(20) DEFAULT NULL,
  `sales_account_id` int(11) DEFAULT NULL,
  `sales_tax_id` int(11) DEFAULT NULL,
  `purchase_account_id` int(11) DEFAULT NULL,
  `purchase_tax_id` int(11) DEFAULT NULL,
  `is_archive` char(1) DEFAULT '0',
  `file` varchar(255) DEFAULT NULL,
  `is_used` char(1) DEFAULT NULL,
  `is_asset` char(1) DEFAULT NULL,
  `asset_account_id` int(11) DEFAULT NULL,
  `min_stock` int(11) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`item_id`),
  KEY `FK_tb_m_items_tb_m_company` (`company_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_package
CREATE TABLE IF NOT EXISTS `tb_m_package` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `package_price` double(18,0) DEFAULT NULL,
  `package_period` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `package_description` text CHARACTER SET latin1 DEFAULT NULL,
  `default_landing` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `default_users` varchar(50) CHARACTER SET latin1 DEFAULT '1' COMMENT 'Isian berupa angka 1 - berapapun. Jika diisi tanda cacing ~ maka pengguna tidak terhingga (Unlimited)',
  `created_by` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_package_detail
CREATE TABLE IF NOT EXISTS `tb_m_package_detail` (
  `package_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `code` int(11) NOT NULL DEFAULT 0 COMMENT 'code 0 untuk tipe package detail selain ambil dari master funtion',
  `type` varchar(50) CHARACTER SET latin1 DEFAULT 'function',
  `description` varchar(50) CHARACTER SET latin1 DEFAULT '',
  `display_flag` char(1) CHARACTER SET latin1 DEFAULT '0',
  `created_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `created_dt` timestamp NULL DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`package_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_supplier
CREATE TABLE IF NOT EXISTS `tb_m_supplier` (
  `supplier_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL DEFAULT 0,
  `supplier_code` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `supplier_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `supplier_address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `supplier_address_billing` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `supplier_city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `supplier_prov` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `supplier_zip_code` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `supplier_country` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `supplier_phone1` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `supplier_phone2` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `supplier_phone3` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `npwp_number` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `npwp_address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ar_id` bigint(20) DEFAULT NULL,
  `ap_id` bigint(20) DEFAULT NULL,
  `default_term` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `opening_balance` double DEFAULT NULL,
  `opening_dt` date DEFAULT NULL,
  `ar_remaining` double DEFAULT NULL,
  `pos_flag` char(1) CHARACTER SET utf8 DEFAULT '0',
  `created_by` varchar(50) CHARACTER SET utf8 DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET utf8 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `contact_person` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_system
CREATE TABLE IF NOT EXISTS `tb_m_system` (
  `system_type` varchar(50) NOT NULL COMMENT 'data prefix ''defcfg_'' untuk generate data company prefferences ketika register',
  `system_code` varchar(50) NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date DEFAULT NULL,
  `system_value_txt` varchar(256) DEFAULT NULL,
  `system_value_num` double DEFAULT NULL,
  `system_value_time` time DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`system_type`,`system_code`,`valid_from`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_tax
CREATE TABLE IF NOT EXISTS `tb_m_tax` (
  `tax_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) DEFAULT NULL,
  `tax_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `tax_rate` double DEFAULT NULL,
  `sales_account_id` int(11) DEFAULT NULL,
  `purchase_account_id` int(11) DEFAULT NULL,
  `tax_type` enum('1','2') CHARACTER SET latin1 DEFAULT '1',
  `created_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`tax_id`),
  KEY `FK_tb_m_tax_tb_m_company` (`company_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_users
CREATE TABLE IF NOT EXISTS `tb_m_users` (
  `user_email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `user_group_id` int(11) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `user_password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `user_photo` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `full_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `is_active` char(1) CHARACTER SET latin1 DEFAULT '1',
  `user_last_logged_in` datetime DEFAULT NULL,
  `super_admin` char(1) CHARACTER SET latin1 DEFAULT '0',
  `email_confirmed` char(1) CHARACTER SET latin1 DEFAULT '0',
  `email_confirmed_code` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `reset_password_code` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `user_expired` date DEFAULT NULL,
  `created_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_email`),
  KEY `FK_tb_m_users_tb_m_user_group` (`user_group_id`) USING BTREE,
  KEY `FK_tb_m_users_tb_m_company` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_user_group
CREATE TABLE IF NOT EXISTS `tb_m_user_group` (
  `user_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) DEFAULT NULL,
  `user_group_description` varchar(100) DEFAULT NULL,
  `is_admin` char(1) DEFAULT '0',
  `default_user_lock` char(1) DEFAULT '0',
  `default_landing` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_group_id`),
  KEY `FK_tb_m_user_group_tb_m_company` (`company_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_user_group_auth
CREATE TABLE IF NOT EXISTS `tb_m_user_group_auth` (
  `user_group_id` int(11) NOT NULL,
  `function_id` int(11) NOT NULL,
  `feature_id` bigint(20) NOT NULL,
  `created_by` varchar(30) DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(30) DEFAULT NULL,
  `changed_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`user_group_id`,`function_id`,`feature_id`),
  KEY `FK_tb_m_user_group_auth_tb_m_function` (`function_id`) USING BTREE,
  KEY `FK_tb_m_user_group_auth_tb_m_function_feature` (`feature_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_m_warehouse
CREATE TABLE IF NOT EXISTS `tb_m_warehouse` (
  `warehouse_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) DEFAULT NULL,
  `warehouse_code` varchar(50) DEFAULT NULL,
  `warehouse_address` varchar(100) DEFAULT NULL,
  `warehouse_desc` varchar(255) DEFAULT NULL,
  `created_by` varchar(30) DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(30) DEFAULT NULL,
  `changed_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`warehouse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_balance_sheet
CREATE TABLE IF NOT EXISTS `tb_r_balance_sheet` (
  `balance_sheet_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) DEFAULT NULL,
  `title` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET latin2 DEFAULT NULL,
  `footnote` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `show_account_code` char(1) CHARACTER SET latin2 DEFAULT '0',
  `col_name` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `col_start_dt` date DEFAULT NULL,
  `col_end_dt` date DEFAULT NULL,
  `created_by` varchar(50) CHARACTER SET latin2 DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin2 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`balance_sheet_id`),
  KEY `FK_tb_r_balance_sheet_tb_m_company` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_balance_sheet_detail
CREATE TABLE IF NOT EXISTS `tb_r_balance_sheet_detail` (
  `balance_sheet_detail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `balance_sheet_id` bigint(20) DEFAULT NULL,
  `col_name` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `col_start_dt` date DEFAULT NULL,
  `col_end_dt` date DEFAULT NULL,
  `col_order` int(11) DEFAULT 0,
  `created_by` varchar(50) CHARACTER SET latin2 DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin2 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`balance_sheet_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_journal
CREATE TABLE IF NOT EXISTS `tb_r_journal` (
  `journal_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL DEFAULT 0,
  `project` varchar(256) DEFAULT NULL,
  `journal_dt` date DEFAULT NULL,
  `journal_reff` varchar(256) DEFAULT NULL,
  `journal_type` varchar(50) DEFAULT NULL COMMENT 'tb_m_system',
  `journal_description` text DEFAULT NULL,
  `dr_total` decimal(25,2) DEFAULT NULL,
  `cr_total` decimal(25,2) DEFAULT NULL,
  `person_name` varchar(50) DEFAULT NULL,
  `reconcile_id` int(11) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`journal_id`),
  KEY `FK_tb_r_journal_tb_m_company` (`company_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_journal_items
CREATE TABLE IF NOT EXISTS `tb_r_journal_items` (
  `journal_item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `journal_id` bigint(20) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `item_description` varchar(100) DEFAULT NULL,
  `amount` decimal(25,2) DEFAULT NULL,
  `dc` char(1) DEFAULT NULL,
  `reconcile_id` int(11) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`journal_item_id`),
  KEY `FK_tb_r_journal_items_tb_r_journal` (`journal_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_profit_loss
CREATE TABLE IF NOT EXISTS `tb_r_profit_loss` (
  `profit_loss_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) DEFAULT NULL,
  `title` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET latin2 DEFAULT NULL,
  `footnote` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `show_account_code` char(1) CHARACTER SET latin2 DEFAULT '0',
  `col_name` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `col_start_dt` date DEFAULT NULL,
  `col_end_dt` date DEFAULT NULL,
  `created_by` varchar(50) CHARACTER SET latin2 DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin2 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`profit_loss_id`),
  KEY `FK_tb_r_profit_loss_tb_m_company` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_profit_loss_detail
CREATE TABLE IF NOT EXISTS `tb_r_profit_loss_detail` (
  `profit_loss_detail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `profit_loss_id` bigint(20) DEFAULT NULL,
  `col_name` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `col_start_dt` date DEFAULT NULL,
  `col_end_dt` date DEFAULT NULL,
  `col_order` int(11) DEFAULT 0,
  `created_by` varchar(50) CHARACTER SET latin2 DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin2 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`profit_loss_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_purchase_order
CREATE TABLE IF NOT EXISTS `tb_r_purchase_order` (
  `order_id` varchar(13) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `no_reff` varchar(13) DEFAULT NULL,
  `request_id` varchar(13) DEFAULT NULL,
  `order_dt` date DEFAULT NULL,
  `order_dt_expired` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier_name` varchar(100) DEFAULT NULL,
  `supplier_address` varchar(255) DEFAULT NULL,
  `supplier_attention` varchar(100) DEFAULT NULL,
  `supplier_tax` double DEFAULT NULL,
  `order_total` decimal(25,2) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `tax_name` varchar(100) DEFAULT NULL,
  `tax_rate` double DEFAULT NULL,
  `order_tax` decimal(25,2) DEFAULT NULL,
  `term_id` varchar(50) DEFAULT NULL,
  `order_grand_total` decimal(25,2) DEFAULT NULL,
  `order_remarks` varchar(255) DEFAULT NULL,
  `po_file` text DEFAULT NULL,
  `gr_flag` char(1) DEFAULT '0',
  `status` varchar(255) DEFAULT '1',
  `top_day` int(11) DEFAULT NULL,
  `order_due_date` date DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`order_id`,`company_id`),
  KEY `FK_tb_r_purchase_order_tb_m_company` (`company_id`) USING BTREE,
  KEY `FK_tb_r_purchase_order_tb_m_supplier` (`supplier_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_purchase_order_items
CREATE TABLE IF NOT EXISTS `tb_r_purchase_order_items` (
  `order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(13) NOT NULL DEFAULT '',
  `company_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `item_price_buy` double DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `item_unit` varchar(50) DEFAULT NULL,
  `item_amount` double DEFAULT NULL,
  `item_qty_taken` int(11) DEFAULT 0,
  `item_qty_remain` int(11) DEFAULT 0,
  `item_amount_taken` double DEFAULT 0,
  `item_amount_remain` double DEFAULT 0,
  `taxable` char(1) DEFAULT '0',
  `tax_amount` double DEFAULT 0,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`order_detail_id`),
  KEY `FK_tb_r_purchase_order_items_tb_r_purchase_order` (`order_id`) USING BTREE,
  KEY `FK_tb_r_purchase_order_items_tb_m_company` (`company_id`) USING BTREE,
  KEY `FK_tb_r_purchase_order_items_tb_m_items` (`item_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_purchase_payment
CREATE TABLE IF NOT EXISTS `tb_r_purchase_payment` (
  `payment_id` varchar(13) CHARACTER SET latin1 NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `no_reff` varchar(13) NOT NULL,
  `order_id` varchar(13) DEFAULT NULL,
  `gr_id` varchar(13) DEFAULT NULL,
  `payment_dt` date DEFAULT NULL,
  `payment_dt_expired` date DEFAULT NULL,
  `payment_method` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `payment_remarks` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `payment_amount` double DEFAULT NULL,
  `payment_file` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier_name` varchar(100) DEFAULT NULL,
  `supplier_address` varchar(255) DEFAULT NULL,
  `supplier_tax` double DEFAULT NULL,
  `supplier_attention` varchar(100) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `term_id` varchar(50) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `payment_total` decimal(25,2) DEFAULT NULL,
  `payment_tax` decimal(25,2) DEFAULT NULL,
  `payment_grand_total` decimal(25,2) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `tax_name` varchar(100) DEFAULT NULL,
  `tax_rate` double DEFAULT NULL,
  `journal_id` bigint(20) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`payment_id`,`company_id`),
  KEY `FK_tb_r_purchase_payment_tb_m_company` (`company_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_purchase_payment_items
CREATE TABLE IF NOT EXISTS `tb_r_purchase_payment_items` (
  `payment_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_id` varchar(13) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `item_price_buy` double DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `item_unit` varchar(50) DEFAULT NULL,
  `item_amount` double DEFAULT NULL,
  `taxable` char(1) DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) DEFAULT 'system',
  PRIMARY KEY (`payment_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_purchase_request
CREATE TABLE IF NOT EXISTS `tb_r_purchase_request` (
  `request_id` varchar(13) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `no_reff` varchar(13) DEFAULT NULL,
  `request_dt` date DEFAULT NULL,
  `request_dt_expired` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier_name` varchar(100) DEFAULT NULL,
  `supplier_address` varchar(255) DEFAULT NULL,
  `supplier_attention` varchar(100) DEFAULT NULL,
  `supplier_tax` double DEFAULT NULL,
  `request_total` decimal(25,2) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `tax_name` varchar(100) DEFAULT NULL,
  `tax_rate` double DEFAULT NULL,
  `request_tax` decimal(25,2) DEFAULT NULL,
  `request_grand_total` decimal(25,2) DEFAULT NULL,
  `request_remarks` varchar(255) DEFAULT NULL,
  `order_flag` char(1) DEFAULT '0',
  `status` char(1) DEFAULT '1' COMMENT '0: DRAFT, 1: UNDER EVALUATION, 3: CLOSED - ORDER CREATED, 4: CLOSED - REJECTED',
  `term_id` varchar(50) DEFAULT '0',
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`request_id`,`company_id`),
  KEY `FK_tb_r_purchase_request_tb_m_company` (`company_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_purchase_request_items
CREATE TABLE IF NOT EXISTS `tb_r_purchase_request_items` (
  `request_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` varchar(13) NOT NULL DEFAULT '',
  `company_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `item_price_buy` double DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `item_unit` varchar(50) DEFAULT NULL,
  `item_amount` double DEFAULT NULL,
  `taxable` char(1) DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) DEFAULT 'system',
  PRIMARY KEY (`request_detail_id`),
  KEY `FK_tb_r_purchase_request_items_tb_r_purchase_request` (`request_id`) USING BTREE,
  KEY `FK_tb_r_purchase_request_items_tb_m_company` (`company_id`) USING BTREE,
  KEY `FK_tb_r_purchase_request_items_tb_m_items` (`item_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_purchase_return
CREATE TABLE IF NOT EXISTS `tb_r_purchase_return` (
  `return_id` varchar(13) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `no_reff` varchar(13) DEFAULT NULL,
  `gr_id` varchar(13) DEFAULT NULL,
  `return_dt` date DEFAULT NULL,
  `return_dt_expired` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier_name` varchar(100) DEFAULT NULL,
  `supplier_address` varchar(255) DEFAULT NULL,
  `supplier_attention` varchar(100) DEFAULT NULL,
  `supplier_tax` double DEFAULT NULL,
  `return_total` decimal(25,2) DEFAULT NULL,
  `return_tax` decimal(25,2) DEFAULT NULL,
  `return_grand_total` decimal(25,2) DEFAULT NULL,
  `return_remarks` varchar(255) DEFAULT NULL,
  `return_file` text DEFAULT NULL,
  `payment_flag` char(1) DEFAULT '0',
  `tax_id` int(11) DEFAULT NULL,
  `tax_name` varchar(100) DEFAULT NULL,
  `tax_rate` double DEFAULT NULL,
  `term_id` varchar(50) DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `journal_id` bigint(20) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`return_id`),
  KEY `FK_tb_r_purchase_return_tb_m_company` (`company_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_purchase_return_items
CREATE TABLE IF NOT EXISTS `tb_r_purchase_return_items` (
  `return_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` varchar(13) NOT NULL DEFAULT '',
  `company_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `item_price_buy` double DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `item_unit` varchar(50) DEFAULT NULL,
  `item_amount` double DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `taxable` char(1) DEFAULT '0',
  `tax_amount` double DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) DEFAULT 'system',
  PRIMARY KEY (`return_detail_id`),
  KEY `FK_tb_r_purchase_return_items_tb_r_purchase_return` (`return_id`) USING BTREE,
  KEY `FK_tb_r_purchase_return_items_tb_m_company` (`company_id`) USING BTREE,
  KEY `FK_tb_r_purchase_return_items_tb_m_items` (`item_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_reconcile
CREATE TABLE IF NOT EXISTS `tb_r_reconcile` (
  `reconcile_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL DEFAULT 0,
  `account_id` bigint(20) DEFAULT NULL,
  `last_reconcile_dt` date DEFAULT NULL,
  `reconcile_dt` date DEFAULT NULL,
  `bank_statement_end_balance` double DEFAULT NULL,
  `start_balance` double DEFAULT NULL,
  `end_balance` double DEFAULT NULL,
  `difference` double DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  PRIMARY KEY (`reconcile_id`),
  KEY `FK_tb_r_reconcile_tb_m_company` (`company_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_reconcile_entries
CREATE TABLE IF NOT EXISTS `tb_r_reconcile_entries` (
  `reconcile_entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `reconcile_id` int(11) DEFAULT NULL,
  `journal_id` bigint(20) DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) CHARACTER SET latin1 DEFAULT 'system',
  PRIMARY KEY (`reconcile_entry_id`),
  KEY `FK_tb_r_reconcile_entries_tb_r_reconcile` (`reconcile_id`) USING BTREE,
  KEY `FK_tb_r_reconcile_entries_tb_r_journal` (`journal_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_register
CREATE TABLE IF NOT EXISTS `tb_r_register` (
  `register_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `company_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `industry_type` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `activation_code` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `activation_expired` datetime DEFAULT NULL,
  `activation` char(1) CHARACTER SET latin1 DEFAULT '0',
  PRIMARY KEY (`register_id`),
  KEY `FK_tb_r_register_tb_m_package` (`package_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_sales_delivery
CREATE TABLE IF NOT EXISTS `tb_r_sales_delivery` (
  `delivery_id` varchar(50) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `no_reff` varchar(50) DEFAULT NULL,
  `delivery_type` varchar(2) DEFAULT NULL,
  `delivery_dt` date DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_attention` varchar(100) DEFAULT NULL,
  `customer_tax` double DEFAULT NULL,
  `delivery_total` decimal(25,2) DEFAULT NULL,
  `delivery_tax` decimal(25,2) DEFAULT NULL,
  `delivery_grand_total` decimal(25,2) DEFAULT NULL,
  `delivery_remarks` varchar(255) DEFAULT NULL,
  `gr_file` text DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `invoice_flag` char(1) DEFAULT '0',
  `status` char(1) DEFAULT '1',
  `tax_id` int(11) DEFAULT NULL,
  `tax_name` varchar(100) DEFAULT NULL,
  `tax_rate` double DEFAULT NULL,
  `journal_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`delivery_id`,`company_id`),
  KEY `company_id` (`company_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_sales_delivery_items
CREATE TABLE IF NOT EXISTS `tb_r_sales_delivery_items` (
  `delivery_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_id` varchar(50) NOT NULL DEFAULT '',
  `company_id` bigint(20) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_name` varchar(100) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `item_price` double DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `item_amount` double DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) DEFAULT 'system',
  `taxable` char(1) DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  `item_unit` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`delivery_detail_id`,`delivery_id`,`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_sales_invoice
CREATE TABLE IF NOT EXISTS `tb_r_sales_invoice` (
  `invoice_id` varchar(50) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `no_reff` varchar(50) DEFAULT NULL,
  `invoice_dt` date DEFAULT NULL,
  `invoice_title` varchar(255) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_attention` varchar(100) DEFAULT NULL,
  `customer_tax` double DEFAULT NULL,
  `invoice_total` decimal(25,2) DEFAULT NULL,
  `invoice_tax` decimal(25,2) DEFAULT NULL,
  `invoice_grand_total` decimal(25,2) DEFAULT NULL,
  `invoice_remarks` varchar(255) DEFAULT NULL,
  `pos_amount_payment` decimal(25,0) DEFAULT NULL,
  `fp_serial_no` varchar(50) DEFAULT NULL,
  `top_day` int(11) DEFAULT NULL,
  `paid_due_date` date DEFAULT NULL,
  `status` char(1) DEFAULT '1',
  `tax_file` text DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `tax_name` varchar(100) DEFAULT NULL,
  `tax_rate` double DEFAULT NULL,
  `tax_no` varchar(45) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`invoice_id`,`company_id`),
  KEY `company_id` (`company_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_sales_invoice_items
CREATE TABLE IF NOT EXISTS `tb_r_sales_invoice_items` (
  `invoice_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(50) NOT NULL DEFAULT '',
  `company_id` bigint(20) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `item_price` double DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `item_unit` varchar(50) DEFAULT NULL,
  `item_amount` double DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) DEFAULT 'system',
  `taxable` char(1) DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  PRIMARY KEY (`invoice_detail_id`,`invoice_id`,`company_id`),
  KEY `invoice_id` (`invoice_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_sales_order
CREATE TABLE IF NOT EXISTS `tb_r_sales_order` (
  `order_id` varchar(50) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `no_reff` varchar(50) DEFAULT NULL,
  `order_dt` date DEFAULT NULL,
  `order_dt_expired` date DEFAULT NULL,
  `order_title` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_attention` varchar(100) DEFAULT NULL,
  `order_subtotal` decimal(25,2) DEFAULT NULL,
  `order_discount` decimal(25,2) DEFAULT NULL,
  `order_total` decimal(25,2) DEFAULT NULL,
  `order_tax` decimal(25,2) DEFAULT NULL,
  `order_grand_total` decimal(25,2) DEFAULT NULL,
  `order_remarks` varchar(255) DEFAULT NULL,
  `po_file` text DEFAULT NULL,
  `status` char(1) DEFAULT '1',
  `term_id` varchar(50) DEFAULT NULL,
  `top_day` int(11) DEFAULT NULL,
  `paid_due_date` date DEFAULT NULL,
  `amount_paid` double DEFAULT 0,
  `amount_remaining` double DEFAULT 0,
  `delivery_flag` char(1) DEFAULT '0',
  `tax_id` int(11) DEFAULT NULL,
  `tax_rate` double DEFAULT NULL,
  `tax_name` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`order_id`,`company_id`),
  KEY `FK_tb_r_sales_order_tb_m_company` (`company_id`) USING BTREE,
  KEY `FK_tb_r_sales_order_tb_m_customer` (`customer_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_sales_order_items
CREATE TABLE IF NOT EXISTS `tb_r_sales_order_items` (
  `order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(50) NOT NULL DEFAULT '',
  `company_id` bigint(20) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_code` varchar(50) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `item_price` double DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `item_discount` double DEFAULT NULL,
  `item_amount` double DEFAULT NULL,
  `item_qty_taken` int(11) DEFAULT 0,
  `item_qty_remain` int(11) DEFAULT 0,
  `item_amount_taken` double DEFAULT 0,
  `item_amount_remain` double DEFAULT 0,
  `taxable` char(1) DEFAULT '0',
  `tax_id` int(11) DEFAULT 0,
  `tax_name` varchar(100) DEFAULT '0',
  `tax_rate` double DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  `invoice_qty_taken` int(11) DEFAULT 0,
  `invoice_qty_remain` int(11) DEFAULT 0,
  `invoice_amount_taken` double DEFAULT 0,
  `invoice_amount_remain` double DEFAULT 0,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`order_detail_id`,`company_id`),
  KEY `FK_tb_r_sales_order_items_tb_r_sales_order` (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_sales_payment
CREATE TABLE IF NOT EXISTS `tb_r_sales_payment` (
  `payment_id` varchar(50) CHARACTER SET latin1 NOT NULL,
  `invoice_id` varchar(50) CHARACTER SET latin1 NOT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) NOT NULL,
  `payment_dt` date DEFAULT NULL,
  `payment_method` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `payment_remarks` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `payment_amount` double(50,0) DEFAULT 0,
  `discount` double(50,0) DEFAULT NULL,
  `payment_file` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `journal_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`payment_id`,`invoice_id`,`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_sales_quotation
CREATE TABLE IF NOT EXISTS `tb_r_sales_quotation` (
  `quotation_id` varchar(50) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `no_reff` varchar(50) DEFAULT NULL,
  `quotation_dt` date DEFAULT NULL,
  `quotation_dt_expired` date DEFAULT NULL,
  `quotation_title` varchar(255) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_attention` varchar(100) DEFAULT NULL,
  `quotation_subtotal` decimal(25,2) DEFAULT NULL,
  `quotation_discount` decimal(25,2) DEFAULT NULL,
  `quotation_total` decimal(25,2) DEFAULT NULL,
  `quotation_tax` decimal(25,2) DEFAULT NULL,
  `quotation_grand_total` decimal(25,2) DEFAULT NULL,
  `quotation_remarks` text DEFAULT NULL,
  `order_flag` char(1) DEFAULT '0',
  `status` char(1) DEFAULT '1' COMMENT '0: DRAFT, 1: UNDER EVALUATION, 3: CLOSED - ORDER CREATED, 4: CLOSED - REJECTED',
  `term_id` varchar(50) DEFAULT '0',
  `order_id` varchar(13) DEFAULT NULL,
  `invoice_id` varchar(13) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'system',
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`quotation_id`,`company_id`),
  KEY `FK_tb_r_sales_quotation_tb_m_company` (`company_id`) USING BTREE,
  KEY `FK_tb_r_sales_quotation_tb_m_customer` (`customer_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table moadrpcd_corporate.tb_r_sales_quotation_items
CREATE TABLE IF NOT EXISTS `tb_r_sales_quotation_items` (
  `quotation_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_id` varchar(50) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `item_code` varchar(50) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_description` varchar(255) DEFAULT NULL,
  `item_price` double DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `item_discount` double DEFAULT NULL,
  `item_amount` double DEFAULT NULL,
  `taxable` char(1) DEFAULT '0',
  `tax_id` int(11) DEFAULT 0,
  `tax_name` varchar(100) DEFAULT '0',
  `tax_rate` double DEFAULT 0,
  `tax_amount` double DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(50) DEFAULT 'system',
  `changed_dt` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) DEFAULT 'system',
  PRIMARY KEY (`quotation_detail_id`),
  KEY `FK_tb_r_sales_quotation_items_tb_m_items` (`item_id`) USING BTREE,
  KEY `FK_tb_r_sales_quotation_items_tb_m_company` (`company_id`) USING BTREE,
  KEY `FK_tb_r_sales_quotation_items_tb_r_sales_quotation` (`quotation_id`,`company_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for procedure moadrpcd_corporate.sp_delete_move_trans
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_move_trans`(IN `p_account_id_update` INT, IN `p_changed_by` VARCHAR(50), IN `p_account_id_delete` INT, IN `p_cid` INT)
BEGIN
	update tb_r_journal_items ji
	inner join 
		tb_r_journal j on j.journal_id = ji.journal_id
	set 
		ji.account_id = p_account_id_update 		, ji.changed_by = p_changed_by 	where
		ji.account_id = p_account_id_delete 			;
		delete from tb_m_account where account_id = p_account_id_delete;
END//
DELIMITER ;

-- Dumping structure for procedure moadrpcd_corporate.sp_delete_trash_trans
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_delete_trash_trans`(IN `p_account_id_delete` BIGINT)
BEGIN
		drop table if exists temp_journal_id;
	create table temp_journal_id (
		journal_id int primary key
	);
	insert into temp_journal_id
	select j.journal_id from tb_r_journal j
	inner join tb_r_journal_items ji on ji.journal_id = j.journal_id
	where ji.account_id = p_account_id_delete
	;
	delete tb_r_journal_items from tb_r_journal_items
	inner join temp_journal_id
	on temp_journal_id.journal_id = tb_r_journal_items.journal_id
	;
	delete tb_r_journal from tb_r_journal
	inner join temp_journal_id
	on temp_journal_id.journal_id = tb_r_journal.journal_id
	;
	delete from tb_m_account where account_id = p_account_id_delete
	;
	drop table if exists temp_journal_id;
END//
DELIMITER ;

-- Dumping structure for procedure moadrpcd_corporate.sp_gl
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_gl`(IN `p_company_id` INT, IN `p_accounts` TEXT, IN `p_s` DATE, IN `p_e` DATE, IN `p_sv` TEXT)
BEGIN
	declare p_first_row int;
	declare sum_d decimal(25,2);
	declare sum_c decimal(25,2);
	declare p_balance decimal(25,2);
	drop table if exists tb_t_journal;
	CREATE TABLE tb_t_journal 
   (
     rownumber int(11) NOT NULL auto_increment primary key,
     journal_id int(11) default NULL,
     journal_dt date,
     journal_reff varchar(50),
     journal_description varchar(255),
     amount decimal(25,2),
     dc char(1),
     account_id int(11),
     account_name varchar(255),
     created_dt datetime,
     item_description varchar(255)
   ) 
   ENGINE=InnoDB DEFAULT CHARSET=utf8;
		insert into tb_t_journal(journal_id, journal_dt, journal_reff, journal_description, amount, dc, account_id, account_name, created_dt, item_description)
	(
		select 
			b.journal_id,
			b.journal_dt,
			b.journal_reff,
			b.journal_description,
			a.amount,
			a.dc,
			a.account_id,
			c.account_name,
			a.created_dt ,
			a.item_description
		from 
			tb_r_journal_items a
			inner join tb_r_journal b on b.journal_id = a.journal_id
			inner join tb_m_account c on c.account_id = a.account_id
		where 
			b.company_id = p_company_id
			and a.account_id = p_accounts
			and (b.journal_description like CONCAT('%',p_sv,'%')
			or ifnull(a.item_description, '') like CONCAT('%',p_sv,'%'))
		order by b.journal_dt, journal_id	
	);
	set p_first_row = ifnull( 
		(
			select rownumber from
			(
				select * from tb_t_journal 
				where 
					journal_dt between p_s and p_e
				order by journal_dt, rownumber
			) e limit 1
		)
		, 0
	);
	set sum_d = ifnull(
		(
			select sum(amount) from tb_t_journal where rownumber < p_first_row and dc = 'd' group by account_id 
		)
		, 0
	);
	set sum_c = ifnull(
		(
			select sum(amount) from tb_t_journal where rownumber < p_first_row and dc = 'c' group by account_id 
		)
		, 0
	);
	set p_balance = sum_d - sum_c;
	select 
		*
		, 
		case 
			when dc = 'd' then @runbalance := @runbalance + amount
			when dc = 'c' then @runbalance := @runbalance - amount
		end as balance
	from 
		tb_t_journal
		, (SELECT @runbalance:=p_balance) r 
	where 
		journal_dt between p_s and p_e
	order by journal_dt, rownumber;
	drop table if exists tb_t_journal;
END//
DELIMITER ;

-- Dumping structure for procedure moadrpcd_corporate.sp_tes
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_tes`(IN `p_company_id` BIGINT, IN `p_account_id` INT, IN `p_s` DATE, IN `p_e` DATE, IN `p_start` INT, IN `p_length` INT, IN `p_sv` TEXT)
BEGIN
	declare p_first_row int;
	declare sum_d decimal(25,2);
	declare sum_c decimal(25,2);
	declare p_balance decimal(25,2);
	drop table if exists tb_t_journal;
	CREATE TABLE tb_t_journal 
   (
     rownumber int(11) NOT NULL auto_increment primary key,
     journal_id int(11) default NULL,
     journal_dt date,
     journal_reff varchar(50),
     journal_description varchar(255),
     item_description varchar(255),
     amount decimal(25,2),
     dc char(1),
     account_id int(11),
     created_dt datetime
   ) 
   ENGINE=InnoDB DEFAULT CHARSET=utf8;
	insert into tb_t_journal(journal_id, journal_dt, journal_reff, journal_description, item_description, amount, dc, account_id, created_dt)
	(
		select 
			b.journal_id,
			b.journal_dt,
			b.journal_reff,
			b.journal_description,
			a.item_description,
			a.amount,
			a.dc,
			a.account_id,
			a.created_dt 
		from 
			tb_r_journal_items a
			inner join tb_r_journal b on b.journal_id = a.journal_id
		where 
			b.company_id = p_company_id
			and account_id = p_account_id
			and (b.journal_description like CONCAT('%',p_sv,'%')
			or ifnull(a.item_description, '') like CONCAT('%',p_sv,'%'))
		order by b.journal_dt, journal_id	
	);
	set p_first_row = ifnull( 
		(
			select rownumber from
			(
				select * from tb_t_journal 
				where 
					journal_dt between p_s and p_e
				order by journal_dt, rownumber
				limit p_start, p_length
			) e limit 1
		)
		, 0
	);
	set sum_d = ifnull(
		(
			select sum(amount) from tb_t_journal where rownumber < p_first_row and dc = 'd' group by account_id 
		)
		, 0
	);
	set sum_c = ifnull(
		(
			select sum(amount) from tb_t_journal where rownumber < p_first_row and dc = 'c' group by account_id 
		)
		, 0
	);
	set p_balance = sum_d - sum_c;
	select 
		*
		, 
		case 
			when dc = 'd' then @runbalance := @runbalance + amount
			when dc = 'c' then @runbalance := @runbalance - amount
		end as balance
	from 
		tb_t_journal
		, (SELECT @runbalance:=p_balance) r 
	where 
		journal_dt between p_s and p_e
	order by journal_dt, rownumber
	limit p_start, p_length;
	drop table if exists tb_t_journal;
END//
DELIMITER ;

-- Dumping structure for procedure moadrpcd_corporate.sp_tes_count
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_tes_count`(IN `p_company_id` INT, IN `p_account_id` INT, IN `p_s` DATE, IN `p_e` DATE, IN `p_sv` TEXT)
BEGIN
	select 
		count(a.journal_item_id) as cnt
	from 
		tb_r_journal_items a
		inner join tb_r_journal b on b.journal_id = a.journal_id
	where 
		b.company_id = p_company_id
		and (b.journal_dt between p_s and p_e)
		and account_id = p_account_id
		and b.journal_description like CONCAT('%',p_sv,'%')
		and ifnull(a.item_description, '') like CONCAT('%',p_sv,'%')
	order by b.journal_dt
;
END//
DELIMITER ;

-- Dumping structure for procedure moadrpcd_corporate.sp_update_account_balance
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_account_balance`()
BEGIN
	update tb_m_account set account_balance = 0;
	update tb_m_account inner join
	(
		select
			a.account_id
									, coalesce(sum(a.d_sum), 0) - coalesce(sum(a.c_sum), 0) as balance
		from
		(
			select 
				ji.account_id
				, sum(ji.amount) as d_sum 
				, null as c_sum
			from 
				tb_r_journal_items ji
			where ji.dc = 'd'
			group by ji.account_id
			union all
			select 
				ji.account_id
				, null d_sum
				, sum(ji.amount) as c_sum 
			from 
				tb_r_journal_items ji
			where ji.dc = 'c'
			group by ji.account_id
		) a
		group by a.account_id
	) u
	on u.account_id = tb_m_account.account_id
	set tb_m_account.account_balance = u.balance
	;
END//
DELIMITER ;

-- Dumping structure for procedure moadrpcd_corporate.sp_update_used_account
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_used_account`()
BEGIN
	update tb_m_account tbl
	inner join 
	(
		select a.account_id, coalesce(b.cnt, 0) as cnt from tb_m_account a
		left join 
		(
			select ji.account_id, count(ji.journal_item_id) as cnt from tb_r_journal_items ji
			inner join tb_r_journal j on j.journal_id = ji.journal_id
			group by ji.account_id
		) b 
		on a.account_id = b.account_id 
		order by a.account_id
	) trx
	on trx.account_id = tbl.account_id 
	set 
		tbl.used = 
		case 
			when trx.cnt > 0 then '1'
			when trx.cnt = 0 then '0'
		end;
END//
DELIMITER ;

-- Dumping structure for function moadrpcd_corporate.get_total_day
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `get_total_day`() RETURNS varchar(255) CHARSET utf8
BEGIN
   DECLARE a          VARCHAR(20);
   DECLARE b          VARCHAR(20);
   DECLARE c          VARCHAR(20);
   DECLARE d          VARCHAR(20) default 30;
   DECLARE today      VARCHAR(20);
   DECLARE next_day   VARCHAR(20);
   DECLARE day_total  VARCHAR(20); 
   SET today = (SELECT CURDATE() );
   SET next_day = (SELECT CURDATE() + INTERVAL 30 DAY);
   SET a = (SELECT 5 * (DATEDIFF(next_day, today) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(today) + WEEKDAY(next_day) + 2, 1));
   SET b = (SELECT COUNT(*) FROM tb_m_holiday_cal tmhc WHERE tmhc.holiday_dt BETWEEN CURDATE() AND CURDATE() + INTERVAL 30 DAY);  
   SET c = a - b;
   SET d =  d - c;
  SET day_total = (SELECT CURDATE() + INTERVAL (30+d) DAY);
  RETURN day_total;
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
