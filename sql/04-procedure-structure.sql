-- --------------------------------------------------------
-- Host:                         192.168.4.6
-- Versi server:                 10.1.21-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- membuang struktur untuk procedure moadrpcd_corporate.sp_delete_move_trans
DELIMITER //
CREATE PROCEDURE `sp_delete_move_trans`(IN `p_account_id_update` INT, IN `p_changed_by` VARCHAR(50), IN `p_account_id_delete` INT, IN `p_cid` INT)
BEGIN
	update tb_r_journal_items ji
	inner join 
		tb_r_journal j on j.journal_id = ji.journal_id
	set 
		ji.account_id = p_account_id_update 		, ji.changed_by = p_changed_by 	where
		ji.account_id = p_account_id_delete 			;
		delete from tb_m_account where account_id = p_account_id_delete;
END//
DELIMITER ;

-- membuang struktur untuk procedure moadrpcd_corporate.sp_delete_trash_trans
DELIMITER //
CREATE PROCEDURE `sp_delete_trash_trans`(IN `p_account_id_delete` BIGINT)
BEGIN
		drop table if exists temp_journal_id;
	create table temp_journal_id (
		journal_id int primary key
	);
	insert into temp_journal_id
	select j.journal_id from tb_r_journal j
	inner join tb_r_journal_items ji on ji.journal_id = j.journal_id
	where ji.account_id = p_account_id_delete
	;
	delete tb_r_journal_items from tb_r_journal_items
	inner join temp_journal_id
	on temp_journal_id.journal_id = tb_r_journal_items.journal_id
	;
	delete tb_r_journal from tb_r_journal
	inner join temp_journal_id
	on temp_journal_id.journal_id = tb_r_journal.journal_id
	;
	delete from tb_m_account where account_id = p_account_id_delete
	;
	drop table if exists temp_journal_id;
END//
DELIMITER ;

-- membuang struktur untuk procedure moadrpcd_corporate.sp_gl
DELIMITER //
CREATE PROCEDURE `sp_gl`(IN `p_company_id` INT, IN `p_accounts` TEXT, IN `p_s` DATE, IN `p_e` DATE, IN `p_sv` TEXT)
BEGIN
	declare p_first_row int;
	declare sum_d decimal(25,2);
	declare sum_c decimal(25,2);
	declare p_balance decimal(25,2);
	drop table if exists tb_t_journal;
	CREATE TABLE tb_t_journal 
   (
     rownumber int(11) NOT NULL auto_increment primary key,
     journal_id int(11) default NULL,
     journal_dt date,
     journal_reff varchar(50),
     journal_description varchar(255),
     amount decimal(25,2),
     dc char(1),
     account_id int(11),
     account_name varchar(255),
     created_dt datetime,
     item_description varchar(255)
   ) 
   ENGINE=InnoDB DEFAULT CHARSET=utf8;
		insert into tb_t_journal(journal_id, journal_dt, journal_reff, journal_description, amount, dc, account_id, account_name, created_dt, item_description)
	(
		select 
			b.journal_id,
			b.journal_dt,
			b.journal_reff,
			b.journal_description,
			a.amount,
			a.dc,
			a.account_id,
			c.account_name,
			a.created_dt ,
			a.item_description
		from 
			tb_r_journal_items a
			inner join tb_r_journal b on b.journal_id = a.journal_id
			inner join tb_m_account c on c.account_id = a.account_id
		where 
			b.company_id = p_company_id
			and a.account_id = p_accounts
			and (b.journal_description like CONCAT('%',p_sv,'%')
			or ifnull(a.item_description, '') like CONCAT('%',p_sv,'%'))
		order by b.journal_dt, journal_id	
	);
	set p_first_row = ifnull( 
		(
			select rownumber from
			(
				select * from tb_t_journal 
				where 
					journal_dt between p_s and p_e
				order by journal_dt, rownumber
			) e limit 1
		)
		, 0
	);
	set sum_d = ifnull(
		(
			select sum(amount) from tb_t_journal where rownumber < p_first_row and dc = 'd' group by account_id 
		)
		, 0
	);
	set sum_c = ifnull(
		(
			select sum(amount) from tb_t_journal where rownumber < p_first_row and dc = 'c' group by account_id 
		)
		, 0
	);
	set p_balance = sum_d - sum_c;
	select 
		*
		, 
		case 
			when dc = 'd' then @runbalance := @runbalance + amount
			when dc = 'c' then @runbalance := @runbalance - amount
		end as balance
	from 
		tb_t_journal
		, (SELECT @runbalance:=p_balance) r 
	where 
		journal_dt between p_s and p_e
	order by journal_dt, rownumber;
	drop table if exists tb_t_journal;
END//
DELIMITER ;

-- membuang struktur untuk procedure moadrpcd_corporate.sp_tes
DELIMITER //
CREATE PROCEDURE `sp_tes`(IN `p_company_id` BIGINT, IN `p_account_id` INT, IN `p_s` DATE, IN `p_e` DATE, IN `p_start` INT, IN `p_length` INT, IN `p_sv` TEXT)
BEGIN
	declare p_first_row int;
	declare sum_d decimal(25,2);
	declare sum_c decimal(25,2);
	declare p_balance decimal(25,2);
	drop table if exists tb_t_journal;
	CREATE TABLE tb_t_journal 
   (
     rownumber int(11) NOT NULL auto_increment primary key,
     journal_id int(11) default NULL,
     journal_dt date,
     journal_reff varchar(50),
     journal_description varchar(255),
     item_description varchar(255),
     amount decimal(25,2),
     dc char(1),
     account_id int(11),
     created_dt datetime
   ) 
   ENGINE=InnoDB DEFAULT CHARSET=utf8;
	insert into tb_t_journal(journal_id, journal_dt, journal_reff, journal_description, item_description, amount, dc, account_id, created_dt)
	(
		select 
			b.journal_id,
			b.journal_dt,
			b.journal_reff,
			b.journal_description,
			a.item_description,
			a.amount,
			a.dc,
			a.account_id,
			a.created_dt 
		from 
			tb_r_journal_items a
			inner join tb_r_journal b on b.journal_id = a.journal_id
		where 
			b.company_id = p_company_id
			and account_id = p_account_id
			and (b.journal_description like CONCAT('%',p_sv,'%')
			or ifnull(a.item_description, '') like CONCAT('%',p_sv,'%'))
		order by b.journal_dt, journal_id	
	);
	set p_first_row = ifnull( 
		(
			select rownumber from
			(
				select * from tb_t_journal 
				where 
					journal_dt between p_s and p_e
				order by journal_dt, rownumber
				limit p_start, p_length
			) e limit 1
		)
		, 0
	);
	set sum_d = ifnull(
		(
			select sum(amount) from tb_t_journal where rownumber < p_first_row and dc = 'd' group by account_id 
		)
		, 0
	);
	set sum_c = ifnull(
		(
			select sum(amount) from tb_t_journal where rownumber < p_first_row and dc = 'c' group by account_id 
		)
		, 0
	);
	set p_balance = sum_d - sum_c;
	select 
		*
		, 
		case 
			when dc = 'd' then @runbalance := @runbalance + amount
			when dc = 'c' then @runbalance := @runbalance - amount
		end as balance
	from 
		tb_t_journal
		, (SELECT @runbalance:=p_balance) r 
	where 
		journal_dt between p_s and p_e
	order by journal_dt, rownumber
	limit p_start, p_length;
	drop table if exists tb_t_journal;
END//
DELIMITER ;

-- membuang struktur untuk procedure moadrpcd_corporate.sp_tes_count
DELIMITER //
CREATE PROCEDURE `sp_tes_count`(IN `p_company_id` INT, IN `p_account_id` INT, IN `p_s` DATE, IN `p_e` DATE, IN `p_sv` TEXT)
BEGIN
	select 
		count(a.journal_item_id) as cnt
	from 
		tb_r_journal_items a
		inner join tb_r_journal b on b.journal_id = a.journal_id
	where 
		b.company_id = p_company_id
		and (b.journal_dt between p_s and p_e)
		and account_id = p_account_id
		and b.journal_description like CONCAT('%',p_sv,'%')
		and ifnull(a.item_description, '') like CONCAT('%',p_sv,'%')
	order by b.journal_dt
;
END//
DELIMITER ;

-- membuang struktur untuk procedure moadrpcd_corporate.sp_update_account_balance
DELIMITER //
CREATE PROCEDURE `sp_update_account_balance`()
BEGIN
	update tb_m_account set account_balance = 0;
	update tb_m_account inner join
	(
		select
			a.account_id
									, coalesce(sum(a.d_sum), 0) - coalesce(sum(a.c_sum), 0) as balance
		from
		(
			select 
				ji.account_id
				, sum(ji.amount) as d_sum 
				, null as c_sum
			from 
				tb_r_journal_items ji
			where ji.dc = 'd'
			group by ji.account_id
			union all
			select 
				ji.account_id
				, null d_sum
				, sum(ji.amount) as c_sum 
			from 
				tb_r_journal_items ji
			where ji.dc = 'c'
			group by ji.account_id
		) a
		group by a.account_id
	) u
	on u.account_id = tb_m_account.account_id
	set tb_m_account.account_balance = u.balance
	;
END//
DELIMITER ;

-- membuang struktur untuk procedure moadrpcd_corporate.sp_update_used_account
DELIMITER //
CREATE PROCEDURE `sp_update_used_account`()
BEGIN
	update tb_m_account tbl
	inner join 
	(
		select a.account_id, coalesce(b.cnt, 0) as cnt from tb_m_account a
		left join 
		(
			select ji.account_id, count(ji.journal_item_id) as cnt from tb_r_journal_items ji
			inner join tb_r_journal j on j.journal_id = ji.journal_id
			group by ji.account_id
		) b 
		on a.account_id = b.account_id 
		order by a.account_id
	) trx
	on trx.account_id = tbl.account_id 
	set 
		tbl.used = 
		case 
			when trx.cnt > 0 then '1'
			when trx.cnt = 0 then '0'
		end;
END//
DELIMITER ;

-- membuang struktur untuk function moadrpcd_corporate.get_total_day
DELIMITER //
CREATE moadrpcd_corporateFUNCTION `get_total_day`() RETURNS varchar(255) CHARSET utf8
BEGIN
   DECLARE a          VARCHAR(20);
   DECLARE b          VARCHAR(20);
   DECLARE c          VARCHAR(20);
   DECLARE d          VARCHAR(20) default 30;
   DECLARE today      VARCHAR(20);
   DECLARE next_day   VARCHAR(20);
   DECLARE day_total  VARCHAR(20); 
   SET today = (SELECT CURDATE() );
   SET next_day = (SELECT CURDATE() + INTERVAL 30 DAY);
   SET a = (SELECT 5 * (DATEDIFF(next_day, today) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(today) + WEEKDAY(next_day) + 2, 1));
   SET b = (SELECT COUNT(*) FROM tb_m_holiday_cal tmhc WHERE tmhc.holiday_dt BETWEEN CURDATE() AND CURDATE() + INTERVAL 30 DAY);  
   SET c = a - b;
   SET d =  d - c;
  SET day_total = (SELECT CURDATE() + INTERVAL (30+d) DAY);
  RETURN day_total;
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
