
$(document).ready(function(){
    // set focus
    $('.select2').select2();
});

function create()
{
    $('#user_email').val('');
    $('#full_name').val('');
    $('#act').val('create');
    $('.pk').show();

    $('#tpass').html('Password Default');
    $('#myModalLabel').html('Create');
    $('#myModal').modal('show');    
}

function edit(e, n, g)
{
    $('#user_email').val(e);
    $('#full_name').val(n);
    $('#user_group_id').select2('val', g);
    $('#act').val('edit');
    $('.pk').hide();

    $('#myModalLabel').html('Edit');
    $('#myModal').modal('show');    
}

function reset(e)
{
    $('#user_email').val(e);
    $('#act').val('reset');
    $('.pk').hide();
    $('.mk').hide();
    $('.pk-reset').show();

    $('#tpass').html('Reset Password');
    $('#myModalLabel').html('Edit');
    $('#myModal').modal('show');    
}


function doSave() {
    if ($('#frm').valid()) 
	{
        $('#btn_save_user').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('#btn_save_user').attr('disabled', 'disabled');
        $('#frm').submit();
    }
    else
	{
		toastr["error"]("Please input required fields")
	}
}

function confirmDelete(id)
{
    $('#user_email_delete').val(id);
    $('#myModal2').modal('show');
}

function doDelete()
{
	$('#btn_delete_confirm').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
	$('#btn_delete_confirm').attr('disabled', 'disabled');
	
	$('#frm2').submit();
}