
$(document).ready(function(){
	$('.select2').select2();

	//initialize datatable
    oTable = $('#datatable').DataTable(
	{
		fixedHeader: true,
        ordering : true,
        order: [[ 0, "asc" ]],
		lengthMenu : [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
		paging : true,
		serverSide : true,
		ajax :
		{
			url: SITE_URL + "emoney/gets",
			type: "POST",
			data : function (d)
			{
				d.route = $('#route').select2('val');
				d.customer = $('#customer').select2('val');
				d.category = $('#category').select2('val');
			}
		},
		//dom : '<"top">rt<"bottom"lpi><"clear">',
		pagingType : 'full_numbers',
		processing : true,
		columnDefs :
				[
						{ targets : 0, name : 'emoney_id'},
            { targets : 1, name : 'bank'},
            { targets : 2, name : 'balance', className: 'text-right'},
						{ targets : 3, name : 'customer'},
						{ targets : 4, name : 'emoney_ctg'},
						{ targets : 5, name : 'route'},
						{ targets : 6, name : 'cycle'},
						{ targets : 7, name : 'gel'},
        ],
        scrollX : true,
	});
});

function doFilter()
{
	// if ($('#form_journal').valid())
	// {
		oTable.ajax.reload();
	// }
}

function doDownload()
{
	window.location.href = SITE_URL + 'emoney/download';
}

function doUpload()
{
	$('#myModal').modal('show');
}

function doUploadProcess()
{
	if ($('#frm').valid())
	{
		var formData = new FormData($('#frm')[0]);
		$('#btn_upload_confirm').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('#btn_upload_confirm').attr('disabled', 'disabled');

		$.ajax({
			type: "POST",
			url: SITE_URL + "emoney/upload",
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			success: function (data) {

				$('#btn_upload_confirm').html('Upload');
				$('#btn_upload_confirm').removeAttr('disabled');

				if (!data.success)
				{
					toastr['error'](data.message);
				}
				else
				{
					toastr['success'](data.message);
					doFilter();
					$('#myModal').modal('hide');
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				$('#btn_upload_confirm').html('Upload');
				$('#btn_upload_confirm').removeAttr('disabled');

				toastr["error"]("Error when processing Data");
			}
		});
	}
	else
	{
		toastr["error"]("Please input required fields")
	}
}

function checkextension() {
    var file = document.querySelector("#file_attach");
    if (/\.(xlsx)$/i.test(file.files[0].name) === false) {
        // alert("Upload file dengan format .xlsx!");
		toastr["error"]("Error. Make sure file template is Excel (xlsx)");
        $('#file_nm').val('');
        $('#file_attach').val('');
    } else {
        $('#file_nm').val(file.files[0].name);
    }
}
