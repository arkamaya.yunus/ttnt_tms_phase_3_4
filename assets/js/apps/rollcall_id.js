var cashier_fuel_actual;		
var cashier_fuel_remark;		
var cashier_etoll_cardno;
var cashier_etoll_amount_start;
var cashier_etoll_balance;	
var cashier_etoll_amount_end;
var cashier_etoll_remark;		
var cashier_others_actual;		
var cashier_others_remark;

var controller_apd_departure;
var controller_apd_arrival;
var controller_basket_departure;
var controller_basket_arrival;
var controller_key_departure;
var controller_key_arrival;


$(document).ready(function(){
    // initiate
    $('.autonumber').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '1'});
    $('.autonumber2').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

    // hide sidebar menu
    $('.button-menu-mobile').trigger('click');

    // first of all
    $('#cashier_etoll_balance').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

    // syncAttendance();
    calculateBalance();

    $('.role').attr('disabled', 'disabled');
    $('.role-' + $('#role_nm').val()).removeAttr('disabled');

    // disabled if status = 0 DRAFT
    // update: 12-Juni 2020 disabled based on rolename and status
    status = $('#status').val();
    rolenm = $('#role_nm').val();

    if (rolenm == 'adminsales')
    {

        if (status == 0) // teamdriver
        {
            $('.ctl-departure').removeAttr('disabled');
            $('.ctl-arrival').attr('disabled', 'disabled');
        }
        else if (status == 1) // departure - cashier
        {
            $('.ctl-departure').attr('disabled', 'disabled');
            $('.ctl-arrival').attr('disabled', 'disabled');
        }
        else if (status == 2) // departure - ccr
        {
            $('.ctl-departure').attr('disabled', 'disabled');
            $('.ctl-arrival').removeAttr('disabled');
        }
        else if (status == 3 || status == 4) // arrival - cashier & Finish
        {
            $('.ctl-departure').attr('disabled', 'disabled');
            $('.ctl-arrival').attr('disabled', 'disabled');
        }

        $('.role-controller').attr('disabled', 'disabled');

    }
    else if (rolenm == 'controller')
    {

        if (status == 0) // teamdriver
        {
            $('.ctl-departure').attr('disabled', 'disabled');
            $('.ctl-arrival').attr('disabled', 'disabled');
        }
        else if (status == 1) // departure - cashier
        {
            $('.ctl-departure').removeAttr('disabled');
            $('.ctl-arrival').attr('disabled', 'disabled');
        }
        else if (status == 2) // departure - ccr
        {
            $('.ctl-departure').attr('disabled', 'disabled');
            $('.ctl-arrival').attr('disabled', 'disabled');
        }
        else if (status == 3 ) //  arrival - cashier
        {
            $('.ctl-departure').attr('disabled', 'disabled');
            $('.ctl-arrival').removeAttr('disabled');
        }
        else if (status == 4 ) //  Finished
        {
            $('.ctl-departure').attr('disabled', 'disabled');
            $('.ctl-arrival').attr('disabled', 'disabled');
        }

        $('.role-adminsales').attr('disabled', 'disabled');

    }
    // if (status != 2)
    // {
    //     $('.ctl-arrival ').attr('disabled', 'disabled');
    // }
    // else if (status == 2)
    // {
    //     $('.ctl-departure').attr('disabled', 'disabled');
    // }

    // 16-Juli-2020
    // adding some color
    // 29-Juni-2020 validasi
    var present_date = new Date(); 

    // driving_license_val
    var driving_license_val = $('#driving_license_val').html();
    var diffdays_sim = datediff(present_date, parseDate(driving_license_val));

    $('#driving_license_val').removeClass('bg bg-warning bg-danger bg-success font-white');
    if (diffdays_sim >= 1 && diffdays_sim < 30)
    {
        $('#driving_license_val').addClass('bg bg-warning');
    }else if (diffdays_sim <=0 )
    {
        $('#driving_license_val').addClass('bg bg-danger font-white');
    }else
    {
        $('#driving_license_val').addClass('bg bg-success font-white');
    }

    // forklift_license_val
    var forklift_license_val = $('#forklift_license_val').html();
    var diffdays_sio = datediff(present_date, parseDate(forklift_license_val));

    $('#forklift_license_val').removeClass('bg bg-warning bg-danger bg-success font-white');
    if (diffdays_sio >= 1 && diffdays_sio < 30)
    {
        $('#forklift_license_val').addClass('bg bg-warning');
    }else if (diffdays_sio <=0 )
    {
        $('#forklift_license_val').addClass('bg bg-danger font-white');
    }else
    {
        $('#forklift_license_val').addClass('bg bg-success font-white');
    }
    
    // license_stnk
    var license_stnk = $('#license_stnk').html();
    var diffdays_stnk = datediff(present_date, parseDate(license_stnk));

    $('#license_stnk').removeClass('bg bg-warning bg-danger bg-success font-white');
    if (diffdays_stnk >= 1 && diffdays_stnk < 30)
    {
        $('#license_stnk').addClass('bg bg-warning');
    }else if (diffdays_stnk <=0 )
    {
        $('#license_stnk').addClass('bg bg-danger font-white');
    }else
    {
        $('#license_stnk').addClass('bg bg-success font-white');
    }

    // license_keur
    var license_keur = $('#license_keur').html();                
    var diffdays_keur = datediff(present_date, parseDate(license_keur));

    $('#license_keur').removeClass('bg bg-warning bg-danger bg-success font-white');
    if (diffdays_keur >= 1 && diffdays_keur < 7)
    {
        $('#license_keur').addClass('bg bg-warning');
    }else if (diffdays_keur <=0 )
    {
        $('#license_keur').addClass('bg bg-danger font-white');
    }else
    {
        $('#license_keur').addClass('bg bg-success font-white');
    }

    // license_sipa
    var license_sipa = $('#license_sipa').html();                
    var diffdays_sipa = datediff(present_date, parseDate(license_sipa));

    $('#license_sipa').removeClass('bg bg-warning bg-danger bg-success font-white');
    if (diffdays_sipa >= 1 && diffdays_sipa < 7)
    {
        $('#license_sipa').addClass('bg bg-warning');
    }else if (diffdays_sipa <=0 )
    {
        $('#license_sipa').addClass('bg bg-danger font-white');
    }else
    {
        $('#license_sipa').addClass('bg bg-success font-white');
    }

}); 

function toggleOn2(id, el)
{
    var val = $('#' + el).val();    
    
    if (val == '0' && $(id).html() == 'OK' || val == '' && $(id).html() == 'OK')
    {
        $('.btn-' + el).removeClass('btn-success');
        $('.btn-' + el).removeClass('btn-danger');
        $('.btn-' + el).addClass('btn-dflt');

        $(id).addClass('btn-success');
        $(id).removeClass('btn-dflt');
        $('#' + el).val('1');
    }
    else if (val == '1' && $(id).html() == 'NG' || val == '' && $(id).html() == 'NG')
    {
        $('.btn-' + el).removeClass('btn-success');
        $('.btn-' + el).removeClass('btn-danger');
        $('.btn-' + el).addClass('btn-dflt');

        $(id).addClass('btn-danger');
        $(id).removeClass('btn-dflt');
        $('#' + el).val('0');
    }
}

function calculateBalance()
{
    cashier_etoll_amount_start = $('#cashier_etoll_amount_start').autoNumeric('get');
    cashier_etoll_amount_end = $('#cashier_etoll_amount_end').autoNumeric('get');

    cashier_etoll_amount_start = (cashier_etoll_amount_start == '') ? 0 : cashier_etoll_amount_start
    cashier_etoll_amount_end = (cashier_etoll_amount_end == '') ? 0 : cashier_etoll_amount_end;

    cashier_etoll_balance = parseFloat(cashier_etoll_amount_start) - parseFloat(cashier_etoll_amount_end);

    $('#cashier_etoll_balance').html(cashier_etoll_balance);
    $('#cashier_etoll_balance').autoNumeric('set', cashier_etoll_balance);
}

function balanceNum()
{
    // $('#cashier_etoll_balance').autoNumeric('set', cashier_etoll_balance);
    // $('#cashier_etoll_balance').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});
}

function doComplete()
{
    cashier_fuel_actual = $('#cashier_fuel_actual').autoNumeric('get');
    cashier_fuel_remark = $('#cashier_fuel_remark').val();
    cashier_etoll_cardno = $('#cashier_etoll_cardno').val();
    cashier_etoll_amount_start = $('#cashier_etoll_amount_start').autoNumeric('get');
    cashier_etoll_balance = $('#cashier_etoll_balance').autoNumeric('get');
    cashier_etoll_amount_end = $('#cashier_etoll_amount_end').autoNumeric('get');
    cashier_etoll_remark = $('#cashier_etoll_remark').val();
    cashier_others_actual = $('#cashier_others_actual').autoNumeric('get');
    cashier_others_remark = $('#cashier_others_remark').val();

    // if (cashier_etoll_cardno != '')
    // {
    //     if (cashier_etoll_amount_start != '')
    //     {
            $('#myModal2').modal('show');
    //     }
    //     else
    //     {
    //         toastr["error"]("Saldo Awal belum diisi");
    //     }
    // }
    // else
    // {
    //     toastr["error"]("No.Kartu E-Toll belum diisi");
    // }

}

function doRemark(r)
{
    var label = '';
    if (r == 'cashier_fuel_remark')
    {
        label = 'Bensin';
    }
    else if (r == 'cashier_etoll_remark')
    {
        label = 'EToll';
    }
    else if (r == 'cashier_others_remark')
    {
        label = 'Others';
    }
    $('#myModalLabel').html('Remark ' + label);

    $('#myModal').modal('show');
    $('#textremark').val($('#' + r).val());
    $('#editremark').val(r);
}

function onSaveRemark()
{
    var r = $('#editremark').val();
    $('#' + r).val($('#textremark').val());
    $('#editremark').val('');
    $('#textremark').val('');
    $('#myModal').modal('hide');
}

// function onUpdateCashier()
// {
//     $('#btnOnComplete').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
//     $('#btnOnComplete').attr('disabled', 'disabled');	

//     $.ajax({
//         type: "POST",
//         url: SITE_URL + "rollcall/saveRc21",
//         dataType: 'json',
//         data: {
//             rollcall_id: $('#rollcall_id').val(),
//             // cashier_fuel_actual: cashier_fuel_actual,
//             // cashier_fuel_remark: cashier_fuel_remark,
//             cashier_etoll_cardno: cashier_etoll_cardno,
//             cashier_etoll_amount_start: cashier_etoll_amount_start,
//             // cashier_etoll_balance: cashier_etoll_balance,
//             // cashier_etoll_amount_end: cashier_etoll_amount_end,
//             // cashier_etoll_remark: cashier_etoll_remark,
//             // cashier_others_actual: cashier_others_actual,
//             // cashier_others_remark: cashier_others_remark
//         },		
//         success: function (data) {

//             if (data.success)
//             {
//                 toastr["success"](data.message);
//                 $('#myModal2').modal('hide');
                
//                 window.open(SITE_URL + 'rollcall/print/' + $('#rollcall_id').val());
//                 // window.location = SITE_URL + 'rollcall';
//             }
//             else
//             {
//                 toastr["error"](data.message);
//             }

//             $('#btnOnComplete').html('Complete');
//             $('#btnOnComplete').removeAttr('disabled');
            
//         },
//         error: function (jqXHR, textStatus, errorThrown)
//         {										
//             $('#btnOnComplete').html('Complete');
//             $('#btnOnComplete').removeAttr('disabled');

//             toastr["error"]("Error when processing Data");
//         }
//     });
// }

function onUpdateCashierArrival()
{
    $('#btnOnComplete').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    $('#btnOnComplete').attr('disabled', 'disabled');	

    $.ajax({
        type: "POST",
        url: SITE_URL + "rollcall/saveRc22",
        dataType: 'json',
        data: {
            rollcall_id: $('#rollcall_id').val(),
            cashier_fuel_actual: cashier_fuel_actual,
            cashier_fuel_remark: cashier_fuel_remark,
            // cashier_etoll_cardno: cashier_etoll_cardno,
            // cashier_etoll_amount_start: cashier_etoll_amount_start,
            cashier_etoll_balance: cashier_etoll_balance,
            cashier_etoll_amount_end: cashier_etoll_amount_end,
            cashier_etoll_remark: cashier_etoll_remark,
            cashier_others_actual: cashier_others_actual,
            cashier_others_remark: cashier_others_remark
        },		
        success: function (data) {

            if (data.success)
            {
                toastr["success"](data.message);
                $('#myModal2').modal('hide');
                
                // window.open(SITE_URL + 'rollcall/print/' + $('#rollcall_id').val());
                window.location = SITE_URL + 'rollcall';
            }
            else
            {
                toastr["error"](data.message);
            }

            $('#btnOnComplete').html('Complete');
            $('#btnOnComplete').removeAttr('disabled');
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {										
            $('#btnOnComplete').html('Complete');
            $('#btnOnComplete').removeAttr('disabled');

            toastr["error"]("Error when processing Data");
        }
    });
}

function onComplete()
{
    $('#btnOnComplete').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    $('#btnOnComplete').attr('disabled', 'disabled');	

    $.ajax({
        type: "POST",
        url: SITE_URL + "rollcall/saveRc2",
        dataType: 'json',
        data: {
            rollcall_id: $('#rollcall_id').val(),
            // cashier_fuel_actual: cashier_fuel_actual,
            // cashier_fuel_remark: cashier_fuel_remark,
            cashier_etoll_cardno: cashier_etoll_cardno,
            cashier_etoll_amount_start: cashier_etoll_amount_start,
            // cashier_etoll_balance: cashier_etoll_balance,
            // cashier_etoll_amount_end: cashier_etoll_amount_end,
            // cashier_etoll_remark: cashier_etoll_remark,
            // cashier_others_actual: cashier_others_actual,
            // cashier_others_remark: cashier_others_remark
        },		
        success: function (data) {

            if (data.success)
            {
                toastr["success"](data.message);
                $('#myModal2').modal('hide');
                

                window.open(SITE_URL + 'rollcall/print/' + $('#rollcall_id').val());
                window.location = SITE_URL + 'rollcall';
            }
            else
            {
                toastr["error"](data.message);
            }

            $('#btnOnComplete').html('Complete');
            $('#btnOnComplete').removeAttr('disabled');
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {										
            $('#btnOnComplete').html('Complete');
            $('#btnOnComplete').removeAttr('disabled');

            toastr["error"]("Error when processing Data");
        }
    });
}

function doCompleteRoute()
{
    controller_apd_departure = $('#controller_apd_departure').val();
    // controller_apd_arrival = $('#controller_apd_arrival').val();
    controller_basket_departure = $('#controller_basket_departure').val();
    // controller_basket_arrival = $('#controller_basket_arrival').val();
    controller_key_departure = $('#controller_key_departure').val();
    // controller_key_arrival = $('#controller_key_arrival').val();

    if (
        controller_apd_departure == '1' &&
        // controller_apd_arrival != '' &&
        controller_basket_departure == '1' &&
        // controller_basket_arrival != '' &&
        controller_key_departure == '1'
        // controller_key_arrival != ''
    )
    {
        $('#myModal3').modal('show');
    }
    else
    {
        toastr["error"]("APD/Badge, Basket, dan Kunci (DEPARTURE) harus OK");
    }
}

function doCompleteRouteArrival()
{
    // controller_apd_departure = $('#controller_apd_departure').val();
    controller_apd_arrival = $('#controller_apd_arrival').val();
    // controller_basket_departure = $('#controller_basket_departure').val();
    controller_basket_arrival = $('#controller_basket_arrival').val();
    // controller_key_departure = $('#controller_key_departure').val();
    controller_key_arrival = $('#controller_key_arrival').val();

    if (
        // controller_apd_departure == '1' &&
        controller_apd_arrival == '1' &&
        // controller_basket_departure == '1' &&
        controller_basket_arrival == '1' &&
        // controller_key_departure == '1'
        controller_key_arrival == '1'
    )
    {
        $('#myModal3').modal('show');
    }
    else
    {
        toastr["error"]("APD/Badge, Basket, dan Kunci (ARRIVAL) harus OK");
    }
}

function onCompleteRoute()
{
    $('#btnOnCompleteRoute').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    $('#btnOnCompleteRoute').attr('disabled', 'disabled');	

    $.ajax({
        type: "POST",
        url: SITE_URL + "rollcall/saveRc3",
        dataType: 'json',
        data: {
            rollcall_id: $('#rollcall_id').val(),
            controller_apd_departure: controller_apd_departure,
            // controller_apd_arrival: controller_apd_arrival,
            controller_basket_departure: controller_basket_departure,
            // controller_basket_arrival: controller_basket_arrival,
            controller_key_departure: controller_key_departure,
            // controller_key_arrival: controller_key_arrival
        },		
        success: function (data) {

            if (data.success)
            {
                toastr["success"](data.message);
                $('#myModal3').modal('hide');
                window.location = SITE_URL + 'rollcall';
            }
            else
            {
                toastr["error"](data.message);
            }

            $('#btnOnCompleteRoute').html('Complete');
            $('#btnOnCompleteRoute').removeAttr('disabled');
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {										
            $('#btnOnCompleteRoute').html('Complete');
            $('#btnOnCompleteRoute').removeAttr('disabled');

            toastr["error"]("Error when processing Data");
        }
    });
}

function onFinishRoute()
{
    $('#btnOnCompleteRoute').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    $('#btnOnCompleteRoute').attr('disabled', 'disabled');	

    $.ajax({
        type: "POST",
        url: SITE_URL + "rollcall/saveRc31",
        dataType: 'json',
        data: {
            rollcall_id: $('#rollcall_id').val(),
            // controller_apd_departure: controller_apd_departure,
            controller_apd_arrival: controller_apd_arrival,
            // controller_basket_departure: controller_basket_departure,
            controller_basket_arrival: controller_basket_arrival,
            // controller_key_departure: controller_key_departure,
            controller_key_arrival: controller_key_arrival
        },		
        success: function (data) {

            if (data.success)
            {
                toastr["success"](data.message);
                $('#myModal3').modal('hide');
                location.reload();
            }
            else
            {
                toastr["error"](data.message);
            }

            $('#btnOnCompleteRoute').html('Complete');
            $('#btnOnCompleteRoute').removeAttr('disabled');
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {										
            $('#btnOnCompleteRoute').html('Complete');
            $('#btnOnCompleteRoute').removeAttr('disabled');

            toastr["error"]("Error when processing Data");
        }
    });
}

function parseDate(str) {
    var mdy = str.split('/');
    return new Date(mdy[2], mdy[1]-1, mdy[0]);
}

function datediff(first, second) {
    // Take the difference between the dates and divide by milliseconds per day.
    // Round to nearest whole number to deal with DST.
    return Math.round((second-first)/(1000*60*60*24));
}