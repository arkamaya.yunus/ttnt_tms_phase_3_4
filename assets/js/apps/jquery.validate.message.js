jQuery.extend(jQuery.validator.messages, {
    required: "Field is required.",
    remote: "Please fix this field.",
    email: "Invalid Email",
    url: "Invalid URL",
    date: "Invalid Date",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter only number",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Confirmation not equal",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Mohon diisi minimal {0} karakter."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});