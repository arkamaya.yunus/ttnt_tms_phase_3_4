
$(document).ready(function(){
    $('.select2').select2();
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy',
    });

    // update 29-Juni-2020 - fix select2 on firefox.
    $('#manual_driver_cd').select2({
        dropdownParent: $("#myModal")
    });
    manualRoute();

	//initialize datatable
    oTable = $('#datatable_rollcall').DataTable(
	{
		fixedHeader: true,
        ordering : true,
        order: [[ 7, "desc" ],[ 9, "desc" ]],
		lengthMenu : [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
		paging : true,
		serverSide : true,
		ajax : 
		{
			url: SITE_URL + "rollcall/gets",
			type: "POST",
			data : function (d)
			{
				d.departure_dt_s = $('#departure_dt_s').val();
                d.departure_dt_e = $('#departure_dt_e').val();
                d.business = $('#business').val();//select2('val');
                d.customer = $('#customer').val();//.select2('val');
                d.route = $('#route').val();
                d.driver_cd = $('#driver_cd').val();//.select2('val');
                d.vehicle_cd = $('#vehicle_cd').val();//.select2('val');
                d.status = $('#status').select2('val');
			}
		},
		pagingType : 'full_numbers',
		processing : true,
		columnDefs : 
		[
			{ targets : 0, name : 'rollcall_id', className: 'text-center'}, 
			{ targets : 1, name : 'voucher'}, 
			{ targets : 2, name : 'status'}, 
			{ targets : 3, name : 'driver_name'},
			{ targets : 4, name : 'vehicle_cd'},
			{ targets : 5, name : 'route'},
			{ targets : 6, name : 'cycle'},
			{ targets : 7, name : 'departure_dt'}, 
            { targets : 8, name : 'departure_plan'},
            { targets : 9, name : 'departure_actual'},
            { targets : 10, name : 'arrival_plan'},
            { targets : 11, name : 'arrival_actual'},
            { targets : 12, name : 'business'},
            { targets : 13, name : 'customer'},
            { targets : 14, name : 'clock_in'},
			{ targets : 15, name : 'clock_out'},
			{ targets : 16, name : 'driver_nik'},
			{ targets : 17, name : 'driver_sim'},
			{ targets : 18, name : 'driver_sio'},
			{ targets : 19, name : 'driver_blood_pres'},            
            { targets : 20, name : 'driver_body_temp'},
            { targets : 21, name : 'driver_sleep_time'},
            { targets : 22, name : 'cashier_fuel_plan'},
            { targets : 23, name : 'cashier_fuel_actual'},
            { targets : 24, name : 'cashier_fuel_remark'},
            { targets : 25, name : 'cashier_etoll_plan'},
            { targets : 26, name : 'cashier_etoll_cardno'},
            { targets : 27, name : 'cashier_etoll_amount_start'},
            { targets : 28, name : 'cashier_etoll_amount_end'},
            { targets : 29, name : 'cashier_etoll_balance'},
            { targets : 30, name : 'cashier_etoll_remark'},
            { targets : 31, name : 'cashier_others_plan'},
            { targets : 32, name : 'cashier_others_actual'},
            { targets : 33, name : 'cashier_others_remark'},
            { targets : 34, name : 'controller_apd_departure'},
            { targets : 35, name : 'controller_basket_departure'},
            { targets : 36, name : 'controller_key_departure'},
            { targets : 37, name : 'controller_apd_arrival'},
            { targets : 38, name : 'controller_basket_arrival'},
            { targets : 39, name : 'controller_key_arrival'},            
            // { targets : 38, name : 'created_by'},
            // { targets : 39, name : 'created_dt'},
            // { targets : 40, name : 'changed_by'},
            // { targets : 41, name : 'changed_dt'},
            

        ],
        scrollX : true,
    });
    
    $('#route').autocomplete({		
		serviceUrl: SITE_URL + 'rollcall/get_route/' + $('#departure_dt_s').val().split('/')[2] + '-' + $('#departure_dt_s').val().split('/')[1] + '-' + $('#departure_dt_s').val().split('/')[0] + '/' + $('#departure_dt_e').val().split('/')[2] + '-' + $('#departure_dt_e').val().split('/')[1] + '-' + $('#departure_dt_e').val().split('/')[0],
	});
});

function changeDepartureDt()
{
    $('#route').autocomplete({		
		serviceUrl: SITE_URL + 'rollcall/get_route/' + $('#departure_dt_s').val().split('/')[2] + '-' + $('#departure_dt_s').val().split('/')[1] + '-' + $('#departure_dt_s').val().split('/')[0] + '/' + $('#departure_dt_e').val().split('/')[2] + '-' + $('#departure_dt_e').val().split('/')[1] + '-' + $('#departure_dt_e').val().split('/')[0],
	});
}

function doFilter()
{
    oTable.ajax.reload();	
}

function doReset()
{
    $('#departure_dt_s').val($('#dt_s').val());
    $('#departure_dt_e').val($('#dt_e').val());
    $('#business').select2('val', 'all');
    $('#customer').select2('val', 'all');
    $('#route').val('');
    $('#driver_cd').select2('val', 'all');
    $('#vehicle_cd').select2('val', 'all');
    oTable.ajax.reload();
}


function doDownload()
{
    var departure_dt_s = $('#departure_dt_s').val();
    var departure_dt_e = $('#departure_dt_e').val();
    var status = $('#status').select2('val');

    ts= departure_dt_s.split('/');
    ds = ts[2] + '-' + ts[1] + '-' + ts[0];

    te= departure_dt_e.split('/');
    de = te[2] + '-' + te[1] + '-' + te[0];
	
	window.location.href = SITE_URL + 'rollcall/download/' + ds + '/' + de + '/' + status;
}

function manualRoute()
{
    var manual_driver_cd = $('#manual_driver_cd').select2('val');
    $('.mdrv').hide();
    $('.mdrv' + manual_driver_cd).show();

}

function startManual(xm, driver_cd, route, cycle, lp_cd)
{
    // cek dulu apakah sudah ada rollcall nya ?
    $('#btnStartManual' + xm).html('Memeriksa rollcall... <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    $('#btnStartManual' + xm).attr('disabled', 'disabled');	

    $.ajax({
        type: "POST",
        url: SITE_URL + "rollcall/cekrcmanual",
        dataType: 'json',
        data: {
            driver_cd: driver_cd,
            route: route,
            cycle: cycle
        },		
        success: function (data) {

            if (data.success)
            {
                toastr["success"](data.message);
                window.location = SITE_URL + 'rollcall/create_manual/' + driver_cd + '/' + route + '/' + cycle + '/' + lp_cd
            }
            else
            {
                toastr["error"](data.message);
            }
            $('#myModal').modal('hide');            

            $('#btnStartManual' + xm).html('Pilih');
            $('#btnStartManual' + xm).removeAttr('disabled');
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {										
            $('#btnStartManual' + xm).html('Pilih');
            $('#btnStartManual' + xm).removeAttr('disabled');

            toastr["error"]("Error when processing Data");
        }
    });
}

function openManualRollcall()
{
    $('#manual_rollcall_username').val('');
    $('#manual_rollcall_password').val('');
    $('.rcm').hide();
    $('.rcm-2').show();
    $('#myModal').modal('show');
}

function accessManualRollcall()
{
    // cek apakah Supervisor atau Administrator
    $('#btnOpenRollcallManual').html('Mohon tunggu... <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    $('#btnOpenRollcallManual').attr('disabled', 'disabled');	

    $.ajax({
        type: "POST",
        url: SITE_URL + "rollcall/accessmanual",
        dataType: 'json',
        data: {
            manual_rollcall_username: $('#manual_rollcall_username').val(),
            manual_rollcall_password: $('#manual_rollcall_password').val()
        },		
        success: function (data) {

            if (data.success)
            {
                toastr["success"](data.message);
                $('.rcm').hide();
                $('.rcm-1').show();
            }
            else
            {
                toastr["error"](data.message);
            }

            $('#btnOpenRollcallManual').html('Buka Akses');
            $('#btnOpenRollcallManual').removeAttr('disabled');
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {										
            $('#btnOpenRollcallManual').html('Buka Akses');
            $('#btnOpenRollcallManual').removeAttr('disabled');

            toastr["error"]("Error when processing Data");
        }
    });
}