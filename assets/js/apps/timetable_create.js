var oTable;

$(document).ready(function(){    
    
	$('.select2').select2({placeholder: 'Select'});
	$('.datepickerm').datepicker(
		{
			multidate: true,
			// startDate: '-0d',
			autoclose: false,
			clearBtn: true,
			multidateSeparator: ",",
			todayHighlight: false,
		}
	).on('changeDate', function(e){

		var timetable_dts = '';
		var i = 1;
		e.dates.forEach(function(date){

			timetable_dts += formatDate(date);
			if (i < e.dates.length)
			{
				timetable_dts += ',';
			}
			i++;
		});
		$('#timetable_dts').val(timetable_dts);				
	  });;
	
	showStep(0);
	init_table_temp();
	
});

function showStep(step)
{
	if (step == 0)
	{
		$('#btn_restart').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('#btn_restart2').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('#btn_restart').attr('disabled', 'disabled');
		$('#btn_restart2').attr('disabled', 'disabled');

		$.ajax({
			type: "POST",
			url: SITE_URL + "timetable/deletetemp",
			dataType: 'json',
			contentType: false,
			processData: false,
			success: function (data) {

				$('#btn_restart').html('Restart');
				$('#btn_restart2').html('Restart');
				$('#btn_restart').removeAttr('disabled');
				$('#btn_restart2').removeAttr('disabled');

				$('.step').hide();
				$('.step-' + step).show();

			},
			error: function (jqXHR, textStatus, errorThrown)
			{										
				$('#btn_restart').html('Restart');
				$('#btn_restart2').html('Restart');
				$('#btn_restart').removeAttr('disabled');
				$('#btn_restart2').removeAttr('disabled');

				toastr["error"]("Error when empty the data: " + textStatus);

				$('.step').hide();
				$('.step-' + step).show();
			}
		});

	}
	else
	{
		$('.step').hide();
		$('.step-' + step).show();
	}	
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

function doSubmit()
{
	if ($('#frm').valid()) 
	{		
		if ($('#timetable_dts').val() != '')
		{
			var formData = new FormData($('#frm')[0]);		
			$('#btn_submit').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
			$('#btn_submit').attr('disabled', 'disabled');	

			$.ajax({
				type: "POST",
				url: SITE_URL + "timetable/do_upload",
				dataType: 'json',
				data: formData,
				contentType: false,
				processData: false,
				success: function (data) {
	
					$('#btn_submit').html('Upload');
					$('#btn_submit').removeAttr('disabled');
	
					if (!data.success)
					{
						toastr['error'](data.message);
					}
					else
					{
						toastr['success'](data.message);
					}

					showStep(data.proceed_step);
					if (data.proceed_step != 0)
					{
						$('#step-1-business').html($('#business').select2('data')[0].text);
						$('#step-1-customer').html($('#customer').select2('data')[0].text);
						$('#step-1-customer_lp_cd').html($('#customer_lp_cd').select2('data')[0].text);
						$('#step-1-timetable_dts').html(data.data.timetable_dts);						
					}

					$("#process_id").val(data.process_id);
					init_table_temp();
				},
				error: function (jqXHR, textStatus, errorThrown)
				{										
					$('#btn_submit').html('Upload');
					$('#btn_submit').removeAttr('disabled');
	
					toastr["error"]("Error when processing Data");
				}
			});
		}
		else
		{
			toastr["error"]("Please define Planning Date(s)");
		}		
	}
	else
	{
		toastr["error"]("Please input required fields")
	}
}

function checkextension() {
    var file = document.querySelector("#file_attach");
    if (/\.(xlsx)$/i.test(file.files[0].name) === false) {
        // alert("Upload file dengan format .xlsx!");
		toastr["error"]("Error. Make sure file template is Excel (xlsx)");
        $('#file_nm').val('');
        $('#file_attach').val('');
    } else {
        $('#file_nm').val(file.files[0].name);
    }
}

function init_table_temp() {
    if (!$.fn.DataTable.isDataTable('#datatable')) {
        loadTable();
    } else {
        oTable.ajax.reload();
    }
} 

function loadTable(processId) {
    //initialize datatable
    oTable = $('#datatable').DataTable(
	{
		fixedHeader: true,
		ordering : true,
		order: [[ 0, "asc" ], [ 2, "asc" ], [ 3, "asc" ], [ 15, "asc" ]],
		lengthMenu : [[50, 100, 200, -1], [50, 100, 200, 'All']],
		paging : true,
		serverSide : true,
		ajax : 
		{
			url: SITE_URL + "timetable/get_temp_timetable",
			type: "POST",
			data : function (d)
			{
				d.process_id = $("#process_id").val()
			}
		},
		pagingType : 'full_numbers',
		processing : true,
		columnDefs : 
		[
			{ targets : 0, visible: false, name: 'is_valid'},
			{ targets : 1, orderable: false, name: 'error_detail'},
			{ targets : 2, name : 'route'},
			{ targets : 3, name : 'cycle'},
			{ targets : 15, name : 'arrival_plan'},
		],
		scrollX : true,
		createdRow: function (row, data, dataIndex) {
			if (data[0] == '0') {
				$(row).addClass('bg-red');
			}
		},
	});
}

function doProcees()
{
	$('#btn_process').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
	$('#btn_process').attr('disabled', 'disabled');	

	$.ajax({
		type: "POST",
		url: SITE_URL + "timetable/do_process",
		dataType: 'json',
		data: {
			process_id: $('#process_id').val()
		},		
		success: function (data) {

			if (data.success)
			{
				window.location = SITE_URL + 'timetable';
			}
			else
			{
				toastr["error"](data.message);
			}

			$('#btn_process').html('Process');
			$('#btn_process').removeAttr('disabled');
			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{										
			$('#btn_process').html('Process');
			$('#btn_process').removeAttr('disabled');

			toastr["error"]("Error when processing Data");
		}
	});
}