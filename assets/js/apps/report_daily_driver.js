$(document).ready(function(){
    $('.select2').select2();
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy',
    });

    if(selected_customer != ''){
      $('#customer').val(selected_customer).trigger('change');
    }
    if(selected_customer != ''){
      $('#departure_area').val(selected_departure_area).trigger('change');
    }
    // alert (selected_customer);
});

function doDownload()
{
    var timetable_dt = $('#timetable_dt').val();
    var timetable_df = $('#timetable_df').val();
    var customer = $('#customer').val();
    var departure_area = $('#departure_area').val();

    tx = timetable_dt.split('/');
    dt = tx[2] + '-' + tx[1] + '-' + tx[0];

    fx = timetable_df.split('/');
    ft = fx[2] + '-' + fx[1] + '-' + fx[0];

	window.location.href = SITE_URL + 'report_daily_driver/download/' + dt + '/' + ft + '/' + customer + '/' + departure_area;
}
