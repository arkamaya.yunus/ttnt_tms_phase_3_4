$(document).ready(function(){
	$('.select2').select2();
	$('.period_dt').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy'	
    })
	
	oTable = $('#datatable').DataTable(
	{
		fixedHeader: true,
        ordering : false,
        dom : '<"top">rt<"bottom"lpi><"clear">',
        paging : false,
        language : {
            "processing" : "Memuat data. Silahkan tunggu...",
            "emptyTable" : "Belum ada transaksi.",
			'info'		: "Menampilkan _START_ - _END_ dari _TOTAL_ baris",
			'infoEmpty'	: "Menampilkan 0 sampai 0 dari 0 baris",
			'paginate': {
				first:      "Awal",
				last:       "Akhir",
				next:       "Maju",
				previous:   "Mundur"
			},
			'search': 'Cari',
			'lengthMenu' 	: "Per _MENU_ Baris",			
        }		
    });
});

function filter_gl()
{
	if ($('#form_general_ledger').valid()) 
	{
		$('#selected_accounts').val(JSON.stringify($('#accounts').select2('val')));
		
		$('#btn_filter').html('Mohon Tunggu  <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('#btn_filter').attr('disabled', 'disabled');
		$('#form_general_ledger').submit();
	}
}
