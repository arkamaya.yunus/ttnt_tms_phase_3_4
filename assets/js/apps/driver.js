
$(document).ready(function(){
	$('.select2').select2();

	//initialize datatable
    oTable = $('#datatable').DataTable(
	{
		fixedHeader: true,
        ordering : true,
        order: [[ 1, "asc" ]],
		lengthMenu : [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
		paging : true,
		serverSide : true,
		ajax : 
		{
			url: SITE_URL + "driver/gets",
			type: "POST",
			data : function (d)
			{
				d.logistic_partner = $('#logistic_partner').select2('val');
				d.active = $('#active').select2('val');
				d.business = $('#business').select2('val');
			}
		},
		pagingType : 'full_numbers',
		processing : true,
		columnDefs : 
		[
			{ targets : 0, name : 'photo', className: 'text-center'}, 
			{ targets : 1, name : 'driver_cd'}, 
			{ targets : 2, name : 'driver_name'}, 
			{ targets : 3, name : 'logistic_partner'}, 
            { targets : 4, name : 'jobdesc'},
            
            { targets : 5, name : 'smartphone'},
            { targets : 6, name : 'driving_license_val'},
            { targets : 7, name : 'driving_license_type'},

			{ targets : 8, name : 'forklift_license_val'},
			{ targets : 9, name : 'forklift_license_type'},
			{ targets : 10, name : 'join_dt'},
			{ targets : 11, name : 'length_of_working'},
			{ targets : 12, name : 'num_of_accident'},
			{ targets : 13, name : 'active'},
			{ targets : 14, name : 'resign_dt'},
			{ targets : 15, name : 'att_id'},
			{ targets : 16, name : 'dob'},
			{ targets : 17, name : 'business'},

        ],
        scrollX : true,
	});
});

function doFilter()
{
	// if ($('#form_journal').valid()) 
	// {
		oTable.ajax.reload();
	// }
}

function doDownload()
{
	var logistic_partner = $('#logistic_partner').select2('val');
	var active = $('#active').select2('val');
	var business = $('#business').select2('val');
	
	window.location.href = SITE_URL + 'driver/download/' + logistic_partner + '/' + active + '/' + business;
}

function doUpload()
{
	$('#myModal').modal('show');
}

function doUploadProcess()
{
	if ($('#frm').valid()) 
	{
		var formData = new FormData($('#frm')[0]);		
		$('#btn_upload_confirm').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('#btn_upload_confirm').attr('disabled', 'disabled');	

		$.ajax({
			type: "POST",
			url: SITE_URL + "driver/upload",
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			success: function (data) {

				$('#btn_upload_confirm').html('Upload');
				$('#btn_upload_confirm').removeAttr('disabled');

				if (!data.success)
				{
					toastr['error'](data.message);					
				}
				else
				{
					toastr['success'](data.message);
					doFilter();
					$('#myModal').modal('hide');
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{										
				$('#btn_upload_confirm').html('Upload');
				$('#btn_upload_confirm').removeAttr('disabled');

				toastr["error"]("Error when processing Data");
			}
		});
	}
	else
	{
		toastr["error"]("Please input required fields")
	}
}

function checkextension() {
    var file = document.querySelector("#file_attach");
    if (/\.(xlsx)$/i.test(file.files[0].name) === false) {
        // alert("Upload file dengan format .xlsx!");
		toastr["error"]("Error. Make sure file template is Excel (xlsx)");
        $('#file_nm').val('');
        $('#file_attach').val('');
    } else {
        $('#file_nm').val(file.files[0].name);
    }
}