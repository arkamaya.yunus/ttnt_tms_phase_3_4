var oTable;

$(document).ready(function()
{	
	//initialize datatable
    oTable = $('#datatable').DataTable(
	{
		fixedHeader: true,
        ordering : false,
        // lengthMenu : [[25, 50, 100, 200, -1], [25, 50, 100, 200, 'All']],
        paging : false,
        serverSide : true,
		ajax : 
		{
            url: SITE_URL + "company/get_companies",
            type: "POST",
            data : function (d){                
            }
        },
        dom : '<"top">rt<"bottom"lpi><"clear">',
        // pagingType : 'full_numbers',
        processing : true,
        columnDefs : 
		[
			{ targets : 0, name : 'company_name'}, 
			{ targets : 1, name : 'company_status'}, 
			{ targets : 2, name : 'option'}
        ],
        language : {
            "processing" : "Loading. Please Wait...",
            "emptyTable" : "No Data.",
			'info'		: "Showing _START_ - _END_ from _TOTAL_ row",
			'infoEmpty'	: "Showing 0 to 0 dari 0 row",
			'paginate': {
				first:      "Awal",
				last:       "Akhir",
				next:       "Maju",
				previous:   "Mundur"
			},
			'search': 'Search',
			'lengthMenu' 	: "Per _MENU_ Row",			
        }		
    });        
});


























