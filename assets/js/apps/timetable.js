
$(document).ready(function(){
    $('.select2').select2();
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy'	
    });

	//initialize datatable
    oTable = $('#datatable').DataTable(
	{
		fixedHeader: true,
        ordering : true,
        order: [[ 1, "asc" ], [ 2, "asc" ], [ 3, "asc" ], [ 4, "desc" ]],
		lengthMenu : [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
		paging : true,
		serverSide : true,
		ajax : 
		{
			url: SITE_URL + "timetable/gets",
			type: "POST",
			data : function (d)
			{
                d.customer_lp_cd = $('#customer_lp_cd').select2('val');
                d.logistic_partner = 'all';
                d.timetable_dt_s = $('#timetable_dt_s').val();
				d.timetable_dt_e = $('#timetable_dt_e').val();
				d.business = $('#business').select2('val');
				d.customer = $('#customer').select2('val');
			}
		},
		pagingType : 'full_numbers',
		processing : true,
		columnDefs : 
		[
			{ targets : 0, name : 'timetable_id', visible: false}, 
			{ targets : 1, name : 'business'},
			{ targets : 2, name : 'customer'},
			{ targets : 3, name : 'customer_lp_cd'},
            { targets : 4, name : 'timetable_dt'},                        
			{ targets : 5, name : 'created_dt'},
			{ targets : 6, name : 'option', orderable: false, width: 50},
			
        ],
        scrollX : true,
	});

	$('#datatable tbody').on('click', 'tr', function () {
        var data = oTable.row( this ).data();
       	window.location = SITE_URL + 'timetable/id/' + data[0];
    } );
});

function doFilter()
{
	// if ($('#form_journal').valid()) 
	// {
		oTable.ajax.reload();
	// }
}