$(document).ready(function(){    
    $('.button-menu-mobile').trigger('click');
    $('.select2').select2({dropdownAutoWidth : true});
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy'	
    });

    getSummary();

    // setupDonut();
});

var trace;

function getSummary()
{
    $('#pLoad').show();    

    $.ajax({
        type: "POST",
        url: SITE_URL + "dashboard_summary/getSummary",
        dataType: 'json',
        data: {
            customer_lp_cd: $('#customer_lp_cd').select2('val'),
            timetable_dt: $('#timetable_dt').val()
        },		
        success: function (data) {
                                    
            var rowsTruck = '';
            var rowsDriver = '';            

            if (data.data_truck.length > 0)
            {
                var no = 1;
                for(i=0; i<data.data_truck.length; i++)
                {
                    rowsTruck += '<tr>';
                    rowsTruck += '   <td>' + no + '</td>';
                    rowsTruck += '   <td>' + data.data_truck[i].vehicle_cd + '</td>';
                    rowsTruck += '   <td>' + data.data_truck[i].vehicle_number + '</td>';
                    rowsTruck += '   <td>' + data.data_truck[i].vehicle_owner + '</td>';
                    rowsTruck += '</tr>';
                    no++;
                }
            }

            if (data.data_driver.length > 0)
            {
                var no = 1;
                for(i=0; i<data.data_driver.length; i++)
                {
                    rowsDriver += '<tr>';
                    rowsDriver += '   <td>' + no + '</td>';
                    rowsDriver += '   <td>' + data.data_driver[i].driver_cd + '</td>';
                    rowsDriver += '   <td>' + data.data_driver[i].driver_name + '</td>';
                    rowsDriver += '   <td>' + data.data_driver[i].logistic_partner + '</td>';
                    rowsDriver += '</tr>';
                    no++;
                }
            }                        

            $("#dtTruck").find("tr:gt(0)").remove();
            $('#dtTruck').append(rowsTruck);

            $("#dtDriver").find("tr:gt(0)").remove();
            $('#dtDriver').append(rowsDriver);

            $('#total_planning').html(data.total_planning);
            $('#total_arrival').html(data.total_arrival);
            $('#total_not_arrival').html(parseInt(data.total_planning) - parseInt(data.total_arrival));
            $('#total_truck').html(data.total_truck);
            $('#total_driver').html(data.total_driver);
            $('#total_truck_active').html(data.total_truck_active);
            $('#total_driver_active').html(data.total_driver_active);

            if (data.total_business_truck.length > 0)
            {
                for (i=0; i<data.total_business_truck.length; i++)
                {
                    $('#total_truck_' + data.total_business_truck[i].business).html(data.total_business_truck[i].total_truck);
                    $('#total_driver_' + data.total_business_driver[i].business).html(data.total_business_driver[i].total_driver);
                    $('#total_cycle_' + data.total_business_cycle[i].business).html(data.total_business_cycle[i].total_cycle);
                    $('#total_delay_' + data.total_delay[i].business).html(data.total_delay[i].total_delay);

                    // bg danger
                    if (data.total_delay[i].total_delay > 0)
                    {
                        $('#delay-bg-' + data.total_delay[i].business).addClass('bg-danger');
                    }
                }
            }
            else
            {
                // reset tampilan
                $('#total_truck_MILKRUN').html(0);
                $('#total_driver_MILKRUN').html(0);
                $('#total_cycle_MILKRUN').html(0);
                $('#total_delay_MILKRUN').html(0);
                $('#delay-bg-MILKRUN').removeClass('bg-danger');

                $('#total_truck_REGULER').html(0);
                $('#total_driver_REGULER').html(0);
                $('#total_cycle_REGULER').html(0);
                $('#total_delay_REGULER').html(0);
                $('#delay-bg-REGULER').removeClass('bg-danger');
            }                        

            $('#pLoad').hide();

            setTimeout(function() { 
                getSummary();
            }, 5000);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {										
            // toastr["error"]("Error when getting Data");
            $('#pLoad').hide();

            setTimeout(function() { 
                getSummary();
            }, 5000);
        }
    });
}

function setupDonut(dataDonut)
{
    $('#business-chart').empty();
    Morris.Donut({
        element: 'business-chart',
        data: dataDonut,
        // formatter: function (x) { return x + "%"}
      }).on('click', function(i, row){
        console.log(i, row);
      });
}

function setupBar(dataBar, barColors)
{
    $('#customerlp-chart').empty();
    Morris.Bar({
        element: 'customerlp-chart',
        data: dataBar,
        xkey: 'customer_lp_cd',
        ykeys: ['plan', 'done', 'notyet'],
        labels: ['Plan', 'Done', 'Not Yet'],
        barColors: barColors,
        labelTop: true,
        hideHover: true
      }).on('click', function(i, row){
        console.log(i, row);
      });
}

function viewOperation()
{
    var t = $('#timetable_dt').val();
    var ts = t.split('/');
    window.location = SITE_URL + 'operation/id/' + ts[2] + '-' + ts[1] + '-' + ts[0];
}

function viewOperationNYA()
{
    var t = $('#timetable_dt').val();
    var ts = t.split('/');
    window.location = SITE_URL + 'operation/id/' + ts[2] + '-' + ts[1] + '-' + ts[0] + '/nya';
}