
$(document).ready(function(){
    // set focus
    $('.select2').select2();
    // $(".select2").select2({
    //   placeholder: "Please select",
    //   // allowClear: true
    // });
    // num only
    $(".num-only").inputFilter(function(value) {
    return /^-?\d*$/.test(value); });
    // ip only
});

function create()
{
    $('#ip').val('');
    $('#mesin').val('');
    $('#app_id').val('');
    $('.pk').show();
    $('#act').val('create');
    $('#myModalLabel').html('Create');
    $('#myModal').modal('show');
    $('#mesin').select2('val', 'in');
    $('#ip').focus();
}

function edit(e, n, g)
{
    $('#ip').val(e);
    // $('#mesin').val(n);
    $('#mesin').select2('val', n);
    $('#app_id').val(g);
    $('#act').val('edit');
    $('.pk').hide();

    $('#myModalLabel').html('Edit '+ e);
    $('#myModal').modal('show');
    $('#mesin').focus();
}

function doSave() {
    if ($('#frm').valid())
	   {
       if($('#act').val() == 'create'){
         //jika create
          check_duplicate = checkDuplicateIp($('#ip').val());

          if(check_duplicate) {
            $('#btn_save_user').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            $('#btn_save_user').attr('disabled', 'disabled');
            $('#frm').submit();
          }else {
            alert('duplicate IP found! please use another IP Address');
          }

        }else {
          //jika edit
          $('#btn_save_user').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
          $('#btn_save_user').attr('disabled', 'disabled');
          $('#frm').submit();
        }

    }
    else
	{
		toastr["error"]("Please input required fields")
	}
}

function confirmDelete(id)
{
    $('#machine_delete').val(id);
    $('#myModal2').modal('show');
}

function doDelete()
{
	$('#btn_delete_confirm').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
	$('#btn_delete_confirm').attr('disabled', 'disabled');

	$('#frm2').submit();
}

function checkDuplicateIp(ip){
  var bool;
  $.ajax({
   url: SITE_URL + 'settings/checkDuplicateIp/' + ip,
   method: 'post',
   async: false,
   // data: {ip: ip},
   dataType: 'json',
   success: function(response){
     // alert(response);
     if(response == 1){
       bool = false;
     }else {
       bool = true;
     }
   }
  });
  return trueOrFalse(bool);
}

function trueOrFalse(bool){
        return bool;
}

// Restricts input for the set of matched elements to the given inputFilter function.
(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
}(jQuery));
