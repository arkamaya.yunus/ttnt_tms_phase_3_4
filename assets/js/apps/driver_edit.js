$(document).ready(function(){    
    
    $('#logistic_partner').autocomplete({
		serviceUrl: SITE_URL + 'driver/get_logistic_partners',
	});

	$('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy'	
	});
	
	$('.select2').select2();
});

function doSubmit()
{
	if ($('#frm').valid()) 
	{		
		$('#btn_submit').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('.sbmt_btn').attr('disabled', 'disabled');
		
		$('#frm').submit();
	}
	else
	{
		toastr["error"]("Please input required fields")
	}
}