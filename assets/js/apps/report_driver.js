$(document).ready(function(){
    // $('.select2').select2();
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy',
    });

    $('.s2-cycle').hide();

    //initialize datatable
      oTable = $('#datatable_report_driver').DataTable(
  	{
  		fixedHeader: true,
          ordering : true,
          order: [[ 7, "desc" ]],
  		lengthMenu : [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
  		paging : true,
  		serverSide : true,
  		ajax :
  		{
  			url: SITE_URL + "report_driver/gets",
  			type: "POST",
  			data : function (d)
  			{
  				d.departure_dt_s = $('#departure_dt_s').val();
          d.departure_dt_e = $('#departure_dt_e').val();
  			}
  		},
  		pagingType : 'full_numbers',
  		processing : true,
  		columnDefs :
  		[
  			{ targets : 0, name : 'rollcall_id', className: 'text-center', 'visible': false},
  			{ targets : 1, name : 'voucher'},
  			{ targets : 2, name : 'driver_cd'},
  			{ targets : 3, name : 'driver_name'},
  			{ targets : 4, name : 'vehicle_cd'},
  			{ targets : 5, name : 'route'},
  			{ targets : 6, name : 'cycle'},
  			{ targets : 7, name : 'departure_dt'},
        { targets : 8, 'orderable': false}
          ],
          // scrollX : true,
      });

});

function doFilter()
{
    oTable.ajax.reload();
}

function doReset()
{
    $('#departure_dt_s').val($('#dt_s').val());
    $('#departure_dt_e').val($('#dt_e').val());
    oTable.ajax.reload();
}
