$(document).ready(function(){  
    $('.select2').select2();
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy',
    });

    $('.s2-cycle').hide();
});

function changeBusiness()
{
    
    $('#customer').val('all').trigger('change');
    $('#customer_lp_cd').val('all').trigger('change');
    $('#route').val('all').trigger('change');

    $('#cycle').val('all').trigger('change');    

    var b = $('#business').val();
    if (b == 'all')
    {
        $('.s2-business').show();
        $('.s2-cycle').hide();
    }
    else
    {
        $('.s2-business').hide()
        $('.s2-business-' + b).show();
    }
}


function changeCustomer()
{
    $('#customer_lp_cd').val('all').trigger('change');
    $('#route').val('all').trigger('change');

    $('#cycle').val('all').trigger('change');    

    var c = $('#customer').val();
    if (c == 'all')
    {
        $('.s2-customer').show();
        $('.s2-cycle').hide();
    }
    else
    {
        $('.s2-customer').hide()
        $('.s2-customer-' + c).show();
    }
    
}

function changeCustomerLP()
{
    $('#route').val('all').trigger('change');
    $('#cycle').val('all').trigger('change');    

    var clp = $('#customer_lp_cd').val();
    if (clp == 'all')
    {
        $('.s2-customer_lp_cd').show();
        $('.s2-cycle').hide();  
    }
    else
    {
        $('.s2-customer_lp_cd').hide()
        $('.s2-customer_lp_cd-' + clp.replace(' ', '_')).show();
    }
}

function changeRoute()
{
    $('#cycle').val('all').trigger('change');
    
    $('.s2-cycle').hide();
    var route = $('#route').val();
    
    if (route == 'all')
    {
        $('.s2-route').hide();
    }
    else
    {
        console.log('here');
        $('.s2-route').hide()
        $('.s2-route-' + route).show();
    }
}

function changeTimetableDt()
{
    var t = $('#timetable_dt').val();
    var ts = t.split('/');
    window.location = SITE_URL + 'report_timetable_actual/id/' + ts[2] + '-' + ts[1] + '-' + ts[0];
}

function doDownload()
{
    var timetable_dt = $('#timetable_dt').val();
    var business = $('#business').val();
    var customer = $('#customer').val();
    var customer_lp_cd = $('#customer_lp_cd').val();
    var route = $('#route').val();
    var cycle = $('#cycle').val();

    tx = timetable_dt.split('/');
    dt = tx[2] + '-' + tx[1] + '-' + tx[0];

	window.location.href = SITE_URL + 'report_timetable_actual/download/' + dt + '/' + business + '/' + customer + '/' + customer_lp_cd + '/' + route + '/' + cycle;
}