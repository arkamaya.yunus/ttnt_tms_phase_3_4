
$(document).ready(function(){
	$('.select2').select2();

	//initialize datatable
    oTable = $('#datatable').DataTable(
	{
		fixedHeader: true,
		ordering : true,
		lengthMenu : [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
		paging : true,
		serverSide : true,
		ajax : 
		{
			url: SITE_URL + "Route/gets",
			type: "POST",
			data : function (d)
			{
			}
		},
		//dom : '<"top">rt<"bottom"lpi><"clear">',
		pagingType : 'full_numbers',
		processing : true,
		columnDefs : 
		[
			{ targets : 0, name : 'customer'}, 
			{ targets : 1, name : 'route'}, 
			{ targets : 2, name : 'cycle'},
			{ targets : 3, name : 'route_detail', className: 'text-center'},
		],
		scrollX : true
	});
});

function doFilter()
{
	// if ($('#form_journal').valid()) 
	// {
		oTable.ajax.reload();
	// }
}

function doDownload()
{
	window.location.href = SITE_URL + 'route/download';
}

function doUpload()
{
	$('#myModal').modal('show');
}

function doUploadProcess()
{
	if ($('#frm').valid()) 
	{
		var formData = new FormData($('#frm')[0]);		
		$('#btn_upload_confirm').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('#btn_upload_confirm').attr('disabled', 'disabled');	

		$.ajax({
			type: "POST",
			url: SITE_URL + "route/upload",
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			success: function (data) {

				$('#btn_upload_confirm').html('Upload');
				$('#btn_upload_confirm').removeAttr('disabled');

				if (!data.success)
				{
					toastr['error'](data.message);					
				}
				else
				{
					toastr['success'](data.message);
					doFilter();
					$('#myModal').modal('hide');
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{										
				$('#btn_upload_confirm').html('Upload');
				$('#btn_upload_confirm').removeAttr('disabled');

				toastr["error"]("Error when processing Data");
			}
		});
	}
	else
	{
		toastr["error"]("Please input required fields")
	}
}

function checkextension() {
    var file = document.querySelector("#file_attach");
    if (/\.(xlsx)$/i.test(file.files[0].name) === false) {
        // alert("Upload file dengan format .xlsx!");
		toastr["error"]("Error. Make sure file template is Excel (xlsx)");
        $('#file_nm').val('');
        $('#file_attach').val('');
    } else {
        $('#file_nm').val(file.files[0].name);
    }
}