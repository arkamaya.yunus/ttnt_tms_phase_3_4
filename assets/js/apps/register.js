$(document).ready(function () {

    $('#register').validate({
        rules: {
            company_name: {
                required: true,
                minlength: 3
            }
            //, industry_type: "required"
            , package_id: "required"
            , name: {
                required: true,
                minlength: 5
            },
            phone: {
                required: true,
                minlength: 6,
                number: true
            },
            password: {
                required: true,
                minlength: 8
            },
            confirm_password: {
                required: true,
                minlength: 8,
                equalTo: "#password"
            }
            , email: {
                required: true,
                email: true,
                remote: {
                    url: SITE_URL + 'register/check_email',
                    type: "post",
                    data: {
                        email: function () { return $('input[name=email]').val(); }
                    }
                }
            }
            , agree : { required : true}
        },
        messages: {
            company_name: {
                required: "Nama Perusahaan harus diisi",
                minlength: "Nama Perusahaan harus lebih dari 3 karakter"
            }
            , name: {
                required: "Nama Lengkap harus diisi",
                minlength: "Nama Lengkap anda harus Lebih dari 5 karakter"
            }
            , email: {
                required: "Email harus diisi",
                email: "Format email salah",
                remote: "Email telah digunakan"
            }
            , phone: {
                required: "No. Telepon harus diisi",
                minlength: "No. Telepon harus lebih dari 6 angka",
                number: "No. Telepon harus berupa angka"
            }
            ,password: {
                required: "Kata Sandi harus diisi",
                minlength: "Kata Sandi harus lebih dari 8 karakter"
            },
            confirm_password: {
                required: "Tulis ulang kata sandi",
                minlength: "Kata Sandi harus lebih dari 8 karakter",
                equalTo: "Kata sandi tidak sama"
            }
            , agree : {
                required : "Harus di ceklis"
            }
        }
        , errorPlacement: function (error, element) {
            if(element[0].name != 'agree'){
                error.insertAfter(element);
            }else{
                error.insertAfter(element.parent());
            }
        }
        , highlight: function (element) {
            $(element).closest('.item').removeClass('has-success').addClass('has-error');
        }
        , success: function (element) {
            $(element).closest('.item').removeClass('has-error').addClass('has-success');
        }

    });    
    
    $('#confirm_password').on('keypress', function(e){
        if(e.which == 13) {
            on_register();
        }
    });
    
    $('#txtTermCondition').on('click', function(){
        $('#modal_termcondition').modal('show');
    });
});

function on_register() 
{
    var valid = $('#register').validate();
    if (valid.form()) 
	{
        $('.btnRegister').addClass('disabled').html('Mohon Tunggu  <i class="fa fa-spinner fa-pulse fa-fw"></i>');
        $('#register').submit();
    }
}


























