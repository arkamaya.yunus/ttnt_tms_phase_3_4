 function changePwd()
 {
    $('#cpwd_old').val('');
    $('#cpwd_new').val('');
    $('#cpwd_conf').val('');
    $('#myCPWD').modal('show');
 }

 function doSavePwd() {
    if ($('#f-cpwd').valid()) 
	{

        var cpwd_old = $('#cpwd_old').val();
        var cpwd_new = $('#cpwd_new').val();
        var cpwd_conf = $('#cpwd_conf').val();

        if (cpwd_new == cpwd_conf)
        {
            $('#btn_cpwd').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            $('#btn_cpwd').attr('disabled', 'disabled');
        
            $.ajax({
                type: "POST",
                url: SITE_URL + "company/changepwd",
                dataType: 'json',
                data: {
                    cpwd_old: cpwd_old,
                    cpwd_new: cpwd_new,
                    cpwd_conf: cpwd_conf,
                },		
                success: function (data) {
        
                    if (data.success)
                    {
                        toastr["success"](data.message);                        
                        $('#myCPWD').modal('hide');
                    }
                    else
                    {
                        toastr["error"](data.message);
                    }
        
                    $('#btn_cpwd').html('Change');
                    $('#btn_cpwd').removeAttr('disabled');
                    
                },
                error: function (jqXHR, textStatus, errorThrown)
                {										
                    $('#btn_cpwd').html('Change');
                    $('#btn_cpwd').removeAttr('disabled');
        
                    toastr["error"]("Error when processing Data");
                }
            });
        }
        else
        {
            toastr["error"]("New Password and Confirmation not match")
        }        
    }
    else
	{
		toastr["error"]("Please input required fields")
	}
}
