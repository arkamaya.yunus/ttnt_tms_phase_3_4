$(document).ready(function(){
  $('.select2').select2();
  $('.autonumber').autoNumeric('init', {aSign: 'Rp. ', aSep: '.', aDec: ','});
    $('#vehicle_owner').autocomplete({
		serviceUrl: SITE_URL + 'vehicle/get_vehicle_owners',
	});

	$('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy'
    });
});

function doSubmit()
{
	if ($('#frm').valid())
	{
		$('#btn_submit').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('.sbmt_btn').attr('disabled', 'disabled');

		$('#frm').submit();
	}
	else
	{
		toastr["error"]("Please input required fields")
	}
}
