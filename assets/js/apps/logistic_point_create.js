$(document).ready(function(){
    $('.select2').select2();
    
    // $('#lp_area').autocomplete({
	// 	//d: $('#journal_description').val(),
	// 	serviceUrl: SITE_URL + 'logistic_point/get_areas',
	// 	// onSelect: function (suggestion) {
	// 	// 	open_shortcut(suggestion.data);
	// 	// }
	// });

	changeArea();

	// $("#lp_color").colorpicker(
	// 	{format:"hex"}
	// );

	// $('#lp_color').colorpicker().on(
	// 	'changeColor',
	// 	function() {
	// 		$('#lp_color').css(
	// 			'background-color',
	// 			$(this).colorpicker('getValue', '#ffffff')
	// 		);
	// 		$('#lp_color').css(
	// 			'color',
	// 			$(this).colorpicker('getValue', '#ffffff')
	// 		);
	// 	}
	// );
});

function changeArea()
{
	var area_color = $("#lp_area").select2().find(":selected").data("color");
	$('#lp_color').val(area_color);
	$('#lp_color').css(
		'background-color',
		area_color
	);
	$('#lp_color').css(
		'color',
		area_color
	);
}

function doSubmit()
{
	if ($('#frm').valid()) 
	{		
		$('#btn_submit').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('.sbmt_btn').attr('disabled', 'disabled');
		
		$('#frm').submit();
	}
	else
	{
		toastr["error"]("Please input required fields")
	}
}