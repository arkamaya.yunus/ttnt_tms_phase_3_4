var rown = 1;
var rownB = 1;
var rownO = 1;

$(document).ready(function(){
    $('.select2').select2();
	
    $('.autonumber').autoNumeric('init', {aSep: '.', aDec: ',', aSign: '', pSign: '', aPad: 'false'});
	$('.currency').autoNumeric('init', {aSep: '.', aDec: ',', aSign: 'Rp. ', pSign: 'p'});
	
	add_new_row_emoney();
	initEmoneySelect2(1);
	
	add_new_row_bensin();
	initBensinSelect2(1);
	
	add_new_row_other();
	
	if($("#notif_route_error").text() != ""){
        toastr.warning('Data Route sudah digunakan.', 'Gagal!');
    }
	
});


function add_new_row_emoney(numOfRow)
{
	numOfRow = (numOfRow == null) ? 0 : numOfRow;
	
	for (i = 1; i <= numOfRow; i++)
	{
		rown = ++rown;
	
		var emoney = '<select class="form-control emoneys" id="emoney-'+ rown +'"></select>';
		var gel = '<input class="form-control emoney_gel text-center autonumber"  placeholder="0" id="gel-'+ rown +'" type="text" value="1"/>';

		var new_row = '<tr id="row-'+ rown +'" class="emoney-details"><td>' + emoney + '</td>';
		new_row += '<td>' + gel + '</td>';
		new_row += '<td><a href="javascript:;" class="remove_row"><i class="fa fa-trash"></i></a></td></tr>';
		
		$('#table-emoney').append(new_row);
		initEmoneySelect2(rown);	
		$('.autonumber').autoNumeric('init', {aSep: '.', aDec: ',', aSign: '', pSign: '', aPad: 'false'});
				
		$(document).on('click','.remove_row',function() {
			var row_id = $(this).closest('tr').attr('id');
			if (row_id != 'row-0' && row_id != 'row-1')
			{
				$(this).closest('tr').remove();
			}		
		});				
	}
}

function initEmoneySelect2(id)
{
	$("#emoney-" + id).select2(
	{
		ajax:
		{
			url: SITE_URL + 'Route/get_emoneys',
			dataType: 'json',
			delay: 250,
			data: function (params)
			{
					return { search: params.term };
			},
			processResults: function (data, params)
			{
				//console.log(data.emoneys);
				var newData = [];

				for ( var i = 0; i < data.emoneys.length; i++ )
				{
					newData.push(
					{
						id: data.emoneys[i].emoney_id,
						text: data.emoneys[i].emoney_id
					});
				}

			return { results: newData };
			},
			cache: true
		},
		minimumInputLength: 0,
		escapeMarkup: function (markup) {
			return markup;
		},
		language: {
			noResults: function() {
			  return 'Data tidak ditemukan';
			},
		},
	}).on("select2:select", function(e)
	{
		var x = 1;
		$("#adaEmoney").val(x);
	});
}

function add_new_row_bensin(numOfRow)
{
	numOfRow = (numOfRow == null) ? 0 : numOfRow;
	
	for (i = 1; i <= numOfRow; i++)
	{
		rownB = ++rownB;
	
		var spbu = '<select class="form-control spbus" id="spbu-'+ rownB +'"></select>';
		var lit = '<input class="form-control bensin_lit text-right autonumber"  placeholder="0" id="lit-'+ rownB +'" type="text" value="1"/>';

		var new_row = '<tr id="row-'+ rownB +'" class="bensin-details"><td>' + spbu + '</td>';
		new_row += '<td>' + lit + '</td>';
		new_row += '<td><a href="javascript:;" class="remove_row"><i class="fa fa-trash"></i></a></td></tr>';
		
		$('#table-bensin').append(new_row);
		initBensinSelect2(rownB);	
		$('.autonumber').autoNumeric('init', {aSep: '.', aDec: ',', aSign: '', pSign: '', aPad: 'false'});
				
		$(document).on('click','.remove_row',function() {
			var row_id = $(this).closest('tr').attr('id');
			if (row_id != 'row-0' && row_id != 'row-1')
			{
				$(this).closest('tr').remove();
			}		
		});				
	}
}

function initBensinSelect2(id)
{
	$("#spbu-" + id).select2(
	{
		ajax:
		{
			url: SITE_URL + 'Route/get_bensins',
			dataType: 'json',
			delay: 250,
			data: function (params)
			{
					return { search: params.term };
			},
			processResults: function (data, params)
			{
				var newData = [];

				for ( var i = 0; i < data.bensins.length; i++ )
				{
					newData.push(
					{
						id: data.bensins[i].SpbuCode,
						text: data.bensins[i].SpbuName
					});
				}

			return { results: newData };
			},
			cache: true
		},
		minimumInputLength: 0,
		escapeMarkup: function (markup) {
			return markup;
		},
		language: {
			noResults: function() {
			  return 'Data tidak ditemukan';
			},
		},
	}).on("select2:select", function(e)
	{
		var x = 1;
		$("#adaBensin").val(x);
	});
}

function add_new_row_other(numOfRow)
{
	numOfRow = (numOfRow == null) ? 0 : numOfRow;
	
	for (i = 1; i <= numOfRow; i++)
	{
		rownO = ++rownO;
	
		var other = '<input class="form-control others" id="other-'+ rownO +'" />';
		var jumlah = '<input class="form-control jumlah_other text-right currency" placeholder="Rp. 0,00" id="jumlah-'+ rownO +'" type="text"/>';

		var new_row = '<tr id="row-'+ rownO +'" class="other-details"><td>' + other + '</td>';
		new_row += '<td>' + jumlah + '</td>';
		new_row += '<td><a href="javascript:;" class="remove_row"><i class="fa fa-trash"></i></a></td></tr>';
		
		$('#table-other').append(new_row);
		$('.autonumber').autoNumeric('init', {aSep: '.', aDec: ',', aSign: '', pSign: '', aPad: 'false'});
		
		$('.currency').autoNumeric('init', {aSep: '.', aDec: ',', aSign: 'Rp. ', pSign: 'p'});
				
		$(document).on('click','.remove_row',function() {
			var row_id = $(this).closest('tr').attr('id');
			if (row_id != 'row-0' && row_id != 'row-1')
			{
				$(this).closest('tr').remove();
			}		
		});				
	}
}

function submit_route() {
	
	if ($('#frm').valid())
	{		
		var emoneys = 0;
		var count_emoneys = 0;	
		var e = $("#adaEmoney").val();
		
		$('.emoney-details').each(function(){			
			if(e > 0){				
				emoneys = $(this).find('.emoneys').select2('data')[0].id;
				count_emoneys = count_emoneys + 1;				
			}
		});
		
		var spbus = 0;
		var count_spbus = 0;
		var s = $("#adaBensin").val();
		
		$('.bensin-details').each(function(){
			if(s > 0){
				spbus = $(this).find('.spbus').select2('data')[0].id;
				count_spbus = count_spbus + 1;
			}
		});
		
		var others = 0;
		var count_others = 0;
		$('.other-details').each(function(){
			
			others = $(this).find('.others').val();
			count_others = count_others + 1;
		});
		
		//insert detail emoney
		if ($("#emoney-1").val() != null)
		{
			var arrEmoneys = new Array();
			$('.emoney-details').each(function(){

				var emoney = {};
				emoney.emoney_id = $(this).find('.emoneys').select2('data')[0].text;
				emoney.gel = $(this).find('.emoney_gel').val();				
				arrEmoneys.push(emoney);
				
			});
			
			$("#emoney_details").val(JSON.stringify(arrEmoneys));
			
		}		
		
		//=======insert detail spbu=======
		if ($("#spbu-1").val() != null)
		{
			var arrSpbus = new Array();
			$('.bensin-details').each(function(){

				var spbu = {};
				spbu.spbu = $(this).find('.spbus').select2('data')[0].text;
				spbu.liter = $(this).find('.bensin_lit').val();				
				arrSpbus.push(spbu);
				
			});
			$("#bensin_details").val(JSON.stringify(arrSpbus));
			
		}
		
		//insert detail other
		if ($("#other-1").val() !=null && $("#other-1").val() !="")
		{
			var arrOther = new Array();
			$('.other-details').each(function(){

				var other = {};
				other.others = $(this).find('.others').val();
				other.amount = $(this).find('.jumlah_other').val();			
				arrOther.push(other);
				
			});
			
			$("#other_details").val(JSON.stringify(arrOther));
			
		}
		
		$('#btn-submit').html('Mohon Tunggu...');
		$("#btn-submit").prop('disabled', true);
		$("#btn-close").prop('disabled', true);	
		
		$('#frm').submit();
		
	} else {
		toastr.warning('Mohon Lengkapi Form yang diperlukan !')
	}
}