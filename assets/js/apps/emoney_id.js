function doDelete()
{
	$('#btn_delete_confirm').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
	$('#btn_delete_confirm').attr('disabled', 'disabled');

	$('#frm').submit();
}
