
$(document).ready(function(){
    // set focus
    $('#company_name').focus();
    // onchange browse file
    $("#logo").change(function() {
        readURL(this);
      });
});


function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#preview_image').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }

$("#btn_submit").on("click",function(){
    var deletes = $('#delete_logo').val();
    if($('#settings_company').valid()){
        $("#btn_submit").html("Mohon Tunggu...");
        $('.sbmt_btn').attr('disabled', 'disabled');
        
       
		$('#settings_company').submit();
    }else{
        toastr["error"]("Mohon Lengkapi Form yang diperlukan");
    }
});

$('.numbersOnly').keyup(function () { 
    this.value = this.value.replace(/[^0-9\.]/g,'');
});
