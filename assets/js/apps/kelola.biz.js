$(document).ready(function()
{
	
	$('#f_search_quick_menu').autocomplete({
		//d: $('#journal_description').val(),
		serviceUrl: SITE_URL + 'app/get_shortcut',
		onSelect: function (suggestion) {
			open_shortcut(suggestion.data);
		}
	});

});

function open_shortcut(rel)
{
	window.location = SITE_URL + rel;
}