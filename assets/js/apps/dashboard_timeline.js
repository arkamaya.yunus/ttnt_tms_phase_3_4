$(document).ready(function(){
  $('.button-menu-mobile').trigger('click');

  var table = $('#table-cus-chart').DataTable( {
    scrollY:        '60vh',
    scrollX:        true,
    // scrollCollapse: true,
    paging:         false,
    info:     false,
    searching: false,
    fixedColumns:   {
      leftColumns: 3
    },

  } );

  // new $.fn.dataTable.FixedHeader( table );

  getDetail();

  $('.datepicker').datepicker({
      autoclose: true,
      todayHighlight: true,
  format: 'dd/mm/yyyy'
  });

});

function getDetail () {
  $.ajax({
   url: SITE_URL + 'dashboard_timeline/get_detail/',
   method: 'post',
   async: false,
   data:
   {
     from: $('#from').val(),
     to: $('#to').val()
   },
   dataType: 'json',
   success: function(data){
     console.log(data);
     $.each (data, function (key) {
         console.log (data[key].keytable);
         lp_code = data[key].lp_cd;
         lp_color = data[key].lp_color;
         lp_t_id = data[key].keytable + data[key].cycle;
         time = (data[key].departure_plan != null) ? data[key].departure_plan : data[key].arrival_plan;

         myTime = time.split(":");

         //get the nearest time for generating placement_id
         if(myTime[1] >= 0 && myTime[1] <= 10){
           nearest_time = 10;
         }else if(myTime[1] >= 20 && myTime[1] <= 29){
           nearest_time = 20;
         }else if(myTime[1] >= 30 && myTime[1] <= 39){
           nearest_time = 30;
         }else if(myTime[1] >= 40 && myTime[1] <= 49){
           nearest_time = 40;
         }else if(myTime[1] >= 50 && myTime[1] <= 50){
           nearest_time = 50;
         }

         // console.log(nearest_time);
         // console.log('menit:'+myTime[1]);
         placement_id = lp_t_id + myTime[0] + nearest_time;
         // console.log(placement_id);

         $('#'+ placement_id).html('<span class="badge" style="display: inline;background-color:'+lp_color+'">'+lp_code+'</span>');
     });
   }
  });
}

function doFilter()
{
  timeline_from = $('#from').val();
  timeline_to = $('#to').val();

  tx = timeline_from.split('/');
  ty = timeline_to.split('/');
  dtx = tx[2] + '-' + tx[1] + '-' + tx[0];
  dty = ty[2] + '-' + ty[1] + '-' + ty[0];

  dtxD = new Date(dtx);
  dtyD = new Date(dty);

  // alert(dtxD);

  if (dtxD <= dtyD){
	  window.location.href = SITE_URL + 'dashboard_timeline?dtx=' + dtx + '&dty=' + dty;
  }else {
    alert('Please select a greater "To" date than the previous "From" date!');
  }
}
