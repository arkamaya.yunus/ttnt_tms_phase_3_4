$(document).ready(function(){  
    $('.select2').select2();
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy',
    });

    onChangeRoute();

    // 1st load - hide Roll Call card
    // $('#card-rollcall').hide();

    //initialize datatable
    oTable = $('#datatable').DataTable(
        {
            fixedHeader: true,
            ordering : true,
            order: [[ 7, "asc" ]],
            lengthMenu : [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
            paging : true,
            serverSide : true,
            ajax : 
            {
                url: SITE_URL + "rollcall/gets",
                type: "POST",
                data : function (d)
                {
                    d.timetable_dt = $('#timetable_dt').val();
                    d.driver_cd = $('#driver_cd').select2('val');
                    d.route = $('#route').select2('val');
                    d.cycle = $('#cycle').select2('val');
                }
            },
            pagingType : 'full_numbers',
            processing : true,
            columnDefs : 
            [
                { targets : 0, name : 'vehicle_cd', className: 'text-center'}, 
                { targets : 1, name : 'driver_cd', visible: false}, 
                { targets : 2, name : 'driver_name'}, 
                { targets : 3, name : 'route'}, 
                { targets : 4, name : 'cycle'},                
                { targets : 5, name : 'lp_cd'},
                { targets : 6, name : 'departure_dt'},
                { targets : 7, name : 'departure_plan'},
                { targets : 8, name : 'clock_in'},
                { targets : 9, name : 'clock_out'},
                { targets : 10, name : 'option', orderable: false, className: 'text-center'},
            ],
            scrollX : true,
            drawCallback : function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                $('#btnFilter').html('Filter');
                $('#btnFilter').removeAttr('disabled');                    
            }
        });

});

function changeTruck(route, cycle, vehicle_cd)
{
    $('#change_route').html(route);
    $('#change_cycle').html(cycle);
    $('#change_vehicle_cd').select2('val', vehicle_cd);

    $('#change_timetable_dt').val($('#timetable_dt').val());
    $('#change_w').val('truck');

    $('#change_w_driver').hide();
    $('#change_w_truck').show();
    
    $('#myChangeLabel').html('Change Truck');
    $('#myChange').modal('show');
}

function changeDriver(route, cycle, driver_cd)
{
    $('#change_route').html(route);
    $('#change_cycle').html(cycle);
    $('#change_driver_cd').select2('val', driver_cd);

    $('#change_timetable_dt').val($('#timetable_dt').val());
    $('#change_w').val('driver');

    $('#change_w_driver').show();
    $('#change_w_truck').hide();
    
    $('#myChangeLabel').html('Change Driver');
    $('#myChange').modal('show');
}

function doSaveChangeTW()
{
    $('#btn_SaveTW').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
	$('#btn_SaveTW').attr('disabled', 'disabled');	

	$.ajax({
		type: "POST",
		url: SITE_URL + "rollcall/saveTW",
		dataType: 'json',
		data: {
            timetable_dt: $('#change_timetable_dt').val(),
            w: $('#change_w').val(),
            r: $('#change_route').html(),
            c: $('#change_cycle').html(),

            vehicle_cd: $('#change_vehicle_cd').select2('val'),
            driver_cd: $('#change_driver_cd').select2('val'),
		},		
		success: function (data) {

			if (data.success)
			{
                toastr["success"](data.message);
                oTable.ajax.reload();
                $('#myChange').modal('hide');
			}
			else
			{
				toastr["error"](data.message);
			}

			$('#btn_SaveTW').html('Save');
			$('#btn_SaveTW').removeAttr('disabled');
			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{										
			$('#btn_SaveTW').html('Save');
			$('#btn_SaveTW').removeAttr('disabled');

			toastr["error"]("Error when processing Data");
		}
	});
}

function onChangeRoute()
{
    var route = $('#route').select2('val');

    $('#cycle').select2({
        templateResult: function (option) {
            var myOption = $('#cycle').find('option[value="' + option.id + '"]');
            if (myOption.data('route') == route || myOption.data('route') == 'all') {
                return option.text;
            }
            return false;
        }
    });
    $('#cycle').select2('val', 'all~.~1');
}

function changeTimetableDt()
{
    var t = $('#timetable_dt').val();
    var ts = t.split('/');
    window.location = SITE_URL + 'rollcall/id/' + ts[2] + '-' + ts[1] + '-' + ts[0];
}

function rollcallProcess(timetable_dt, route, cycle, driver_cd, todo)
{    
    window.location = SITE_URL + 'rollcall/process/' + timetable_dt + '/' + route + '/' + cycle + '/' + driver_cd + '/' + todo;
}

function doFilter()
{
    $('#btnFilter').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Please Wait');
    $('#btnFilter').attr('disabled', 'disabled');
    oTable.ajax.reload();
    // var route = $('#route').select2('val');
    // var cycle = $('#cycle').select2('val');    

    // if (route != '' && cycle != '' && route != null && cycle != null)
    // {
    //     $('#btnFilter').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    //     $('#btnFilter').attr('disabled', 'disabled');
        
    //     var t = $('#timetable_dt').val();
    //     var ts = t.split('/');
        
    //     $.ajax({
    //         type: "POST",
    //         url: SITE_URL + "rollcall/getDataVehicle",
    //         dataType: 'json',
    //         data: {
    //             timetable_dt: ts[2] + '-' + ts[1] + '-' + ts[0],
    //             route: $('#route').select2('val'),
    //             cycle: $('#cycle').select2('val').split('~.~')[1],
    //         },		
    //         success: function (data) {

    //             rowsCall = '';
    
    //             for (i=0; i<data.length; i++)
    //             {
    //                 // hanya tampilkan Driver yg berangkat saja
    //                 if (data[i].departure_plan != null)
    //                 {
    //                     rowsCall += '<tr>';
    //                     rowsCall += '   <td>' + data[i].vehicle_cd + '</td>';
    //                     rowsCall += '   <td>' + data[i].driver_name + '</td>';                    
    //                     rowsCall += '   <td>' + data[i].lp_cd + '</td>';
    //                     rowsCall += '   <td>' + data[i].departure_dt + ' ' + data[i].departure_plan + '</td>';
    //                     rowsCall += '   <td>';
    //                     // rowsCall += '       <button type="button" class="btn btn-purple waves-effect waves-light btn-xs" title="Change Truck" onclick="changeTruck()">Change Truck</button>';
    //                     // rowsCall += '       <button type="button" class="btn btn-purple waves-effect waves-light btn-xs" title="Change Driver" onclick="changeDriver()">Change Driver</button>';
    //                     rowsCall += '       <button id="btnRollCall'+ data[i].timetable_detail_id +'" type="button" class="btn btn-primary waves-effect waves-light btn-xs" title="Roll Call Process" onclick="rollCallProcess(\'' + data[i].timetable_detail_id + '\')">RollCall Process</button>';
    //                     rowsCall += '   </td>';
    //                     rowsCall += '</tr>';                        
    //                 }
    //             }

    //             $("#datatable").find("tr:gt(0)").remove();
    //             $('#datatable').append(rowsCall);   
                
    //             $('#btnFilter').html('Filter');
    //             $('#btnFilter').removeAttr('disabled');
                
    //         },
    //         error: function (jqXHR, textStatus, errorThrown)
    //         {										
    //             $('#btnFilter').html('Filter');
    //             $('#btnFilter').removeAttr('disabled');
    
    //             toastr["error"]("Error when processing Data");
    //         }
    //     });
    // }
    // else
    // {
    //     toastr["error"]("Please select Route & Cycle");
    // }
}

// function rollCallProcess(timetable_detail_id)
// {
//     $('#btnRollCall' + timetable_detail_id).html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
//     $('#btnRollCall' + timetable_detail_id).attr('disabled', 'disabled');

//     $.ajax({
//         type: "POST",
//         url: SITE_URL + "rollcall/getTimetableDetail",
//         dataType: 'json',
//         data: {
//             timetable_detail_id: timetable_detail_id
//         },		
//         success: function (data) {

//             photo = data.driver.photo;
//             if (photo == '' || photo == null)
//             {
//                 photo = 'assets/images/user_photo.png';
//             }
//             else
//             {
//                 photo = 'assets/files/cid2/profile/thumbs/' + photo;
//             }
//             img = '<img src="' + SITE_URL + photo + '" class="img-thumbnail" alt="' + photo + '"  />';
//             $('#photo').html (img);
//             $('#driver_cd').html (data.driver.driver_cd);
//             $('#driver_name').html (data.driver.driver_name);
//             $('#driving_license_type').html (data.driver.driving_license_type);
//             $('#driving_license_val').html (data.driver.driving_license_val);
//             $('#smartphone').html (data.driver.smartphone);
//             $('#logistic_partner').html (data.driver.logistic_partner);
//             $('#vehicle_cd').html (data.driver.vehicle_cd);

//             $('#card-rollcall').show();
//             $('#card-table').hide();
//             $('#timetable_dt').attr('disabled', 'disabled');
//             $('#route').attr('disabled', 'disabled');
//             $('#cycle').attr('disabled', 'disabled');
//             $('#btnFilter').attr('disabled', 'disabled');
            
//             $('#btnRollCall' + timetable_detail_id).html('Rollcall Process');
//             $('#btnRollCall' + timetable_detail_id).removeAttr('disabled');
            
//         },
//         error: function (jqXHR, textStatus, errorThrown)
//         {										
//             $('#btnRollCall' + timetable_detail_id).html('Rollcall Process');
//             $('#btnRollCall' + timetable_detail_id).removeAttr('disabled');

//             toastr["error"]("Error when processing Data");
//         }
//     });    
// }

// function doClose()
// {
//     $("#datatable").find("tr:gt(0)").remove();
//     $('#card-rollcall').hide();
//     $('#card-table').show();
//     $('#timetable_dt').removeAttr('disabled');
//     $('#route').removeAttr('disabled');
//     $('#cycle').removeAttr('disabled');
//     $('#btnFilter').removeAttr('disabled');
// }