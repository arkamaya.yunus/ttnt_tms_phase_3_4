
$(document).ready(function(){
    // set focus
    
});


function doSave() {
    if ($('#frm').valid()) 
	{
        $('#btn_save_role').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('#btn_save_role').attr('disabled', 'disabled');
        $('#frm').submit();
    }
    else
	{
		toastr["error"]("Please input required fields")
	}
}

function confirmDelete(id)
{
    $('#user_group_id_delete').val(id);
    $('#myModal2').modal('show');
}

function doDelete()
{
	$('#btn_delete_confirm').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
	$('#btn_delete_confirm').attr('disabled', 'disabled');
	
	$('#frm2').submit();
}