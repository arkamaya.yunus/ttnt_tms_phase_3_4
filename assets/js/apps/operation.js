var oTable;
var oTable1;
var stopperRedraw = 0;
var startOtable = 0;
var defSort = [[ 1, "asc" ], [2, "asc"], [3, "asc"], [4, "asc"], [5, "asc"]];

$(document).ready(function(){  
    $('.select2').select2();
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy',
    });

    $('.timepicker').timepicker({showMeridian:!1,icons:{up:"mdi mdi-chevron-up",down:"mdi mdi-chevron-down"}});

    $('.autonumber2').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

    // hide all route first biar ga bingung
    $('.s2-route').hide();

    // nya (not yet arrived) to give better understanding of sorting not yet arrived timetable.
    var nya = $('#nya').val();    
    if (nya != '')
    {
        defSort = [[ 10, "desc" ]]; // give sort by remain Desc.
    }

    oTable1 = $('#datatable_timetable1').DataTable(
    {
        fixedHeader: true,
        ordering : true,
        order: defSort,
        lengthMenu : [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
        paging : true,
        serverSide : true,
        ajax : 
        {
            url: SITE_URL + "operation/getSummary",
            type: "POST",
            data : function (d)
            {            
                d.timetable_dt = $('#timetable_dt').val();
                d.business = $('#business').val();
                d.customer = $('#customer').val();
                d.customer_lp_cd = $('#customer_lp_cd').val();
                d.route = $('#route').val();
                d.cycle = $('#cycle').val();
                d.nya = nya;
            }
        },        
        pagingType : 'full_numbers',
        processing : true,
        columnDefs : 
        [
            { targets : 0, name : 'no', orderable: false, width: 20}, 
            { targets : 1, name : 'business'}, 
            { targets : 2, name : 'customer'}, 
            { targets : 3, name : 'customer_lp_cd'}, 
            { targets : 4, name : 'route', width: 50}, 
            { targets : 5, name : 'cycle', width: 50},            
            { targets : 6, name : 'vehicle_cd'},
            { targets : 7, name : 'driver_name'},
            { targets : 8, name : 'total', className: 'text-center', width: 50},
            { targets : 9, name : 'done', className: 'text-center', width: 50},
            { targets : 10, name : 'remain', className: 'text-center', width: 50},
            { targets : 11, name : 'percentage', className: 'text-center'},
            { targets : 12, name : 'option', orderable: false, className: 'text-center'},
        ],
        scrollX : true,
        drawCallback : function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

            $('#btnFilter').html('Filter');
            $('#btnFilter').removeAttr('disabled');

            $('#btn_backtt').html('Back');
            $('#btn_backtt').removeAttr('disabled');

            showHideTt(1);

            // change back stopperRedraw to 0
            stopperRedraw = 0;
            
            // // check triggered or not
            // // if triggered then change back trigger to false
            // var trigger_route = $('#trigger_route').val();
            // var trigger_cycle = $('#trigger_cycle').val();

            // if (trigger_route != '' && trigger_cycle != '')
            // {
            //     // set empty so they will never call again.
            //     $('#trigger_route').val('');
            //     $('#trigger_cycle').val('');

            //     viewDetail(trigger_route, trigger_cycle);
            // }
        }
    });

    oTable = $('#datatable_timetable').DataTable(
    {
        fixedHeader: true,
        ordering : true,
        order: [[ 20, "asc" ]],
        lengthMenu : [[100, 200, -1], [100, 200, 'All']],
        paging : true,
        serverSide : true,
        ajax : 
        {
            url: SITE_URL + "operation/getDetails",
            type: "POST",
            data : function (d)
            {
                d.timetable_id = $('#detail-timetable_id').val();
                d.route = $('#detail-route').val();
                d.cycle = $('#detail-cycle').val();
            }
        },        
        pagingType : 'full_numbers',
        processing : true,
        columnDefs : 
        [
            { targets : 0, name : 'route'}, 
            { targets : 1, name : 'cycle'}, 
            { targets : 2, name : 'vehicle_cd'},            
            { targets : 3, name : 'vehicle_number'},
            { targets : 4, name : 'driver_cd', visible: false},
            { targets : 5, name : 'driver_name', width: 200},
            { targets : 6, name : 'lp_cd'},
            { targets : 7, name : 'arrival_dt'},
            { targets : 8, name : 'arrival_plan'},
            { targets : 9, name : 'arrival_actual'},
            { targets : 10, name : 'arrival_gap'},
            { targets : 11, name : 'arrival_eval'},
            { targets : 12, name : 'departure_dt'},
            { targets : 13, name : 'departure_plan'},
            { targets : 14, name : 'departure_actual'},
            { targets : 15, name : 'departure_gap'},
            { targets : 16, name : 'departure_eval'},
            { targets : 17, name : 'remark'},
            { targets : 18, name : 'arrival_problem'},
            { targets : 19, name : 'departure_problem'},
            { targets : 20, name : 'timetable_detail_id', visible:false},
            { targets : 21, name : 'arrival_next_day', visible:false},
            { targets : 22, name : 'arrival_manual_by', visible:false},
            { targets : 23, name : 'arrival_manual_dt', visible:false},
            { targets : 24, name : 'departure_manual_by', visible:false},
            { targets : 25, name : 'departure_manual_by', visible:false},
            { targets : 26, name : 'km'},
            { targets : 27, name : 'fuel'},
            { targets : 28, name : 'spbu'},
            { targets : 29, name : 'toll'},
            { targets : 30, name : 'others'},
            { targets : 31, name : 'pallet'},
            { targets : 32, name : 'rack'},
            { targets : 33, name : 'division'},
        ],
        scrollX : true,
        drawCallback : function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

            $('#btn_refresh').html('Refresh');
            $('#btn_refresh').removeAttr('disabled');

            if ( $('#detail-route').val() != '-1' && $('#detail-cycle').val() != '-1' &&  $('#detail-timetable_id').val() != '-1')
            {                
                loadDetail();
            }
        }
    });

    $('#datatable_timetable tbody').on('click', 'tr', function () {
        var data = oTable.row( this ).data();
        $('#myTimeTable').modal('show');
        // alert( 'You clicked on '+data[0]+'\'s row' );
        
        // update 29-Juni-2020 - fix select2 on firefox.
        $('#d-vehicle_cd, #d-driver_cd').select2({
            dropdownParent: $("#myTimeTable")
        });


        // $('#d-route').html(data[0]);
        for(i=0; i<data.length;i++){
            $('#d-' + i).html(data[i]);
        }

        // hack for set manual
        $('#d-9-setq').hide();
        $('#d-9-set').hide();

        $('#d-9-set').val('00:00');
        $('#d-14-set').val('00:00');

        if (data[9] == '-')
        {
            // $('#d-9-set').val(data[8]).trigger("change");
            $('#d-9-setq').show();
        }
        $('#d-14-setq').hide();
        $('#d-14-set').hide();
        if (data[14] == '-')
        {
            // $('#d-14-set').val(data[13]).trigger("change");
            $('#d-14-setq').show();
        }
        
        $('#d-vehicle_cd').select2('val', data[2]);//.trigger('change');
        $('#d-driver_cd').select2('val', data[4]);//.trigger('change');
        $('#timetable_detail_id').val(data[20]);
        $('#remark').val(data[17]);
        $('#arrival_problem').val(data[18]);
        $('#departure_problem').val(data[19]);

        // 04 Mei 2020
        // Adding Manualy by Who ?
        $('#d-arrival_manual').html('');
        $('#d-departure_manual').html('');
        if (data[22] != '' && data[22] != null)
        {
            $('#d-arrival_manual').html('Set Manual By: <strong>' + data[22] + '</strong> at <strong>' + data[23] + '</strong>');
        }
        if (data[24] != '' && data[24] != null)
        {
            $('#d-departure_manual').html('Set Manual By: <strong>' + data[24] + '</strong> at <strong>' + data[25] + '</strong>');
        }

        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            oTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        // only show delete for NLP created manual (Not allowed for uploaded excel -- TEMPORARY / NEED CONFIRM ?)
        // $('#btn_delete_nlp').hide();
        
        // if (data[21] == '' || data[21] == null)
        // {
        //     $('#btn_delete_nlp').show();
        // }

        // 25 Mei 2020
        $('#km').val(data[26]);
        $('#fuel').val(data[27]);
        $('#spbu').val(data[28]);
        $('#tol').val(data[29]);
        $('#others').val(data[30]);
        $('#pallet').val(data[31]);
        $('#rack').val(data[32]);
        $('#division').val(data[33]);

        $('#lbl-ays').hide();
    } );

    // show hide
    showHideTt(1);

    // need to call it for the first time ?
    changeBusiness();
});

function doSaveTimeTableDetail()
{
    $('#btn_saving_timetable_detail').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
	$('#btn_saving_timetable_detail').attr('disabled', 'disabled');	

	$.ajax({
		type: "POST",
		url: SITE_URL + "timetable/saveDetail",
		dataType: 'json',
		data: {
            timetable_detail_id: $('#timetable_detail_id').val(),
            vehicle_cd: $('#d-vehicle_cd').select2('val'),
            driver_cd: $('#d-driver_cd').select2('val'),
            remark: $('#remark').val(),
            arrival_problem: $('#arrival_problem').val(),
            departure_problem: $('#departure_problem').val(),
            arrival_actual: $('#d-9-set').val(),
            departure_actual: $('#d-14-set').val(),
            km: $('#km').autoNumeric('get'),
            fuel: $('#fuel').autoNumeric('get'),
            spbu: $('#spbu').val(),
            tol: $('#tol').autoNumeric('get'),
            others: $('#others').autoNumeric('get'),
            pallet: $('#pallet').autoNumeric('get'),
            rack: $('#rack').autoNumeric('get'),
            division: $('#division').val()
		},		
		success: function (data) {

			if (data.success)
			{
                toastr["success"](data.message);
                oTable.ajax.reload();
                $('#myTimeTable').modal('hide');
			}
			else
			{
				toastr["error"](data.message);
			}

			$('#btn_saving_timetable_detail').html('Save');
			$('#btn_saving_timetable_detail').removeAttr('disabled');
			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{										
			$('#btn_saving_timetable_detail').html('Save');
			$('#btn_saving_timetable_detail').removeAttr('disabled');

			toastr["error"]("Error when processing Data");
		}
	});
}

function proceedDeleteNLP()
{
    $('#lbl-ays').hide();
    $('#btn_delete_nlp').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    $('#btn_delete_nlp').attr('disabled', 'disabled');
    
    $.ajax({
		type: "POST",
		url: SITE_URL + "timetable/deleteNLP",
		dataType: 'json',
		data: {
            timetable_detail_id: $('#timetable_detail_id').val(),
		},		
		success: function (data) {

			if (data.success)
			{
                toastr["success"](data.message);
                oTable.ajax.reload();
                $('#myTimeTable').modal('hide');
			}
			else
			{
				toastr["error"](data.message);
			}

			$('#btn_delete_nlp').html('Delete');
			$('#btn_delete_nlp').removeAttr('disabled');
			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{										
			$('#btn_delete_nlp').html('Delete');
			$('#btn_delete_nlp').removeAttr('disabled');

			toastr["error"]("Error when processing Data");
		}
	});
}

function doDeleteNLP()
{
    $('#lbl-ays').show();
}

function doSaveNLP()
{
    $('#btn_saving_nlp').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
	$('#btn_saving_nlp').attr('disabled', 'disabled');	

	$.ajax({
		type: "POST",
		url: SITE_URL + "timetable/saveNLP",
		dataType: 'json',
		data: {
            timetable_id: $('#detail-timetable_id').val(),
            timetable_dt: $('#timetable_dt').val(),
            route: $('#detail-route').val(),
            cycle: $('#detail-cycle').val(),
            vehicle_cd: $('#nlp-vehicle_cd').select2('val'),
            driver_cd: $("#nlp-driver_cd").select2('val'),
            lp_cd: $('#nlp-lp_cd').select2('val'),
            arrival_dt: $('#nlp-arrival_dt').val(),
            arrival_plan: $('#nlp-arrival_plan').val(),
            departure_dt: $('#nlp-departure_dt').val(),
            departure_plan: $('#nlp-departure_plan').val(),
		},		
		success: function (data) {

			if (data.success)
			{
                toastr["success"](data.message);
                oTable.ajax.reload();
                $('#myNewLP').modal('hide');
			}
			else
			{
				toastr["error"](data.message);
			}

			$('#btn_saving_nlp').html('Save');
			$('#btn_saving_nlp').removeAttr('disabled');
			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{										
			$('#btn_saving_nlp').html('Save');
			$('#btn_saving_nlp').removeAttr('disabled');

			toastr["error"]("Error when processing Data");
		}
	});
}

function addNewLP()
{
    $('#nlp-r').html($('#detail-route').val());
    $('#nlp-c').html($('#detail-cycle').val());

    $('#nlp-arrival_dt').val($('#timetable_dt').val());
    $('#nlp-departure_dt').val($('#timetable_dt').val());

    $('#nlp-arrival_plan').val('0:00');
    $('#nlp-departure_plan').val('0:00');

    $('#myNewLP').modal('show');
}

function back()
{
    $('#btn_backtt').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Please Wait');
    $('#btn_backtt').attr('disabled', 'disabled');
    
    $('.f1').removeAttr('disabled');
    // $('#business').val('all');
    // $('#customer').val('all');
    // $('#customer_lp_cd').val('all');
    // $('#route').val('all');
    // $('#cycle').val('all');

    startOtable = 0;

    oTable1.ajax.reload();
}

function showHideTt(tt)
{
    // show hide
    $('.timetable-detail').hide();
    $('.timetable-detail-' + tt).show();        
}

function loadDetail()
{
    var r = $('#detail-route').val();
    var c = $('#detail-cycle').val();
    var custlp = $('#customer_lp_cd').val();
    
   
    $.ajax({
        type: "POST",
        url: SITE_URL + "timetable/getTotalHours",
        dataType: 'json',
        data: {
            timetable_id: $('#detail-timetable_id').val(),
            r: r,
            c: c,
        },		
        success: function (data) {

            showHideTt(2);

            $('.timetable-detail-3').show();
            
            $('#btn-' + custlp.replace(' ', '_') + '-' + r.replace(' ', '_') + '-' + c).html('<i class=" mdi mdi-magnify"></i>');
            $('#btn-' + custlp.replace(' ', '_') + '-' + r.replace(' ', '_') + '-' + c).removeAttr('disabled');

            $('#total_driver_hours').html(data.total_driver_hours);
            $('#total_working_hours').html(data.total_working_hours);
            $('#total_loading_unloading').html(data.total_loading_unloading);

            $('#arrival_ontime').html(data.arrival_ontime);
            $('#arrival_advance').html(data.arrival_advance);
            $('#arrival_delay').html(data.arrival_delay);
            $('#arrival_result').html(Math.round(((parseInt(data.arrival_ontime) + parseInt(data.arrival_advance)) / parseInt(data.arrival_total) ) * 100) + '%');
            
            $('#departure_ontime').html(data.departure_ontime);
            $('#departure_advance').html(data.departure_advance);
            $('#departure_delay').html(data.departure_delay);

            $('#departure_result').html(Math.round(((parseInt(data.departure_ontime) + parseInt(data.departure_advance)) / parseInt(data.departure_total)) * 100) + '%');

            $('#destination_result').html(data.destination_result);
            
            if (stopperRedraw == 0)
            {
                oTable.columns.adjust().draw();
                stopperRedraw = 1;
            }

            // if (startOtable == 0)
            // {
            //     startOtable = 1;
            //     SyncOTable();
            // }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {										    
            toastr["error"]("Error when processing Data");
            $('#btn-' + custlp.replace(' ', '_') + '-' + r.replace(' ', '_') + '-' + c).html('<i class=" mdi mdi-magnify"></i>');
            $('#btn-' + custlp.replace(' ', '_') + '-' + r.replace(' ', '_') + '-' + c).removeAttr('disabled');
        }
    });
}

function viewDetail(tid, r, c, b, cust, custlp)
{
    $('#detail-timetable_id').val(tid);
    $('#detail-route').val(r);
    $('#detail-cycle').val(c);

    // $('#business').val(b);
    // $('#customer').val(cust);
    // $('#customer_lp_cd').val(custlp);
    // $('#route').val(r.replace(' ', '_'));
    // $('#cycle').val(c);
    $('#customer_lp_cd-text').html(custlp);
    $('.f1').attr('disabled', 'disabled');

    $('#btn-' + custlp.replace(' ', '_') + '-' + r.replace(' ', '_') + '-' + c).html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
    $('#btn-' + custlp.replace(' ', '_') + '-' + r.replace(' ', '_') + '-' + c).attr('disabled', 'disabled');
    
    oTable.ajax.reload();
}

function doDeleteRC()
{
    $('#btn_delete_rc_confirm').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    $('#btn_delete_rc_confirm').attr('disabled', 'disabled');
    
    $.ajax({
		type: "POST",
		url: SITE_URL + "timetable/deleteRCNotMd5",
		dataType: 'json',
		data: {
            timetable_id: $('#del-rc-timetable_id').val(),
            route: $('#del-rc-route').val(),
            cycle: $('#del-rc-cycle').val()
		},		
		success: function (data) {

			if (data.success)
			{
                toastr["success"](data.message);
                oTable1.ajax.reload();
                $('#myModalRC').modal('hide');
			}
			else
			{
				toastr["error"](data.message);
			}

			$('#btn_delete_rc_confirm').html('Delete');
			$('#btn_delete_rc_confirm').removeAttr('disabled');
			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{										
			$('#btn_delete_rc_confirm').html('Delete');
			$('#btn_delete_rc_confirm').removeAttr('disabled');

			toastr["error"]("Error when processing Data");
		}
	});
}

function delPerRC(tid, r,c)
{
    $('#del-rc-timetable_id').val(tid);
    $('#del-rc-route').val(r);
    $('#del-rc-cycle').val(c);

    $('#lbl-rc-route').html(r);
    $('#lbl-rc-cycle').html(c);

    $('#myModalRC').modal('show');
}

function changeTruck(w, tid, r,c,t,d)
{
    $('#myChangeTruck').modal('show');
    $('#change_w').val(w);
    $('#change_truck_timetable_id').val(tid);
    $('#change_truck_route').val(r);
    $('#change_truck_cycle').val(c);

    $('#change_truck_route-d-0').html(r);
    $('#change_truck_route-d-1').html(c);    
    $('#change_truck_route-d1-vehicle_cd').select2('val', t);
    $('#change_truck_route-d1-driver_cd').select2('val', d);

    $('.change-w').hide();
    $('.change_w-' + w).show();

    $('#myChangeTruckLabel').html('Change ' + w.toUpperCase());
}

function doSaveChangeTW()
{
    $('#btn_SaveTW').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
	$('#btn_SaveTW').attr('disabled', 'disabled');	

	$.ajax({
		type: "POST",
		url: SITE_URL + "timetable/saveTW",
		dataType: 'json',
		data: {
            timetable_id: $('#change_truck_timetable_id').val(),
            w: $('#change_w').val(),
            r: $('#change_truck_route').val(),
            c: $('#change_truck_cycle').val(),

            vehicle_cd: $('#change_truck_route-d1-vehicle_cd').select2('val'),
            driver_cd: $('#change_truck_route-d1-driver_cd').select2('val'),
		},		
		success: function (data) {

			if (data.success)
			{
                toastr["success"](data.message);
                oTable1.ajax.reload();
                $('#myChangeTruck').modal('hide');
			}
			else
			{
				toastr["error"](data.message);
			}

			$('#btn_SaveTW').html('Save');
			$('#btn_SaveTW').removeAttr('disabled');
			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{										
			$('#btn_SaveTW').html('Save');
			$('#btn_SaveTW').removeAttr('disabled');

			toastr["error"]("Error when processing Data");
		}
	});
}

function doFilter()
{
    $('#btnFilter').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Please Wait');
    $('#btnFilter').attr('disabled', 'disabled');
    oTable1.ajax.reload();
}

function changeBusiness()
{
    
    $('#customer').val('all').trigger('change');
    $('#customer_lp_cd').val('all').trigger('change');
    $('#route').val('all').trigger('change');
    $('#cycle').val('all').trigger('change');

    $('.s2-business').hide();
    $('.s2-customer').hide();
    $('.s2-customer_lp_cd').hide();
    $('.s2-route').hide();
    $('.s2-cycle').hide();

    var b = $('#business').val();
    if (b != 'all')
    {        
        $('.s2-business-' + b).show();
    }
}


function changeCustomer()
{
    $('#customer_lp_cd').val('all').trigger('change');
    $('#route').val('all').trigger('change');

    $('#cycle').val('all').trigger('change');    

    var c = $('#customer').val();

    $('.s2-customer').hide();
    $('.s2-customer_lp_cd').hide();
    $('.s2-route').hide();
    $('.s2-cycle').hide();

    if (c != 'all')    
    {        
        $('.s2-customer-' + c).show();
    }
    
}

function changeCustomerLP()
{
    $('#route').val('all').trigger('change');
    $('#cycle').val('all').trigger('change');    

    var clp = $('#customer_lp_cd').val();
    $('.s2-customer_lp_cd').hide();
    $('.s2-route').hide();
    $('.s2-cycle').hide();

    if (clp != 'all')    
    {        
        $('.s2-customer_lp_cd-' + clp.replace(' ', '_')).show();
    }
}

function changeRoute()
{
    $('#cycle').val('all').trigger('change');            
    
    var route = $('#route').val();    
    $('.s2-route').hide();
    $('.s2-cycle').hide();

    if (route != 'all')
    {
        $('.s2-route-' + route).show();
    }
}

function changeTimetableDt()
{
    var t = $('#timetable_dt').val();
    var ts = t.split('/');
    window.location = SITE_URL + 'operation/id/' + ts[2] + '-' + ts[1] + '-' + ts[0];
}

// function SyncOTable()
// {

//     if (startOtable == 1)
//     {
//         oTable.ajax.reload();

//         setTimeout(function() { 
//             SyncOTable();
//         }, 15000);
//     }    
    
// }

function doRefresh()
{
    $('#btn_refresh').html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Please Wait');
    $('#btn_refresh').attr('disabled', 'disabled');
    oTable.ajax.reload();
}