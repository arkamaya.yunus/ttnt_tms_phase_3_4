var rc_sleep_time;
var rc_body_temp;
var rc_blood_pres_sistole;
var rc_blood_pres_diastole;
var rc_apd_vest;
var rc_apd_helm;
var rc_apd_sepatu;
var rc_apd_uniform;
var rc_badge;
var rc_basket;
var rc_key;
var etoll_id;
var etoll_balance_departure;
var etoll_balance_arrival;

$(document).ready(function(){
    // $('#btnPrint').attr('disabled', 'disabled');
    $('.autonumber').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '1'});
    $('.autonumber2').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});

    checkPrint();

    $('.role').hide();
    $('.role-' + $('#role_nm').val()).show();
}); 

function checkPrint()
{    

    rc_sleep_time = $('input[name=rc_sleep_time]:checked', '#myForm').val();
    rc_body_temp = $('#rc_body_temp').autoNumeric('get');
    rc_blood_pres_sistole = $('#rc_blood_pres_sistole').autoNumeric('get');
    rc_blood_pres_diastole = $('#rc_blood_pres_diastole').autoNumeric('get');
    rc_apd_vest = $('#rc_apd_vest').is(":checked");    
    rc_apd_helm = $('#rc_apd_helm').is(":checked");
    rc_apd_sepatu = $('#rc_apd_sepatu').is(":checked");    
    rc_apd_uniform = $('#rc_apd_uniform').is(":checked");
    rc_badge = $('input[name=rc_badge]:checked', '#myForm').val();
    rc_basket = $('input[name=rc_basket]:checked', '#myForm').val();
    rc_key = $('input[name=rc_key]:checked', '#myForm').val();
    etoll_id = $('#etoll_id').val();
    etoll_balance_departure = $('#etoll_balance_departure').autoNumeric('get');
    etoll_balance_arrival = $('#etoll_balance_arrival').autoNumeric('get');
    
    if (
        rc_sleep_time == '1' &&
        ( parseFloat(rc_body_temp) >= 35 && parseFloat(rc_body_temp) <= 37.5 ) &&
        parseInt(rc_blood_pres_sistole) < 140 &&
        ( parseInt(rc_blood_pres_diastole) >= 60 && parseInt(rc_blood_pres_diastole) <= 90 ) &&
        rc_apd_vest == true &&
        rc_apd_helm == true && 
        rc_apd_sepatu == true && 
        rc_apd_uniform == true &&
        rc_badge == '1' && 
        rc_basket == '1' && 
        rc_key == '1' &&
        etoll_id != '' &&
        etoll_balance_departure > 0
    )
    {
        $('#btnPrint').removeAttr('disabled');
    }
    else
    {
        $('#btnPrint').attr('disabled', 'disabled');
    }
}

function doSave()
{
    checkPrint();
    if ($('#myForm').valid()) 
    {
        if (rc_blood_pres_sistole != '' && rc_blood_pres_diastole != '')
        {
            $('#btnSave').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
            $('#btnSave').attr('disabled', 'disabled');	
    
            $.ajax({
                type: "POST",
                url: SITE_URL + "rollcall/save",
                dataType: 'json',
                data: {
                    timetable_dt: $('#timetable_dt').html(),
                    customer: $('#customer').html(),
                    route: $('#route').html(),
                    cycle: $('#cycle').html(),
                    driver_cd: $('#driver_cd').html(),
                    vehicle_cd: $('#vehicle_cd').html(),
    
                    rc_sleep_time: rc_sleep_time,
                    rc_body_temp: rc_body_temp,
                    rc_blood_pres_sistole: rc_blood_pres_sistole,
                    rc_blood_pres_diastole: rc_blood_pres_diastole,
                    rc_apd_vest: rc_apd_vest,
                    rc_apd_helm: rc_apd_helm,
                    rc_apd_sepatu: rc_apd_sepatu,
                    rc_apd_uniform: rc_apd_uniform,
                    rc_badge: rc_badge,
                    rc_basket: rc_basket,
                    rc_key: rc_key,
                    etoll_id: etoll_id,
                    etoll_balance_departure: etoll_balance_departure,
                    etoll_balance_arrival: etoll_balance_arrival                    
                },		
                success: function (data) {
    
                    if (data.success)
                    {
                        toastr["success"](data.message);
                    }
                    else
                    {
                        toastr["error"](data.message);
                    }
    
                    $('#btnSave').html('Save');
                    $('#btnSave').removeAttr('disabled');
                    
                },
                error: function (jqXHR, textStatus, errorThrown)
                {										
                    $('#btnSave').html('Save');
                    $('#btnSave').removeAttr('disabled');
    
                    toastr["error"]("Error when processing Data");
                }
            });
        }
        else
        {
            toastr["error"]("Please input required Field");
        }
    }
}

function doPrint()
{
    $('#btnPrint').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    $('#btnPrint').attr('disabled', 'disabled');	

    $.ajax({
        type: "POST",
        url: SITE_URL + "rollcall/print",
        dataType: 'json',
        data: {
            timetable_dt: $('#timetable_dt').html(),
            route: $('#route').html(),
            cycle: $('#cycle').html(),
        },
        success: function (data) {

            if (data.success)
            {
                tx = $('#timetable_dt').html().split('/');
                dt = tx[2] + '-' + tx[1] + '-' + tx[0];

                route = $('#route').html();
                cycle = $('#cycle').html();

                window.open(SITE_URL + 'report_driver/print/' + dt + '/' + route + '/' + cycle);
            }
            else
            {
                // toastr["error"](data.message);
                $('#printmsg').html(data.message);
                $('#myModal').modal('show');
            }

            $('#btnPrint').html('<i class="mdi mdi-printer"></i> Driver Report');
            $('#btnPrint').removeAttr('disabled');
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {										
            $('#btnPrint').html('<i class="mdi mdi-printer"></i> Driver Report');
            $('#btnPrint').removeAttr('disabled');

            toastr["error"]("Error when processing Data");
        }
    });
}

function onPrintForced()
{
    $('#myModal').modal('hide');

    tx = $('#timetable_dt').html().split('/');
    dt = tx[2] + '-' + tx[1] + '-' + tx[0];

    route = $('#route').html();
    cycle = $('#cycle').html();

    window.open(SITE_URL + 'report_driver/print/' + dt + '/' + route + '/' + cycle);
}