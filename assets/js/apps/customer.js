$(document).ready(function(){

    $('#lbl-ays').hide();
	
    oTable = $('#datatable').DataTable(
	{
		fixedHeader: true,
        ordering : true,
        order: [[ 1, "asc" ]],
		lengthMenu : [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
		paging : true,
		serverSide : true,
		ajax : 
		{
			url: SITE_URL + "customer/gets",
			type: "POST",			
		},
		pagingType : 'full_numbers',
		processing : true,
		columnDefs : 
		[
			{ targets : 0, name : 'system_code'}, 
			{ targets : 1, name : 'system_value_txt'}, 
        ],
        scrollX : true,
    });
    
    $('#datatable tbody').on('click', 'tr', function () {
        var data = oTable.row( this ).data();

        $('#system_code').val(data[0]);
        $('#system_code').attr('disabled', 'disabled');
        $('#system_value_txt').val(data[1]);
        $('#todo').val('edit');
        $('#myModalLabel').html('Edit Customer');
        $('#myModal').modal('show');                
    });
});

function doFilter()
{	
    oTable.ajax.reload();
}

function onCreate()
{
    $('#system_code').removeAttr('disabled');
    $('#system_code').val('');
    $('#system_value_txt').val('');
    $('#todo').val('create');
    $('#myModalLabel').html('Create Customer');
    $('#myModal').modal('show');
}

function onSave()
{
    if ($('#frm').valid()) 
	{		
		$('#btn_submit').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('#btn_submit').attr('disabled', 'disabled');
		
        // ajax
        $.ajax({
            type: "POST",
            url: SITE_URL + "customer/save",
            dataType: 'json',
            data: {
                todo: $('#todo').val(),
                system_code: $('#system_code').val(),
                system_value_txt: $('#system_value_txt').val()
            },		
            success: function (data) 
            {
                if (data.success == true)
                {
                    toastr["success"](data.message);
                    $('#myModal').modal('hide');
                    doFilter();
                }
                else
                {
                    toastr['error'](data.message);
                }

                $('#btn_submit').html('Save');
		        $('#btn_submit').removeAttr('disabled');
                
            },
            error: function (jqXHR, textStatus, errorThrown)
            {										
                toastr["error"]("Error when getting Data");
                $('#myModal').modal('hide');
                
                $('#btn_submit').html('Save');
		        $('#btn_submit').removeAttr('disabled');
            }
        });
	}
	else
	{
        toastr["error"]("Please input required fields");

        $('#btn_submit').html('Save');
        $('#btn_submit').removeAttr('disabled');
        
	}
}

function doDelete()
{
    $('#btn_delete').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
    $('#btn_delete').attr('disabled', 'disabled');
    $('#lbl-ays').hide();
		
    // ajax
    $.ajax({
        type: "POST",
        url: SITE_URL + "customer/ondelete",
        dataType: 'json',
        data: {            
            system_code: $('#system_code').val()
        },		
        success: function (data) 
        {
            if (data.success == true)
            {
                toastr["success"](data.message);
                $('#myModal').modal('hide');
                doFilter();
            }
            else
            {
                toastr['error'](data.message);
            }

            $('#btn_delete').html('Delete');
            $('#btn_delete').removeAttr('disabled');
            
        },
        error: function (jqXHR, textStatus, errorThrown)
        {										
            toastr["error"]("Error when getting Data");
            $('#myModal').modal('hide');
            
            $('#btn_delete').html('Delete');
            $('#btn_delete').removeAttr('disabled');
        }
    });
}