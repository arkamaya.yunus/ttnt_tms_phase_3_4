
$(document).ready(function(){
	$('.select2').select2();

	//initialize datatable
    oTable = $('#datatable').DataTable(
	{
		fixedHeader: true,
        ordering : true,
        order: [[ 0, "asc" ]],
		lengthMenu : [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, 'All']],
		paging : true,
		serverSide : true,
		ajax : 
		{
			url: SITE_URL + "vehicle/gets",
			type: "POST",
			data : function (d)
			{
				d.vehicle_owner = $('#vehicle_owner').select2('val');
				d.active = $('#active').select2('val');
				d.business = $('#business').select2('val');
			}
		},
		//dom : '<"top">rt<"bottom"lpi><"clear">',
		pagingType : 'full_numbers',
		processing : true,
		columnDefs : 
		[
			{ targets : 0, name : 'vehicle_cd'}, 
            // { targets : 1, name : 'vehicle_id'}, 
            { targets : 1, name : 'vehicle_number'}, 
			{ targets : 2, name : 'vehicle_type'},
            
            { targets : 3, name : 'license_stnk'},
            { targets : 4, name : 'license_keur'},
            { targets : 5, name : 'license_sipa'},
            { targets : 6, name : 'license_ibm'},

			{ targets : 7, name : 'vehicle_owner'},
			{ targets : 8, name : 'brand'},
			{ targets : 9, name : 'active'},
			{ targets : 10, name : 'business'}
        ],
        scrollX : true,
	});
});

function doFilter()
{
	// if ($('#form_journal').valid()) 
	// {
		oTable.ajax.reload();
	// }
}

function doDownload()
{
	var vehicle_owner = $('#vehicle_owner').select2('val');
	var active = $('#active').select2('val');
	var business = $('#business').select2('val');
	window.location.href = SITE_URL + 'vehicle/download/' + vehicle_owner + '/' + active + '/' + business;
}

function doUpload()
{
	$('#myModal').modal('show');
}

function doUploadProcess()
{
	if ($('#frm').valid()) 
	{
		var formData = new FormData($('#frm')[0]);		
		$('#btn_upload_confirm').html('Please Wait <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('#btn_upload_confirm').attr('disabled', 'disabled');	

		$.ajax({
			type: "POST",
			url: SITE_URL + "vehicle/upload",
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			success: function (data) {

				$('#btn_upload_confirm').html('Upload');
				$('#btn_upload_confirm').removeAttr('disabled');

				if (!data.success)
				{
					toastr['error'](data.message);					
				}
				else
				{
					toastr['success'](data.message);
					doFilter();
					$('#myModal').modal('hide');
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{										
				$('#btn_upload_confirm').html('Upload');
				$('#btn_upload_confirm').removeAttr('disabled');

				toastr["error"]("Error when processing Data");
			}
		});
	}
	else
	{
		toastr["error"]("Please input required fields")
	}
}

function checkextension() {
    var file = document.querySelector("#file_attach");
    if (/\.(xlsx)$/i.test(file.files[0].name) === false) {
        // alert("Upload file dengan format .xlsx!");
		toastr["error"]("Error. Make sure file template is Excel (xlsx)");
        $('#file_nm').val('');
        $('#file_attach').val('');
    } else {
        $('#file_nm').val(file.files[0].name);
    }
}