$(document).ready(function(){    
    $('.button-menu-mobile').trigger('click');
    $('.select2').select2({dropdownAutoWidth : true});
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
		format: 'dd/mm/yyyy'	
    });

    getDepartureDelay();
});

var trace;

function getDepartureDelay()
{
    $('#pLoad').show();        

    $.ajax({
        type: "POST",
        url: SITE_URL + "dashboard_delay_list/getDepartureDelay",
        dataType: 'json',
        data: {
            customer_lp_cd: $('#customer_lp_cd').select2('val'),
            timetable_dt: $('#timetable_dt').val()
        },		
        success: function (data) {
            
            var rowsDeparture = '';
            var rowsArrival = '';

            if (data.departure.length > 0)
            {
                for(i=0; i<data.departure.length; i++)
                {
                    var bgcolor = ''; var clr = ''; var c2 = 'red';
                    // hack
                    var h = data.departure[i][7].split(':');
                    if (parseInt(h[1]) > 30 || parseInt(h[0]) >= 1)
                    {
                        bgcolor = 'style="background-color: #f5707a;"';
                        clr = ' style="color: white;"';
                        c2 = 'white';
                    }

                    rowsDeparture += '<tr ' + bgcolor + ' onclick="window.open(\'' + SITE_URL + 'timetable/id/' + data.departure[i][3] + '/' + data.departure[i][1] + '/' + data.departure[i][2] + '\')">';
                    rowsDeparture += '   <td' + clr + '>' + data.departure[i][0] + '</td>';
                    rowsDeparture += '   <td' + clr + '>' + data.departure[i][1] + '</td>';
                    rowsDeparture += '   <td' + clr + '>' + data.departure[i][2] + '</td>';
                    rowsDeparture += '   <td' + clr + '>' + data.departure[i][4] + '</td>';
                    rowsDeparture += '   <td' + clr + '>' + data.departure[i][5] + '</td>';
                    rowsDeparture += '   <td' + clr + '>' + data.departure[i][6] + '</td>';
                    rowsDeparture += '   <td style="color: ' + c2 + '">' + data.departure[i][7] + '</td>';
                    rowsDeparture += '   <td' + clr + '>' + data.departure[i][8] + '</td>';
                    rowsDeparture += '</tr>';
                }
            }

            if (data.arrival.length > 0)
            {
                for(i=0; i<data.arrival.length; i++)
                {
                    var bgcolor = ''; var clr = ''; var c2 = 'red';
                    // hack
                    var h = data.arrival[i][7].split(':');
                    if (parseInt(h[1]) > 30 || parseInt(h[0]) >= 1)
                    {
                        bgcolor = 'style="background-color: #f5707a;"';
                        clr = ' style="color: white;"';
                        c2 = 'white';
                    }

                    rowsArrival += '<tr ' + bgcolor + ' onclick="window.open(\'' + SITE_URL + 'timetable/id/' + data.arrival[i][3] + '/' + data.arrival[i][1] + '/' + data.arrival[i][2] + '\')">';
                    rowsArrival += '   <td' + clr + '>' + data.arrival[i][0] + '</td>';
                    rowsArrival += '   <td' + clr + '>' + data.arrival[i][1] + '</td>';
                    rowsArrival += '   <td' + clr + '>' + data.arrival[i][2] + '</td>';
                    rowsArrival += '   <td' + clr + '>' + data.arrival[i][4] + '</td>';
                    rowsArrival += '   <td' + clr + '>' + data.arrival[i][5] + '</td>';
                    rowsArrival += '   <td' + clr + '>' + data.arrival[i][6] + '</td>';
                    rowsArrival += '   <td style="color: ' + c2 + '">' + data.arrival[i][7] + '</td>';
                    rowsArrival += '   <td' + clr + '>' + data.arrival[i][8] + '</td>';
                    rowsArrival += '</tr>';
                }
            }
            
            $("#dtDepartureDelay").find("tr:gt(0)").remove();
            $('#dtDepartureDelay').append(rowsDeparture);

            $("#dtArrivalDelay").find("tr:gt(0)").remove();
            $('#dtArrivalDelay').append(rowsArrival);            

            $('#pLoad').hide();

            setTimeout(function() { 
                getDepartureDelay();
            }, 5000);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {										
            // toastr["error"]("Error when getting Data");
            $('#pLoad').hide();

            setTimeout(function() { 
                getDepartureDelay();
            }, 5000);
        }
    });
}