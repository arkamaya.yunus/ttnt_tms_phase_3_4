<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author: misbah@arkamaya.co.id
 * @created: 2017-02-22
 */
class Permission 
{
    var $CI; // CI instance
    var $CID;
    var $UgroupID;
    var $isExpired;
    var $page;
    var $_page;
    var $action;
    var $stdAction = array("create", "id", "edit", "delete", "approve");
    var $blockAction = array("create", "edit", "delete", "approve"); //block action ketika masa percobaan sudah expired
	
	var $app_id 	= '';
	var $app_name 	= '';
	var $app_icon 	= '';
    
    function __construct() {
        $this->CI = & get_instance();
		
        // set user_group and company id from session
        $this->UgroupID     = ($this->CI->session->userdata(S_USER_GROUP_ID)) ? $this->CI->session->userdata(S_USER_GROUP_ID) : 0;
        $this->CID          = ($this->CI->session->userdata(S_COMPANY_ID)) ? $this->CI->session->userdata(S_COMPANY_ID) : 0;
        $this->isExpired    = ($this->CI->session->userdata(S_COMPANY_EXPIRED)) ? $this->CI->session->userdata(S_COMPANY_EXPIRED) : 0;
        $this->page         = ($this->CI->uri->segment(1)) ? $this->CI->uri->segment(1) : "";
        $this->action       = ($this->CI->uri->segment(2)) ? $this->CI->uri->segment(2) : "";
		
		if ($this->isExpired == 1) // jika sudah kadaluarsa masa percobaannya, check actionnya
		{ 
            if (in_array($this->action, $this->blockAction))
			{
                redirect($this->CI->session->userdata(S_DEFAULT_LANDING));
            }
        }
        
        if (in_array($this->action, $this->stdAction))
		{
            $this->_page = $this->page.'/'.$this->action;
        }
		else
		{
            $this->_page = $this->page;
        }

        if ($this->CID == null || $this->CID == 0) 
		{
            $this->CI->session->sess_destroy();
            redirect('login');
        }

		// get app info (ID and Name)
        $this->get_app_info();
        
        if ($this->app_id == '')
        {
            redirect('login/out');
        }
        		
    }
	
	function get_app_info()
	{
		$sql = "
			select 
				f.app_id
				, a.app_name
				, a.app_icon
			from 
				tb_m_function f
				inner join tb_m_app a on a.app_id = f.app_id
			where 
				function_controller = '" . $this->page . "'
		";
		// echo $sql;
		$res = $this->CI->db->query($sql)->row();
		
		$this->app_id 	= $res->app_id;
		$this->app_name	= $res->app_name;
		$this->app_icon	= $res->app_icon;
	}
    
    function get_user_permissions()
    {
        $this->CI->db->select('ff.feature_id,ff.feature_name');
        $this->CI->db->from('tb_m_function f');       
        $this->CI->db->join('tb_m_company_function cf', 'f.function_id = cf.function_id and cf.company_id = '.$this->CID,'inner');
        $this->CI->db->join('tb_m_user_group_auth uga', 'f.function_id = uga.function_id and uga.user_group_id = '.$this->UgroupID,'inner');
        $this->CI->db->join('tb_m_function_feature ff', 'f.function_id = ff.function_id and uga.feature_id = ff.feature_id','inner');
        $this->CI->db->where('f.function_active', 1);
        $this->CI->db->where('ff.feature_element_type', 'page');
        $this->CI->db->where('ff.feature_action', $this->_page);
        
        $query = $this->CI->db->get();

        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
	
    function get_access_button()
    {
        if ($this->isExpired == 1) // jika sudah kadaluarsa masa percobaannya, ga melakukan generate button
		{
            return false;
        }
        
        $where = "(ff.feature_element_type LIKE '%button%' OR ff.feature_element_type = 'submit')";
        
        $this->CI->db->select('ff.feature_element_id, ff.feature_element_class, ff.feature_name, ff.feature_element_icon, ff.feature_position, ff.feature_action, ff.feature_element_type');
        $this->CI->db->from('tb_m_function f');       
        $this->CI->db->join('tb_m_company_function cf', 'f.function_id = cf.function_id and cf.company_id = '.$this->CID,'inner');
        $this->CI->db->join('tb_m_user_group_auth uga', 'f.function_id  = uga.function_id and uga.user_group_id = '.$this->UgroupID,'inner');
        $this->CI->db->join('tb_m_function_feature ff', 'f.function_id = ff.function_id and uga.feature_id = ff.feature_id','inner');

        $this->CI->db->or_where($where);
        $this->CI->db->where('f.function_controller', $this->page);
        $this->CI->db->where('f.function_active', 1);
        
        $query = $this->CI->db->get();
        
        if ($query->num_rows())
        {
            $data = array();
            foreach ( $query->result_array() as $row)
            {
                if ( $row['feature_element_type']=="button_group" || $row["feature_element_type"]== "button_group_list")
				{
                    $r = array(
                        'name'          => $row['feature_element_id'],
                        'id'            => $row['feature_element_id'],
                        'type'          => $row['feature_element_type'],
                        'content'       => $row['feature_name'],
                        'class'         => $row['feature_element_class'],
                        'onclick'       => $row['feature_action']
                    );
                }
				else if ($row['feature_element_type']=="button" || $row['feature_element_type'] == "submit")
				{
                    $r = array(
                        'name'          => $row['feature_element_id'],
                        'id'            => $row['feature_element_id'],
                        'type'          => $row['feature_element_type'],
                        'content'       => '<i class="'.$row['feature_element_icon'].'"> </i> '.$row['feature_name'],
                        'class'         => $row['feature_element_class'],
                        'onclick'       => $row['feature_action']
                    );
                }
                
                $data[ $row['feature_element_id'] ] = $r;
            }
            return $data;
        }
        else
        {
            return false;
        }
    }
    
    
    function get_access_menu()
    {        
		$sql = "
			select 
				ff.function_id as function_grp
				, ff.function_name as function_grp_nm
				, ff.function_icon
				, f.function_id
				, f.function_name
				, f.function_controller
				, f.function_link_visible
			from 
				tb_m_user_group_auth uga 
				inner join tb_m_user_group ug on ug.user_group_id = uga.user_group_id
				inner join tb_m_function f on f.function_id = uga.function_id
				inner join tb_m_function ff on ff.function_id = f.function_parent
			where
				ug.company_id = '" . $this->CID . "' and ug.user_group_id = '" . $this->UgroupID . "' and f.function_active = '1'
				and f.app_id = '" . $this->app_id . "' AND f.function_link_visible = '1'
			group by
				uga.function_id
			order by
				ff.function_order, f.function_order
		";
        
        $query = $this->CI->db->query($sql);
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    function get_access_menu2()
    {        
		$sql = "
			select 
				ff.function_id as function_grp
				, ff.function_name as function_grp_nm
				, ff.function_icon
				, f.function_id
				, f.function_name
				, f.function_controller
				, f.function_link_visible
			from 
				tb_m_function f 
				inner join tb_m_function ff on ff.function_id = f.function_parent
			where
				f.function_active = '1'
				and f.app_id = '" . $this->app_id . "' AND f.function_link_visible = '1'
			group by
				f.function_id
			order by
				ff.function_order, f.function_order
		";
        
        $query = $this->CI->db->query($sql);
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
}
