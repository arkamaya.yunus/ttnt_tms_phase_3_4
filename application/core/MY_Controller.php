<?php if (! defined('BASEPATH')) exit ('No direct script access allowed');
		
class MY_Controller extends CI_Controller 
{    
    var $permissions 	= array();
    var $button 		= array(); 
    var $menu 			= array();
    var $menu2 			= array();
    var $company 		= array();
	var $app_id	 		= '';
	var $app_name 		= '';
	var $app_icon 		= '';
    
    function __construct() 
	{
        parent:: __construct();
        $this->load->helper('error');
        $this->load->library('permission');
        $this->load->helper('common');
		$this->load->model('company_model');
        
        if (!$this->permissions = $this->permission->get_user_permissions())
        {
            show_401();
        }
        else
		{            
			//generate button
            $this->button 	= $this->permission->get_access_button();
            $this->menu 	= $this->permission->get_access_menu();
            $this->menu2 	= $this->permission->get_access_menu2();
            $this->company 	= $this->company_model->get_company();
			
			$this->app_id	= $this->permission->app_id;
			$this->app_name	= $this->permission->app_name;
			$this->app_icon = $this->permission->app_icon;
        }
        
    }
    
}