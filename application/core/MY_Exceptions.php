<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author: misbah@arkamaya.co.id
 * @created: 2017-02-22
 */
class MY_Exceptions extends CI_Exceptions {

    function __construct(){
        parent::__construct();
    }

    function show_401()
    {
        $heading = "401 Unauthorized";
        $message = "You don't have permission to access this page.";
        echo $this->show_error($heading, $message, 'error_401',401);
        exit;
    }
    
    function show_error_custom($heading = "", $message = "", $code = 404)
    {
        echo $this->show_error($heading, $message, 'error_custom',$code);
        exit;
    }
}