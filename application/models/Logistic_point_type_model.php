<?php
/*
* @author: Irfan Satriadarma
* @created: 12 Maret 2020
*/

class Logistic_point_type_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
    }

    function gets()
    {
        $sql = "select lp_type_id, lp_type_name 
                from tb_m_logistic_point_type 
                where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                order by lp_type_name";
        return $this->db->query($sql)->result();
    }
}