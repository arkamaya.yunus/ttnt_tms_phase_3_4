<?php
/*
* @author: Irfan Satriadarma
* @created: 19 Januari 2018
*/

class App_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
	}
	
	function get_apps()
	{
		$sql = "
			select 
				distinct f.app_id
				, a.app_name
				, a.app_label
				, a.app_icon
				, a.app_bgcolor
				, coalesce(f2.function_controller, 'javascript:;') as function_controller
			from 
				tb_m_company_function cf 
			inner join tb_m_function f on f.function_id = cf.function_id
			inner join tb_m_app a on a.app_id = f.app_id
			inner join tb_m_function f2 on f2.function_id = a.app_default_function_id
			where 
				cf.company_id = '". $this->session->userdata(S_COMPANY_ID) ."'
				and f.app_id <> 0
				and a.app_active = '1'
			order by
				a.app_order
		";
		return $this->db->query($sql)->result();
	}
	
	function get_shortcut($d)
	{
		$sql = "
			select 
				feature_name, feature_action 
			from 
				tb_m_function_feature 
			where 
				feature_element_type = 'page' 
				and feature_element_id = 'shortcut' 
				and (feature_name like '%" . $d . "%' or feature_action like '%" . $d . "%')
			order by feature_name
		";
		return $this->db->query($sql)->result();
	}
}
?>