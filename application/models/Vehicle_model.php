<?php
/*
* @author: Irfan Satriadarma
* @created: 12 Maret 2020
*/

class Vehicle_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
    }

    function getVehicleOwners($keyword = '')
    {
        $sql = "select distinct vehicle_owner
                from tb_m_vehicle
                where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                and vehicle_owner like '%" . $keyword . "%'
                order by vehicle_owner";
        return $this->db->query($sql)->result();
    }

    function getVehicles($vehicle_owner, $active, $start, $length, $sv, $order, $columns, $business)
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {            
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }

        $where_vehicle_owner = ($vehicle_owner != 'all') ? " and a.vehicle_owner = '" . $vehicle_owner . "'" : '';
        $where_active = ($active != 'all') ? " and a.active = '" . $active . "'" : '';
        $where_business = ($business != 'all') ? " and a.business = '" . $business . "'" : '';

        $sql = "
            select 
                a.vehicle_cd, a.vehicle_id,
                a.vehicle_number, a.vehicle_type,
                a.license_stnk, a.license_keur, a.license_sipa, a.license_ibm,
                a.vehicle_owner, a.insurance_no, a.brand, a.active, a.business
			from 
				tb_m_vehicle a 
			where
				(
                    vehicle_cd like '%" . $sv . "%'
                    or vehicle_id like '%" . $sv . "%'
                    or vehicle_number like '%" . $sv . "%'
                    or vehicle_type like '%" . $sv . "%'
                    or license_stnk like '%" . $sv . "%'
                    or license_keur like '%" . $sv . "%'
                    or license_sipa like '%" . $sv . "%'
                    or license_ibm like '%" . $sv . "%'
                    or vehicle_owner like '%" . $sv . "%'
                    or insurance_no like '%" . $sv . "%'
                    or brand like '%" . $sv . "%'
                )
                " . $where_vehicle_owner . $where_active . $where_business . "
            order by " . $order_by . " ";
            
		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
		return $this->db->query($sql)->result();
    }

    function getCountOfVehicle($vehicle_owner, $active, $sv, $business)
    {
        $where_vehicle_owner = ($vehicle_owner != 'all') ? " and a.vehicle_owner = '" . $vehicle_owner . "'" : '';
        $where_active = ($active != 'all') ? " and a.active = '" . $active . "'" : '';
        $where_business = ($business != 'all') ? " and a.business = '" . $business . "'" : '';

        $sql = "
			select 
                count(a.vehicle_number) as cnt
            from 
				tb_m_vehicle a 
			where
				(
                    vehicle_cd like '%" . $sv . "%'
                    or vehicle_id like '%" . $sv . "%'
                    or vehicle_number like '%" . $sv . "%'
                    or vehicle_type like '%" . $sv . "%'
                    or license_stnk like '%" . $sv . "%'
                    or license_keur like '%" . $sv . "%'
                    or license_sipa like '%" . $sv . "%'
                    or license_ibm like '%" . $sv . "%'
                    or vehicle_owner like '%" . $sv . "%'
                    or insurance_no like '%" . $sv . "%'
                    or brand like '%" . $sv . "%'
                )
                " . $where_vehicle_owner . $where_active . $where_business . "
		";
		return $this->db->query($sql)->row()->cnt;
    }

    function getVehicle($vehicle_cd)
    {
        $sql = "
            select 
                a.vehicle_cd, a.vehicle_id,
                a.vehicle_number, a.vehicle_type,
                a.license_stnk, a.license_keur, a.license_sipa, a.license_ibm,
                a.vehicle_owner, a.insurance_no, a.brand, a.active, a.business
            from 
                tb_m_vehicle a
            where
                md5(vehicle_cd) = '" .$vehicle_cd . "'
        ";        
        return $this->db->query($sql)->result();
    }

    function delete($vehicle_cd)
    {        
        $this->db->where('md5(vehicle_cd)', $vehicle_cd);
        $this->db->delete('tb_m_vehicle');
    }

    function save($update = '')
    {
        // validate vehicle_cd
        $check_lp = $this->getVehicle(md5($this->input->post('vehicle_cd', true)));
        if (count($check_lp) == 0 || $update == '1')
        {
            $lp = array
            (
                'company_id' => $this->session->userdata(S_COMPANY_ID),
                'vehicle_cd' => $this->input->post('vehicle_cd', true),
                'vehicle_id' => $this->input->post('vehicle_id', true),
                'vehicle_number' => $this->input->post('vehicle_number', true),                
                'vehicle_type' => $this->input->post('vehicle_type', true),
                'vehicle_owner' => $this->input->post('vehicle_owner', true),
                'insurance_no' => $this->input->post('insurance_no', true),
                'brand' => $this->input->post('brand', true),
                'license_stnk' => ($this->input->post('license_stnk',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('license_stnk', true)))) : null,
                'license_keur' => ($this->input->post('license_keur',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('license_keur', true)))) : null,
                'license_sipa' => ($this->input->post('license_sipa',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('license_sipa', true)))) : null,
                'license_ibm' => ($this->input->post('license_ibm',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('license_ibm', true)))) : null,
                'active' => $this->input->post('active', true),
                'business' => $this->input->post('business', true),
            );

            if ($update == '')
            {
                $lp['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
                $lp['created_dt'] = date('Y-m-d H:i:s');

                $this->db->insert('tb_m_vehicle', $lp);
                
                return '1';
            }
            else 
            {
                $lp['changed_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
                $lp['changed_dt'] = date('Y-m-d H:i:s');

                $this->db->where('vehicle_cd', $this->input->post('vehicle_cd', true));
                $this->db->update('tb_m_vehicle', $lp);

                return '2'; // update success
            }    
        }
        else
        {
            return '0'; // vehicle_number already exists
        }        
    }

    function getVehiclesForDownload($vehicle_owner, $active, $business)
    {
        $where_vehicle_owner = ($vehicle_owner != 'all') ? " and vehicle_owner = '" . $vehicle_owner . "'" : '';
        $where_active = ($active != 'all') ? " and active = '" . $active . "'" : '';
        $where_business = ($business != 'all') ? " and business = '" . $business . "'" : '';

        $sql = "
            Select * from tb_m_vehicle where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
             " . $where_vehicle_owner . $where_active . $where_business . " 
            order by vehicle_owner, vehicle_cd
        ";
        return $this->db->query($sql)->result();
    }
}