<?php
/*
* @author: Rakha@arkamaya.co.id
* @created: 12 May 2020
*/

class Route_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
    }

    function getRoutes($start, $length, $sv, $order, $columns)
    {
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {            
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }
        $sql = "
			SELECT
				route,
				cycle,
				customer
			FROM
				tb_m_route
			where
				(
                    route like '%" . $sv . "%'
                    or cycle like '%" . $sv . "%'
                    or customer like '%" . $sv . "%'
                )
            order by " . $order_by . " ";
            
		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
		
		return $this->db->query($sql)->result();
    }

    function getCountRoutes($sv)
    {
        $sql = "
			select 
                count(route) as cnt
			FROM
				tb_m_route
			where
				(
                    route like '%" . $sv . "%'
                    or cycle like '%" . $sv . "%'
                    or customer like '%" . $sv . "%'
                )";
		return $this->db->query($sql)->row()->cnt;
    }

    function getRoute($route, $cycle)
    {
        $sql = "
			SELECT
				route,
				cycle,
				customer
			FROM
				tb_m_route
			where
				route = '" . $route . "'
				and cycle = '" . $cycle . "'
			";
		return $this->db->query($sql)->result();
    }
	
	 function getMaterCustomer($keyword = '')
    {
        $sql = "SELECT
					a.system_code AS CustomerCode,
					a.system_value_txt AS CustomerName 
				FROM
					tb_m_system a 
				WHERE
					a.system_type = 'customer' 
					and a.system_code like '%" . $keyword . "%'
                order by a.system_code";
        return $this->db->query($sql)->result();
    }
	
	function get_emoneys($sv='')
	{
		$sql = 
		"
			SELECT
				* 
			FROM
				tb_m_emoney
			WHERE
				1=1
		";
		
		if($sv != ''){
			$sql = $sql . "
				AND(emoney_id like '%" .$sv. "%' OR 
					bank like '%" .$sv. "%')  
			";
		}
		
		return $this->db->query($sql)->result();
    }
	
	function get_bensins($sv='')
	{
		$sql = 
		"
			SELECT
				a.system_code AS SpbuCode,
				a.system_value_txt AS SpbuName 
			FROM
				tb_m_system a 
			WHERE
				a.system_type = 'spbu'
		";
		
		if($sv != ''){
			$sql = $sql . "
				AND(a.system_code like '%" .$sv. "%' OR 
					a.system_value_txt like '%" .$sv. "%')  
			";
		}
		
		return $this->db->query($sql)->result();
    }
	
	function saveRoute($dataRoute) 
	{	
        ////////// start transaction /////////////
        $this->db->trans_start();

		$dataRoute['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
		$dataRoute['created_dt'] = date('Y-m-d H:i:s');
		
		$this->db->insert('tb_m_route', $dataRoute);
		$this->insert_detail_emoney($dataRoute['route'], $dataRoute['cycle']);
		$this->insert_detail_bensin($dataRoute['route'], $dataRoute['cycle']);
		$this->insert_detail_other($dataRoute['route'], $dataRoute['cycle']);

        $this->db->trans_complete();
        /////// END TRANSCTION ////////
    }
	
	function editRoute($dataRoute) 
	{	
        ////////// start transaction /////////////
        $this->db->trans_start();
		
		$dataRoute['changed_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
		$dataRoute['changed_dt'] = date('Y-m-d H:i:s');

		$where = array (
			'route' 	=> $dataRoute['route'],
			'cycle' 	=> $dataRoute['cycle'],
			'customer' 	=> $dataRoute['customer']
		);
		
		$where_detail = array (
			'route' 	=> $dataRoute['route'],
			'cycle' 	=> $dataRoute['cycle']
		);

		$this->db->where($where)->delete('tb_m_route');		
		$this->db->where($where_detail)->delete('tb_m_route_emoney');		
		$this->db->where($where_detail)->delete('tb_m_route_fuel');		
		$this->db->where($where_detail)->delete('tb_m_route_others');		
		
		$this->db->insert('tb_m_route', $dataRoute);
		$this->insert_detail_emoney($dataRoute['route'], $dataRoute['cycle']);
		$this->insert_detail_bensin($dataRoute['route'], $dataRoute['cycle']);
		$this->insert_detail_other($dataRoute['route'], $dataRoute['cycle']);

        $this->db->trans_complete();
        /////// END TRANSCTION ////////
    }
	
	function insert_detail_emoney($route, $cycle)
	{	
		foreach (json_decode($this->input->post('emoney_details', true)) as $d)
		{		
			
			$this->db->query("
				insert into tb_m_route_emoney
				(
					route
					, cycle
					, emoney_id
					, gel
				)
				values
				(
					'" . $route . "'
					, '" . $cycle . "'
					, '" . $d->emoney_id . "'
					, '" . $d->gel . "'
				)

			");
		}
	}
	
	function insert_detail_bensin($route, $cycle)
	{	
		foreach (json_decode($this->input->post('bensin_details', true)) as $d)
		{		
			
			$this->db->query("
				insert into tb_m_route_fuel
				(
					route
					, cycle
					, spbu
					, liter
				)
				values
				(
					'" . $route . "'
					, '" . $cycle . "'
					, '" . $d->spbu . "'
					, '" . $d->liter . "'
				)

			");
		}
	}
	
	function insert_detail_other($route, $cycle)
	{	
		foreach (json_decode($this->input->post('other_details', true)) as $d)
		{		
			$amount = str_replace(".","",$d->amount);
			$amounts = str_replace("Rp ","",$amount);
			
			$this->db->query("
				insert into tb_m_route_others
				(
					route
					, cycle
					, others
					, amount
				)
				values
				(
					'" . $route . "'
					, '" . $cycle . "'
					, '" . $d->others . "'
					, '" . $amounts . "'
				)

			");
		}
	}
	
	function get_emoney_detail($route, $cycle)
    {
    	$sql = "
			SELECT
				* 
			FROM
				tb_m_route_emoney
			WHERE
				route = '" . $route . "'
				and cycle = '" . $cycle . "'
		";

		return $this->db->query($sql)->result();
    }
	
	function get_fuel_detail($route, $cycle)
    {
    	$sql = "
			SELECT
				* 
			FROM
				tb_m_route_fuel
			WHERE
				route = '" . $route . "'
				and cycle = '" . $cycle . "'
		";

		return $this->db->query($sql)->result();
    }
	
	function get_other_detail($route, $cycle)
    {
    	$sql = "
			SELECT
				route,
				cycle,
				others,
				concat( 'Rp. ', FORMAT(amount, 2 ) ) AS amount_text,
				amount
			FROM
				tb_m_route_others
			WHERE
				route = '" . $route . "'
				and cycle = '" . $cycle . "'
		";

		return $this->db->query($sql)->result();
    }
	
	function delete($route, $cycle)
    {        
		$this->db->where("route", $route);
		$this->db->where("cycle", $cycle);
        $this->db->delete('tb_m_route');
    }
	
	function delete_emoney($route, $cycle){
		$this->db->where("route", $route);
		$this->db->where("cycle", $cycle);
		$this->db->delete('tb_m_route_emoney');
	}
	
	function delete_fuel($route, $cycle){
		$this->db->where("route", $route);
		$this->db->where("cycle", $cycle);
		$this->db->delete('tb_m_route_fuel');
	}
	
	function delete_other($route, $cycle){
		$this->db->where("route", $route);
		$this->db->where("cycle", $cycle);
		$this->db->delete('tb_m_route_others');
	}
	
	function getRouteForDownload()
    {
        $sql = "
            Select 
				* 
			From 
				tb_m_route
        ";
        return $this->db->query($sql)->result();
    }
}