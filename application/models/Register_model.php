<?php

/*
 * @author: misbah@arkamaya.co.id
 * @created: 2017-03-01
 * @modifier: Irfan Satriadarma (irfan@arkamaya.co.id)
 */

class Register_model extends CI_Model 
{
    function __construct() 
	{
        parent:: __construct();
    }
	
	function check_email($email) 
	{        
        $sql = "select email from  tb_r_register where 
					email = '".$email."' 
					and activation = '1' 
					and activation_expired >= now()
					
                union				
				
                select user_email from tb_m_company  where user_email = '".$email."'				
				
                union
				
                select user_email from tb_m_users where user_email = '".$email."' 
		";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return 'false';
        } else {
            return 'true';
        }
    }
	
	function register($data) 
	{
        return $this->db->insert('tb_r_register', $data);
    }
	
	function get_activation_code($email) 
	{
        $this->db->select('*');
        $this->db->from('tb_r_register');
        $this->db->where('email', $email);
        $this->db->where('activation', 0);
        $this->db->where('activation_expired >= now()');
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return false;
        }
    }
	
	function get_register($code) 
	{
        $this->db->select('register_id, name, email, phone, password, company_name, industry_type, package_id');
        $this->db->from('tb_r_register');
        $this->db->where('activation_code', $code);
        $this->db->where('activation', 0);
        $this->db->where('activation_expired >=', date('Y-m-d H:i:s'));
        
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    function get_register_by_user_email() 
	{
        $this->db->select('register_id, name, email, phone, password, company_name, industry_type, package_id');
        $this->db->from('tb_r_register');
        $this->db->where('email', $this->session->userdata(S_USER_EMAIL));
        $this->db->where('activation', 1);
        
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    function setupNewUser($register)
    {        
		// insert user
		$sqlUser = "insert into tb_m_users(user_email, user_password, full_name, is_active, super_admin, email_confirmed, created_dt)
		values ('" . $register->email . "', '" . $register->password . "', '" . $register->name . "', '1', '1', '1', NOW())";
		$this->db->query($sqlUser);
    }

    function generate_data($company, $register) 
	{
        $this->db->trans_begin();
		
		// get default_landing
		$sqlPackage 	= "select default_landing, default_users from tb_m_package where package_id = '" . $company['package_id'] . "'";
		$packageRow 	= $this->db->query($sqlPackage)->row();
		$defaultLanding	= ($packageRow->default_landing != '') ? $packageRow->default_landing : 'setting';
		$default_users 	= ($packageRow->default_users != '') ? $packageRow->default_users : '5';

        // insert company
        $this->db->insert('tb_m_company', $company);
        
		// get last company id
        $company_id = $this->db->insert_id();
		
        //insert Company Function
        $sqlCompanyFunc = "insert into tb_m_company_function(company_id, function_id, created_by, created_dt, changed_by, changed_dt)
		select " . $company_id . ", f.function_id, 'system', now(), 'system', now()  from tb_m_package_detail pd 
		inner join tb_m_function f on pd.`code` = f.function_id and f.function_active = 1
		where pd.type = 'function' and pd.package_id = '" . $company['package_id'] . "'";
        $this->db->query($sqlCompanyFunc);
		
        //insert User Group
        $sqlUserGroup = "insert into tb_m_user_group(company_id, user_group_description, is_admin, default_user_lock, created_by, created_dt, default_landing )
                        select " . $company_id . ", system_value_txt, system_value_num as is_admin, 0 as def_user_lock, 'system', now(), '" . $defaultLanding . "' from tb_m_system where system_type = 'app_config' and system_code = 'user_group_admin'";
        $this->db->query($sqlUserGroup);

        // get last user_group_id
		$user_group_id = $this->db->insert_id();

        // insert user group auth
        $sqlUserGroupAuth = "insert into tb_m_user_group_auth(user_group_id, function_id, feature_id, created_by, created_dt)
                select ug.user_group_id, cf.function_id,  ff.feature_id, 'system' as created_by, now() as created_dt
		from tb_m_company_function cf
		inner join tb_m_function_feature ff on cf.function_id = ff.function_id
		INNER JOIN tb_m_user_group ug on cf.company_id = ug.company_id and is_admin = 1 
		where cf.company_id = '" . $company_id . "'";
        $this->db->query($sqlUserGroupAuth);		
		
		// insert user
		// $sqlUser = "insert into tb_m_users(user_email, user_group_id, company_id, user_password, full_name, is_active, super_admin, email_confirmed, employee_id, created_dt)
		// values ('" . $register->email . "', '" . $user_group_id . "', '" . $company_id . "', '" . $register->password . "', '" . $register->name . "', '1', '1', '1', NULL, NOW())";
        // 29-Sep-2019
        // jadinya update user yg udah dibuat sebelumnya
        $sqlUser = "update tb_m_users set user_group_id = '" . $user_group_id . "', company_id = '".$company_id."' where user_email = '" . $register->email . "'";
        $this->db->query($sqlUser);

        //insert into company prefferences
        $sqlPreff = "insert into tb_m_company_prefferences(company_id, system_type, system_code, system_value_txt, system_value_num, created_by, created_dt)
		select " . $company_id . ", REPLACE(system_type,'app_init_',''), system_code, system_value_txt, system_value_num, 'system', now() from tb_m_system where SUBSTR(system_type,1,8) = 'app_init'";
        $this->db->query($sqlPreff);
		
		//insert into company prefferences
        $sqlPreff = "
			insert into tb_m_company_prefferences(company_id, system_type, system_code, system_value_txt, system_value_num)
			values
			('" . $company_id . "', 'config_users', 'default', 'Jumlah User Default', " . $default_users . ")
			, ('" . $company_id . "', 'config_users', 'addons', 'Jumlah User Tambahan', '0')
		
		";
        $this->db->query($sqlPreff);        
        
        //insert account , tax, and additional prefferences
        $this->setup_data($company_id);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return $company_id;
        }
    }

    function setup_data($company_id /*, $employee_id*/) 
	{
        $asset = $this->add_account($company_id, '1-0000', 'Asset', 'assets', '0', '0', '0');
        $harta_lancar = $this->add_account($company_id, '1-1000', 'Harta Lancar', 'assets', $asset, '1', '0');
        $harta_tetap = $this->add_account($company_id, '1-2000', 'Harta Tetap', 'assets', $asset, '1', '0');

        $kas = $this->add_account($company_id, '1-1100', 'Kas', 'assets', $harta_lancar, '2', '1', '1');
        
        $kas_pos = $this->add_account($company_id, '1-1110', 'Kas 1', 'assets', $kas, '3', '1', '0');
        //1-1120
        $kas_bank_pos = $this->add_account($company_id, '1-1120', 'Kas 2', 'assets', $kas, '3', '1', '0');

        $this->add_account($company_id, '1-1200', 'Bank', 'assets', $harta_lancar, '2', '1');
        $piutang = $this->add_account($company_id, '1-1300', 'Piutang', 'assets', $harta_lancar, '2', '0', '1');

        // piutang blm ditagih: digunakan sbg default saat bikin pengiriman penjualan
        $piutang_belum_ditagih = $this->add_account($company_id, '1-1299', 'Piutang Belum Ditagih', 'assets', $harta_lancar, '2', '0', '1');
        $ppn_masukan = $this->add_account($company_id, '1-1400', 'PPN Masukan', 'assets', $harta_lancar, '2', '0', '1');
        $Persediaan = $this->add_account($company_id, '1-1500', 'Persediaan Barang', 'assets', $harta_lancar, '2', '0', '1');

        $this->add_account($company_id, '1-2100', 'Peralatan Kantor', 'assets', $harta_tetap, '2', '0');
        $this->add_account($company_id, '1-2200', 'Akumulasi Penyusutan Peralatan', 'assets', $harta_tetap, '2', '0');
        $this->add_account($company_id, '1-2300', 'Sewa Dibayar dimuka', 'assets', $harta_tetap, '2', '0');

        $hutang = $this->add_account($company_id, '2-0000', 'Hutang', 'liability', '0', '0', '0');
        $hutang_usaha = $this->add_account($company_id, '2-1000', 'Hutang Usaha', 'liability', $hutang, '1', '0', '1');
        $this->add_account($company_id, '2-1100', 'Hutang Gaji Karyawan', 'liability', $hutang, '1', '0');
        $this->add_account($company_id, '2-1200', 'Hutang Lainnya', 'liability', $hutang, '1', '0');
        $ppn_keluaran = $this->add_account($company_id, '2-2000', 'PPN Keluaran', 'liability', $hutang, '1', '0', '1');
        $this->add_account($company_id, '2-2100', 'PPH Pasal 21', 'liability', $hutang, '1', '0');
        $this->add_account($company_id, '2-2200', 'PPH Pasal 23', 'liability', $hutang, '1', '0');

        $modal = $this->add_account($company_id, '3-0000', 'Modal', 'equity', '0', '0', '0');
        $saldo_awal = $this->add_account($company_id, '3-2000', 'Saldo Awal', 'equity', $modal, '1', '0', '1');
        $this->add_account($company_id, '3-3000', 'Prive', 'equity', $modal, '1', '0');
        $equitas_saldo_awal = $this->add_account($company_id, '3-3900', 'Ekuitas Saldo Awal', 'equity', $modal, '1', '0', '1');

        $pendapatan = $this->add_account($company_id, '4-0000', 'Pendapatan', 'income', '0', '0', '0');
        $this->add_account($company_id, '4-1000', 'Pendapatan Jasa', 'income', $pendapatan, '1', '0', '1');
        $ppj = $this->add_account($company_id, '4-2000', 'Pendapatan Penjualan', 'income', $pendapatan, '1', '0', '1');
        $potongan_penjualan = $this->add_account($company_id, '4-4000', 'Potongan Penjualan', 'income', $pendapatan, '1', '0', '1');

        // pbd: penjualan belum ditagih
        $penjualan_belum_ditagih = $this->add_account($company_id, '4-3000', 'Penjualan Belum Ditagih', 'income', $pendapatan, '1', '0', '1');

        $biaya = $this->add_account($company_id, '5-0000', 'Biaya', 'expenses', '0', '0', '0');
        $hpp = $this->add_account($company_id, '5-1000', 'Harga Pokok Penjualan', 'expenses', $biaya, '1', '0', '1');
        $this->add_account($company_id, '5-2000', 'Biaya Gaji', 'expenses', $biaya, '1', '0');
        $this->add_account($company_id, '5-3000', 'Biaya Transportasi', 'expenses', $biaya, '1', '0');
        $this->add_account($company_id, '5-4000', 'Biaya Penyusutan & Amortisasi', 'expenses', $biaya, '1', '0');
        $this->add_account($company_id, '5-5000', 'Biaya Sewa', 'expenses', $biaya, '1', '0');
        $this->add_account($company_id, '5-6000', 'Biaya Lainnya', 'expenses', $biaya, '1', '0');

        $sql = "
                insert into tb_m_tax (
                        company_id, tax_name, tax_rate, sales_account_id, purchase_account_id
                        , tax_type, created_by, created_dt, changed_by
                )
                values
                (
                        '" . $company_id . "'
                        , 'PPN 10%', '0.1'
                        , '" . $ppn_keluaran . "'
                        , '" . $ppn_masukan . "'
                        , '1', 'register', now(), 'register'
                )
		";
        $this->db->query($sql);
        $tax_id = $this->db->insert_id();
        
        $sql = "
			INSERT INTO `tb_m_company_prefferences` 
			(`company_id`, `system_type`, `system_code`, `system_value_txt`, `system_value_num`) 
			VALUES
			  ('" . $company_id . "', 'config_sales', 'default_ar_id', '" . $piutang . "', '" . $piutang . "')
			, ('" . $company_id . "', 'config_sales', 'default_ap_id', '" . $hutang_usaha . "', '" . $hutang_usaha . "')
			, ('" . $company_id . "', 'config_sales', 'default_balance_id', 'Ekuitas Saldo Awal', '" . $equitas_saldo_awal . "')
            , ('" . $company_id . "', 'config_sales', 'default_pos_tax_id', '', " . $tax_id . ")
			, ('" . $company_id . "', 'config_sales', 'default_pos_outcome_id', '', " . $ppn_keluaran . ")
			, ('" . $company_id . "', 'config_sales', 'default_pos_cash_id', '', " . $kas_pos . ")
			, ('" . $company_id . "', 'config_sales', 'default_pos_cashbank_id', '', " . $kas_bank_pos . ")
			, ('" . $company_id . "', 'config_sales', 'default_sales_discount', '', " . $potongan_penjualan . ")
			, ('" . $company_id . "', 'config_sales', 'default_inventory', 'Persediaan Barang', '" . $Persediaan . "')            
			                
			, ('" . $company_id . "', 'config_purchase', 'default_ar_id', '" . $piutang . "', '" . $piutang . "')
			, ('" . $company_id . "', 'config_purchase', 'default_ap_id', '" . $hutang_usaha . "', '" . $hutang_usaha . "')
			, ('" . $company_id . "', 'config_purchase', 'default_balance_id', 'Ekuitas Saldo Awal', '" . $equitas_saldo_awal . "')			
			, ('" . $company_id . "', 'fix_value', 'ppj', " . $ppj . ", " . $ppj . ")
			, ('" . $company_id . "', 'fix_value', 'hpp', " . $hpp . ", " . $hpp . ")
			, ('" . $company_id . "', 'fix_value', 'piutang_belum_ditagih', " . $piutang_belum_ditagih . ", " . $piutang_belum_ditagih . ")
			, ('" . $company_id . "', 'fix_value', 'penjualan_belum_ditagih', " . $penjualan_belum_ditagih . ", " . $penjualan_belum_ditagih . ")
            , ('" . $company_id . "', 'fix_value', 'ppn_masukan', 'PPN Masukan', " . $ppn_masukan . ")
			, ('" . $company_id . "', 'fix_value', 'ppn_keluaran', 'PPN Keluaran', " . $ppn_keluaran . ")
			
			, ('" . $company_id . "', 'fix_value', 'saldo_awal_account_id', 'Saldo Awal', " . $saldo_awal . ")
			;
		";
        $this->db->query($sql);
        
        //insert default customer 
        $sqlCustomer = "insert into tb_m_customer(company_id, customer_code, customer_name, customer_address, customer_address_billing
            , customer_city, customer_prov, customer_zip_code, customer_country, customer_phone1, customer_phone2, customer_phone3, npwp_number
            , npwp_address, ar_id, ap_id, default_term, opening_balance, opening_dt, ar_remaining, pos_flag, created_by, created_dt)
            SELECT " . $company_id . ", 'DC', 'Default Customer', 'POSC', 'POSC' "
                . ", '', '', '', '', '', '', '', ''"
                . ", '', '".$piutang."', '', '', '', NULL, '', '1','register', now()";
        $this->db->query($sqlCustomer);		
    }
	
	function add_account($company_id, $account_code, $account_name, $account_type, $account_parent, $level, $account_cashbank, $account_lock = '0') 
	{
        $sql = "
			insert into tb_m_account (
					company_id, account_code, account_name, account_type, account_parent, level, account_cashbank
					, account_lock
					, created_by, created_dt, changed_by
			)
			values
			(
					'" . $company_id . "'
					, '" . $account_code . "', '" . $account_name . "', '" . $account_type . "', '" . $account_parent . "', '" . $level . "', '" . $account_cashbank . "'
					, '" . $account_lock . "'
					, 'register', now(), 'register'
			)
		";
        $this->db->query($sql);
        return $this->db->insert_id();
    }

	
	/* ------ */

    function getDataPackage() 
	{
        $this->db->select('package_id, package_name, package_price');
        $this->db->from('tb_m_package');
        return $this->db->get()->result();
    }

    function getDataPackageDetail($id) {
        $sql = "SELECT 
                    pd.package_id, pd.`code`, pd.type, pd.display_flag
                    , f.function_parent
                    , (case when f.function_parent = 0 then pd.`code` else function_parent end) as parent 
                    , group_concat(concat(pd.description) ORDER BY display_flag desc, f.function_order asc SEPARATOR ' , ') AS item
                FROM tb_m_package_detail pd
                LEFT JOIN tb_m_function f on pd.`code` = f.function_id
                where pd.package_id = " . $id . "
                GROUP BY parent
                ORDER BY  parent asc, pd.`code` asc";
        return $this->db->query($sql)->result();
    }    

    function update_register($activation_code, $sts) {
        $this->db->where('activation_code', $activation_code);
        $this->db->update('tb_r_register', array('activation' => $sts));
        return $this->db->affected_rows();
    }

    
    
    
    function getSystem($type = "", $code = "", $field ="") {

        $this->db->select('*');
        $this->db->from('tb_m_system');
        if($type != ''){
            $this->db->where('system_type', $type);
        }
        if($code != ''){
            $this->db->where('system_code', $code);
        }
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row()->$field;
        } else {
            return false;
        }
    }
    
    

    

}
