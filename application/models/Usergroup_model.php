<?php
/*
* @author: irfan@arkamaya.co.id
* @created: 2017-01-23
*/
class Usergroup_model extends CI_Model 
{
	function __construct()
	{
		parent:: __construct();
	}
	
	function get_user_groups($start = '', $length = '', $order_by = 'user_group_description asc')
	{			
		$sql = "
			select 
				*
			from
				tb_m_user_group
			where
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
		";
		$sql .= ' order by ' . $order_by;
		
		if ($start != '' && $length != '')
		{
			$sql .= ' limit ' . $start . ', ' . $length;
		}
		
		return $this->db->query($sql)->result();
    }
	
	function get_user_group_count($order_by = 'user_group_description asc')
	{		
		$sql = "
			select 
				count(user_group_id) as cnt
			from
				tb_m_user_group
			where
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
		";
		
		$sql .= ' order by ' . $order_by;
		
		return $this->db->query($sql)->row()->cnt;
    }
	
	function get_user_group($user_group_id)
	{	
			
		$sql = "
			select 
				*
			from
				tb_m_user_group
			where
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and user_group_id = '" . $user_group_id . "'
		";				
		
		return $this->db->query($sql)->result();
    }
	
	function getData($start = '', $length = '', $keyword = '') {
        $this->db->select('ug.user_group_id, ug.user_group_description, ug.is_admin, ug.default_landing, f.function_name');
        $this->db->from('tb_m_user_group ug');
        $this->db->join('tb_m_function f', "default_landing != '' and ug.default_landing = f.function_controller" , 'left');
        if ($keyword != ''){
            $this->db->like('user_group_description', $keyword);
        }
        $this->db->where('ug.company_id', $this->session->userdata(S_COMPANY_ID));
        $this->db->order_by('user_group_description');


        if ($start >= 0 && $length >= 0) {
            $this->db->limit($length, $start);
        }
        return $this->db->get()->result();
    }

    function getDataById($id) {
        $this->db->select('ug.user_group_id, ug.user_group_description, ug.is_admin, ug.default_landing, count(e.employee_id) as jmlh_pegawai, f.function_name');
        $this->db->from('tb_m_user_group ug');
        $this->db->where('ug.company_id', $this->session->userdata(S_COMPANY_ID));
		$this->db->join('tb_m_users u', 'u.user_group_id = ug.user_group_id', 'inner');
        $this->db->join('tb_m_employee e', 'e.user_email = u.user_email', 'left');
        $this->db->join('tb_m_function f', "ug.default_landing != '' and ug.default_landing = f.function_controller" , 'left');
        $this->db->where('ug.user_group_id', $id);
        //return $this->db->get()->result();
        return $this->db->get()->row();
    }

    function countData($keyword = '') {
        $this->db->select('user_group_id');
        $this->db->from('tb_m_user_group');
        $this->db->like('user_group_description', $keyword);
        $this->db->where('company_id', $this->session->userdata(S_COMPANY_ID));
        return $this->db->count_all_results();
    }

    function saveData($mode, $data, $id = "") {
        if ($mode == 'insert') {
            // insert user group
            $this->db->insert('tb_m_user_group', $data);

            //$this->insert_details($this->db->insert_id());
            return $this->db->insert_id();
        } else {
            // update user group
            $this->db->where('user_group_id', $id);
            $this->db->where('company_id', $this->session->userdata(S_COMPANY_ID));
            $this->db->update('tb_m_user_group', $data);

            // delete items
            //$this->db->where('role_id', $role_id);		
            //$this->db->delete('tb_m_role_salary');		
            // insert again
            //$this->insert_details($role_id);
            return $this->db->affected_rows();
        }
    }

    function delete($user_group_id) {
        
        $this->db->where('user_group_id', $user_group_id);
        $this->db->delete('tb_m_user_group_auth');

        $this->db->where('user_group_id', $user_group_id);
        $this->db->where('company_id', $this->session->userdata(S_COMPANY_ID));
        $this->db->delete('tb_m_user_group');
    }

    function updateUsergroupAuth($data, $usergroup_id, $function_id) {
        //delete from  tb_m_user_group_auth
        $this->db->where('user_group_id', $usergroup_id)->where('function_id', $function_id)->delete('tb_m_user_group_auth');
        //insert data
        if ($data) {
            $this->db->insert_batch('tb_m_user_group_auth', $data);
        }
        return $this->db->affected_rows();
    }

    function getFunctionList($function_id = '') {
        $this->db->select('f.function_id, f.function_parent, f.function_name, f.function_name_id, f.function_active, f.function_controller , case when f.function_parent = 0 then f.function_id else f.function_parent end as sort', false);
        $this->db->from('tb_m_function f');
        $this->db->join('tb_m_company_function cf', 'f.function_id = cf.function_id and cf.company_id =' . $this->session->userdata(S_COMPANY_ID), 'inner');
        //$this->db->where('f.function_active', 1);
        if ($function_id != '') {
            $this->db->where('f.function_i', $function_id);
        }

        if ($this->session->userdata(S_IS_ADMIN) == '1') {
            $this->db->where('f.function_controller != "user_group"');
        }

//        else{
//            $this->db->where('f.function_parent is null');
//            $this->db->or_where('f.function_parent', 0);
//        }
        $this->db->order_by('sort, function_id');
        return $this->db->get()->result();
        //$this->output->enable_profiler(true);
    }

    function getFeatureList($function_id, $usergroup, $type) {
        $_join = ($type == "view") ? "inner" : "left";
        $this->db->select('ff.feature_id, ff.function_id, ff.feature_name, ff.feature_element_type, ff.feature_description, case when uga.user_group_id is not null then 1 else 0 end as is_used', false);
        $this->db->from('tb_m_function_feature ff');
        $this->db->join('tb_m_user_group_auth uga', 'ff.feature_id = uga.feature_id and ff.function_id = uga.function_id  and uga.user_group_id = ' . $usergroup, $_join);
		$this->db->where('ff.function_id', $function_id);
		$this->db->order_by('ff.feature_element_type desc, ff.feature_name');
        return $this->db->get()->result();
    }
}
?>