<?php

class Report_driver_model extends CI_Model
{
    function __construct()
	{
        parent:: __construct();
    }

    function getRollcall(
        $start, $length, $sv, $order, $columns, $departure_dt_s, $departure_dt_e
    )
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }

        $sql = "
        SELECT
        	a.rollcall_id, a.voucher, a.driver_cd, b.driver_name, a.vehicle_cd, a.route, a.cycle , a.departure_plan
        FROM
        	tb_r_rollcall a
        	INNER JOIN tb_m_driver b ON b.driver_cd = a.driver_cd
			where
				      (
                    a.rollcall_id like '%" . $sv . "%'
                    or a.voucher like '%" . $sv . "%'
                    or a.driver_cd like '%" . $sv . "%'
                    or b.driver_name like '%" . $sv . "%'
                    or a.vehicle_cd like '%" . $sv . "%'
                    or a.route like '%" . $sv . "%'
                    or a.cycle like '%" . $sv . "%'
                    or a.departure_plan like '%" . $sv . "%'
                )
        AND a.departure_dt between '".date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_s)))."'  AND '".date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_e)))."'
        order by " . $order_by . " ";

		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
		return $this->db->query($sql)->result();
    }

    function getCountOfRollcall(
          $departure_dt_s, $departure_dt_e, $sv
    )
    {

        $sql = "
        SELECT
          a.rollcall_id, a.voucher, a.driver_cd, b.driver_name, a.vehicle_cd, a.route, a.cycle , a.departure_plan
        FROM
          tb_r_rollcall a
          INNER JOIN tb_m_driver b ON b.driver_cd = a.driver_cd
        where
          (
            a.rollcall_id like '%" . $sv . "%'
            or a.voucher like '%" . $sv . "%'
            or a.driver_cd like '%" . $sv . "%'
            or b.driver_name like '%" . $sv . "%'
            or a.vehicle_cd like '%" . $sv . "%'
            or a.route like '%" . $sv . "%'
            or a.cycle like '%" . $sv . "%'
            or a.departure_plan like '%" . $sv . "%'
                  )
        AND a.departure_dt between '".date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_s)))."'  AND '".date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_e)))."'
        ";


        return $this->db->query($sql)->num_rows();
    }

}
