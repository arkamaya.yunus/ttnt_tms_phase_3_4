<?php
/*
* @author: Irfan Satriadarma
* @created: 28 April 2020
*/

class Customer_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
    }

    function getCustomers($start, $length, $sv, $order, $columns)
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {            
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }

        $sql = "
			select 
                a.system_code as CustomerCode,
                a.system_value_txt as CustomerName
			from 
				tb_m_system a
            where
                a.system_type = 'customer'
                and
				(
                    a.system_code like '%" . $sv . "%'
                    or a.system_value_txt like '%" . $sv . "%'
                )                
            order by " . $order_by . " ";
            
		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
		return $this->db->query($sql)->result();
    }

    function getCountOfCustomer($sv)
    {
        $sql = "
			select 
                count(a.system_code) as cnt
            from 
				tb_m_system a
            where
                a.system_type = 'customer'
                and
				(
                    a.system_code like '%" . $sv . "%'
                    or a.system_value_txt like '%" . $sv . "%'
                )                
		";
		return $this->db->query($sql)->row()->cnt;
    }

}