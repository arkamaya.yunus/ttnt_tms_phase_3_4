<?php
/*
* @author: eri.safari@arkamaya.co.id
* @created: 2019-08-07
*
* # TODO
* - Handle query Company	 (Create,update)
* - Handle query Sales
* - Handle query Purchase
* - Handle query Items
* - Handle query Account
* - Handle query Users
* - Handle query Billing
* -
*/

class Settings_model extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->load->model('company_model');
    }

    /*
    * @author: eri.safari@arkamaya.co.id
    * @created: 2019-08-07
    * # TODO
    *  Get Company By id_company login
    */

    function get_company_id(){
        $sql = "
            select * from tb_m_company where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
        ";

        return $this->db->query($sql)->row();
    }

    function save_company($nama_files){

        $company_logo = $this->input->post('logo');
        $company = array(
            'company_name'      => $this->input->post('company_name',true),
            'address'           => $this->input->post('address',true),
            'company_industry'  => $this->input->post('company_industry',true),
            'email'             => $this->input->post('email',true),
            'phone'             => $this->input->post('phone',true),
            'fax'               => $this->input->post('fax',true),
            'tax_number'        => $this->input->post('tax_number',true),
            'website'        => $this->input->post('company_website',true),
        );

        $company_id = '';
        if(
            $this->input->post('company_id',true) == '' ||
            $this->input->post('company_id',true) == null
        ){
            //$nama_files = $this->_do_upload('logo');
            $imageurl = './assets/files/cid' . $this->session->userdata(S_COMPANY_ID) . "/profile/" . $nama_files;
            $company['logo']       = $imageurl;
            $company['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
            $company['created_dt'] = date('Y-m-d H:i:s');
            $this->db->insert('tb_m_company',$company);
            $company_id = $this->db->insert_id();
        }else{
            $company_id = $this->input->post('company_id',true);
            $file_tmp   = $this->input->post('file_tmp',true);
            $delete = $this->input->post('delete_logo');
            if($delete == 1){
                if($this->input->post('file_tmp',true) != ""){
                    if(file_exists("./".$file_tmp)){
                        unlink("./" .$file_tmp);
                    }
                }
                $company['logo']        = "";
            }else{
                if($_FILES['logo']['size'] > 0){
                    if($this->input->post('file_tmp',true) != ""){
                        if(file_exists("./".$file_tmp)){
                            unlink("./" .$file_tmp);
                        }
                    }
                    //$nama_files = $this->_do_upload('logo');
                    $imageurl = './assets/files/cid' . $this->session->userdata(S_COMPANY_ID) . "/profile/" . $nama_files;
                }else{
                    $imageurl = $file_tmp;
                }
                $company['logo']        = $imageurl;
            }


			$company['changed_by']	= $this->session->userdata(S_EMPLOYEE_NAME);
			$company['changed_dt']	= date('Y-m-d H:i:s');

			$this->db->where('company_id',$company_id);
            $this->db->update('tb_m_company',$company);

        }

        return $company_id;


    }

    function save_sales(){
        $requisite_sales = $this->input->post('requisite_sales');
        $income_sales = $this->input->post('income_sales');
        $sales_discount = $this->input->post('sales_discount');
        $prefix_quotation = $this->input->post('prefix_quotation');
        $prefix_order = $this->input->post('prefix_order');
        $prefix_delivery = $this->input->post('prefix_delivery');
        $prefix_invoice = $this->input->post('prefix_invoice');
        $prefix_sales_return = $this->input->post('prefix_sales_return');

        $get_requisite_sales = $this->get_company_preff('config_sales', 'default_invoice_terms');
        $get_income_sales = $this->get_company_preff('fix_value', 'ppj');
        $get_discount_sales = $this->get_company_preff('config_sales', 'default_sales_discount');
        $get_prefix_quotation = $this->get_company_preff('config_sales', 'pre_sales_quotation');
        $get_prefix_order = $this->get_company_preff('config_sales', 'pre_sales_order');
        $get_prefix_delivery = $this->get_company_preff('config_sales', 'pre_sales_delivery');
        $get_prefix_invoice = $this->get_company_preff('config_sales', 'pre_sales_invoice');
        $get_prefix_sales_return = $this->get_company_preff('config_sales', 'pre_sales_return');
        // Begin transaction //
        $this->db->trans_begin();



        // update tabel tb_m_company_prefferences by Syarat penjualan utama
        $where_requisite_sales = array(
            'preff_id' => $get_requisite_sales[0]['preff_id']
        );

        $data_requisite_sales['system_value_txt'] = $requisite_sales;
        $this->db
             ->where($where_requisite_sales)
             ->update('tb_m_company_prefferences',$data_requisite_sales);


        // update tabel tb_m_company_prefferences by Pendapatan Penjualan
        $where_income_sales = array(
            'preff_id' => $get_income_sales[0]['preff_id']
        );

        $data_income_sales['system_value_num'] = $income_sales;


        $this->db
                ->where($where_income_sales)
                ->update('tb_m_company_prefferences',$data_income_sales);

        // update tabel tb_m_company_prefferences by Diskon Penjualan
        $where_sales_discount = array(
            'preff_id' => $get_discount_sales[0]['preff_id']
        );

        $data_sales_discount['system_value_num'] = $sales_discount;


        $this->db
                ->where($where_sales_discount)
                ->update('tb_m_company_prefferences',$data_sales_discount);


         // update tabel tb_m_company_prefferences by Prefix Penawaran
         $where_prefix_quotation = array(
            'preff_id' => $get_prefix_quotation[0]['preff_id']
        );

        $data_prefix_quotation['system_value_txt'] = $prefix_quotation;


        $this->db
                ->where($where_prefix_quotation)
                ->update('tb_m_company_prefferences',$data_prefix_quotation);

        // update tabel tb_m_company_prefferences by Prefix Pemesanan
        $where_prefix_order = array(
            'preff_id' => $get_prefix_order[0]['preff_id']
        );

        $data_prefix_order['system_value_txt'] = $prefix_order;


        $this->db
                ->where($where_prefix_order)
                ->update('tb_m_company_prefferences',$data_prefix_order);

        // update tabel tb_m_company_prefferences by Prefix Pengiriman
        $where_prefix_delivery = array(
            'preff_id' => $get_prefix_delivery[0]['preff_id']
        );

        $data_prefix_delivery['system_value_txt'] = $prefix_delivery;


        $this->db
                ->where($where_prefix_delivery)
                ->update('tb_m_company_prefferences',$data_prefix_delivery);

        // update tabel tb_m_company_prefferences by Prefix Penagihan
        $where_prefix_invoice = array(
            'preff_id' => $get_prefix_invoice[0]['preff_id']
        );

        $data_prefix_invoice['system_value_txt'] = $prefix_invoice;


        $this->db
                ->where($where_prefix_invoice)
                ->update('tb_m_company_prefferences',$data_prefix_invoice);


        // update tabel tb_m_company_prefferences by Prefix Retur
        $where_prefix_sales_return = array(
            'preff_id' => $get_prefix_sales_return[0]['preff_id']
        );

        $data_prefix_sales_return['system_value_txt'] = $prefix_sales_return;


        $this->db
                ->where($where_prefix_sales_return)
                ->update('tb_m_company_prefferences',$data_prefix_sales_return);


        $this->db->trans_complete();
        // End transaction //
        if ($this->db->trans_status() === FALSE) {
            # Something went wrong.
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            # Everything is Perfect.
            # Committing data to the database.
            $this->db->trans_commit();
            return TRUE;
        }
    }

    function get_company_preff($system_type, $system_code = '', $order_by = '')
	{
		$sql = "
			select
                preff_id,company_id,system_type,system_code,system_value_num,system_value_txt
			from
				tb_m_company_prefferences
			where
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
		";


		$sql .= ($system_type != '') ? " and system_type = '" . $system_type . "'" : '';
		$sql .= ($system_code != '') ? " and system_code = '" . $system_code . "'" : '';

		$sql .= ($order_by != '') ? " order by " . $order_by : '';

		return  $this->db->query($sql)->result_array();
    }

    function get_income_sales(){
        $sql = "
                SELECT
                    account_id, account_name
                from
                    tb_m_account
                where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                ORDER BY account_name
                AND account_type = 'income' DESC
        ";

        return $this->db->query($sql)->result_array();
    }

    function get_discount_sales(){
        $sql = "
                SELECT
                    account_id, account_name
                from
                    tb_m_account
                where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                and account_type in ('income','expenses')
                ORDER BY account_name
        ";

        return $this->db->query($sql)->result_array();
    }

    function menu_pengaturan(){
        $cid = ($this->session->userdata(S_COMPANY_ID)) ? $this->session->userdata(S_COMPANY_ID) : 0;
        $groupId = ($this->session->userdata(S_USER_GROUP_ID)) ? $this->session->userdata(S_USER_GROUP_ID) : 0;

        $sql = "
                select
                f.function_id
                , function_icon
                , f.function_name
                , f.function_controller
                , f.function_link_visible
            from
                tb_m_user_group_auth uga
                inner join tb_m_user_group ug on ug.user_group_id = uga.user_group_id
                inner join tb_m_function f on f.function_id = uga.function_id
            where ug.company_id = '". $cid ."' and f.function_link_visible = 0 and f.function_active = 1
                    and f.app_id = 1 and ug.user_group_id = '" . $groupId . "'
            ORDER BY f.function_order
        ";

        $query = $this->db->query($sql);
        if($query->num_rows()){
            return $query->result_array();
        }else{
            return false;
        }

    }

    function create_menu_pengaturan($url)
    {
        $list = $this->menu_pengaturan();
        //$html = '';
        $html = "<ul>";
        foreach ($list as $row)
        {
            if($url == $row['function_controller']){
                $html .= '<li class="active">'
                    .'<a href="'.base_url().$row['function_controller'].'" class="waves-effect">'
                    .' <span>'.$row['function_name'].'</span></a>'
                    .'</li>';
            }else{
                $html .= '<li>'
                    .'<a href="'.base_url().$row['function_controller'].'" class="waves-effect">'
                    .' <span>'.$row['function_name'].'</span></a>'
                    .'</li>';
            }

        }
        $html .= '</ul>';
        return $html;
    }

    public function getMasters($type,$cd='')
    {
        $sql = "select
                    system_code,
                    system_value_txt,
                    system_value_num,
                    system_value_time
                FROM tb_m_system
                where system_type = '" . $type . "'";

        if($cd != null){
            $sql = $sql ."and system_code ='" . $cd . "'";
            return $this->db->query($sql)->row();
        }else {
            return $this->db->query($sql)->result();
        }
    }

    public function checkDuplicateIp($ip) {
      $this->db->where('system_type', 'attendance');
      $this->db->where('system_code', $ip);
      $query = $this->db->get('tb_m_system');
      $count_row = $query->num_rows();
      return $count_row;
      // if ($count_row > 0) {
      //   //if duplicate found
      //     return FALSE;
      //     // echo 'duplicate = 0';
      //  } else {
      //     return TRUE;
      //     // echo 'duplicate = 1';
      //  }
     }

}
