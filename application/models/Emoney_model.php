<?php
/*
* @author: Arka.Gamal
* @created: 12 Mei 2020
*/

class Emoney_model extends CI_Model
{
	function __construct()
	{
        parent:: __construct();
    }

		public function getMasters($type,$cd='')
		{
				$sql = "select
										system_code,
										system_value_txt,
										system_value_num,
										system_value_time
								FROM tb_m_system
								where system_type = '" . $type . "'";

				if($cd != null){
						$sql = $sql ."and system_code ='" . $cd . "'";
						return $this->db->query($sql)->row();
				}else {
						return $this->db->query($sql)->result();
				}
		}

		function checkTrans($emoney_id){
			$sql ="
						select count(emoney_trans_id) as cnt from tb_r_emoney where md5(emoney_id) = '".$emoney_id."'
			";
			return $this->db->query($sql)->row();
		}

		function getRoutes($keyword = '')
		{
				$sql = "select distinct route
								from tb_m_route
								order by route";
				return $this->db->query($sql)->result();
		}

		function getHistoryById($emoney_id){
			$sql = "SELECT *
							FROM tb_r_emoney
							WHERE md5(emoney_id) = '". $emoney_id . "'
							ORDER BY transaction_dt
							";
							return $this->db->query($sql)->result();
		}

    function getEmoneys($route, $customer, $category, $start, $length, $sv, $order, $columns)
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }

        $where_customer = ($customer != 'all') ? " and c.customer = '" . $customer . "'" : '';
        $where_route = ($route != 'all') ? " and b.route = '" . $route . "'" : '';
        $where_category = ($category != 'all') ? " and a.emoney_ctg = '" . $category . "'" : '';
        $sql = "
	            select
	                a.emoney_id, a.bank, a.balance, e.system_value_txt as emoney_ctg, b.route, b.cycle, b.gel, c.customer, d.system_value_txt as cust_name
							from
								tb_m_emoney a
								left join tb_m_route_emoney b ON a.emoney_id = b.emoney_id
								left join tb_m_route c ON c.route = b.route AND c.cycle = b.cycle
								left join tb_m_system d ON c.customer = d.system_code
								left join tb_m_system e ON a.emoney_ctg = e.system_code
							where
								(
                    a.emoney_id like '%" . $sv . "%'
                    or a.bank like '%" . $sv . "%'
                    or a.balance like '%" . $sv . "%'
										or a.emoney_ctg like '%" . $sv . "%'
										or b.route like '%" . $sv . "%'
										or b.cycle like '%" . $sv . "%'
										or b.gel like '%" . $sv . "%'
										or c.customer like '%" . $sv . "%'
                )
                " . $where_route . $where_customer . $where_category . "
            order by " . $order_by . " ";

		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
    }
		return $this->db->query($sql)->result();
		//end of getEmoneys
    }

    function getCountOfEmoney($route, $customer, $category, $sv)
    {
        $where_route = ($route != 'all') ? " and c.customer = '" . $route . "'" : '';
        $where_customer = ($customer != 'all') ? " and b.route = '" . $customer . "'" : '';
				$where_category = ($category != 'all') ? " and a.emoney_ctg = '" . $category. "'" : '';

        $sql = "
				select
						count(a.emoney_id) as cnt
				from
					tb_m_emoney a
					left join tb_m_route_emoney b ON a.emoney_id = b.emoney_id
					left join tb_m_route c ON c.route = b.route AND c.cycle = b.cycle
					left join tb_m_system d ON c.customer = d.system_code
					left join tb_m_system e ON a.emoney_ctg = e.system_code
			where
				(
						a.emoney_id like '%" . $sv . "%'
						or a.bank like '%" . $sv . "%'
						or a.balance like '%" . $sv . "%'
						or a.emoney_ctg like '%" . $sv . "%'
						or b.route like '%" . $sv . "%'
						or b.cycle like '%" . $sv . "%'
						or b.gel like '%" . $sv . "%'
						or c.customer like '%" . $sv . "%'
                )
                " . $where_route . $where_customer . $where_category . "
		";
		return $this->db->query($sql)->row()->cnt;
    }

    function getEmoney($emoney_id)
    {
        $sql = "
						select
								a.emoney_id, a.bank, a.balance, a.emoney_ctg as emoney_ctg_raw, b.route, b.cycle, b.gel, c.customer, d.system_value_txt as cust_name, e.system_value_txt as emoney_ctg
						from
							tb_m_emoney a
						left join tb_m_route_emoney b ON a.emoney_id = b.emoney_id
						left join tb_m_route c ON c.route = b.route AND c.cycle = b.cycle
						left join tb_m_system d ON c.customer = d.system_code
						left join tb_m_system e ON a.emoney_ctg = e.system_code
            where
                md5(a.emoney_id) = '" .$emoney_id . "'
        ";
        return $this->db->query($sql)->result();
    }

    function delete($emoney_id)
    {
        $this->db->where('md5(emoney_id)', $emoney_id);
        $this->db->delete('tb_m_emoney');
				return $this->db->affected_rows();
    }

    function save($update = '')
    {
        // validate vehicle_cd
        $check_lp = $this->getEmoney(md5($this->input->post('emoney_id', true)));
        if (count($check_lp) == 0 || $update == '1')
        {
            $lp = array
            (
                'emoney_id' => $this->input->post('emoney_id', true),
                'bank' => $this->input->post('bank', true),
								'balance' => filter_var($this->input->post('balance', true), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_THOUSAND),
								'emoney_ctg' => $this->input->post('category', true)
            );

            if ($update == '')
            {
                $lp['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
                $lp['created_dt'] = date('Y-m-d H:i:s');

                $this->db->insert('tb_m_emoney', $lp);

                return '1';
            }
            else
            {
                $lp['changed_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
                $lp['changed_dt'] = date('Y-m-d H:i:s');

                $this->db->where('emoney_id', $this->input->post('emoney_id', true));
                $this->db->update('tb_m_emoney', $lp);

                return '2'; // update success
            }
        }
        else
        {
            return '0'; // vehicle_number already exists
        }
    }

    function getEmoneysForDownload()
    {
        $sql = "
					select
							a.emoney_id, a.bank, a.balance, a.emoney_ctg
					from
						tb_m_emoney a
        ";
        return $this->db->query($sql)->result();
    }
}
