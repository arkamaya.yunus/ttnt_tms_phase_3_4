<?php

/*
 * @author: misbah@arkamaya.co.id
 * @created: 2017-03-08
 */

class Setting_model extends CI_Model {

    function __construct() {
        parent:: __construct();
    }

    function getCompany() {
        $this->db->select("
			com.company_id, com.user_email, com.company_name, com.company_industry,
			com.logo, com.show_logo_in_report, com.address, com.shipping_address,
			com.phone, com.fax, com.tax_number, com.website, com.email, com.currency,
			com.default_language, com.bank_name, com.bank_branch, com.bank_address,
			com.bank_account_number, com.bank_account_name, com.swift_code,
			com.accounting_period_start, com.accounting_period_end,
			com.sales_open_invoices, com.sales_overdue_invoices,
			com.sales_invoices_paid_last30, com.trial_expired,
			com.activation_code, com.activation_expired, com.package_id,
			datediff(com.trial_expired, date_format(now(), '%Y-%m-%d')) as left_day,
			date_format(com.trial_expired, '%d %M %Y') as trial_expired_date");
        $this->db->from('tb_m_company com');
        $this->db->where('com.company_id', $this->session->userdata(S_COMPANY_ID));
        $query = $this->db->get();
        if ($query->num_rows() == 1){
            return $query->row();
        }else{
            return false;
        }
        
    }

    function getCompanyPreff(){
        $this->db->select("system_type, system_code, system_value_txt as value_txt, system_value_num as value_num, 
		(select acc.account_type from tb_m_account as acc where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "' and account_id = system_value_txt) as account_type");
        $this->db->from('tb_m_company_prefferences');
        $this->db->where('company_id', $this->session->userdata(S_COMPANY_ID));
        $this->db->order_by('system_type', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
    }
    
    function getAccount(){
        $this->db->select('account_id, account_name, account_type');
        $this->db->from('tb_m_account');
        $this->db->where('company_id', $this->session->userdata(S_COMPANY_ID));
        $this->db->order_by('account_type', 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
    }
	
	//added by  nanin mulyani 20170317
	function getPackage(){
		$this->db->select('pack.package_id, pack.package_name, pack.package_price, pack.package_period, pack.package_description');
        $this->db->from('tb_m_package pack');
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
	}
	
	function getCompanyPayment(){
		$this->db->select('com_pay.payment_id, com_pay.payment_dt, com_pay.company_id, com_pay.package_id,
			com_pay.package_month_payment, com_pay.period_dt_from, com_pay.period_dt_to,
			com_pay.amount_total, com_pay.payment_metod, com_pay.payment_flag,
			com_pay.payment_from_name, com_pay.payment_from_bank, com_pay.payment_from_bank_acc,
			com_pay.payment_confirm_dt, com_pay.payment_ok_by, com_pay.payment_ok_dt,
			com_pay.package_cost, com_pay.user_additional, com_pay.user_additional_cost,
			com_pay.unique_code, com_pay.payment_to_code,
			com_pay.payment_amount, sys.system_value_txt as payment_to_desc');
        $this->db->from('tb_r_company_payment as com_pay');
        $this->db->join("tb_m_system as sys", "com_pay.payment_to_code = sys.system_code and sys.system_type = 'bank_account'", "inner");
        $this->db->where('com_pay.company_id', $this->session->userdata(S_COMPANY_ID));
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
	}
	
	function getConfigPayment($system_type){
		$sql = "
			select 
				m_sys.system_type, m_sys.system_code, m_sys.system_value_txt, m_sys.system_value_num
			from tb_m_company_prefferences m_sys
			where
				m_sys.system_type = '$system_type'
				and m_sys.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
		";
		
		$query = $this->db->query($sql);
        if ($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
	}
	
	function getdata_package($package_id){
		$sql = "
			select 
				pack.package_id, pack.package_name, pack.package_price, pack.package_period, pack.package_description,
				pack.created_by, pack.created_dt, pack.changed_by, pack.changed_dt, pack.default_landing, pack.default_users,
				(select system_value_num from tb_m_system where system_type = 'price' and system_code = 'users') as additional_user_price
			from 
				tb_m_package pack
			where
				package_id = '$package_id'
		";
		
		return $this->db->query($sql)->row();
	}
	//end
		
    function updateCompany($data){
        $this->db->where('company_id', $this->session->userdata(S_COMPANY_ID));
        $this->db->update('tb_m_company', $data);
        return $this->db->affected_rows();
    }
	
	//added by nanin mulyani 20170320
	function getPackage_company(){
		$sql = "
			select
				pack.package_id, pack.package_name, pack.package_price, pack.package_period,
				pack.package_description, pack.default_landing, pack.default_users,
				com.company_id, com.company_name
			from 
				tb_m_package pack
				left join tb_m_company com
				on pack.package_id = com.package_id
			where
				com.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
		";
		
		return $this->db->query($sql)->result_array();
	}
	
	function getTax(){
		$this->db->select('tax.tax_id, tax.company_id, tax.tax_name, tax.tax_rate, tax.sales_account_id, tax.purchase_account_id');
        $this->db->from('tb_m_tax as tax');
        $this->db->where('tax.company_id', $this->session->userdata(S_COMPANY_ID));
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return false;
        }
	}
	
	function insert_ComPayment($insert_comPay){
		$this->db->insert('tb_r_company_payment', $insert_comPay);
	}
	
	function update_ComPayment($update_comPay, $payment_id){
		$this->db->where('payment_id', $payment_id);
		$this->db->where('company_id', $this->session->userdata(S_COMPANY_ID));
		$this->db->update('tb_r_company_payment', $update_comPay);
	}
	//end
	
	//added by nanin mulyani 20170324
	function getUsers(){
		$sql = "
			select
				users.user_email, users.user_group_id, users.company_id, users.user_photo, users.full_name,
				users.is_active, users.user_last_logged_in, users.super_admin, users.email_confirmed,
				users.email_confirmed_code
			from tb_m_users as users
			where
				users.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
		";
		
		return $this->db->query($sql)->result_array();
	}
	
	function get_data_payment_transaction($start = '', $length = '', $date_from, $date_to){
		$sql = "
			select
				com_pay.payment_id, com_pay.payment_dt, com_pay.company_id, com_pay.package_id,
				com_pay.package_month_payment, com_pay.period_dt_from, com_pay.period_dt_to,
				com_pay.payment_metod, com_pay.payment_flag, com_pay.payment_from_name, 
				com_pay.payment_from_bank, com_pay.payment_from_bank_acc,
				com_pay.payment_confirm_dt, com_pay.payment_ok_by, com_pay.payment_ok_dt,
				com_pay.package_cost, com_pay.user_additional, com_pay.user_additional_cost,
				com_pay.amount_total, com_pay.unique_code, com_pay.payment_to_code,
				com_pay.payment_amount, sys.system_value_txt as payment_to_desc,
				com.company_name, com.company_industry, com.logo,
				pack.package_name, pack.package_period, pack.package_description,
				pack.default_landing, pack.default_users, com.email,
				com.trial_expired
			from 
				tb_r_company_payment as com_pay
				left join tb_m_system as sys
				on com_pay.payment_to_code = sys.system_code
				and sys.system_type = 'bank_account'
				left join tb_m_company as com
				on com_pay.company_id = com.company_id
				left join tb_m_package as pack
				on com_pay.package_id = pack.package_id
			where
				com_pay.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
		";
		
		if(($date_from != "" && $date_from != null) && ($date_to != "" && $date_to != null)){
			$sql = $sql . " and date_format(com_pay.payment_dt, '%Y-%m-%d') between '$date_from' and '$date_to'";
		}
		
		if ($length > 0){
			$sql = $sql . " limit $start, $length";
		}
			
        return $this->db->query($sql)->result();
	}
	
	function get_data_payment_transaction_cnt($date_from, $date_to){
		$sql = "
			select
				count(*) as cnt
			from 
				tb_r_company_payment as com_pay
				inner join tb_m_system as sys
				on com_pay.payment_to_code = sys.system_code
				and sys.system_type = 'bank_account'
				inner join tb_m_company as com
				on com_pay.company_id = com.company_id
				inner join tb_m_package as pack
				on com_pay.package_id = pack.package_id
			where
				com_pay.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				
		";
		
		if(($date_from != "" && $date_from != null) && ($date_to != "" && $date_to != null)){
			$sql = $sql . " and date_format(com_pay.payment_dt, '%Y-%m-%d') between '$date_from' and '$date_to'";
		}
		
        return $this->db->query($sql)->row()->cnt;
	}
	//end
	
	//added by  nanin mulyani 20170321
	function getCompanyPayment_email($payment_id){
		$sql = "
			select 
				com_pay.payment_id, com_pay.payment_dt, com_pay.company_id, com.company_name, com.user_email,
				com_pay.package_id, pack.package_name, pack.package_price, pack.package_period,
				pack.package_description, com_pay.package_month_payment, com_pay.period_dt_from, 
				com_pay.period_dt_to, com_pay.amount_total, com_pay.payment_metod, com_pay.payment_flag,
				com_pay.payment_from_name, com_pay.payment_from_bank, com_pay.payment_from_bank_acc,
				com_pay.payment_confirm_dt, com_pay.payment_ok_by, com_pay.payment_ok_dt
			from
				tb_r_company_payment as com_pay
				inner join tb_m_company as com
				on com_pay.company_id = com.company_id
				inner join tb_m_package as pack
				on com_pay.package_id = pack.package_id
			where
				1=1
		";
		
		if($payment_id != "" && $payment_id != null){
			$sql = $sql . " and com_pay.payment_id = $payment_id";
		}else{
			$sql = $sql . "
				and com_pay.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and com_pay.payment_dt like ('" . date('Y-m-d') . "%');
			";
		}
		
		$q = $this->db->query($sql);
		
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
	}
	
	function getBankAccount(){
		$sql = "
			select
				m_sys.system_type, m_sys.system_code, m_sys.system_value_txt, m_sys.system_value_num
			from tb_m_system as m_sys
			where system_type = 'bank_account';
		";
		
		return $this->db->query($sql)->result();
	}
	//end
	
	//added by  nanin mulyani 20170316
	function checkdt_Transaksi($system_type, $system_code){
		$sql = "
			select count(*) as cnt from tb_m_company_prefferences as com_preff
			where
				com_preff.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and com_preff.system_type = '$system_type'
				and com_preff.system_code = '$system_code'
		";
		
		return $this->db->query($sql)->row()->cnt;
	}
	
	function updateTransaksi($system_type, $system_code, $system_value_txt, $system_value_num){
		$sql = "
			update tb_m_company_prefferences
			set
		";
		
		if($system_value_txt != null && $system_value_num == null){
			$sql = $sql . " system_value_txt = '$system_value_txt'";
		}
		
		if($system_value_txt == null && $system_value_num != null){
			$sql = $sql . " system_value_num = '$system_value_num'";
		}
		
		if($system_value_txt != null && $system_value_num != null){
			$sql = $sql . " system_value_txt = '$system_value_txt' , system_value_num = '$system_value_num'";
		}
		
		$sql = $sql . " , changed_by = '" . $this->session->userdata(S_USER_NAME) .  "'";
				
		$sql = $sql . "
			where
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and system_type = '$system_type'
				and system_code = '$system_code'
		";
		
		return $this->db->query($sql);
	}
	
	function insertTransaksi($system_type, $system_code, $system_value_txt, $system_value_num){
		$company_id = $this->session->userdata(S_COMPANY_ID);
		$created_by = $this->session->userdata(S_USER_NAME);
		$created_dt = date('Y-m-d H:i:s');
		$changed_by = $this->session->userdata(S_USER_NAME);
		
		$sql = "
			insert into tb_m_company_prefferences (company_id, system_type, system_code, system_value_txt, system_value_num, created_by, created_dt, changed_by)
			values ('$company_id', '$system_type', '$system_code', '$system_value_txt', '$system_value_num', '$created_by', '$created_dt', '$changed_by');
		";
		
		return $this->db->query($sql);
	}
	
	//

	//added by nanin mulyani 20170329
	function getSetDate($package_month_payment, $payment_dt){
		$sql = "
			select
				date_format('$payment_dt', '%d/%m/%Y') as tanggal_pembayaran,
				'$package_month_payment' as jangka_pembayaran,
				date_format(date_add('$payment_dt', interval 2 day), '%d/%m/%Y') as masa_aktif_from,
				date_format(date_add(date_add('$payment_dt', interval $package_month_payment month), interval 1 day), '%d/%m/%Y') as masa_aktif_to,
				date_format(date_add('$payment_dt', interval $package_month_payment month), '%d/%m/%Y') as tgl_next
		";

		return $this->db->query($sql)->row();
	}
	//end
    
    //added by nanin mulyani 20170330
	//edited by nanin mulyani 20170331
	function check_transaksi_cid($payment_id, $payment_dt, $company_id){
		$sql = "
			select
				com_pay.payment_id, com_pay.payment_dt, com_pay.company_id, com_pay.package_id,
				com_pay.package_month_payment, com_pay.period_dt_from, com_pay.period_dt_to,
				com_pay.package_cost, com_pay.user_additional, com_pay.user_additional_cost, 
				com_pay.amount_total, com_pay.unique_code, com_pay.payment_total,
				com_pay.payment_metod, com_pay.payment_flag, com_pay.payment_to_code,
				com_pay.payment_from_name, com_pay.payment_from_bank, com_pay.payment_from_bank_acc,
				com_pay.payment_confirm_dt, com_pay.payment_amount, com_pay.payment_ok_by,
				com_pay.payment_ok_dt
			from tb_r_company_payment as com_pay
			where 
				com_pay.company_id = '$company_id'
		";
		
		if($payment_id != null && $payment_id != ""){
			$sql = $sql . " and com_pay.payment_id = '$payment_id'";
		}
		
		if($payment_dt != null && $payment_dt != ""){
			$sql = $sql . " and date_format(com_pay.payment_dt, '%Y-%m-%d') = '$payment_dt'";
		}
		
		$qry = $this->db->query($sql);
        if ($qry->num_rows() > 0){
            return $qry->row();
        }else{
            return false;
        }
	}
	
	function getDataCompanyPayment_transaction($payment_id, $payment_dt, $company_id){
		$sql = "
			select
				com_pay.payment_id, date_format(com_pay.payment_dt, '%d %M %Y') as payment_dt, 
				com_pay.company_id, com_pay.package_id,
				com_pay.package_month_payment, com_pay.period_dt_from, com_pay.period_dt_to,
				com_pay.payment_metod, com_pay.payment_flag, com_pay.payment_from_name, 
				com_pay.payment_from_bank, com_pay.payment_from_bank_acc,
				date_format(com_pay.payment_confirm_dt, '%d %M %Y') as payment_confirm_dt, 
				com_pay.payment_ok_by, com_pay.payment_ok_dt,
				com_pay.package_cost, com_pay.user_additional, com_pay.user_additional_cost,
				com_pay.amount_total, com_pay.unique_code, com_pay.payment_to_code,
				com_pay.payment_amount, sys.system_value_txt as payment_to_desc,
				com.company_name, com.company_industry, com.logo,
				pack.package_name, pack.package_period, pack.package_description,
				pack.default_landing, pack.default_users, com.email,
				com.trial_expired, pack.package_price, com_pay.payment_total,
				(select system_value_num from tb_m_system where system_type = 'price' and system_code = 'users') as additional_user_price,
				date_add(com_pay.period_dt_to, interval -1 day) as payment_dt_next,
				com.email
			from 
				tb_r_company_payment as com_pay
				left join tb_m_system as sys
				on com_pay.payment_to_code = sys.system_code
				and sys.system_type = 'bank_account'
				left join tb_m_company as com
				on com_pay.company_id = com.company_id
				left join tb_m_package as pack
				on com_pay.package_id = pack.package_id
			where
				com_pay.company_id = '" . $company_id . "'
		";
		
		if($payment_id != null && $payment_id != ""){
			$sql = $sql . " and com_pay.payment_id = '$payment_id'";
		}
		
		if($payment_dt != null && $payment_dt != ""){
			$sql = $sql . " and date_format(com_pay.payment_dt, '%Y-%m-%d') = '$payment_dt'";
		}
		
		$qry = $this->db->query($sql);
        if ($qry->num_rows() > 0){
            return $qry->row();
        }else{
            return false;
        }
	}
	//end
}
