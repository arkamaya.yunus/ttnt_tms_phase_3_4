<?php
/*
* @author: Irfan Satriadarma
* @created: 16 Maret 2020
*/

class Driver_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
    }

    function getLogisticPartners($keyword = '')
    {
        $sql = "select distinct logistic_partner
                from tb_m_driver
                where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                and logistic_partner like '%" . $keyword . "%'
                order by logistic_partner";
        return $this->db->query($sql)->result();
    }

    function getDrivers($logistic_partner, $active, $start, $length, $sv, $order, $columns, $business)
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {            
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }

        $where_logistic_partner = ($logistic_partner != 'all') ? " and a.logistic_partner = '" . $logistic_partner . "'" : '';
        $where_active = ($active != 'all') ? " and a.active = '" . $active . "'" : '';
        $where_business = ($business != 'all') ? " and a.business = '" . $business . "'" : '';

        $sql = "
			select 
                a.driver_cd, a.driver_name, a.logistic_partner, a.photo,
                a.jobdesc, a.smartphone, a.phone, a.driving_license_val,
                a.driving_license_type, a.forklift_license_val, a.forklift_license_type,
                a.length_of_working, a.num_of_accident, a.join_dt, a.active, a.resign_dt, a.att_id, a.dob, a.business
			from 
				tb_m_driver a 
			where
				(
                    a.driver_cd like '%" . $sv . "%'
                    or a.driver_name like '%" . $sv . "%'
                    or a.logistic_partner like '%" . $sv . "%'
                    or a.jobdesc like '%" . $sv . "%'
                    or a.smartphone like '%" . $sv . "%'
                    or a.phone like '%" . $sv . "%'
                    or a.driving_license_val like '%" . $sv . "%'
                    or a.driving_license_type like '%" . $sv . "%'
                    or a.forklift_license_val like '%" . $sv . "%'
                    or a.forklift_license_type like '%" . $sv . "%'
                    or a.length_of_working like '%" . $sv . "%'
                    or a.num_of_accident like '%" . $sv . "%'
                    or a.join_dt like '%" . $sv . "%'

                )
                " . $where_logistic_partner . $where_active . $where_business . "
            order by " . $order_by . " ";
            
		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
		return $this->db->query($sql)->result();
    }

    function getCountOfDriver($logistic_partner, $active, $sv, $business)
    {
        $where_logistic_partner = ($logistic_partner != 'all') ? " and a.logistic_partner = '" . $logistic_partner . "'" : '';
        $where_active = ($active != 'all') ? " and a.active = '" . $active . "'" : '';
        $where_business = ($business != 'all') ? " and a.business = '" . $business . "'" : '';

        $sql = "
			select 
                count(a.driver_cd) as cnt
                from 
				tb_m_driver a 
			where
				(
                    a.driver_cd like '%" . $sv . "%'
                    or a.driver_name like '%" . $sv . "%'
                    or a.logistic_partner like '%" . $sv . "%'
                    or a.jobdesc like '%" . $sv . "%'
                    or a.smartphone like '%" . $sv . "%'
                    or a.phone like '%" . $sv . "%'
                    or a.driving_license_val like '%" . $sv . "%'
                    or a.driving_license_type like '%" . $sv . "%'
                    or a.forklift_license_val like '%" . $sv . "%'
                    or a.forklift_license_type like '%" . $sv . "%'
                    or a.length_of_working like '%" . $sv . "%'
                    or a.num_of_accident like '%" . $sv . "%'
                    or a.join_dt like '%" . $sv . "%'

                )
                " . $where_logistic_partner . $where_active . $where_business . "
		";
		return $this->db->query($sql)->row()->cnt;
    }

    function getDriver($driver_cd)
    {
        $sql = "
            select 
                a.driver_cd, a.driver_name, a.logistic_partner, a.jobdesc, a.smartphone, a.phone,
                a.driving_license_val, a.driving_license_type, a.forklift_license_val, 
                a.forklift_license_type, a.length_of_working, a.num_of_accident, a.join_dt, a.photo, 
                a.active, a.resign_dt, a.att_id, a.dob, a.business
            from 
                tb_m_driver a
            where
                md5(a.driver_cd) = '" .$driver_cd . "'
        ";
        return $this->db->query($sql)->result();
    }

    function getDriverByName($driver_name)
    {
        $sql = "
            select 
                a.driver_cd, a.driver_name, a.logistic_partner, a.jobdesc, a.smartphone, a.phone,
                a.driving_license_val, a.driving_license_type, a.forklift_license_val, 
                a.forklift_license_type, a.length_of_working, a.num_of_accident, a.join_dt, a.photo, a.active, a.resign_dt, a.att_id, a.dob, a.business
            from 
                tb_m_driver a
            where
                a.driver_name = '" .$driver_name . "'
        ";
        
        return $this->db->query($sql)->result();
    }

    function delete($driver_cd)
    {
        $driver = $this->getDriver($driver_cd);
        $cid_file_path 			= COMPANY_ASSETS_PATH . 'cid' . $this->session->userdata(S_COMPANY_ID) . '/profile/';
		$cid_file_thumbs_path 	= $cid_file_path . 'thumbs/';
		
        $temp_file_lama = $driver[0]->photo;
        if ($temp_file_lama != '') 
        {
            $filep = $cid_file_path . $temp_file_lama;
            if (file_exists($filep)) {
                unlink($filep);
            }

            $filep = $cid_file_thumbs_path . $temp_file_lama;
            if (file_exists($filep)) {
                unlink($filep);
            }
        }

        $this->db->where('md5(driver_cd)', $driver_cd);
        $this->db->delete('tb_m_driver');
    }

    function save($update = '')
    {
        // validate driver_cd
        $check_lp = $this->getDriver(md5($this->input->post('driver_cd', true)));
        if (count($check_lp) == 0 || $update == '1')
        {
            $photo 		= $this->do_upload();
            $lp = array
            (
                'company_id' => $this->session->userdata(S_COMPANY_ID),
                'driver_cd' => $this->input->post('driver_cd', true),
                'driver_name' => $this->input->post('driver_name', true),
                'logistic_partner' => $this->input->post('logistic_partner', true),
                'jobdesc' => $this->input->post('jobdesc', true),
                'smartphone' => $this->input->post('smartphone', true),
                'phone' => $this->input->post('phone', true),
                'driving_license_val' => ($this->input->post('driving_license_val',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('driving_license_val', true)))) : null,
                'driving_license_type' => $this->input->post('driving_license_type', true),
                'forklift_license_val' => ($this->input->post('forklift_license_val',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('forklift_license_val', true)))) : null,
                'forklift_license_type' => $this->input->post('forklift_license_type', true),
                'length_of_working' => $this->input->post('length_of_working', true),
                'num_of_accident' => $this->input->post('num_of_accident', true),
                'join_dt' => ($this->input->post('join_dt',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('join_dt', true)))) : null,
                'photo' => $photo,
                'active' => $this->input->post('active', true),
                'resign_dt' => ($this->input->post('resign_dt',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('resign_dt', true)))) : null,
                'att_id' => $this->input->post('att_id', true),
                'dob' => ($this->input->post('dob',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('dob', true)))) : null,
                'business' => $this->input->post('business', true),
            );

            if ($update == '')
            {
                // validate driver_name
                $check_driver_name = $this->getDriverByName($this->input->post('driver_name', true));        
                if (count($check_driver_name) == 0)
                {
                    $lp['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
                    $lp['created_dt'] = date('Y-m-d H:i:s');
    
                    $this->db->insert('tb_m_driver', $lp);    
                }
                else{
                    return '3'; // driver_name already exists
                }

                
                return '1';
            }
            else 
            {
                $lp['changed_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
                $lp['changed_dt'] = date('Y-m-d H:i:s');

                $this->db->where('driver_cd', $this->input->post('driver_cd', true));
                $this->db->update('tb_m_driver', $lp);

                return '2'; // update success
            }    
        }
        else
        {
            return '0'; // driver_cd already exists
        }        
    }

    function do_upload() 
	{
		// file path
		$cid_file_path 			= COMPANY_ASSETS_PATH . 'cid' . $this->session->userdata(S_COMPANY_ID) . '/profile/';
		$cid_file_thumbs_path 	= $cid_file_path . 'thumbs/';
		
        $nama_filenya = $this->input->post('photo', true);
        if ($_FILES['file_photo']['name'] != '') {
            $temp_file_lama = $nama_filenya;
            if ($temp_file_lama != '') 
			{
                $filep = $cid_file_path . $temp_file_lama;
                if (file_exists($filep)) {
                    unlink($filep);
                }

                $filep = $cid_file_thumbs_path . $temp_file_lama;
                if (file_exists($filep)) {
                    unlink($filep);
                }
            }
            $config['upload_path'] 		= $cid_file_path;
            $config['allowed_types'] 	= 'jpg|jpeg|gif|png';
            $config['max_size'] 		= '20000';
            $config['remove_spaces'] 	= TRUE;

            $this->load->library('upload', $config);
            $this->upload->do_upload('file_photo');
			
            $file_data 		= $this->upload->data();
            $nama_filenya 	= $file_data['file_name'];

			// renaming for mobile need easy access to photos
			// echo '<pre>'; print_r($file_data); echo '</pre>'; die();
			
            $thumbs = array(
                'source_image' => $file_data['full_path'],
                'new_image' => $cid_file_thumbs_path,
                'maintain_ration' => true,
                'width' => 256,
                'height' => 256
            );

            $this->load->library('image_lib');
            $this->image_lib->initialize($thumbs);
            $this->image_lib->resize();
			
			rename($cid_file_path . $nama_filenya, $cid_file_path . $this->input->post('driver_cd', true)  . $file_data['file_ext']);
			rename($cid_file_thumbs_path. $nama_filenya, $cid_file_thumbs_path . $this->input->post('driver_cd', true)  . $file_data['file_ext']);

            // hasil rename filenya jadi noreg
			$nama_filenya = $this->input->post('driver_cd', true)  . $file_data['file_ext'];
        }
        return $nama_filenya;
    }

    function getDriversForDownload($logistic_partner, $active, $business)
    {
        // $logistic_partner = $this->input->post('logistic_partner', true);
        // $active = $this->input->post('active', true);
        // $business = $this->input->post('business', true);

        $where_logistic_partner = ($logistic_partner != 'all') ? " and logistic_partner = '" . $logistic_partner . "'" : '';
        $where_active = ($active != 'all') ? " and active = '" . $active . "'" : '';
        $where_business = ($business != 'all') ? " and business = '" . $business . "'" : '';

        $sql = "
            Select * from tb_m_driver where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
             " . $where_logistic_partner . $where_active . $where_business . "
            order by logistic_partner, driver_name
        ";
        
        return $this->db->query($sql)->result();
    }
}