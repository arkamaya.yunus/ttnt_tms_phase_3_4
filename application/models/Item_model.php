<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 2016-12-20
 */

class Item_model extends CI_Model {

    function __construct() {
        parent:: __construct();
    }


    //update by uye

    function update_stock_opname($data) {
        $total = 0;
        foreach ($data as $r) {
            $sql = "update tb_m_items set item_qty = item_qty + " . $r[1] . " where item_id = " . $r[0];
            $this->db->query($sql);
            $total += abs($r[1] * $r[2]);
        }

        $journal_h = array
            (
            'company_id' => $this->session->userdata(S_COMPANY_ID)
            , 'project' => ''
            , 'journal_dt' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('opname_dt', true))))
            , 'journal_reff' => ''
            , 'journal_type' => 'opname'
            , 'journal_description' => $this->input->post('memo', true)
            , 'dr_total' => $total
            , 'cr_total' => $total
            , 'created_by' => $this->session->userdata(S_EMPLOYEE_NAME)
            , 'created_dt' => date('Y-m-d H:i:s')
        );

        $this->db->insert('tb_r_journal', $journal_h);
        $journal_id = $this->db->insert_id();

        foreach ($data as $r) {
            $amount = $r[1] * $r[2];
            //acc_assets
            if ($amount > 0) {
                $acc_d = $this->input->post('acc_assets', true);
                $acc_c = $this->input->post('acc_expenses', true);
            } else {
                $acc_d = $this->input->post('acc_expenses', true);
                $acc_c = $this->input->post('acc_assets', true);
                $amount = abs($amount);
            }
            if ($amount != 0) {
                $this->db->query("
				insert into tb_r_journal_items
				(
					journal_id
					, account_id
					, item_description
					, amount
					, dc
					, created_by
					, created_dt
					, changed_by
					, changed_dt
				)
				values
				(
					'" . $journal_id . "'
					, '" . $acc_d . "'
					, 'Penyesuaian (". $this->get_item_name($r[0])->item_name ." ". $r[1]." ". $this->get_item_name($r[0])->unit ." @ ".$r[2].")'
					, '" . $amount . "'
					, 'd'
					, '" . $this->session->userdata(S_EMPLOYEE_NAME) . "'
					, now()
					, '" . $this->session->userdata(S_EMPLOYEE_NAME) . "'
					, now()
				)

			");

                $this->db->query("
				insert into tb_r_journal_items
				(
					journal_id
					, account_id
					, item_description
					, amount
					, dc
					, created_by
					, created_dt
					, changed_by
					, changed_dt
				)
				values
				(
					'" . $journal_id . "'
					, '" . $acc_c . "'
					, 'Penyesuaian Persediaan (". $this->get_item_name($r[0])->item_name .")'
					, '" . $amount . "'
					, 'c'
					, '" . $this->session->userdata(S_EMPLOYEE_NAME) . "'
					, now()
					, '" . $this->session->userdata(S_EMPLOYEE_NAME) . "'
					, now()
				)

			");
            }
        }


        echo '1';
    }


    function get_item_name($id) {
        $sql = "select * from tb_m_items where item_id=" . $id;
        return $this->db->query($sql)->row();
    }

    // end update by uye

    function get_items($start = '', $length = '', $sv = '', $order_by = 'item_name asc') {
        $sql = "
			select
				a.item_id,
				a.item_code,
				a.item_name,
				a.item_description,
				a.item_category_id,
				a.item_qty,
				a.unit,
				a.item_price,
				a.item_price_buy,
				a.sales_tax_id
			from
				tb_m_items a
			where
				a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and 
				(
				a.item_description like '%" . $sv . "%'
				or a.item_code like '%" . $sv . "%'
                or a.item_name like '%" . $sv . "%'
				or a.item_category like '%" . $sv . "%'
				or a.item_price like '%" . $sv . "%'
				or a.item_price_buy like '%" . $sv . "%'
				or a.item_qty like '%" . $sv . "%'
				)
		";

        if ($this->input->post('category', true) != '0' && $this->input->post('category', true) != '') {
            $sql .= " and a.item_category = '" . $this->input->post('category', true) . "'";
        }

        if ($this->input->post('archive', true) == "true") {
            $sql .= " and a.is_archive in ('1','0') ";
        } else {
            $sql .= " and a.is_archive = '0' ";
        }

        $sql .= " order by a." . $order_by;

        if ($start != '' && $length != '') {
            $sql .= " limit " . $start . ", " . $length;
        }

        return $this->db->query($sql)->result();
    }

    function get_item_count($sv = '') {
        $sql = "
			select
				count(item_id) as cnt
			from
				tb_m_items a
			where
				a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
  				and (a.item_description like '%" . $sv . "%'
				or a.item_code like '%" . $sv . "%'
				or a.item_category like '%" . $sv . "%')
		";

        if ($this->input->post('category', true) != '0' && $this->input->post('category', true) != '') {
            $sql .= " and a.item_category = '" . $this->input->post('category', true) . "'";
        }

        if ($this->input->post('archive', true) == "true") {
            $sql .= " and a.is_archive in ('1','0') ";
        } else {
            $sql .= " and a.is_archive = '0' ";
        }



        return $this->db->query($sql)->row()->cnt;
    }

    function get_item($item_id) {

        $sql = "
			select
				a.item_id,
				a.company_id,
				a.item_code,
				a.item_category_id,
				a.item_category,
				a.item_name,
				a.item_description,
				a.item_qty,
				a.item_price,
				a.item_price_buy,
				a.unit,
				a.sales_account_id,
				a.sales_taxable,
				a.purchase_account_id,
				a.purchase_taxable,
				a.is_archive,
                a.is_asset,
                a.asset_account_id,
				a.file,
				a.is_used,
				(
					select concat('(', account_code, ') - ', account_name)
					from tb_m_account
					where account_id = a.sales_account_id and company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				)
				as sales_account,
				(
					select concat('(', account_code, ') - ', account_name)
					from tb_m_account
					where account_id = a.purchase_account_id and company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				)
				as purchase_account,
                (
					select concat('(', account_code, ') - ', account_name)
					from tb_m_account
					where account_id = a.asset_account_id and company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				)
				as asset_account
			from
				tb_m_items a
			where
				a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and a.item_id = '" . $item_id . "'
		";

        return $this->db->query($sql)->result();
    }
	
	function get_item_by_id($item_id) {

        $sql = "
			select
				a.item_id,
				a.company_id,
				a.item_code,
				a.item_category_id,
				a.item_category,
				a.item_name,
				a.item_description,
				a.item_qty,
				a.item_price,
				a.item_price_buy,
				a.unit,
				a.sales_account_id,
				a.sales_tax_id,
				a.purchase_account_id,
				a.purchase_tax_id,
				a.is_archive,
                a.is_asset,
                a.asset_account_id,
				a.file,
				a.is_used,
				a.min_stock,
				(
					select concat('(', account_code, ') - ', account_name)
					from tb_m_account
					where account_id = a.sales_account_id and company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				)
				as sales_account,
				(
					select concat('(', account_code, ') - ', account_name)
					from tb_m_account
					where account_id = a.purchase_account_id and company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				)
				as purchase_account,
                (
					select concat('(', account_code, ') - ', account_name)
					from tb_m_account
					where account_id = a.asset_account_id and company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				)
				as asset_account,
				b.tax_name AS sales_tax,
				c.tax_name AS purchase_tax,
				a.warehouse_id,
				d.warehouse_desc,
				a.asset_account_id,
				e.account_name
			FROM
				tb_m_items a
				LEFT JOIN tb_m_tax b ON b.tax_id = a.sales_tax_id
				LEFT JOIN tb_m_tax c ON c.tax_id = a.purchase_tax_id
				LEFT JOIN tb_m_warehouse d ON d.warehouse_id = a.warehouse_id
				LEFT JOIN tb_m_account e on e.account_id = a.asset_account_id
			where
				a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and a.item_id = '" . $item_id . "'
			ORDER BY
				a.item_name ASC
		";

        return $this->db->query($sql)->result();
    }

    function save_item() {
        $photo = $this->do_upload();
        $is_asset = ($this->input->post('is_asset', true) == "on" ? "1" : "0");

        $is_buy = ($this->input->post('is_buy', true) == "on" ? "1" : "0");
        $is_sell = ($this->input->post('is_sell', true) == "on" ? "1" : "0");
        //product_track_inventory

        $i_price_sell = $this->input->post('item_price', true);
        $i_price_buy = $this->input->post('item_price_buy', true);
        $s_acc_id = $this->input->post('sales_account_id', true);
        $s_tax = ($this->input->post('sales_taxable', true) == "on" ? "1" : "0");
        $p_acc_id = $this->input->post('purchase_account_id', true);
        $p_tax = ($this->input->post('purchase_taxable', true) == "on" ? "1" : "0");

        //$i_qty = $this->input->post('item_qty', true);


        $item = array
            (
            'company_id' => $this->session->userdata(S_COMPANY_ID),
            'item_id' => $this->input->post('item_id', true),
            'item_code' => $this->input->post('item_code', true),
            'item_category_id' => $this->input->post('item_category_id', true),
            'item_category' => $this->input->post('item_category', true),
            'item_name' => $this->input->post('item_name', true),
            'item_description' => $this->input->post('item_description', true),
            'item_qty' => $this->input->post('item_qty', true),
            'item_price' => $is_sell ? $i_price_sell : 0,
            'item_price_buy' => $is_buy ? $i_price_buy : 0,
            'unit' => $this->input->post('unit', true),
            'sales_account_id' => $is_sell ? $s_acc_id : 0,
            'sales_taxable' => $is_sell ? $s_tax : 0,
            'purchase_account_id' => $is_buy ? $p_acc_id : 0,
            'purchase_taxable' => $is_buy ? $p_tax : 0,
            'is_archive' => ($this->input->post('is_archive', true) == "on" ? "1" : "0"),
            'is_asset' => $is_asset,
            'asset_account_id' => $is_asset ? $this->input->post('asset_account_id', true) : NULL,
            'file' => $photo
        );

        if
        (
                $this->input->post('item_id', true) == '' ||
                $this->input->post('item_id', true) == null
        ) {
            $item['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
            $item['created_dt'] = date('Y-m-d H:i:s');

            $this->db->insert('tb_m_items', $item);
        } else {
            $item_id = $this->input->post('item_id', true);

            $item['changed_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
            $item['changed_dt'] = date('Y-m-d H:i:s');

            // update header
            $this->db->where('item_id', $item_id);
            $this->db->update('tb_m_items', $item);
        }

        echo '1';
    }
	
	function check_purchase_order_item($item_id)
    {
		$sql = "
			
			SELECT
				( SELECT COUNT( * ) FROM tb_r_purchase_order_items WHERE item_id = '" .$item_id. "' ) AS count1,
				( SELECT COUNT( * ) FROM tb_r_purchase_payment_items WHERE item_id = '" .$item_id. "' ) AS count2,
				( SELECT COUNT( * ) FROM tb_r_purchase_request_items WHERE item_id = '" .$item_id. "' ) AS count3,
				( SELECT COUNT( * ) FROM tb_r_purchase_return_items WHERE item_id = '" .$item_id. "' ) AS count4,
				( SELECT COUNT( * ) FROM tb_r_sales_delivery_items WHERE item_id = '" .$item_id. "' ) AS count5,
				( SELECT COUNT( * ) FROM tb_r_sales_invoice_items WHERE item_id = '" .$item_id. "' ) AS count6,
				( SELECT COUNT( * ) FROM tb_r_sales_order_items WHERE item_id = '" .$item_id. "' ) AS count7,
				( SELECT COUNT( * ) FROM tb_r_sales_quotation_items WHERE item_id = '" .$item_id. "' ) AS count8
		";
				
		
        $q = $this->db->query($sql);
		if($q->num_rows() > 0){
			return $q->row();
		}else{
			return false;
		}
    }

    function delete($item_id) {
        $this->db->where('item_id', $item_id);
        $this->db->delete('tb_m_items');
    }

    function check_duplicate($code, $name) {
        $sql = "
			select
				count(a.item_id) as cnt
			from
				tb_m_items a
			where
				a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and (a.item_code = '" . $code . "' or a.item_name = '" . $name . "')
		";

        if ($_POST['old_item_code']) {
            if ($_POST['old_item_name'] != '') {
                $sql .= " and (a.item_code != '" . $_POST['old_item_code'] . "' or a.item_name != '" . $_POST['old_item_name'] . "')";
            } else {
                $sql .= " and (a.item_code != '" . $_POST['old_item_code'] . "')";
            }
        } else {
            if ($_POST['old_item_name'] != '') {
                $sql .= " and (a.item_name != '" . $_POST['old_item_name'] . "')";
            }
        }


        return $this->db->query($sql)->row()->cnt;
    }

    // Get Categories
    function get_categories($start = '', $length = '', $sv = '') {
        $sql = "
			select
				a.preff_id,
				a.system_type,
				a.system_code,
				a.system_value_txt,
				a.system_value_num,
				(select count(*) from tb_m_items where item_category = a.system_code) as used
			from
				tb_m_company_prefferences a
			where
				a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and a.system_type = 'item_category'
				and (a.preff_id like '%" . $sv . "%' or a.system_code like '%" . $sv . "%'
				or a.system_value_txt like '%" . $sv . "%')
			order by a.system_value_num asc
		";

        // $sql .= " order by position_name";

        if ($start != '' && $length != '') {
            $sql .= " limit " . $start . ", " . $length;
        }

        return $this->db->query($sql)->result();
    }

    function get_category_count($sv = '') {
        $sql = "
			select count(a.preff_id) as cnt from tb_m_company_prefferences a
			where
				a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and a.system_type = 'item_category'
				and (a.preff_id like '%" . $sv . "%' or a.system_code like '%" . $sv . "%'
				or a.system_value_txt like '%" . $sv . "%')
		";

        return $this->db->query($sql)->row()->cnt;
    }

    // Get Categories
    function get_units($start = '', $length = '', $sv = '') {
        $sql = "
			select
				a.preff_id,
				a.system_type,
				a.system_code,
				a.system_value_txt,
				a.system_value_num,
				(select count(*) from tb_m_items where unit = a.system_code) as used
			from
				tb_m_company_prefferences a
			where
				a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and a.system_type = 'config_units'
				and (a.preff_id like '%" . $sv . "%' or a.system_code like '%" . $sv . "%'
				or a.system_value_txt like '%" . $sv . "%')
			order by a.system_value_num asc
		";

        // $sql .= " order by position_name";

        if ($start != '' && $length != '') {
            $sql .= " limit " . $start . ", " . $length;
        }

        return $this->db->query($sql)->result();
    }

    function get_unit_count($sv = '') {
        $sql = "
			select count(a.preff_id) as cnt from tb_m_company_prefferences a
			where
				a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
				and a.system_type = 'item_category'
				and (a.preff_id like '%" . $sv . "%' or a.system_code like '%" . $sv . "%'
				or a.system_value_txt like '%" . $sv . "%')
		";

        return $this->db->query($sql)->row()->cnt;
    }

    // added by deden
    function update_asset($item_id, $qty) {

        $where_item = array(
            'company_id' => $this->session->userdata(S_COMPANY_ID),
            'item_id' => $item_id,
            'is_asset' => '1'
        );

        $item = $this->db
                     ->from('tb_m_items')
                     ->where($where_item)
                     ->get()
                     ->row();

        if(!is_null($item)) {
            $this->db
                 ->set('item_qty', $item->item_qty + $qty)
                 ->where($where_item)
                 ->update('tb_m_items');
        }
    }

    // added by deden
    function is_all_asset_available($items) {
        foreach ($items as $item) {
            $asset = $this->db
                          ->from('tb_m_items')
                          ->where(array(
                            'company_id' => $this->session->userdata(S_COMPANY_ID),
                            'item_id' => $item->item_id,
                            'is_asset' => '1'
                            ))
                          ->get()
                          ->row();

            if(!is_null($asset)) {
                if($item->item_qty > $asset->item_qty) return false;
            }

        }
        return true;
    }

    function do_upload() {
        $nama_filenya = $this->input->post('photo ', true);
        if ($_FILES['file_photo']['name'] != '') {
            $temp_file_lama = $nama_filenya;

            $config['upload_path'] = './zrcs/default/assets/images/items';
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size'] = '20000';
            $config['remove_spaces'] = TRUE;

            $this->load->library('upload', $config);

            $this->upload->do_upload('file_photo');
            // $data = $this->upload->data('file_photo');
            $file_data = $this->upload->data();

            $nama_filenya = $file_data['file_name'];

            $thumbs = array(
                'source_image' => $file_data['full_path'],
                'new_image' => './zrcs/default/assets/images/items/thumbs',
                'maintain_ration' => true,
                'width' => 256,
                'height' => 256
            );

            $this->load->library('image_lib');
            $this->image_lib->initialize($thumbs);

            $this->image_lib->resize();

            // hapus file lama
            if ($temp_file_lama != '') {
                $filep = 'zrcs/default/assets/images/items/' . $temp_file_lama;
                if (file_exists($filep)) {
                    unlink($filep);
                }

                $filep = 'zrcs/default/assets/images/items/thumbs/' . $temp_file_lama;
                if (file_exists($filep)) {
                    unlink($filep);
                }
            }
        }
        return $nama_filenya;
    }

    function isPerformSalesQuotation($item_id) {
        $sql = "select count(*) as cnt from tb_r_sales_quotation_items where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
			and item_id = '" . $item_id . "'";

        return $this->db->query($sql)->row()->cnt;
    }

    function isPerformSalesOrder($item_id) {
        $sql = "select count(*) as cnt from tb_r_sales_order_items a
			inner join tb_r_sales_order b on a.order_id = b.order_id
			where b.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
			and a.item_id = '" . $item_id . "'";

        return $this->db->query($sql)->row()->cnt;
    }

    function isPerformSalesDelivery($item_id) {
        $sql = "select count(*) as cnt from tb_r_sales_delivery_items where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
			and item_id = '" . $item_id . "'";

        return $this->db->query($sql)->row()->cnt;
    }

    function isPerformSalesInvoice($item_id) {
        $sql = "select count(*) as cnt from tb_r_sales_invoice_items where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
			and item_id = '" . $item_id . "'";

        return $this->db->query($sql)->row()->cnt;
    }

    function check_category_usage($preff_id) {
        $c = "select * from tb_m_company_prefferences where preff_id = '" . $preff_id . "'
			 	and company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'";

        if ($this->db->query($c)->num_rows() > 0) {
            $category = $this->db->query($c)->row()->system_code;

            $sql = "select count(item_id) as cnt from tb_m_items where item_category = '" . $category . "'
					and company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'";

            return $this->db->query($sql)->row()->cnt;
        } else {
            return 0;
        }
    }

    function check_unit_usage($preff_id) {
        $c = "select * from tb_m_company_prefferences where preff_id = '" . $preff_id . "'
			 	and company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'";

        if ($this->db->query($c)->num_rows() > 0) {
            $unit = $this->db->query($c)->row()->system_code;

            $sql = "select count(item_id) as cnt from tb_m_items where unit = '" . $unit . "'
			and company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'";

            return $this->db->query($sql)->row()->cnt;
        } else {
            return 0;
        }
    }

    function quick_save($data) {
        $item = array
		(
            'company_id' => $this->session->userdata(S_COMPANY_ID),
            'item_code' => $this->input->post('item_code', true),
            'item_category_id' => $this->input->post('item_category_id', true),
            'item_category' => $this->input->post('item_category', true),
            'item_name' => $this->input->post('item_name', true),
			'item_description' => $this->input->post('item_description', true),
            'item_qty' => $this->input->post('item_qty', true),
            'item_price' => $this->input->post('item_price', true),
            'item_price_buy' => $this->input->post('item_price_buy', true),
            'unit' => $this->input->post('unit', true),
            'sales_account_id' => $data['default_ar'],
            'sales_taxable' => '1',
            'purchase_account_id' => $data['default_ap'],
            'purchase_taxable' => '1'
        );

        if
        (
                $this->input->post('item_id', true) == '' ||
                $this->input->post('item_id', true) == null
        ) {
            $item['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
            $item['created_dt'] = date('Y-m-d H:i:s');

            $this->db->insert('tb_m_items', $item);
        } else {
            $item_id = $this->input->post('item_id', true);

            $item['changed_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
            $item['changed_dt'] = date('Y-m-d H:i:s');

            // update header
            $this->db->where('item_id', $item_id);
            $this->db->update('tb_m_items', $item);
        }

        return $this->db->insert_id();
    }
	
	//ADDED BY YOGA
	function get_data($start, $length,$id,$sv='') {
	   
	   $sql = "SELECT type,tanggal,no_transaksi,qty_out,qty_in  from (
					SELECT 
						 'Delivery' as type
						,sd.delivery_dt as tanggal
						,sd.no_reff as no_transaksi
						,sdi.item_qty as qty_out 
						,'-' as qty_in
				FROM 
					tb_r_sales_delivery as sd
				INNER JOIN tb_r_sales_delivery_items as sdi on sd.delivery_id = sdi.delivery_id and sd.company_id = sdi.company_id
				where sd.company_id ='" . $this->session->userdata(S_COMPANY_ID) . "' and sdi.item_id='" . $id. "'
				and (
					sd.no_reff like '%" . $sv . "%' 
					or sdi.item_qty like '%" . $sv . "%'
				)
				
				UNION all

				SELECT 
					   'Invoice' as type
					  ,si.invoice_dt
					  ,si.no_reff
					  ,sii.item_qty 
					  ,'-'
				FROM tb_r_sales_invoice as si
				INNER JOIN tb_r_sales_invoice_items as sii on si.invoice_id = sii.invoice_id and si.company_id= sii.company_id
				where si.company_id ='" . $this->session->userdata(S_COMPANY_ID) . "' and sii.item_id='" . $id. "'
				and (
					si.no_reff like '%" . $sv . "%' 
					or sii.item_qty  like '%" . $sv . "%'
				)
				
				UNION all

				SELECT 
					  'GR' as type
					  ,gr.gr_dt
					  ,gr.no_reff
					  ,'-'
					  ,gri.item_qty
				FROM tb_r_good_receipt as gr
				INNER JOIN tb_r_good_receipt_items as gri on gr.gr_id = gri.gr_id and gr.company_id= gri.company_id
				where gr.company_id ='" . $this->session->userdata(S_COMPANY_ID) . "' and gri.item_id='" . $id. "'
				and (
					gr.no_reff like '%" . $sv . "%' 
					or gri.item_qty  like '%" . $sv . "%'
				)
				)as T1 ORDER BY tanggal desc
				";

		
		if ($length != -1)
		{
			$sql .= " limit " . $start . ", " . $length;
		}
		return $this->db->query($sql)->result();
	}

    function count_data($sv='',$id_product='') {
	    $sql = "
				SELECT SUM(cnt_dlvr) as cnt from (
					SELECT 
							 count(sd.delivery_id) as cnt_dlvr
						FROM tb_r_sales_delivery as sd
					INNER JOIN tb_r_sales_delivery_items as sdi on sd.delivery_id = sdi.delivery_id
					where sd.company_id ='" . $this->session->userdata(S_COMPANY_ID) . "' and sdi.item_id='" . $id_product . "'
					and (
						sd.no_reff like '%" . $sv . "%' 
						or sdi.item_qty like '%" . $sv . "%'
					)
					
					UNION all
					
					SELECT 
							count(si.invoice_id) as cnt_invc
					FROM tb_r_sales_invoice as si
					INNER JOIN tb_r_sales_invoice_items as sii on si.invoice_id = sii.invoice_id and si.company_id= sii.company_id
					where si.company_id ='" . $this->session->userdata(S_COMPANY_ID) . "' and sii.item_id='" . $id_product . "'
					and (
						si.no_reff like '%" . $sv . "%' 
						or sii.item_qty  like '%" . $sv . "%'
					)
					
					UNION all
					
					SELECT 
					   count(gr.gr_id) as cnt_gr
					FROM tb_r_good_receipt as gr
					INNER JOIN tb_r_good_receipt_items as gri on gr.gr_id = gri.gr_id and gr.company_id= gri.company_id
					where gr.company_id ='" . $this->session->userdata(S_COMPANY_ID) . "' and gri.item_id='" . $id_product. "'
					and (
						gr.no_reff like '%" . $sv . "%' 
						or gri.item_qty  like '%" . $sv . "%'
					)
						)as test
				";

		
		return $this->db->query($sql)->row()->cnt;
	   
    }
	
	function get_item_unit(){
		$sql = "
			SELECT
				system_code 
			FROM
				tb_m_company_prefferences
			WHERE
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "' and
				system_type = 'config_units'
			ORDER BY
				system_code
		";

		return $this->db->query($sql)->result();
	}
	
	function get_item_category($sv=''){
		$sql = "
			SELECT
				system_code,
				system_value_txt
			FROM
				tb_m_company_prefferences
			WHERE
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "' and
				system_type = 'item_category' and
				(system_code like '%" . $sv . "%' or system_value_txt like '%" . $sv . "%')
			ORDER BY
				system_code
		";

		return $this->db->query($sql)->result();
	}
	
	function get_sales_account(){
		$sql = "
			SELECT
				account_id,
				account_name,
				account_code
			FROM
				tb_m_account
			WHERE
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "' and
				account_type = 'income'
			ORDER BY
				account_name
		";

		return $this->db->query($sql)->result();
	}
	
	function get_purchase_account(){
		$sql = "
			SELECT
				account_id,
				account_name,
				account_code
			FROM
				tb_m_account
			WHERE
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "' and
				account_type = 'expenses'
			ORDER BY
				account_name
		";

		return $this->db->query($sql)->result();
	}
	
	function get_sales_tax(){
		$sql = "
			SELECT
				tax_id,
				tax_name 
			FROM
				tb_m_tax 
			WHERE
				company_id = '" . $this->session->userdata(S_COMPANY_ID) ."'
		";

		return $this->db->query($sql)->result();
	}
	
	function get_purchase_tax(){
		$sql = "
			SELECT
				tax_id,
				tax_name 
			FROM
				tb_m_tax 
			WHERE
				company_id = '" . $this->session->userdata(S_COMPANY_ID) ."'
		";

		return $this->db->query($sql)->result();
	}
	
	function get_warehouse(){
		$sql = "
			SELECT
				warehouse_id,
				warehouse_desc
			FROM
				tb_m_warehouse 
			WHERE
				company_id = '" . $this->session->userdata(S_COMPANY_ID) ."'
		";

		return $this->db->query($sql)->result();
	}
	
	function get_asset(){
		$sql = "
			SELECT
				account_id,
				account_name,
				account_code
			FROM
				tb_m_account
			WHERE
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "' and
				account_type = 'assets'
			ORDER BY
				account_name
		";

		return $this->db->query($sql)->result();
	}

	function save_items() {
        $arrCategory = implode(', ',$this->input->post('item_category_id',true));

		$item = array(
			'company_id' 			=> $this->session->userdata(S_COMPANY_ID),
			'item_code'	 			=> $this->input->post('item_code',true),
			'item_name'	 			=> $this->input->post('item_name',true),
			'unit'	 				=> $this->input->post('unit',true),
			'item_category_id'	 	=> $arrCategory,
			'item_category'	 		=> $arrCategory,
			'item_price'	 		=> $this->input->post('item_price',true),
			'sales_account_id'	 	=> $this->input->post('sales_account_id',true),
			'sales_tax_id'	 		=> $this->input->post('sales_tax_id',true),
			'item_price_buy'	 	=> $this->input->post('item_price_buy',true),
			'purchase_account_id'	=> $this->input->post('purchase_account_id',true),
			'purchase_tax_id'		=> $this->input->post('purchase_tax_id',true),
			'asset_account_id'		=> $this->input->post('asset_account_id',true),
			'warehouse_id'			=> $this->input->post('warehouse_id',true),
			'item_qty'				=> $this->input->post('item_qty',true),
			'min_stock'				=> $this->input->post('min_stock',true)
		);

		$item_id = '';
		
        if ( $this->input->post('item_id', true) == '' || $this->input->post('item_id', true) == null )
		{
            $item['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
            $item['created_dt'] = date('Y-m-d H:i:s');

            $this->db->insert('tb_m_items', $item);
            $item_id = $this->db->insert_id();
			
        } else {
            $item_id = $this->input->post('item_id', true);

            $item['changed_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
            $item['changed_dt'] = date('Y-m-d H:i:s');

            // update header
            $this->db->where('item_id', $item_id);
            $this->db->update('tb_m_items', $item);
        }

        return $item_id;
    }
	
	function check_item($item_code){
		$sql = "
			SELECT
				count(item_id) as cnt
			FROM
				tb_m_items
			WHERE
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "' and
				item_code = '" . $item_code . "' 
		";

		return $this->db->query($sql)->row()->cnt;
	}
	
	function quick_save_category() {
	
	    $category = array
            (
            'company_id' => $this->session->userdata(S_COMPANY_ID),
            'system_type' => 'item_category',
            'system_code' => $this->input->post('category_name', true),
            'system_value_txt' => $this->input->post('category_name', true),
            'system_value_num' => '0'
        );
							
			$category['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
			$category['created_dt'] = date('Y-m-d H:i:s');

			$this->db->insert('tb_m_company_prefferences', $category);

			return $this->db->insert_id();
			
            
    }
	
	function check_category($system_code){
		$sql = "
			SELECT
				count(*) as cnt
			FROM
				tb_m_company_prefferences
			WHERE
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "' and
				system_code = '" . $system_code . "' 
		";

		return $this->db->query($sql)->row()->cnt;
	}
	
	function get_category($item_category_id) {
		
		$sql = "
			SELECT
				system_code,
				system_value_txt
			FROM
				tb_m_company_prefferences
			WHERE
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "' and
				system_type = 'item_category' and 
				preff_id = '" . $item_category_id . "'

			ORDER BY
				system_code
		";

        return $this->db->query($sql)->result();
	}
}

?>
