<?php
/*
* @author: Irfan Satriadarma
* @created: 12 Maret 2020
*/

class Logistic_point_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
    }

    function getMaterArea($keyword = '')
    {
        $sql = "select *
                from tb_m_area
                where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                and area_cd like '%" . $keyword . "%'
                order by area_cd";
        return $this->db->query($sql)->result();
    }

    function getAreas($keyword = '')
    {
        $sql = "select distinct lp_area
                from tb_m_logistic_point
                where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                and lp_area like '%" . $keyword . "%'
                order by lp_area";
        return $this->db->query($sql)->result();
    }

    function getLogisticPoints($lp_type_id, $lp_area, $start, $length, $sv, $order, $columns)
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {            
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }

        $where_lp_type = ($lp_type_id != 'all') ? " and a.lp_type_id = '" . $lp_type_id . "'" : '';
        $where_lp_area = ($lp_area != 'all') ? " and a.lp_area = '" . $lp_area . "'" : '';

        $sql = "
			select 
                a.lp_cd, a.lp_name, a.lp_address, a.lp_area, a.geofenceid
                , b.lp_type_name, a.lp_remark, a.lp_business, lp_color
			from 
				tb_m_logistic_point a inner join tb_m_logistic_point_type b on b.lp_type_id = a.lp_type_id
			where
				(
                    lp_cd like '%" . $sv . "%'
                    or lp_name like '%" . $sv . "%'
                    or lp_address like '%" . $sv . "%'
                    or lp_area like '%" . $sv . "%'
                    or geofenceid like '%" . $sv . "%'
                    or lp_color like '%" . $sv . "%'
                )
                " . $where_lp_type . $where_lp_area . "
            order by " . $order_by . " ";
            
		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
		return $this->db->query($sql)->result();
    }

    function getCountOfLogisticPoint($lp_type_id, $lp_area, $sv)
    {
        $where_lp_type = ($lp_type_id != 'all') ? " and a.lp_type_id = '" . $lp_type_id . "'" : '';
        $where_lp_area = ($lp_area != 'all') ? " and a.lp_area = '" . $lp_area . "'" : '';

        $sql = "
			select 
                count(a.lp_cd) as cnt
			from 
				tb_m_logistic_point a inner join tb_m_logistic_point_type b on b.lp_type_id = a.lp_type_id
			where
				(
                    lp_cd like '%" . $sv . "%'
                    or lp_name like '%" . $sv . "%'
                    or lp_address like '%" . $sv . "%'
                    or lp_area like '%" . $sv . "%'
                    or geofenceid like '%" . $sv . "%'
                    or lp_color like '%" . $sv . "%'
                )
                " . $where_lp_type . $where_lp_area . "
		";
		return $this->db->query($sql)->row()->cnt;
    }

    function getLogisticPoint($lp_cd)
    {
        $sql = "
            select 
                a.lp_cd,a.lp_type_id, a.lp_name, a.lp_address, a.lp_area, a.lp_remark, a.lp_business, a.geofenceid
                , b.lp_type_name, a.lp_color, a.empty_plan_flg
            from 
                tb_m_logistic_point a inner join tb_m_logistic_point_type b on b.lp_type_id = a.lp_type_id
            where
                md5(lp_cd) = '" .$lp_cd . "'
        ";
        return $this->db->query($sql)->result();
    }

    function delete($lp_cd)
    {        
        $this->db->where('md5(lp_cd)', $lp_cd);
        $this->db->delete('tb_m_logistic_point');
    }

    function save($update = '')
    {
        // validate lp_cd
        $check_lp = $this->getLogisticPoint(md5($this->input->post('lp_cd', true)));
        if (count($check_lp) == 0 || $update == '1')
        {
            $lp = array
            (
                'company_id' => $this->session->userdata(S_COMPANY_ID),
                'lp_cd' => $this->input->post('lp_cd', true),
                'lp_name' => $this->input->post('lp_name', true),
                'lp_address' => $this->input->post('lp_address', true),
                'lp_area' => $this->input->post('lp_area', true),
                'lp_remark' => $this->input->post('lp_remark', true),
                'lp_business' => $this->input->post('lp_business', true),
                'geofenceid' => $this->input->post('geofenceid', true),
                'lp_type_id' => $this->input->post('lp_type_id', true),
                'lp_color' => $this->input->post('lp_color', true),
                'empty_plan_flg' => $this->input->post('empty_plan_flg', true)
            );

            if ($update == '')
            {
                $lp['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
                $lp['created_dt'] = date('Y-m-d H:i:s');

                $this->db->insert('tb_m_logistic_point', $lp);
                
                return '1';
            }
            else 
            {
                $lp['changed_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
                $lp['changed_dt'] = date('Y-m-d H:i:s');

                $this->db->where('lp_cd', $this->input->post('lp_cd', true));
                $this->db->update('tb_m_logistic_point', $lp);

                return '2'; // update success
            }    
        }
        else
        {
            return '0'; // lp_cd already exists
        }        
    }

    function getLPForDownload()
    {
        $sql = "
            Select * from tb_m_logistic_point where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
            order by lp_cd
        ";
        return $this->db->query($sql)->result();
    }
}