<?php
/*
* @author: Irfan Satriadarma
* @created: 16 Maret 2020
*/

class Dashboard_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
    }

    function getCustomerLpCds($keyword = '')
    {
        $sql = "select 
                    lp_cd, 
                    lp_name 
                from 
                    tb_m_logistic_point
                where lp_name like '%" . $keyword . "%'
                order by lp_cd, lp_name";
        return $this->db->query($sql)->result();

    }
    
    function getDepartureDelay($customer_lp_cd, $timetable_dt)
    {
        // $sql = "call sp_departure_delay(
		// 	'" . $customer_lp_cd . "'
		// 	, '" . $timetable_dt . "'
        // );";
        $wlp = ($customer_lp_cd != 'all') ? " and b.customer_lp_cd = '" . $customer_lp_cd . "'" : '';
        $sql = "
            SELECT
                b.customer_lp_cd,
                a.route,
                a.cycle,
                a.timetable_id,
                a.timetable_dt,
                a.lp_cd,
                a.vehicle_cd,
                a.driver_cd,
                a.driver_name,	
                cast(concat(a.departure_dt, ' ', a.departure_plan) AS DATETIME) AS departure_plan_dt, 
                a.departure_actual,
                TIME_FORMAT( TIMEDIFF(NOW(), cast(concat(a.departure_dt, ' ', a.departure_plan) AS DATETIME)), '%H:%i') AS delay,
                a.remark

            FROM 
                tb_r_timetable_detail a
                INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                INNER JOIN tb_m_logistic_point c ON c.lp_cd = a.lp_cd	
            WHERE 
                a.timetable_dt = '" . $timetable_dt . "'
                AND c.empty_plan_flg = '1'
                AND a.arrival_plan IS NULL 
                AND a.departure_plan IS NOT null
                AND a.departure_actual IS NULL
                AND cast(concat(a.departure_dt, ' ', a.departure_plan) AS DATETIME) < NOW()
                AND a.vehicle_cd <> 'STANDBY' and a.vehicle_cd <> 'FIELDMAN'
                " . $wlp . "
            ORDER BY
                b.customer_lp_cd
                , a.route
                , a.cycle
                , cast(concat(a.departure_dt, ' ', a.departure_plan) AS DATETIME) ASC;
        ";

		// echo $sql;
		$result = $this->db->query($sql)->result();
		// mysqli_next_result($this->db->conn_id);
		return $result;
    }

    function getArrivalDelay($customer_lp_cd, $timetable_dt)
    {
        $wclc = ($customer_lp_cd != 'all') ? " and b.customer_lp_cd = '" . $customer_lp_cd . "'" : '';
        $sql = "
            SELECT
                b.customer_lp_cd,
                a.route,
                a.cycle,
                a.timetable_id,
                a.timetable_dt,
                a.lp_cd,
                a.vehicle_cd,
                a.driver_cd,
                a.driver_name,	
                cast(concat(a.arrival_dt, ' ', a.arrival_plan) AS DATETIME) AS arrival_plan_dt, 
                a.arrival_actual
                , TIME_FORMAT( TIMEDIFF(NOW(), cast(concat(a.arrival_dt, ' ', a.arrival_plan) AS DATETIME)), '%H:%i') AS delay,
                a.remark
            FROM 
                tb_r_timetable_detail a
                INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                INNER JOIN tb_m_logistic_point c ON c.lp_cd = a.lp_cd	
            WHERE 
                a.timetable_dt = '" . $timetable_dt . "'
                " . $wclc . "
                AND c.empty_plan_flg = '0'
                AND a.lp_cd = b.customer_lp_cd
                AND arrival_actual IS NULL 
                AND cast(concat(a.arrival_dt, ' ', a.arrival_plan) AS DATETIME) < NOW()	
            ORDER BY
                b.customer_lp_cd
                , a.route
                , a.cycle
                , cast(concat(a.arrival_dt, ' ', a.arrival_plan) AS DATETIME) ASC;
        ";
        return $this->db->query($sql)->result();
    }    
}
