<?php
/*
* @author: uye
* @created: 12 Mei 2018
*/

class Attendance_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}


	function getSystemMaster($systemType, $systemCode = "", $isRow = true)
	{
		$this->db->select("system_type, system_code, system_value_txt, system_value_num");
		$this->db->from('tb_m_system');
		//$this->db->where('system_code', $companyId);
		$this->db->where('system_type', $systemType);
		if ($isRow) $this->db->limit(1);

		if ($systemCode != "") $this->db->where('system_code', $systemCode);

		$query = $this->db->get();
		//$this->output->enable_profiler(TRUE);
		if ($query->num_rows() > 0) {
			if ($isRow) return $query->row();
			else return $query->result();
		} else {
			return (object) array("system_value_txt"=>"");
		}
	}

	public function cekDuplicate($driver_cd, $datetime)
	{
		$sql = "select * from tb_r_attendance where driver_cd = '$driver_cd' and attendance_dt='$datetime' ";
		return $this->db->query($sql)->result_array();
	}

	public function cekAttendance($driver_cd, $val)
	{
		$sql = "select * from tb_r_attendance where driver_cd = '$driver_cd' and clock_in = '$val' ";
		
		return $this->db->query($sql)->num_rows();
	}

	public function getIpAddress($appid)
	{

		$sql = "select system_code, system_value_txt from tb_m_system where system_type='attendance' ";

		if ($appid != "") {
			$sql = $sql . " and system_value_num = '" . $appid . "'";
		}


		return $this->db->query($sql)->result_array();
	}

	function getDriverCd($attId)
	{
		$this->db->select("driver_cd");
		$this->db->from('tb_m_driver');
		$this->db->where('att_id', $attId);

		$query = $this->db->get();
		return $query->row();
	}

	function getAttId($driverCd)
	{
		$sql = "select attendance_id, attendance_dt,rollcall_id from tb_r_attendance where driver_cd = '" . $driverCd . "' and clock_out is null order by attendance_id desc limit 1 ";


		return  $this->db->query($sql)->row();
	}

	function getAuroReconTime(){
		$sql = "select system_value_txt from tb_m_system where system_type='attendance' and system_code ='auto_recon'";

		return $this->db->query($sql)->result_array();
	}

	function getLog($appid = "")
	{
		$sql = 'SELECT
				DATE_FORMAT(x.dttm,"%d %M %Y %k:%i:%S") as dttm,
				x.dttm as datetm, 
				x.app_id,
				x.ipaddress,
				y.message,
				y.STATUS  from (
			SELECT
				max(a.datetime) as dttm, a.app_id, a.ipaddress 
			FROM
				tb_r_log_attendance a where 1 =1
			
			 ';

		if ($appid != "") {
			$sql = $sql . " and app_id ='" . $appid . "' ";
		}

		$sql = $sql . "group by a.app_id, a.ipaddress,
		a.status ) x inner join tb_r_log_attendance y
		on y.datetime = x.dttm where y.status <> 'Connected'  order by datetm desc ;";

		//echo $sql;

		return $this->db->query($sql)->result_array();
	}

	function saveLog($data)
	{

		$this->db->insert('tb_r_log_attendance', $data);
		return 1;
	}


	function insertAttendanceQueue($data)
	{
		$this->db->insert_batch('tb_r_attendance', $data);
	}


	function updateAttendanceQueue($data)
	{
		$iMax = count($data);

		for ($i = 0; $i < $iMax; $i++) {

			if (array_key_exists('clock_in', $data[$i])) {
				
					$dUpdate = array(
						'clock_in' => $data[$i]['clock_in'],
						'rollcall_id' => null,
						'changed_by' => $data[$i]['changed_by'],
						'changed_dt' => $data[$i]['changed_dt'],
	
					);
				
				
			} else {
				$dUpdate = array(
					'clock_out' => $data[$i]['clock_out'],
					'changed_by' => $data[$i]['changed_by'],
					'changed_dt' => $data[$i]['changed_dt'],
				);

				$this->updateRollCall($data[$i]);
			}
			$this->db->where('attendance_id', $data[$i]['attendance_id']);
			$this->db->where('driver_cd', $data[$i]['driver_cd']);
			$this->db->update('tb_r_attendance', $dUpdate);
		}

		return 1;
	}


	function updateRollCall($data)
	{

		//print_r($data);

		if (array_key_exists('rollcall_id', $data)) {
			if (is_numeric($data['rollcall_id']) && $data['rollcall_id'] > 0) {
				$dUpdate = array(
					'arrival_actual' => $data['clock_out'],
					'changed_by' => $data['changed_by'],
					'changed_dt' => date('Y-m-d H:i:s'),
				);

				$this->db->where('rollcall_id', $data['rollcall_id']);
				$this->db->where('driver_cd', $data['driver_cd']);
				$this->db->update('tb_r_rollcall', $dUpdate);

				// 17-Juli-2020
				// inject update timetable kepulangan.
				
				$sql = "
					UPDATE tb_r_timetable_detail a
					INNER JOIN tb_r_rollcall b 
					ON b.timetable_dt = a.timetable_dt 
							AND b.route = a.route 
							AND b.cycle = a.cycle 
							AND a.driver_cd = b.driver_cd
							AND a.vehicle_cd = b.vehicle_cd		
					INNER JOIN tb_m_logistic_point c ON c.lp_cd = a.lp_cd AND c.empty_plan_flg = '1'
					
					SET
						a.departure_actual = '" . $data['clock_out'] . "'
					WHERE
						b.rollcall_id = '" . $data['rollcall_id'] . "'
						AND a.departure_plan IS null
				";
				$this->db->query($sql);

			}
		}
	}

	// function insertLog($data){
	// 	$this->db->insert('tb_h_log_attendance', $data);
	// 	return $this->db->insert_id();
	// }

	// function updateLog($processId, $companyId, $data){
	// 	$this->db->where('process_id', $processId);
	// 	$this->db->where('company_id', $companyId);
	// 	$this->db->update('tb_h_log_attendance', $data);
	// }



}
