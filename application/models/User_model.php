<?php

/*
 * @author: misbah@arkamaya.co.id
 * @created: 2017-02-06
 */

class User_model extends CI_Model {

    function __construct() {
        parent:: __construct();
		
		$this->search_col = array(
            "TB.user_email",
            "TB.full_name",
            "TB.user_group_description",
            "TB.is_active_text",
            "TB.super_admin_text",
            "TB.email_confirmed_text",
        );
    }
	
	function query($keyword){
        $this->db->select('*');
        $this->db->from("(
			SELECT 
				u.*
				,ug.user_group_description
				,CASE WHEN u.is_active = '1' THEN 'Aktif' ELSE 'Tidak Aktif' END AS is_active_text
				,CASE WHEN super_admin = '1' THEN 'Ya' ELSE 'Tidak' END AS super_admin_text
				,CASE WHEN u.email_confirmed = '1' THEN 'Sudah Konfirmasi' ELSE 'Belum Konfirmasi' END AS email_confirmed_text
				, m.employee_name
			FROM 
				tb_m_users u
			LEFT JOIN tb_m_user_group ug ON ug.user_group_id = u.user_group_id
			LEFT JOIN tb_m_employee m on m.employee_id = u.employee_id
		) TB");

		$i = 0;
		$where = "";
		$cnt = count($this->search_col) -1;
		foreach ($this->search_col as $item)
		{

			if ($i == 0){
					$where = "( ".$where . "UPPER(CAST(" . $item ." AS char)) LIKE UPPER('%$keyword%') ";
			}else if ($i == $cnt ){
					$where = $where ."OR " . "UPPER(CAST(" . $item ." AS char)) LIKE UPPER('%$keyword%') )";
			}else{
					$where = $where ."OR " . "UPPER(CAST(" . $item ." AS char)) LIKE UPPER('%$keyword%') ";
			}
			$i++;
		}
		$this->db->where($where);
        $this->db->where('company_id', $this->session->userdata(S_COMPANY_ID));

    }
	
	function getData($start = '', $length = '', $keyword = '') {
        $this->query($keyword);
        $this->db->order_by('user_email');
        if ($start >= 0 && $length >= 0) {
            $this->db->limit($length, $start);
        }
        return $this->db->get()->result();
    }
	
    function countData($keyword = ''){
        $this->query($keyword);
        return $this->db->count_all_results();
       
    }
	
	//=======================================================================================
	
	function get_user_group() {
        $this->db->select('*');
        $this->db->from('tb_m_user_group');
        $this->db->where('company_id', $this->session->userdata(S_COMPANY_ID));
        return $this->db->get()->result();
    }
	
	function check_email($user_email) {
        $sql = "SELECT * FROM tb_m_users where user_email = '".$user_email."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return 'false';
        } else {
            return 'true';
        }
    }

    function saveData($mode, $data, $id = "") {
        if ($mode == 'insert') {
            $this->db->insert('tb_m_users', $data);
            return $this->db->affected_rows();
        } else {
            $this->db->where('user_email', $id);
            $this->db->where('company_id', $this->session->userdata(S_COMPANY_ID));
            $this->db->update('tb_m_users', $data);
            return $this->db->affected_rows();
        }
    }
	
	function getDataById($id) {
		
        $query = $this->db->query(
			"SELECT 
				u.*
				,ug.user_group_description
				,CASE WHEN u.is_active = '1' THEN 'Aktif' ELSE 'Tidak Aktif' END AS is_active_text
				,CASE WHEN super_admin = '1' THEN 'Ya' ELSE 'Tidak' END AS super_admin_text
				,CASE WHEN u.email_confirmed = '1' THEN 'Sudah Konfirmasi' ELSE 'Belum Konfirmasi' END AS email_confirmed_text
				, m.employee_name
			FROM 
				tb_m_users u
			LEFT JOIN tb_m_user_group ug ON ug.user_group_id = u.user_group_id
			LEFT JOIN tb_m_employee m on m.employee_id = u.employee_id
			WHERE u.company_id='".$this->session->userdata(S_COMPANY_ID)."'
			AND md5(u.user_email) = '".$id."'"
		);
        return $query->row();
    }

    function delete($user_email) {
        try {
			$this->db->where('user_email', $user_email);
			$this->db->where('company_id', $this->session->userdata(S_COMPANY_ID));
			$this->db->delete('tb_m_users');
			
			// remove from employee
			$this->db->query("update tb_m_employee set user_email = null, work_email = null where user_email = '" . $user_email . "'");
			return true;
		} catch (Exception $e) {								
			return false;		
		}
    }
	
	function do_confirm($email_confirmed_code){
		$data =  array(
			'email_confirmed' => 1
		);
		$this->db->where('email_confirmed_code', $email_confirmed_code);
		$this->db->update('tb_m_users', $data);
		return $this->db->affected_rows();
	}
	
	// 20170407 irfan@arkamaya.com
	// untuk mengambil data employee yang belum di assign user nya
	function get_employee_to_assign()
	{
		$sql = "select a.employee_id, a.employee_name, a.user_email, a.work_email
				from 
					tb_m_employee a 
				where 
					a.company_id = '".$this->session->userdata(S_COMPANY_ID)."' 
					and (a.user_email is null or a.work_email is null)
				order by a.employee_name
				";
		return $this->db->query($sql)->result();
	}
	
	// 20170407 irfan@arkamaya.com 
	// untuk cek jumlah user yg diperbolehkan sesuai paket
	function get_usercount()
	{
		$sql = "select default_users from tb_m_package a 
			inner join tb_m_company b on b.package_id = a.package_id 
			where b.company_id = '".$this->session->userdata(S_COMPANY_ID)."'";
		return $this->db->query($sql)->row()->default_users;
	}
}
