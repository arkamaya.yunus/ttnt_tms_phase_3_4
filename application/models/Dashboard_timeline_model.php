<?php
/*
* @author: Irfan Satriadarma
* @created: 16 Maret 2020
*/

class Dashboard_timeline_model extends CI_Model
{
	function __construct()
	{
        parent:: __construct();
    }

    function getTimeline($get_from, $get_to)
    {
        $sql = "
								SELECT
									CONCAT(a.vehicle_cd, a.route, a.cycle) AS keytable,
									a.vehicle_cd,
									a.route,
									a.cycle,
									a.lp_cd,
									TIME_FORMAT(a.departure_plan,'%H:%i') as departure_plan,
									TIME_FORMAT(a.arrival_plan,'%H:%i') as arrival_plan,
									b.lp_color
								FROM
									tb_r_timetable_detail a
									INNER JOIN tb_m_logistic_point b ON b.lp_cd = a.lp_cd
								WHERE
									a.timetable_dt between '".$get_from."' AND '".$get_to."'
									AND (a.departure_plan IS NOT NULL OR a.arrival_plan IS not NULL)
								ORDER BY a.timetable_id, a.route, a.cycle, a.timetable_detail_id, a.departure_plan;
								";
        return $this->db->query($sql)->result();
    }

		function getTimelineDetail($from_db, $to_db)
		{
			$sql = "
							SELECT
								CONCAT(a.vehicle_cd, a.route, a.cycle) AS keytable,
								a.vehicle_cd,
								a.route,
								a.cycle,
								a.lp_cd,
								TIME_FORMAT(a.departure_plan,'%H:%i') as departure_plan,
								TIME_FORMAT(a.arrival_plan,'%H:%i') as arrival_plan,
								b.lp_color
							FROM
								tb_r_timetable_detail a
								INNER JOIN tb_m_logistic_point b ON b.lp_cd = a.lp_cd
							WHERE
								a.timetable_dt between '".$from_db."' AND '".$to_db."'
								AND (a.departure_plan IS NOT NULL OR a.arrival_plan IS not NULL)

							ORDER BY a.timetable_id, a.route, a.cycle, a.timetable_detail_id, a.departure_plan;
							";
							return $this->db->query($sql)->result();
		}


}
