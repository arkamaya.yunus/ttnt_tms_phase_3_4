<?php

/*
 * @author: irfan.satriadarma@gmail.com
 * @created: 2018-01-16
 */

class Shared_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
    }
	
	function get_system_value($system_type = '', $system_code = '', $field = 'system_value_txt')
	{
        $this->db->select('system_value_txt, system_value_num, system_value_time');
        $this->db->from('tb_m_system');
        
		if ($system_type != '') $this->db->where('system_type', $system_type);
        if ($system_code != '') $this->db->where('system_code', $system_code); 
		
        $query = $this->db->get();
        return ($query->num_rows() == 1) ? $query->row()->$field : '';
    }
	
	function get_account_by_type($type)
	{				

		$sql = "
			select * from tb_m_account where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "' and account_type = '" . $type . "' order by account_name
		";

		return $this->db->query($sql)->result_array();
	}
}