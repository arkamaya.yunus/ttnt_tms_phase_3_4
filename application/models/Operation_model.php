<?php
/*
* @author: Irfan Satriadarma
* @created: 16 Maret 2020
*/

class Operation_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
    }

    function getTimetableSummary($timetable_dt, $business, $customer, $customer_lp_cd, $route, $cycle, $start, $length, $sv, $order, $columns, $nya = '')
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {            
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }

        $timetable_dt = date('Y-m-d', strtotime( str_replace('/', '-', $timetable_dt)));

        $w_business = ($business != 'all') ? " and b.business = '" . $business . "'" : '';
        $w_customer = ($customer != 'all') ? " and b.customer = '" . $customer . "'" : '';
        $w_customer_lp_cd = ($customer_lp_cd != 'all') ? " and b.customer_lp_cd = '" . $customer_lp_cd . "'" : '';
        $w_route = ($route != 'all') ? " and a.route = '" . $route . "'" : '';
        $w_cycle = ($cycle != 'all') ? " and a.cycle = '" . $cycle . "'" : '';
       
        $sql = "
                SELECT 
                    business, customer, customer_lp_cd,
                    route, `cycle`, vehicle_cd, 
                    GROUP_CONCAT(DISTINCT driver_name) as driver_name, 
                    GROUP_CONCAT(DISTINCT driver_cd) as driver_cd,
                    SUM(done) AS done, SUM(total) AS total, 
                    SUM(total)-SUM(done) AS remain,                     
                    round(( SUM(done)/SUM(total) * 100 ),0) AS percentage ,
                    timetable_id                    
                FROM
                (
                    SELECT 
                        b.business,
                        b.customer,
                        b.customer_lp_cd,
                        a.route, 
                        a.`cycle`, 
                        a.vehicle_cd, 
                        a.driver_name, 
                        a.driver_cd, 
                        COUNT(a.timetable_detail_id) AS done,
                        0 AS total ,
                        a.timetable_id 
                    FROM 
                        tb_r_timetable_detail a
                        INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                    WHERE                     
                        b.timetable_dt = '" . $timetable_dt . "'
                        and a.departure_actual IS NOT NULL
                        AND a.departure_plan IS NOT null
                        " . $w_business . $w_customer . $w_customer_lp_cd . $w_route . $w_cycle . "
                    GROUP BY  
                        b.business, b.customer, b.customer_lp_cd,  a.route, a.`cycle`, a.driver_cd
                    
                    UNION all
                    
                    SELECT 
                        b.business,
                        b.customer,
                        b.customer_lp_cd,
                        a.route, 
                        a.`cycle`, 
                        a.vehicle_cd, 
                        a.driver_name, 
                        a.driver_cd, 
                        0 AS done,
                        COUNT(a.timetable_detail_id) AS total ,
                        a.timetable_id 
                    FROM 
                        tb_r_timetable_detail a
                        INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                    WHERE 
                        b.timetable_dt = '" . $timetable_dt . "'
                        AND a.departure_plan IS NOT null
                        " . $w_business . $w_customer . $w_customer_lp_cd . $w_route . $w_cycle . "
                    GROUP BY  
                        b.business, b.customer, b.customer_lp_cd,  a.route, a.`cycle`, a.driver_cd
                ) t
                WHERE
                    (
                        t.business like '%" . $sv . "%'
                        or t.customer like '%" . $sv . "%'
                        or t.customer_lp_cd like '%" . $sv . "%'
                        or t.route like '%" . $sv . "%'
                        or t.`cycle` like '%" . $sv . "%'
                        or t.vehicle_cd like '%" . $sv . "%'
                        or t.driver_name like '%" . $sv . "%'
                    )
                GROUP BY t.business, t.customer, t.customer_lp_cd, t.route, t.cycle
                ORDER BY " . $order_by . " ";
                
        // nya : not yet arrived.
        // hanya menampilkan rute yg actual logistic point customer nya kosong
        if ($nya != '')
        {
            $sql = "
            SELECT 
                    business, customer, customer_lp_cd,
                    route, `cycle`, vehicle_cd, 
                    GROUP_CONCAT(DISTINCT driver_name) as driver_name, 
                    GROUP_CONCAT(DISTINCT driver_cd) as driver_cd,
                    SUM(done) AS done, SUM(total) AS total, 
                    SUM(total)-SUM(done) AS remain,                     
                    round(( SUM(done)/SUM(total) * 100 ),0) AS percentage ,
                    timetable_id                    
                FROM
                (
                    SELECT 
                        b.business,
                        b.customer,
                        b.customer_lp_cd,
                        a.route, 
                        a.`cycle`, 
                        a.vehicle_cd, 
                        a.driver_name, 
                        a.driver_cd, 
                        COUNT(a.timetable_detail_id) AS done,
                        0 AS total ,
                        a.timetable_id 
                    FROM 
                        tb_r_timetable_detail a
                        INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                    WHERE                     
                        b.timetable_dt = '" . $timetable_dt . "'
                        and a.lp_cd = b.customer_lp_cd
                        and a.arrival_actual is null
                        " . $w_business . $w_customer . $w_customer_lp_cd . $w_route . $w_cycle . "
                    GROUP BY  
                        b.business, b.customer, b.customer_lp_cd,  a.route, a.`cycle`, a.driver_cd
                    
                    UNION all
                    
                    SELECT 
                        b.business,
                        b.customer,
                        b.customer_lp_cd,
                        a.route, 
                        a.`cycle`, 
                        a.vehicle_cd, 
                        a.driver_name, 
                        a.driver_cd, 
                        0 AS done,
                        COUNT(a.timetable_detail_id) AS total ,
                        a.timetable_id 
                    FROM 
                        tb_r_timetable_detail a
                        INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                    WHERE 
                        b.timetable_dt = '" . $timetable_dt . "'
                        AND a.departure_plan IS NOT null
                        " . $w_business . $w_customer . $w_customer_lp_cd . $w_route . $w_cycle . "
                    GROUP BY  
                        b.business, b.customer, b.customer_lp_cd,  a.route, a.`cycle`, a.driver_cd
                ) t
                WHERE
                    (
                        t.business like '%" . $sv . "%'
                        or t.customer like '%" . $sv . "%'
                        or t.customer_lp_cd like '%" . $sv . "%'
                        or t.route like '%" . $sv . "%'
                        or t.`cycle` like '%" . $sv . "%'
                        or t.vehicle_cd like '%" . $sv . "%'
                        or t.driver_name like '%" . $sv . "%'
                    )
                GROUP BY t.business, t.customer, t.customer_lp_cd, t.route, t.cycle
                ORDER BY " . $order_by . " ";
            // echo $sql;
        }

		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
		return $this->db->query($sql)->result();
    }

    function getCountOfTimetableSummary($timetable_dt, $business, $customer, $customer_lp_cd, $route, $cycle, $sv, $nya = '')
    {
        $timetable_dt = date('Y-m-d', strtotime( str_replace('/', '-', $timetable_dt)));

        $w_business = ($business != 'all') ? " and b.business = '" . $business . "'" : '';
        $w_customer = ($customer != 'all') ? " and b.customer = '" . $customer . "'" : '';
        $w_customer_lp_cd = ($customer_lp_cd != 'all') ? " and b.customer_lp_cd = '" . $customer_lp_cd . "'" : '';
        $w_route = ($route != 'all') ? " and a.route = '" . $route . "'" : '';
        $w_cycle = ($cycle != 'all') ? " and a.cycle = '" . $cycle . "'" : '';

        $sql = "
                SELECT 
                    t.route
                FROM
                (
                    SELECT 
                        b.business,
                        b.customer,
                        b.customer_lp_cd,
                        a.route, 
                        a.`cycle`, 
                        a.vehicle_cd, 
                        GROUP_CONCAT(DISTINCT driver_name) as driver_name, 
                        GROUP_CONCAT(DISTINCT driver_cd) as driver_cd,
                        COUNT(a.timetable_detail_id) AS done,
                        0 AS total,
                        a.timetable_id 
                    FROM 
                        tb_r_timetable_detail a
                        INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                    WHERE                     
                        b.timetable_dt = '" . $timetable_dt . "'
                        and a.departure_actual IS NOT NULL
                        AND a.departure_plan IS NOT null
                        " . $w_business . $w_customer . $w_customer_lp_cd . $w_route . $w_cycle . "
                    GROUP BY  
                        b.business, b.customer, b.customer_lp_cd,  a.route, a.`cycle`, a.driver_cd
                    
                    UNION all
                    
                    SELECT 
                        b.business,
                        b.customer,
                        b.customer_lp_cd,
                        a.route, 
                        a.`cycle`, 
                        a.vehicle_cd, 
                        a.driver_name, 
                        a.driver_cd, 
                        0 AS done,
                        COUNT(a.timetable_detail_id) AS total ,
                        a.timetable_id 
                    FROM 
                        tb_r_timetable_detail a
                        INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                    WHERE 
                        b.timetable_dt = '" . $timetable_dt . "'
                        AND a.departure_plan IS NOT null
                        " . $w_business . $w_customer . $w_customer_lp_cd . $w_route . $w_cycle . "
                    GROUP BY  
                        b.business, b.customer, b.customer_lp_cd,  a.route, a.`cycle`, a.driver_cd
                ) t
                WHERE
                    (
                        t.business like '%" . $sv . "%'
                        or t.customer like '%" . $sv . "%'
                        or t.customer_lp_cd like '%" . $sv . "%'
                        or t.route like '%" . $sv . "%'
                        or t.`cycle` like '%" . $sv . "%'
                        or t.vehicle_cd like '%" . $sv . "%'
                        or t.driver_name like '%" . $sv . "%'
                    )
                GROUP BY t.business, t.customer, t.customer_lp_cd, t.route, t.cycle";

        // nya : not yet arrived.
        // hanya menampilkan rute yg actual logistic point customer nya kosong
        if ($nya != '')
        {
            $sql = "
            SELECT 
                    business, customer, customer_lp_cd,
                    route, `cycle`, vehicle_cd, 
                    GROUP_CONCAT(DISTINCT driver_name) as driver_name, 
                    GROUP_CONCAT(DISTINCT driver_cd) as driver_cd,
                    SUM(done) AS done, SUM(total) AS total, 
                    SUM(total)-SUM(done) AS remain,                     
                    round(( SUM(done)/SUM(total) * 100 ),0) AS percentage ,
                    timetable_id                    
                FROM
                (
                    SELECT 
                        b.business,
                        b.customer,
                        b.customer_lp_cd,
                        a.route, 
                        a.`cycle`, 
                        a.vehicle_cd, 
                        a.driver_name, 
                        a.driver_cd, 
                        COUNT(a.timetable_detail_id) AS done,
                        0 AS total ,
                        a.timetable_id 
                    FROM 
                        tb_r_timetable_detail a
                        INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                    WHERE                     
                        b.timetable_dt = '" . $timetable_dt . "'
                        -- and a.departure_actual IS NOT NULL
                        -- AND a.departure_plan IS NOT null
                        and a.lp_cd = b.customer_lp_cd
                        and a.arrival_actual is null
                        " . $w_business . $w_customer . $w_customer_lp_cd . $w_route . $w_cycle . "
                    GROUP BY  
                        b.business, b.customer, b.customer_lp_cd,  a.route, a.`cycle`, a.driver_cd
                    
                    UNION all
                    
                    SELECT 
                        b.business,
                        b.customer,
                        b.customer_lp_cd,
                        a.route, 
                        a.`cycle`, 
                        a.vehicle_cd, 
                        a.driver_name, 
                        a.driver_cd, 
                        0 AS done,
                        COUNT(a.timetable_detail_id) AS total ,
                        a.timetable_id 
                    FROM 
                        tb_r_timetable_detail a
                        INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                    WHERE 
                        b.timetable_dt = '" . $timetable_dt . "'
                        AND a.departure_plan IS NOT null
                        " . $w_business . $w_customer . $w_customer_lp_cd . $w_route . $w_cycle . "
                    GROUP BY  
                        b.business, b.customer, b.customer_lp_cd,  a.route, a.`cycle`, a.driver_cd
                ) t
                WHERE
                    (
                        t.business like '%" . $sv . "%'
                        or t.customer like '%" . $sv . "%'
                        or t.customer_lp_cd like '%" . $sv . "%'
                        or t.route like '%" . $sv . "%'
                        or t.`cycle` like '%" . $sv . "%'
                        or t.vehicle_cd like '%" . $sv . "%'
                        or t.driver_name like '%" . $sv . "%'
                    )
                GROUP BY t.business, t.customer, t.customer_lp_cd, t.route, t.cycle
                ";

        }

        return count($this->db->query($sql)->result());
    }
}