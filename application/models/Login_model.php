<?php
/*
* @author: irfan.satriadarma@gmail.com
* @created: 2016-12-19
*/
class Login_model extends CI_Model 
{
	function __construct()
	{
		parent:: __construct();
	}
	
	function auth_user($user_email)
	{
		$sqlAuth = "
			select 
				tb_m_users.user_email
				, tb_m_users.full_name
				, tb_m_users.user_group_id
                , tb_m_users.user_password
				, tb_m_users.company_id
				, tb_m_users.email_confirmed
				, tb_m_company.company_name
				, tb_m_company.trial_expired
				, tb_m_user_group.is_admin
				, tb_m_user_group.default_landing
				, tb_m_user_group.user_group_description
				, case when(tb_m_company.trial_expired <= now()) then 1 else 0 end as is_expired
			from 
				tb_m_users
				left join tb_m_company on tb_m_company.company_id = tb_m_users.company_id
				left join tb_m_user_group on tb_m_user_group.user_group_id = tb_m_users.user_group_id
			where 
				tb_m_users.user_email 			= '" . $user_email . "'
				and tb_m_users.is_active 		= '1' 
		";
		return $this->db->query($sqlAuth)->result();	
	}
	
	function last_logged_in($user_email)
	{
		$this->db->query(
		"
			update tb_m_users set user_last_logged_in = now() 
			where user_email = '" . $user_email . "'
		");
	}
    

	function getEmployeeByUname($email){
		$sql = "select user_email, full_name from tb_m_users where user_email = '".$email."'";
		$query = $this->db->query($sql);
		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			return false;
		}
	}
		
	function get_email_by_reset_password_code($k){
		$sql = "select user_email from tb_m_users where reset_password_code = '" . $k . "'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0)
		{
			return $result->row()->user_email;
		}
		else
		{
			return false;
		}
	}
	
	function get_email_by_confirmed_password_code($k){
		$sql = "select user_email from tb_m_users where email_confirmed_code = '" . $k . "'";
		$result = $this->db->query($sql);
		if ($result->num_rows() > 0)
		{
			return $result->row()->user_email;
		}
		else
		{
			return false;
		}
	}
	
	function reset_password($data, $email, $isReset = false, $key ="" ){
		$this->db->where('user_email', $email);
		
		if($isReset == true && $key != ""){
			$this->db->where('reset_password_code', $key);
		}
		$this->db->update('tb_m_users', $data);
		return $this->db->affected_rows();
	}
 
}
?>