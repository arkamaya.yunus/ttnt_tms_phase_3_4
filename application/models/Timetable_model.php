<?php
/*
* @author: Irfan Satriadarma
* @created: 16 Maret 2020
*/

class Timetable_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
    }

    function getCustomerLpCds($keyword = '')
    {
        $sql = "select 
                    lp_cd, 
                    lp_name 
                from 
                    tb_m_logistic_point
                where lp_name like '%" . $keyword . "%'
                order by lp_cd, lp_name";
        return $this->db->query($sql)->result();

    }

    function getLogisticPartners($keyword = '')
    {
        $sql = "select distinct logistic_partner
                from tb_m_driver
                where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                and logistic_partner like '%" . $keyword . "%'
                order by logistic_partner";
        return $this->db->query($sql)->result();
    }

    function getTimetables($business, $customer, $customer_lp_cd, $logistic_partner, $timetable_dt_s, $timetable_dt_e, $start, $length, $sv, $order, $columns)
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {            
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }

        $where_business = ($business != 'all') ? " and a.business = '" . $business . "'" : '';
        $where_customer = ($customer != 'all') ? " and a.customer = '" . $customer . "'" : '';
        $where_customer_lp_cd = ($customer_lp_cd != 'all') ? " and a.customer_lp_cd = '" . $customer_lp_cd . "'" : '';
        $where_logistic_partner = ($logistic_partner != 'all') ? " and a.logistic_partner = '" . $logistic_partner . "'" : '';
        $where_timetable_dt = ($timetable_dt_s != '' && $timetable_dt_e != '') ? " and a.timetable_dt between '" . date('Y-m-d', strtotime( str_replace('/', '-', $timetable_dt_s))) . "' and '" . date('Y-m-d', strtotime( str_replace('/', '-', $timetable_dt_e))) . "'" : '';
        $where_timetable_dt = ($timetable_dt_s != '' && $timetable_dt_e == '') ? " and a.timetable_dt >= '" . date('Y-m-d', strtotime( str_replace('/', '-', $timetable_dt_s))) . "'" : $where_timetable_dt;
        $where_timetable_dt = ($timetable_dt_s == '' && $timetable_dt_e != '') ? " and a.timetable_dt <= '" . date('Y-m-d', strtotime( str_replace('/', '-', $timetable_dt_e))) . "'" : $where_timetable_dt;
        
        $sql = "
			select 
                a.timetable_id,
                a.customer_lp_cd, a.logistic_partner, a.timetable_dt,
                b.lp_name, a.created_dt, a.created_by
                , a.business, a.customer
			from 
				tb_r_timetable a inner join tb_m_logistic_point b on b.lp_cd = a.customer_lp_cd
			where
				(
                    a.customer_lp_cd like '%" . $sv . "%'
                    or b.lp_name like '%" . $sv . "%'
                    or a.logistic_partner like '%" . $sv . "%'
                    or a.timetable_dt like '%" . $sv . "%'
                    or a.business like '%" . $sv . "%'
                    or a.customer like '%" . $sv . "%'
                )
                " . $where_business . $where_customer . $where_customer_lp_cd . $where_logistic_partner . $where_timetable_dt ."
            order by " . $order_by . " ";
            
		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
		return $this->db->query($sql)->result();
    }

    function getCountOfTimetable($business, $customer, $customer_lp_cd, $logistic_partner, $timetable_dt_s, $timetable_dt_e, $sv)
    {
        $where_business = ($business != 'all') ? " and a.business = '" . $business . "'" : '';
        $where_customer = ($customer != 'all') ? " and a.customer = '" . $customer . "'" : '';
        $where_customer_lp_cd = ($customer_lp_cd != 'all') ? " and a.customer_lp_cd = '" . $customer_lp_cd . "'" : '';
        $where_logistic_partner = ($logistic_partner != 'all') ? " and a.logistic_partner = '" . $logistic_partner . "'" : '';
        $where_timetable_dt = ($timetable_dt_s != '' && $timetable_dt_e != '') ? " and a.timetable_dt between '" . date('Y-m-d', strtotime( str_replace('/', '-', $timetable_dt_s))) . "' and '" . date('Y-m-d', strtotime( str_replace('/', '-', $timetable_dt_e))) . "'" : '';
        $where_timetable_dt = ($timetable_dt_s != '' && $timetable_dt_e == '') ? " and a.timetable_dt >= '" . date('Y-m-d', strtotime( str_replace('/', '-', $timetable_dt_s))) . "'" : $where_timetable_dt;
        $where_timetable_dt = ($timetable_dt_s == '' && $timetable_dt_e != '') ? " and a.timetable_dt <= '" . date('Y-m-d', strtotime( str_replace('/', '-', $timetable_dt_e))) . "'" : $where_timetable_dt;
        
        $sql = "
			select 
                count(a.timetable_id) as cnt
			from 
				tb_r_timetable a inner join tb_m_logistic_point b on b.lp_cd = a.customer_lp_cd
			where
				(
                    a.customer_lp_cd like '%" . $sv . "%'
                    or b.lp_name like '%" . $sv . "%'
                    or a.logistic_partner like '%" . $sv . "%'
                    or a.timetable_dt like '%" . $sv . "%'
                )
                " . $where_business . $where_customer . $where_customer_lp_cd . $where_logistic_partner . $where_timetable_dt;            

		return $this->db->query($sql)->row()->cnt;
    }

    function getTimetable($timetable_id)
    {
        $sql = "
            select 
                a.timetable_id,
                a.customer_lp_cd, a.logistic_partner, a.timetable_dt,
                b.lp_name, a.created_dt, a.created_by 
                , a.business, a.customer, c.system_value_txt     
            from 
                tb_r_timetable a inner join tb_m_logistic_point b on b.lp_cd = a.customer_lp_cd
                inner join tb_m_system c on c.system_code = a.customer and c.system_type = 'customer'
            where
                md5(a.timetable_id) = '" .$timetable_id . "'
        ";
        return $this->db->query($sql)->result();
    }

    function getTimetableDetail($timetable_id)
    {
        $sql = "
            select
                a.*
            from
                tb_r_timetable_detail a
            where 
                md5(a.timetable_id) = '" . $timetable_id . "'
        ";
        return $this->db->query($sql)->result();
    }

    function delete($timetable_id)
    {        
        $this->db->where('md5(timetable_id)', $timetable_id);
        $this->db->delete('tb_r_timetable_detail');

        $this->db->where('md5(timetable_id)', $timetable_id);
        $this->db->delete('tb_r_timetable');
    }

    function save($update = '')
    {
        // validate driver_cd
        $check_lp = $this->getTimetable(md5($this->input->post('driver_cd', true)));
        if (count($check_lp) == 0 || $update == '1')
        {
            $lp = array
            (
                'company_id' => $this->session->userdata(S_COMPANY_ID),
                'driver_cd' => $this->input->post('driver_cd', true),
                'driver_name' => $this->input->post('driver_name', true),
                'logistic_partner' => $this->input->post('logistic_partner', true),
                'jobdesc' => $this->input->post('jobdesc', true),
                'smartphone' => $this->input->post('smartphone', true),
                'phone' => $this->input->post('phone', true),
                'driving_license_val' => ($this->input->post('driving_license_val',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('driving_license_val', true)))) : null,
                'driving_license_type' => $this->input->post('driving_license_type', true),
                'forklift_license_val' => ($this->input->post('forklift_license_val',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('forklift_license_val', true)))) : null,
                'forklift_license_type' => $this->input->post('forklift_license_type', true),
                'length_of_working' => $this->input->post('length_of_working', true),
                'num_of_accident' => $this->input->post('num_of_accident', true),
            );

            if ($update == '')
            {
                $lp['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
                $lp['created_dt'] = date('Y-m-d H:i:s');

                $this->db->insert('tb_m_driver', $lp);
                
                return '1';
            }
            else 
            {
                $lp['changed_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
                $lp['changed_dt'] = date('Y-m-d H:i:s');

                $this->db->where('driver_cd', $this->input->post('driver_cd', true));
                $this->db->update('tb_m_driver', $lp);

                return '2'; // update success
            }    
        }
        else
        {
            return '0'; // driver_cd already exists
        }        
    }

    function getTimetableDetails($timetable_id, $route, $cycle, $start, $length, $sv, $order, $columns)
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {            
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }

        $w_route = ($route != '') ? " and a.route = '" . $route . "'" : '';
        $w_cycle = ($cycle != '') ? " and a.cycle = '" . $cycle . "'" : '';
       
        $sql = "
			select 
                a.*
			from 
				tb_r_timetable_detail a
            where
                (
                    route like '%" . $sv . "%'
                    or a.cycle like '%" . $sv . "%'
                    or a.vehicle_cd like '%" . $sv . "%'
                    or a.vehicle_number like '%" . $sv . "%'
                    or a.driver_cd like '%" . $sv . "%'
                    or a.driver_name like '%" . $sv . "%'
                    or a.lp_cd like '%" . $sv . "%'
                    or a.lp_name like '%" . $sv . "%'
                    or a.geofenceid like '%" . $sv . "%'
                    or a.arrival_dt like '%" . $sv . "%'
                    or a.arrival_plan like '%" . $sv . "%'
                    or a.arrival_actual like '%" . $sv . "%'
                    or a.arrival_gap like '%" . $sv . "%'
                    or a.arrival_eval like '%" . $sv . "%'
                    or a.departure_dt like '%" . $sv . "%'
                    or a.departure_plan like '%" . $sv . "%'
                    or a.departure_actual like '%" . $sv . "%'
                    or a.departure_gap like '%" . $sv . "%'
                    or a.departure_eval like '%" . $sv . "%'                     
                )
                and md5(a.timetable_id) = '" . $timetable_id . "'"
                . $w_route . $w_cycle . "
            order by " . $order_by . " ";
            
		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
		return $this->db->query($sql)->result();
    }

    function getCountOfTimetableDetail($timetable_id, $route, $cycle, $sv)
    {
        $w_route = ($route != '') ? " and a.route = '" . $route . "'" : '';
        $w_cycle = ($cycle != '') ? " and a.cycle = '" . $cycle . "'" : '';

        $sql = "
            select 
                count(a.timetable_detail_id) as cnt
            from 
                tb_r_timetable_detail a
            where
                (
                    route like '%" . $sv . "%'
                    or a.cycle like '%" . $sv . "%'
                    or a.vehicle_cd like '%" . $sv . "%'
                    or a.vehicle_number like '%" . $sv . "%'
                    or a.driver_cd like '%" . $sv . "%'
                    or a.driver_name like '%" . $sv . "%'
                    or a.lp_cd like '%" . $sv . "%'
                    or a.lp_name like '%" . $sv . "%'
                    or a.geofenceid like '%" . $sv . "%'
                    or a.arrival_dt like '%" . $sv . "%'
                    or a.arrival_plan like '%" . $sv . "%'
                    or a.arrival_actual like '%" . $sv . "%'
                    or a.arrival_gap like '%" . $sv . "%'
                    or a.arrival_eval like '%" . $sv . "%'
                    or a.departure_dt like '%" . $sv . "%'
                    or a.departure_plan like '%" . $sv . "%'
                    or a.departure_actual like '%" . $sv . "%'
                    or a.departure_gap like '%" . $sv . "%'
                    or a.departure_eval like '%" . $sv . "%'                    
                )
                and md5(a.timetable_id) = '" . $timetable_id . "'"
                . $w_route . $w_cycle;
        return $this->db->query($sql)->row()->cnt;
    }

    function getTimetableSummary($timetable_id, $start, $length, $sv, $order, $columns)
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {            
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }
       
        $sql = "
                SELECT 
                    route, `cycle`, vehicle_cd, 
                    GROUP_CONCAT(DISTINCT driver_name) as driver_name, 
                    GROUP_CONCAT(DISTINCT driver_cd) AS driver_cd,
                    SUM(done) AS done, SUM(total) AS total, 
                    SUM(total)-SUM(done) AS remain,                     
                    round(( SUM(done)/SUM(total) * 100 ),0) AS percentage 
                FROM
                (
                    SELECT route, `cycle`, vehicle_cd, driver_name, driver_cd, COUNT(timetable_detail_id) AS done, 0 AS total
                    FROM 
                        tb_r_timetable_detail 
                    WHERE 
                        md5(timetable_id) = '". $timetable_id ."'
                        AND departure_actual IS NOT null
                        and departure_plan IS NOT NULL
                    GROUP BY route, `cycle`, driver_cd
                    
                    UNION all
                    
                    SELECT route, `cycle`, vehicle_cd, driver_name, driver_cd, 0 AS done, COUNT(timetable_detail_id) AS total
                    FROM 
                        tb_r_timetable_detail 
                    WHERE 
                        md5(timetable_id) = '". $timetable_id ."'
                        and departure_plan IS NOT NULL
                    
                    GROUP BY route, `cycle`, driver_cd
                ) a
                WHERE
                    (
                        a.route like '%" . $sv . "%'
                        or a.`cycle` like '%" . $sv . "%'
                        or a.vehicle_cd like '%" . $sv . "%'
                        or a.driver_name like '%" . $sv . "%'
                    )
                GROUP BY a.route, a.cycle
                order by " . $order_by . " ";
            
		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
		return $this->db->query($sql)->result();
    }

    function getCountOfTimetableSummary($timetable_id, $sv)
    {
        $sql = "
            select count(b.route) as cnt from
            (
                SELECT 
                    route, `cycle`, vehicle_cd, 
                    GROUP_CONCAT(DISTINCT driver_name) as driver_name, 
                    GROUP_CONCAT(DISTINCT driver_cd) AS driver_cd,
                    SUM(done) AS done, SUM(total) AS total, 
                    SUM(total)-SUM(done) AS remain, 
                    concat(round(( SUM(done)/SUM(total) * 100 ),0),'%') AS percentage 
                FROM
                (
                    SELECT route, `cycle`, vehicle_cd, driver_name, driver_cd, COUNT(timetable_detail_id) AS done, 0 AS total
                    FROM 
                        tb_r_timetable_detail 
                    WHERE 
                        md5(timetable_id) = '". $timetable_id ."'
                        AND departure_actual IS NOT null
                        and departure_plan IS NOT NULL
                    
                    GROUP BY route, `cycle`, driver_cd
                    
                    UNION all
                    
                    SELECT route, `cycle`, vehicle_cd, driver_name, driver_cd, 0 AS done, COUNT(timetable_detail_id) AS total
                    FROM 
                        tb_r_timetable_detail 
                    WHERE 
                        md5(timetable_id) = '". $timetable_id ."'
                        and departure_plan IS NOT NULL
                    
                    GROUP BY route, `cycle`, driver_cd
                ) a
                GROUP BY a.route, a.cycle
            ) b
            WHERE
                    (
                        b.route like '%" . $sv . "%'
                        or b.`cycle` like '%" . $sv . "%'
                        or b.vehicle_cd like '%" . $sv . "%'
                        or b.driver_name like '%" . $sv . "%'
                    )";

        return $this->db->query($sql)->row()->cnt;
    }

    function getDrivers($q='')
    {
        $sql = "
            select driver_cd, driver_name
            from tb_m_driver where company_id = '" .$this->session->userdata(S_COMPANY_ID) . "'
            and 
            (
                driver_cd like '%" . $q . "%' or
                driver_name like '%" . $q . "%'
            )
            order by driver_name            
        ";
        return $this->db->query($sql)->result();
    }

    function getVehicles()
    {
        $sql = "
            select vehicle_cd, vehicle_number
            from tb_m_vehicle where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
            order by vehicle_cd
        ";
        return $this->db->query($sql)->result();
    }

    function checkTimetable($customer_lp_cd, $timetable_dts)
    {
        $tx = explode(',', $timetable_dts);
        $in = (count($tx)>1) ? "'" . str_replace(',', "','", $timetable_dts) . "'" : "'" . $timetable_dts . "'";

        $sql = "
            select
                count(a.timetable_id) as cnt
            from
                tb_r_timetable a
            where
                a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                and a.customer_lp_cd = '" . $customer_lp_cd . "'
                and a.timetable_dt in (".$in.");
        ";        
        return $this->db->query($sql)->row()->cnt;
    }

    function checkTruck($truck)
    {
        $sql = "
            select
                count(a.vehicle_cd) as cnt
            from
                tb_m_vehicle a
            where
                a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                and a.vehicle_cd = '" . $truck . "'
        ";        
        return $this->db->query($sql)->row()->cnt;
    }

    function checkDriver($driver)
    {
        $sql = "
            select
                count(a.driver_cd) as cnt
            from
                tb_m_driver a
            where
                a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                and a.driver_name = " . $this->db->escape($driver) . "
                and a.active = '1'
        ";        
        return $this->db->query($sql)->row()->cnt;
    }

    function checkLogisticPoint($logistic_point)
    {
        $sql = "
            select
                count(a.lp_cd) as cnt
            from
                tb_m_logistic_point a
            where
                a.company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
                and a.lp_cd = '" . $logistic_point . "'
        ";        
        return $this->db->query($sql)->row()->cnt;
    }

    function insertTempUploadH($a)
    {
        return $this->db->insert_batch('tb_t_timetable_upload_h', $a);
    }

    function insertTempUploadD($a)
    {
        return $this->db->insert_batch('tb_t_timetable_upload', $a);
    }

    function deleteTemp()
    {
        $sql = "delete FROM tb_t_timetable_upload_h WHERE process_id LIKE '%".$this->session->userdata(S_USER_EMAIL)."%'";
        $this->db->query($sql);
        $sql = "delete FROM tb_t_timetable_upload WHERE process_id LIKE '%".$this->session->userdata(S_USER_EMAIL)."%'";
        $this->db->query($sql);
        
    }

    function getTimetableUpload($process_id, $start, $length, $sv, $order, $columns)
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {            
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }

        $sql = "
			select 
                a.*    
			from 
				tb_t_timetable_upload a
            where
                a.process_id = '" . $process_id . "'
                and 
                (
                    route like '%" . $sv . "%' or    
                    cycle like '%" . $sv . "%' or
                    vehicle_cd like '%" . $sv . "%' or
                    driver_name like '%" . $sv . "%' or
                    lp_cd like '%" . $sv . "%' or
                    arrival_next_day like '%" . $sv . "%' or
                    arrival_plan like '%" . $sv . "%' or
                    departure_next_day like '%" . $sv . "%' or
                    departure_plan like '%" . $sv . "%' or
                    error_detail like '%" . $sv . "%'
                )
            order by " . $order_by . " ";
            
		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
        // echo $sql;
		return $this->db->query($sql)->result();
    }

    function getCountOfTimetableUpload($process_id, $sv)
    {
        $sql = "
            select 
                count(a.upload_id) as cnt
            from 
                tb_t_timetable_upload a
            where
                a.process_id = '" . $process_id . "'
                and 
                (
                    route like '%" . $sv . "%' or    
                    cycle like '%" . $sv . "%' or
                    vehicle_cd like '%" . $sv . "%' or
                    driver_name like '%" . $sv . "%' or
                    lp_cd like '%" . $sv . "%' or
                    arrival_next_day like '%" . $sv . "%' or
                    arrival_plan like '%" . $sv . "%' or
                    departure_next_day like '%" . $sv . "%' or
                    departure_plan like '%" . $sv . "%' or
                    error_detail like '%" . $sv . "%'
                )"
        ;        

		return $this->db->query($sql)->row()->cnt;
    }

    function doProcess($process_id)
    {
        return $this->doProcess2($process_id);
        // date_default_timezone_set('Asia/Jakarta');

        // // pindahin dari h ke timetable
        // $sql = "
        //     INSERT into tb_r_timetable 
        //     (
        //         company_id, business, customer, customer_lp_cd, logistic_partner, timetable_dt, process_id, created_by, created_dt
        //     )
        //     SELECT 
        //         '".$this->session->userdata(S_COMPANY_ID)."', 
        //         business,
        //         customer,
        //         customer_lp_cd, 
        //         'TTNT', 
        //         timetable_dt, 
        //         '" . $process_id . "',
        //         '".$this->session->userdata(S_USER_EMAIL)."', 
        //         NOW() 
        //     FROM
        //         tb_t_timetable_upload_h 
        //     WHERE 
        //         process_id = '".$process_id."'
        //     ORDER BY timetable_dt
        // ";
        // $this->db->query($sql);

        // // $timetable_id = $this->db->insert_id();

        // // pindahin dari d ke timetable_detail
        // $timetable_dts = $this->db->query("
        //     select timetable_id, timetable_dt from tb_r_timetable where process_id = '" . $process_id . "' order by timetable_dt
        // ")->result();

        // $sql = "
        //     select 
        //         a.route, a.cycle, a.vehicle_cd, a.driver_name, a.lp_cd, 
        //         a.arrival_next_day, a.arrival_plan, a.departure_next_day, a.departure_plan,
        //         b.vehicle_id, b.vehicle_number,
        //         c.driver_cd,
        //         d.lp_name, d.geofenceid,
        //         a.km, a.fuel, a.spbu, a.tol, a.others, a.pallet, a.rack, a.division
        //     from 
        //         tb_t_timetable_upload a 
        //         inner join tb_m_vehicle b on b.vehicle_cd = a.vehicle_cd                
        //         inner join tb_m_driver c on c.driver_name = a.driver_name
        //         inner join tb_m_logistic_point d on d.lp_cd = a.lp_cd
        //     where
        //         a.process_id = '" . $process_id . "'
        //         and a.is_valid = '1'
        //     order by
        //         a.route, a.cycle
        // ";
        // $timetable_details = $this->db->query($sql)->result();        

        // $arr = array();
        // $tempArr = array();
        // foreach ($timetable_dts as $timetable_dt)
        // {
        //     // get from temp_detail
        //     foreach ($timetable_details as $td)
        //     {
        //         $arr['timetable_id'] = $timetable_dt->timetable_id;
        //         $arr['timetable_dt'] = $timetable_dt->timetable_dt;
        //         $arr['route'] = $td->route;
        //         $arr['cycle'] = $td->cycle;
        //         $arr['vehicle_cd'] = $td->vehicle_cd;
        //         $arr['vehicle_id'] = $td->vehicle_id;
        //         $arr['vehicle_number'] = $td->vehicle_number;
        //         $arr['driver_cd'] = $td->driver_cd;
        //         $arr['driver_name'] = $td->driver_name;
        //         $arr['lp_cd'] = $td->lp_cd;
        //         $arr['lp_name'] = $td->lp_name;
        //         $arr['geofenceid'] = $td->geofenceid;
        //         $arr['arrival_next_day'] = $td->arrival_next_day;

        //         // addition 25-Mei-2020 -- To handle Rollcall driver report new format
        //         $arr['km'] = $td->km;
        //         $arr['fuel'] = $td->fuel;
        //         $arr['spbu'] = $td->spbu;
        //         $arr['tol'] = $td->tol;
        //         $arr['others'] = $td->others;
        //         $arr['pallet'] = $td->pallet;
        //         $arr['rack'] = $td->rack;
        //         $arr['division'] = $td->division;
                
        //         $dt_arrival	= date_create(date("Y-m-d", strtotime($timetable_dt->timetable_dt)));
        //         if ($td->arrival_next_day != '0' && $td->arrival_next_day != '')
        //         {
        //             date_add($dt_arrival, date_interval_create_from_date_string($td->arrival_next_day . ' days'));
        //         }                

        //         $arr['arrival_dt'] = date_format($dt_arrival, "Y-m-d");
        //         $arr['arrival_plan'] = $td->arrival_plan;
        //         $arr['departure_next_day'] = $td->departure_next_day;

        //         $dt_departure = date_create(date("Y-m-d", strtotime($timetable_dt->timetable_dt)));
        //         if ($td->departure_next_day != '0' && $td->departure_next_day != '')
        //         {
        //             date_add($dt_departure, date_interval_create_from_date_string($td->departure_next_day . ' days'));
        //         }
                

        //         $arr['departure_dt'] = date_format($dt_departure, "Y-m-d");
        //         $arr['departure_plan'] = $td->departure_plan;
        //         $arr['created_by'] = $this->session->userdata(S_USER_EMAIL);
        //         $arr['created_dt'] = date('Y-m-d H:i:s');

        //         $tempArr[] = $arr;

        //     }
        // }

        // // echo '<pre>'; print_r($tempArr); echo '</pre>';

        // // remove the temporary
        // $this->db->where('process_id', $process_id);
        // $this->db->delete('tb_t_timetable_upload_h');

        // $this->db->where('process_id', $process_id);
        // $this->db->delete('tb_t_timetable_upload');

        // // insert batch.
        // return $this->db->insert_batch('tb_r_timetable_detail', $tempArr);
    }

    function doProcess2($process_id)
    {
        date_default_timezone_set('Asia/Jakarta');

        // get tempH
        $head = $this->db->query("
            SELECT 
                '".$this->session->userdata(S_COMPANY_ID)."', 
                business,
                customer,
                customer_lp_cd, 
                'TTNT', 
                timetable_dt, 
                '" . $process_id . "',
                '".$this->session->userdata(S_USER_EMAIL)."', 
                NOW() 
            FROM
                tb_t_timetable_upload_h 
            WHERE 
                process_id = '".$process_id."'
            ORDER BY timetable_dt
        ")->result();

        if (count($head) > 0)
        {
            $tempH = array();
            
            foreach ($head as $h)
            {
                $temp = array();
                $temp['company_id'] = $this->session->userdata(S_COMPANY_ID);
                $temp['business'] = $h->business;
                $temp['customer'] = $h->customer;
                $temp['customer_lp_cd'] = $h->customer_lp_cd;
                $temp['logistic_partner'] = 'TTNT';
                $temp['timetable_dt'] = $h->timetable_dt;
                $temp['process_id'] = $process_id;
                $temp['created_by'] = $this->session->userdata(S_USER_EMAIL);
                $temp['created_dt'] = date('Y-m-d H:i:s');

                $tempH[] = $temp;
            }
        }

        $this->db->trans_start();
        $this->db->insert_batch('tb_r_timetable', $tempH);        
        $this->db->trans_complete();

        // pindahin dari d ke timetable_detail
        $timetable_dts = $this->db->query("
            select timetable_id, timetable_dt from tb_r_timetable where process_id = '" . $process_id . "' order by timetable_dt
        ")->result();

        $sql = "
            select 
                a.route, a.cycle, a.vehicle_cd, a.driver_name, a.lp_cd, 
                a.arrival_next_day, a.arrival_plan, a.departure_next_day, a.departure_plan,
                b.vehicle_id, b.vehicle_number,
                c.driver_cd,
                d.lp_name, d.geofenceid,
                a.km, a.fuel, a.spbu, a.tol, a.others, a.pallet, a.rack, a.division
            from 
                tb_t_timetable_upload a 
                inner join tb_m_vehicle b on b.vehicle_cd = a.vehicle_cd                
                inner join tb_m_driver c on c.driver_name = a.driver_name
                inner join tb_m_logistic_point d on d.lp_cd = a.lp_cd
            where
                a.process_id = '" . $process_id . "'
                and a.is_valid = '1'
            order by
                a.route, a.cycle
        ";
        $timetable_details = $this->db->query($sql)->result();        

        
        $tempArr = array();
        foreach ($timetable_dts as $timetable_dt)
        {
            // get from temp_detail
            foreach ($timetable_details as $td)
            {
                $arr = array();
                $arr['timetable_id'] = $timetable_dt->timetable_id;
                $arr['timetable_dt'] = $timetable_dt->timetable_dt;
                $arr['route'] = $td->route;
                $arr['cycle'] = $td->cycle;
                $arr['vehicle_cd'] = $td->vehicle_cd;
                $arr['vehicle_id'] = $td->vehicle_id;
                $arr['vehicle_number'] = $td->vehicle_number;
                $arr['driver_cd'] = $td->driver_cd;
                $arr['driver_name'] = $td->driver_name;
                $arr['lp_cd'] = $td->lp_cd;
                $arr['lp_name'] = $td->lp_name;
                $arr['geofenceid'] = $td->geofenceid;
                $arr['arrival_next_day'] = $td->arrival_next_day;

                // addition 25-Mei-2020 -- To handle Rollcall driver report new format
                $arr['km'] = $td->km;
                $arr['fuel'] = $td->fuel;
                $arr['spbu'] = $td->spbu;
                $arr['tol'] = $td->tol;
                $arr['others'] = $td->others;
                $arr['pallet'] = $td->pallet;
                $arr['rack'] = $td->rack;
                $arr['division'] = $td->division;
                
                $dt_arrival	= date_create(date("Y-m-d", strtotime($timetable_dt->timetable_dt)));
                if ($td->arrival_next_day != '0' && $td->arrival_next_day != '')
                {
                    date_add($dt_arrival, date_interval_create_from_date_string($td->arrival_next_day . ' days'));
                }                

                $arr['arrival_dt'] = date_format($dt_arrival, "Y-m-d");
                $arr['arrival_plan'] = $td->arrival_plan;
                $arr['departure_next_day'] = $td->departure_next_day;

                $dt_departure = date_create(date("Y-m-d", strtotime($timetable_dt->timetable_dt)));
                if ($td->departure_next_day != '0' && $td->departure_next_day != '')
                {
                    date_add($dt_departure, date_interval_create_from_date_string($td->departure_next_day . ' days'));
                }
                

                $arr['departure_dt'] = date_format($dt_departure, "Y-m-d");
                $arr['departure_plan'] = $td->departure_plan;
                $arr['created_by'] = $this->session->userdata(S_USER_EMAIL);
                $arr['created_dt'] = date('Y-m-d H:i:s');

                $tempArr[] = $arr;

            }
        }

        $this->deleteTemp();

        // insert batch.
        $this->db->trans_start();
        $this->db->insert_batch('tb_r_timetable_detail', $tempArr);
        $this->db->trans_complete();
        return true;
    }

    function saveTW()
    {
        $result = false;

        $timetable_id = $this->input->post('timetable_id', true);
        $w = $this->input->post('w', true);
        $r = $this->input->post('r', true);
        $c = $this->input->post('c', true);
        $vehicle_cd = $this->input->post('vehicle_cd',true);
        $driver_cd = $this->input->post('driver_cd', true);
        
        if ($w == 'truck')
        {
            // edit - 29 Juni. cari apakah sudah ada rollcall untuk driver ini
            $tt_changed = $this->db->query("
                select 
                    a.departure_dt,
                    a.route,
                    a.cycle,
                    a.driver_cd,
                    a.vehicle_cd
                from 
                    tb_r_timetable_detail a 
                    inner join tb_m_logistic_point b on b.lp_cd = a.lp_cd
                where
                    a.timetable_id = '" . $timetable_id . "'
                    and b.empty_plan_flg = '1'
                    and a.route = '" . $r . "'
                    and a.cycle = '"  .$c . "'
                    and a.departure_plan is not null
                "
            )->result();
            if (count($tt_changed) > 0)
            {
                $departure_dt =  $tt_changed[0]->departure_dt;
                $route = $tt_changed[0]->route;
                $cycle = $tt_changed[0]->cycle;
                $driver_cd = $tt_changed[0]->driver_cd;

                // cari apakah ada rollcall nya
                $rollcall_id = $this->db->query("
                    select 
                        rollcall_id 
                    from
                        tb_r_rollcall
                    where
                        departure_dt = '" . $departure_dt . "'
                        and route = '" . $route . "'
                        and cycle = '" . $cycle . "'
                        and driver_cd = '" . $driver_cd . "'

                ")->row()->rollcall_id;

                if ($rollcall_id != '')
                {
                    // update truck nya di rollcall
                    $rollcall_update_truck['vehicle_cd'] = $vehicle_cd;
                    $this->db->where('rollcall_id', $rollcall_id);
                    $this->db->update('tb_r_rollcall', $rollcall_update_truck);

                }
            }

            // TODO: Check apakah truck yg mau diganti ada juga ditanggal yg sama.

            $sql = "
                update tb_r_timetable_detail a
                set
                    a.vehicle_cd = '" . $vehicle_cd . "'
                where
                    a.timetable_id = '" . $timetable_id . "'
                    and a.route = '" . $r . "'
                    and a.cycle = '" . $c . "'
            ";            
            $this->db->query($sql);

            $sql = "
                update tb_r_timetable_detail a
                    inner join tb_m_vehicle b on b.vehicle_cd = a.vehicle_cd
                set 
                    a.vehicle_id = b.vehicle_id
                    , a.vehicle_number = b.vehicle_number
                where
                    a.timetable_id = '" . $timetable_id . "'
                    and a.route = '" . $r . "'
                    and a.cycle = '" . $c . "'
            ";
            $this->db->query($sql);            

            $result = true;
        }
        else if ($w == 'driver')
        {
            $sql = "
                update tb_r_timetable_detail a
                set
                    a.driver_cd = '" . $driver_cd . "'
                where
                    a.timetable_id = '" . $timetable_id . "'
                    and a.route = '" . $r . "'
                    and a.cycle = '" . $c . "'
            ";            
            $this->db->query($sql);

            $sql = "
                update tb_r_timetable_detail a
                    inner join tb_m_driver b on b.driver_cd = a.driver_cd
                set 
                    a.driver_name = b.driver_name
                where
                a.timetable_id = '" . $timetable_id . "'
                and a.route = '" . $r . "'
                and a.cycle = '" . $c . "'
            ";
            $this->db->query($sql);

            // edit - 26-Juni. Cari apakah driver_cd sudah punya Rollcall Standby. jika ada Isi data di rollcall nya.
            $timetable = $this->db->query("
                select 
                    a.timetable_detail_id,
                    a.timetable_id,
                    a.route,
                    a.cycle,
                    a.driver_cd,
                    a.vehicle_cd,
                    a.departure_dt,
                    -- TIME_FORMAT(SUBTIME(a.departure_plan, '00:20:00'), '%H:%i')  as departure_plan,
                    DATE_FORMAT(DATE_ADD(DATE_FORMAT(CONCAT(a.departure_dt,' ', a.departure_plan),'%Y-%m-%d %H:%i:%s'), INTERVAL -20 MINUTE), '%H:%i') AS departure_plan,
                    c.business,
                    c.customer
                from
                    tb_r_timetable_detail a
                    inner join tb_m_logistic_point b on b.lp_cd = a.lp_cd
                    inner join tb_r_timetable c on c.timetable_id = a.timetable_id
                where 
                    a.timetable_id = '" . $timetable_id . "' 
                    and a.arrival_plan is null
                    and b.empty_plan_flg = '1'
                    and a.driver_cd = '" . $driver_cd . "'
                order by departure_plan 
                limit 1
            ")->result();

            if (count($timetable) > 0)
            {
                $tt = $timetable[0];

                $rollcall = $this->db->query("
                    select a.* 
                    from 
                        tb_r_rollcall a
                    where 
                        departure_dt = '" . $tt->departure_dt . "'
                        and driver_cd = '" . $driver_cd . "'
                        and vehicle_cd = 'STANDBY'
                    "
                )->result();

                // ada rollcall standby untuk Driver ini di Tgl Departure ini.
                if (count($rollcall) > 0)
                {
                    // update data rollcall nya.
                    $rollcall_update['departure_plan'] = $tt->departure_plan;
                    $rollcall_update['business'] = $tt->business;
                    $rollcall_update['customer'] = $tt->customer;
                    $rollcall_update['route'] = $tt->route;
                    $rollcall_update['cycle'] = $tt->cycle;
                    $rollcall_update['vehicle_cd'] = $tt->vehicle_cd;

                    // cari arrival plan;
                    $sql = "
                    SELECT                         
                        TIME_FORMAT(DATE_ADD(a.arrival_plan, INTERVAL 20 MINUTE), '%H:%i') as arrival_plan
                    FROM 
                        tb_r_timetable_detail a 
                        INNER JOIN tb_m_logistic_point b ON b.lp_cd = a.lp_cd AND b.empty_plan_flg = '1' 
                    WHERE 
                        a.timetable_id = '" . $tt->timetable_id . "' AND 
                        a.arrival_plan IS NOT NULL AND 
                        a.departure_plan IS NULL 
                        AND a.route = '" . $tt->route . "'
                        AND a.cycle = '" . $tt->cycle . "'
                        and a.driver_cd = '" . $tt->driver_cd . "'
                    LIMIT 1
                    ";

                    $arrival_plan = '';
                    $ap = $this->db->query($sql)->result();
                    if (count($ap) > 0)
                    {
                        $rollcall_update['arrival_plan'] = $ap[0]->arrival_plan;
                    }

                    // Cari Cashier Summary
                    $sql = "
                    SELECT 
                        SUM(a.km) AS km, 
                        SUM(a.fuel) AS fuel, 
                        GROUP_CONCAT(distinct a.spbu) AS spbu, 
                        SUM(a.tol) AS tol , 
                        SUM(a.others) AS others
                    FROM 
                        tb_r_timetable_detail a 
                    WHERE 
                        a.timetable_id = '" . $tt->timetable_id . "' AND 
                        a.route = '" . $tt->route . "' AND 
                        a.driver_cd = '" . $tt->driver_cd . "' AND 
                        a.vehicle_cd = '" . $tt->vehicle_cd . "'
                    GROUP BY
                        a.timetable_id, a.route, a.driver_cd, a.vehicle_cd
                    ";
                    $cashier = $this->db->query($sql)->row();
                    $rollcall_update['cashier_fuel_plan'] = $cashier->fuel;
                    $rollcall_update['cashier_etoll_plan'] = $cashier->tol;
                    $rollcall_update['cashier_others_plan'] = $cashier->others;
                    $rollcall_update['cashier_etoll_spbu'] = $cashier->spbu;

                    // execute update rollcall
                    $this->db->where('rollcall_id', $rollcall[0]->rollcall_id);
                    $this->db->update('tb_r_rollcall', $rollcall_update);

                    // update gap dan eval
                    $agap = $this->get_time_difference($tt->departure_plan, date('H:i:s', strtotime($rollcall[0]->clock_in)));
                    $data_arrival['arrival_gap'] = $agap;
                    $data_arrival['arrival_eval'] = $this->get_time_eval($tt->departure_plan, date('H:i:s', strtotime($rollcall[0]->clock_in)));
                    $this->db->where('timetable_detail_id', $tt->timetable_detail_id);
                    $this->db->update('tb_r_timetable_detail', $data_arrival);

                    // delete data timetable standby nya tersebut.
                    $tt_standby = $this->db->query("select a.timetable_id from 
                        tb_r_timetable_detail a
                        where 
                            a.driver_cd = '" . $driver_cd . "'
                            and a.departure_dt = '" . $tt->departure_dt . "'
                            and (a.vehicle_cd = 'STANDBY' or a.vehicle_cd = 'FIELDMAN')
                    ")->result();

                    if (count($tt_standby) > 0)
                    {
                        $this->db->where('timetable_id', $tt_standby[0]->timetable_id);                        
                        $this->db->delete('tb_r_timetable_detail');

                        $this->db->where('timetable_id', $tt_standby[0]->timetable_id);
                        $this->db->delete('tb_r_timetable');
                    }
                    
                }
            }

            $result = true;

        }

        return $result;
    }

    function saveDetail()
    {
        $timetable_detail_id = $this->input->post('timetable_detail_id', true);
        $vehicle_cd = $this->input->post('vehicle_cd',true);
        $driver_cd = $this->input->post('driver_cd', true);
        $remark = $this->input->post('remark', true);
        $arrival_problem = $this->input->post('arrival_problem', true);
        $departure_problem = $this->input->post('departure_problem', true);

        $arrival_actual = $this->input->post('arrival_actual', true);
        $departure_actual = $this->input->post('departure_actual', true);

        // 25 Mei 2020
        $km = $this->input->post('km', true);
        $fuel = $this->input->post('fuel', true);
        $spbu = $this->input->post('spbu', true);
        $tol = $this->input->post('tol', true);
        $others = $this->input->post('others', true);
        $pallet = $this->input->post('pallet', true);
        $rack = $this->input->post('rack', true);
        $division = $this->input->post('division', true);

        // get timetable_dt
        $timetable_dt = $this->db->query("select timetable_dt from tb_r_timetable_detail where timetable_detail_id = '" . $timetable_detail_id . "'")->row()->timetable_dt;

        $sa = ($arrival_actual != '' && $arrival_actual != '0:00' && $arrival_actual != '00:00') ? ", a.arrival_actual ='" . $timetable_dt . ' ' . $arrival_actual . ":00', a.arrival_gap = '-1', a.arrival_eval = 'Manual', a.arrival_manual = '1', a.arrival_manual_by = '".$this->session->userdata(S_USER_EMAIL)."', a.arrival_manual_dt = NOW()" : '';
        $sd = ($departure_actual != '' && $departure_actual != '0:00' && $departure_actual != '00:00') ? ", a.departure_actual = '" . $timetable_dt . ' ' . $departure_actual . ":00', a.departure_gap = '-1', a.departure_eval = 'Manual', a.departure_manual = '1', a.departure_manual_by = '".$this->session->userdata(S_USER_EMAIL)."', a.departure_manual_dt = NOW()" : '';

        $sql = "
            update tb_r_timetable_detail a
            set
                a.vehicle_cd = '" . $vehicle_cd . "'
                , a.driver_cd = '" . $driver_cd . "'
                , a.remark = " . $this->db->escape($remark) . "
                , a.arrival_problem = " . $this->db->escape($arrival_problem) . "
                , a.departure_problem = " . $this->db->escape($departure_problem) . "
                
                , a.km = '" . $km . "'
                , a.fuel = '" . $fuel . "'
                , a.spbu = " . $this->db->escape($spbu) . "
                , a.tol = '" . $tol . "'
                , a.others = '" . $others . "'
                , a.pallet = '" . $pallet . "'
                , a.rack = '" . $rack . "'
                , a.division = " . $this->db->escape($division) . "

                " . $sa . $sd . "
            where
                a.timetable_detail_id = '" . $timetable_detail_id . "'
        ";
        //echo $sql; die();
        $this->db->query($sql);

        // update relasinya.
        $sql = "
            update tb_r_timetable_detail a
                inner join tb_m_driver b on b.driver_cd = a.driver_cd
            set 
                a.driver_name = b.driver_name
            where
                a.timetable_detail_id = '" . $timetable_detail_id . "'
        ";
        $this->db->query($sql);

        $sql = "
            update tb_r_timetable_detail a
                inner join tb_m_vehicle b on b.vehicle_cd = a.vehicle_cd
            set 
                a.vehicle_id = b.vehicle_id
                , a.vehicle_number = b.vehicle_number
            where
                a.timetable_detail_id = '" . $timetable_detail_id . "'
        ";
        $this->db->query($sql);

        // TODO : Update Rollcall nya di bagian fuel, etol, dll summary nya

        $tt = $this->db->query("
            select 
                departure_dt, route, cycle, driver_cd, timetable_id, vehicle_cd 
            from tb_r_timetable_detail where timetable_detail_id = '" . $timetable_detail_id . "'")->result();
        if (count($tt) > 0)
        {
            $tt = $tt[0];            

            // cari rollcall nya
            // cari apakah ada rollcall nya
            $rollcall = $this->db->query("
                select 
                    rollcall_id 
                from
                    tb_r_rollcall
                where
                    departure_dt = '" . $tt->departure_dt . "'
                    and route = '" . $tt->route . "'
                    and cycle = '" . $tt->cycle . "'
                    and driver_cd = '" . $tt->driver_cd . "'

            ")->result();

            $rollcall_id = (count($rollcall) > 0) ? $rollcall[0]->rollcall_id : '';

            if ($rollcall_id != '')
            {
                //Cari Cashier Summary
                $sql = "
                SELECT 
                    SUM(a.km) AS km, 
                    SUM(a.fuel) AS fuel, 
                    GROUP_CONCAT(distinct a.spbu) AS spbu, 
                    SUM(a.tol) AS tol , 
                    SUM(a.others) AS others
                FROM 
                    tb_r_timetable_detail a 
                WHERE 
                    a.timetable_id = '" . $tt->timetable_id . "' AND 
                    a.route = '" . $tt->route . "' AND 
                    a.driver_cd = '" . $tt->driver_cd . "' AND 
                    a.vehicle_cd = '" . $tt->vehicle_cd . "'
                GROUP BY
                    a.timetable_id, a.route, a.driver_cd, a.vehicle_cd
                ";
                $cashier = $this->db->query($sql)->row();

                // update truck nya di rollcall
                $rollcall_update['cashier_fuel_plan'] = $cashier->fuel;
                $rollcall_update['cashier_etoll_plan'] = $cashier->tol;
                $rollcall_update['cashier_etoll_spbu'] = $cashier->spbu;
                $rollcall_update['cashier_others_plan'] = $cashier->others;

                $this->db->where('rollcall_id', $rollcall_id);
                $this->db->update('tb_r_rollcall', $rollcall_update);

            }
        }
    }

    function getTotalHours()
    {
        $timetable_id = $this->input->post('timetable_id', true);
        $r = $this->input->post('r', true);
        $c = $this->input->post('c', true);

        $sql = "call sp_sum_hours(
			'" . $timetable_id . "'
			, '" . $r . "'
			, '" . $c . "'
		);";
		// echo $sql;
		$qry_res = $this->db->query($sql);
		$result = $qry_res->result();
		mysqli_next_result($this->db->conn_id);
		return $result;
    }

    function saveNLP()
    {
        $data = array(
            'timetable_id' => $this->input->post('timetable_id', true),
            'timetable_dt' => ($this->input->post('arrival_dt',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('arrival_dt', true)))) : $this->input->post('timetable_dt', true),
            'route' => $this->input->post('route', true),
            'cycle' => $this->input->post('cycle', true),
            'vehicle_cd' => $this->input->post('vehicle_cd', true),
            'driver_cd' => $this->input->post('driver_cd', true),
            'lp_cd' => $this->input->post('lp_cd', true),
            'arrival_dt' => ($this->input->post('arrival_dt',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('arrival_dt', true)))) : $this->input->post('timetable_dt', true),
            'arrival_plan' => ($this->input->post('arrival_plan', true) != '') ? $this->input->post('arrival_plan', true) : null,
            'departure_dt' => ($this->input->post('departure_dt',true) != '') ? date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('departure_dt', true)))) : $this->input->post('timetable_dt', true),
            'departure_plan' => ($this->input->post('departure_plan', true) != '') ? $this->input->post('departure_plan', true) : null,
        );
        $this->db->insert('tb_r_timetable_detail', $data);

        // update all related masters

        // vehicle
        $sql = "
            update tb_r_timetable_detail a
                inner join tb_m_vehicle b on b.vehicle_cd = a.vehicle_cd
            set 
                a.vehicle_id = b.vehicle_id
                , a.vehicle_number = b.vehicle_number
            where
                a.timetable_id = '" . $this->input->post('timetable_id', true) . "'
                and a.route = '" . $this->input->post('route', true) . "'
                and a.cycle = '" . $this->input->post('cycle', true) . "'
        ";
        $this->db->query($sql);

        // driver
        $sql = "
            update tb_r_timetable_detail a
                inner join tb_m_driver b on b.driver_cd = a.driver_cd
            set 
                a.driver_name = b.driver_name
            where
                a.timetable_id = '" . $this->input->post('timetable_id', true) . "'
                and a.route = '" . $this->input->post('route', true) . "'
                and a.cycle = '" . $this->input->post('cycle', true) . "'
        ";
        $this->db->query($sql);

        // lp
        $sql = "
            update tb_r_timetable_detail a
                inner join tb_m_logistic_point b on b.lp_cd = a.lp_cd
            set 
                a.lp_name = b.lp_name
                , a.geofenceid = b.geofenceid
            where
                a.timetable_id = '" . $this->input->post('timetable_id', true) . "'
                and a.route = '" . $this->input->post('route', true) . "'
                and a.cycle = '" . $this->input->post('cycle', true) . "'
        ";
        $this->db->query($sql);

        return true;
    }

    function deleteNLP()
    {        
        $this->db->where('timetable_detail_id', $this->input->post('timetable_detail_id', true));
        $this->db->delete('tb_r_timetable_detail');
        return true;
    }

    function get_time_difference($time1, $time2) {
        $time1 = strtotime("1980-01-01 $time1");
        $time2 = strtotime("1980-01-01 $time2");
        
        if ($time2 < $time1) {
            $time2 += 86400;
        }
        
        return date("H:i", strtotime("1980-01-01 00:00:00") + ($time2 - $time1));
    }

    function get_time_eval($time1, $time2) {
        $time1 = strtotime("1980-01-01 $time1");
        $time2 = strtotime("1980-01-01 $time2");
        
        $eval = 'Ontime';
        if ($time2 < $time1) {
            $eval = 'Advance';
        }else if ($time2 > $time1)
        {
            $eval = 'Delay';
        }
        
        return $eval;;
    }
}