<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 14-Mei-2020
 */

class Rollcall_model extends CI_Model 
{
    function __construct() 
	{
        parent:: __construct();
    }

    function getRollcall(
        $departure_dt_s, $departure_dt_e, $business, $customer,
        $route, $driver_cd, $vehicle_cd,
        $start, $length, $sv, $order, $columns, $status
    )
    {
        // setup order by
        // order by [column] [dir], [column] [dir]
        $order_by = ''; $i=1;
        foreach ($order as $o)
        {
            $order_by .= $columns[$o['column']]['name'] . " " . $o['dir'];
            if ($i<count($order))
            {
                $order_by .= ", ";
            }
            $i++;
        }

        $where_departure_dt = ($departure_dt_s != '' && $departure_dt_e != '') ? " and a.departure_dt between '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_s))) . "' and '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_e))) . "'" : '';
        $where_departure_dt = ($departure_dt_s != '' && $departure_dt_e == '') ? " and a.departure_dt >= '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_s))) . "'" : $where_departure_dt;
        $where_departure_dt = ($departure_dt_s == '' && $departure_dt_e != '') ? " and a.departure_dt <= '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_e))) . "'" : $where_departure_dt;
        $where_business     = ($business != 'all') ? " and a.business = '" . $business . "'" : '';
        $where_customer     = ($customer != 'all') ? " and a.customer = '" . $customer . "'" : '';
        $where_route        = ($route != '') ? " and a.route = '" . $route . "'" : '';
        $where_driver       = ($driver_cd != 'all') ? " and a.driver_cd = '" . $driver_cd . "'" : '';
        $where_vehicle      = ($vehicle_cd != 'all') ? " and a.vehicle_cd = '" . $vehicle_cd . "'" : '';
        $where_status       = ($status != 'all') ? " and a.status = '" . $status . "'" : '';

        $sql = "
			select 
                a.*,
                b.driver_name
			from 
                tb_r_rollcall a 
                inner join tb_m_driver b on b.driver_cd = a.driver_cd
			where
				(
                    a.rollcall_id like '%" . $sv . "%'
                    or a.voucher like '%" . $sv . "%'
                    or a.status like '%" . $sv . "%'
                    or a.departure_dt like '%" . $sv . "%'
                    or a.departure_plan like '%" . $sv . "%'
                    or a.business like '%" . $sv . "%'
                    or a.customer like '%" . $sv . "%'
                    or a.route like '%" . $sv . "%'
                    or a.cycle like '%" . $sv . "%'
                    or a.driver_cd like '%" . $sv . "%'
                    or b.driver_name like '%" . $sv . "%'
                    or a.vehicle_cd like '%" . $sv . "%'
                    or a.clock_in like '%" . $sv . "%'
                    or a.clock_out like '%" . $sv . "%'
                    or a.cashier_fuel_remark like '%" . $sv . "%'
                    or a.cashier_etoll_remark like '%" . $sv . "%'
                    or a.cashier_others_remark like '%" . $sv . "%'
                    or a.cashier_etoll_cardno like '%" . $sv . "%' 
                )
                " . $where_departure_dt . $where_business . $where_customer . $where_route . $where_driver . $where_vehicle . $where_status . "
            order by " . $order_by . " ";
            
		if ($length > 0)
		{
			$sql .= "limit ".$start.", ".$length;
        }
		return $this->db->query($sql)->result();
    }

    function getCountOfRollcall(
        $departure_dt_s, $departure_dt_e, $business, $customer,
        $route, $driver_cd, $vehicle_cd, $sv, $status
    )
    {
        $where_departure_dt = ($departure_dt_s != '' && $departure_dt_e != '') ? " and a.departure_dt between '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_s))) . "' and '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_e))) . "'" : '';
        $where_departure_dt = ($departure_dt_s != '' && $departure_dt_e == '') ? " and a.departure_dt >= '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_s))) . "'" : $where_departure_dt;
        $where_departure_dt = ($departure_dt_s == '' && $departure_dt_e != '') ? " and a.departure_dt <= '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_e))) . "'" : $where_departure_dt;
        $where_business     = ($business != 'all') ? " and a.business = '" . $business . "'" : '';
        $where_customer     = ($customer != 'all') ? " and a.customer = '" . $customer . "'" : '';
        $where_route        = ($route != '') ? " and a.route = '" . $route . "'" : '';
        $where_driver       = ($driver_cd != 'all') ? " and a.driver_cd = '" . $driver_cd . "'" : '';
        $where_vehicle      = ($vehicle_cd != 'all') ? " and a.vehicle_cd = '" . $vehicle_cd . "'" : '';
        $where_status       = ($status != 'all') ? " and a.status = '" . $status . "'" : '';

        $sql = "
			select 
                count(a.rollcall_id) as cnt
			from 
                tb_r_rollcall a 
                inner join tb_m_driver b on b.driver_cd = a.driver_cd
			where
				(
                    a.rollcall_id like '%" . $sv . "%'
                    or a.voucher like '%" . $sv . "%'
                    or a.status like '%" . $sv . "%'
                    or a.departure_dt like '%" . $sv . "%'
                    or a.departure_plan like '%" . $sv . "%'
                    or a.business like '%" . $sv . "%'
                    or a.customer like '%" . $sv . "%'
                    or a.route like '%" . $sv . "%'
                    or a.cycle like '%" . $sv . "%'
                    or a.driver_cd like '%" . $sv . "%'
                    or b.driver_name like '%" . $sv . "%'
                    or a.vehicle_cd like '%" . $sv . "%'
                    or a.clock_in like '%" . $sv . "%'
                    or a.clock_out like '%" . $sv . "%'
                    or a.cashier_fuel_remark like '%" . $sv . "%'
                    or a.cashier_etoll_remark like '%" . $sv . "%'
                    or a.cashier_others_remark like '%" . $sv . "%'
                    or a.cashier_etoll_cardno like '%" . $sv . "%'
                )
                " . $where_departure_dt . $where_business . $where_customer . $where_route . $where_driver . $where_vehicle . $where_status;

        return $this->db->query($sql)->row()->cnt;
    }

    function getRollcallForDownload($departure_dt_s, $departure_dt_e, $status)
    {

        $where_departure_dt = ($departure_dt_s != '' && $departure_dt_e != '') ? " and a.departure_dt between '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_s))) . "' and '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_e))) . "'" : '';
        $where_departure_dt = ($departure_dt_s != '' && $departure_dt_e == '') ? " and a.departure_dt >= '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_s))) . "'" : $where_departure_dt;
        $where_departure_dt = ($departure_dt_s == '' && $departure_dt_e != '') ? " and a.departure_dt <= '" . date('Y-m-d', strtotime( str_replace('/', '-', $departure_dt_e))) . "'" : $where_departure_dt;
        $where_status       = ($status != 'all') ? " and a.status = '" . $status . "'" : '';

        $sql = "
        select 
            a.*,
            b.driver_name,
            c.vehicle_number
        from 
            tb_r_rollcall a 
            inner join tb_m_driver b on b.driver_cd = a.driver_cd
            inner join tb_m_vehicle c on c.vehicle_cd = a.vehicle_cd
        where
            1 " . $where_departure_dt . $where_status;

        return $this->db->query($sql)->result();



    }
}