<?php
/*
* @author: Irfan Satriadarma
* @created: 06 Januari 2018
*/

class Company_model extends CI_Model
{
	function __construct() 
	{
        parent:: __construct();
	}
	
	function get_companies()
	{
		$sql = "select * from tb_m_company where user_email = '" . $this->session->userdata(S_USER_EMAIL) . "' order by company_name";
		
        return $this->db->query($sql)->result();
	}

	function get_companies_count()
	{
		$sql = "select count(company_id) as cnt from tb_m_company where user_email = '" . $this->session->userdata(S_USER_EMAIL) . "'";
		return $this->db->query($sql)->row()->cnt;
	}

	function delete($company_id)
	{
		// all related master
		$this->db->where('company_id', $company_id)->delete('tb_m_account');
		$this->db->where('company_id', $company_id)->delete('tb_m_account');
		$this->db->where('company_id', $company_id)->delete('tb_m_company_function');
		$this->db->where('company_id', $company_id)->delete('tb_m_company_prefferences');
		$this->db->where('company_id', $company_id)->delete('tb_m_company');
		$this->db->where('company_id', $company_id)->delete('tb_m_holiday_cal');
		$this->db->where('company_id', $company_id)->delete('tb_m_items');
		$this->db->where('company_id', $company_id)->delete('tb_m_supplier');
		$this->db->where('company_id', $company_id)->delete('tb_m_tax');
		$this->db->where('company_id', $company_id)->delete('tb_m_warehouse');

		// user group auth
		$sql = "
			DELETE tb_m_user_group_auth
			FROM tb_m_user_group_auth
			INNER JOIN tb_m_user_group ON tb_m_user_group.user_group_id = tb_m_user_group_auth.user_group_id
			WHERE tb_m_user_group.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_m_user_group');

		
		// balance sheet
		$sql = "
			DELETE tb_r_balance_sheet_detail
			FROM tb_r_balance_sheet_detail
			INNER JOIN tb_r_balance_sheet ON tb_r_balance_sheet.balance_sheet_id = tb_r_balance_sheet_detail.balance_sheet_id
			WHERE tb_r_balance_sheet.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_balance_sheet');

		// journal
		$sql = "
			DELETE tb_r_journal_items
			FROM tb_r_journal_items
			INNER JOIN tb_r_journal ON tb_r_journal.journal_id = tb_r_journal_items.journal_id
			WHERE tb_r_journal.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_journal');

		// profit loss
		$sql = "
			DELETE tb_r_profit_loss_detail
			FROM tb_r_profit_loss_detail
			INNER JOIN tb_r_profit_loss ON tb_r_profit_loss.profit_loss_id = tb_r_profit_loss_detail.profit_loss_id
			WHERE tb_r_profit_loss.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_profit_loss');

		// purchase orders
		$sql = "
			DELETE tb_r_purchase_order_items
			FROM tb_r_purchase_order_items
			INNER JOIN tb_r_purchase_order ON tb_r_purchase_order.order_id = tb_r_purchase_order_items.order_id
			WHERE tb_r_purchase_order.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_purchase_order');

		// purchase payments
		$sql = "
			DELETE tb_r_purchase_payment_items
			FROM tb_r_purchase_payment_items
			INNER JOIN tb_r_purchase_payment ON tb_r_purchase_payment.payment_id = tb_r_purchase_payment_items.payment_id
			WHERE tb_r_purchase_payment.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_purchase_payment');

		// purchase request
		$sql = "
			DELETE tb_r_purchase_request_items
			FROM tb_r_purchase_request_items
			INNER JOIN tb_r_purchase_request ON tb_r_purchase_request.request_id = tb_r_purchase_request_items.request_id
			WHERE tb_r_purchase_request.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_purchase_request');

		// purchase return
		$sql = "
			DELETE tb_r_purchase_return_items
			FROM tb_r_purchase_return_items
			INNER JOIN tb_r_purchase_return ON tb_r_purchase_return.return_id = tb_r_purchase_return_items.return_id
			WHERE tb_r_purchase_return.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_purchase_return');

		// reconcile
		$sql = "
			DELETE tb_r_reconcile_entries
			FROM tb_r_reconcile_entries
			INNER JOIN tb_r_reconcile ON tb_r_reconcile.reconcile_id = tb_r_reconcile_entries.reconcile_id
			WHERE tb_r_reconcile.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_reconcile');

		// sales delivery
		$sql = "
			DELETE tb_r_sales_delivery_items
			FROM tb_r_sales_delivery_items
			INNER JOIN tb_r_sales_delivery ON tb_r_sales_delivery.delivery_id = tb_r_sales_delivery_items.delivery_id
			WHERE tb_r_sales_delivery.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_sales_delivery');

		// sales invoice
		$sql = "
			DELETE tb_r_sales_invoice_items
			FROM tb_r_sales_invoice_items
			INNER JOIN tb_r_sales_invoice ON tb_r_sales_invoice.invoice_id = tb_r_sales_invoice_items.invoice_id
			WHERE tb_r_sales_invoice.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_sales_invoice');

		// sales order
		$sql = "
			DELETE tb_r_sales_order_items
			FROM tb_r_sales_order_items
			INNER JOIN tb_r_sales_order ON tb_r_sales_order.order_id = tb_r_sales_order_items.order_id
			WHERE tb_r_sales_order.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_sales_order');

		// sales payment
		$this->db->where('company_id', $company_id)->delete('tb_r_sales_payment');

		// sales quotation
		$sql = "
			DELETE tb_r_sales_quotation_items
			FROM tb_r_sales_quotation_items
			INNER JOIN tb_r_sales_quotation ON tb_r_sales_quotation.quotation_id = tb_r_sales_quotation_items.quotation_id
			WHERE tb_r_sales_quotation.company_id = '" . $company_id . "'
		";
		$this->db->query($sql);
		$this->db->where('company_id', $company_id)->delete('tb_r_sales_quotation');

		// The company itself
		$this->db->where('company_id', $company_id)->delete('tb_m_company');

		// update company related to user
		$this->db->query("
			update tb_m_users set user_group_id = NULL, company_id = NULL where user_email = '" . $this->session->userdata(S_USER_EMAIL) . "' and company_id = '" . $company_id . "'
		");

		
		$this->session->set_userdata(S_USER_LANDING, '');
		
	}
	
	function get_company($company_id = '')
	{
		$company_id = ($company_id != '') ? $company_id : $this->session->userdata(S_COMPANY_ID);
		$this->db->select("
			com.company_id
			, com.user_email
			, com.company_name
			, com.company_industry
			, com.logo
			, com.show_logo_in_report
			, com.address
			, com.shipping_address
			, com.phone
			, com.fax
			, com.tax_number
			, com.website
			, com.email
			, com.currency
			, com.default_language
			, com.bank_name
			, com.bank_branch
			, com.bank_address
			, com.bank_account_number
			, com.bank_account_name
			, com.swift_code
			, com.accounting_period_start
			, com.accounting_period_end
			, com.sales_open_invoices
			, com.sales_overdue_invoices
			, com.sales_invoices_paid_last30
			, com.trial_expired
			, com.activation_code
			, com.activation_expired
			, com.package_id
			, datediff(com.trial_expired, date_format(now(), '%Y-%m-%d')) as left_day
			, date_format(com.trial_expired, '%d %M %Y') as trial_expired_date
			, com.created_dt, com.trial_expired as trial_expired_dt
			, cp.system_value_num as num_of_users
			, cp2.system_value_num as num_of_addon_users
			, com.signature
		"
		);
        
		$this->db->from('tb_m_company com');
		$this->db->join('tb_m_company_prefferences cp', 'cp.company_id = com.company_id', 'left');
		$this->db->join('tb_m_company_prefferences cp2', 'cp2.company_id = com.company_id', 'left');
        
		$this->db->where('com.company_id', $company_id);
		$this->db->where('com.user_email', $this->session->userdata(S_USER_EMAIL));
		$this->db->where('cp.system_type', 'config_users');
		$this->db->where('cp.system_code', 'default');
		$this->db->where('cp2.system_type', 'config_users');
		$this->db->where('cp2.system_code', 'addons');
	
        
        
		$query = $this->db->get();
        
		if ($query->num_rows() == 1)
		{
            return $query->row();
        }
		else
		{
            return false;
        }
	}
	
	function get_company_field($field) 
	{
		$sql = "
			select " . $field . " from tb_m_company where company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
		";
		return $this->db->query($sql)->result_array();
	}
	
	function get_company_prefferences($system_type, $system_code = '', $order_by = '', $resultval = '1') 
	{
		$sql = "
			select 
				system_value_txt, system_value_num, system_code
			from
				tb_m_company_prefferences
			where
				company_id = '" . $this->session->userdata(S_COMPANY_ID) . "'
		";
		
		
		$sql .= ($system_type != '') ? " and system_type = '" . $system_type . "'" : '';
		$sql .= ($system_code != '') ? " and system_code = '" . $system_code . "'" : '';
		
		$sql .= ($order_by != '') ? " order by " . $order_by : '';
		
		$sysval = $this->db->query($sql)->result();
		if (count($sysval)>0)
		{
			return ($resultval == '1') ? $sysval[0] : $sysval;
		}
		else
		{
			$row = new stdClass();
			$row->system_code = '';
            $row->system_value_txt = '';
            $row->system_value_num = '';
            return $row;
		}        
	}
	
	function save_company_prefferences($system_type, $system_code, $system_value_txt, $system_value_num = '')
	{
		$data = array(
			'company_id' => $this->session->userdata(S_COMPANY_ID),
			'system_type' => $system_type,
			'system_code' => $system_code,
			'system_value_txt' => $system_value_txt,
			'system_value_num' => $system_value_num
		);
		$this->db->insert('tb_m_company_prefferences', $data);		
	}

	function update_company()
	{
		$company = array
            (
            'company_name' => $this->input->post('company_name', true),
            'address' => $this->input->post('address', true),
            'company_industry' => $this->input->post('company_industry', true),
            'email' => $this->input->post('email', true),
            'phone' => $this->input->post('phone', true),
            'fax' => $this->input->post('fax', true),
            'tax_number' => $this->input->post('tax_number', true),
            'website' => $this->input->post('website', true),            
		);
		
		$company_id = $this->input->post('company_id', true);

		$company['changed_by'] = $this->session->userdata(S_USER_EMAIL);
		$company['changed_dt'] = date('Y-m-d H:i:s');

		// update header
		$this->db->where('company_id', $company_id)->update('tb_m_company', $company);
	}

	function switchCompany($company_id)
	{
		$sql = "select user_group_id from tb_m_user_group where company_id = '" . $company_id . "' and is_admin = '1'";
		$user_group_id = $this->db->query($sql)->row()->user_group_id;
		
		$sqlUser = "update tb_m_users set user_group_id = '" . $user_group_id . "', company_id = '".$company_id."' where user_email = '" . $this->session->userdata(S_USER_EMAIL) . "'";
        $this->db->query($sqlUser);

	}
}
?>