<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author: misbah@arkamaya.co.id
 * @created: 2017-02-23
 */

/* added by misbah 20170223 - Untuk generate button dari feature*/
function create_button($btnAccess = array(), $btnId = "", $extra_attr = '', $param = "")
{
    if($btnAccess) 
	{
        if (array_key_exists($btnId, $btnAccess))
		{
            if ( $btnAccess[$btnId]['onclick'] != "" && $param != "" )
			{
                $str1 = str_replace("{0}", base_url(), trim($btnAccess[$btnId]['onclick']) );
                $str2 = str_replace("{1}", $param, $str1 );
                $btnAccess[$btnId]['onclick'] = $str2;
            }
            echo form_button($btnAccess[$btnId], $btnAccess[$btnId]["content"] ,$extra_attr);
        }
    }
}

function search_for_key($id, $array, $btnId) 
{
    $k = array();
    foreach ($array as $key => $val) 
	{
        if ($val['type'] === $id && strpos($val['id'], $btnId) !== false ) 
		{
            array_push($k, $val);
        }
    }
    return $k;
 }

function create_button_group($btnAccess = array(), $btnId = "")
{
    if($btnAccess) 
	{
        if (array_key_exists($btnId, $btnAccess))
		{
            $btnDropdown = $btnAccess[$btnId];

            $list = search_for_key('button_group_list', $btnAccess, $btnId);

            $html = "";
            $html = '<div class="btn-group dropup">';
            $html .= '<button type="button" class="'.$btnDropdown['class'].'" data-toggle="dropdown" aria-expanded="false"> '.$btnDropdown['content'].' <span class="caret"></span> </button>';
            $html .= '<ul class="dropdown-menu">';
            foreach ($list as $r) 
			{
                $html .= '<li><a href="#" onclick="'.$r['onclick'].'">'.$r['content'].'</a></li>';
            }
            $html .= '</ul>';
            $html .= '</div>';
            echo $html;
        }
    }
}

function create_menu( $list = array() )
{
    $html = '';
    if ($list){
        
        $group = "";
        $i = 1;
        foreach ($list as $row) 
		{
            if ($group != $row['function_grp']) 
			{
                $group = $row['function_grp'];
                
                if ($i > 1) 
				{
                    $html .= '</ul></li>';
                }
                $html .= '<li class="has_sub">'
                    . '<a href="javascript:void(0);" class="waves-effect"><i class="'.$row['function_icon'].'"></i> <span> '.$row['function_grp_nm'].' </span> <span class="menu-arrow"></span></a>'
                    . '<ul class="list-unstyled">';
                $i++;
            }
            
			if ($row['function_link_visible'] == '1')
			{
				$url = ($row['function_controller'] != 'javascript:;' && $row['function_controller'] != '#') ? site_url($row['function_controller']) : $row['function_controller'];
				$html .= '<li><a href="'.$url.'">'.$row['function_name'].'</a></li>';
			}
            
        } 
        $html .= '</ul></li>';
    }
    
    echo $html;
}

function get_param_special_char() 
{
    $obj =& get_instance();
    $controller = $obj->uri->segment(1);
    $method = $obj->uri->segment(2);

    $arr = explode("$controller/$method/",uri_string());

    if(count($arr) == 2)
        return $arr[1];
    else
        return '';
}

function is_weekend($your_date) 
{
    $week_day = date('w', strtotime($your_date));
    return ($week_day == 0 || $week_day == 6);
}

function to_std_dt($dt) 
{
    if(empty($dt)) return $dt;
    return date('Y-m-d', strtotime(str_replace('/', '-', $dt)));
}

function to_std_dt_screen($dt) 
{
    if(empty($dt)) return $dt;
    return date('d/m/Y', strtotime(str_replace('-', '/', $dt)));
}

function to_std_dt_table($dt) 
{
    if(empty($dt)) return $dt;
    return date('d M Y', strtotime(str_replace('-', '/', $dt)));
}

function extract_date_range_picker($dateStr) 
{
    $date_range_raw = explode('-', $dateStr);

    $start_date = '';
    $end_date = '';
    if(count($date_range_raw) == 2) 
	{
        $start_date = to_std_dt($date_range_raw[0]);
        $end_date = to_std_dt($date_range_raw[1]);
    }

    return (object)[
                'start' => $start_date,
                'end' => $end_date];
}

function a($href,
           $title,
           $text) 
		   {
    return "<a href=".site_url($href).' title="'.$title.'">'.$text.'</a>';
}

function to_std_currency($val) 
{
    return 'Rp. ' . number_format($val, 2);
}


function timepicker_to_hour($val) 
{
    if(is_null($val)) return $val;

    $vals =  explode(":", $val);
    $hour = $vals[0] + ($vals[1]/60);
    return number_format($hour, 2);
}

function hour_to_timepicker($val) 
{
    if(is_null($val)) return $val;

    $vals =  explode(".", $val);

    if(count($vals) == 1) 
	{
        return $val . ':00';
    } else if(count($vals) == 2) 
	{
        $minute = doubleval("0.{$vals[1]}") * 60;
        return $vals[0] . ':' . intval($minute);
    }
}

function shorten_string($string, $wordsreturned)
{
	$retval = $string;
	$string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
	$string = str_replace("\n", " ", $string);
	$array = explode(" ", $string);
	
	if (count($array)<=$wordsreturned)
	{
		$retval = $string;
	}
	else
	{
		array_splice($array, $wordsreturned);
		$retval = implode(" ", $array)." ...";
	}
	return $retval;
}


function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}

function get_time_difference($time1, $time2) {
    $time1 = strtotime("1980-01-01 $time1");
    $time2 = strtotime("1980-01-01 $time2");
    
    if ($time2 < $time1) {
        $time2 += 86400;
    }
    
    return date("H:i", strtotime("1980-01-01 00:00:00") + ($time2 - $time1));
}    