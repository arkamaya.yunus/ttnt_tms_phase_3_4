<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo APP_NAME ?>">
        <meta name="author" content="irfan.satriadarma@gmail.com">

        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon2.png">
        <title>Login - <?php echo APP_NAME ?></title>

        <!-- App css -->
        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/pace.min.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url() ?>assets/js/modernizr.min.js"></script>		
    </head>

    <body class="bg-transparent">
        <section>
            <div class="container-alt">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="wrapper-page">
							<div class="m-t-120 account-pages">
                                <div class="text-center account-logo-box" style="padding-top: 20px; padding-bottom: 20px">
									<a href="<?php echo site_url(); ?>" class="text-success">
										<span>
											<img src="<?php echo base_url() ?>assets/images/logo-text-dark.png" alt="" height="40">
										</span>
									</a>                                    
                                </div>								
                                <div class="account-content ac_fix">
                                    <?php if ($this->session->flashdata('login_failed') != ''): ?>
                                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">
                                                    <span aria-hidden="true">×</span>
                                            </button>
                                            <strong>Login Gagal.</strong> <br/><?php echo $this->session->flashdata('login_failed')?>
                                        </div>
                                    <?php endif; ?>

                                    <?php if ($this->uri->segment(2) == 'expired'): ?>
                                        <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">
                                                    <span aria-hidden="true">×</span>
                                            </button>
                                            <strong>Login Gagal.</strong> <br/>Masa Berlaku Aplikasi Sudah Habis. Silahkan Hubungi <a href="mailto: info@arkamaya.co.id">info@arkamaya.co.id</a> atau Telepon <a href="tel:08172311185">+62-817-2311-185</a>
                                        </div>
                                    <?php endif; ?>
									
									<?php if ($this->uri->segment(2) == 'misscode'): ?>
                                        <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                            <button type="button" class="close" data-dismiss="alert">
                                                    <span aria-hidden="true">×</span>
                                            </button>
                                            <strong>Konfirmasi Gagal.</strong> <br/>Kode Konfirmasi Pengguna tidak terdaftar. Silahkan Hubungi <a href="mailto: info@arkamaya.co.id">info@arkamaya.co.id</a> atau Telepon <a href="tel:08172311185">+62-817-2311-185</a>
                                        </div>
                                    <?php endif; ?>
									
                                    <form class="form-horizontal" action="" method="post" id="form_login">

                                        <div class="form-group ">
                                            <div class="col-xs-12">
                                                <input name="user_email" class="form-control" type="text" placeholder="Email" value="" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <input name="user_password" class="form-control" type="password" placeholder="Kata Sandi" value="" />
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <div class="col-xs-12">
                                                <button name="btn_submit" id="btn_submit" class="form-control btn btn-bordered btn-custom" type="submit" />Masuk</button>
                                            </div>
                                        </div>

										<!-- <div class="form-group text-center">
                                            <div class="col-sm-12">
                                                <a href="<?php echo base_url(); ?>forgot_password"><i class="fa fa-lock m-r-5"></i> Lupa Kata Sandi</a>
                                            </div>
                                            <div class="col-sm-12 m-t-5">
                                                <a href="<?php echo base_url(); ?>register"> <span class="text-muted">Belum Memiliki Akun?</span> Daftar Sekarang</a>
                                            </div>
                                        </div> -->

                                    </form>

                                    <div class="clearfix"></div>
									
									<div class="row">
										<div class="col-sm-12 text-center">
											<p class="text-muted">&copy; PT. TTLC Nasmoco Transport - 2020</p>
										</div>
									</div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END HOME -->

        <script>
            var resizefunc = [];
        </script>

        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/detect.js"></script>
        <script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url() ?>assets/js/waves.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/jquery.core.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.app.js"></script>
		
		<script src="<?php echo base_url() ?>assets/js/plugins/jquery-validate/jquery.validate.js"></script>
		
		<script src="<?php echo base_url() ?>assets/js/apps/jquery.validate.message.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/apps/login.js"></script>
		
        
    </body>
</html>