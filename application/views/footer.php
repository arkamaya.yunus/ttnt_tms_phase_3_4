</div> <!-- content -->

<footer class="footer text-right">
    &copy; <?php echo date('Y')?> <a href="<?php echo site_url()?>" class="text-primary m-l-5"><b><?php echo APP_NAME ?></b></a> - Transportation Management System
</footer>

</div>

<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

</div>
<!-- END wrapper -->

<div id="myCPWD" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myCPWDLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myCPWDLabel">Change Password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="f-cpwd" class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                            <div class="form-group pk pk-reset">
                                <label class="col-md-4 control-label"><span id="tpass">Old Password</span> <span class="text-danger">*</span></label>
                                <div class="col-md-8 m-t-5">
                                    <input type="password" class="form-control" name="cpwd_old" id="cpwd_old" value=""  required />
                                </div>
                            </div>
                            <div class="form-group pk pk-reset">
                                <label class="col-md-4 control-label"><span id="tpass">New Password</span> <span class="text-danger">*</span></label>
                                <div class="col-md-8 m-t-5">
                                    <input type="password" class="form-control" name="cpwd_new" id="cpwd_new" value=""  required />
                                </div>
                            </div>
                            <div class="form-group pk pk-reset">
                                <label class="col-md-4 control-label"><span id="tpass">Confirm Password</span> <span class="text-danger">*</span></label>
                                <div class="col-md-8 m-t-5">
                                    <input type="password" class="form-control" name="cpwd_conf" id="cpwd_conf" value=""  required />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="doSavePwd()" id="btn_cpwd">Change</button>
            </div>
        </div>
    </div>
</div>

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/detect.js"></script>
<script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url() ?>assets/js/waves.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.app.js"></script>

<!-- Plugin JS -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/autocomplete/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/select2/select2/select2.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.keyTable.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.colVis.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/jquery.dataTables.rowGrouping.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/apps/default.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/toastr/toastr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/countUp/countup.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/jquery-printarea/jquery.PrintArea.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/morris.js-0.5.1/raphael-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/morris.js-0.5.1/morris.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/switchery/switchery.min.js"></script>

<?php if (isset($externaljs)){ foreach ($externaljs as $js){ ?>
<script type="text/javascript" src="<?php echo $js ?>"></script>
<?php } } ?>

<?php if (isset($jsapp)): foreach ($jsapp as $js): ?>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/apps/<?php echo $js ?>.js"></script>
<?php
    endforeach;
endif;
?>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/apps/jquery.validate.message.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/apps/changepwd.js"></script>

<script src="<?php echo base_url() ?>assets/js/apps/kelola.biz.js"></script>
<script src="<?php echo base_url() ?>assets/js/pace.min.js"></script>
</body>
</html>
