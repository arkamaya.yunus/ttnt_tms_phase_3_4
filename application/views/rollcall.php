<style>
#datatable_rollcall_wrapper tr th,
#datatable_rollcall_wrapper tr th,
.fixedHeader-floating tr th
{
	vertical-align: middle !important;
	background-color: #f2f6fc;
}

#datatable_rollcall_wrapper tr th,
#datatable_rollcall_wrapper tr td
{	
	/* font-size: 12px !important; */
}
.create-btn .dropdown-menu 
{
	height: 300px; overflow-y:scroll
}

</style>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Transaction</li>
					<li class="active"><?php echo $stitle?></li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Rollcall Manual</h4>
				</div>
				<div class="modal-body">
					<?php
					// Tombol ini hanya akan aktif jika diijinkan supervisor.
					// Issue No. 63.
					// $sess_manual_rollcall = $this->session->userdata('sess_manual_rollcall');
					// if ($sess_manual_rollcall != ''):
					?>
					<div class="row rcm rcm-1">
						<div class="col-sm-12">
							<label class="col-sm-4 control-label">Departure Date</label>
							<div class="col-md-8"><?php echo $departure_dt_e?></div>
						</div>
						<div class="col-sm-12">
							<label class="col-sm-4 control-label">Driver</label>
							<div class="col-md-8">
								<select id="manual_driver_cd" class="form-control select2" onchange="manualRoute()">
									<option value="0">Pilih Driver</option>
									<?php foreach ($driver_manual as $d):?>
									<option value="<?php echo $d->driver_cd?>"><?php echo $d->driver_cd?> - <?php echo $d->driver_name?></option>
									<?php endforeach; ?>									
								</select>
							</div>
						</div>
						<div class="col-sm-12">
							<hr/>
							<div  style="height: 300px; overflow-y: scroll">
							<table class="table table-striped table-hover">
								<thead>
								<tr>
									<th>Route</th>
									<th>Cycle</th>
									<th width="200"></th>
								</tr>
								</thead>
								<tbody>
									<?php $xm=1; foreach ($route_manual as $r): ?>
									<tr class="mdrv mdrv<?php echo $r->driver_cd?>">
										<td><?php echo $r->route?></td>
										<td><?php echo $r->cycle?></td>
										<td>
										<button type="button" class="btn btn-sm btn-purple btn-default waves-effect" id="btnStartManual<?php echo $xm?>" onclick="startManual('<?php echo $xm?>', '<?php echo $r->driver_cd?>','<?php echo $r->route?>','<?php echo $r->cycle?>', '<?php echo $r->lp_cd?>')">Pilih</button>
										</td>
									</tr>
									<?php $xm++; endforeach; ?>
								</tbody>
							</table>
							</div>
						</div>
					</div>
					<?php // else: ?>
					<div class="row rcm rcm-2">
						<div class="col-sm-12">
							<div class="col-sm-12">
								<p>Diperlukan ijin Supervisor/Administrator untuk membuka fitur ini</p>
							</div>
							<label class="col-sm-4 control-label">Username</label>
							<div class="col-md-8">
								<input type="text" id="manual_rollcall_username" name="manual_rollcall_username" class="form-control" value="" />							
							</div>
							<label class="col-sm-4 m-t-5 control-label">Password</label>
							<div class="col-md-8 m-t-5">
								<input type="password" id="manual_rollcall_password" name="manual_rollcall_password" class="form-control" value="" />							
							</div>
							<label class="col-sm-4 m-t-5 control-label">&nbsp;</label>
							<div class="col-md-8 m-t-5">
								<button type="button" class="btn btn-purple btn-default waves-effect" id="btnOpenRollcallManual" onclick="accessManualRollcall()">Buka Akses</button>
							</div>
						</div>
					</div>
					<?php // endif;?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>					
				</div>
			</div>
		</div>
	</div>
				
	<div class="row m-b-10">
		<?php if ($this->session->flashdata('notif_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
			</div>
		</div>
		<?php endif; ?>
		<?php if ($this->session->flashdata('notif_danger') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_danger')?>
			</div>
		</div>
		<?php endif; ?>
	
		<div class="col-sm-4 create-btn">			
			<?php // echo create_button($this->button, "btn_add"); ?>
			<div class="btn-group dropdown">
				<div class="btn-group dropdown">
					<ul class="dropdown-menu">						
						<?php foreach ($lp_cds as $lp_cd): ?>
						<li><a style="cursor: pointer" onclick="window.location='<?php echo site_url('rollcall/create/' . $lp_cd->lp_cd);?>'"><?php echo $lp_cd->lp_cd?></a></li>
						<?php endforeach; ?>
					</ul>
					<button class="btn btn-custom btn-bordered dropdown-toggle waves-light waves-effect w-md m-b-5" type="button" data-toggle="dropdown" aria-expanded="false">Start Roll Call <span class="caret"></span></button>
				</div>
			</div>
			<?php echo create_button($this->button, "btn_download"); ?>

			<?php 
			 $user_group_nm = $this->session->userdata(S_USER_GROUP_NM);      
				$role_nm = strtolower(str_replace(' ', '', $user_group_nm));
				if ($role_nm == 'administrator' || $role_nm == 'teamdriver'):									
			?>
			<button id="btn_rollcallmanual" type="button" class="btn btn-purple btn-bordered waves-light waves-effect w-md m-b-5" onclick="openManualRollcall()">Rollcall Manual</button>
			<?php endif; ?>
		</div>

        <div class="col-sm-8">
			<div class="row">
				<div class="col-sm-4">
					<label class="col-sm-12 control-label">Departure Date</label>
					<div class="col-md-5">
						<input type="text" class="text-center form-control datepicker" placeholder="Date Start" id="departure_dt_s" name="departure_dt_s" value="<?php echo $departure_dt_s?>" onchange="changeDepartureDt()" >
						<input type="hidden" id="dt_s" value="<?php echo $departure_dt_s?>" />
						<input type="hidden" id="dt_e" value="<?php echo $departure_dt_e?>" />
						<input type="hidden" id="business" value="all" />
						<input type="hidden" id="customer" value="all" />
						<input type="hidden" id="route" value="" />
						<input type="hidden" id="driver_cd" value="all" />
						<input type="hidden" id="vehicle_cd" value="all" />
					</div>
					<label class="col-sm-2">To</label>
					<div class="col-md-5">
						<input type="text" class="text-center form-control datepicker" placeholder="Date End" id="departure_dt_e" name="departure_dt_e" value="<?php echo $departure_dt_e?>" onchange="changeDepartureDt()" >
					</div>
				</div>
				<div class="col-sm-4">
					<label class="col-sm-12 control-label">Next Process</label>
					<div class="col-sm-12">
						<select id="status" class="form-control select2">
							<option value="all">All Process</option>
							<!-- <option value="0">1. TEAM DRIVER</option>
							<option value="1">2. DEPARTURE CASHIER</option>
							<option value="2">3. DEPARTURE CCR</option>
							<option value="3">4. ARRIVAL CASHIER</option>
							<option value="4">5. ARRIVAL CCR</option> -->
							<option value="0">1. DEPARTURE CASHIER</option>
							<option value="1">2. DEPARTURE CCR</option>
							<option value="2">3. ARRIVAL CASHIER </option>
							<option value="3">4. ARRIVAL CCR</option>
							<option value="4">5. FINISHED</option>
						</select>
					</div>
				</div>
				<div class="col-sm-4 text-right m-t-20">
					<button id="btn_filter" type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="doFilter()">Filter</button>
					<button id="btn_filter" type="button" class="btn btn-secondary btn-bordered waves-light waves-effect w-md m-b-5" onclick="doReset()">Reset</button>
				</div>
				<!-- <div class="col-sm-4">
					<label class="col-sm-12 control-label">Business</label>
					<div class="col-sm-12">
						<select id="business" class="form-control select2">                    					
							<option value="all">All Business</option>
							<option value="MILKRUN">MILKRUN</option>
							<option value="REGULER">REGULER</option>
						</select>
					</div>
				</div>
				<div class="col-sm-4">
					<label class="col-sm-12 control-label">Customer</label>
					<div class="col-sm-12">
						<select id="customer" class="form-control select2">                    					
							<option value="all">All Customer</option>
							<?php foreach ($customer as $c): ?>
							<option value="<?php echo $c->system_code?>"><?php echo $c->system_code . ' - ' . $c->system_value_txt?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div> -->
			</div>
			<!-- <div class="row m-t-10">
				<div class="col-sm-4">
					<label class="col-sm-12 control-label">Route</label>
					<div class="col-sm-12">
						<input type="text" placeholder="All Route" class="form-control" id="route" />
					</div>
				</div>
				<div class="col-sm-4">
					<label class="col-sm-12 control-label">Driver</label>
					<div class="col-sm-12">
						<select id="driver_cd" class="form-control select2">                    					
							<option value="all">All Driver</option>
							<?php foreach ($driver as $d): ?>
							<option value="<?php echo $d->driver_cd?>"><?php echo $d->driver_cd . ' - ' . $d->driver_name?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-sm-4">
					<label class="col-sm-12 control-label">Truck</label>
					<div class="col-sm-12">
						<select id="vehicle_cd" class="form-control select2">
							<option value="all">All Truck</option>
							<?php foreach ($vehicle as $d): ?>
							<option value="<?php echo $d->vehicle_cd?>"><?php echo $d->vehicle_cd?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div> -->
			<!-- <div class="row m-t-10" style="margin-right: 5px">
				
				<div class="col-sm-4">
				</div>
				
			</div> -->
        </div>
	</div>
	
	<div class="row">		
		<div class="col-sm-12">
			<div class="card-box table-responsive">
				<table id="datatable_rollcall" class="table table-striped table-hover display nowrap">
					<thead>
					<tr>
						<th rowspan="2">Rollcall ID</th>
						<th rowspan="2">No. Driver Report</th>
						<th rowspan="2">Next Process</th>
						<th rowspan="2">Driver</th>
						<th rowspan="2">Truck</th>
                        <th rowspan="2">Route</th>
						<th rowspan="2">Cycle</th>
						<th rowspan="2">Departure Date</th>
						<th rowspan="2">IN Plan</th>
						<th rowspan="2">IN Actual</th>
						<th rowspan="2">OUT Plan</th>
						<th rowspan="2">OUT Actual</th>
                        <th rowspan="2">Business</th>                     
                        <th rowspan="2">Customer</th>
						<th rowspan="2">Clock-In</th>
						<th rowspan="2">Clock-Out</th>
						<th colspan="6">Team Driver</th>
						<th colspan="3">Fuel</th>
						<th colspan="6">E-Toll</th>
						<th colspan="3">Others</th>
						<th colspan="3">Departure</th>
						<th colspan="3">Arrival</th>
					</tr>
					<tr>
						<th>NIK</th>
						<th>SIM</th>
						<th>SIO</th>
						<th>Blood Pressure</th>
						<th>Temperature</th>
						<th>Sleeping Time</th>
						<th>Plan</th>
						<th>Actual</th>
						<th>Remark</th>
						<th>Plan</th>
						<th>E-Toll Card</th>
						<th>Saldo Awal</th>
						<th>Saldo Akhir</th>
						<th>Balance</th>
						<th>Remark</th>
						<th>Plan</th>
						<th>Actual</th>
						<th>Remark</th>
						<th>APD</th>
						<th>Basket</th>
						<th>Kunci</th>
						<th>APD</th>
						<th>Basket</th>
						<th>Kunci</th>
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>			
</div>