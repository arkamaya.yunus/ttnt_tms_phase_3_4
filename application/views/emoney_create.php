<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('emoney')?>">E-Money</a>
					</li>
					<li class="active"><?php echo $stitle?></li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->

	<div class="row">
		<?php if ($save_sts == '0'): ?>
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> Save failed.
			</div>
		</div>
		<?php endif; ?>

		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">E-Money ID <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
										<input type="text" class="form-control" id="emoney_id" name="emoney_id" value="" required/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Category <span class="text-danger">*</span></label>
									<div class="col-md-8 m-t-5">
										<select id="category" name="category" class="form-control select2" required>
											<option></option>
											<?php foreach ($categories as $ct): ?>
											<option value="<?php echo $ct->system_code?>"><?php echo $ct->system_value_txt ?></option>
											<?php endforeach; ?>
										</select>
									</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Bank </label>
								<div class="col-md-8 m-t-5">
										<input type="text" class="form-control" id="bank" name="bank" value="" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Balance</label>
								<div class="col-md-8 m-t-5">
										<input type="text" class="form-control autonumber text-right" id="balance" name="balance" value="0" />
								</div>
						</div>
					</div>

					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('vehicle')?>'">Back</button>
						<!-- <?php echo create_button($this->button, "btn_submit"); ?> -->
						<button name="btn_submit" type="button" id="btn_submit" class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5" onclick="doSubmit()" do_submit=""><i class=""> </i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
