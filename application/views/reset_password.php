<!DOCTYPE html>
<html>
    <head>        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php APP_NAME ?> Of PT. Arkamaya">
        <meta name="author" content="irfan.satriadarma@gmail.com">

        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon2.png">
        <title>Lupa Kata Sandi - <?php echo APP_NAME ?></title>

        <!-- App css -->
        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script>
            var SITE_URL = '<?php echo base_url(); ?>';
        </script>
    </head>


    <body class="bg-transparent">

        <!-- HOME -->
        <section>
            <div class="container-alt">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrapper-page">

                            <div class="m-t-50 account-pages">
                                <div class="text-center account-logo-box">
                                    <h2 class="text-uppercase">
                                        <a href="<?php echo site_url(); ?>" class="text-success">
                                            <span><img src="<?php echo base_url() ?>assets/images/logo-text-dark.png" alt="" height="40"></span>
                                        </a>
                                    </h2>
                                </div>
                                <div class="account-content">
                                    <div class="text-center m-b-20">
                                        <p class="text-muted m-b-0">Masukan kata sandi yang baru untuk Akun Email anda, <strong><?php echo $um?></strong> </p>
                                    </div>
                                    <form class="form-horizontal" action="" method="post" id="form_send_email">
										<input type="hidden" name="key" value="<?php echo $key; ?>">
                                        <input name="email" type="hidden" value="<?php echo $um?>">
                                        <div class="form-group item">
                                            <div class="col-xs-12">
                                                <input class="form-control" name="newpassword" type="password" id="newpassword"  required="" placeholder="Kata Sandi">
                                            </div>
                                        </div>
                                        <div class="form-group item">
                                            <div class="col-xs-12">
                                                <input class="form-control" id="confirm_password" name="confirm_password" type="password" placeholder="Tulis Ulang Kata Sandi">
                                            </div>
                                        </div>
                                        <div class="form-group">

                                            <?php if ($this->session->flashdata('notif_reset_pass') != ''): ?>
                                                <div class="col-sm-12">
                                                    <?php if ($this->session->flashdata('notif_status') == 'success') { ?>
                                                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                                                        <?php } else { ?>
                                                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                                        <?php } ?>
                                                        <?php echo $this->session->flashdata('notif_reset_pass') ?>

                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
											<div class="form-group">
												<div class="col-xs-12">
													<button name="btn_submit" id="btn_submit" class="form-control btn btn-bordered btn-custom btnResend" type="button" onclick="on_resend()">Simpan</button>
												</div>
											</div>

                                    </form>

                                    <div class="clearfix"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end wrapper -->

                </div>
            </div>
        </div>
    </section>
    <!-- END HOME -->

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/detect.js"></script>
    <script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo base_url() ?>assets/js/waves.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/plugins/jquery-validate/jquery.validate.js"></script>
    <!-- App js -->
    <script src="<?php echo base_url() ?>assets/js/jquery.core.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.app.js"></script>

    <script>
        $(document).ready(function () {

            $('#form_send_email').validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                    , newpassword: {
                        required: true,
                        minlength: 8
                    },
                    confirm_password: {
                        equalTo: "#newpassword"
                    }
                },
                messages: {
                    email: {
                        required: "Email harus diisi",
                        email: "Format email salah"
                    }
                    ,newpassword: {
                        required: "Kata Sandi harus diisi",
                        minlength: "Kata Sandi harus lebih dari 8 karakter"
                    },
                    confirm_password: {
                        minlength: "Kata Sandi harus lebih dari 8 karakter",
                        equalTo: "Kata sandi yang dimasukan tidak sama"
                    }
                }
                , highlight: function (element) {
                    $(element).closest('.item').removeClass('has-success').addClass('has-error');
                }
                , success: function (element) {
                    $(element).closest('.item').removeClass('has-error').addClass('has-success');
                }

            });

            
        });
        
        function on_resend() {
            var valid = $('#form_send_email').validate();
            if (valid.form()) {
                $('.btnResend').addClass('disabled').html('Mohon Tunggu  <i class="fa fa-spinner fa-pulse fa-fw"></i>');
                $('#form_send_email').submit();
            }
        }

    </script>
</body>

<!-- page-register.html 13:25:29 GMT -->
</html>