<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('emoney')?>">E-Money</a>
					</li>
					<li class="active"><?php echo $stitle?></li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	<?php echo $category; ?>
	<div class="row">
		<?php if ($save_sts == '0'): ?>
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> Save failed.
			</div>
		</div>
		<?php endif; ?>

		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<input type="hidden" name="emoney_id" id="emoney_id" value="<?php echo $emoney_id?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">E-Money ID <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
                  	<input type="text" class="form-control" id="emoney_idx" name="emoney_idx" value="<?php echo $emoney_id?>" disabled="disabled" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Category <span class="text-danger">*</span></label>
									<div class="col-md-8 m-t-5">
										<select id="category" name="category" class="form-control select2" required>
											<option></option>
											<?php foreach ($categories as $ct): ?>
											<?php
												$selected = '';
												if($ct->system_code == $category) {
													$selected = 'selected="selected"';
												}else {
													$selected = '';
												}
											?>
											<option value="<?php echo $ct->system_code?>" <?php echo $selected ?>><?php echo $ct->system_value_txt ?></option>
											<?php endforeach; ?>
										</select>
									</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Bank </label>
								<div class="col-md-8 m-t-5">
                  	<input type="text" class="form-control" id="bank" name="bank" value="<?php echo $bank?>" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Balance</label>
								<div class="col-md-8 m-t-5">
                    <input type="text" class="form-control autonumber text-right" id="balance" name="balance" value="<?php echo $balance?>" />
								</div>
						</div>
					</div>
						<div class="col-sm-6">

							<div class="form-group">
								<label class="col-md-4 control-label">Customer</label>
								<div class="col-md-8 m-t-5">
									<?php echo $customer?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Cycle</label>
								<div class="col-md-8 m-t-5">
									<?php echo $cycle?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Gel</label>
								<div class="col-md-8 m-t-5">
									<?php echo $gel?>
								</div>
							</div>

						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('emoney')?>'">Back</button>
						<!-- <?php echo create_button($this->button, "btn_submit"); ?> -->
						<button name="btn_submit" type="button" id="btn_submit" class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5" onclick="doSubmit()" do_submit=""><i class=""> </i> Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
