<style>
    #sidebar-pengaturan{
        padding-bottom:30px;
        width:100%;
    }
    #container-forms{
        border-left: 2px solid black;
        padding-left:25px;
    }

    #sidebar-pengaturan, #sidebar-pengaturan a, #sidebar-pengaturan li, #sidebar-pengaturan ul {
        font-weight: 400;
        line-height: 1;
        list-style: none;
        margin: 0;
        padding: 0;
        position: relative;
        text-decoration: none;
    }

    #sidebar-pengaturan > ul > li > a {
        color: #2e383d;
        display: block;
        padding: 12px 20px;
        margin: 2px 0;
    }
    #sidebar-pengaturan ul li.active a, .button-menu-mobile:hover {
        color: #7fc1fc;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Pengaturan</h4>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <?php if ($this->session->flashdata('notif_users_success') != ''): ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_users_success')?>
			</div>
		</div>
	</div>
	<?php endif; ?>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Add New</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<form id="frm" class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
								<div class="form-group">
									<input type="text" class="form-control" name="user_group_description" value="" placeholder="Role Name" required />
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary waves-effect waves-light" onclick="doSave()" id="btn_save_role">Save</button>
				</div>
			</div>
		</div>
	</div>
    
    <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal2Label" aria-hidden="true">
        <form id="frm2" class="form-horizontal" role="form" method="post" action="">
            <input type="hidden" id="user_group_id_delete" name="user_group_id_delete" value="" />
        </form>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModal2Label">Delete</h4>
                </div>
                <div class="modal-body">
                    Are you sure?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger waves-effect waves-light" onclick="doDelete()" id="btn_delete_confirm">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <form id="settings_users" class="form-horizontal" role="form" method="post" action="<?php echo site_url('settings/create_users')?>" enctype="multipart/form-data">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-2">
                            <div id="sidebar-pengaturan">
                               <?php echo $menu;?>
                            </div>
                        </div>
                        <div class="col-md-10" id="container-forms">
                            <div class="rows">
                                <div class="col-md-6 m-b-20">
                                    <h4 class="page-title pb-20">Roles</h4>
                                </div>
                                <div class="col-md-6 m-b-20" style="text-align: right">                                    
                                    <button id="btn_add" type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" onclick="$('#myModal').modal('show');" >Create</button>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <?php if ($this->session->flashdata('notif_success') != ''): ?>
                                        <div class="col-sm-12">
                                            <div class="alert alert-success" role="alert">
                                                <i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <div class="col-sm-12">
                                            <table id="datatable" class="table table-striped table-hover display nowrap table-colored table-primary">
                                                <thead>
                                                    <tr>
                                                        <th>Role Name</th>
                                                        <th style="text-align: center">Num Of Users</th>
                                                        <th style="text-align: center">Authorization</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($roles as $r): ?>
                                                    <tr>
                                                        <td><?php echo $r->user_group_description?></td>
                                                        <td style="text-align: center"><?php echo $r->num_of_users?></td>
                                                        <td style="text-align: center">
                                                            <a href="<?php echo site_url('settings/roles/id/' . md5($r->user_group_id))?>" title="view detail">Edit</a>
                                                            <?php if ($r->num_of_users == 0 && $r->is_admin == 0):?>
                                                                &nbsp;&nbsp; | &nbsp;&nbsp;
                                                            <a href="javascript:;" onclick="confirmDelete('<?php echo $r->user_group_id?>')" title="Delete">Delete</a>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>		
                                </div>
                            </div>                            
                        </div>                       
                    </div>
                    <div class="row">
                        
                    </div>
                </div>
                
            </form>
        </div>
</div>