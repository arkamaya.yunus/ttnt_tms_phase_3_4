<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li class="active"><?php echo $stitle?></li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Upload</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<form id="frm" class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
								<input type="hidden" id="file_nm" name="file_nm" value="" required>
								<div class="form-group">
									<label class="col-md-4 col-sm-3 control-label">Choose File <span class="text-danger">*</span></label>
									<div class="col-md-8 col-sm-9">
									<input class="filestyle" data-size="sm" data-iconname="fa fa-cloud-upload" id="file_attach" name="file_attach" accept=".xlsx" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" tabindex="-1" required onchange= "checkextension();"type="file">
										<a href="<?php echo base_url().'assets/files/template-vehicle.xlsx'?>" style="font-size: 11px;" target="_blank" style="font-size: 11px;">Download Template</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary waves-effect waves-light" onclick="doUploadProcess()" id="btn_upload_confirm">Upload</button>
				</div>
			</div>
		</div>
	</div>
				
	<div class="row m-b-10">
		<?php if ($this->session->flashdata('notif_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
			</div>
		</div>
		<?php endif; ?>
	
		<div class="col-sm-4">			
			<?php echo create_button($this->button, "btn_add"); ?>
			<?php echo create_button($this->button, "btn_upload"); ?>
			<?php echo create_button($this->button, "btn_download"); ?>
		</div>

        <div class="col-sm-8">
            <!-- <label class="col-md-2 control-label m-t-10 text-right">Owner</label> -->
            <div class="col-md-3">
                <select id="vehicle_owner" class="form-control select2">                    					
					<option value="all">All Owner</option>
					<?php foreach ($owners as $owner): ?>
					<option value="<?php echo $owner->vehicle_owner?>"><?php echo $owner->vehicle_owner?></option>
					<?php endforeach; ?>
                </select>
			</div>
			<!-- <label class="col-md-2 control-label m-t-10 text-right">Active</label> -->
            <div class="col-md-3">
                <select id="active" class="form-control select2">                    					
					<option value="all">All Active Status</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
                </select>
            </div>
			<div class="col-md-3">
                <select id="business" class="form-control select2">                    					
					<option value="all">All Business</option>
					<option value="MILKRUN">MILKRUN</option>
					<option value="REGULER">REGULER</option>
                </select>
            </div>
            <div class="col-md-2">
                <button id="btn_filter" type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="doFilter()">Filter</button>
            </div>
        </div>
	</div>	
	
	<div class="row">		
		<div class="col-sm-12">
			<div class="card-box table-responsive">
				<table id="datatable" class="table table-striped table-hover display nowrap">
					<thead>
					<tr>
						<th>Truck Number</th>
						<!-- <th>Vehicle ID (Innova Track)</th> -->
						<th>Police Number</th>
						<th>Vehicle Type</th>
                        <th>STNK</th>
                        <th>KEUR</th>
                        <th>SIPA</th>
                        <th>IBM</th>
						<th>Owner</th>
						<th>Brand</th>
						<th>Active</th>
						<th>Business</th>
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>			
</div>