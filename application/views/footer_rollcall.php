</div> <!-- content -->

</div>

<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/detect.js"></script>
<script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url() ?>assets/js/waves.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.core.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.app.js"></script>

<!-- Plugin JS -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/autocomplete/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/select2/select2/select2.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/autoNumeric/autoNumeric.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.keyTable.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.colVis.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/datatables/jquery.dataTables.rowGrouping.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/apps/default.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/toastr/toastr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/countUp/countup.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/jquery-printarea/jquery.PrintArea.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/morris.js-0.5.1/raphael-min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/morris.js-0.5.1/morris.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/switchery/switchery.min.js"></script>

<?php if (isset($externaljs)){ foreach ($externaljs as $js){ ?>
<script type="text/javascript" src="<?php echo $js ?>"></script>
<?php } } ?>

<?php if (isset($jsapp)): foreach ($jsapp as $js): ?>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/apps/<?php echo $js ?>.js"></script>
<?php
    endforeach;
endif;
?>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/apps/jquery.validate.message.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/apps/changepwd.js"></script>

<script src="<?php echo base_url() ?>assets/js/apps/kelola.biz.js"></script>
<script src="<?php echo base_url() ?>assets/js/pace.min.js"></script>
</body>
</html>
