<style>
#dtDepartureDelay tr td,
#dtArrivalDelay tr td
{
	font-size: 12px;
	cursor: pointer;
}

.card-body {
  height: 500px;
  overflow-y: scroll;
}
</style>

<div class="container">
	
	<form id="f_dashboard_business" name="f_dashboard_business" method="post" action="">
	<div class="row">
		<div class="col-xs-12">			
			<div class="page-title-box" style="padding-bottom: 15px">
				<div class="row">
					<div class="col-md-6">
						<h4 class="page-title" style="margin-top: 10px"><?php echo $stitle?></h4>
					</div>
					<div class="col-md-6">
						<div class="col-md-2 m-t-10" style="color: red"><span id="pLoad" style="display: none" ><i class="fa fa-spinner fa-pulse fa-fw"></i> Sync...</span></div>
						<div class="col-md-6">
							<select id="customer_lp_cd" class="form-control select2">                    					
								<option value="all">All Customer Logistic Point</option>
								<?php foreach ($customer_lp_cds as $customer_lp_cd): ?>
								<option value="<?php echo $customer_lp_cd->lp_cd?>"><?php echo $customer_lp_cd->lp_cd?> - <?php echo $customer_lp_cd->lp_name?></option>
								<?php endforeach; ?>
							</select>
							<input type="hidden" name="logistic_partner" id="logistic_partner" value="all" />                
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control datepicker" id="timetable_dt" name="timetable_dt" value="<?php echo date('d/m/Y')?>" />
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	</form>	    

	<div class="row">
		<div class="col-lg-6">
			<div class="card">
				<div class="card-header bg-light">										
					<div class="row">
						<div class="col-md-6">
						<h4 class="card-title mb-0">Departure Delay</h4>
						</div>						
					</div>
				</div>
				<div class="card-body">
					<table id="dtDepartureDelay" class="table table-sm table-striped table-hover table-colored-bordered table-bordered-primary">
						<thead>
							<tr>
								<th>Customer LP</th>
								<th>Route</th>
								<th>Cycle</th>
								<th>Status</th>
								<th>Truck</th>
								<th>Driver</th>
								<th>Delay Time</th>
								<th>Remark</th>
							</tr>
						</thead>						
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="card">
				<div class="card-header bg-light">										
					<div class="row">
						<div class="col-md-6">
						<h4 class="card-title mb-0">Arrival Delay</h4>
						</div>
						<div class="col-md-6 m-t-20 text-right" id="pLoad2" style="display: none"><span style="color: red"><i class="fa fa-spinner fa-pulse fa-fw"></i> Sync...</span></div>
					</div>
				</div>
				<div class="card-body">
					<table id="dtArrivalDelay" class="table table-sm table-striped table-hover table-colored-bordered table-bordered-primary">
						<thead>
							<tr>
								<th>Customer LP</th>
								<th>Route</th>
								<th>Cycle</th>
								<th>Status</th>
								<th>Truck</th>
								<th>Driver</th>
								<th>Delay Time</th>
								<th>Remark</th>
							</tr>
						</thead>						
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- end row -->
</div>