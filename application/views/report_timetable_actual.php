<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('timetable')?>">Timetable</a>
					</li>
					<li class="active">Detail</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->

    <div class="row">
        <?php if ($this->session->flashdata('notif_error') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_error')?>
			</div>
		</div>
		<?php endif; ?>
        <div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">                                                
							<div class="form-group">
								<label class="col-md-4 control-label">Planning Date</label>
								<div class="col-md-4 m-t-5">
                                    <input type="text" class="form-control datepicker f1" id="timetable_dt" name="timetable_dt" value="<?php echo date('d/m/Y', strtotime($timetable_dt))?>" onchange="changeTimetableDt()" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Business</label>
								<div class="col-md-8 m-t-5">
                                    <select id="business" class="form-control f1" onchange="changeBusiness()">
                                        <option value="all">All Business</option>
                                        <?php foreach ($business as $b): ?>
                                        <option value="<?php echo $b->business?>"><?php echo $b->business?></option>
                                        <?php endforeach; ?>
                                    </select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Customer</label>
								<div class="col-md-8 m-t-5">
                                    <select id="customer" class="form-control f1" onchange="changeCustomer()">
                                        <option value="all">All Customer</option>
                                        <?php foreach ($customers as $c): ?>
                                        <option value="<?php echo $c->customer?>" data-business="<?php echo $c->business?>" class="s2-business s2-business-<?php echo $c->business?>"><?php echo $c->customer . ' - ' . $c->system_value_txt?></option>
                                        <?php endforeach; ?>
                                    </select>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Customer Logistic Point</label>
								<div class="col-md-8 m-t-5">
                                    <select id="customer_lp_cd" class="form-control f1" onchange="changeCustomerLP()">                    					
                                        <option value="all">All Customer Logistic Point</option>
                                        <?php foreach ($customer_lp_cds as $c): ?>
                                        <option value="<?php echo $c->customer_lp_cd?>" class="s2-customer s2-customer-<?php echo $c->customer?> s2-business s2-business-<?php echo $c->business?>"><?php echo $c->customer_lp_cd?></option>
                                        <?php endforeach; ?>
                                    </select>
								</div>
							</div>
                            <div class="form-group">
								<label class="col-md-4 control-label">Route</label>
								<div class="col-md-8 m-t-5">
                                    <select id="route" class="form-control f1" onchange="changeRoute()">
                                        <option value="all">All Route</option>
                                        <?php foreach ($routes as $r): ?>
                                        <option value="<?php echo $r->route?>" class="s2-customer s2-customer-<?php echo $r->customer?> s2-business s2-business-<?php echo $r->business?> s2-customer_lp_cd s2-customer_lp_cd-<?php echo str_replace(' ', '_', $r->customer_lp_cd)?>"><?php echo $r->route?></option>
                                        <?php endforeach; ?>
                                    </select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Cycle</label>
								<div class="col-md-8 m-t-5">
                                    <select id="cycle" class="form-control f1">                    					
                                        <option value="all">All Cycle</option>
                                        <?php foreach ($cycles as $r): ?>
                                        <option value="<?php echo $r->cycle?>" class="s2-route s2-route-<?php echo str_replace(' ', '_', $r->route)?> s2-cycle">
											<?php echo $r->cycle?></option>
                                        <?php endforeach; ?>
                                    </select>
								</div>
							</div>
						</div>
					</div>
					<div class="row timetable-detail timetable-detail-1">
						<div class="col-md-12">  
							<button type="button" class="pull-right btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" id="btnPrint" onclick="doDownload()">Download</button>
						</div>
					</div>
					<hr/>
                </div>
            </form>
        </div>
    </div>
</div>