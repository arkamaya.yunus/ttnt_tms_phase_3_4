<style>

.table-condensed{
  font-size: 13px;
}

td.td-chart
{
    max-width: 25px;
    min-width: 25px;
    overflow: visible;
}

.cus-chart-container {
    overflow:auto;
    height: 400px;
}

table.cus-chart th:first-child, table.cus-chart td:first-child {
  white-space: nowrap;
    background-color: #f9f9f9;
}
table.cus-chart th:nth-child(2), table.cus-chart td:nth-child(2) {
  white-space: nowrap;
    background-color: #f9f9f9;
}
table.cus-chart th:nth-child(3), table.cus-chart td:nth-child(3) {
  white-space: nowrap;
    background-color: #f9f9f9;
}

/* .table-fixed-head thead th {
  position: sticky;
  top: 0;
  background-color: #ffffff;
} */

/* freezing first, second and third table col */
/* table.cus-chart th:first-child, table.cus-chart td:first-child {
  position:sticky;
  left:0px;
  background-color: #f9f9f9;
	width: 32px;
  text-align: left!important;
}
table.cus-chart th:nth-child(2), table.cus-chart td:nth-child(2) {
	position:sticky;
	left:0px;
	background-color: #f9f9f9;
	min-width: 50px;
	left: 32px;
  text-align: left!important;
}
table.cus-chart th:nth-child(3), table.cus-chart td:nth-child(3) {
	position:sticky;
	left:0px;
	background-color: #f9f9f9;
  min-width: 100px;
	left:80px;
  text-align: left!important;
} */
table.cus-chart > thead > tr > th {
     vertical-align: middle;
		 text-align: center;
}
.tebal {
      border-right:1px solid #b8b8b8!important;
}
/* end of freezing first, second and third table col */
</style>
<div class="container">

	<form id="timeline_form" name="timeline_form" method="post" action="<?php site_url('dashboard/dashboard_timeline'); ?>">
	<div class="row">
		<div class="col-md-12">

			<div class="page-title-box" style="padding-bottom: 15px">
        <div class="row">
          <div class="col-md-6">
    				<h4 class="page-title" style="margin-top: 10px"><?php echo $stitle?></h4>
    				<div class="clearfix"></div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-sm-9" style="padding-top: 18px;">
                <!-- <label class="col-sm-12 control-label">Date</label> -->
                <label class="col-sm-2 text-center" style="margin-top:5px;">From</label>
                <div class="col-md-4">
                  <input type="text" class="text-center form-control datepicker" placeholder="Date Start" id="from" name="from" value="<?php echo $get_from ?>">
                </div>
                <label class="col-sm-2 text-center" style="margin-top:5px;">To</label>
                <div class="col-md-4">
                  <input type="text" class="text-center form-control datepicker" placeholder="Date End" id="to" name="to" value="<?php echo $get_to ?>">
                </div>
              </div>
              <div class="col-sm-3 text-right m-t-20">
                <button id="btn_filter" type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="doFilter();">Filter</button>
              </div>
            </div>
          </div>
        </div>
			</div>

		</div>
	</div>
	</form>
    <div class="row">
		<div class="col-sm-12">
			<!-- <div class="cus-chart-container"> -->
        <?php if(!empty($timeline)){ ?>
				<table class="table cus-chart table-bordered table-striped table-sm table-condensed" id="table-cus-chart">
  				<thead>
  					<tr>
  						<th rowspan="2">No.</th>
  						<th rowspan="2" style="white-space: nowrap;">Truck</th>
  						<th rowspan="2" style="white-space: nowrap;">Route Name</th>
  						<th colspan="6" class="tebal">1</th>
  						<th colspan="6" class="tebal">2</th>
  						<th colspan="6" class="tebal">3</th>
  						<th colspan="6" class="tebal">4</th>
  						<th colspan="6" class="tebal">5</th>
  						<th colspan="6" class="tebal">6</th>
  						<th colspan="6" class="tebal">7</th>
  						<th colspan="6" class="tebal">8</th>
  						<th colspan="6" class="tebal">9</th>
  						<th colspan="6" class="tebal">10</th>
  						<th colspan="6" class="tebal">11</th>
  						<th colspan="6" class="tebal">12</th>
  						<th colspan="6" class="tebal">13</th>
  						<th colspan="6" class="tebal">14</th>
  						<th colspan="6" class="tebal">15</th>
  						<th colspan="6" class="tebal">16</th>
  						<th colspan="6" class="tebal">17</th>
  						<th colspan="6" class="tebal">18</th>
  						<th colspan="6" class="tebal">19</th>
  						<th colspan="6" class="tebal">20</th>
  						<th colspan="6" class="tebal">21</th>
  						<th colspan="6" class="tebal">22</th>
  						<th colspan="6" class="tebal">23</th>
  						<th colspan="6" class="tebal">24</th>
  					</tr>
            <tr style="display:none;">
              <?php
              for($x = 1; $x <= 144; $x++){
                echo '<td class="td-chart"></td>';
              }
              ?>
            </tr>
  				</thead>
  				<tbody>
  					<?php
  						$temp_keytable = '';
  						$i = 1;
  						foreach ($timeline as $t) {
  							if($temp_keytable != $t->keytable){
  								$temp_keytable = $t->keytable;
  								$route_cycle = $t->route.'-'.$t->cycle;
  								$vehicle_cd = $t->vehicle_cd;
                  $cycle = $t->cycle;
  								echo '<tr>';
  								echo '<td>'.$i.'</td>';
  								echo '<td>'.$vehicle_cd.'</td>';
  								echo '<td>'.$route_cycle.'</td>';

  								//render chart
  								for($x = 1; $x <= 24; $x++){
                    $xp = sprintf("%02d", $x);
  									echo '<td class="td-chart" id='.$temp_keytable.$cycle.$xp.'10'.'></td>';
  									echo '<td class="td-chart" id='.$temp_keytable.$cycle.$xp.'20'.'></td>';
  									echo '<td class="td-chart" id='.$temp_keytable.$cycle.$xp.'30'.'></td>';
  									echo '<td class="td-chart" id='.$temp_keytable.$cycle.$xp.'40'.'></td>';
  									echo '<td class="td-chart" id='.$temp_keytable.$cycle.$xp.'50'.'></td>';
  									echo '<td class="td-chart tebal" id='.$temp_keytable.$cycle.$xp.'60'.'></td>';
  								}
  								//end of rendering chart
  								echo '</tr>';
  								$i++;
  							}
  						}
  					 ?>
  				 </tbody>
			  </table>
      <?php }else { ?>
        <div class="alert alert-info">There's no data for the current date range, please try another range.</div>
      <?php } ?>
			<!-- </div> -->
		</div>
	</div>
</div>
