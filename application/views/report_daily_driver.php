<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('timetable')?>">Timetable</a>
					</li>
					<li class="active">Detail</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->

    <div class="row">
        <?php if ($this->session->flashdata('notif_error') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_error')?>
			</div>
		</div>
		<?php endif; ?>
        <div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<div class="card-box">
					<div class="row">
						<div class="col-md-10">
							<div class="form-group">
								<label class="col-md-2 control-label">Planning Date</label>
								<div class="col-md-2 m-t-4">
                  <input type="text" class="form-control datepicker f1" id="timetable_dt" name="timetable_dt" value="<?php echo date('d/m/Y', strtotime($timetable_dt))?>" />
								</div>
								<div class="col-md-2 m-t-4">
									<input type="text" class="form-control datepicker f1" id="timetable_df" name="timetable_df" value="<?php echo date('d/m/Y', strtotime($timetable_df))?>" />
								</div>
								<div class="col-md-3">
									<select id="customer" class="form-control select2">
										<option value="all">All Customer</option>
										<?php foreach ($customer as $c): ?>
										<option value="<?php echo $c->system_code?>"><?php echo $c->system_code . ' - ' . $c->system_value_txt?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-md-3">
									<select id="departure_area" class="form-control select2">
										<option value="all">All Area</option>
										<?php foreach ($departure_area as $c): ?>
										<option value="<?php echo $c->lp_cd?>"><?php echo $c->lp_cd?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>

						</div>
						<div class="col-md-2">
							<button type="button" class="pull-right btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" id="btnPrint" onclick="doDownload()">Download</button>
						</div>
					</div>

					<hr/>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
var selected_customer = '<?php echo $selected_customer ?>';
var selected_departure_area = '<?php echo $selected_departure_area ?>';
</script>
