<!DOCTYPE html>
<html class="account-pages-bg">
    <head>        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo APP_NAME ?>">
        <meta name="author" content="irfan.satriadarma@gmail.com">

        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon2.png">
        <title><?php echo APP_NAME ?> - Kirim Ulang Kode Aktivasi</title>

        <!-- App css -->
        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <style>
            .account-content{
                background-color: #fff;
            }
            .account-logo-box {
                padding: 20px 10px 5px 10px; 
            }
            .account-pages .account-content {
                padding: 20px;
            }
            h2 {
                font-family: "Varela Round",sans-serif;
            }
            label {
                font-weight: normal;
            }
        </style>

        <script>
            var SITE_URL = '<?php echo base_url(); ?>';
        </script>
    </head>


    <body class="bg-transparent">

        <!-- HOME -->
        <section>
            <div class="container-alt">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrapper-page">

                            <div class="m-t-50 account-pages">
                                <div class="text-center account-logo-box">
                                    <h2 class="text-uppercase">
                                        <a href="<?php echo site_url(); ?>" class="text-success">
                                            <span><img src="<?php echo base_url() ?>assets/images/logo-text-dark.png" alt="" height="40"></span>
                                        </a>
                                    </h2>
                                </div>
                                <div class="account-content">
                                    <div class="text-center m-b-20">
										<h4>Kirim ulang Kode Aktivasi</h4>
                                        <p class="text-muted m-b-0 font-13">Masukan alamat email anda dan kami akan mengirim ulang tautan untuk aktivasi pendaftaran.  </p>
                                    </div>
                                    
									<form class="form-horizontal" action="" method="post" id="form_resend">
										<div class="form-group">

                                            <?php if ($this->session->flashdata('notif_resend') != ''): ?>
											<div class="col-sm-12">
												<div class="text-muted alert alert-<?php echo ($this->session->flashdata('notif_status') == 'success') ? 'success' : 'danger'?> alert-dismissible fade in" role="alert">
													<?php echo $this->session->flashdata('notif_resend') ?>
												</div>
											</div>
											<?php endif; ?>
										</div>
									
                                        <div class="form-group item">
                                            <div class="col-xs-12">
                                                <input class="form-control" name="email" type="email" required="" placeholder="Email saat pendaftaran">
                                            </div>
                                        </div>
                                        
										<div class="form-group">
                                            <div class="col-xs-12">
                                                <button name="btn_submit" id="btn_submit" class="form-control btn btn-bordered btn-custom btnResend" type="button" onclick="on_resend()">Kirim Ulang Email Aktivasi</button>
                                            </div>
                                        </div>
										
										<div class="form-group text-center m-t-30">
											<div class="col-sm-12">
                                            <a href="<?php echo base_url(); ?>login"><i class="fa fa-lock m-r-5"></i> Login <span class="text-muted">bila punya akun</span></a>
											</div>
											<div class="col-sm-12">
                                            <a href="<?php echo base_url(); ?>register"> <span class="text-muted">Belum Memiliki Akun?</span> Daftar Sekarang</a>
											</div>
										</div>

                                    </form>

                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end wrapper -->

                </div>
            </div>
        </div>
    </section>
    <!-- END HOME -->

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/detect.js"></script>
    <script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo base_url() ?>assets/js/waves.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js//plugins/jquery-validate/jquery.validate.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.core.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.app.js"></script>


    <script>
        $(document).ready(function () {

            $('#form_resend').validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    email: {
                        required: "Email harus diisi",
                        email: "Format email salah"
                    }
                }
                , highlight: function (element) {
                    $(element).closest('.item').removeClass('has-success').addClass('has-error');
                }
                , success: function (element) {
                    $(element).closest('.item').removeClass('has-error').addClass('has-success');
                }

            });

            
        });
        
        function on_resend() {
            var valid = $('#form_resend').validate();
            if (valid.form()) {
                $('.btnResend').addClass('disabled').html('Mohon Tunggu  <i class="fa fa-spinner fa-pulse fa-fw"></i>');
                $('#form_resend').submit();
            }
        }

    </script>
</body>

<!-- page-register.html 13:25:29 GMT -->
</html>