<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Report</li>
					<li class="active">
						<a href="<?php echo site_url('report_driver')?>">Report Driver</a>
					</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->

    <div class="row">
    <?php if ($this->session->flashdata('notif_error') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_error')?>
			</div>
		</div>
		<?php endif; ?>
		<div class="row m-b-10">
			<div class="col-sm-4">
				<label class="col-sm-12 control-label">Departure Date</label>
				<div class="col-md-5">
					<input type="text" class="text-center form-control datepicker" placeholder="Date Start" id="departure_dt_s" name="departure_dt_s" value="<?php echo $today?>">
					<input type="hidden" id="dt_s" value="<?php echo $today?>" />
					<input type="hidden" id="dt_e" value="<?php echo $today?>" />
				</div>
				<label class="col-sm-2">To</label>
				<div class="col-md-5">
					<input type="text" class="text-center form-control datepicker" placeholder="Date End" id="departure_dt_e" name="departure_dt_e" value="<?php echo $today?>">
				</div>
			</div>
			<div class="col-sm-3 text-right m-t-20">
				<button id="btn_filter" type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="doFilter()">Filter</button>
				<button id="btn_filter" type="button" class="btn btn-secondary btn-bordered waves-light waves-effect w-md m-b-5" onclick="doReset()">Reset</button>
			</div>
		</div>
        <div class="col-sm-12">
					<form id="frm" class="form-horizontal" role="form" method="post" action="">
							<div class="row">
								<div class="col-sm-12">
									<div class="card-box table-responsive">
										<table id="datatable_report_driver" class="table table-striped table-hover display nowrap">
											<thead>
											<tr>
												<th>ID</th>
												<th>Voucher</th>
												<th>Driver Cd</th>
												<th>Driver Name</th>
												<th>Vehicle Cd</th>
												<th>Route</th>
												<th>Cycle</th>
												<th>Departure Plan</th>
												<th>Action</th>
											</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
		      </form>
        </div>
    </div>
</div>
