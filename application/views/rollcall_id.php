<style>
    .btn-dflt
    {
        border-bottom: 2px solid #dadada !important;
    }
    .rcdata, 
    .control-label,
    .tdl
    {
        font-size: 18px !important;
    }
    .modal {
    }
    .vertical-alignment-helper {
        display:table;
        height: 100%;
        width: 100%;
    }
    .vertical-align-center {
        /* To center vertically */
        display: table-cell;
        vertical-align: middle;
    }
    .modal-content {
        /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
        width:inherit;
        height:inherit;
        /* To center horizontally */
        margin: 0 auto;
    }
    .font-white
    {
        color: #ffffff;
    }
</style>
<div class="container">

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <div class="modal-header">					
                        <h4 class="modal-title" id="myModalLabel">Remark</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <input type="hidden" id="editremark" value=""/>
                                <textarea id="textremark" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: center !important;">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary waves-effect waves-light" onclick="onSaveRemark()" id="btnOnSaveRemark">Save</button>
                    </div>
                </div>
            </div>
        </div>
	</div>

    <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal2Label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModal2Label">Konfirmasi</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12" style="font-size: 16px" id="modalmsg">
                            Apakah Anda Yakin ?
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <?php if ($rollcall->status == '0'): ?>
					    <button type="button" class="btn btn-primary waves-effect waves-light" onclick="onComplete()" id="btnOnComplete">Complete Cashier & Print Driver Report</button>
                    <?php elseif ($rollcall->status == '2'): ?>
                        <button type="button" class="btn btn-primary waves-effect waves-light" onclick="onUpdateCashierArrival()" id="btnOnComplete">Complete Cashier (Arrival)</button>
                    <?php endif;?>
				</div>
			</div>
		</div>
	</div>

    <div id="myModal3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal3Label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModal3Label">Confirmation</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12" style="font-size: 16px" id="modalmsg">
                            <?php
                                $konfirmMsg = ($rollcall->status == '1') ? 'Truck Jalan' : 'Finish';
                            ?>
                            Apakah anda yakin (<?php echo $konfirmMsg?>) ?
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <?php if ($rollcall->status == '1'): ?>
					<button type="button" class="btn btn-primary waves-effect waves-light" onclick="onCompleteRoute()" id="btnOnCompleteRoute">Complete CCR</button>
                    <?php elseif ($rollcall->status == '3'): ?>
                    <button type="button" class="btn btn-primary waves-effect waves-light" onclick="onFinishRoute()" id="btnOnCompleteRoute">Finish</button>
                    <?php endif; ?>
				</div>
			</div>
		</div>
	</div>
        
    <div class="row page-title-box" style="margin-bottom: 5px;background-color: #188ae2; color: white; padding: 5px 10px 5px 10px !important">
        <div class="col-sm-3">
            <label class="col-md-4 control-label">Date :</label>
            <div class="col-md-8 rcdata" id="departure_dt" style="border-bottom: 1px solid #dadada"><?php echo date('d/m/Y', strtotime($rollcall->departure_dt))?></div>
        </div>
        <div class="col-sm-9">
            <label class="col-md-2 control-label text-right">Customer :</label>
            <div class="col-md-2 rcdata" id="customer" style="border-bottom: 1px solid #dadada"><?php echo $rollcall->customer?></div>
            <label class="col-md-2 control-label text-right">Route :</label>
            <div class="col-md-2 rcdata" id="route" style="border-bottom: 1px solid #dadada"><?php echo $rollcall->route?></div>
            <label class="col-md-2 control-label text-right">Cycle :</label>
            <div class="col-md-2 rcdata" id="cycle" style="border-bottom: 1px solid #dadada"><?php echo $rollcall->cycle?></div>
        </div>
    </div>
    <div class="row m-t-10">
        <div class="col-sm-3">
            <div class="row" style="border-bottom: 2px solid #303030">
                <label class="col-md-12 control-label2" style="font-size: 24px; ">Roll Call (<?php echo $rollcall->lp_cd?>)</label>
                <input type="hidden" id="lp_cd" value="<?php echo $rollcall->lp_cd?>" />
            </div>
            <div class="row" >
                <div class="col-sm-12">
                    <div class="row">
                        <label class="col-md-6 m-t-10 control-label">Driver Name</label>
                        <div class="col-md-6 m-t-10 text-right">
                            <span class="tdl badge badge-primary" style="text-align: left; padding: 10px">1.</span> 
                        </div>
                        <div class="col-md-12 rcdata" id="driver_name" style="border-bottom: 1px solid #303030; padding-left: 30px; padding-bottom: 10px"><?php echo $rollcall->driver_name?></div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 10px">                
                        <label class="col-md-4 control-label">NIK</label>
                        <div class="col-md-8" id="">
                            <span id="driver_cd" class="rcdata" check=""><?php echo $rollcall->driver_cd?></span>
                            <?php $btn_nik = ($rollcall->driver_nik == '1') ? 'btn-success' : 'btn-danger';?>
                            <button class="tdl pull-right btn <?php echo $btn_nik?>  btn-toggle" id="btn_driver_cd" disabled="disabled"><?php echo ($rollcall->driver_nik == '1') ? 'OK': 'NG'?></button>
                        </div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 5px">                
                        <label class="col-md-4 control-label">Phone</label>
                        <div class="col-md-8 rcdata" id="smartphone"><?php echo $rollcall->smartphone?></div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 10px">                
                        <label class="col-md-4 control-label">SIM</label>
                        <div class="col-md-8">
                            <span id="driving_license_val" class="rcdata" check=""><?php echo date('d/m/Y', strtotime($rollcall->driving_license_val))?></span>
                            <?php $btn_sim = ($rollcall->driver_sim == '1') ? 'btn-success' : 'btn-danger';?>
                            <button class="tdl pull-right btn <?php echo $btn_sim?> btn-toggle" id="btn_driving_license_val" disabled="disabled"><?php echo ($rollcall->driver_sim == '1') ? 'OK': 'NG'?></button>
                        </div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 2px solid #303030; padding-bottom: 10px">                
                        <label class="col-md-4 control-label">SIO</label>
                        <div class="col-md-8">
                            <span id="forklift_license_val" class="rcdata" check=""><?php echo date('d/m/Y', strtotime($rollcall->forklift_license_val))?></span>
                            <?php $btn_sio = '';
                            if ($rollcall->driver_sio == '1') 
                            {
                                $bnt_sio = 'btn-success';
                            }elseif ($rollcall->driver_sio == '0')
                            {
                                $btn_sio = 'btn-danger';
                            }
                            ?>
                            <button class="tdl pull-right btn <?php echo $btn_sio?> btn-toggle" id="btn_forklift_license_val" disabled="disabled"><?php echo ($rollcall->driver_sio == '1') ? 'OK': 'NG'?></button>
                        </div>
                    </div>                        
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 5px">                
                        <label class="col-md-4 control-label">Truck</label>
                        <div class="col-md-8 rcdata" id="vehicle_cd"><?php echo $rollcall->vehicle_cd?></div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 5px">                
                        <label class="col-md-4 control-label">STNK</label>
                        <div class="col-md-8 rcdata" id="license_stnk"><?php echo date('d/m/Y', strtotime($rollcall->license_stnk))?></div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 5px">                
                        <label class="col-md-4 control-label">KIR</label>
                        <div class="col-md-8 rcdata" id="license_keur"><?php echo date('d/m/Y', strtotime($rollcall->license_keur))?></div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 5px">                
                        <label class="col-md-4 control-label">SIPA</label>
                        <div class="col-md-8 rcdata" id="license_sipa"><?php echo date('d/m/Y', strtotime($rollcall->license_sipa))?></div>
                    </div>
                </div>
            </div>            
        </div>
        
        <div class="col-sm-9">
            <?php 
                // 25 Mei 2020
                // Authorization based on Group Name ?
                $user_group_nm = $this->session->userdata(S_USER_GROUP_NM);                
                $role_nm = strtolower(str_replace(' ', '', $user_group_nm));                
            ?>
            <input type="hidden" id="role_nm" value="<?php echo $role_nm?>" />
            <input type="hidden" id="rollcall_id" value="<?php echo $rollcall->rollcall_id?>" />
            <input type="hidden" id="status" value="<?php echo $rollcall->status?>" />
            <table style="width: 100%" class="table table-bordered">
                <tr>
                    <td style="text-align: right">Next Process</td>
                    <td style="padding-bottom: 3px !important; border-bottom: 2px solid #303030 !important;">
                        <?php
                            $statustext['0'] = '<span class="badge badge-primary" style="width: 100%; text-align: left; padding: 5px">DEPARTURE CASHIER</span>';
                            $statustext['1'] = '<span class="badge badge-info" style="width: 100%; text-align: left; padding: 5px">DEPARTURE CCR</span>';
                            $statustext['2'] = '<span class="badge badge-danger" style="width: 100%; text-align: left; padding: 5px">ARRIVAL CASHIER</span>';
                            $statustext['3'] = '<span class="badge badge-success" style="width: 100%; text-align: left; padding: 5px">ARRIVAL CCR</span>';
                            $statustext['4'] = '<span class="badge badge-success" style="width: 100%; text-align: left; padding: 5px">FINISHED</span>';
                            echo $statustext[$rollcall->status];
                        ?>

                    </td>
                    <td class="tdl" style="padding-bottom: 3px !important; border-bottom: 2px solid #303030 !important; width: 30%; text-align: center; font-weight: bold">IN</td>
                    <td class="tdl" style="padding-bottom: 3px !important; border-left: 2px solid #303030; border-bottom: 2px solid #303030 !important; width: 30%; text-align: center; font-weight: bold">OUT</td>
                </tr>
                <tr>
                    <td rowspan="2" class="" style="border-bottom: 2px solid #303030 !important;vertical-align: middle !important; font-weight: bold; font-size: 18px">Time</td>
                    <td class="tdl">Plan</td>
                    <td class="text-center rcdata" id="departure_plan"><?php echo substr($rollcall->departure_plan, 0, 5)?></td>
                    <td style=" border-left: 2px solid #303030;" class="text-center rcdata" id="arrival_plan"><?php echo substr($rollcall->arrival_plan, 0, 5)?></td>
                </tr>
                <tr>                    
                    <td class="tdl" style="border-bottom: 2px solid #303030 !important;">Act</td>
                    <td style="border-bottom: 2px solid #303030 !important;" class="text-center rcdata" id="departure_actual"><?php echo substr($rollcall->departure_actual, 0, 5)?></td>
                    <td style=" border-left: 2px solid #303030;border-bottom: 2px solid #303030 !important;" class="text-center rcdata" id="arrival_actual"><?php echo ($rollcall->arrival_actual != '00:00:00') ? substr($rollcall->arrival_actual, 0, 5) : '-'?></td>
                </tr>
                <tr >
                    <td rowspan="3" class="" style="border-bottom: 2px solid #303030 !important; vertical-align: middle !important; font-weight: bold; font-size: 18px">
                        <span class="tdl badge badge-primary" style="text-align: left; padding: 10px">2.</span> Team Driver
                    </td>
                    <td class="tdl">Tekanan Darah</td>
                    <td>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <?php
                                        // 16-Juli-2020
                                        // validasi color
                                        $bgClr = ($rollcall->driver_blood_pres_sistole < 140) ? 'bg bg-success' : 'bg bg-danger';
                                        $bgClr2 = ($rollcall->driver_blood_pres_diastole >= 60 && $rollcall->driver_blood_pres_diastole <= 90) ? 'bg bg-success' : 'bg bg-danger';
                                    ?>

                                    <input type="text" value="<?php echo $rollcall->driver_blood_pres_sistole?>" id="driver_blood_pres_sistole" class="<?php echo $bgClr?> tdl form-control autonumber2 text-right" placeholder="< 140" disabled="disabled"/>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" value="<?php echo $rollcall->driver_blood_pres_diastole?>" id="driver_blood_pres_diastole" class="<?php echo $bgClr2?> tdl form-control autonumber2 text-right" placeholder="60 - 90"  disabled="disabled"/>
                                </div>
                                <div class="col-md-4 tdl">mmHg</div>
                            </div>
                        </div>
                    </td>
                    <td style=" border-left: 2px solid #303030;">                        
                    </td>
                </tr>
                <tr >
                    <td class="tdl">Temperatur</td>
                    <td>
                        <div class="row">
                            <div class="col-md-8">
                                <?php
                                    // 16-Juli-2020
                                    // validasi color
                                    $bgClr3 = ($rollcall->driver_body_temp < 37.5) ? 'bg bg-success' : 'bg bg-danger';
                                ?>
                                <input type="text" value="<?php echo $rollcall->driver_body_temp?>" id="driver_body_temp" class="<?php echo $bgClr3?> tdl form-control autonumber text-right" placeholder="35 - 37,5" disabled="disabled" />
                            </div>
                            <div class="col-md-4 tdl">&#8451;</div>
                        </div>
                    </td>

                    <td style=" border-left: 2px solid #303030;"></td>
                </tr>
                <tr >
                    <td class="tdl" style="border-bottom: 2px solid #303030 !important;">Waktu Tidur >= 5 Jam</td>
                    <td style="border-bottom: 2px solid #303030 !important;">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="driver_sleep_time" value="<?php echo $rollcall->driver_sleep_time?>"/>
                                <?php
                                    $btn_ok = ($rollcall->driver_sleep_time == '1') ? 'btn-success' : 'btn_dflt';
                                    $btn_ng = ($rollcall->driver_sleep_time == '0') ? 'btn-danger' : 'btn_dflt';
                                ?>
                                <button class="tdl btn <?php echo $btn_ok?> btn-driver_sleep_time" style="width: 100%" disabled="disabled">OK</button>
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="tdl btn <?php echo $btn_ng?> btn-driver_sleep_time" style="width: 100%" disabled="disabled">NG</button>
                            </div>
                        </div>
                    </td>
                    <td style=" border-left: 2px solid #303030;border-bottom: 2px solid #303030 !important;"></td>
                </tr>

                <!-- Admin Sales -->
                <tr>
                    <td rowspan="4" class="" style="border-bottom: 2px solid #303030 !important; vertical-align: middle !important; font-weight: bold; font-size: 18px">
                        <span class="tdl badge badge-info" style="text-align: left;  padding: 10px">3. </span> Kasir
                    </td>
                    <td class="tdl">Fuel
                    (<span id="cashier_etoll_spbu"><?php echo $rollcall->cashier_etoll_spbu?></span>)
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-8 text-right rcdata" id="cashier_fuel_plan">
                                <?php echo number_format($rollcall->cashier_fuel_plan, 0, ',', '.')?>
                            </div>
                            <div class="col-md-4">L</div>
                        </div>
                    </td>
                    <td style=" border-left: 2px solid #303030;">
                        <div class="row">
                            <div class="col-md-8">
                                <input type="text" value="<?php echo $rollcall->cashier_fuel_actual?>" id="cashier_fuel_actual" class="ctl-arrival tdl role role-administrator role-adminsales form-control autonumber2 text-right" placeholder="0" onfocus="this.select()" />
                            </div>
                            <div class="col-md-1">L</div>
                            <div class="col-md-3">
                                <input type="hidden" id="cashier_fuel_remark" value="<?php echo $rollcall->cashier_fuel_remark?>"/>
                                <button class="role role-administrator role-adminsales btn btn-dflt ctl-arrival " onclick="doRemark('cashier_fuel_remark')">R</button></div>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td class="tdl">E-Toll</td>
                    <td>
                        <div class="row">
                            <div class="col-md-8 text-right rcdata" id="cashier_etoll_plan">
                                <?php echo number_format($rollcall->cashier_etoll_plan, 0, ',', '.')?>
                            </div>
                            <div class="col-md-4">Rp.</div>
                        </div>
                    </td>
                    <td style=" border-left: 2px solid #303030;">
                        <div class="row">
                            <div class="tdl col-md-8 text-right" id="cashier_etoll_balance">
                                0
                            </div>
                            <div class="col-md-4">Rp.</div>
                            <!-- <div class="col-md-3"><button class="role role-administrator role-adminsales btn btn-dflt" onclick="doRemark('cashier_etoll_remark')">R</button></div> -->
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" value="<?php echo $rollcall->cashier_etoll_cardno?>" id="cashier_etoll_cardno" class="ctl-departure tdl role role-administrator role-adminsales form-control" placeholder="Scan No. Kartu E-Toll" onfocus="this.select()"/>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-8">                                
                                <input type="text" value="<?php echo $rollcall->cashier_etoll_amount_start?>" id="cashier_etoll_amount_start" class="ctl-departure tdl role role-administrator role-adminsales form-control autonumber2 text-right" placeholder="0" onkeyup="calculateBalance()" onfocus="this.select()" />
                            </div>
                            <div class="col-md-4">Rp.</div>
                        </div>
                    </td>
                    <td style=" border-left: 2px solid #303030;">
                        <div class="row">
                            <div class="col-md-8">
                                <input type="text" value="<?php echo $rollcall->cashier_etoll_amount_end?>" id="cashier_etoll_amount_end" class="ctl-arrival tdl role role-administrator role-adminsales form-control autonumber2 text-right" placeholder="0" onkeyup="calculateBalance()" onfocus="this.select()" />
                            </div>
                            <div class="col-md-1">Rp.</div>
                            <div class="col-md-3">
                                <input type="hidden" id="cashier_etoll_remark" value="<?php echo $rollcall->cashier_etoll_remark?>"/>
                                <button class="role role-administrator role-adminsales btn btn-dflt ctl-arrival " onclick="doRemark('cashier_etoll_remark')">R</button></div>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td class="tdl" style="border-bottom: 2px solid #303030 !important;">Others</td>
                    <td style="border-bottom: 2px solid #303030 !important;">
                        <div class="row">
                            <div class="col-md-8 text-right rcdata" id="cashier_others_plan">
                                <?php echo number_format($rollcall->cashier_others_plan, 0, ',', '.')?>
                            </div>
                            <div class="col-md-4">Rp.</div>
                        </div>
                    </td>
                    <td style=" border-left: 2px solid #303030; border-bottom: 2px solid #303030 !important;">
                        <div class="row">
                            <div class="col-md-8 text-right">
                                <input type="text" value="<?php echo $rollcall->cashier_others_actual?>" id="cashier_others_actual" class="ctl-arrival tdl role role-administrator role-adminsales form-control autonumber2 text-right" placeholder="0" onfocus="this.select()"/>
                            </div>
                            <div class="col-md-1">Rp.</div>
                            <div class="col-md-3">
                                <input type="hidden" id="cashier_others_remark" value="<?php echo $rollcall->cashier_others_remark?>"/>
                                <button class="role role-administrator role-adminsales btn btn-dflt ctl-arrival " onclick="doRemark('cashier_others_remark')">R</button>
                            </div>
                        </div>                    
                    </td>
                </tr>

                <!-- Controller -->
                <tr>
                    <td rowspan="3" class="" style="vertical-align: middle !important; font-weight: bold; font-size: 18px">
                        <span class="tdl badge badge-danger" style="text-align: left;  padding: 10px">4.</span>  CCR
                    </td>
                    <td class="tdl">APD / Badge</td>
                    <td>
                        <div class="row">
                            <?php
                                $btn_ok = ($rollcall->controller_apd_departure == '1') ? 'btn-success' : 'btn_dflt';
                                $btn_ng = ($rollcall->controller_apd_departure == '0') ? 'btn-danger' : 'btn_dflt';
                            ?>
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_apd_departure" value="<?php echo $rollcall->controller_apd_departure?>"/>
                                <button class="ctl-departure tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ok?> btn-controller_apd_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_apd_departure')">OK</button>
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="ctl-departure tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ng?> btn-controller_apd_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_apd_departure')">NG</button>
                            </div>
                        </div>
                    </td>
                    <td style=" border-left: 2px solid #303030;">
                        <div class="row">
                            <?php
                                $btn_ok = ($rollcall->controller_apd_arrival == '1') ? 'btn-success' : 'btn_dflt';
                                $btn_ng = ($rollcall->controller_apd_arrival == '0') ? 'btn-danger' : 'btn_dflt';
                            ?>
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_apd_arrival" value="<?php echo $rollcall->controller_apd_arrival?>"/>
                                <button class=" ctl-arrival tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ok?> btn-controller_apd_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_apd_arrival')">OK</button>
                            </div>
                            <div class="col-md-6 text-center">
                                <button class=" ctl-arrival tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ng?> btn-controller_apd_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_apd_arrival')">NG</button>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">Basket</td>
                    <td>
                        <?php
                            $btn_ok = ($rollcall->controller_basket_departure == '1') ? 'btn-success' : 'btn_dflt';
                            $btn_ng = ($rollcall->controller_basket_departure == '0') ? 'btn-danger' : 'btn_dflt';
                        ?>
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_basket_departure" value="<?php echo $rollcall->controller_basket_departure?>"/>
                                <button class="ctl-departure tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ok?> btn-controller_basket_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_basket_departure')">OK</button>
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="ctl-departure tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ng?> btn-controller_basket_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_basket_departure')">NG</button>
                            </div>
                        </div>
                    </td>
                    <td style=" border-left: 2px solid #303030;">
                        <?php
                            $btn_ok = ($rollcall->controller_basket_arrival == '1') ? 'btn-success' : 'btn_dflt';
                            $btn_ng = ($rollcall->controller_basket_arrival == '0') ? 'btn-danger' : 'btn_dflt';
                        ?>
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_basket_arrival" value="<?php echo $rollcall->controller_basket_arrival?>"/>
                                <button class=" ctl-arrival tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ok?> btn-controller_basket_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_basket_arrival')">OK</button>
                            </div>
                            <div class="col-md-6 text-center">
                                <button class=" ctl-arrival tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ng?> btn-controller_basket_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_basket_arrival')">NG</button>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">Kunci</td>
                    <td>
                        <?php
                            $btn_ok = ($rollcall->controller_key_departure == '1') ? 'btn-success' : 'btn_dflt';
                            $btn_ng = ($rollcall->controller_key_departure == '0') ? 'btn-danger' : 'btn_dflt';
                        ?>
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_key_departure" value="<?php echo $rollcall->controller_key_departure?>"/>
                                <button class="ctl-departure tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ok?> btn-controller_key_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_key_departure')">OK</button>                                
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="ctl-departure tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ng?> btn-controller_key_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_key_departure')">NG</button>                                
                            </div>
                        </div>
                    </td>
                    <td style=" border-left: 2px solid #303030;">
                        <?php
                            $btn_ok = ($rollcall->controller_key_arrival == '1') ? 'btn-success' : 'btn_dflt';
                            $btn_ng = ($rollcall->controller_key_arrival == '0') ? 'btn-danger' : 'btn_dflt';
                        ?>
                        <input type="hidden" id="controller_key_arrival" value="1"/>
                        <!-- <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_key_arrival" value="<?php echo $rollcall->controller_key_arrival?>"/>
                                <button class=" ctl-arrival tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ok?> btn-controller_key_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_key_arrival')">OK</button>
                            </div>
                            <div class="col-md-6 text-center">
                                <button class=" ctl-arrival tdl role role-administrator role-controller role-controllerkarawang btn <?php echo $btn_ng?> btn-controller_key_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_key_arrival')">NG</button>
                            </div>
                        </div> -->
                    </td>
                </tr>
                <tr>
                    <td>
                        <button type="button" name="btn_back" id="btn_back" class="form-control btn btn-bordered btn-custom" onclick="window.location='<?php echo site_url('rollcall')?>'">&lt; Back</button>
                    </td>
                    <td></td>
                    <td>
                        <?php if (substr($role_nm,0,10) == 'adminsales' || $role_nm == 'administrator'): ?>
                            <?php if ($rollcall->status == '0'): ?>
                            <button type="button" name="btn_complete" id="btn_complete" class="form-control btn btn-bordered btn-primary" onclick="doComplete()">Complete Cashier (Departure)</button>                        
                            <?php endif; ?>

                            <?php if ($rollcall->status > 0): ?>
                            <button type="button" name="btn_complete" id="btn_complete" class="form-control btn btn-bordered btn-primary" onclick="window.open('<?php echo site_url('rollcall/print/' . $rollcall->rollcall_id)?>')">Re-Print Driver Report</button>
                            <?php endif;?>
                        <?php endif;?>
                        <!-- <button name="" id="" class="form-control btn btn-bordered btn-purple">Driver Report</button> -->
                    </td>
                    <td>
                    <?php if (substr($role_nm,0,10) == 'controller' || $role_nm == 'administrator'): ?>
                        <?php if ($rollcall->status == '1'): ?>
                            <button name="" id="" class="dsbl form-control btn btn-bordered btn-purple" onclick="doCompleteRoute()">Complete CCR</button>
                        <?php elseif ($rollcall->status == '3'): ?>
                            <button name="" id="" class="dsbl form-control btn btn-bordered btn-purple" onclick="doCompleteRouteArrival()">Finish</button>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if (substr($role_nm,0,10) == 'adminsales' || $role_nm == 'administrator'): ?>                        
                        <?php if ($rollcall->status == '2'): ?>
                        <button type="button" name="btn_complete" id="btn_complete" class="form-control btn btn-bordered btn-primary" onclick="doComplete()">Complete Cashier (Arrival)</button>
                        <?php endif; ?>
                    <?php endif;?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>