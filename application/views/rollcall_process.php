<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Transaction</li>
					<li>
						<a href="<?php echo site_url('rollcall/id/' . $timetable_dt)?>">Rollcall</a>
					</li>
                    <li class="active">Process</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Print Driver Report</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 text-danger" id="printmsg">
							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
					<!-- <button type="button" class="btn btn-purple waves-effect waves-light" onclick="onPrintForced()" id="btnOnPrintForced">Print Again</button> -->
				</div>
			</div>
		</div>
	</div>
    
    <?php if ($this->session->flashdata('notif_success') != ''): ?>
    <div class="row">
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
			</div>
		</div>
    </div>
    <?php endif; ?>
    
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group">
                <label class="col-md-12 control-label">Date</label>
                <div class="col-md-12" id="timetable_dt"><?php echo date('d/m/Y', strtotime($timetable_dt))?></div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label class="col-md-12 control-label">Customer</label>
                <div class="col-md-12" id="customer"><?php echo $timetable->customer?></div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label class="col-md-12 control-label">Route</label>
                <div class="col-md-12" id="route"><?php echo $route?></div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label class="col-md-12 control-label">Cycle</label>
                <div class="col-md-12" id="cycle"><?php echo $cycle?></div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label class="col-md-12 control-label">Departure Plan</label>
                <div class="col-md-12" id="cycle"><?php echo substr($timetable->departure_plan, 0, 5);?></div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                <label class="col-md-12 control-label">Clock In | Out</label>
                <div class="col-md-12" id="cycle"><?php 
                
                $clock_in = ($timetable->clock_in != '') ?  date('H:i:s', strtotime($timetable->clock_in)) : '-';
                $clock_out = ($timetable->clock_out != '') ?  date('H:i:s', strtotime($timetable->clock_out)) : '-';
                
                echo '' . $clock_in . ' | ' .  $clock_out . '';?>

                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-10 p-t-10" style="border-top: 1px dotted #dadada">
        <form id="myForm">
        <div class="col-sm-5">
            <div class="row">
                <div class="col-sm-5 text-center">
                    <?php
                    $filep = COMPANY_ASSETS_PATH. 'cid' . $this->session->userdata(S_COMPANY_ID) . '/profile/thumbs/' . $timetable->photo;
                    $photo = 'assets/images/user_photo.png';
                    if ($timetable->photo != '') {
                        if (file_exists($filep)) {
                            $photo = $filep;
                        }
                    }                                        
                    ?>
                    <img src="<?php echo base_url() . $photo?>" class="img-thumbnail" alt="Driver Photo"/>
                </div>
                <div class="col-sm-7">
                    <div class="form-group">
                        <label class="col-md-12 control-label">Name</label>
                        <div class="col-md-12 m-b-10" style="border-bottom: 1px dotted #dadada"><?php echo $timetable->driver_name?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12 control-label">NIK</label>
                        <div class="col-md-12 m-b-10" style="border-bottom: 1px dotted #dadada" id="driver_cd"><?php echo $timetable->driver_cd?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12 control-label">Phone</label>
                        <div class="col-md-12 m-b-10" style="border-bottom: 1px dotted #dadada"><?php echo ($timetable->smartphone != '') ? $timetable->smartphone : '-'; ?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12 control-label">SIM</label>
                        <?php
                                                        
                            // sim sio stnk itu 1 bulan sebelum habis warna kuning, kalo lewat merah

                            $bg_sim = '';
                            $bg_sio = '';
                            $bg_stnk = '';
                            $bg_kir = '';
                            $bg_sipa = '';

                            if ($timetable->driving_license_val != '')
                            {
                                // cari selisihnya brp lama
                                $now = time(); // or your date as well
                                $your_date = strtotime($timetable->driving_license_val);
                                $datediff = $your_date - $now;

                                $days_sim = round($datediff / (60 * 60 * 24));
                                
                                if ($days_sim >= 1 && $days_sim  < 30)
                                {
                                    $bg_sim='bg-warning';
                                }
                                if ($days_sim <= 0)
                                {
                                    $bg_sim='bg-danger';
                                }
                            }

                            if ($timetable->forklift_license_val != '')
                            {
                                // cari selisihnya brp lama
                                $now = time(); // or your date as well
                                $your_date = strtotime($timetable->forklift_license_val);
                                $datediff = $your_date - $now;

                                $days_sio = round($datediff / (60 * 60 * 24));
                                
                                if ($days_sio >= 1 && $days_sio  < 30)
                                {
                                    $bg_sio='bg-warning';
                                }
                                if ($days_sio <= 0)
                                {
                                    $bg_sio='bg-danger';
                                }
                            }

                            if ($timetable->license_stnk != '')
                            {
                                // cari selisihnya brp lama
                                $now = time(); // or your date as well
                                $your_date = strtotime($timetable->license_stnk);
                                $datediff = $your_date - $now;

                                $days_stnk = round($datediff / (60 * 60 * 24));
                                
                                if ($days_stnk >= 1 && $days_stnk  < 30)
                                {
                                    $bg_stnk='bg-warning';
                                }
                                if ($days_stnk <= 0)
                                {
                                    $bg_stnk='bg-danger';
                                }
                            }

                            if ($timetable->license_keur != '')
                            {
                                // cari selisihnya brp lama
                                $now = time(); // or your date as well
                                $your_date = strtotime($timetable->license_keur);
                                $datediff = $your_date - $now;

                                $days_kir = round($datediff / (60 * 60 * 24));
                                
                                if ($days_kir >= 1 && $days_kir  < 7)
                                {
                                    $bg_kir='bg-warning';
                                }
                                if ($days_kir <= 0)
                                {
                                    $bg_kir='bg-danger';
                                }
                            }

                            if ($timetable->license_sipa != '')
                            {
                                // cari selisihnya brp lama
                                $now = time(); // or your date as well
                                $your_date = strtotime($timetable->license_sipa);
                                $datediff = $your_date - $now;

                                $days_sipa = round($datediff / (60 * 60 * 24));
                                
                                if ($days_sipa >= 1 && $days_sipa  < 7)
                                {
                                    $bg_sipa='bg-warning';
                                }
                                if ($days_sipa <= 0)
                                {
                                    $bg_sipa='bg-danger';
                                }
                            }
                            
                        ?>

                        <div class="col-md-12 m-b-10 <?php echo $bg_sim?>" style="border-bottom: 1px dotted #dadada"><?php echo ($timetable->driving_license_val != '') ? date('d/m/Y', strtotime($timetable->driving_license_val)) : '-'; ?></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12 control-label">SIO</label>
                        <div class="col-md-12 <?php echo $bg_sio?>"><?php echo ($timetable->forklift_license_val != '') ? date('d/m/Y', strtotime($timetable->forklift_license_val)) : '-'; ?></div>
                    </div>
                </div>
            </div>
            <div class="row m-t-10 p-t-10" style="border-top: 1px dotted #dadada">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="col-md-4 control-label" style="border-bottom: 1px dotted #dadada">Truck</label>
                        <div class="col-md-8" style="border-bottom: 1px dotted #dadada" id="vehicle_cd"><?php echo $timetable->vehicle_cd ?></div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="col-md-4 control-label" style="border-bottom: 1px dotted #dadada">STNK</label>
                        <div class="col-md-8 <?php echo $bg_stnk?>" style="border-bottom: 1px dotted #dadada"><?php echo ($timetable->license_stnk != '') ? date('d/m/Y', strtotime($timetable->license_stnk)) : '-'; ?></div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="col-md-4 control-label" style="border-bottom: 1px dotted #dadada">KIR</label>
                        <div class="col-md-8 <?php echo $bg_kir?>" style="border-bottom: 1px dotted #dadada"><?php echo ($timetable->license_keur != '') ? date('d/m/Y', strtotime($timetable->license_keur)) : '-'; ?></div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="col-md-4 control-label" style="border-bottom: 1px dotted #dadada">SIPA</label>
                        <div class="col-md-8 <?php echo $bg_sipa?>" style="border-bottom: 1px dotted #dadada"><?php echo ($timetable->license_sipa != '') ? date('d/m/Y', strtotime($timetable->license_sipa)) : '-'; ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-7">
            <div class="row">
                <?php 
                    // 25 Mei 2020
                    // Authorization based on Group Name ?
                    $user_group_nm = $this->session->userdata(S_USER_GROUP_NM);
                    
                    $role_nm = strtolower(str_replace(' ', '', $user_group_nm));                
                ?>
                <input type="hidden" id="role_nm" value="<?php echo $role_nm?>" />
                <!-- <div class="col-sm-12 m-t-10 role role-administrator role-teamdriver">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-primary">1 &mdash; Team Driver</span></label>
                    </div>
                </div> -->
                <div class="col-sm-12 m-t-10 role role-administrator role-teamdriver">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-primary">1.1</span> Blood Pressure <span class="text-danger">*</span></label>
                        <div class="col-md-2">
                            <input type="text" value="<?php echo $rc_blood_pres_sistole?>" id="rc_blood_pres_sistole" class="form-control autonumber2 text-right" placeholder="< 140" onblur="checkPrint()" required />
                        </div>
                        <div class="col-md-2">
                            <input type="text" value="<?php echo $rc_blood_pres_diastole?>" id="rc_blood_pres_diastole" class="form-control autonumber2 text-right" placeholder="60 - 90" onblur="checkPrint()" required />
                        </div>
                        <div class="col-md-4">mmHg</div>
                    </div>
                </div>

                <div class="col-sm-12 m-t-10 role role-administrator role-teamdriver">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-primary">1.2</span> Temperature <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input type="text" value="<?php echo $rc_body_temp?>" id="rc_body_temp" class="form-control autonumber text-right" placeholder="35 - 37,5" onblur="checkPrint()" required />
                        </div>
                        <div class="col-md-4">&#8451;</div>
                    </div>
                </div>

                <div class="col-sm-12 m-t-10 role role-administrator role-teamdriver">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-primary">1.3</span> Waktu Tidur</label>
                        <div class="col-md-8">
                        <input onclick="checkPrint()" type="radio" value="1" id="rc_sleep_time1" name="rc_sleep_time" <?php if ($rc_sleep_time == '1'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_sleep_time1">&nbsp;&nbsp;>= 5 Jam</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="radio" value="0" id="rc_sleep_time2" name="rc_sleep_time" <?php if ($rc_sleep_time == '0'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_sleep_time2">&nbsp;&nbsp;< 4 Jam</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 m-t-10 role role-administrator role-teamdriver" style="border-bottom: 1px solid #dadada">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-primary">1.4</span> APD</label>
                        <div class="col-md-8">
                            <input onclick="checkPrint()" type="checkbox" id="rc_apd_vest" name="rc_apd_vest" value="1" <?php if ($rc_apd_vest == 'true'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_apd_vest">&nbsp;&nbsp;Vest</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="checkbox" id="rc_apd_helm" name="rc_apd_helm" value="1" <?php if ($rc_apd_helm == 'true'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_apd_helm">&nbsp;&nbsp;Helm</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="checkbox" id="rc_apd_sepatu" name="rc_apd_sepatu" value="1" <?php if ($rc_apd_sepatu == 'true'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_apd_sepatu">&nbsp;&nbsp;Sepatu</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="checkbox" id="rc_apd_uniform" name="rc_apd_uniform" value="1" <?php if ($rc_apd_uniform == 'true'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_apd_uniform">&nbsp;&nbsp;Uniform</label>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-danger">2 &mdash; Admin Sales</span> </label>                        
                        <div class="col-md-4">                            
                        </div>
                    </div>
                </div> -->
                <!-- <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-danger">2.1</span> Fuel <span class="text-danger">*</span> (120L)</label>
                        <div class="col-md-4">
                            <input type="text" value="<?php echo $etoll_id?>" id="etoll_id" class="form-control" placeholder="Scan E-Toll Card" onblur="checkPrint()" required />
                            120 Liter
                        </div>
                        <div class="col-md-4">
                            <input type="text" value="" id="fuel_actual" class="form-control" placeholder="Liter" />
                        </div>
                    </div>
                </div> -->
                <!-- <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-danger">2.1</span> E-Toll <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input type="text" value="<?php echo $etoll_id?>" id="etoll_id" class="form-control" placeholder="Scan E-Toll Card" onblur="checkPrint()" required />
                            Rp. 45.000
                        </div>
                        <div class="col-md-4">
                            <input type="text" value="" id="etoll_actual" class="form-control" placeholder="Rp." />
                        </div> 
                    </div>
                </div> -->
                <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-danger">2.1</span> Fuel <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <strong><mark>50L (120 KM) &mdash; Kawasan KIIC</mark></strong>
                        </div>
                        <div class="col-md-4">
                            <!-- <input type="text" value="<?php echo $etoll_id?>" id="etoll_id" class="form-control" placeholder="Scan E-Toll Card" onblur="checkPrint()" required /> -->
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label text-right">Departure</label>
                        <div class="col-md-4">                            
                            <input type="text" value="50" id="liter_actual" class="form-control" placeholder="Liter" />
                        </div>
                        <div class="col-md-4">                            
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label text-right">Actual</label>
                        <div class="col-md-4">                            
                            <input type="text" value="" id="liter_actual" class="form-control" placeholder="Liter" />
                        </div>
                        <div class="col-md-4">                            
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-danger">2.2</span> E-Toll <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <strong><mark>Rp. 36.000</mark></strong>
                        </div>
                        <div class="col-md-4">
                            <!-- <input type="text" value="<?php echo $etoll_id?>" id="etoll_id" class="form-control" placeholder="Scan E-Toll Card" onblur="checkPrint()" required /> -->
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label text-right">Etoll Card No</label>
                        <div class="col-md-4">                            
                            <input type="text" value="<?php echo $etoll_id?>" id="etoll_id" class="form-control" placeholder="Scan E-Toll Card" onblur="checkPrint()" required />
                        </div>
                        <div class="col-md-4">                            
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label text-right">Saldo Awal</label>
                        <div class="col-md-4">                            
                            <input type="text" value="<?php echo $etoll_balance_departure?>" id="etoll_balance_departure" class="form-control autonumber2 text-right" placeholder="Saldo Awal" onblur="checkPrint()" required />
                        </div>
                        <div class="col-md-4">                            
                        </div> 
                    </div>
                </div>
                
                <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label text-right">Saldo Akhir</label>
                        <div class="col-md-4">                            
                            <input type="text" value="<?php echo $etoll_balance_arrival?>" id="etoll_balance_arrival" class="form-control autonumber2 text-right" placeholder="Saldo Akhir" />
                        </div>
                        <div class="col-md-4">
                            
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-danger">2.3</span> Others <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <strong><mark>Rp. 5.000</mark></strong>
                        </div>
                        <div class="col-md-4">
                            <!-- <input type="text" value="<?php echo $etoll_id?>" id="etoll_id" class="form-control" placeholder="Scan E-Toll Card" onblur="checkPrint()" required /> -->
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label text-right">Departure</label>
                        <div class="col-md-4">                            
                            <input type="text" value="5000" id="others_actual" class="form-control" placeholder="Rp" />
                        </div>
                        <div class="col-md-4">                            
                        </div> 
                    </div>
                </div>
                <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label text-right">Actual</label>
                        <div class="col-md-4">                            
                            <input type="text" value="" id="others_actual" class="form-control" placeholder="Rp" />
                        </div>
                        <div class="col-md-4">                            
                        </div> 
                    </div>
                </div>
                
                <!-- <div class="col-sm-12 m-t-10 role role-administrator role-adminsales">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-danger">2.2</span> Saldo Awal <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            
                        </div>
                        <div class="col-md-4">                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 m-t-10 role role-administrator role-adminsales m-b-10">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-danger">2.3</span> Saldo Akhir</label>
                        <div class="col-md-4">
                            
                        </div>
                        <div class="col-md-4">                            
                        </div>
                    </div>
                </div> -->
                <div class="col-sm-12 m-t-10 p-t-10 role role-administrator role-controller role-controllerkarawang" style="border-top: 1px solid #dadada">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-info">3.1 &mdash; Departure</span> </label>
                        <div class="col-md-4">
                            <strong>Departure</strong>
                        </div>
                        <div class="col-md-4">
                            <strong>Arrival</strong>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 role role-administrator role-controller role-controllerkarawang">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label">&nbsp;</label>
                        <div class="col-md-4">
                            <input onclick="checkPrint()" type="checkbox" id="rc_badge" name="rc_badge" value="1" <?php if ($rc_badge == '1'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_badge">&nbsp;&nbsp;Badge</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="checkbox" id="rc_basket" name="rc_basket" value="1" <?php if ($rc_basket == '1'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_basket">&nbsp;&nbsp;Basket</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="checkbox" id="rc_key" name="rc_key" value="1" <?php if ($rc_key == '1'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_key">&nbsp;&nbsp;Kunci</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <!-- <input onclick="checkPrint()" type="radio" value="1" id="rc_badge1" name="rc_badge" <?php if ($rc_badge == '1'): ?>checked="checked"<?php endif; ?>  /><label style="font-weight: normal" for="rc_badge1">&nbsp;&nbsp;Ada</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="radio" value="0" id="rc_badge2" name="rc_badge" <?php if ($rc_badge == '0'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_badge2">&nbsp;&nbsp;Tidak Ada</label> -->
                        </div>
                        <div class="col-md-4">
                            <input onclick="checkPrint()" type="checkbox" id="rc_badge_a" name="rc_badge_a" value="1" <?php if ($rc_badge == '1'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_badge_a">&nbsp;&nbsp;Badge</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="checkbox" id="rc_basket_a" name="rc_basket_a" value="1" <?php if ($rc_basket == '1'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_basket_a">&nbsp;&nbsp;Basket</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="checkbox" id="rc_key_a" name="rc_key_a" value="1" <?php if ($rc_key == '1'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_key_a">&nbsp;&nbsp;Kunci</label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <!-- <input onclick="checkPrint()" type="radio" value="1" id="rc_badge1" name="rc_badge" <?php if ($rc_badge == '1'): ?>checked="checked"<?php endif; ?>  /><label style="font-weight: normal" for="rc_badge1">&nbsp;&nbsp;Ada</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="radio" value="0" id="rc_badge2" name="rc_badge" <?php if ($rc_badge == '0'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_badge2">&nbsp;&nbsp;Tidak Ada</label> -->
                        </div>
                    </div>
                </div>
                <!-- <div class="col-sm-12 m-t-10 role role-administrator role-controller role-controllerkarawang">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-info">6</span> Basket</label>
                        <div class="col-md-8">
                            <input onclick="checkPrint()" type="radio" value="1" id="rc_basket1" name="rc_basket" <?php if ($rc_basket == '1'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_basket1">&nbsp;&nbsp;Ada</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="radio" value="0" id="rc_basket2" name="rc_basket" <?php if ($rc_basket == '0'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_basket2">&nbsp;&nbsp;Tidak Ada</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 m-t-10 role role-administrator role-controller role-controllerkarawang">
                    <div class="form-group">
                        <label class="col-md-4 m-t-5 control-label"><span class="badge badge-info">7</span> Kunci</label>
                        <div class="col-md-8">
                            <input onclick="checkPrint()" type="radio" value="1" id="rc_key1" name="rc_key" <?php if ($rc_key == '1'): ?>checked="checked"<?php endif; ?>  /><label style="font-weight: normal" for="rc_key1">&nbsp;&nbsp;Ada</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input onclick="checkPrint()" type="radio" value="0" id="rc_key2" name="rc_key" <?php if ($rc_key == '0'): ?>checked="checked"<?php endif; ?> /><label style="font-weight: normal" for="rc_key2">&nbsp;&nbsp;Tidak Ada</label>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        </form>
    </div>
    <div class="row m-t-10 p-t-10" style="border-top: 1px dotted #dadada">
        <div class="col-sm-6">
            <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" id="btnBack" onclick="window.location='<?php echo site_url('rollcall/id/' . $timetable_dt)?>'">Back</button>
        </div>
        <div class="col-sm-6 text-right">
            <button type="button" class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" id="btnSave" onclick="doSave()">Save</button>
            <button type="button" class="btn btn-purple btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" id="btnPrint" onclick="doPrint()"><i class="mdi mdi-printer"></i> Driver Report</button>
        </div>
    </div>
</div>