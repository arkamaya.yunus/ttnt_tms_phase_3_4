<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo APP_NAME ?> - Software untuk mengelola bisnis gratis">
        <meta name="author" content="irfan.satriadarma@gmail.com">
		
		<link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon2.png">
        
		<!--

   _____                                 _       
  / ____|                               | |      
 | |     ___  _ __ _ __   ___  _ __ __ _| |_ ___ 
 | |    / _ \| '__| '_ \ / _ \| '__/ _` | __/ _ \
 | |___| (_) | |  | |_) | (_) | | | (_| | ||  __/
  \_____\___/|_|  | .__/ \___/|_|  \__,_|\__\___|
                  | |                            
                  |_|                            

			You're reading. We're hiring.		
			Don't see something there for you? Email us hrd@arkamaya.co.id
		-->
		
		<title><?php 
				if (isset($stitle))
				{
					if ($stitle != '')
					{
						echo $stitle . ' - ';
					}
				}
				echo $this->session->userdata(S_COMPANY_NAME) . ' - ' . APP_NAME;
			?></title>
			
		<!-- Plugin css -->
        <link href="<?php echo base_url() ?>assets/js/plugins/select2/select2/select2.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/keyTable.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/morris.js-0.5.1/morris.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css" />
                
        <!-- Core App css -->
        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/pace.min.css" rel="stylesheet" type="text/css" />
				        
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url() ?>assets/js/modernizr.min.js"></script>
        <script type="text/javascript">
            var SITE_URL = '<?php echo site_url() ?>';
        </script>
    </head>


    <body class="fixed-left">
        <div id="wrapper">
            <div class="topbar">
                <div class="topbar-left">
                    <?php  $logo = base_url()."assets/images/logo-text-white.png"; ?>                    
					<a href="<?php echo site_url('') ?>" class="logo" title="Kembali ke menu utama">
                        <span> <img src="<?php echo $logo;?>" alt="" style="height: 30px; max-height: 100px; max-width: 230px"></span>
                        <i> <img src="<?php echo base_url()."assets/images/logo_app_3.png"?>" alt="" style="height: auto; max-width: 50px"></i>
                    </a>
					
                </div>
                
                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Navbar-left -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left waves-effect">
                                    <i class="mdi mdi-menu"></i>
                                </button>
                            </li>
                            <li class="hidden-xs">
                                <a href="javascript:;" class="menu-item" style="font-weight: bolder; font-size: 18px; cursor: auto">
									<?php echo ($this->uri->segment(1) != 'company') ? $this->session->userdata(S_COMPANY_NAME) : 'Companies'?>
								</a>								
                            </li>
                        </ul>

                        <!-- Right(Notification) -->
                        <ul class="nav navbar-nav navbar-right">
                            <?php if ($this->uri->segment(1) != 'company'): ?>
							<!-- <li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search or type command..." class="form-control" id="f_search_quick_menu" />
                                    <a href="#"><i class="fa fa-search"></i></a>                                    
                                </form>
                                
                            </li> -->
                            <?php endif; ?>
                            <li class="hidden-xs m-t-20">
                                <span class="badge badge-danger"><?php echo $this->session->userdata(S_USER_GROUP_NM)?></span>
                            </li>
                            <li class="hidden-xs">
								<a class="" href="javascript:;">
                                    <span style="font-weight: bold">
                                    <?php echo $this->session->userdata(S_EMPLOYEE_NAME)?></span><br/>
                                    <sup>
                                        <?php echo $this->session->userdata(S_USER_EMAIL)?>                                                                                
                                    </sup>
                                </a>
                            </li>

                            <li class="dropdown user-box">
                                <a href="javascript:;" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                                    <?php
                                    $photo = 'assets/images/user_photo.png';
                                    if ($this->session->userdata(S_EMPLOYEE_PHOTO) != '') 
									{
                                        $filep = S_COMPANY_PATH . 'cid' . $this->session->userdata(S_COMPANY_ID) . '/profile/thumbs/' . $this->session->userdata(S_EMPLOYEE_PHOTO);

                                        if (file_exists($filep)) 
										{
                                            $photo = $filep;
                                        }
                                    }
                                    ?>

                                    <img src="<?php echo base_url() ?><?php echo $photo ?>" alt="profile-image" class="img-circle user-img" />
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
									<?php if ($this->session->userdata(S_USER_IS_ADMIN) == '1'): ?>
                                    <li><a href="<?php echo site_url('settings')?>" ><i class="ti-settings m-r-5"></i> Settings</a></li>
                                    <!-- <li><a href="<?php echo site_url('company')?>" ><i class="ti-crown m-r-5"></i> Company</a></li> -->
									<?php endif; ?>
                                    <li><a href="<?php echo site_url('login/out') ?>"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                                </ul>
                            </li>

                        </ul> <!-- end navbar-right -->

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                            
                        	


							<?php if ($this->uri->segment(1) != 'company' && $this->uri->segment(1) != 'changxe_pwd'): ?>
							<li class="menu-title">Navigation</li>
                            
                            <!--li class="menu-title">
								<?php echo ($this->app_icon != '') ? '<i class="' . $this->app_icon . '"></i> ' : ''; ?>
								<?php echo $this->app_name?>
							</li-->
							<?php create_menu($this->menu); ?> 
							<?php endif; ?>

                            <li class="menu-title">Others</li>
                            <?php if ($this->uri->segment(1) != 'company' && $this->session->userdata(S_USER_IS_ADMIN) == '1'): ?>
                            <li>
                                <a href="<?php echo site_url('settings')?>" class="waves-effect"><i class="mdi mdi-settings"></i><span> Settings </span></a>
                            </li>
                            <?php endif; ?>
                            <li>
                                <a href="javascript:;" class="waves-effect" onclick="changePwd()"><i class=" mdi mdi-pencil-box"></i><span> Change Password </span></a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('login/out')?>" class="waves-effect"><i class="mdi mdi-logout"></i><span> Logout </span></a>
                            </li>
                        </ul>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>                    
                </div>
                <!-- Sidebar -left -->
            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
