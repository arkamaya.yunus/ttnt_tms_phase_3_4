<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo APP_NAME ?> - Software untuk mengelola bisnis gratis">
        <meta name="author" content="irfan.satriadarma@gmail.com">
		
		<link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon2.png">
        
		<!--

   _____                                 _       
  / ____|                               | |      
 | |     ___  _ __ _ __   ___  _ __ __ _| |_ ___ 
 | |    / _ \| '__| '_ \ / _ \| '__/ _` | __/ _ \
 | |___| (_) | |  | |_) | (_) | | | (_| | ||  __/
  \_____\___/|_|  | .__/ \___/|_|  \__,_|\__\___|
                  | |                            
                  |_|                            

			You're reading. We're hiring.		
			Don't see something there for you? Email us hrd@arkamaya.co.id
		-->
		
		<title><?php 
				if (isset($stitle))
				{
					if ($stitle != '')
					{
						echo $stitle . ' - ';
					}
				}
				echo $this->session->userdata(S_COMPANY_NAME) . ' - ' . APP_NAME;
			?></title>
			
		<!-- Plugin css -->
        <link href="<?php echo base_url() ?>assets/js/plugins/select2/select2/select2.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/datatables/keyTable.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/morris.js-0.5.1/morris.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/js/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css" />
                
        <!-- Core App css -->
        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/pace.min.css" rel="stylesheet" type="text/css" />
				        
        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url() ?>assets/js/modernizr.min.js"></script>
        <script type="text/javascript">
            var SITE_URL = '<?php echo site_url() ?>';
        </script>
    </head>


    <body class="fixed-left" style="padding-bottom: 0 !important;">
        <div id="wrapper">