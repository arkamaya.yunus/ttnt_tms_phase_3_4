<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('driver')?>">Driver</a>
					</li>
					<li class="active">Detail</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	
	<div class="row">
		<?php if ($this->session->flashdata('notif_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
			</div>
		</div>
		<?php endif; ?>

		<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="myModalLabel">Delete</h4>
					</div>
					<div class="modal-body">
						Are you sure?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-danger waves-effect waves-light" onclick="doDelete()" id="btn_delete_confirm">Delete</button>
					</div>
				</div>
			</div>
		</div>
	
		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<input type="hidden" name="driver_cd" id="driver_cd" value="<?php echo $this->uri->segment(3)?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">NIK</label>
								<div class="col-md-8 m-t-5">
									<?php echo $driver->driver_cd?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Driver Name</label>
								<div class="col-md-8 m-t-5">
									<?php echo $driver->driver_name?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Logistic Partner</label>
								<div class="col-md-8 m-t-5">
									<?php echo $driver->logistic_partner;?>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Job Desc</label>
								<div class="col-md-8 m-t-5">
									<?php										
										echo $driver->jobdesc;
									?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Phone Number</label>
								<div class="col-md-8 m-t-5">
									<?php echo $driver->smartphone?>
								</div>
							</div>                            
							<div class="form-group">
								<label class="col-md-4 control-label">Photo</label>
								<div class="col-md-8 m-t-5">
									<?php
										$filep = COMPANY_ASSETS_PATH. 'cid' . $this->session->userdata(S_COMPANY_ID) . '/profile/thumbs/' . $driver->photo;
										$photo = 'assets/images/user_photo.png';										
										if ($driver->photo != '') {
											if (file_exists($filep)) {
												$photo = $filep;
											}
										}
									?>
									<img src="<?php echo base_url() ?><?php echo $photo ?>" class="img-thumbnail" alt="<?php echo $driver->driver_name ?>" style="max-height: 128px" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Active</label>
								<div class="col-md-8 m-t-5">
								<?php echo ($driver->active == '1') ? 'Yes' : 'No'?>
								</div>
                            </div>
							<div class="form-group">
								<label class="col-md-4 control-label">Resign Date</label>
								<div class="col-md-8 m-t-5">
									<?php echo ($driver->resign_dt != '')  ? date('d-M-Y', strtotime($driver->resign_dt)) : '-'?>
								</div>
                            </div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-md-5 control-label">Join Date</label>
								<div class="col-md-7 m-t-5">									                                    
									<?php echo ($driver->join_dt != '')  ? date('d-M-Y', strtotime($driver->join_dt)) : '-'?>
								</div>
							</div>
                            <div class="form-group">
								<label class="col-md-5 control-label">Length Of Working</label>
								<div class="col-md-7 m-t-5">									
                                    <?php echo $low ?>
								</div>
							</div>
                            <div class="form-group">
								<label class="col-md-5 control-label">Number Of Accident/Incident</label>
								<div class="col-md-7 m-t-5">									
                                    <?php echo $driver->num_of_accident ?>
								</div>
							</div>							
                            <div class="form-group">
								<label class="col-md-5 control-label">License Validation Driving</label>
								<div class="col-md-7 m-t-5">
									<?php echo ($driver->driving_license_val != '')  ? date('d-M-Y', strtotime($driver->driving_license_val)) : '-'?>
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-5 control-label">Type Of Driver License Driving</label>
								<div class="col-md-7 m-t-5">									
                                    <?php echo $driver->driving_license_type?>
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-5 control-label">SIO</label>
								<div class="col-md-7 m-t-5">									
                                    <?php echo ($driver->forklift_license_val != '')  ? date('d-M-Y', strtotime($driver->forklift_license_val)) : '-'?>
								</div>
							</div>
                            <div class="form-group">
								<label class="col-md-5 control-label">SIO Type</label>
								<div class="col-md-7 m-t-5">									
                                    <?php echo $driver->forklift_license_type ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Attendance ID</label>
								<div class="col-md-7 m-t-5">									
                                    <?php echo ($driver->att_id != '') ? $driver->att_id : '(Not Set)' ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Date of Birth</label>
								<div class="col-md-7 m-t-5">									
								<?php echo ($driver->dob != '')  ? date('d-M-Y', strtotime($driver->dob)) : '-'?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Business</label>
								<div class="col-md-7 m-t-5">									
                                    <?php echo $driver->business ?>
								</div>
							</div>
						</div>
					</div>
					<hr/>
																		
				</div>
				
				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('driver')?>'">Back</button>
						<?php							
							$attrEdit = ''; // ($journal->reconcile_id!='' || $journal->journal_type != 'voucher') ? 'disabled':'';
							echo create_button($this->button, "btn_edit", $attrEdit, $this->uri->segment(3)); 
                        ?>
                        <?php 
							$attrDelete = ''; //($journal->reconcile_id!='' || $journal->journal_type != 'voucher') ? 'disabled':'';
                            echo create_button($this->button, "btn_delete", $attrEdit); 
                        ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>