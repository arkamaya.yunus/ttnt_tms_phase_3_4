<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('vehicle')?>">Vehicle</a>
					</li>
					<li class="active">Detail</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	
	<div class="row">
		<?php if ($this->session->flashdata('notif_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
			</div>
		</div>
		<?php endif; ?>

		<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="myModalLabel">Delete</h4>
					</div>
					<div class="modal-body">
						Are you sure?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-danger waves-effect waves-light" onclick="doDelete()" id="btn_delete_confirm">Delete</button>
					</div>
				</div>
			</div>
		</div>
	
		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<input type="hidden" name="vehicle_cd" id="vehicle_cd" value="<?php echo $this->uri->segment(3)?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
						<div class="form-group">
								<label class="col-md-4 control-label">Truck Number</label>
								<div class="col-md-8 m-t-5">
									<?php echo $vehicle->vehicle_cd;?>
								</div>
							</div>
							<!-- <div class="form-group">
								<label class="col-md-4 control-label">Vehicle ID (Innova Track)</label>
								<div class="col-md-8 m-t-5">
									<?php echo $vehicle->vehicle_id;?>
								</div>
							</div> -->
							<div class="form-group">
								<label class="col-md-4 control-label">Police Number</label>
								<div class="col-md-8 m-t-5">
									<?php echo $vehicle->vehicle_number?>
								</div>
							</div>							
							<div class="form-group">
								<label class="col-md-4 control-label">Vehicle Type</label>
								<div class="col-md-8 m-t-5">
									<?php										
										echo $vehicle->vehicle_type;
									?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Owner</label>
								<div class="col-md-8 m-t-5">
									<?php echo $vehicle->vehicle_owner?>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Brand</label>
								<div class="col-md-8 m-t-5">
									<?php echo $vehicle->brand?>
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-4 control-label">Active</label>
								<div class="col-md-8 m-t-5">
									<?php echo ($vehicle->active == '1') ? 'Yes' : 'No'?>
								</div>
                            </div>
                            
							<div class="form-group">
								<label class="col-md-4 control-label">Business</label>
								<div class="col-md-8 m-t-5">
									<?php echo $vehicle->business?>
								</div>
                            </div>
						</div>
						<div class="col-sm-6">							
                            <div class="form-group">
								<label class="col-md-4 control-label">License STNK</label>
								<div class="col-md-8 m-t-5">
									<?php echo ($vehicle->license_stnk != '')  ? date('d-M-Y', strtotime($vehicle->license_stnk)) : '-'?>
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-4 control-label">License KEUR</label>
								<div class="col-md-8 m-t-5">									
                                    <?php echo ($vehicle->license_keur != '')  ? date('d-M-Y', strtotime($vehicle->license_keur)) : '-'?>
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-4 control-label">License SIPA</label>
								<div class="col-md-8 m-t-5">									
                                    <?php echo ($vehicle->license_sipa != '')  ? date('d-M-Y', strtotime($vehicle->license_sipa)) : '-'?>
								</div>
							</div>
                            <div class="form-group">
								<label class="col-md-4 control-label">License IBM</label>
								<div class="col-md-8 m-t-5">									
                                    <?php echo ($vehicle->license_ibm != '')  ? date('d-M-Y', strtotime($vehicle->license_ibm)) : '-'?>
								</div>
							</div>
						</div>
					</div>
					<hr/>
																		
				</div>
				
				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('vehicle')?>'">Back</button>
						<?php							
							$attrEdit = ''; // ($journal->reconcile_id!='' || $journal->journal_type != 'voucher') ? 'disabled':'';
							echo create_button($this->button, "btn_edit", $attrEdit, $this->uri->segment(3)); 
                        ?>
                        <?php 
							$attrDelete = ''; //($journal->reconcile_id!='' || $journal->journal_type != 'voucher') ? 'disabled':'';
                            echo create_button($this->button, "btn_delete", $attrEdit); 
                        ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>