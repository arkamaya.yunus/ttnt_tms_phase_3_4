<style>
    #sidebar-pengaturan{
        padding-bottom:30px;
        width:100%;
    }
    #container-forms{
        border-left: 2px solid black;
        padding-left:25px;
    }

    #sidebar-pengaturan, #sidebar-pengaturan a, #sidebar-pengaturan li, #sidebar-pengaturan ul {
        font-weight: 400;
        line-height: 1;
        list-style: none;
        margin: 0;
        padding: 0;
        position: relative;
        text-decoration: none;
    }

    #sidebar-pengaturan > ul > li > a {
        color: #2e383d;
        display: block;
        padding: 12px 20px;
        margin: 2px 0;
    }
    #sidebar-pengaturan ul li.active a, .button-menu-mobile:hover {
        color: #7fc1fc;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Settings</h4>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <?php if ($this->session->flashdata('notif_company_success') != ''): ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_company_success')?>
			</div>
		</div>
	</div>
	<?php endif; ?>

    <?php if ($this->session->flashdata('notif_company_uploads_error') != ''): ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_company_uploads_error')?>
			</div>
		</div>
	</div>
	<?php endif; ?>

    <div class="row">
        <div class="col-xs-12">
            <form id="settings_company" class="form-horizontal" role="form" method="post" action="<?php echo site_url('settings/create_company')?>" enctype="multipart/form-data">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-2">
                            <div id="sidebar-pengaturan">
                                <?php echo $menu; ?>
                            </div>
                        </div>
                        <div class="col-md-10" id="container-forms">
                            <div class="rows">
                                <div class="col-md-12 m-b-20">
                                    <h4 class="page-title pb-20">Company</h4>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="hidden" id="company_id" name="company_id"  value="<?php echo ($company != null) ? $company->company_id : '' ?>">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Company Name<span class="text-danger">*</span></label>
                                                <div class="col-md-8">
                                                    <input type="text" maxlength="100" class="form-control" value="<?php echo ($company != null) ? $company->company_name : '' ?>" name="company_name" id="company_name" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Address</label>
                                                <div class="col-md-8">
                                                    <textarea class="form-control" maxlength="255"   rows="2" name="address" id="address" ><?php echo ($company != null) ? $company->address : '' ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Industry</label>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" maxlength="100" value="<?php echo ($company != null) ? $company->company_industry : '' ?>" name="company_industry" id="company_industry">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Company Logo</label>
                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        <input type="file" id="logo" name="logo" accept=".png, .jpg, .jpeg" style="display:none" onchange="document.getElementById('filename').value=this.value">
                                                        <input type="text" id="filename" class="form-control" disabled>
                                                        <input type="hidden" id="file_tmp" name="file_tmp" value="<?php echo ($company != null) ? $company->logo : '' ?>" >
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn waves-effect waves-light btn-primary" onclick="document.getElementById('logo').click()">Browse File</button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if($company != null) :?>
                                                <?php if($company->logo != null && file_exists("./".$company->logo)) :?>
                                                <div class="form-group">
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-8">
                                                        <img src="<?php echo $company->logo; ?>" id="preview_image" alt="" style="max-width: 300px">
                                                    </div>
                                                </div>
                                                <?php endif;?>
                                            <?php endif; ?>
                                            
                                            <div class="form-group">
                                                <div class="col-md-4"></div>
                                                <div class="col-md-8">
                                                    <div class="checkbox">
                                                        <input type="checkbox" value="1" name="delete_logo" id="delete_logo">
                                                        <label for="delete_logo">
                                                            Hapus Logo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Email</label>
                                                <div class="col-md-8">
                                                    <input type="email" maxlength="100" class="form-control" value="<?php echo ($company != null) ? $company->email : '' ?>" name="email" id="email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Phone No.</label>
                                                <div class="col-md-8">
                                                    <input type="text" maxlength="100" class="form-control numbersOnly" value="<?php echo ($company != null) ? $company->phone : '' ?>" name="phone" id="phone">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Fax No.</label>
                                                <div class="col-md-8">
                                                    <input type="text" maxlength="20" class="form-control numbersOnly" id="fax" name="fax" value="<?php echo ($company != null) ? $company->fax : '' ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Npwp No.</label>
                                                <div class="col-md-8">
                                                    <input type="text" maxlength="50" class="form-control numbersOnly" id="tax_number" name="tax_number" value="<?php echo ($company != null) ? $company->tax_number : '' ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Website</label>
                                                <div class="col-md-8">
                                                    <input type="text" maxlength="100" class="form-control" id="company_website" name="company_website" value="<?php echo ($company != null) ? $company->website : '' ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                            <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('')?>'">Cancel</button>
                            <button type="button" class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 sbmt_btn" id="btn_submit">Save</button>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>
</div>