<style>
    .btn-dflt
    {
        border-bottom: 2px solid #dadada !important;
    }
    .rcdata, 
    .control-label,
    .tdl
    {
        font-size: 18px !important;
    }
    .modal {
    }
    .vertical-alignment-helper {
        display:table;
        height: 100%;
        width: 100%;
    }
    .vertical-align-center {
        /* To center vertically */
        display: table-cell;
        vertical-align: middle;
    }
    .modal-content {
        /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
        width:inherit;
        height:inherit;
        /* To center horizontally */
        margin: 0 auto;
    }
    .font-white
    {
        color: #ffffff;
    }
</style>
<div class="container">

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <!-- <div class="modal-header">					
                        <h4 class="modal-title" id="myModalLabel">Fingerscan Driver To Start a Roll Call</h4>
                    </div> -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 text-center" style="font-size: 26px; font-weight:bold; color: red">
                                GET MANUAL ROLLCALL DATA <i class="fa fa-spinner fa-pulse fa-fw"></i>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: center !important;">
                        <button type="button" class="btn btn-default btn-sm waves-effect" onclick="window.location='<?php echo site_url('rollcall')?>'">Back to Roll Call Report</button>
                    </div>
                </div>
            </div>
        </div>
	</div>

    <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal2Label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModal2Label">Konfirmasi</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12" style="font-size: 16px" id="modalmsg">
                            Apakah Anda Yakin ?
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary waves-effect waves-light" onclick="onComplete()" id="btnOnComplete">Complete</button>
				</div>
			</div>
		</div>
	</div>
        
    <div class="row page-title-box" style="margin-bottom: 5px;background-color: #188ae2; color: white; padding: 5px 10px 5px 10px !important">
        <div class="col-sm-3">
            <label class="col-md-4 control-label">Date :</label>
            <div class="col-md-8 rcdata" id="departure_dt" style="border-bottom: 1px solid #dadada">08/06/2020</div>
        </div>
        <div class="col-sm-9">
            <label class="col-md-2 control-label text-right">Customer :</label>
            <div class="col-md-2 rcdata" id="customer" style="border-bottom: 1px solid #dadada">TMMIN</div>
            <label class="col-md-2 control-label text-right">Route :</label>
            <div class="col-md-2 rcdata" id="route" style="border-bottom: 1px solid #dadada">DN-01</div>
            <label class="col-md-2 control-label text-right">Cycle :</label>
            <div class="col-md-2 rcdata" id="cycle" style="border-bottom: 1px solid #dadada">1 - 10</div>
        </div>
    </div>
    <div class="row m-t-10">
        <div class="col-sm-3">
            <div class="row" style="border-bottom: 2px solid #303030">
                <label class="col-md-12 control-label2" style="font-size: 24px; ">Roll Call (<?php echo $lp_cd?>)</label>
                <input type="hidden" id="m_driver_cd" value="<?php echo $this->uri->segment(3)?>" />
                <input type="hidden" id="m_route" value="<?php echo $this->uri->segment(4)?>" />
                <input type="hidden" id="m_cycle" value="<?php echo $this->uri->segment(5)?>" />
                <input type="hidden" id="lp_cd" value="<?php echo $this->uri->segment(6)?>" />
                <input type="hidden" id="business" value="" />
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <label class="col-md-6 m-t-10 control-label">
                             Driver Name
                        </label>
                        <div class="col-md-6 m-t-10 text-right">
                            <span class="tdl badge badge-primary" style="text-align: left; padding: 10px">1.</span> 
                        </div>
                        <div class="col-md-12 rcdata" id="driver_name" style="border-bottom: 1px solid #303030; padding-left: 30px; padding-bottom: 10px">Sigit Wasis</div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 10px">                
                        <label class="col-md-4 control-label">NIK</label>
                        <div class="col-md-8" id="">
                            <span id="driver_cd" class="rcdata" check="">12345678</span>
                            <button class="tdl pull-right btn btn-dflt btn-toggle" id="btn_driver_cd" onclick="toggleOn(this);">NG</button>
                        </div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 5px">                
                        <label class="col-md-4 control-label">Phone</label>
                        <div class="col-md-8 rcdata" id="smartphone">12345678</div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 10px">                
                        <label class="col-md-4 control-label">SIM</label>
                        <div class="col-md-8">
                            <span id="driving_license_val" class="rcdata" check="">12345678</span>
                            <button class="tdl pull-right btn btn-dflt btn-toggle" id="btn_driving_license_val" onclick="toggleOn(this);">NG</button>
                        </div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 2px solid #303030; padding-bottom: 10px">                
                        <label class="col-md-4 control-label">SIO</label>
                        <div class="col-md-8">
                            <span id="forklift_license_val" class="rcdata" check="">12345678</span>
                            <button class="tdl pull-right btn btn-dflt btn-toggle" id="btn_forklift_license_val" onclick="toggleOn(this);">NG</button>
                        </div>
                    </div>                        
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 5px">                
                        <label class="col-md-4 control-label">Truck</label>
                        <div class="col-md-8 rcdata" id="vehicle_cd">T1602</div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 5px">                
                        <label class="col-md-4 control-label">STNK</label>
                        <div class="col-md-8 rcdata" id="license_stnk">DD/MM/YYYY</div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 5px">                
                        <label class="col-md-4 control-label">KIR</label>
                        <div class="col-md-8 rcdata" id="license_keur">DD/MM/YYYY</div>
                    </div>
                    <div class="row m-t-10" style="border-bottom: 1px solid #303030; padding-bottom: 5px">                
                        <label class="col-md-4 control-label">SIPA</label>
                        <div class="col-md-8 rcdata" id="license_sipa">DD/MM/YYYY</div>
                    </div>
                </div>
            </div>            
        </div>
        
        <div class="col-sm-9">        
            <table style="width: 100%" class="table table-bordered">
                <tr>
                    <td style="padding-bottom: 3px !important; border-left: solid #ffffff !important; border-top: solid #ffffff !important; border-bottom: 2px solid #303030 !important;" colspan="2"></td>
                    <td class="tdl" style="padding-bottom: 3px !important; border-bottom: 2px solid #303030 !important; width: 30%; text-align: center; font-weight: bold">IN</td>
                    <td class="tdl" style="padding-bottom: 3px !important; border-left: 2px solid #303030; border-bottom: 2px solid #303030 !important; width: 30%; text-align: center; font-weight: bold">OUT</td>
                </tr>
                <tr>
                    <td rowspan="2" class="" style="border-bottom: 2px solid #303030 !important;vertical-align: middle !important; font-weight: bold; font-size: 18px">Time</td>
                    <td class="tdl">Plan</td>
                    <td class="text-center rcdata" id="departure_plan">07:00</td>
                    <td class="text-center rcdata" style="border-left: 2px solid #303030; " id="arrival_plan">08:00</td>
                </tr>
                <tr>                    
                    <td class="tdl" style="border-bottom: 2px solid #303030 !important;">Act</td>
                    <td style="border-bottom: 2px solid #303030 !important;" class="text-center rcdata" id="departure_actual">07:00</td>
                    <td style="border-bottom: 2px solid #303030 !important; border-left: 2px solid #303030; " class="text-center rcdata" id="arrival_actual">08:00</td>
                </tr>
                <tr>
                    <td rowspan="3" class="" style="border-bottom: 2px solid #303030 !important; vertical-align: middle !important; font-weight: bold; font-size: 18px">
                        <span class="tdl badge badge-primary" style="text-align: left; padding: 10px">2.</span> Team Driver
                    </td>
                    <td class="tdl">Tekanan Darah</td>
                    <td>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <input type="text" value="" id="driver_blood_pres_sistole" class="tdl form-control autonumber2 text-right" placeholder="< 140" onclick="this.select()" onkeyup="checkSistole()" />
                                </div>
                                <div class="col-md-4">
                                    <input type="text" value="" id="driver_blood_pres_diastole" class="tdl form-control autonumber2 text-right" placeholder="60 - 90"  onclick="this.select()" onkeyup="checkDiastole()" />
                                </div>
                                <div class="tdl col-md-4">mmHg</div>
                            </div>
                        </div>
                    </td>
                    <td style="border-left: 2px solid #303030; "></td>
                </tr>
                <tr>
                    <td class="tdl">Temperatur</td>
                    <td>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="text" value="" id="driver_body_temp" class="tdl form-control autonumber text-right" placeholder="35 - 37,5"  onclick="this.select()" onkeyup="checkBodyTemp()" />
                            </div>
                            <div class="col-md-4 tdl">&#8451;</div>
                        </div>
                    </td>

                    <td style="border-left: 2px solid #303030; "></td>
                </tr>
                <tr>
                    <td class="tdl" style="border-bottom: 2px solid #303030 !important;">Waktu Tidur >= 5 Jam</td>
                    <td style="border-bottom: 2px solid #303030 !important;">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="driver_sleep_time" value=""/>
                                <button class="tdl btn btn-dflt btn-driver_sleep_time" style="width: 100%" onclick="toggleOn2(this, 'driver_sleep_time')">OK</button>
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="tdl btn btn-dflt btn-driver_sleep_time" style="width: 100%" onclick="toggleOn2(this, 'driver_sleep_time')">NG</button>                                
                            </div>
                        </div>
                    </td>
                    <td style="border-left: 2px solid #303030; border-bottom: 2px solid #303030 !important;"></td>
                </tr>
                <tr>
                    <td rowspan="4" class="" style="border-bottom: 2px solid #303030 !important; vertical-align: middle !important; font-weight: bold; font-size: 18px">
                        <span class="tdl badge badge-info" style="text-align: left;  padding: 10px">3. </span> Kasir
                    </td>
                    <td class="tdl">Fuel
                        (<span id="cashier_etoll_spbu"></span>)
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-8 text-right rcdata" id="cashier_fuel_plan">
                                50
                            </div>
                            <div class="col-md-4">L</div>
                        </div>
                    </td>
                    <td style="border-left: 2px solid #303030">
                        <div class="row">
                            <div class="col-md-8">
                                <input type="text" value="" id="" class="tdl dsbl form-control autonumber2 text-right" placeholder="0"  />
                            </div>
                            <div class="col-md-1">L</div>
                            <div class="col-md-3"><button class="dsbl btn btn-dflt">R</button></div>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td class="tdl">E-Toll</td>
                    <td>
                        <div class="row">
                            <div class="col-md-8 text-right rcdata" id="cashier_etoll_plan">
                                5.000
                            </div>
                            <div class="col-md-4">Rp.</div>
                        </div>
                    </td>
                    <td style="border-left: 2px solid #303030">
                        <div class="row">
                            <div class="col-md-8 text-right">
                                0
                            </div>
                            <div class="col-md-1">Rp.</div>
                            <div class="col-md-3"></div>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" value="" id="etoll_id" class="tdl dsbl form-control" placeholder="Scan E-Toll Card" />
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-8">                                
                                <input type="text" value="" id="etoll_balance_departure" class="tdl dsbl form-control autonumber2 text-right" placeholder="0" />
                            </div>
                            <div class="col-md-4">Rp.</div>
                        </div>
                    </td>
                    <td style="border-left: 2px solid #303030">
                        <div class="row">
                            <div class="col-md-8">
                                <input type="text" value="" id="" class="tdl dsbl form-control autonumber2 text-right" placeholder="0" />
                            </div>
                            <div class="col-md-1">Rp.</div>
                            <div class="col-md-3"><button class="dsbl btn btn-dflt">R</button></div>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td class="tdl" style="border-bottom: 2px solid #303030 !important;">Others</td>
                    <td style="border-bottom: 2px solid #303030 !important;">
                        <div class="row">
                            <div class="col-md-8 text-right rcdata" id="cashier_others_plan">
                                5.000
                            </div>
                            <div class="col-md-4">Rp.</div>
                        </div>
                    </td>
                    <td style="border-left: 2px solid #303030; border-bottom: 2px solid #303030 !important;">
                        <div class="row">
                            <div class="col-md-8 text-right">
                                <input type="text" value="" id="" class="tdl dsbl form-control autonumber2 text-right" placeholder="0" />
                            </div>
                            <div class="col-md-1">Rp.</div>
                            <div class="col-md-3"><button class="dsbl btn btn-dflt">R</button>
                            </div>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" class="" style="vertical-align: middle !important; font-weight: bold; font-size: 18px">
                        <span class="tdl badge badge-danger" style="text-align: left;  padding: 10px">4.</span>  CCR
                    </td>
                    <td class="tdl">APD / Badge</td>
                    <td>
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_apd_departure" value="0"/>
                                <button class="tdl dsbl btn btn-dflt btn-controller_apd_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_apd_departure')">OK</button>                                
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="tdl dsbl btn btn-dflt btn-controller_apd_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_apd_departure')">NG</button>                                
                            </div>
                        </div>
                    </td>
                    <td style="border-left: 2px solid #303030">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_apd_arrival" value="0"/>
                                <button class="tdl dsbl btn btn-dflt btn-controller_apd_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_apd_arrival')">OK</button>                                
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="tdl dsbl btn btn-dflt btn-controller_apd_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_apd_arrival')">NG</button>                                
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">Basket</td>
                    <td>
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_basket_departure" value="0"/>
                                <button class="tdl dsbl btn btn-dflt btn-controller_basket_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_basket_departure')">OK</button>                                
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="tdl dsbl btn btn-dflt btn-controller_basket_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_basket_departure')">NG</button>                                
                            </div>
                        </div>
                    </td>
                    <td style="border-left: 2px solid #303030">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_basket_arrival" value="0"/>
                                <button class="tdl dsbl btn btn-dflt btn-controller_basket_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_basket_arrival')">OK</button>                                
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="tdl dsbl btn btn-dflt btn-controller_basket_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_basket_arrival')">NG</button>                                
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">Kunci</td>
                    <td>
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_key_departure" value="0"/>
                                <button class="tdl dsbl btn btn-dflt btn-controller_key_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_key_departure')">OK</button>                                
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="tdl dsbl btn btn-dflt btn-controller_key_departure"  style="width: 100%" onclick="toggleOn2(this, 'controller_key_departure')">NG</button>                                
                            </div>
                        </div>
                    </td>
                    <td style="border-left: 2px solid #303030">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <input type="hidden" id="controller_key_arrival" value="0"/>
                                <button class="tdl dsbl btn btn-dflt btn-controller_key_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_key_arrival')">OK</button>                                
                            </div>
                            <div class="col-md-6 text-center">
                                <button class="tdl dsbl btn btn-dflt btn-controller_key_arrival"  style="width: 100%" onclick="toggleOn2(this, 'controller_key_arrival')">NG</button>                                
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><button type="button" name="btn_back" id="btn_back" class="form-control btn btn-bordered btn-custom" onclick="window.location='<?php echo site_url('rollcall')?>'">&lt; Back</button></td>
                    <td></td>
                    <td>
                        <button type="button" name="btn_complete" id="btn_complete" class="tdl form-control btn btn-bordered btn-primary" onclick="doComplete('team_driver')">Complete</button>
                        <!-- <button name="" id="" class="form-control btn btn-bordered btn-purple">Driver Report</button> -->
                    </td>
                    <td>
                        <!-- <button name="" id="" class="dsbl form-control btn btn-bordered btn-primary">Start Route</button> -->
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>