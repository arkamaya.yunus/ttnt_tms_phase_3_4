<style>
table#datatable.dataTable tbody tr:hover {
	background-color: #d8e2f0;cursor: pointer;
}
</style>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li class="active"><?php echo $stitle?></li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Create/Edit</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<form id="frm" class="form-horizontal" role="form" method="post" action="">
                                <input type="hidden" id="todo" />
								<div class="form-group">
									<label class="col-md-4 col-sm-3 control-label">Customer Code <span class="text-danger">*</span></label>
									<div class="col-md-8 col-sm-9">
                                        <input type="text" class="form-control" id="system_code" name="system_code" value="" required />
									</div>
								</div>
                                <div class="form-group">
									<label class="col-md-4 col-sm-3 control-label">Name Code <span class="text-danger">*</span></label>
									<div class="col-md-8 col-sm-9">
                                        <input type="text" class="form-control" id="system_value_txt" name="system_value_txt" value="" required />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-bordered waves-light waves-effect w-md m-b-5" data-dismiss="modal">Cancel</button>
					<?php echo create_button($this->button, "btn_submit"); ?>
                    <button type="button" class="btn btn-danger btn-bordered waves-light waves-effect w-md m-b-5 pull-left" onclick="$('#lbl-ays').show();" id="btn_delete" onclick="onDelete()">Delete</button>
					<span class="pull-left m-t-5" id="lbl-ays"> &nbsp;&nbsp; &nbsp;&nbsp; Are You Sure? <a href="javascript:;" onclick="doDelete()">Yes</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="javascript:;" onclick="$('#lbl-ays').hide();">No</a> </span>
				</div>
			</div>
		</div>
	</div>
				
	<div class="row m-b-10">
		<?php if ($this->session->flashdata('notif_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
			</div>
		</div>
		<?php endif; ?>
	
		<div class="col-sm-5">			
			<?php echo create_button($this->button, "btn_add"); ?>
		</div>

        <div class="col-sm-7">            
            <div class="col-md-2 pull-right">
                <button id="btn_filter" type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="doFilter()">Refresh</button>
            </div>
        </div>
	</div>
	
	<div class="row">		
		<div class="col-sm-12">
			<div class="card-box table-responsive">
				<table id="datatable" class="table table-striped table-hover display nowrap">
					<thead>
					<tr>
						<th>Customer Code</th>
						<th>Customer Name</th>
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>			
</div>