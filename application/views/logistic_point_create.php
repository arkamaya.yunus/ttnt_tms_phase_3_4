<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('logistic_point')?>">Logistic Point</a>
					</li>
					<li class="active">Create</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	
	<div class="row">
		<?php if ($save_sts == '0'): ?>
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> Save failed. Initial <b><?php echo $lp_cd?></b> already used.
			</div>
		</div>
		<?php endif; ?>

		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">				
			<input type="hidden" class="form-control" id="lp_type_id" name="lp_type_id" value="2" required />
			<input type="hidden" class="form-control" id="lp_business" name="lp_business" value="<?php echo $lp_business?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Initial <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="lp_cd" name="lp_cd" value="<?php echo $lp_cd?>" required />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Name <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="lp_name" name="lp_name" value="<?php echo $lp_name?>" required />
								</div>
							</div>							
							<div class="form-group">
								<label class="col-md-4 control-label">Address</label>
								<div class="col-md-8 m-t-5">
                                    <textarea class="form-control" rows="5" name="lp_address" id="lp_address"><?php echo $lp_address?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Logistic Flag</label>
								<div class="col-md-8 m-t-5">
									<select name="empty_plan_flg" id="empty_plan_flg" class="form-control select2">
										<option value="0" <?php echo ($empty_plan_flg == '0') ? 'selected="selected"' : ''?>>No</option>
										<option value="1" <?php echo ($empty_plan_flg == '1') ? 'selected="selected"' : ''?>>Yes</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-6">	
							
							<div class="form-group">
								<label class="col-md-4 control-label">Area</label>
								<div class="col-md-8 m-t-5">
                                    <!-- <input type="text" class="form-control" id="lp_area" name="lp_area" value="<?php echo $lp_area?>" /> -->
									<select name="lp_area" id="lp_area" class="form-control select2" onchange="changeArea()">
										<?php foreach ($areas as $a): ?>
										<option value="<?php echo $a->area_cd?>" data-color="<?php echo $a->area_color?>" <?php echo ($lp_area == $a->area_cd) ? 'selected="selected"' : ''?>><?php echo $a->area_cd?></option>
										<?php endforeach; ?>
                                	</select>
								</div>
							</div>							
                            <div class="form-group">
								<label class="col-md-4 control-label">Color</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="lp_color" name="lp_color" value="<?php echo $lp_color?>" readonly="readonly" />
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-4 control-label">Remark</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="lp_remark" name="lp_remark" value="<?php echo $lp_remark?>" />
								</div>
                            </div>
                            <!-- <div class="form-group">
								<label class="col-md-4 control-label">Business</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="lp_business" name="lp_business" value="<?php echo $lp_business?>" />
								</div>
                            </div> -->
                            <div class="form-group">
								<label class="col-md-4 control-label">Geofence Id</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="geofenceid" name="geofenceid" value="<?php echo $geofenceid?>" />
								</div>
                            </div>						
                            <!-- <div class="form-group">
								<label class="col-md-4 control-label">Type</label>
								<div class="col-md-8 m-t-5">
                                <select name="lp_type_id" id="lp_type_id" class="form-control select2">
                                    <?php foreach ($lpts as $lpt): ?>
                                    <option value="<?php echo $lpt->lp_type_id?>" <?php echo ($lp_type_id == $lpt->lp_type_id) ? 'selected="selected"' : ''?>><?php echo $lpt->lp_type_name?></option>
                                    <?php endforeach; ?>
                                </select>
								</div>
							</div> -->
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('logistic_point')?>'">Back</button>
						<?php echo create_button($this->button, "btn_submit"); ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>