<html>
    <head>
        <title>Driver-Report-<?php echo $rollcall->voucher?></title>
        <style>
        *
        {
            font-family: sans-serif;
            font-size: 10px;
        }
        h1
        {
            font-size: 20px;
            margin: 0;
        }
        h3
        {
            font-size: 14px;
            margin: 0;
        }
        table
        {
            border-spacing: 0;
            border-collapse: collapse; 
        }
        #tbl1 tr td,
        #tbl2 tr td
        {
            border: 1px solid #000000;
            text-align: center;
        }
        .dock
        {
            background-color: #000000 !important;
            -webkit-print-color-adjust: exact; 
            color: white !important;
        }

        /* for watermarking */
        .content
        {
            position:relative;
            display:inline-block;
            overflow: hidden;
            width: 100%;
        }

        .content p {
            position: absolute;
            top: 0;
            left: 0;
            color: #000; /* Fallback for older browsers */
            color: rgba(34, 62, 117, 0.2);
            font-size: 18px;
            font-family: Consolas,Menlo,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New,monospace,sans-serif;
            pointer-events: none;
            z-index: -1;
            /* -webkit-transform: rotate(-25deg);
            -moz-transform: rotate(-25deg); */
        }
        </style>        
    </head>
    <body>
        <table width="100%">
            <tr>
                <td>
                    <img src="<?php echo base_url()?>assets/files/cid2/profile/TTNT_TP.png" width="50" height="50" />
                </td>
                <td width="50%">
                    <h1>Laporan Pengemudi</h1>                   
                    <u>ID: <?php echo $rollcall->voucher?></u>
                </td>
                <td colspan="2" rowspan="2" style="font-size: 16px; font-weight: bolder">
                    <!-- <img src="<?php echo base_url()?>assets/images/checklist.png" width="50" height="50" /> -->
                    <?php 
                        // cari jumlah driver di route dan tgl yg sama
                        $sql = "
                            SELECT 
                                a.driver_cd
                                , a.driver_name 
                            FROM 
                                tb_r_timetable_detail a 
                            WHERE 
                                a.timetable_dt = '".$rollcall->departure_dt."' 
                                AND a.route = '".$rollcall->route."' 
                                AND a.vehicle_cd = '".$rollcall->vehicle_cd."'
                            GROUP BY a.driver_cd
                            ORDER BY a.departure_plan
                        ";
                        $drivers = $this->db->query($sql)->result();
                        $i = 1;
                        $i_driver = 1;
                        if (count($drivers) > 0)
                        {
                            foreach ($drivers as $d)
                            {
                                if ($d->driver_cd == $rollcall->driver_cd)
                                {
                                    $i_driver = $i;
                                }
                                $i++;
                            }

                        }
                        $cdrv = (count($drivers) == 0) ? 1 : count($drivers);
                        echo $i_driver . ' Of ' . $cdrv;

                    ?>                    
                </td>
                <!-- <td rowspan="2">
                    <input type="checkbox" /> Triplek <br/>
                    <input type="checkbox" /> Lashing <br/>
                    <input type="checkbox" /> Tambang 
                </td> -->
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <tr>
                            <td>Tgl.</td>
                            <td><strong><?php echo date('d-F Y', strtotime($rollcall->timetable_dt))?></strong></td>
                            <td>Rute</td>
                            <td><strong><?php echo $rollcall->route?></strong></td>
                            <td>Cycle</td>
                            <td><strong><?php echo $rollcall->cycle?></strong></td>
                            <td>Nama</td>
                            <td><strong><?php  echo $rollcall->driver_name ?></strong></td>
                            <td>Truck</td>
                            <td><strong><?php  echo $rollcall->vehicle_cd ?></strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" valign="top">
                    <table width="100%">
                        <tr>
                            <td width="50%" valign="top">
                                <table id="tbl1" width="100%">
                                    <tr>
                                        <td style="font-size: 16px; font-weight: bold; border-top: none; border-left: none; border-right: 2px solid; border-bottom: 2px solid" rowspan="2" colspan="2"><?php echo $rollcall->customer?></td>
                                        <td style="font-size: 12px; border-top: 2px solid; border-right: 2px solid; font-weight: bold" colspan="3">Waktu</td>
                                        <td style="font-size: 12px; font-weight: bold" colspan="3">Masalah</td>                                        
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px; font-weight: bold">IN</td>
                                        <td style="font-size: 12px; font-weight: bold">OUT</td>
                                        <td style="; border-right: 2px solid"><img src="<?php echo base_url()?>assets/images/loadingtime.png" width="10" height="10" /></td>
                                        <td style="font-size: 12px; font-weight: bold">Keterangan</td>
                                        <td style="font-size: 12px; font-weight: bold" colspan="2">Nomor</td>
                                    </tr>
                                    <?php 
                                    $z=1; $max_row = 15; 
                                    $sel_row = count($timetable) > $max_row ? 0 : $max_row - count($timetable);
                                    $sisa = count($timetable) - 1;
                                    foreach($timetable as $t):
                                        
                                        $bbot = '';
                                        if ($z == $max_row)
                                        {
                                            $bbot = "; border-bottom: 2px solid";
                                        }
                                    ?>
                                    <tr>
                                        <td rowspan="2" style="font-weight: bold; font-size: 14px; border-left: 2px solid<?php echo $bbot?>"><?php echo $t->lp_cd?></td>
                                        <td style="font-weight: bold; font-size: 8px">P</td>
                                        <td style="font-weight: bold; font-size: 14px;"><?php echo substr($t->arrival_plan,0,5)?></td>
                                        <td style="font-weight: bold; font-size: 14px"><?php echo substr($t->departure_plan,0,5)?></td>
                                        <td style="font-weight: bold; font-size: 14px; border-right: 2px solid"><?php echo ($t->arrival_plan != '' && $t->departure_plan != '') ? get_time_difference($t->arrival_plan, $t->departure_plan) : ''?></td>
                                        <td rowspan="2"></td>
                                        <td rowspan="2"></td>
                                        <td rowspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; font-size: 8px<?php echo $bbot?>">A</td>
                                        <td style="<?php echo $bbot?>;"></td>
                                        <td style="<?php echo $bbot?>"></td>
                                        <td style="border-right: 2px solid<?php echo $bbot?>"></td>
                                    </tr>
                                    <?php  if ($z == $sisa): ?>
                                    <tr>
                                        <td style="font-weight: bold; border-left: 2px solid; font-size: 14px" rowspan="2">DOCK</td>
                                        <td style="font-weight: bold; font-size: 8px">P</td>
                                        <td></td>
                                        <td></td>
                                        <td style="border-right: 2px solid"></td>
                                        <td rowspan="2"></td>
                                        <td rowspan="2" width="5%"></td>
                                        <td rowspan="2" width="5%"></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; font-size: 8px">A</td>
                                        <td></td>
                                        <td></td>
                                        <td style="border-right: 2px solid"></td>
                                    </tr>
                                    <?php endif;?>
                                    <?php $z++; endforeach; ?>

                                    <?php for($i=0; $i<$sel_row-1; $i++): 
                                        $bbot = '';
                                        if ($i == $sel_row-2)
                                        {
                                            $bbot = "; border-bottom: 2px solid";
                                        }    
                                    ?>
                                        <tr>
                                        <td style="font-weight: bold; border-left: 2px solid; <?php echo $bbot?>" rowspan="2"></td>
                                        <td style="font-weight: bold; font-size: 8px">P</td>
                                        <td></td>
                                        <td></td>
                                        <td style="border-right: 2px solid"></td>
                                        <td rowspan="2"></td>
                                        <td rowspan="2" width="5%"></td>
                                        <td rowspan="2" width="5%"></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; font-size: 8px<?php echo $bbot?>">A</td>
                                        <td style="<?php echo $bbot?>"></td>
                                        <td style="<?php echo $bbot?>"></td>
                                        <td style="border-right: 2px solid<?php echo $bbot?>"></td>
                                    </tr>
                                    <?php endfor;?>
                                </table>
                            </td>
                            <td valign="top" style="padding-left: 10px">
                                <table width="100%">
                                    <tr>
                                        <td><img src="<?php echo base_url()?>assets/images/panah.png" width="20" /></td>
                                        <td valign="top">                                            
                                            1. Atur Ulang Barang <br/>
                                            2. Datang Awal <br/>
                                            3. Overflow <br/>
                                            <u>Tidak Ada</u><br/>
                                            4. Kanopy<br/>
                                            5. PIC
                                        </td>
                                        <td valign="top">
                                            <u>Tunggu</u> <br/>
                                            6. Forklift <br/>
                                            7. Dokumen <br/>
                                            8. Barang <br/>
                                            9. Truk <br/>
                                            10. Parkir
                                        </td>
                                        <td valign="top">
                                            <u>Masalah</u><br/>
                                            11. Truk <br/>
                                            12. Sebelumnya <br/>
                                            13. Driver
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" valign="middle" height="250" style="text-align: center; border: dotted #7a7a7a;">
                                            <h1 style="color: #000000;color: rgba(34, 62, 117, 0.5); font-size: 28px">Receipt Stamps <br/> Customer / Supplier</h1>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" valign="top">
                                            <table border="1" style="width: 100%">
                                                <tr>
                                                    <td colspan="2" class="text-center strong" style="text-align: center"><strong>TTNT</strong></td>
                                                    <td rowspan="2" class="text-center strong" style="width: 40%; text-align: center"><strong>Customer</strong></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center strong" style="width: 30%; text-align: center"><strong>Driver</strong></td>
                                                    <td class="text-center strong" style="width: 30%; text-align: center"><strong>Controller</strong></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td class="text-center strong">
                                                        <br/><br/><br/><br/>
                                                    </td>
                                                    <td class="text-center strong"></td>
                                                    <td class="text-center strong"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" valign="top">
                    <table width="100%">
                        <tr>
                            <td width="50%" style="border-top: dashed; padding-right: 20px; padding-top: 10px" valign="top">
                                <table width="100%">
                                    <tr>
                                        <td colspan="4" valign="top" style="padding-bottom: 10px">
                                            <div class="contentx">
                                                <table width="100%" class="">
                                                    <tr>
                                                        <td colspan="2" valign="top">
                                                            <p style="font-size: 10px">Print Ke <?php echo $print_driver_report?></p>
                                                            <h1 style="text-align: center">Uang Jalan</h1>
                                                        </td>
                                                        <td colspan="2" style="text-align: right">
                                                            <img src="<?php echo site_url('rollcall/barcode/' . $rollcall->voucher)?>" height="50" />
                                                            <br/><br/>
                                                        </td>                                                                                
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 10px">Tgl</td>
                                                        <td style="font-size: 10px"><?php echo date('d-F Y', strtotime($rollcall->timetable_dt))?></td>
                                                        <td style="font-size: 10px">Driver</td>
                                                        <td style="font-size: 10px">
                                                        <?php echo $rollcall->driver_name; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 10px">Route</td>
                                                        <td style="font-size: 10px"><?php echo $rollcall->route?></td>
                                                        <td style="font-size: 10px">Truk</td>
                                                        <td style="font-size: 10px">
                                                        <?php  echo $rollcall->vehicle_cd; ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 10px">Cycle</td>
                                                        <td style="font-size: 10px"><?php echo $rollcall->cycle?></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 14px; font-weight: bold">KM</td>
                                                        <td style="font-size: 14px; font-weight: bold" width="30%">Awal</td>
                                                        <td style="font-size: 14px; font-weight: bold" width="30%" colspan="2">Akhir</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <table id="tbl2" width="100%">
                                                                <tr>
                                                                    <td style="font-size: 14px; font-weight: bold; border-top: none; border-left: none" ><?php echo $rollcall->customer?></td>
                                                                    <td style="font-size: 14px; font-weight: bold" >Plan</td>
                                                                    <td style="font-size: 14px; font-weight: bold"  width="20%">Actual</td>
                                                                    <td style="font-size: 14px; font-weight: bold"  width="20%">Balance</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right; font-size: 12px; font-weight: bold">
                                                                        Solar<br/>                                                                    
                                                                        SPBU<br/>
                                                                        <span style="font-weight: 10px; font-weight: normal">Kode Voucher</span>
                                                                    </td>
                                                                    <td style="text-align: right; font-size: 12px; font-weight: bold">
                                                                        <?php echo $rollcall->cashier_fuel_plan?> L<br/>                                                                    
                                                                        <?php echo $rollcall->cashier_etoll_spbu?><br/>                                                                        
                                                                        <span style="font-weight: 10px; font-weight: normal"><?php echo substr($rollcall->voucher,6,6)?></span>
                                                                    </td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right;">
                                                                        <span style="font-weight: bold; font-size: 12px">Toll</span><br/>
                                                                        No. Kartu<br/>
                                                                        Saldo Awal
                                                                    </td>
                                                                    <td style="text-align: right">
                                                                        <span style="font-weight: bold; font-size: 12px">
                                                                        Rp. <?php echo number_format($rollcall->cashier_etoll_plan, 0, ',', '.')?><br/>
                                                                        </span>
                                                                        <?php echo $rollcall->cashier_etoll_cardno?><br/>
                                                                        Rp. <?php echo number_format($rollcall->cashier_etoll_amount_start, 0, ',', '.')?>
                                                                    </td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right">
                                                                        <span style="font-weight: bold; font-size: 12px">
                                                                        Other
                                                                        </span>
                                                                    </td>
                                                                    <td style="text-align: right">
                                                                        <span style="font-weight: bold; font-size: 12px">
                                                                        Rp. <?php echo number_format($rollcall->cashier_others_plan, 0, ',', '.')?>
                                                                        </span>
                                                                    </td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right">
                                                                        <h3>
                                                                        Total
                                                                        </h3>                                                        
                                                                    </td>
                                                                    <td style="text-align: right">
                                                                        <h3>
                                                                        Rp. <?php echo number_format($rollcall->cashier_etoll_plan + $rollcall->cashier_others_plan, 0, ',', '.')?>
                                                                        </h3>
                                                                    </td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" valign="top">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td valign="top" width="20%" style="text-align: center">
                                                                    <img src="<?php echo base_url()?>assets/images/warning.png" width="30" height="30" />
                                                                    </td>
                                                                    <td valign="top">
                                                                        - Aplikasi harus dikumpulkan setelah selesai route. <br/>
                                                                        - Jika ada sisa uang, harap dikembalikan.
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" valign="top">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td valign="top" width="40%" style="text-align: center; border: dotted #7a7a7a">
                                                                    <strong><u>Tempel struk toll & solar disini</u> </strong>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <table border="1" style="width: 100%">
                                                                            <tr>
                                                                                <td class="text-center strong" style="width: 30%; text-align: center"><strong>Driver</strong></td>
                                                                                <td class="text-center strong" style="width: 40%; text-align: center"><strong>Controller</strong></td>
                                                                                <td class="text-center strong" style="width: 30%; text-align: center"><strong>Casher</strong></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-center strong">
                                                                                    <br/><br/>
                                                                                </td>
                                                                                <td class="text-center strong"></td>
                                                                                <td class="text-center strong"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>                                            
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td colspan="4" valign="top" style="border-top: dashed; padding-top: 10px">
                                            <div class="content">
                                                <p>TTLC Nasmoco Transport.TTLC Nasmoco Transport.-<?php echo $rollcall->voucher?>-TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. TTLC Nasmoco Transport. </p>                                                
                                                <table width="100%">
                                                    <tr>
                                                        <td colspan="2" valign="top">
                                                        <span style="font-size: 10px">Print Ke <?php echo $print_driver_report?></span>
                                                        <h1  style="text-align: left">* Voucher</h1></td>
                                                        <td colspan="2" style="text-align: right">
                                                            <img src="<?php echo site_url('rollcall/barcode/' . substr($rollcall->voucher,6,6))?>" height="50" />                                                        
                                                            <br/><br/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 12px; padding-left: 10px; font-weight:bold">Tgl</td>
                                                        <td style="font-size: 12px; font-weight:bold"><?php echo date('d-F Y', strtotime($rollcall->timetable_dt))?></td>                                                    
                                                        <td rowspan="2" style="text-align: center">
                                                            <h1><?php echo $rollcall->cashier_fuel_plan?>L <br/>Solar</h1>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 12px; padding-left: 10px; font-weight:bold">Route</td>
                                                        <td style="font-size: 12px; font-weight:bold"><?php echo $rollcall->route?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 12px; padding-left: 10px; font-weight:bold">Cycle</td>
                                                        <td style="font-size: 12px; font-weight:bold"><?php echo $rollcall->cycle?></td>
                                                        <td rowspan="3" style="text-align: center">
                                                                <table border="1" style="width: 100%">
                                                                    <tr>
                                                                        <td class="text-center strong" style="width: 30%; text-align: center;"><strong style=" font-size: 14px">Petugas SPBU</strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-center strong">
                                                                            <br/><br/>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 12px; padding-left: 10px; font-weight:bold">Driver</td>
                                                        <td style="font-size: 12px; font-weight:bold"><?php echo $rollcall->driver_name?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 12px; padding-left: 10px; font-weight:bold">No.Polisi</td>
                                                        <td style="font-size: 12px; font-weight:bold"><?php echo $rollcall->vehicle_number?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="border-top: dashed; padding-left: 10px; border-left: dashed; padding-top: 10px">
                                <table width="100%">
                                    <tr>
                                        <td colspan="4" style="text-align: center"><h1>Overtime</h1></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px; padding-left: 10px;">Tgl.</td>
                                        <td style="font-size: 12px;"><?php echo date('d-F Y', strtotime($rollcall->departure_dt))?></td>
                                        <td style="font-size: 12px;">Driver</td>
                                        <td style="font-size: 12px;"><?php echo $rollcall->driver_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px; padding-left: 10px;">Route</td>
                                        <td style="font-size: 12px;"><?php echo $rollcall->route?></td>
                                        <td style="font-size: 12px;">NIK</td>
                                        <td style="font-size: 12px;"><?php  echo $rollcall->driver_cd; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px; padding-left: 10px;">Cycle</td>
                                        <td style="font-size: 12px;"><?php echo $rollcall->cycle?></td>
                                        <td style="font-size: 12px;">Truk</td>
                                        <td style="font-size: 12px;"><?php  echo $rollcall->vehicle_cd; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 12px; padding-left: 10px;">Hari</td>
                                        <td style="font-size: 12px;"><input type="checkbox" /> Libur</td>
                                        <td style="font-size: 12px;"><input type="checkbox" /> Kerja</td>
                                        <td><input type="checkbox" /> Pengganti</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="padding-left: 10px">
                                            <br/>
                                            <table id="tbl2" width="100%">
                                                <tr>
                                                    <td width="20%"><h3><?php echo $rollcall->customer?></h3></td>
                                                    <td><h3>IN</h3></td>
                                                    <td><h3>OUT</h3></td>
                                                    <td><h3>OVER TIME</h3></td>
                                                </tr>
                                                <tr>
                                                    <td><h3>PLAN</h3></td>
                                                    <td><h3><?php echo substr($rollcall->departure_plan,0,5)?></h3></td>
                                                    <td><h3><?php echo substr($rollcall->arrival_plan,0,5)?></h3></td>
                                                    <td>
                                                        <?php 
                                                            // 23-Juni-2020
                                                            // Menghitung Overtime (9 Jam);
                                                            // cari selisihnya brp lama
                                                            $startdt    = strtotime($rollcall->departure_dt . ' ' . $rollcall->departure_plan); // or your date as well
                                                            $enddt      = strtotime($rollcall->departure_dt . ' ' . $rollcall->arrival_plan);
                                                            $datediff = $enddt - $startdt;

                                                            $selisihJam = $datediff / 60 / 60;
                                                            $ot = '-';
                                                            if ($selisihJam > 9)
                                                            {
                                                                $total_selisih = date("H:i", strtotime("1980-01-01 00:00:00") + $datediff);
                                                                
                                                                $ts1 = date_create('1980-01-01 ' . $total_selisih . ':00');
                                                                $ts2 = date_create('1980-01-01 09:00:00');
                                                                $interval = date_diff($ts1, $ts2);
                                                                $ot = $interval->format('%H:%i');                                                                                                                                
                                                            }
                                                        ?>
                                                    
                                                        <h3>
                                                            <?php 
                                                            // echo ($rollcall->arrival_plan != '' && $rollcall->departure_plan != '') ? get_time_difference($rollcall->departure_plan, $rollcall->arrival_plan) : ''
                                                            echo $ot;                                                           
                                                            ?>
                                                        </h3>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><h3>ACTUAL</h3></td>
                                                    <td><h3><?php echo substr($rollcall->departure_actual,0,5)?></h3></td>
                                                    <td><h3></h3></td>
                                                    <td><h3></h3></td>
                                                </tr>
                                            </table>
                                            <br/>
                                            <h3>Alasan Overtime</h3>
                                            <table id="tbl2" width="100%">
                                                <tr>
                                                    <td width="20%"><h3>Jam</h3></td>
                                                    <td colspan="3"><h3>Alasan</h3></td>
                                                </tr>
                                                <tr>
                                                    <td>1.)</td>
                                                    <td colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td>2.)</td>
                                                    <td colspan="3"></td>
                                                </tr>                                                
                                            </table>
                                            <table width="100%">                                                
                                                <tr>
                                                    <td colspan="3"style="font-size: 14px; font-weight: bold">Alasan List</td>                                                    
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <u>Tunggu</u><br/>
                                                        1. Forklift<br/>
                                                        2. Dokumen<br/>
                                                        3. Barang/Empty Box<br/>
                                                        4. Truk<br/>
                                                        5. Parkir<br/>                                                        
                                                    </td>
                                                    <td>
                                                        <u>Masalah</u><br/>
                                                        6. Truk<br/>
                                                        7. Sebelumnya<br/>
                                                        8. Atur Ulang Barang<br/>
                                                        <u>Tidak Ada</u><br/>
                                                        9. Canopy<br/>
                                                        10. PIC<br/>
                                                    </td>
                                                    <td>
                                                        11. Datang Awal<br/>
                                                        12. Nerus Route<br/>
                                                        13. Lain-lain
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="20%" style="text-align: center">
                                                    <img src="<?php echo base_url()?>assets/images/warning.png" width="30" height="30" />
                                                    </td>
                                                    <td valign="top" colspan="2">
                                                        - Aplikasi harus dikumpulkan setelah selesai route. <br/>
                                                        - <i><strong>Maksimum form ini H+2 sampai ke HRD</strong></i><br/><br/>
                                                        <strong>Terimakasih telah jujur dalam pengisian aplikasi</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <table border="1" style="width: 100%">
                                                            <tr>
                                                                <td class="text-center strong" style="width: 25%; text-align: center"><strong>Driver</strong></td>
                                                                <td class="text-center strong" style="width: 25%; text-align: center"><strong>Controller</strong></td>
                                                                <td class="text-center strong" style="width: 25%; text-align: center"><strong>GPS PIC</strong></td>
                                                                <td class="text-center strong" style="width: 25%; text-align: center"><strong>HRD</strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-center strong">
                                                                    <br/><br/><br/><br/>
                                                                </td>
                                                                <td class="text-center strong"></td>
                                                                <td class="text-center strong"></td>
                                                                <td class="text-center strong"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
        <script type="text/javascript">
        $(document).ready(function(){  
            setTimeout(function () { window.print(); }, 500);
            window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }
        });


        </script>
    </body>
</html>