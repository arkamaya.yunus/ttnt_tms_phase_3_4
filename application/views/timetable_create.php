<style>
	.bg-red{
            background-color: #f8c5c5 !important;
	}
	.bg-yellow{
		background-color: #f5ec91 !important;
	}
	.table-responsive
	{
		border: none;
	}
</style>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('timetable')?>">Timetable</a>
					</li>
					<li class="active">Create</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	
	<div class="row">		

		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
				<input type="hidden" name="logistic_partner" id="logistic_partner" value="TTNT" />
				<input type="hidden" id="process_id" name="process_id" value="" />
				<input type="hidden" id="file_nm" name="file_nm" value="" required>

				<div class="card-box">
					<div class="row step step-0">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Business <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
									<select name="business" id="business" class="form-control select2" required>
										<option value=""></option>
										<?php foreach ($business as $b): ?>
										<option value="<?php echo $b->system_code?>"><?php echo $b->system_value_txt?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Customer <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
									<select name="customer" id="customer" class="form-control select2" required>
										<option value=""></option>
										<?php foreach ($customer as $c): ?>
										<option value="<?php echo $c->system_code?>"><?php echo $c->system_code . ' - ' . $c->system_value_txt?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Customer Logistic Point <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
									<select name="customer_lp_cd" id="customer_lp_cd" class="form-control select2" required>
										<option value=""></option>
										<?php foreach ($customer_lp_cds as $customer_lp_cd): ?>
										<option value="<?php echo $customer_lp_cd->lp_cd?>"><?php echo $customer_lp_cd->lp_cd . ' - ' . $customer_lp_cd->lp_name?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
                            <div class="form-group">
								<label class="col-md-4 col-sm-3 control-label">File <span class="text-danger">*</span></label>
								<div class="col-md-8 col-sm-9">
								<input class="filestyle" data-size="sm" data-iconname="fa fa-cloud-upload" id="file_attach" name="file_attach" accept=".xlsx" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" tabindex="-1" required onchange= "checkextension();"type="file">
									<a href="<?php echo base_url().'assets/files/template-timetable.xlsx'?>" style="font-size: 11px;" target="_blank" style="font-size: 11px;">Download Template</a>
								</div>
							</div>
						</div>
						<div class="col-sm-6">  														
							<div class="form-group">
								<label class="col-md-4 control-label">Planning Date's <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
                                    <!-- <input type="text" id="timetable_dts" name="timetable_dts" class="form-control datepickerm" required /> -->
									<div id="timetable_dts_inline" style="margin: 10px auto" class="datepickerm" data-date-format="DD/MM/YYYY"></div>
									<input type="hidden" id="timetable_dts" name="timetable_dts" value="" class="form-control" />
								</div>
                            </div>                          
						</div>
					</div>

					<div class="row step step-1 step-2">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Business</label>
								<div class="col-md-8 m-t-5" id="step-1-business">
									
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Customer</label>
								<div class="col-md-8 m-t-5" id="step-1-customer">
									
								</div>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Customer Logistic Point </label>
								<div class="col-md-8 m-t-5" id="step-1-customer_lp_cd">
									
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Planning Date's</label>
								<div class="col-md-8 m-t-5" id="step-1-timetable_dts">
                                    
								</div>
                            </div> 
						</div>
					</div>

					<div class="row step step-1 step-2">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="col-md-12 col-sm-12 control-label text-danger">Upload Data Confirmation (Please confirm before Process)</label>
							</div>
							<div class="table-responsive m-b-5">
								<table id="datatable" class="table table-striped table-hover display nowrap">
									<thead>
										<tr>
											<th></th>
											<th class="text-center">Status Data</th>
											<th>Route</th>
											<th>Cycle</th>
											<th>Truck</th>
											<th>Driver</th>
											<th>KM</th>
											<th>Fuel (Liter)</th>
											<th>SPBU</th>
											<th>Tol (Rp)</th>
											<th>Others (Rp)</th>
											<th>Pallet (Pcs)</th>
											<th>Rack (Pcs)</th>
											<th>Division</th>
											<th>Arrival Next Day</th>
											<th>Arrival Plan</th>
											<th>Departure Next Day</th>
											<th>Departure Plan</th>
										</tr>										
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('timetable')?>'">Back</button>
						<?php echo create_button($this->button, "btn_submit"); ?>
						<?php echo create_button($this->button, "btn_restart"); ?>
						<?php echo create_button($this->button, "btn_restart2"); ?>
						<?php echo create_button($this->button, "btn_process"); ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>