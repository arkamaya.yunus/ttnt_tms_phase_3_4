<!DOCTYPE html>
<html class="account-pages-bg">
    <head>        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo APP_NAME ?>">
        <meta name="author" content="irfan.satriadarma@gmail.com">

        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.ico">
        <title><?php echo APP_NAME ?> - Aktivasi Akun</title>

        <!-- App css -->
        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <style>
            .account-content{
                background-color: #fff;
            }
            .account-logo-box {
                padding: 20px 10px 5px 10px; 
            }
            .account-pages .account-content {
                padding: 20px;
            }
            h2 {
                font-family: "Varela Round",sans-serif;
            }
            label {
                font-weight: normal;
            }
        </style>

        <script>
            var SITE_URL = '<?php echo base_url(); ?>';
        </script>
    </head>


    <body class="bg-transparent">

        <section>
            <div class="container-alt">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrapper-page">

                            <div class="m-t-50 account-pages">
                                <div class="account-content">
                                    
                                    <?php if ($result == "Success") { ?>
                                        <div class="text-center m-b-20">
                                            <div class="m-b-20">
                                                <div class="checkmark">
                                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 161.2 161.2" enable-background="new 0 0 161.2 161.2" xml:space="preserve">
                                                    <path class="path" fill="none" stroke="#4bd396" stroke-miterlimit="10" d="M425.9,52.1L425.9,52.1c-2.2-2.6-6-2.6-8.3-0.1l-42.7,46.2l-14.3-16.4
                                                          c-2.3-2.7-6.2-2.7-8.6-0.1c-1.9,2.1-2,5.6-0.1,7.7l17.6,20.3c0.2,0.3,0.4,0.6,0.6,0.9c1.8,2,4.4,2.5,6.6,1.4c0.7-0.3,1.4-0.8,2-1.5
                                                          c0.3-0.3,0.5-0.6,0.7-0.9l46.3-50.1C427.7,57.5,427.7,54.2,425.9,52.1z"/>
                                                    <circle class="path" fill="none" stroke="#4bd396" stroke-width="4" stroke-miterlimit="10" cx="80.6" cy="80.6" r="62.1"/>
                                                    <polyline class="path" fill="none" stroke="#4bd396" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="113,52.8
                                                              74.1,108.4 48.2,86.4 "/>

                                                    <circle class="spin" fill="none" stroke="#4bd396" stroke-width="4" stroke-miterlimit="10" stroke-dasharray="12.2175,12.2175" cx="80.6" cy="80.6" r="73.9"/>

                                                    </svg>
                                                </div>
                                            </div>
                                            <h3>Berhasil !</h3>
                                            <p class="text-muted font-13 m-t-10"> Aktivasi berhasil <b class="text-primary"><?php echo $email; ?></b>. Sekarang anda bisa menggunakan Aplikasi <strong class="text-dark"> <?php echo APP_NAME ?></strong>. </p>
                                            <p class="text-muted font-13 m-t-10"> Halaman akan berpindah otomatis. Mohon tunggu... </p>
                                        </div>
                                    <?php } else { ?>
                                        <div class="text-center m-b-20">
                                            <div class="m-b-20">
                                                <div class="checkmark">
                                                    <h2><i class="fa fa-frown-o fa-5x text-warning"></i></h2>
                                                </div>
                                            </div>

                                            <p class="text-muted font-13 m-t-10"> <?php echo $message; ?></p>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>

                            <?php if ($result == "Success") { ?>
                            <div class="row m-t-30">
                                <div class="col-sm-12 text-center">
                                    <p class="text-muted"><a href="<?php echo base_url(); ?>login" class="text-primary m-l-5"><b>Klik disini</b></a> untuk masuk ke aplikasi</p>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/detect.js"></script>
        <script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url() ?>assets/js/waves.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/plugins/jquery-validate/jquery.validate.js"></script>        
        <script src="<?php echo base_url() ?>assets/js/jquery.core.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.app.js"></script>
		<script src="<?php echo base_url() ?>assets/js/apps/register.js"></script>
        
		 <script>
            $(function()
			{				
				setTimeout(function () 
				{
                    <?php $url = ($result == "Success") ? 'company' : 'login';?>
				   window.location.href = "<?php echo site_url($url)?>"; //will redirect to dashboard
				}, 2000); //will call the function after 2 secs.				
			});
        </script>
    </body>
</html>