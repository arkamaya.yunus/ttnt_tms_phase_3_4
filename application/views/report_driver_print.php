<html>
<head>
    <title>Driver Report</title>
    <style>
        *
        {
            font-family: sans-serif;
            font-size: 10px;
        }
        table
        {
            border-spacing: 0;
            border-collapse: collapse; 
        }

        #tbl-1 tr td
        {
            border-bottom: 1px solid #000000;                     
        }
        #tbl-2 tr td
        {
            border: 1px solid #000000;            
        }
        #tbl-3 tr td
        {
            border: 1px solid #000000;
            text-align: center;
        }
        #tbl-4 tr td
        {
            border: 1px solid #000000;
        }
        .text-center
        {
            text-align: center;
        }
        .wing {
            background-color: #000000 !important;
            -webkit-print-color-adjust: exact; 
            color: white !important;
            padding: 10px 20px;
            font-size: 20px;font-weight: bold;
        }
        .wing-lite
        {
            background-color: #000000 !important;
            -webkit-print-color-adjust: exact; 
            color: white !important;
            padding: 5px 10px;
            font-size: 14px !important;
            font-weight: bold;
            font-style: italic;
        }
        .lr span
        {
            writing-mode: sideways-lr;
            -webkit-writing-mode: sideways-lr;
            -ms-writing-mode: sideways-lr;
        }
        #alasan td
        {
            width: 50px !important;
        }
        .dock
        {
            background-color: #000000 !important;
            -webkit-print-color-adjust: exact; 
            color: white !important;
        }
        .strong
        {
            font-weight: bold !important;
        }
        @media print {
            .wing {
                background-color: #000000 !important;
                -webkit-print-color-adjust: exact; 
                color: white !important;
                padding: 10px 20px;
                font-size: 20px;font-weight: bold;
            }
            .wing-lite
            {
                background-color: #000000 !important;
                -webkit-print-color-adjust: exact; 
                color: white !important;
                padding: 5px 10px;
                font-size: 14px !important;font-weight: bold;font-style: italic;
            }
            .dock
            {
                background-color: #000000 !important;
                -webkit-print-color-adjust: exact; 
                color: white !important;
            }
        }
    </style>
</head>
    <body>
        <table border='0' width="100%">
            <thead>
            <tr>
                <td colspan="3" class="text-center"><h3><?php echo $timetable[0]->customer?> Driver Report *<?php echo $timetable[0]->business?>*</h3></td>
            </tr>
            <tr>
                <td style="width: 20%">
                    <table id="tbl-1">
                        <tr>
                            <td class="strong">Tanggal</td>
                            <td class="strong">:</td>
                            <td style="width: 100px"><?php echo date('d-F Y', strtotime($timetable[0]->timetable_dt))?></td>
                        </tr>
                        <tr>
                            <td class="strong">Route</td>
                            <td class="strong">:</td>
                            <td><?php echo $timetable[0]->route?></td>
                        </tr>
                        <tr>
                            <td class="strong">Ret (Cycle)</td>
                            <td class="strong">:</td>
                            <td><?php echo $timetable[0]->cycle?></td>
                        </tr>
                        <tr>
                            <td class="strong">Nama Driver</td>
                            <td class="strong">:</td>
                            <td>
                                <?php 
                                $dn = '';
                                foreach ($timetable as $t):
                                    if ($dn != $t->driver_name)
                                    {
                                        $dn = $t->driver_name;
                                        echo $dn . ',';
                                    }
                                ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="strong" colspan="3">P<?php echo date('m', strtotime($timetable[0]->timetable_dt)) . '-' . date('Y', strtotime($timetable[0]->timetable_dt)) ?></td>
                        </tr>   
                    </table>
                </td>
                <td class="text-center">
                    <span class="wing">JANGAN LUPA TUTUP WING !!!</span>
                </td>
                <td style="width: 40%">
                <table id="tbl-2">
                        <tr>
                            <td class="strong">NO BODY TRUK</td>
                            <td class="strong">:</td>
                            <td colspan="2">
                            <?php 
                                $dn = '';
                                foreach ($timetable as $t):
                                    if ($dn != $t->vehicle_cd)
                                    {
                                        $dn = $t->vehicle_cd;
                                        echo $dn . ',';
                                    }
                                ?>
                                <?php endforeach; ?>
                            </td>
                            <td class="strong">HELM</td>
                            <td class="strong">:</td>
                            <td class="strong text-center" style="width: 40px">OK</td>
                            <td class="strong text-center" style="width: 40px">TDK</td>
                        </tr>
                        <tr>
                            <td class="strong">PONSEL</td>
                            <td class="strong">:</td>
                            <td colspan="2">
                            <?php 
                                $dn = '';
                                foreach ($timetable as $t):
                                    if ($dn != $t->smartphone && $t->smartphone != '')
                                    {
                                        $dn = $t->smartphone;
                                        echo $dn . ',';
                                    }
                                ?>
                                <?php endforeach; ?>
                            </td>
                            <td class="strong">TRIPLEK</td>
                            <td class="strong">:</td>
                            <td class="strong text-center" >OK</td>
                            <td class="strong text-center" >TDK</td>
                        </tr>
                        <tr>
                            <td class="strong">BASKET</td>
                            <td class="strong">:</td>
                            <td colspan="2"></td>
                            <td class="strong">LASHING</td>
                            <td class="strong">:</td>
                            <td class="strong text-center" >OK</td>
                            <td class="strong text-center" >TDK</td>
                        </tr>
                        <tr>
                            <td class="strong">DOKUMEN TRUK</td>
                            <td class="strong">:</td>
                            <td class="strong text-center" style="width: 60px">OK</td>
                            <td class="strong text-center" style="width: 60px">TDK</td>
                            <td class="strong">TAMBANG</td>
                            <td class="strong">:</td>
                            <td class="strong text-center" >OK</td>
                            <td class="strong text-center" >TDK</td>
                        </tr>
                    </table>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="3" class="text-center">
                    <table id="tbl-3" style="width: 100%">
                        <tr>
                            <td class="strong" rowspan="3" style="width: 100px">LAPORAN HARIAN DRIVER<br/><?php echo $timetable[0]->customer . ' ' . $timetable[0]->business?></td>
                            <td class="strong" rowspan="3" style="width: 50px">&nbsp;</td>
                            <td class="strong" rowspan="3" style="width: 50px">Waktu Kedatangan</td>
                            <td class="strong" rowspan="3" style="width: 50px">Waktu Keberangkatan</td>
                            <td class="strong" rowspan="3" style="width: 50px">Loading / Unloading Time (Minute)</td>
                            <td class="strong" colspan="14">Problem</td>
                            <td class="strong" rowspan="3">Remarks</td>
                            <td class="strong" rowspan="3" style="width: 200px">PARAF & NAMA SUPPLIER</td>
                        </tr>
                        <tr id="alasan">
                            <td class="strong">Macet</td>
                            <td class="strong">Masalah Truck</td>
                            <td class="strong">Part Tidak Siap</td>
                            <td class="strong">Menunggu Forklift</td>
                            <td class="strong">Overflow</td>
                            <td class="strong">Menunggu Document</td>
                            <td class="strong">Atur Ulang Part</td>
                            <td class="strong">Tidak Ada PIC</td>
                            <td class="strong">Kecelakaan Part</td>
                            <td class="strong">Masalah Supplier / Dock Sebelumnya</td>
                            <td class="strong">Datang Lebih Awal</td>
                            <td class="strong">Tidak ada cannopy</td>
                            <td class="strong">Other's</td>
                            <td class="strong">Tunggu Parkir</td>                            
                        </tr>
                        <tr>
                            <?php for($i=1; $i<=14; $i++):?>
                            <td><?php echo $i?></td>
                            <?php endfor;?>
                        </tr>
                        <?php 
                            $z=1; 
                            $sisa = count($timetable) - 1;
                        foreach ($timetable as $t):?>
                        <tr>
                            <td class="strong" rowspan="2"><?php echo $t->lp_cd ?></td>
                            <td>Plan</td>
                            <td><?php echo $t->arrival_plan?></td>
                            <td><?php echo $t->departure_plan?></td>
                            <td><?php echo ($t->arrival_plan != '' && $t->departure_plan != '') ? get_time_difference($t->departure_plan, $t->arrival_plan) : ''?></td>
                            <?php for($i=1; $i<=16; $i++):?>
                            <td rowspan="2"></td>
                            <?php endfor;?>
                        </tr>
                        <tr>
                            <td>Act</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php  if ($z == $sisa): ?>
                        <tr>
                            <td rowspan="2" class="dock">DOCK</td>
                            <td>Plan</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <?php for($i=1; $i<=16; $i++):?>
                            <td rowspan="2"></td>
                            <?php endfor;?>
                        </tr>
                        <tr>
                            <td>Act</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php endif; ?>                        
                    
                        <?php $z++; endforeach; ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    <table id="tbl-4" style="width: 100%">
                        <tr>
                            <td class="dock" style="width: 100px">MENERIMA</td>
                            <td class="dock text-center" style="width: 50px"><?php echo $timetable[0]->cycle?></td>
                            <td rowspan="2" class="strong" style="width: 50px">KM AWAL</td>
                            <td rowspan="2"></td>
                        </tr>
                        <tr>
                            <td>1. Empty Box</td>
                            <td rowspan="4" class="text-center">TTLC</td>                            
                        </tr>
                        <tr>
                            <td>2. Manifest</td>
                            <td colspan="2"></td>                            
                        </tr>
                        <tr>
                            <td>3. Skid Label</td>
                            <td rowspan="2" class="strong" style="width: 50px">KM AKHIR</td>
                            <td rowspan="2"></td>
                        </tr>
                        <tr>
                            <td>4. Kanban</td>                            
                        </tr>
                        <tr>
                            <td colspan="2" class="strong">Jenis Bahan Bakar</td>
                            <td class="strong">Waktu</td>
                            <td class="strong">Liter</td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table border="1" style="width: 100%">
                        <tr>
                            <td class="text-center wing-lite">JANGAN LUPA SCAN GATE</td>
                        </tr>
                        <tr>
                            <td class="text-center">
                            <br/><br/>
                                PERHATIAN !!! <br/>
                                JUMLAH BOX / RAK KOSONG YANG NAIK = <br/>
                                JUMLAH BOX / RAK  YANG TURUN
                                <br/><br/>
                            </td>
                        </tr>
                        <tr>
                            <td class="wing-lite text-center">UTAMAKAN INFORMASI JIKA TERJADI ABNORMAL !!!</td>
                        </tr>
                    </table>
                </td>
                <td style="width: 50px">
                    <table border="1" style="width: 100%">
                        <tr>
                            <td class="text-center strong" colspan="2">TTNT</td>
                            <td class="text-center strong"><?php echo $timetable[0]->customer?></td>
                            <td class="text-center strong">FIELDMAN</td>
                        </tr>
                        <tr>
                            <td class="text-center strong" style="width: 25%">DRIVER</td>
                            <td class="text-center strong" style="width: 25%">CONTROLLER</td>
                            <td class="text-center strong" style="width: 25%">KANBAN ROOM</td>
                            <td class="text-center strong" style="width: 25%">TTNT</td>
                        </tr>
                        <tr>
                            <td class="text-center strong">
                                <br/><br/><br/><br/>
                            </td>
                            <td class="text-center strong"></td>
                            <td class="text-center strong"></td>
                            <td class="text-center strong"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
        <script type="text/javascript">
        $(document).ready(function(){  
            setTimeout(function () { window.print(); }, 500);
            window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }
        });


        </script>
    </body>
</html>