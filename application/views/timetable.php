<style>
table#datatable.dataTable tbody tr:hover {
	background-color: #d8e2f0;cursor: pointer;
}

table#datatable.dataTable tbody tr:hover > .sorting_1 {
	background-color: #d8e2f0;cursor: pointer;
}
</style>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li class="active"><?php echo $stitle?></li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>	
				
	<div class="row m-b-10">
		<?php if ($this->session->flashdata('notif_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
			</div>
		</div>
		<?php endif; ?>
        <?php if ($this->session->flashdata('notif_danger') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_danger')?>
			</div>
		</div>
		<?php endif; ?>
	
		<div class="col-sm-2">			
			<?php echo create_button($this->button, "btn_add"); ?>
		</div>

        <div class="col-sm-10">
			<div class="card-box">
				<div class="row">
					<div class="col-md-12">						
						<div class="col-md-4">
							<select id="business" class="form-control select2">                    					
								<option value="all">All Business</option>
								<?php foreach ($business as $b): ?>
								<option value="<?php echo $b->system_code?>"><?php echo $b->system_value_txt?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-md-4">
							<select id="customer" class="form-control select2">
								<option value="all">All Customer</option>
								<?php foreach ($customer as $c): ?>
								<option value="<?php echo $c->system_code?>"><?php echo $c->system_code . ' - ' . $c->system_value_txt?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="col-md-4">
							<select id="customer_lp_cd" class="form-control select2">                    					
								<option value="all">All Customer Logistic Point</option>
								<?php foreach ($customer_lp_cds as $customer_lp_cd): ?>
								<option value="<?php echo $customer_lp_cd->lp_cd?>"><?php echo $customer_lp_cd->lp_cd?></option>
								<?php endforeach; ?>
							</select>
							<input type="hidden" name="logistic_partner" id="logistic_partner" value="all" />
						</div>
					</div>
				</div>
				<div class="row m-t-10">
					<div class="col-md-12">
						<div class="col-md-2">
							<input type="text" class="text-center form-control datepicker" placeholder="Date Start" id="timetable_dt_s" name="timetable_dt_s" value="<?php echo $timetable_dt_s?>" >
						</div>            
						<div class="col-md-2">
							<input type="text" class="text-center form-control datepicker" placeholder="Date End" id="timetable_dt_e" name="timetable_dt_e" value="<?php echo $timetable_dt_e?>" >
						</div>
						<div class="col-md-1">
							<button id="btn_filter" type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="doFilter()">Filter</button>
						</div>
					</div>
				</div>
			</div>			
        </div>
	</div>
	
	<div class="row">		
		<div class="col-sm-12">
			<div class="card-box table-responsive">
				<table id="datatable" class="table table-striped table-hover display nowrap">
					<thead>
					<tr>
						<th></th>
						<th>Business</th>
						<th>Customer</th>
						<th>Customer Logistic Point</th>						
						<th>Planning Date</th>						
                        <th>Created By</th>
						<th></th>						
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>			
</div>