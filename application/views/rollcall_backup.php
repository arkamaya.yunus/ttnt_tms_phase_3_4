<style>
    #datatable tr th, 
    #datatable tr td 
    {
        font-size: 12px !important
    }

	#datatable tr th
	{
		background-color: #f2f6fc !important;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Transaction</li>
					<li class="active">
						<a href="<?php echo site_url('rollcall')?>">Rollcall</a>
					</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->

    <div class="row">
		<?php if ($this->session->flashdata('notif_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
			</div>
		</div>
		<?php endif; ?>
		<?php if ($this->session->flashdata('notif_error') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_error')?>
			</div>
		</div>
		<?php endif; ?>

		<div id="myChange" class="modal fade" role="dialog" aria-labelledby="myChangeLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="myChangeLabel">Change Truck or Driver</h4>
						<input type="hidden" id="change_timetable_dt" value="" />
						<input type="hidden" id="change_w" value="" />
					</div>
					<div class="modal-body form-horizontal">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-md-5 control-label">Route</label>
									<div class="col-md-7 m-t-5" id="change_route"></div>
								</div>
								<div class="form-group">
									<label class="col-md-5 control-label">Cycle</label>
									<div class="col-md-7 m-t-5" id="change_cycle"></div>
								</div>
								<div class="form-group" id="change_w_truck">
									<label class="col-md-5 control-label">Truck Number</label>
									<div class="col-md-7 m-t-5">
										<select id="change_vehicle_cd" class="form-control select2">                    					
											<?php foreach ($vehicles as $v): ?>
											<option value="<?php echo $v->vehicle_cd?>"><?php echo $v->vehicle_cd?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="form-group" id="change_w_driver">
									<label class="col-md-5 control-label">Driver</label>
									<div class="col-md-7 m-t-5">
										<select id="change_driver_cd" class="form-control select2">                    					
											<?php foreach ($drivers as $d): ?>
											<option value="<?php echo $d->driver_cd?>"><?php echo $d->driver_name?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
							</div>							
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>						
						<button type="button" class="btn btn-primary waves-effect waves-light" onclick="doSaveChangeTW()" id="btn_SaveTW">Save</button>
					</div>
				</div>
			</div>
		</div>

        <div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<div class="card-box" style="padding: 10px !important">
					<div class="row">
						<div class="col-md-2">                                                
							<div class="form-group">
								<label class="col-md-12 control-label">Date</label>
								<div class="col-md-12 m-t-5">
                                    <input type="text" class="form-control datepicker f1" id="timetable_dt" name="timetable_dt" value="<?php echo date('d/m/Y', strtotime($timetable_dt))?>" onchange="changeTimetableDt()" />
								</div>
							</div>
                        </div>
						<div class="col-md-4">                                                
							<div class="form-group">
								<label class="col-md-12 control-label">Driver</label>
								<div class="col-md-12 m-t-5">
                                    <select id="driver_cd" name="driver_cd" class="form-control select2">
									<option value="all">All</option>
                                    <?php foreach ($driver as $d): ?>
                                    <option value="<?php echo $d->driver_cd?>"><?php echo $d->driver_name . ' - ' . $d->driver_cd?></option>
                                    <?php endforeach; ?>
                                    </select>
								</div>
							</div>
                        </div>
                        <div class="col-md-2">                                                
							<div class="form-group">
								<label class="col-md-12 control-label">Route</label>
								<div class="col-md-12 m-t-5">
                                    <select id="route" name="route" class="form-control select2" onchange="onChangeRoute();">
									<option value="all">All</option>
                                    <?php foreach ($route as $r): ?>
                                    <option value="<?php echo $r->route?>"><?php echo $r->route?></option>
                                    <?php endforeach; ?>
                                    </select>
								</div>
							</div>
                        </div>
                        <div class="col-md-2">                                                
							<div class="form-group">
								<label class="col-md-12 control-label">Cycle</label>
								<div class="col-md-12 m-t-5">
                                    <select id="cycle" name="cycle" class="form-control select2">
									<option value="all~.~1" data-route="all">All</option>
                                    <?php foreach ($cycle as $c): ?>
                                    <option data-route="<?php echo $c->route?>" value="<?php echo $c->route . '~.~' . $c->cycle?>"><?php echo $c->cycle?></option>
                                    <?php endforeach; ?>
                                    </select>
								</div>
							</div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
								<label class="col-md-12 control-label">&nbsp;</label>
								<div class="col-md-12 m-t-5">
                                    <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" id="btnFilter" onclick="doFilter()">Filter</button>
								</div>
							</div>                        
                        </div>
                    </div>
                </div>
                
				<div class="row" id="card-table">
					<div class="col-md-12">
						<table id="datatable" class="table table-sm table-striped table-hover">
							<thead>							
								<tr>
									<th>Truck</th>
									<th>Driver Code</th>
									<th>Driver</th>
									<th>Route</th>
									<th>Cycle</th>
									<th>Logistic Point</th>
									<th>Departure Date</th>
									<th>Departure Plan</th>
									<th>Clock-In</th>
									<th>Clock-Out</th>
									<th>Rollcall Process</th>
								</tr>
							</thead>						
						</table>
					</div>
				</div>                
				
            </form>
        </div>
    </div>
</div>
