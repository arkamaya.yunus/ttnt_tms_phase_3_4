<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('route')?>">Route</a>
					</li>
					<li class="active">Edit</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	
	<div class="row">
		<?php if ($this->session->flashdata('notif_route_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_route_success')?>
			</div>
		</div>
		<?php endif; ?>
		
		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<input type="hidden" name="route" id="route" value="<?php echo $this->uri->segment(3)?>" />
				<input type="hidden" name="cycle" id="cycle" value="<?php echo $this->uri->segment(4)?>" />
				<input type="hidden" name="old_customer" id="old_customer" value="<?php echo $dataRoute->customer  ?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Customer <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
									<select name="customer" id="customer" class="form-control select2" disabled>
										<?php foreach ($customer as $a): ?>
										<option value="<?php echo $a->CustomerCode?>" <?php echo ($dataRoute->customer == $a->CustomerCode) ? 'selected="selected"' : ''?>><?php echo $a->CustomerName?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">							
							<div class="form-group">
								<label class="col-md-4 control-label">Route<span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">	
								   <input type="text" class="form-control" id="route" name="route" value="<?php echo $dataRoute->route ?>" required readonly/>
								</div>
							</div>                           
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">							
							<div class="form-group">
								<label class="col-md-4 control-label">Cycle<span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">		
								   <input type="text" class="form-control" id="cycle" name="cycle" value="<?php echo $dataRoute->cycle ?>" required readonly/>
								</div>
							</div>                           
						</div>
					</div>
					<br>
					<hr>
					<input type="hidden" id="emoney_details" name="emoney_details" value="" />
					<div class="row">
						<div class="col-sm-12">							
							<div class="form-group">
								<label class="col-md-4 control-label">E-Money</label>
							</div>		
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="" style="overflow-y: hidden">
								<table id="table-emoney" class="table-small-fonts table m-0 table-colored table-custom table-hover table-striped" style="width:50%">
									<thead>
										<tr>
											<th>No. Kartu</th>											
											<th>Gelombang</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $rown = 1; foreach ($emoney_detail as $q): ?>
										<tr id="row-<?php echo $rown?>" class="emoney-details">
											<td style="width: 50%">
												<select class="form-control emoneys" id="emoney-<?php echo $rown?>">
													<option value="<?php echo $q->emoney_id?>"><?php echo $q->emoney_id?></option>
												</select>
											</td>											
											<td style="width: 40%"><input class="form-control emoney_gel text-center" id="gel-<?php echo $rown?>" type="text" value="<?php echo $q->gel ?>"/></td>
											<td>
												<?php if ($rown > 1): ?>
													<a href="javascript:;" class="remove_row"><i class="fa fa-trash"></i></a>
												<?php endif; ?>
											</td>
										</tr>
										<?php $rown++; endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<button class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" type="button" aria-expanded="false" onclick="add_new_row_emoney('1');">+ Tambah Baris<span class="add"></span></button>
						</div>
					</div>
					<hr>
					<input type="hidden" id="bensin_details" name="bensin_details" value="" />
					<div class="row">
						<div class="col-sm-12">							
							<div class="form-group">
								<label class="col-md-4 control-label">Bensin</label>
							</div>		
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="" style="overflow-y: hidden">
								<table id="table-bensin" class="table-small-fonts table m-0 table-colored table-custom table-hover table-striped" style="width:50%">
									<thead>
										<tr>
											<th>SPBU</th>											
											<th>Jumlah Liter</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $rown = 1; foreach ($fuel_detail as $q): ?>
										<tr id="row-<?php echo $rown?>" class="bensin-details">
											<td style="width: 50%">
												<select class="form-control spbus" id="spbu-<?php echo $rown?>">
													<option value="<?php echo $q->spbu?>"><?php echo $q->spbu?></option>
												</select>
											</td>											
											<td style="width: 40%">
												<input class="form-control bensin_lit text-right" id="lit-<?php echo $rown?>" type="text" value="<?php echo $q->liter ?>"/></td>
											<td>
												<?php if ($rown > 1): ?>
													<a href="javascript:;" class="remove_row"><i class="fa fa-trash"></i></a>
												<?php endif; ?>
											</td>
										</tr>
										<?php $rown++; endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<button class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" type="button" aria-expanded="false" onclick="add_new_row_bensin('1');">+ Tambah Baris<span class="add"></span></button>
						</div>
					</div>
					<hr>
					<input type="hidden" id="other_details" name="other_details" value="" />
					<div class="row">
						<div class="col-sm-12">							
							<div class="form-group">
								<label class="col-md-4 control-label">Others</label>
							</div>		
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="" style="overflow-y: hidden">
								<table id="table-other" class="table-small-fonts table m-0 table-colored table-custom table-hover table-striped" style="width:50%">
									<thead>
										<tr>
											<th>Kategori</th>											
											<th>Jumlah</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $rown = 1; foreach ($other_detail as $q): ?>
										<tr id="row-<?php echo $rown?>" class="other-details">
											<td style="width: 50%">
												<input class="form-control others" id="other-<?php echo $rown?>" type="text" value="<?php echo $q->others ?>"/>
											</td>											
											<td style="width: 40%">
												<input class="form-control jumlah_other text-right currency" id="other-<?php echo $rown?>" type="text" value="<?php echo $q->amount ?>" placeholder="Rp. 0,00"/>
											</td>
											<td>
												<?php if ($rown > 1): ?>
													<a href="javascript:;" class="remove_row"><i class="fa fa-trash"></i></a>
												<?php endif; ?>
											</td>
										</tr>
										<?php $rown++; endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<button class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" type="button" aria-expanded="false" onclick="add_new_row_other('1');">+ Tambah Baris<span class="add"></span></button>
						</div>
					</div>
				</div>				
				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" id="btn-close" onclick="window.location='<?php echo site_url('route/id/' .$dataRoute->route.'/'.$dataRoute->cycle)?>'">Back</button>
						<?php echo create_button($this->button, "btn_submit"); ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>