<html>
    <head></head>
    <body>
        <table align="center" width="750" border="0" cellspacing="0" cellpadding="20px" style="border:1px solid #d8d8d8;border-radius:3px;overflow:hidden">
            <tr>
                <td align="center">

                    <table width="700" border="0" cellspacing="0" cellpadding="5px">
                        <tbody>
                            <tr>
                                <td align="left" style="font-family:helvetica neue,helvetica;font-size:16px;line-height:24px;background-color:#fff;">
                                    <p>
                                         Kepada <strong><?php echo ($employee_name!='') ? $employee_name : $email; ?>.</strong>
                                    </p>
									<p>
                                        Seseorang melakukan permintaan ubah Kata Sandi untuk masuk ke Aplikasi <strong><?php echo APP_NAME; ?></strong>. Jika anda tidak merasa melakukan permintaan ubah Kata Sandi, maka bisa diabaikan saja pesan ini.
                                    </p>
									<p>Untuk ubah Kata Sandi, silahkan klik <i>link</i> berikut :</p>
                                    <a href="<?php echo $link; ?>" style="color:#188AE2;"><?php echo $link; ?></a>                                    
                                </td>
                            </tr>
                            <tr>
                                <td width="600" align="left" style="font-family:helvetica neue,helvetica;font-size:16px;font-family:helvetica neue,helvetica;font-size:16px;padding:10px;background-color:#ffffff;border-collapse:collapse">
                                    <strong><?php echo APP_NAME; ?></strong>
                                    <p><a href="<?php echo base_url(); ?>" style="color:#777;text-decoration: none;"><strong><?php echo base_url(); ?></strong></a></p>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>

                </td>
            </tr>
        </table>
    </body>
</html>