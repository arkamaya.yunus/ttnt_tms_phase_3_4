<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('logistic_point')?>">Logistic Point</a>
					</li>
					<li class="active">Detail</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	
	<div class="row">
		<?php if ($this->session->flashdata('notif_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
			</div>
		</div>
		<?php endif; ?>

		<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="myModalLabel">Delete</h4>
					</div>
					<div class="modal-body">
						Are you sure?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-danger waves-effect waves-light" onclick="doDelete()" id="btn_delete_confirm">Delete</button>
					</div>
				</div>
			</div>
		</div>
	
		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<input type="hidden" name="lp_cd" id="lp_cd" value="<?php echo $this->uri->segment(3)?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Initial</label>
								<div class="col-md-8 m-t-5">
									<?php echo $lp->lp_cd?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Name</label>
								<div class="col-md-8 m-t-5">
									<?php echo $lp->lp_name;?>
								</div>
							</div>							
							<div class="form-group">
								<label class="col-md-4 control-label">Address</label>
								<div class="col-md-8 m-t-5">
									<?php										
										echo nl2br($lp->lp_address);
									?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Logistic Flag</label>
								<div class="col-md-8 m-t-5">
									<?php echo ($lp->empty_plan_flg == '0') ? 'No' : 'Yes'?>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							
							<div class="form-group">
								<label class="col-md-4 control-label">Area</label>
								<div class="col-md-8 m-t-5">
									<?php echo $lp->lp_area?>
								</div>
							</div>
                            <div class="form-group">
								<label class="col-md-4 control-label">Color</label>
								<div class="col-md-4 m-t-5" style="height: 30px; background-color: <?php echo $lp->lp_color?>;">
									&nbsp;
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-4 control-label">Remark</label>
								<div class="col-md-8 m-t-5">
									<?php echo $lp->lp_remark?>
								</div>
                            </div>
                            <!-- <div class="form-group">
								<label class="col-md-4 control-label">Business</label>
								<div class="col-md-8 m-t-5">
									<?php echo $lp->lp_business?>
								</div>
                            </div> -->
                            <div class="form-group">
								<label class="col-md-4 control-label">Geofence Id</label>
								<div class="col-md-8 m-t-5">
									<?php echo $lp->geofenceid?>
								</div>
                            </div>
                            <!-- <div class="form-group">
								<label class="col-md-4 control-label">Type</label>
								<div class="col-md-8 m-t-5">
									<?php echo $lp->lp_type_name?>
								</div>
							</div> -->
						</div>
					</div>
					<hr/>
																		
				</div>
				
				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('logistic_point')?>'">Back</button>
						<?php							
							$attrEdit = ''; // ($journal->reconcile_id!='' || $journal->journal_type != 'voucher') ? 'disabled':'';
							echo create_button($this->button, "btn_edit", $attrEdit, $this->uri->segment(3)); 
                        ?>
                        <?php 
							$attrDelete = ''; //($journal->reconcile_id!='' || $journal->journal_type != 'voucher') ? 'disabled':'';
                            echo create_button($this->button, "btn_delete", $attrEdit); 
                        ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>