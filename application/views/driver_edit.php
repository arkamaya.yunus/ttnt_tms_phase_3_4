<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('driver')?>">Driver</a>
					</li>
					<li class="active"><?php echo $stitle?></li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	
	<div class="row">
		<?php if ($save_sts == '0'): ?>
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> Save failed. Driver NIK <b><?php echo $driver_cd?></b> already used.
			</div>
		</div>
		<?php endif; ?>

		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
				<input type="hidden" class="form-control" id="driver_cd" name="driver_cd" value="<?php echo $driver_cd?>" />
				<input type="hidden" class="form-control" id="phone" name="phone" value="<?php echo $phone?>" />
				<input type="hidden" id="photo" name="photo" value="<?php echo $photo?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">NIK <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="driver_cdx" name="driver_cdx" value="<?php echo $driver_cd?>" disabled="disabled" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Driver Name <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="driver_name" name="driver_name" value="<?php echo $driver_name?>" required />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label">Logistic Partner</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="logistic_partner" name="logistic_partner" value="<?php echo $logistic_partner?>" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Jobdesc</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="jobdesc" name="jobdesc" value="<?php echo $jobdesc?>" />
								</div>
							</div>	
                            <div class="form-group">
								<label class="col-md-4 control-label">Phone Number</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="smartphone" name="smartphone" value="<?php echo $smartphone?>" />
								</div>
                            </div>
							<!-- <div class="form-group">
								<label class="col-md-4 control-label">Phone</label>
								<div class="col-md-8 m-t-5">                                    
								</div>
                            </div> -->
							<div class="form-group">
								<label class="col-md-4 col-sm-3 control-label">Photo</label>
								<div class="col-md-8 col-sm-9">
									<?php
										$filep = COMPANY_ASSETS_PATH. 'cid' . $this->session->userdata(S_COMPANY_ID) . '/profile/thumbs/' . $photo;
										
										if ($photo != '') {
											if (file_exists($filep)) {
												$photo = $filep;
											}
										}
										else
										{
											$photo = 'assets/images/user_photo.png';
										}
									?>
									<img src="<?php echo base_url() ?><?php echo $photo ?>" class="	img-thumbnail" alt="<?php echo $driver_name ?>" style="max-height: 128px" />
									<input type="file" name="file_photo" class="filestyle" data-input="false" accept="image/*" />
								</div>
							</div>							
							<div class="form-group">
								<label class="col-md-4 control-label">Active</label>
								<div class="col-md-8 m-t-5">
                                    <select id="active" name="active" class="form-control select2">
										<option value="1" <?php echo ($active == '1') ? 'selected="selected"' : ''?>>Yes</option>
										<option value="0" <?php echo ($active == '0') ? 'selected="selected"' : ''?>>No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Resign Date</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control datepicker" id="resign_dt" name="resign_dt" value="<?php echo ($resign_dt != '') ? date('d/m/Y', strtotime($resign_dt)) : '' ?>" />
								</div>
                            </div>
						</div>
						<div class="col-sm-6">							                            
							<div class="form-group">
								<label class="col-md-5 control-label">Join Date</label>
								<div class="col-md-7 m-t-5">
                                    <input type="text" class="form-control datepicker" id="join_dt" name="join_dt" value="<?php echo ($join_dt != '') ? date('d/m/Y', strtotime($join_dt)) : '' ?>" />
								</div>
                            </div>	
							<div class="form-group">
								<label class="col-md-5 control-label">Length of Working</label>
								<div class="col-md-7 m-t-5">
                                    <input type="hidden" class="form-control" id="length_of_working" name="length_of_working" value="<?php echo $length_of_working?>" />
									(auto)
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-5 control-label">Number Of Accident/Incident</label>
								<div class="col-md-7 m-t-5">
                                    <input type="number" class="form-control" id="num_of_accident" name="num_of_accident" value="<?php echo $num_of_accident?>" />
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-5 control-label">License Validation Driving</label>
								<div class="col-md-7 m-t-5">
                                    <input type="text" class="form-control datepicker" id="driving_license_val" name="driving_license_val" value="<?php echo ($driving_license_val != '') ? date('d/m/Y', strtotime($driving_license_val)) : '' ?>" />
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-5 control-label">Type Of Driver License Driving</label>
								<div class="col-md-7 m-t-5">
                                    <input type="text" class="form-control" id="driving_license_type" name="driving_license_type" value="<?php echo $driving_license_type?>" />
								</div>
                            </div>
							<div class="form-group">
								<label class="col-md-5 control-label">SIO</label>
								<div class="col-md-7 m-t-5">
                                    <input type="text" class="form-control datepicker" id="forklift_license_val" name="forklift_license_val" value="<?php echo ($forklift_license_val != '') ? date('d/m/Y', strtotime($forklift_license_val)) : '' ?>" />
								</div>
                            </div>
							<div class="form-group">
								<label class="col-md-5 control-label">SIO Type</label>
								<div class="col-md-7 m-t-5">
                                    <input type="text" class="form-control" id="forklift_license_type" name="forklift_license_type" value="<?php echo $forklift_license_type?>" />
								</div>
                            </div>
							<div class="form-group">
								<label class="col-md-5 control-label">Attendance ID</label>
								<div class="col-md-7 m-t-5">
                                    <input type="text" class="form-control" id="att_id" name="att_id" value="<?php echo $att_id?>" />
								</div>
                            </div>
							
							<div class="form-group">
								<label class="col-md-5 control-label">Date of Birth</label>
								<div class="col-md-7 m-t-5">
                                    <input type="text" class="form-control datepicker" id="dob" name="dob" value="<?php echo ($dob != '') ? date('d/m/Y', strtotime($dob)) : '' ?>" />
								</div>
                            </div>
							<div class="form-group">
								<label class="col-md-5 control-label">Business</label>
								<div class="col-md-7 m-t-5">
                                    <select name="business" id="business" class="form-control select2">
										<option value="MILKRUN" <?php echo ($business == 'MILKRUN') ? 'selected="selected"' : ''?>>MILKRUN</option>
										<option value="REGULER" <?php echo ($business == 'REGULER') ? 'selected="selected"' : ''?>>REGULER</option>
									</select>
								</div>
                            </div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('driver')?>'">Back</button>
						<?php echo create_button($this->button, "btn_submit"); ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>