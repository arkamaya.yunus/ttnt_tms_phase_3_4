<style>
    #sidebar-pengaturan{
        padding-bottom:30px;
        width:100%;
    }
    #container-forms{
        border-left: 2px solid black;
        padding-left:25px;
    }

    #sidebar-pengaturan, #sidebar-pengaturan a, #sidebar-pengaturan li, #sidebar-pengaturan ul {
        font-weight: 400;
        line-height: 1;
        list-style: none;
        margin: 0;
        padding: 0;
        position: relative;
        text-decoration: none;
    }

    #sidebar-pengaturan > ul > li > a {
        color: #2e383d;
        display: block;
        padding: 12px 20px;
        margin: 2px 0;
    }
    #sidebar-pengaturan ul li.active a, .button-menu-mobile:hover {
        color: #7fc1fc;
    }

    .label_normal
    {
        font-weight: normal;
        cursor: pointer;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Pengaturan</h4>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <?php if ($this->session->flashdata('notif_users_success') != ''): ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_users_success')?>
			</div>
		</div>
	</div>
	<?php endif; ?>

    <?php if ($this->session->flashdata('notif_users_uploads_error') != ''): ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_users_uploads_error')?>
			</div>
		</div>
	</div>
	<?php endif; ?>

    <div class="row">
        <div class="col-xs-12">            
            <div class="card-box">
                <div class="row">
                    <div class="col-md-2">
                        <div id="sidebar-pengaturan">
                            <?php echo $menu;?>
                        </div>
                    </div>
                    <div class="col-md-10" id="container-forms">
                        <form id="frm" method="post" action="">
                            <input type="hidden" name="user_group_id" value="<?php echo $role->user_group_id?>" />
                            <input type="hidden" name="is_admin" value="<?php echo $role->is_admin?>" />
                            <div class="rows">
                                <div class="col-md-6 m-b-20">
                                    <h4 class="page-title pb-20"><a href="<?php echo site_url('settings/roles')?>">Roles</a> <span style="color: #777">/</span> <?php echo $role->user_group_description?></h4>
                                </div>
                                <div class="col-md-6 m-b-20" style="text-align: right">
                                    <button id="btn_back" type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" onclick="window.location='<?php echo site_url('settings/roles')?>'">Back</button>
                                    <button id="btn_submit" type="submit" class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" >Save</button>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12 m-b-10">
                                            <input type="text" class="form-control" name="user_group_description_edit" value="<?php echo $role->user_group_description?>" required />
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <?php 
                                                $mn = $this->menu2;

                                                $uga = $this->db->query("
                                                    select * from tb_m_user_group_auth where md5(user_group_id) = '" . $this->uri->segment(4) . "'
                                                ")->result();

                                                // echo '<pre>'; print_r($uga); echo '</pre>';

                                                $arrUga = array();
                                                foreach ($uga as $u)
                                                {
                                                    $arrUga[$u->function_id . '~' . $u->feature_id] = 1;
                                                }

                                                // echo '<pre>'; print_r($arrUga); echo '</pre>';

                                            ?>
                                            <table class="table table-striped table-hover display nowrap table-colored table-primary">
                                                <thead>
                                                    <tr>
                                                        <th width="300">Menu</th>
                                                        <th>Auth</th>
                                                    </tr>                                                    
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    $function_grp = '';
                                                    foreach ($mn as $m):
                                                        
                                                        if ($function_grp != $m['function_grp'])
                                                        {
                                                            $function_grp = $m['function_grp'];
                                                            echo '
                                                                <tr>
                                                                    <td colspan="2" style="background-color: #fdffe0; font-weight: bold">'. $m['function_grp_nm'] . '</td>
                                                                </tr>
                                                            ';
                                                        }

                                                        $ff = $this->db->query("
                                                            select * from tb_m_function_feature where function_id = '" . $m['function_id'] . "' order by feature_element_type desc, function_id
                                                        ")->result();
                                                    ?>
                                                    <tr>
                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $m['function_id']?> - <?php echo $m['function_name']?></td>
                                                        <td>
                                                            <div class="row">
                                                                <?php foreach ($ff as $f): ?>
                                                                <div class="col-md-4">
                                                                    <input 
                                                                        type="checkbox" 
                                                                        name="uga[]" 
                                                                        value="<?php echo $f->function_id?>~<?php echo $f->feature_id?>" 
                                                                        id="<?php echo $f->feature_id?>"
                                                                        <?php if (isset($arrUga[$f->function_id . '~' . $f->feature_id])): ?>
                                                                        checked="checked"
                                                                        <?php endif;?>
                                                                    /> 
                                                                    <label class="label_normal" for="<?php echo $f->feature_id?>">
                                                                        <?php echo ucfirst($f->feature_element_type) ?>
                                                                        <?php echo $f->feature_name?>
                                                                    </label>
                                                                </div>
                                                                <?php endforeach; ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach ?>
                                                </tbody>
                                            </table>
                                        </div>                                    
                                    </div>		
                                </div>
                            </div>
                        </form>
                    </div>                       
                </div>
            </div>
        </div>
</div>