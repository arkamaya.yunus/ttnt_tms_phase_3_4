<style>
    #sidebar-pengaturan{
        padding-bottom:30px;
        width:100%;
    }
    #container-forms{
        border-left: 2px solid black;
        padding-left:25px;
    }

    #sidebar-pengaturan, #sidebar-pengaturan a, #sidebar-pengaturan li, #sidebar-pengaturan ul {
        font-weight: 400;
        line-height: 1;
        list-style: none;
        margin: 0;
        padding: 0;
        position: relative;
        text-decoration: none;
    }

    #sidebar-pengaturan > ul > li > a {
        color: #2e383d;
        display: block;
        padding: 12px 20px;
        margin: 2px 0;
    }
    #sidebar-pengaturan ul li.active a, .button-menu-mobile:hover {
        color: #7fc1fc;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Pengaturan</h4>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <?php if ($this->session->flashdata('notif_users_success') != ''): ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_users_success')?>
			</div>
		</div>
	</div>
	<?php endif; ?>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Create</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<form id="frm" class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
                                <input type="hidden" id="act" name="act" value="" />
								<div class="form-group pk">
                                    <label class="col-md-4 control-label">Email Login <span class="text-danger">*</span></label>
                                    <div class="col-md-8 m-t-5">
                                        <input type="email" class="form-control" name="user_email" id="user_email" value=""  required />
                                    </div>
								</div>
                                <div class="form-group pk pk-reset">
                                    <label class="col-md-4 control-label"><span id="tpass">Password Default</span> <span class="text-danger">*</span></label>
                                    <div class="col-md-8 m-t-5">
                                        <input type="text" class="form-control" name="user_password" id="user_password" value=""  required />
                                    </div>
								</div>
                                <div class="form-group mk">
                                    <label class="col-md-4 control-label">Full Name<span class="text-danger">*</span></label>
                                    <div class="col-md-8 m-t-5">
                                        <input type="text" class="form-control" name="full_name" id="full_name" value=""  required />
                                    </div>
								</div>
                                <div class="form-group mk">
                                    <label class="col-md-4 control-label">Role <span class="text-danger">*</span></label>
                                    <div class="col-md-8 m-t-5">
                                        <select class="form-control select2" name="user_group_id" id="user_group_id">
                                            <?php foreach ($roles as $r): ?>
                                            <option value="<?php echo $r->user_group_id?>"><?php echo $r->user_group_description?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary waves-effect waves-light" onclick="doSave()" id="btn_save_user">Save</button>
				</div>
			</div>
		</div>
	</div>

    <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal2Label" aria-hidden="true">
        <form id="frm2" class="form-horizontal" role="form" method="post" action="">
            <input type="hidden" id="user_email_delete" name="user_email_delete" value="" />
        </form>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModal2Label">Delete</h4>
                </div>
                <div class="modal-body">
                    Are you sure?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger waves-effect waves-light" onclick="doDelete()" id="btn_delete_confirm">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <form id="settings_users" class="form-horizontal" role="form" method="post" action="<?php echo site_url('settings/create_users')?>" enctype="multipart/form-data">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-2">
                            <div id="sidebar-pengaturan">
                               <?php echo $menu;?>
                            </div>
                        </div>
                        <div class="col-md-10" id="container-forms">
                            <div class="rows">
                                <div class="col-md-6 m-b-20">
                                    <h4 class="page-title pb-20">Users</h4>
                                </div>
                                <div class="col-md-6 m-b-20" style="text-align: right">                                    
                                    <button id="btn_add" type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" onclick="create();" >Create</button>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <?php if ($this->session->flashdata('notif_success') != ''): ?>
                                        <div class="col-sm-12">
                                            <div class="alert alert-success" role="alert">
                                                <i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <div class="col-sm-12">
                                            <table id="datatable" class="table table-striped table-hover display nowrap table-colored table-primary">
                                                <thead>
                                                    <tr>
                                                        <th>Role</th>
                                                        <th>Full Name</th>
                                                        <th>Email Login</th>
                                                        <th style="text-align: center"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($users as $r): ?>
                                                    <tr>
                                                        <td><?php echo $r->user_group_description?></td>
                                                        <td><?php echo $r->full_name?></td>
                                                        <td><?php echo $r->user_email?></td>
                                                        <td style="text-align: center">
                                                            <a href="javascript:;" title="Edit User" onclick="edit('<?php echo $r->user_email?>', '<?php echo $r->full_name?>', '<?php echo $r->user_group_id?>')">Edit</a>
                                                            <?php if ($r->super_admin == 0):?>
                                                                &nbsp;&nbsp; | &nbsp;&nbsp;
                                                                <a href="javascript:;" onclick="confirmDelete('<?php echo $r->user_email?>')" title="Delete">Delete</a>
                                                                &nbsp;&nbsp; | &nbsp;&nbsp;
                                                                <a href="javascript:;" onclick="reset('<?php echo $r->user_email?>')" title="Reset">Reset</a>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>		
                                </div>
                            </div> 
                        </div>
                    </div>                    
                </div>
                
            </form>
        </div>
</div>