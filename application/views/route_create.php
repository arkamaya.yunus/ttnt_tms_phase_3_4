<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('route')?>">Route</a>
					</li>
					<li class="active">Create</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	<?php if ($this->session->flashdata('notif_route_error') != ''): ?>
		<div class="col-sm-12" hidden>
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> <div id="notif_route_error"><?php echo $this->session->flashdata('notif_route_error'); ?></div>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">				
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Customer <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
									<select name="customer" id="customer" class="form-control select2">
										<?php foreach ($customer as $a): ?>
										<option value="<?php echo $a->CustomerCode?>" ><?php echo $a->CustomerName?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">							
							<div class="form-group">
								<label class="col-md-4 control-label">Route<span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">									
                                    <input type="text" class="form-control" id="route" name="route" required />
								</div>
							</div>                           
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">							
							<div class="form-group">
								<label class="col-md-4 control-label">Cycle<span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">									
                                    <input type="text" class="form-control" id="cycle" name="cycle" required />
								</div>
							</div>                           
						</div>
					</div>
					<br>
					<hr>
					<div class="row">
						<div class="col-sm-12">							
							<div class="form-group">
								<label class="col-md-4 control-label">E-Money</label>
							</div>		
						</div>
					</div>
					<input type="hidden" id="emoney_details" name="emoney_details" value="" />
						<div class="row">
							<div class="col-md-12">							
								<div class="" style="overflow-y: hidden">
									<table id="table-emoney" class="table-small-fonts table m-0 table-colored table-custom table-hover table-striped" style="width:50%">
										<thead>
											<tr>
												<th>No. Kartu</th>											
												<th>Gelombang</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
										<tr id="row-1" class="emoney-details">
											<td style="width: 50%"><select class="form-control emoneys" id="emoney-1"></select></td>
											<td style="width: 40%"><input class="form-control emoney_gel text-center"  id="gel-1" type="text" value="1" /></td>
											<td></td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>						
						<input type="hidden" id="adaEmoney" name="adaEmoney" value="0" />
						<div class="row">
							<div class="col-md-6">
								<button class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" type="button" aria-expanded="false" onclick="add_new_row_emoney('1');">+ Tambah Baris<span class="add"></span></button>
							</div>
						</div>
					<hr>
					<div class="row">
						<div class="col-sm-12">							
							<div class="form-group">
								<label class="col-md-4 control-label">Bensin</label>
							</div>		
						</div>
					</div>
					<input type="hidden" id="bensin_details" name="bensin_details" value="" />
						<div class="row">
							<div class="col-md-12">							
								<div class="" style="overflow-y: hidden">
									<table id="table-bensin" class="table-small-fonts table m-0 table-colored table-custom table-hover table-striped" style="width:50%">
										<thead>
											<tr>
												<th>SPBU</th>											
												<th>Jumlah Liter</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
										<tr id="row-1" class="bensin-details">
											<td style="width: 50%"><select class="form-control spbus" id="spbu-1"></select></td>
											<td style="width: 40%"><input class="form-control bensin_lit text-right"  id="lit-1" type="text" value="1" /></td>
											<td></td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					<input type="hidden" id="adaBensin" name="adaBensin" value="0" />
					<div class="row">
						<div class="col-md-6">
							<button class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" type="button" aria-expanded="false" onclick="add_new_row_bensin('1');">+ Tambah Baris<span class="add"></span></button>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-sm-12">							
							<div class="form-group">
								<label class="col-md-4 control-label">Others</label>
							</div>		
						</div>
					</div>
					<input type="hidden" id="other_details" name="other_details" value="" />
					<div class="row">
						<div class="col-md-12">							
							<div class="" style="overflow-y: hidden">
								<table id="table-other" class="table-small-fonts table m-0 table-colored table-custom table-hover table-striped" style="width:50%">
									<thead>
										<tr>
											<th>Kategori</th>											
											<th>Jumlah</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									<tr id="row-1" class="other-details">
										<td style="width: 50%"><input class="form-control others" id="other-1" /></td>
										<td style="width: 40%"><input class="form-control jumlah_other text-right currency" id="jumlah-1" type="text" placeholder="Rp. 0,00" /></td>
										<td></td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<button class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" type="button" aria-expanded="false" onclick="add_new_row_other('1');">+ Tambah Baris<span class="add"></span></button>
						</div>
					</div>
				</div>				
				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" id="btn-close" onclick="window.location='<?php echo site_url('route')?>'">Back</button>
						<?php echo create_button($this->button, "btn_submit"); ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>