<div class="container">

	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $company->company_name?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Beranda</li>
					<li>
						<a href="<?php echo site_url('company')?>">Perusahaan</a>
					</li>
					<li class="active">Info</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->

    <?php if ($this->session->flashdata('notif_company_success') != ''): ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_company_success')?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	<div class="row">

        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="myModalLabel">Hapus Perusahaan</h4>
					</div>
					<div class="modal-body">
						Anda Yakin ? Seluruh data transaksi akan hilang.
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
						<button type="button" class="btn btn-primary waves-effect waves-light" onclick="on_delete_confirm()" id="btn_delete_confirm">Hapus</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
		<div class="col-sm-12">
			<form id="form_company" class="form-horizontal" role="form" method="post" action="">
				<input type="hidden" name="company_id" id="company_id" value="<?php echo $company->company_id?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Nama Perusahaan <span class="text-danger">*</span></label>
								<div class="col-md-8">
                                    <?php echo $company->company_name?>
								</div>
							</div>
							<div class="form-group">
                                <label class="col-md-4 control-label">Alamat</label>
                                <div class="col-md-8">
                                    <?php echo $company->address?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Jenis Industri</label>
                                <div class="col-md-8">
                                    <?php echo $company->company_industry?>
                                </div>
                            </div>
						</div>
						<div class="col-md-6">
                        <div class="form-group">
                                <label class="col-md-4 control-label">Email</label>
                                <div class="col-md-8">
                                <?php echo $company->email?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nomor Telepon</label>
                                <div class="col-md-8">
                                <?php echo $company->phone?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nomor Fax</label>
                                <div class="col-md-8">
                                <?php echo $company->fax?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nomor Npwp</label>
                                <div class="col-md-8">
                                <?php echo $company->tax_number?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Website</label>
                                <div class="col-md-8">
                                <?php echo $company->website?>
                                </div>
                            </div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('company')?>'">Kembali</button>
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('company/edit/' . $company->company_id)?>'">Ubah</button>
						<button type="button" class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 sbmt_btn" id="btn_submit" onclick="window.location='<?php echo site_url('company/launch/' . $company->company_id)?>'">Gunakan Perusahaan</button>
					</div>
                    <div class="col-md-6 text-right">
						<button type="button" class="btn btn-danger btn-bordered waves-light waves-effect w-md m-b-5 sbmt_btn" id="btn_submit" onclick="$('#myModal').modal('show');">Hapus</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	
</script>