<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('emoney')?>">Emoney</a>
					</li>
					<li class="active">Detail</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->

	<div class="row">
		<?php if ($this->session->flashdata('notif_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
			</div>
		</div>
		<?php endif; ?>

		<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="myModalLabel">Delete</h4>
					</div>
					<div class="modal-body">
						Are you sure?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-danger waves-effect waves-light" onclick="doDelete()" id="btn_delete_confirm">Delete</button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<input type="hidden" name="emoney_id" id="emoney_id" value="<?php echo $this->uri->segment(3)?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">E-Money ID</label>
								<div class="col-md-8 m-t-5">
									<?php echo $emoney->emoney_id;?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Category</label>
								<div class="col-md-8 m-t-5">
									<?php echo $emoney->emoney_ctg?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Bank</label>
								<div class="col-md-8 m-t-5">
									<?php echo $emoney->bank?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Balance</label>
								<div class="col-md-8 m-t-5">
									<?php
										echo 'Rp. '.number_format($emoney->balance,2,",",".");
									?>
								</div>
							</div>
						</div>
						<div class="col-md-6">

							<div class="form-group">
								<label class="col-md-4 control-label">Customer</label>
								<div class="col-md-8 m-t-5">
									<?php echo $emoney->cust_name?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Cycle</label>
								<div class="col-md-8 m-t-5">
									<?php echo $emoney->cycle?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Gel</label>
								<div class="col-md-8 m-t-5">
									<?php echo $emoney->gel?>
								</div>
							</div>

						</div>
					</div>
					<hr/>

				</div>
					<div class="form-group">
						<label class="col-md-4 control-label">History</label>
					</div>
					<div class="row">
						<div class="col-md-12">
							<!-- History transaksi -->
							<?php if(!empty($history)){ ?>
							<div class="card-box table-responsive">
								<table id="datatable" class="table table-striped table-hover display nowrap">
									<thead>
									<tr>
										<th>Tanggal</th>
										<th>Transaksi</th>
										<th class="text-right">Amount</th>
										<th class="text-right">Balance</th>
									</tr>
									</thead>
									<tbody>
										<?php
										$amount_total = '';
										$balance = '';
										foreach($history as $h){
										$amount_total += floatval($h->transaction_amount);
										$balance = (floatval($emoney->balance) - floatval($amount_total));
										?>
										<tr>
											<td><?php echo date("d-M-Y", strtotime($h->transaction_dt)); ?></td>
											<td><?php echo $h->transaction_desc ?></td>
											<td class="text-right"><?php echo 'Rp. '.number_format($h->transaction_amount,2,",",".") ?></td>
											<td class="text-right"><?php echo 'Rp. '.number_format($balance,2,",",".") ?></td>
										</tr>
										<?php } ?>
										<tr>
											<td></td>
											<td></td>
											<td class="text-right"><?php echo 'Rp. '.number_format($amount_total,2,",","."); ?></td>
											<td class="text-right"><?php echo 'Rp. '.number_format($balance,2,",",".") ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						<?php }else { ?>
							<div class="col-sm-12">
								<div class="alert alert-info" role="alert">
									Ther are still no transaction yet for this E-Money.
								</div>
							</div>
						<?php } ?>
<!-- History transaksi -->
						</div>
					</div>

				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('emoney')?>'">Back</button>
						<?php
							$attrEdit = ''; // ($journal->reconcile_id!='' || $journal->journal_type != 'voucher') ? 'disabled':'';
							echo create_button($this->button, "btn_edit", $attrEdit, $this->uri->segment(3));
                        ?>
                        <?php
							$attrDelete = ''; //($journal->reconcile_id!='' || $journal->journal_type != 'voucher') ? 'disabled':'';
												if($checkTrans->cnt == 0){
                            echo create_button($this->button, "btn_delete", $attrEdit);
												}
                        ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
