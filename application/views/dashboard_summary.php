<style>
#dtDepartureDelay tr td,
#dtArrivalDelay tr td
{
	font-size: 12px;
	cursor: pointer;
}

.card-body {
  height: 500px;
  overflow-y: scroll;
}
</style>

<div class="container">
	
	<form id="f_dashboard_business" name="f_dashboard_business" method="post" action="">
	<div class="row">
		<div class="col-xs-12">			
			<div class="page-title-box" style="padding-bottom: 15px">
				<div class="row">
					<div class="col-md-6">
						<h4 class="page-title" style="margin-top: 10px"><?php echo $stitle?></h4>
					</div>
					<div class="col-md-6">
						<div class="col-md-2 m-t-10" style="color: red"><span id="pLoad" style="display: none" ><i class="fa fa-spinner fa-pulse fa-fw"></i> Sync...</span></div>
						<div class="col-md-6">
							<select id="customer_lp_cd" class="form-control select2">                    					
								<option value="all">All Customer Logistic Point</option>
								<?php foreach ($customer_lp_cds as $customer_lp_cd): ?>
								<option value="<?php echo $customer_lp_cd->lp_cd?>"><?php echo $customer_lp_cd->lp_cd?> - <?php echo $customer_lp_cd->lp_name?></option>
								<?php endforeach; ?>
							</select>
							<input type="hidden" name="logistic_partner" id="logistic_partner" value="all" />                
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control datepicker" id="timetable_dt" name="timetable_dt" value="<?php echo date('d/m/Y')?>" />
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	</form>

	<div id="myTruckModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myTruckModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myTruckModalLabel">Trucks On Duty</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12" style="height: 500px; overflow: scroll">
							<table id="dtTruck" class="table table-sm table-striped table-hover table-colored-bordered table-bordered-primary">
								<thead>
									<tr>
										<th>No</th>
										<th>Truck Number</th>
										<th>Police Number</th>
										<th>Vehicle Owner</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div id="myDriverModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myDriverModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myDriverModalLabel">Drivers On Duty</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12" style="height: 500px; overflow: scroll">
							<table id="dtDriver" class="table table-sm table-striped table-hover table-colored-bordered table-bordered-primary">
								<thead>
									<tr>
										<th>No</th>
										<th>NIK</th>
										<th>Driver Name</th>
										<th>Logistic Partner</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

    <div class="row">							
		<div class="col-lg-3 col-md-6">
			<div class="card-box widget-box-two widget-two-primary">
				<i class="mdi mdi-calendar-multiple-check widget-two-icon"></i>
				<div class="wigdet-two-content">
					<p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Total Planning</p>
					<h2><span id="total_planning">0</span> Plans</h2>
					<p class="text-muted m-0"><a href="javascript:;" onclick="viewOperation()">View Timetable Operation</a></p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="card-box widget-box-two widget-two-primary">
				<i class="mdi mdi-check widget-two-icon"></i>
				<div class="wigdet-two-content">
					<p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Arrival Done</p>
					<h2><span id="total_arrival">0</span> Arrived</h2>
					<p class="text-muted m-0"><a href="javascript:;" onclick="viewOperationNYA()"><span id="total_not_arrival" class="text-danger" style="font-weight: bold" >0</span> Not Yet Arrived</a></p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="card-box widget-box-two widget-two-primary">
				<i class="mdi mdi-truck-delivery widget-two-icon"></i>
				<div class="wigdet-two-content">
					<p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">Total Trucks (Assigned / Active)</p>
					<h2><span id="total_truck">0</span> of <span id="total_truck_active">0</span> Trucks</h2>
					<p class="text-muted m-0"><a href="javascript:;" onclick="$('#myTruckModal').modal('show')">View trucks assigned</a></p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="card-box widget-box-two widget-two-primary">
				<i class="mdi mdi-account widget-two-icon"></i>
				<div class="wigdet-two-content">
					<p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User Today">Total Drivers (Assigned / Active)</p>
					<h2><span id="total_driver">0</span> of <span id="total_driver_active">0</span> Drivers</h2>
					<p class="text-muted m-0"><a href="javascript:;" onclick="$('#myDriverModal').modal('show')">View Drivers</a></p>
				</div>
			</div>
		</div>
	</div>


	<div class="row">
		<?php foreach ($business as $b): ?>
		<div class="col-md-6">
			<div class="demo-box">
				<h3 class="header-title"><?php echo $b->system_value_txt?></h3>
				<div class="row">
					<div class="col-sm-12">
						<table id="dtDriver" class="table table-sm table-striped table-hover table-colored-bordered table-bordered-primary">							
								<tr>
									<th class="text-center" style="border: 2px solid #188ae2; background-color: rgba(24, 138, 226, 0.2)">Total Driver</th>
									<th class="text-center" style="border: 2px solid #188ae2; background-color: rgba(24, 138, 226, 0.2)">Total Truck</th>
								</tr>							
								<tr>
									<td class="text-center"><h1 id="total_driver_<?php echo $b->system_value_txt?>">0</h1></td>
									<td class="text-center" style="border-left: 2px solid #188ae2"><h1 id="total_truck_<?php echo $b->system_value_txt?>">0</h1></td>
								</tr>
								<tr>
									<th class="text-center" style="border: 2px solid #188ae2; background-color: rgba(24, 138, 226, 0.2)">Total Cycle</th>
									<th class="text-center" style="border: 2px solid #188ae2; background-color: rgba(24, 138, 226, 0.2)">Total Delay</th>
								</tr>
								<tr>
									<td class="text-center"><h2 id="total_cycle_<?php echo $b->system_value_txt?>">0</h2></td>
									<td id="delay-bg-<?php echo $b->system_value_txt?>" class="text-center" style="border-left: 2px solid #188ae2"><h2 id="total_delay_<?php echo $b->system_value_txt?>">0</h2></td>
								</tr>							
						</table>
					</div>
				</div>

			</div>
		</div>
		<?php endforeach; ?>
	
	<!-- <div class="row">
		<div class="col-md-4">
			<div class="demo-box">
				<h4 class="header-title">Total Plan By Business</h4>
				<p class="sub-header">
					This graph show total timetable planning group by it's business.
				</p>

				<div id="business-chart" dir="ltr" class="morris-charts" style="height: 300px;"></div>

				<div class="text-center">
					<ul class="list-inline chart-detail-list">
						<?php 
						$colors = array('primary', 'success', 'warning', 'danger', 'inverse', 'purple', 'pink', 'custom', 'orange', 'brown', 'teal');

						$i=0;
						foreach ($business as $b): ?>
						<li class="list-inline-item">
							<h5 class="text-<?php echo $colors[$i]?>"><i class="mdi mdi-checkbox-blank-circle-outline mr-1"></i>  <?php echo $b->system_value_txt?> (<span id="total-<?php echo $b->system_code?>">0</span>)</h5>
						</li>
						<?php $i++; endforeach; ?>
					</ul>
				</div>

			</div>
		</div>
		<div class="col-md-8">
			<div class="demo-box">
				<h4 class="header-title">Total By Customer Logistic Point</h4>
				<p class="sub-header">
					This graph show total plan and done group by Customer and it's logistic point.
				</p>

				<div class="text-center">
					<ul class="list-inline chart-detail-list">
						<li class="list-inline-item">
							<h5 class="text-primary"><i class="mdi mdi-crop-square mr-1"></i>Plan</h5>
						</li>
						<li class="list-inline-item">
							<h5 class="text-success"><i class="mdi mdi-crop-square mr-1"></i>Arrival Done</h5>
						</li>
						<li class="list-inline-item">
							<h5 class="text-danger"><i class="mdi mdi-crop-square mr-1"></i>Arrival Not Yet</h5>
						</li>
					</ul>
				</div>
				<div id="customerlp-chart" dir="ltr" class="morris-charts" style="height: 300px;"></div>

			</div>
		</div>
	</div>	 -->
</div>