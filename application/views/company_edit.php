<div class="container">

	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title">Ubah Perusahaan</h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Beranda</li>
					<li>
						<a href="<?php echo site_url('company')?>">Perusahaan</a>
					</li>
					<li class="active">Ubah Perusahaan</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	
	<div class="row">
		
		<div class="col-sm-12">
			<form id="form_company" class="form-horizontal" role="form" method="post" action="">
				<input type="hidden" name="company_id" id="company_id" value="<?php echo $company->company_id?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Company Name <span class="text-danger">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control" value="<?php echo $company->company_name?>" name="company_name" required />
								</div>
							</div>
							<div class="form-group">
                                <label class="col-md-4 control-label">Address</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" maxlength="255"   rows="2" name="address" id="address" ><?php echo $company->address?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Industry</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" maxlength="100" value="<?php echo $company->company_industry?>" name="company_industry" id="company_industry">
                                </div>
                            </div>
						</div>
						<div class="col-md-6">
                        <div class="form-group">
                                <label class="col-md-4 control-label">Email</label>
                                <div class="col-md-8">
                                    <input type="email" maxlength="100" class="form-control" value="<?php echo $company->email?>" name="email" id="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Phone Number</label>
                                <div class="col-md-8">
                                    <input type="text" maxlength="100" class="form-control numbersOnly" value="<?php echo $company->phone?>" name="phone" id="phone">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Fax Number</label>
                                <div class="col-md-8">
                                    <input type="text" maxlength="20" class="form-control numbersOnly" id="fax" name="fax" value="<?php echo $company->fax?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Npwp Number</label>
                                <div class="col-md-8">
                                    <input type="text" maxlength="50" class="form-control numbersOnly" id="tax_number" name="tax_number" value="<?php echo $company->tax_number?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Website</label>
                                <div class="col-md-8">
                                    <input type="text" maxlength="100" class="form-control" id="company_website" name="company_website" value="<?php echo $company->website?>">
                                </div>
                            </div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('company')?>'">Kembali</button>
						<button type="button" class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 sbmt_btn" id="btn_submit" onclick="submit_company()">Simpan</button>						
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	
</script>