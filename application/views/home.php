<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Kelola.biz - Software Bisnis Terintegrasi #1 di Indonesia">
        <meta name="author" content="mail@kelola.biz">

        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.png">

        <title>Kelola.biz | Software Bisnis Terintegrasi #1 di Indonesia</title>

        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url()?>assets/frontend/css/bootstrap.min.css" rel="stylesheet">

        <!-- Owl Carousel CSS -->
        <link href="<?php echo base_url()?>assets/frontend/css/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/frontend/css/owl.theme.default.min.css" rel="stylesheet">

        <!-- Icon CSS -->
        <link href="<?php echo base_url()?>assets/frontend/css/font-awesome.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url()?>assets/frontend/css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>


    <body data-spy="scroll" data-target="#navbar-menu">

        <!-- Navbar -->
        <div class="navbar navbar-custom sticky navbar-fixed-top" role="navigation" id="sticky-nav">
            <div class="container">

                <!-- Navbar-header -->
                <div class="navbar-header">

                    <!-- Responsive menu button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- LOGO -->
                    <a class="navbar-brand logo" href="<?php echo site_url('home')?>">
                        <img src="<?php echo base_url()?>assets/images/logo_app_2.png" height="35" style="margin-top: -10px" />
                    </a>

                </div>
                <!-- end navbar-header -->

                <!-- menu -->
                <div class="navbar-collapse collapse" id="navbar-menu">

                    <!-- Navbar right -->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active">
                            <a href="#home" class="nav-link">Beranda</a>
                        </li>
                        <li>
                            <a href="#features" class="nav-link">Fitur</a>
                        </li>
                        <li>
                            <a href="#pricing" class="nav-link">Harga</a>
                        </li>
                        <!--li>
                            <a href="#clients" class="nav-link">Clients</a>
                        </li-->
                        <li>
                            <a href="<?php echo site_url('login')?>">Login</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('register')?>" class="btn btn-white-bordered navbar-btn">Coba sekarang</a>
                        </li>
                    </ul>

                </div>
                <!--/Menu -->
            </div>
            <!-- end container -->
        </div>
        <!-- End navbar-custom -->



        <!-- HOME -->
        <section class="home bg-img-1" id="home">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="home-fullscreen">
                            <div class="full-screen">
                                <div class="home-wrapper home-wrapper-alt">
                                    <h2 class="font-light text-white">Software Bisnis Terintegrasi #1 di Indonesia - Gratis </h2>
                                    <h4 class="text-white">Kelola.biz membantu anda mengelola bisnis anda secara realtime, online. Berbasis cloud untuk mengontrol Keuangan &amp; Akuntansi, Manajemen SDM, Penjualan &amp; Distribusi, Pembelian &amp; Inventori, manajemen stok, CRM, bahkan sampai dengan membantu manajemen proyek perusahaan anda</h4>
                                    <a href="<?php echo site_url('register')?>" target="_blank" class="btn btn-custom">Daftar Sekarang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END HOME -->


        <!-- Features -->
        <section class="section" id="features">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3 class="title">Dapatkan kemudahan untuk mengakses data bisnis Perusahaan Anda<br/>Kapanpun. Dimanapun.</h3>
                        <p class="text-muted sub-title">Segala informasi terkait bisnis anda kini dapat di akses dengan mudah baik melalui web Kelola.biz <br/>maupun Aplikasi Mobile Kelola.biz yang bisa anda unduh di Google Playstore. <img src="<?php echo base_url()?>assets/frontend/images/badge_new.png" height="30" style="margin-top: 2px" /> <sup>Coming Soon</sup></p>
                    </div>
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-sm-4">
                        <div class="features-box">
                            <i class="fa fa-balance-scale"></i>
                            <h4>Keuangan &amp; Akuntansi</h4>
                            <p class="text-muted">Membuat Laporan keuangan akan menjadi lebih mudah dan menyenangkan. Mulai dari mengatur Akun sendiri, membuat jurnal, sampai membuat Laporan Laba Rugi, Neraca Saldo, Laporan Perubahan Modal, Buku Besar dan Arus Kas</p>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="features-box">
                            <i class="fa fa-id-card"></i>
                            <h4>Sumber Daya Manusia</h4>
                            <p class="text-muted">Kelola SDM anda dengan Kelola.biz. Undang pegawai Anda untuk masuk dan mendapatkan informasi pribadinya langsung. Info &amp; Pengajuan Cuti, Reimburse, Pinjaman, Lembur, Slip Gaji. <em style="font-weight: bolder">Paperless !</em>.</p>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="features-box">
                            <i class="fa fa-tags"></i>
                            <h4>Penjualan &amp; Distribusi</h4>
                            <p class="text-muted">Anda bisa membuat Penawaran Penjualan, Pemesanan, Pengiriman, Penagihan (invoicing) Faktur dan Pembayarannya melalui aplikasi ini. Kontrol Laporan Usia Piutang Pelanggan anda dan lihat Proyeksi pendapatan Penjualan dalam sekali klik.</p>
                        </div>
                    </div>

                </div> <!-- end row -->
				
				<div class="row">
                    <div class="col-sm-4">
                        <div class="features-box">
                            <i class="fa fa-truck"></i>
                            <h4>Pembelian &amp; Inventori</h4>
                            <p class="text-muted">Buat permintaan pembelian. Kelola Pemasok (supplier) anda, dan lakukan order pembelian, penerimaan barang disini. Inventori barang, Stock Opname dan transfer antar gudang dilakukan darisini</p>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="features-box">
                            <i class="fa fa-check-square-o"></i>
                            <h4>Manajemen Proyek &amp; Tugas</h4>
                            <p class="text-muted">Ada proyek yang harus anda kelola? Siapkan datanya dan gunakan fitur kelola Manajemen Proyek. Membuat tugas dan tentukan siapa yang mengerjakan. Kontrol progress dari proyek dapat dilihat juga melalui gantt-chart yang interaktif.</p>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="features-box">
                            <i class="fa fa-pagelines"></i>
                            <h4>CRM <em>(Customer Relationship Management)</em></h4>
                            <p class="text-muted">Ada prospek (Leads) yang harus di kejar untuk dijadikan Peluang (Opportunity) ? Jadikan penjualan! Kelola disini. Ada Fitur Notifikasi untuk menentukan prioritas prospek untuk memaksimalkan CRM diperusahaan anda.</p>
                        </div>
                    </div>

                </div> <!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- end Features -->



        <!-- Features Alt -->
        <section class="section p-t-0">
            <div class="container">

                <div class="row">
                    <div class="col-sm-5">
                        <div class="feat-description m-t-20">
                            <h4>Lihat semua di Dasbor</h4>
                            <p class="text-muted">Setiap Fitur yang ada di Kelola.biz memiliki dasbor nya masing - masing untuk memberikan gambaran kepada anda bagaimana bisnis anda berjalan. Salah satu kegunaannya untuk mendapatkan <strong>Insight</strong> * sebagai bahan untuk pengambilan keputusan manajemen yang tepat. <br/><br/>Aplikasi dibuat responsive sehingga nyaman dilihat dari layar smartphone Anda.</p>

                            <a href="<?php echo site_url('register')?>" class="btn btn-custom">Coba Yuk</a>
                        </div>
                    </div>

                    <div class="col-sm-6 col-sm-offset-1">
                        <img src="<?php echo base_url()?>assets/frontend/images/mock_2.png" alt="img" class="img-responsive m-t-20">
                    </div>

                </div><!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- end features alt -->


        <!-- Features Alt -->
        <section class="section">
            <div class="container">

                <div class="row">
                    <div class="col-sm-6">
                        <img src="<?php echo base_url()?>assets/frontend/images/mock_3.png" alt="img" class="img-responsive">
                    </div>

                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="feat-description">
                            <h4>Kelola Pegawai Anda, Berapapun jumlahnya.</h4>
                            <p class="text-muted">Di Kelola.biz, anda yang memegang kendali untuk mengatur pengelolaan SDM dan sumberdaya perusahaan agar memberikan hasil maksimal mulai dari perencanaan rekrutmen pegawai, pembuatan kontrak kerja, mutasi pegawai, menentukan komponen gaji sampai Training/Pelatihan Pegawai. <br/><br/> Pegawai Anda banyak? Coba dulu fasilitas impor data dari Kelola.biz sehingga anda tidak perlu repot menambahkan pegawai satu persatu.</p>

                            <a href="<?php echo site_url('register')?>" class="btn btn-custom">Tertarik mencobanya?</a>
                        </div>
                    </div>

                </div><!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- end features alt -->


        <!-- Features Alt -->
        <section class="section">
            <div class="container">

                <div class="row">
                    <div class="col-sm-5">
                        <div class="feat-description">
                            <h4>Layanan Mandiri Pegawai</h4>
                            <p class="text-muted">Penyebaran informasi dari Perusahaan ke Pegawai maupun sebaliknya bisa dilakukan dengan cepat. Berikut fitur utama dari Layanan mandiri Kelola.biz:</p>

							<p class="text-muted">
							<ul class="text-muted">
								<li><strong>Tap InOut</strong> (Presensi/Absensi Online)</li>
								<li><strong>Pelatihan Pegawai</strong> *</li>
								<li>Pengajuan Perjalanan Dinas</li>
								<li>Pengajuan Penggantian Uang (Reimburse)</li>
								<li>Pengajuan Cuti/Tidak Masuk</li>
								<li>Pengajuan Lembur Bulanan</li>
								<li>Pengajuan Pinjaman Pegawai</li>
								<li>Laporan Kehadiran</li>
								<li>Slip Gaji Pegawai</li>
								<li>Manajemen Tugas &amp; Proyek</li>
								<li><strong>Bewara</strong> - Media Informasi Perusahaan *</li>
																
							</ul>
							
							<br/>
							*Fitur Menyusul
							</p>
							
                            <a href="<?php echo site_url('register')?>" class="btn btn-custom">Daftar Disini</a>
                        </div>
                    </div>

                    <div class="col-sm-6 col-sm-offset-1">
                        <img src="<?php echo base_url()?>assets/frontend/images/mock_4.png" alt="img" class="img-responsive">
                    </div>

                </div><!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- end features alt -->


        <!-- Testimonials section -->
        <section class="section bg-img-1">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                        <div class="owl-carousel text-center">
                            <div class="item">
                                <div class="testimonial-box">
                                   <h4>Kelola SDM &amp; Keuangan Bisnis anda dengan lebih mudah secara Realtime Online. Akses dimanapun dan kapanpun dengan Layanan Bantuan 24 jam.</h4>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-box">
                                    <h4>Kelola.biz menjamin bahwa semua data Anda yang di masukkan dalam rangka menggunakan layanan kami akan selalu bersifat rahasia.</h4>
                                </div>
                            </div>
							<div class="item">
                                <div class="testimonial-box">
                                    <h4>Kelola.biz tidak dapat menjamin program aplikasi ini akan terus berjalan (uptime) 100%, namun kami akan memastikan dan berusaha agar aplikasi tersebut dapat berjalan uptime 90% setiap bulannya. Anda berhak untuk komplain terhadap uptime ini.</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- End Testimonials section -->


        <!-- PRICING -->
        <section class="section" id="pricing">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3 class="title">Harga</h3>
                        <!--p class="text-muted sub-title">Harga yang kami tawarkan sangat Simpel: <strong>GRATIS - Untuk 5 Pengguna</strong>.<br/> Hubungi Kami untuk Permintaan Fitur Baru/Instalasi Kelola.biz di Server/Komputer Anda.</p-->
                    </div>
                </div> <!-- end row -->


                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="row">

                            <!--Pricing Column-->
                            <article class="pricing-column col-lg-4 col-md-4">
                                <div class="inner-box active">
                                    <div class="plan-header text-center">
                                        <h3 class="plan-title">Paket Kelola 1</h3>
                                        <h2 class="plan-price">GRATIS</h2>
                                        <div class="plan-duration">Per Bulan</div>
                                    </div>
                                    <ul class="plan-stats list-unstyled">
                                        <li> <i class="pe-7s-server"></i>Fitur Didapat <br/><strong class="text-muted">Semua Fitur di Kelola.biz </strong></li>
										<li> <i class="pe-7s-server"></i>Jumlah Pegawai <br/><strong class="text-muted">~ Tidak Terbatas</strong></li>
                                        <li> <i class="pe-7s-server"></i>Jumlah Pengguna <br/><strong class="text-muted">5 Email</strong></li>
										<li> <i class="pe-7s-server"></i>Tambahan Pengguna <br/><strong class="text-muted">Rp. 59rb / Pengguna / Bulan</strong></li>
										<li> <i class="pe-7s-graph"></i>Layanan Pelanggan</li>
                                        <li> <i class="pe-7s-mail-open"></i>Gratis Update</li>
                                        <li> <i class="pe-7s-tools"></i>Tanpa Support</li>
                                    </ul>

                                    <div class="text-center">
                                        <a href="<?php echo site_url('register')?>" class="btn btn-sm btn-custom">Daftar Sekarang</a>
                                    </div>
                                </div>
                            </article>
							
							<!--Pricing Column-->
                            <article class="pricing-column col-lg-4 col-md-4">
                                <div class="inner-box active">
                                    <div class="plan-header text-center">
                                        <h3 class="plan-title">Paket Kelola 2</h3>
                                        <h2 class="plan-price">349.000</h2>
                                        <div class="plan-duration">Per Bulan</div>
                                    </div>
                                    <ul class="plan-stats list-unstyled">
                                        <li> <i class="pe-7s-server"></i>Fitur Didapat <br/><strong class="text-muted">Semua Fitur di Kelola.biz </strong></li>
										<li> <i class="pe-7s-server"></i>Jumlah Pegawai <br/><strong class="text-muted">~ Tidak Terbatas</strong></li>
                                        <li> <i class="pe-7s-server"></i>Jumlah Pengguna <br/><strong class="text-muted">50 Email</strong></li>
										<li> <i class="pe-7s-server"></i>Tambahan Pengguna <br/><strong class="text-muted">Rp. 29rb / Pengguna / Bulan</strong></li>
										<li> <i class="pe-7s-graph"></i>Layanan Pelanggan</li>
                                        <li> <i class="pe-7s-mail-open"></i>Gratis Update</li>
                                        <li> <i class="pe-7s-tools"></i>24x7 Support</li>
                                    </ul>

                                    <div class="text-center">
                                        <a href="<?php echo site_url('register')?>" class="btn btn-sm btn-custom">Daftar Sekarang</a>
                                    </div>
                                </div>
                            </article>


                            <!--Pricing Column-->
                            <article class="pricing-column col-lg-4 col-md-4">
                                <div class="inner-box">
                                    <div class="plan-header text-center">
                                        <h3 class="plan-title">Paket Kustom</h3>
                                        <h2 class="plan-price">Hub. Kami</h2>
                                        <div class="plan-duration">Per Lisensi</div>
                                    </div>
                                    <ul class="plan-stats list-unstyled">
                                        <li> <i class="pe-7s-server"></i>Fitur Didapat <br/><strong class="text-muted">Fitur Kustom dari Kelola.biz</strong></li>
										<li> <i class="pe-7s-server"></i>Jumlah Pegawai <br/><strong class="text-muted">~ Tidak Terbatas</strong></li>
                                        <li> <i class="pe-7s-server"></i>Jumlah Pengguna <br/><strong class="text-muted">~ Tidak Terbatas</strong></li>
										<li> <i class="pe-7s-server"></i>Tambahan Pengguna <br/><strong class="text-muted">Gratis</strong></li>
										<li> <i class="pe-7s-graph"></i>Layanan Pelanggan</li>
                                        <li> <i class="pe-7s-mail-open"></i>Gratis Update</li>
                                        <li> <i class="pe-7s-tools"></i>24x7 Support</li>
                                    </ul>

                                    <div class="text-center">
                                        <a href="mailto: mail@kelola.biz" class="btn btn-sm btn-custom">Hubungi Kami</a>
                                    </div>
                                </div>
                            </article>

                        </div>
                    </div><!-- end col -->
                </div>
                 <!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- End Pricing -->


        <!-- FOOTER -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <a class="navbar-brand logo" href="<?php echo site_url()?>">
                            <img src="<?php echo base_url()?>assets/images/logo_app_2.png" height="30" style="margin-top: -10px" />
                        </a>
                    </div>
                    <div class="col-lg-4 col-lg-offset-3 col-md-7">
                        <ul class="nav navbar-nav">
                            <li><a href="#features">Fitur</a></li>
                            <li><a href="#pricing">Harga</a></li>
                            <li><a href="<?php echo site_url('login')?>">Masuk</a></li>
							<li><a href="<?php echo site_url('register')?>">Daftar</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-2">
                        <ul class="social-icons">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="http://twitter.com/kelolabiz"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </footer>
        <!-- End Footer -->
        

        <!-- Back to top -->    
        <a href="#" class="back-to-top" id="back-to-top"> <i class="fa fa-angle-up"></i> </a>


        <!-- js placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url()?>assets/frontend/js/jquery-2.1.4.min.js"></script>
        <script src="<?php echo base_url()?>assets/frontend/js/bootstrap.min.js"></script>

        <!-- Jquery easing -->                                                      
        <script type="text/javascript" src="<?php echo base_url()?>assets/frontend/js/jquery.easing.1.3.min.js"></script>

        <!-- Owl Carousel -->                                                      
        <script type="text/javascript" src="<?php echo base_url()?>assets/frontend/js/owl.carousel.min.js"></script>

        <!--sticky header-->
        <script type="text/javascript" src="<?php echo base_url()?>assets/frontend/js/jquery.sticky.js"></script>

        <!--common script for all pages-->
        <script src="<?php echo base_url()?>assets/frontend/js/jquery.app.js"></script>

        <script type="text/javascript">
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:false,
                autoplay: true,
                autoplayTimeout: 4000,
                responsive:{
                    0:{
                        items:1
                    }
                }
            })
        </script>
    </body>
</html>