<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('vehicle')?>">Vehicle</a>
					</li>
					<li class="active"><?php echo $stitle?></li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	
	<div class="row">
		<?php if ($save_sts == '0'): ?>
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">
				<i class="mdi mdi-check-all"></i> Save failed. Police Number <b><?php echo $vehicle_number?></b> already used.
			</div>
		</div>
		<?php endif; ?>

		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
                <input type="hidden" class="form-control" id="vehicle_cd" name="vehicle_cd" value="<?php echo $vehicle_cd?>" required />
				<input type="hidden" class="form-control" id="vehicle_id" name="vehicle_id" value="<?php echo $vehicle_id?>" />
				<input type="hidden" class="form-control" id="insurance_no" name="insurance_no" value="<?php echo $insurance_no?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Truck Number <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="vehicle_cdx" name="vehicle_cdx" value="<?php echo $vehicle_cd?>"  disabled="disabled" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Police Number <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="vehicle_number" name="vehicle_number" value="<?php echo $vehicle_number?>"/>
								</div>
							</div>							
							<div class="form-group">
								<label class="col-md-4 control-label">Vehicle Type</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="vehicle_type" name="vehicle_type" value="<?php echo $vehicle_type?>" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Owner</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="vehicle_owner" name="vehicle_owner" value="<?php echo $vehicle_owner?>" />
								</div>
							</div>							
							<div class="form-group">
								<label class="col-md-4 control-label">Brand</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control" id="brand" name="brand" value="<?php echo $brand?>" />
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-4 control-label">Active</label>
								<div class="col-md-8 m-t-5">
                                    <select id="active" name="active" class="form-control select2">
										<option value="1" <?php echo ($active == '1') ? 'selected="selected"' : ''?>>Yes</option>
										<option value="0" <?php echo ($active == '0') ? 'selected="selected"' : ''?>>No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Business</label>
								<div class="col-md-8 m-t-5">
                                    <select name="business" id="business" class="form-control select2">
										<option value="MILKRUN" <?php echo ($business == 'MILKRUN') ? 'selected="selected"' : ''?>>MILKRUN</option>
										<option value="REGULER" <?php echo ($business == 'REGULER') ? 'selected="selected"' : ''?>>REGULER</option>
									</select>
								</div>
                            </div>
						</div>
						<div class="col-sm-6">							                            
                            <div class="form-group">
								<label class="col-md-4 control-label">License STNK</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control datepicker" id="license_stnk" name="license_stnk" value="<?php echo ($license_stnk != '') ? date('d/m/Y', strtotime($license_stnk)) : ''?>" />
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-4 control-label">License KEUR</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control datepicker" id="license_keur" name="license_keur" value="<?php echo ($license_keur != '') ? date('d/m/Y', strtotime($license_keur)) : ''?>" />
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-4 control-label">License SIPA</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control datepicker" id="license_sipa" name="license_sipa" value="<?php echo ($license_sipa != '') ? date('d/m/Y', strtotime($license_sipa)) : ''?>" />
								</div>
                            </div>
                            <div class="form-group">
								<label class="col-md-4 control-label">License IBM</label>
								<div class="col-md-8 m-t-5">
                                    <input type="text" class="form-control datepicker" id="license_ibm" name="license_ibm" value="<?php echo ($license_ibm != '') ? date('d/m/Y', strtotime($license_ibm)) : ''?>" />
								</div>
                            </div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('vehicle')?>'">Back</button>
						<?php echo create_button($this->button, "btn_submit"); ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>