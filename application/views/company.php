<div class="container">

	<!--div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title">Perusahaan</h4>
				<ol class="breadcrumb p-0 m-0">
                    <li>Beranda</li>
					<li class="active">Perusahaan</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div-->	
			
	<div class="row m-t-20 m-b-20">
		<?php if ($this->session->flashdata('notif_company_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_company_success')?>
			</div>
		</div>
		<?php endif; ?>

		<?php if ($cnt == 0):?>
		<div class="col-sm-12">
			<p class="">Halo <?php echo $this->session->userdata(S_EMPLOYEE_NAME)?>, Look like you don't have any company. Create one.</p>
		</div>	
		<?php endif;?>
	
		<!-- <div class="col-sm-6">			
			<button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('company/create')?>'"><i class="mdi mdi-plus"></i> Create New Company</button>
		</div> -->

		<?php if ($cnt > 0):?>
		<div class="col-sm-6 text-right">			
			*) Click on company name to start using.
		</div>
		<?php endif;?>
	</div>
	
	<?php if ($cnt > 0):?>
	<div class="row">
		
		<div class="col-sm-12">

			<div class="card-box table-responsive">
				<table id="datatable" class="table table-striped table-hover table-custom">
					<thead>
					<tr>
						<th>Company Name</th>
						<th>Address</th>
						<th>Industry</th>
                        <!-- <th>Option</th> -->
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<!-- end row -->
	<?php endif;?>
	
</div>