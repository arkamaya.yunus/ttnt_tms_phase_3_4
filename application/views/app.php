<div class="container">

	
	<div class="row m-t-20">
		
		<div class="col-md-8">
			<div class="panel panel-default panel-border">
				<div class="panel-body">
					
					<div class="row">	
						
						<?php foreach ($apps as $a): ?>
						<div class="col-lg-4 col-md-6">
							<a href="<?php echo $a->function_controller?>">
							<div class="card-box widget-box-two widget-two-<?php echo $a->app_bgcolor?> card-box-special">
								<i class="<?php echo $a->app_icon?> widget-two-icon widget-two-icon-special"></i>
								<div class="wigdet-two-content m-b-5 p-b-10">
									<h3 class="ahov"><span data-plugin="counterup"><?php echo $a->app_label?></span></h3>
								</div>
							</div>
							</a>
						</div>
						<?php endforeach; ?>												

					</div>

					
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-default panel-border">
				<div class="panel-heading">
					<h3 class="panel-title">Paket Enterprise (Aktif)</h3>
				</div>
				<div class="panel-body table-responsive">
					
					<table class="table table-striped m-0 table-hover">
						<tbody>
							<tr>
								<th>Perusahaan</th>
								<td><?php echo $company->company_name?></td>
							</tr>
							<tr>
								<th>Terdaftar</th>
								<td><?php echo date('d F Y', strtotime($company->created_dt))?></td>
							</tr>
							<tr>
								<th>Expired</th>
								<td><?php echo $company->trial_expired_date?> (<?php echo $company->left_day?> Hari)</td>
							</tr>
							<tr>
								<th>Tgl Pembayaran</th>
								<td>
									<?php
										$next_payment_dt = date_create($company->trial_expired_dt);
										date_add($next_payment_dt, date_interval_create_from_date_string('-5 days'));										
										echo  date_format($next_payment_dt, 'd F Y');
									?>
								</td>
							</tr>
							<tr>
								<th>Jumlah Pengguna</th>
								<td><?php echo $company->num_of_users?> Pengguna, <?php echo $company->num_of_addon_users?> Tambahan</td>
							</tr>
							<tr>
								<td colspan=2">&nbsp;</td>
							</tr>

							<tr>
								<th>Akun</th>
								<td>
									<?php echo $this->session->userdata(S_USER_EMAIL)?>
									<br/><br/>
									<a href="javascript:;">Kelola Akun Saya</a>
									<br/>
									<a href="javascript:;" class="text-danger">Hapus Akun</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div> <!-- container -->