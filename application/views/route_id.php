<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?></h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Master Data</li>
					<li>
						<a href="<?php echo site_url('route')?>">Route</a>
					</li>
					<li class="active">Detail</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->
	
	<div class="row">
		<?php if ($this->session->flashdata('notif_route_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_route_success')?>
			</div>
		</div>
		<?php endif; ?>

		<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="myModalLabel">Delete</h4>
					</div>
					<div class="modal-body">
						Are you sure?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-danger waves-effect waves-light" onclick="doDelete()" id="btn_delete_confirm">Delete</button>
					</div>
				</div>
			</div>
		</div>
	
		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<input type="hidden" name="route" id="route" value="<?php echo $this->uri->segment(3)?>" />
				<input type="hidden" name="cycle" id="cycle" value="<?php echo $this->uri->segment(4)?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Customer <span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">
									<?php echo $dataRoute->customer ?>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">							
							<div class="form-group">
								<label class="col-md-4 control-label">Route<span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">									
                                   <?php echo $dataRoute->route ?>
								</div>
							</div>                           
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">							
							<div class="form-group">
								<label class="col-md-4 control-label">Cycle<span class="text-danger">*</span></label>
								<div class="col-md-8 m-t-5">									
                                   <?php echo $dataRoute->cycle ?>
								</div>
							</div>                           
						</div>
					</div>
					<br>
					<hr>
					<div class="row">
						<div class="col-sm-12">							
							<div class="form-group">
								<label class="col-md-4 control-label">E-Money</label>
							</div>		
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="" style="overflow-y: hidden">
								<table id="table-emoney" class="table-small-fonts table m-0 table-colored table-custom table-hover table-striped" style="width:50%">
									<thead>
										<tr>
											<th>No. Kartu</th>											
											<th>Gelombang</th>
										</tr>
									</thead>
									<tbody class="details">
										<?php foreach ($emoney_detail as $q): ?>
										<tr class="emoney-details">
											<td width="50%">
												<?php echo $q->emoney_id ?>
											</td>
											<td width="40%">
												<?php echo $q->gel ?>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>	
					<hr>
					<div class="row">
						<div class="col-sm-12">							
							<div class="form-group">
								<label class="col-md-4 control-label">Bensin</label>
							</div>		
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="" style="overflow-y: hidden">
								<table id="table-bensin" class="table-small-fonts table m-0 table-colored table-custom table-hover table-striped" style="width:50%">
									<thead>
										<tr>
											<th>SPBU</th>											
											<th>Jumlah Liter</th>
										</tr>
									</thead>
									<tbody class="details">
										<?php foreach ($fuel_detail as $q): ?>
										<tr class="bensin-details">
											<td width="50%">
												<?php echo $q->spbu ?>
											</td>
											<td width="40%">
												<?php echo $q->liter ?>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>	
					<hr>
					<div class="row">
						<div class="col-sm-12">							
							<div class="form-group">
								<label class="col-md-4 control-label">Others</label>
							</div>		
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="" style="overflow-y: hidden">
								<table id="table-other" class="table-small-fonts table m-0 table-colored table-custom table-hover table-striped" style="width:50%">
									<thead>
										<tr>
											<th>Kategori</th>											
											<th>Jumlah</th>
										</tr>
									</thead>
									<tbody class="details">
										<?php foreach ($other_detail as $q): ?>
										<tr class="other-details">
											<td width="50%">
												<?php echo $q->others ?>
											</td>
											<td width="40%">
												<?php echo $q->amount_text ?>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>	
				</div>				
				<div class="row">
					<div class="col-md-6">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" onclick="window.location='<?php echo site_url('route')?>'">Back</button>
						<?php 
							$route = $this->uri->segment(3);
							$cycle = $this->uri->segment(4);							
							$param   = $route.'/'.$cycle;
							$uri = '';
							echo create_button($this->button, "btn_edit", $uri, $param); 
						?>
                        <?php echo create_button($this->button, "btn_delete"); ?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>