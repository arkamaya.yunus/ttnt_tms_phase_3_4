<!DOCTYPE html>
<html class="account-pages-bg">
    <head>        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo APP_NAME ?>">
        <meta name="author" content="irfan.satriadarma@gmail.com">

        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon2.png">
        <title>Pendaftaran - <?php echo APP_NAME ?></title>

        <!-- App css -->
        <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <style>
            .account-logo-box {
                background-color: transparent !important;
                padding: 20px 10px 5px 10px; 
            }
            .account-pages .account-content {
                padding: 20px;
            }
            h2 {
                font-family: "Varela Round",sans-serif;
            }
            label {
                font-weight: normal;
            }
            .error .bootstrap-select .dropdown-toggle, .has-error .bootstrap-select .dropdown-toggle {
                border-color: #f5707a;
            }
            .error .bootstrap-select .dropdown-toggle, .has-success .bootstrap-select .dropdown-toggle {
                border-color: #4bd396;
            }
        </style>
		
        <script>
            var SITE_URL = '<?php echo base_url(); ?>';
        </script>
    </head>


    <body class="bg-transparent">

		<section>
            <div class="container-alt">
                <div class="row">
                    <div class="col-sm-9 center-page">
                        <div class="wrapper">
                            <div class="m-t-50 account-pages">
                                <div class="account-content" style="border-radius: 5px 5px 5px 5px;">
									<div class="row">
										<div class="col-sm-6">
											<a href="<?php echo site_url(); ?>" class="text-success">
												<span><img src="<?php echo base_url(); ?>assets/images/logo-text-dark.png" alt="<?php echo APP_NAME?>" height="40"></span>
											</a>
										</div>
										<div class="col-sm-6 m-t-20">
											<p class="lead">Pendaftaran <span class="text-inverse">Akun</span></p>
										</div>
									</div>
									<hr class="m-t-0"/>
                                    <div class="row">
                                        <div class="col-md-12">
                                                <section>													
                                                    <div class="row">
														<form class="form-horizontal" action="<?php echo site_url('register/process');?>" id="register" method="post">
															<div class="col-sm-6">
																<input class="form-control" name="industry_type" value="" type="hidden" />
																<input class="form-control" name="package_id" value="1" type="hidden" />
															
																<?php if ($this->session->flashdata('notif_register') != ''): ?>
																<div class="form-group">
																	<div class="col-sm-12">
																		<div class="text-muted alert alert-<?php echo ($this->session->flashdata('notif_status') == 'success') ? 'success' : 'danger' ?> alert-dismissible fade in" role="alert">
																			<?php echo $this->session->flashdata('notif_register') ?>
																		</div>
																	</div>
																</div>
																<?php endif; ?>
																
																<div class="form-group item">
																	<div class="col-xs-12">
																		<input class="form-control" type="text" name="name" required="" placeholder="Nama Lengkap">
																	</div>
																</div>
																<!--div class="form-group item">
																	<div class="col-xs-12">
																		<input class="form-control" type="text" name="company_name" required="" placeholder="Nama Perusahaan">
																	</div>
																</div-->
																<div class="form-group item">
																	<div class="col-xs-12">
																		<input class="form-control" name="email" type="email" required="" placeholder="Email">
																	</div>
																</div>
																<div class="form-group item">
																	<div class="col-xs-12">
																		<input class="form-control" name="phone" type="text" required="" placeholder="Telepon">
																	</div>
																</div>

																<div class="form-group item">
																	<div class="col-xs-12">
																		<input class="form-control" id="password" name="password" type="password" required="" placeholder="Kata Sandi">
																	</div>
																</div>
																<div class="form-group item">
																	<div class="col-xs-12">
																		<input class="form-control" id="confirm_password" name="confirm_password" type="password" required="" placeholder="Tulis Ulang Kata Sandi">
																	</div>
																</div>
																<div class="form-group item">
																	<div class="col-xs-12">
																		<div class="checkbox checkbox-primary">
																			<input type="checkbox" id="agree" name="agree" checked="checked">
																			<label for="checkbox-signup">Saya Setuju, <a href="javascript:;" id="txtTermCondition">Syarat dan Kententuan</a></label>
																		</div>
																	</div>
																</div>																
																<div class="form-group m-t-10">
																	<div class="col-xs-12">
																		<button class="form-control btn btn-bordered btn-custom btnRegister" type="button" onclick="on_register()" >Daftarkan Akun Sekarang</button>
																	</div>
																</div>
																<div class="form-group m-t-10">
																	<div class="col-sm-12">
																		<a href="<?php echo base_url(); ?>login"><i class="fa fa-sign-in m-r-5"></i> Sudah punya akun? Login</a> <br/>
																		<a href="<?php echo base_url(); ?>register/resend_activation"><i class="fa fa-bell m-r-5"></i> Kirim Ulang Kode Aktivasi</a>
																	</div>
																</div>
															</div>
														</form>

														<div class="col-sm-6">																															
															<!--div class="card-box widget-two-default detail-package">
																<h2 class="header-title m-t-0">Daftar Sekarang untuk gratis masa percobaan selama <strong class="text-danger"><?php echo $trial_days; ?> Hari</strong>.</h2>                                                                
															</div-->
															<div class="card-box widget-two-default">
																<!--h2 class="header-title m-t-0 m-b-20">Kelola Bisnis Anda  dengan lebih mudah secara <i class="text-primary">Real-time Online</i> dengan <?php echo APP_NAME?> </h2-->

																<div class="inbox-widget">
																	<a href="#">
																		<div class="inbox-item">         
																			<div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/icons/money_transfer.svg" alt=""></div>
																			<p class="inbox-item-author">Penjualan </p>
																			<p class="inbox-item-text">Kelola penjualan dan customer.</p>
																		</div>
																	</a>
																	<a href="#">
																		<div class="inbox-item">
																			<div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/icons/shipped.svg" alt=""></div>
																			<p class="inbox-item-author"> Pembelian </p>
																			<p class="inbox-item-text">Kelola pembelian dan supplier.</p>
																		</div>
																	</a>
																	<a href="#">
																		<div class="inbox-item">
																			<div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/icons/bullish.svg" alt=""></div>
																			<p class="inbox-item-author"> Akuntansi </p>
																			<p class="inbox-item-text"> Kelola Jurnal & Laporan Keuangan. </p>
																		</div>
																	</a>
                                                                    <a href="#">
																		<div class="inbox-item">
																			<div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/icons/calculator.svg" alt=""></div>
																			<p class="inbox-item-author"> Penggajian </p>
																			<p class="inbox-item-text"> Kelola pegawai dan penggajian. </p>
																		</div>
																	</a>
																</div>
															</div>
														</div>														
                                                    </div>
                                                </section>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
																	
									<div class="row">
										<div class="col-sm-12 text-center">
											<p class="text-muted">&copy; <?php echo date('Y')?> <span><?php echo APP_NAME?></span></p>
										</div>
									</div>
                                </div>
                            </div>                            
                        </div>
                    </div>

                    <div id="modal_termcondition" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myLargeModalLabel">SYARAT DAN KETENTUAN</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="inbox-widget slimscroll-alt mx-box">
                                        <div class="panel-group panel-group-joined" id="accordion-test">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title text-dark">
                                                        <a data-toggle="collapse" data-parent="#accordion-test" href="#collapseOne" class="collapsed">
                                                            Persyaratan Penggunaan 
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <ul>
															<li class="m-b-5">Sebelum dapat menggunakan program <?php echo APP_NAME?> anda diwajibkan untuk mendaftarkan diri dengan menggunakan Informasi lengkap dan benar, hal ini diperlukan untuk proses verifikasi keabsahan data anda</li>
															<li class="m-b-5">Pendaftar minimal berusia 18 tahun atau lebih untuk dapat menggunakan layanan ini</li>
															<li class="m-b-5">Anda tidak dapat menggunakan <?php echo APP_NAME?> untuk tujuan ilegal atau melanggar hukum di wilayah yurisdiksi anda</li>
															<li class="m-b-5">Anda bertanggung jawab sepenuhnya untuk semua aktivitas dan konten (data, gambar, dokumen, dll) yang ditambahkan dalam aplikasi <?php echo APP_NAME?></li>
															<li class="m-b-5">Anda diwajibkan menggunakan <?php echo APP_NAME?> dengan sebaik-baiknya dan di larang keras mengirimkan virus atau kode yang bersifat merusak program <?php echo APP_NAME?>. <?php echo APP_NAME?> berhak untuk mengakhiri penggunaan akun Anda dalam kasus pelanggaran atau pelanggaran syarat dan ketentuan kami, tanpa pemberitahuan terlebih dahulu</li>												
															<li class="m-b-5"><?php echo APP_NAME?> berhak untuk menolak layanan ini kepada siapa pun tanpa memberikan alasan apapun. Semua resiko yang timbul dalam menggunakan program <?php echo APP_NAME?> akan menjadi resiko sendiri dan tidak dapat menuntut pihak penyedia jasa dalam bentuk apapun</li>												
															<li class="m-b-5">Dukungan teknis hanya disediakan untuk pemegang akun yang berbayar baik melalui email ataupun fasilitas lainnya yang bisa kami sediakan</li>
															<li class="m-b-5">Anda setuju untuk tidak mereproduksi, menduplikasi, menyalin, menjual, menjual kembali atau mengeksploitasi bagian manapun dari layanan, penggunaan layanan, atau akses ke layanan tanpa izin tertulis oleh <?php echo APP_NAME?></li>
														</ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title text-dark">
                                                        <a data-toggle="collapse" data-parent="#accordion-test" href="#collapseTwo" class="collapsed">
                                                            Privasi Dalam Informasi
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <ul>
                                                            <li class="m-b-5"><?php echo APP_NAME?> menjamin bahwa semua password yang di daftarkan dalam rangka menggunakan layanan kami akan selalu bersifat rahasia, kami akan selalu mengenkripsi semua password yang di daftarkan. Dalam hal pemulihan password, kami hanya dapat mengganti (reset) password tanpa bisa mengetahui password lama anda</li>
															<li class="m-b-5">Anda harus menjaga kerahasiaan password anda sehingga tidak ada pihak lain yang dapat menyalahgunakan data-data yang ada di aplikasi <?php echo APP_NAME?>, <?php echo APP_NAME?> tidak akan bertanggung jawab atas kerugian atau penyalahgunaan akun anda oleh pihak lain yang di karenakan dari kelalaian anda menjaga keamanan akun dan password</li>
															
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title text-dark">
                                                        <a data-toggle="collapse" data-parent="#accordion-test" href="#collapseThree" class="collapsed">
                                                            Ketentuan Layanan
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <ul>
                                                            <li class="m-b-5"><?php echo APP_NAME?> tidak dapat menjamin program aplikasi ini akan terus berjalan (uptime) 100%, namun kami akan memastikan dan berusaha agar aplikasi tersebut dapat berjalan uptime 90% setiap bulannya. Anda berhak untuk komplain terhadap uptime ini.</li>
															<li class="m-b-5"><?php echo APP_NAME?> tidak menjamin keandalan dan akurasi hasil yang diperoleh dari penggunaan layanan ini, tetapi kami sangat terbuka apabila ada saran dari pengguna untuk dapat meningkatkan kualitas dari program aplikasi tersebut</li>
															<li class="m-b-5"><?php echo APP_NAME?> berhak untuk menghapus isi dan akun yang mengandung konten  yang bersifat melanggar hukum, menyinggung, mengancam, memfitnah,  pornografi atau tidak pantas atau melanggar kekayaan intelektual pihak <?php echo APP_NAME?> atau pihak lain atau Ketentuan Layanan ini</li>
															<li class="m-b-5"><?php echo APP_NAME?> tidak menjamin bahwa kualitas setiap produk, layanan, informasi, atau bahan lain yang dibeli atau diperoleh oleh melalui layanan kami akan memenuhi harapan anda, atau bahwa setiap kesalahan dalam Layanan akan diperbaiki secara berkesinambungan</li>
															<li class="m-b-5"><?php echo APP_NAME?> tidak bertanggung jawab untuk kerugian langsung, tidak langsung, insidental, khusus, konsekuensial atau teladan, termasuk namun tidak terbatas pada, kerusakan untuk kehilangan keuntungan, penggunaan, data atau kerugian tak berwujud lain yang dihasilkan dari penggunaan atau ketidakmampuan untuk menggunakan layanan.</li>
															
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/detect.js"></script>
        <script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url() ?>assets/js/waves.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/plugins/jquery-validate/jquery.validate.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.core.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.app.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/js/apps/register.js"></script>
    </body>

</html>