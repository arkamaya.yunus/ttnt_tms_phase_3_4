<style>
.infotable tr th,
.infotable tr td
{
	font-size: 12px !important;
}
.pb-tt
{
	margin-bottom: 0 !important;
    border: 1px solid #dadada;
}

table#datatable_timetable.dataTable tbody tr:hover
/* , table#datatable_timetable1.dataTable tbody tr:hover  */
{
  background-color: #d8e2f0;cursor: pointer;
}

table#datatable_timetable.dataTable tbody tr:hover > .sorting_1
/* , table#datatable_timetable1.dataTable tbody tr:hover > .sorting_1  */
{
  background-color: #d8e2f0;cursor: pointer;
}

.table-responsive
{
	border: none;
}

#datatable_timetable1_wrapper tr th,
#datatable_timetable1_wrapper tr td
{
	font-size: 12px !important;
}

.waves-effect
{
	z-index: 0 !important;
}
</style>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-title-box">
				<h4 class="page-title"><?php echo $stitle?>
					<?php if ($this->uri->segment(4) == 'nya'): ?>
					- Not Yet Arrived. <a href="<?php echo site_url('operation')?>" style="font-size: 12px">show all</a>
					<?php endif; ?>
				</h4>
				<ol class="breadcrumb p-0 m-0">
					<li>Transaction</li>
					<li class="active">
						<a href="<?php echo site_url('operation')?>">Operation</a>
					</li>
				</ol>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- end row -->

	<div id="myChangeTruck" class="modal fade" role="dialog" aria-labelledby="myChangeTruckLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myChangeTruckLabel">Change Truck or Driver</h4>
					<input type="hidden" id="change_truck_timetable_id" value="" />
					<input type="hidden" id="change_w" value="" />
					<input type="hidden" id="change_truck_route" value="" />
					<input type="hidden" id="change_truck_cycle" value="" />
				</div>
				<div class="modal-body form-horizontal">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-5 control-label">Route</label>
								<div class="col-md-7 m-t-5" id="change_truck_route-d-0"></div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Cycle</label>
								<div class="col-md-7 m-t-5" id="change_truck_route-d-1"></div>
							</div>
							<div class="form-group change-w change_w-truck">
								<label class="col-md-5 control-label">Truck Number</label>
								<div class="col-md-7 m-t-5" id="">
									<select id="change_truck_route-d1-vehicle_cd" class="form-control select2">                    					
										<?php foreach ($vehicles as $v): ?>
										<option value="<?php echo $v->vehicle_cd?>"><?php echo $v->vehicle_cd?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group change-w change_w-driver">
								<label class="col-md-5 control-label">Driver</label>
								<div class="col-md-7 m-t-5" id="">
									<select id="change_truck_route-d1-driver_cd" class="form-control select2">                    					
										<?php foreach ($drivers as $d): ?>
										<option value="<?php echo $d->driver_cd?>"><?php echo $d->driver_name?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>							
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>						
					<button type="button" class="btn btn-primary waves-effect waves-light" onclick="doSaveChangeTW()" id="btn_SaveTW">Save</button>
				</div>
			</div>
		</div>
	</div>

	<div id="myModalRC" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalRCLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalRCLabel">Delete</h4>
				</div>
				<div class="modal-body">
					Are you sure to delete all details for Route [<span id="lbl-rc-route"></span>] and Cycle [<span id="lbl-rc-cycle"></span>] ?
				</div>
				<div class="modal-footer">
					<input type="hidden" id="del-rc-timetable_id" />
					<input type="hidden" id="del-rc-route" />
					<input type="hidden" id="del-rc-cycle" />
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-danger waves-effect waves-light" onclick="doDeleteRC()" id="btn_delete_rc_confirm">Delete</button>
				</div>
			</div>
		</div>
	</div>

	<div id="myNewLP" class="modal fade" role="dialog" aria-labelledby="myNewLPLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myNewLPLabel">Add New LP</h4>						
				</div>
				<div class="modal-body form-horizontal">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Route</label>
								<div class="col-md-8 m-t-5" id="nlp-r"></div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Cycle</label>
								<div class="col-md-8 m-t-5" id="nlp-c"></div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Truck Number</label>
								<div class="col-md-8 m-t-5" id="">
									<select id="nlp-vehicle_cd" class="form-control select2">                    					
										<?php foreach ($vehicles as $v): ?>
										<option value="<?php echo $v->vehicle_cd?>"><?php echo $v->vehicle_cd?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Driver</label>
								<div class="col-md-8 m-t-5" id="">
									<select id="nlp-driver_cd" class="form-control select2">                    					
										<?php foreach ($drivers as $d): ?>
										<option value="<?php echo $d->driver_cd?>"><?php echo $d->driver_name?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Logistic Point</label>
								<div class="col-md-8 m-t-5" id="">
									<select id="nlp-lp_cd" class="form-control select2">                    					
										<?php foreach ($lps as $lp): ?>
										<option value="<?php echo $lp->lp_cd?>"><?php echo $lp->lp_cd?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Arrival Date</label>
								<div class="col-md-8 m-t-5" id="">
									<input type="text" id="nlp-arrival_dt" class="form-control datepicker" value="" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Arrival Time</label>
								<div class="col-md-8 m-t-5" id="">
									<input type="text" id="nlp-arrival_plan" class="form-control timepicker" value="00:00" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Departure Date</label>
								<div class="col-md-8 m-t-5" id="">
									<input type="text" id="nlp-departure_dt" class="form-control datepicker" value="" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Departure Time</label>
								<div class="col-md-8 m-t-5" id="">
									<input type="text" id="nlp-departure_plan" class="form-control timepicker" value="00:00" />
								</div>
							</div>															
						</div>							
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>						
					<button type="button" class="btn btn-primary waves-effect waves-light" onclick="doSaveNLP()" id="btn_saving_nlp">Save</button>
				</div>
			</div>
		</div>
	</div>

	<div id="myTimeTable" class="modal fade" role="dialog" aria-labelledby="myTimeTableLabel" aria-hidden="true">
		<div class="modal-dialog modal-full">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myTimeTableLabel">Timetable Detail</h4>
					<input type="hidden" id="timetable_detail_id" value="" />
				</div>
				<div class="modal-body form-horizontal" style="height: 500px; overflow-y: scroll">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="col-md-4 control-label">Route</label>
								<div class="col-md-8 m-t-5" id="d-0"></div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Cycle</label>
								<div class="col-md-8 m-t-5" id="d-1"></div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Truck Number</label>
								<div class="col-md-8 m-t-5" id="">
									<select id="d-vehicle_cd" class="form-control select2">                    					
										<?php foreach ($vehicles as $v): ?>
										<option value="<?php echo $v->vehicle_cd?>"><?php echo $v->vehicle_cd?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Driver</label>
								<div class="col-md-8 m-t-5" id="">
									<select id="d-driver_cd" class="form-control select2">                    					
										<?php foreach ($drivers as $d): ?>
										<option value="<?php echo $d->driver_cd?>"><?php echo $d->driver_name?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Logistic Point</label>
								<div class="col-md-8 m-t-5" id="d-6"></div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">KM</label>
								<div class="col-md-8 m-t-5">
									<input type="text" id="km" class="form-control autonumber2 text-right" value="" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Fuel</label>
								<div class="col-md-8 m-t-5">
									<input type="text" id="fuel" class="form-control autonumber2 text-right" value="" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">SPBU</label>
								<div class="col-md-8 m-t-5">
									<input type="text" id="spbu" class="form-control" value="" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Tol (Rp)</label>
								<div class="col-md-8 m-t-5">
									<input type="text" id="tol" class="form-control autonumber2 text-right" value="" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Others (Rp)</label>
								<div class="col-md-8 m-t-5">
									<input type="text" id="others" class="form-control autonumber2 text-right" value="" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Pallet (pcs)</label>
								<div class="col-md-8 m-t-5">
									<input type="text" id="pallet" class="form-control autonumber2 text-right" value="" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Rack (pcs)</label>
								<div class="col-md-8 m-t-5">
									<input type="text" id="rack" class="form-control autonumber2 text-right" value="" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Division</label>
								<div class="col-md-8 m-t-5">
									<input type="text" id="division" class="form-control" value="" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12 control-label">Remark :</label>
								<div class="col-md-12 m-t-20">
									<textarea class="form-control" rows="3" name="remark" id="remark"></textarea>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="col-md-12 control-label">ARRIVAL</label>
								<div class="col-md-12 m-t-10">
										<table class="infotable table table-sm table-stripped table-hover table-colored-bordered table-bordered-info">
											<thead>
												<tr>
													<th>Date</th>
													<th>Plan</th>
													<th>Actual</th>
												</th>													
											</thead>
											<tbody>
												<tr>
													<td id="d-7"></td>
													<td id="d-8"></td>
													<td width="150">
														<span id="d-9"></span>
														<span id="d-9-setq"><button type="button" class="btn btn-xs btn-purple" onclick="$('#d-9-set').show();$('#d-9-setq').hide();">Set Actual</button></span>
														<input type="text" id="d-9-set" class="col-md-3 form-control timepicker" value="00:00" />
													</td>
											</tbody>
										</table>
										<sup id="d-arrival_manual"></sup>

										<table class="infotable table table-sm table-stripped table-hover table-colored-bordered table-bordered-info">
											<thead>
												<tr>
													<th>Gap</th>
													<th>Eval</th>
												</th>													
											</thead>
											<tbody>
												<tr>
													<td id="d-10"></td>
													<td id="d-11"></td>
											</tbody>
										</table>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-12 control-label">Arrival Problem :</label>
								<div class="col-md-12 m-t-5">
									<textarea class="form-control" rows="5" name="arrival_problem" id="arrival_problem"></textarea>
								</div>
							</div>
														
						</div>
						<div class="col-sm-4">                            
							<div class="form-group">
							<label class="col-md-12 control-label">DEPARTURE</label>
								<div class="col-md-12 m-t-10">
										<table class="infotable table table-sm table-stripped table-hover table-colored-bordered table-bordered-info">
											<thead>
												<tr>
													<th>Date</th>
													<th>Plan</th>
													<th>Actual</th>
												</th>													
											</thead>
											<tbody>
												<tr>
													<td id="d-12"></td>
													<td id="d-13"></td>
													<td>
														<span id="d-14"></span>
														<span id="d-14-setq"><button type="button" class="btn btn-xs btn-purple" onclick="$('#d-14-set').show();$('#d-14-setq').hide();">Set Actual</button></span>
														<input type="text" id="d-14-set" class="col-md-3 form-control timepicker" value="00:00" />
													</td>
											</tbody>
										</table>
										<sup id="d-departure_manual"></sup>

										<table class="infotable table table-sm table-stripped table-hover table-colored-bordered table-bordered-info">
											<thead>
												<tr>
													<th>Gap</th>
													<th>Eval</th>
												</th>													
											</thead>
											<tbody>
												<tr>
													<td id="d-15"></td>
													<td id="d-16"></td>
											</tbody>
										</table>
								</div>										
							</div>	
							<div class="form-group">
								<label class="col-md-12 control-label">Departure Problem : </label>
								<div class="col-md-12 m-t-5">
									<textarea class="form-control" rows="5" name="departure_problem" id="departure_problem"></textarea>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>						
					<button type="button" class="btn btn-primary waves-effect waves-light" onclick="doSaveTimeTableDetail()" id="btn_saving_timetable_detail">Save</button>
					<button type="button" class="btn btn-danger waves-effect waves-light pull-left" onclick="doDeleteNLP()" id="btn_delete_nlp" onclick="doDeleteNLP()">Delete</button>
					<span class="pull-left m-t-5" id="lbl-ays"> &nbsp;&nbsp; &nbsp;&nbsp; Are You Sure? <a href="javascript:;" onclick="proceedDeleteNLP()">Yes</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="javascript:;" onclick="$('#lbl-ays').hide();">No</a> </span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<?php if ($this->session->flashdata('notif_success') != ''): ?>
		<div class="col-sm-12">
			<div class="alert alert-success" role="alert">
				<i class="mdi mdi-check-all"></i> <?php echo $this->session->flashdata('notif_success')?>
			</div>
		</div>
		<?php endif; ?>		
	
		<div class="col-sm-12">
			<form id="frm" class="form-horizontal" role="form" method="post" action="">
				<input type="hidden" id="nya" value="<?php echo $nya?>" />
				<div class="card-box">
					<div class="row">
						<div class="col-md-6">                                                
							<div class="form-group">
								<label class="col-md-4 control-label">Planning Date</label>
								<div class="col-md-4 m-t-5">
                                    <input type="text" class="form-control datepicker f1" id="timetable_dt" name="timetable_dt" value="<?php echo date('d/m/Y', strtotime($timetable_dt))?>" onchange="changeTimetableDt()" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Business</label>
								<div class="col-md-8 m-t-5">
                                    <select id="business" class="form-control f1" onchange="changeBusiness()">
                                        <option value="all">All Business</option>
                                        <?php foreach ($business as $b): ?>
                                        <option value="<?php echo $b->business?>"><?php echo $b->business?></option>
                                        <?php endforeach; ?>
                                    </select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Customer</label>
								<div class="col-md-8 m-t-5">
                                    <select id="customer" class="form-control f1" onchange="changeCustomer()">
                                        <option value="all">All Customer</option>
                                        <?php foreach ($customers as $c): ?>
                                        <option value="<?php echo $c->customer?>" class="s2-business s2-business-<?php echo $c->business?>"><?php echo $c->customer . ' - ' . $c->system_value_txt?></option>
                                        <?php endforeach; ?>
                                    </select>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Customer Logistic Point</label>
								<div class="col-md-8 m-t-5">
                                    <select id="customer_lp_cd" class="form-control f1" onchange="changeCustomerLP()">                    					
                                        <option value="all">All Customer Logistic Point</option>
                                        <?php foreach ($customer_lp_cds as $c): ?>
                                        <option value="<?php echo $c->customer_lp_cd?>" class="s2-customer s2-customer-<?php echo $c->customer?>"><?php echo $c->customer_lp_cd?></option>
                                        <?php endforeach; ?>
                                    </select>
								</div>
							</div>
                            <div class="form-group">
								<label class="col-md-4 control-label">Route</label>
								<div class="col-md-8 m-t-5">
                                    <select id="route" class="form-control f1" onchange="changeRoute()">
                                        <option value="all">All Route</option>
                                        <?php foreach ($routes as $r): ?>
                                        <option value="<?php echo $r->route?>" class="s2-customer_lp_cd s2-customer_lp_cd-<?php echo str_replace(' ', '_', $r->customer_lp_cd)?>"><?php echo $r->route?></option>
                                        <?php endforeach; ?>
                                    </select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Cycle</label>
								<div class="col-md-8 m-t-5">
                                    <select id="cycle" class="form-control f1">                    					
                                        <option value="all">All Cycle</option>
                                        <?php foreach ($cycles as $r): ?>
                                        <option value="<?php echo $r->cycle?>" class="s2-route s2-route-<?php echo str_replace(' ', '_', $r->route)?> s2-cycle">
											<?php echo $r->cycle?></option>
                                        <?php endforeach; ?>
                                    </select>
								</div>
							</div>
						</div>
					</div>
					<div class="row timetable-detail timetable-detail-1">
						<div class="col-md-12">  
							<button type="button" class="pull-right btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" id="btnFilter" onclick="doFilter()">Filter</button>
						</div>
					</div>
					<hr/>

					<div class="row timetable-detail timetable-detail-1">
						<!-- <div class="col-sm-12 m-b-10">							
							<button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" id="btnBack1" onclick="backToTt()">Back</button>
							<?php 
								$attrDelete = ''; //($journal->reconcile_id!='' || $journal->journal_type != 'voucher') ? 'disabled':'';
								echo create_button($this->button, "btn_delete", $attrDelete); 
							?>
						</div> -->
						<div class="col-sm-12">
							<div class=" table-responsive">
								<table id="datatable_timetable1" class="table table-striped table-hover display nowrap">
									<thead>
									<tr>
										<th class="text-center" rowspan="2">No</th>
										<th rowspan="2">Business</th>
										<th rowspan="2">Customer</th>
										<th rowspan="2">Customer LP</th>
										<th rowspan="2">Route</th>
										<th rowspan="2">Cycle</th>
										<th rowspan="2">Truck</th>
										<th rowspan="2">Driver</th>
										<th colspan="4" class="text-center">Destination</th>
										<th rowspan="2"></th>
									</tr>
									<tr>
										<th class="text-center">Total</th>
										<th class="text-center">Done</th>
										<th class="text-center">Remain</th>
										<th class="text-center">Progress</th>
									</tr>
									</thead>									
								</table>
							</div>
						</div>
					</div>
					
					<div class="row timetable-detail timetable-detail-2">
						<input type="hidden" name="detail-timetable_id" id="detail-timetable_id" value="-1" />
						<input type="hidden" name="detail-route" id="detail-route" value="-1" />
						<input type="hidden" name="detail-cycle" id="detail-cycle" value="-1" />

						<div class="col-sm-12 m-b-10">							
							<button id="btn_backtt" type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" onclick="back()">Back</button>
							<button type="button" class="btn btn-purple btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" onclick="addNewLP()">Add New Logistic Point</button>
							<button id="btn_refresh" type="button" class="btn btn-primary btn-bordered waves-light waves-effect w-md m-b-5 btn-sm" onclick="doRefresh()">Refresh</button>
						</div>
						<div class="col-sm-12">
							<div class=" table-responsive">
								<table id="datatable_timetable" class="table table-striped table-hover display nowrap floating-tt">
									<thead>
									<tr>                        
										<th rowspan="2">Route</th>
										<th rowspan="2">Cycle</th>
										<th rowspan="2">Truck Number</th>										
										<th rowspan="2">Police Number</th>
										<th rowspan="2">Driver NIK</th>
										<th rowspan="2">Driver</th>
										<th rowspan="2">Logistic Point</th>
										<th colspan="5" class="text-center" style="border-right: 1px solid #dadada">Arrival</th>
										<th colspan="5" class="text-center">Departure</th>
										<th rowspan="2">Remark</th>
										<th rowspan="2">Arrival Problem</th>
										<th rowspan="2">Departure Problem</th>
										<th rowspan="2">ID</th>
										<th rowspan="2">arrival_next_day</th>
										<th rowspan="2">arrival_manual_by</th>
										<th rowspan="2">arrival_manual_dt</th>
										<th rowspan="2">departure_manual_by</th>
										<th rowspan="2">departure_manual_dt</th>
										<th rowspan="2">KM</th>
										<th rowspan="2">Fuel</th>
										<th rowspan="2">Spbu</th>
										<th rowspan="2">Tol (Rp)</th>
										<th rowspan="2">Others (Rp)</th>
										<th rowspan="2">Pallet (pcs)</th>
										<th rowspan="2">Rack (pcs)</th>
										<th rowspan="2">Division</th>
									</tr>
									<tr>
										<th class="text-center">Date</th>
										<th class="text-center">Plan</th>
										<th class="text-center">Actual</th>
										<th class="text-center">Gap</th>
										<th style="border-right: 1px solid #dadada">Eval</th>
										<th class="text-center">Date</th>
										<th class="text-center">Plan</th>
										<th class="text-center">Actual</th>
										<th class="text-center">Gap</th>
										<th class="text-center">Eval</th>
									</tr>
									</thead>									
								</table>
							</div>
						</div>
					</div>
						
					<div class="row timetable-detail timetable-detail-2">
						<div class="col-lg-4">
							<div class="card card-border">
								<div class="card-header bg-transparent">
									<h3 class="card-title text-muted mb-0">Total Hours</h3>
								</div>
								<div class="card-body">
									<table class="table">
										<tr>
											<td>Driving Hours</td>
											<td id="total_driver_hours">0</td>
										</tr>
										<tr>
											<td>Working Hours</td>
											<td id="total_working_hours">0</td>
										</tr>
										<tr>
											<td>Loading/Unloading Hours</td>
											<td id="total_loading_unloading">0</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-border card-primary">
								<div class="card-header border-primary bg-transparent">
									<h3 class="card-title text-primary mb-0">Arrival</h3>
								</div>
								<div class="card-body">
									<table class="table">
										<tr>
											<td>Ontime</td>
											<td id="arrival_ontime">0</td>
										</tr>
										<tr>
											<td>Advance</td>
											<td id="arrival_advance">0</td>
										</tr>
										<tr>
											<td>Delay</td>
											<td id="arrival_delay">0</td>
										</tr>
										<tr>
											<td>Result</td>
											<td id="arrival_result">0</td>
										</tr>
										<tr>
											<td>Destination Result To <span id="customer_lp_cd-text"></span></td>
											<td id="destination_result">0</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-border card-success">
								<div class="card-header border-success bg-transparent">
									<h3 class="card-title text-success mb-0">Departure</h3>
								</div>
								<div class="card-body">
									<table class="table">
										<tr>
											<td>Ontime</td>
											<td id="departure_ontime">0</td>
										</tr>
										<tr>
											<td>Advance</td>
											<td id="departure_advance">0</td>
										</tr>
										<tr>
											<td>Delay</td>
											<td id="departure_delay">0</td>
										</tr>
										<tr>
											<td>Result</td>
											<td id="departure_result">0</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</form>
		</div>
	</div>
</div>