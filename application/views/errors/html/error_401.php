<!DOCTYPE html>
<html class="account-pages-bg">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo APP_NAME ?>">
        <meta name="author" content="irfan.satriadarma@gmail.com">

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.png">
        <!-- App title -->
        <title><?php echo APP_NAME; ?></title>

        <!-- App css -->
        <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url()?>assets/js/modernizr.min.js"></script>
        

    </head>


    <body class="bg-transparent">

        <!-- HOME -->
        <section>
            <div class="container-alt">
                <div class="row">
                    <div class="col-sm-12 text-center">

                        <div class="wrapper-page">
                            <h3><i class="fa fa-hand-stop-o fa-5x text-danger"></i></h3>
                            <h2 class="text-uppercase text-danger"><?php echo $heading; ?></h2>
                            <p class="text-muted"><?php echo $message; ?></p>

                            <a class="btn btn-success waves-effect waves-light m-t-20" href="<?php echo base_url(); ?>"> Return Home</a>
                            <a class="btn btn-danger waves-effect waves-light m-t-20" href="<?php echo site_url('login/out'); ?>"> Logout</a>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- END HOME -->

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url()?>assets/js/detect.js"></script>
        <script src="<?php echo base_url()?>assets/js/fastclick.js"></script>
        <script src="<?php echo base_url()?>assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url()?>assets/js/waves.js"></script>
        <script src="<?php echo base_url()?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url()?>assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url()?>assets/js/jquery.core.js"></script>
        <script src="<?php echo base_url()?>assets/js/jquery.app.js"></script>

    </body>
    
</html>