<html>
    <head></head>
    <body style="background-color: #f2f4f8">
        <br/><br/><br/>
        <table align="center" width="750" border="0" cellspacing="0" cellpadding="20px" style="background-color: #ffffff; border:1px solid #d8d8d8;border-radius:3px;overflow:hidden">
            <tr>
                <td >

                    <table width="700" border="0" cellspacing="0" cellpadding="5px">
                        <tbody>
                            <tr>
                                <td width="600" style="font-family:helvetica neue,helvetica;font-size:14px;font-family:helvetica neue,helvetica;font-size:14px;padding:10px;background-color:#ffffff;border-collapse:collapse">
                                    <p>
                                    Hai <?php echo $name?>,<br/> <br/>
                                    Terima kasih sudah bergabung di <?php echo APP_NAME?>. Satu langkah lagi untuk mulai membuat Perusahaan Anda.                                    
                                    </p>

                                    <p>Silahkan konfirmasi email Anda dengan melakukan klik pada tautan di bawah ini.   </p>
                                    
                                    <a target="_blank" style="text-decoration:none;display:block" href="<?php echo base_url() . 'register/activation/' . $activation_code; ?>">
                                        <table height="40" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td height="38" style="background-color:#188AE2;padding:2px 15px 0px 15px;border-radius:3px">
                                                        <a target="_blank" style="font-family:helvetica;font-size:16px;color:#ffffff;text-decoration:none;display:block" href="<?php //echo $activation_link;  ?>">
                                                            Aktifkan Sekarang
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </a>
                                    <p>Jika link diatas tidak bisa diklik, salin link dibawah ini dan buka pada browser Anda.</p>
                                    <a target="_blank" onmouseover="this.style.color = 'CornflowerBlue'" onmouseout="this.style.color = '#333'" href="<?php echo base_url() . 'register/activation/' . $activation_code; ?>"><?php echo base_url() . 'register/activation/' . $activation_code; ?></a>
                                    <p>
                                        Cheers, <br/><br/>
                                        <?php echo APP_NAME?> Team
                                    </p>
                                </td>
                            </tr>
						</tbody>
                    </table>
                </td>
            </tr>
        </table>
        <br/><br/><br/>
    </body>
</html>