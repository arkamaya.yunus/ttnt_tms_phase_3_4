<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 06 Maret 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rollcall extends MY_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->model('Rollcall_model');
        $this->load->model('Timetable_model');
        $this->load->library('Zend');

        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index() 
    {
        date_default_timezone_set('Asia/Jakarta');
        $today_minus_one = date('d/m/Y', strtotime(date('Y-m-d') . " -1 days"));

        $data['stitle'] = 'Roll Call';
        $data['departure_dt_s'] = $today_minus_one;
        $data['departure_dt_e'] = date('d/m/Y');
        $data['jsapp'] = array('rollcall');

        $data['customer'] = $this->db->query("
            select system_code, system_value_txt from tb_m_system where system_type = 'customer' order by system_value_txt
        ")->result();

        $data['driver'] = $this->db->query("
            SELECT driver_cd, driver_name from tb_m_driver where active = '1' order by driver_name
        ")->result();

        $data['vehicle'] = $this->db->query("
            SELECT vehicle_cd from tb_m_vehicle where active = '1' order by vehicle_cd
        ")->result();

        $data['lp_cds'] = $this->db->query("
            SELECT lp_cd from tb_m_logistic_point where empty_plan_flg = '1' order by lp_cd
        ")->result();

        // 03072020
        // rollcall manual
        $data['driver_manual'] = $this->db->query(
            "select distinct driver_cd, driver_name from tb_r_timetable_detail where departure_dt = '" . date('Y-m-d') . "'
            order by driver_name
            "
        )->result();

        // route
        $data['route_manual'] = $this->db->query("
            SELECT DISTINCT a.route, a.cycle, a.driver_cd, a.lp_cd 
            FROM tb_r_timetable_detail a 
            INNER JOIN tb_m_logistic_point b ON b.lp_cd = a.lp_cd AND b.empty_plan_flg = '1'
            WHERE
            a.departure_dt = '" . date('Y-m-d') . "' 
            AND a.departure_plan IS NOT null
        ")->result();

        $this->load->view('header', $data);
        $this->load->view('rollcall');
        $this->load->view('footer');
    }

    function get_route()
	{
        $dt_s = $this->uri->segment(3);
        $dt_e = $this->uri->segment(4);
        $query = $_GET['query'];

        $sql = "select 
                    distinct route 
                from 
                    tb_r_timetable_detail 
                where 
                    (departure_dt between '" . $dt_s . "' and '" . $dt_e . "') 
                    and route like '%" . $query . "%' 
                order by route";        
		$results = $this->db->query($sql)->result();
		
		$suggestions = array();
		foreach ($results as $r)
		{
			$suggestions[] = array(
				'value' => $r->route,
				'data' => $r->route
			);
		}
		
        echo json_encode(array(
			'query' => 'Unit'
			, 'suggestions' => $suggestions
		));
	}

    function gets()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
		$sv		        = $this->input->post('search')['value'];
        $departure_dt_s = $this->input->post('departure_dt_s', true);
        $departure_dt_e = $this->input->post('departure_dt_e', true);
        $business       = $this->input->post('business', true);
        $customer       = $this->input->post('customer', true);
        $route          = $this->input->post('route', true);
        $driver_cd      = $this->input->post('driver_cd', true);
        $vehicle_cd     = $this->input->post('vehicle_cd', true);
        $status         = $this->input->post('status', true);
		
        $results 			= $this->Rollcall_model->getRollcall(
                                $departure_dt_s, $departure_dt_e, $business, $customer,
                                $route, $driver_cd, $vehicle_cd,
                                $start, $length, $sv, $order, $columns, $status
                            );
        $recordsTotal       = (int)$this->Rollcall_model->getCountOfRollcall(
                                $departure_dt_s, $departure_dt_e, $business, $customer,
                                $route, $driver_cd, $vehicle_cd, $sv, $status
                            );

        $data = array();
		foreach ($results as $r)
		{
            $row = array();
            
            $row[] = $r->rollcall_id;

            $voucher = $r->voucher;

            $row[] = '<a href="'.site_url('rollcall/id/' . $r->rollcall_id).'" title="Lihat Detail" style="font-weight: bold; ">' . $voucher . '</a>';
            
            $statustext['0'] = '<span class="badge badge-primary" style="width: 100%; text-align: left; padding: 5px">DEPARTURE CASHIER</span>';
            $statustext['1'] = '<span class="badge badge-info" style="width: 100%; text-align: left; padding: 5px">DEPARTURE CCR</span>';
            $statustext['2'] = '<span class="badge badge-danger" style="width: 100%; text-align: left; padding: 5px">ARRIVAL CASHIER</span>';
            $statustext['3'] = '<span class="badge badge-success" style="width: 100%; text-align: left; padding: 5px">ARRIVAL CCR</span>';
            $statustext['4'] = '<span class="badge badge-success" style="width: 100%; text-align: left; padding: 5px">FINISHED</span>';
            
            $row[] = '<a href="'.site_url('rollcall/id/' . $r->rollcall_id).'" title="Lihat Detail" style="font-weight: bold; ">' . $statustext[$r->status] . '</a>';
            $row[] = $r->driver_name;
            $row[] = $r->vehicle_cd;
            $row[] = $r->route;
            $row[] = $r->cycle;
            $row[] = ($r->departure_dt != '' && $r->departure_dt != '0000-00-00') ? date('d-M-Y', strtotime($r->departure_dt)) : '-';
            $row[] = substr($r->departure_plan, 0, 5);
            $row[] = substr($r->departure_actual, 0, 5);
            $row[] = substr($r->arrival_plan, 0, 5);
            $row[] = ($r->arrival_actual != '00:00:00') ? substr($r->arrival_actual, 0, 5) : '-';
            $row[] = $r->business;
            $row[] = $r->customer;
            $row[] = substr($r->clock_in, 0, 5);
            $row[] = ($r->clock_out != '00:00:00') ? substr($r->clock_out, 0, 5) : '-';
            $row[] = ($r->driver_nik == '1') ? 'OK' : 'NG';
            $row[] = ($r->driver_sim == '1') ? 'OK' : 'NG';

            $driver_sio = '-';
            if ($r->driver_sio == '1')
            {
                $driver_sio = 'OK';
            }elseif($r->driver_sio == '0')
            {
                $driver_sio = 'NG';
            }
            $row[] = $driver_sio;
            $row[] = $r->driver_blood_pres_sistole . '/' . $r->driver_blood_pres_diastole;
            $row[] = $r->driver_body_temp;
            $row[] = ($r->driver_sleep_time == '1') ? 'OK' : 'NG';

            $row[] = number_format($r->cashier_fuel_plan, 0, ',', '.');
            $row[] = number_format($r->cashier_fuel_actual, 0, ',', '.');
            $row[] = $r->cashier_fuel_remark;
            $row[] = number_format($r->cashier_etoll_plan, 0, ',', '.');
            $row[] = $r->cashier_etoll_cardno;
            $row[] = number_format($r->cashier_etoll_amount_start, 0, ',', '.');
            $row[] = number_format($r->cashier_etoll_amount_end, 0, ',', '.');
            $row[] = number_format($r->cashier_etoll_balance, 0, ',', '.');
            $row[] = $r->cashier_etoll_remark;
            $row[] = number_format($r->cashier_others_plan, 0, ',', '.');
            $row[] = number_format($r->cashier_others_actual, 0, ',', '.');
            $row[] = $r->cashier_others_remark;

            $controller_label['1'] = 'OK';
            $controller_label['0'] = 'NG';
            $controller_label[''] = '-';

            $row[] = $controller_label[$r->controller_apd_departure]; // ($r->controller_apd_departure == '1') ? 'OK' : 'NG';
            $row[] = $controller_label[$r->controller_basket_departure]; //($r->controller_basket_departure == '1') ? 'OK' : 'NG';
            $row[] = $controller_label[$r->controller_key_departure]; //($r->controller_key_departure == '1') ? 'OK' : 'NG';
            $row[] = $controller_label[$r->controller_apd_arrival]; //($r->controller_apd_arrival == '1') ? 'OK' : 'NG';
            $row[] = $controller_label[$r->controller_basket_arrival]; //($r->controller_basket_arrival == '1') ? 'OK' : 'NG';
            $row[] = $controller_label[$r->controller_key_arrival]; //($r->controller_key_arrival == '1') ? 'OK' : 'NG';

			$data[] = $row;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }

    function create($lp_cd='')
    {
        // lp_cd - Mandatory
        if ($lp_cd == '')        
        {
            $this->session->set_flashdata('notif_danger', '<strong>Error.</strong> Please Choose Starting Logistic Point.');
            redirect('rollcall');
        }
        else
        {
            // cek lpnya ada nggak di DB
            $cek = $this->db->query("select lp_cd from tb_m_logistic_point where lp_cd = '" . $lp_cd . "' and empty_plan_flg = '1'")->row()->lp_cd;
            if ($cek == '')
            {
                $this->session->set_flashdata('notif_danger', '<strong>Error.</strong> Please Choose Starting Logistic Point.');
                redirect('rollcall');
            }
        }

        $data['stitle'] = 'Roll Call';
        $data['jsapp'] = array('rollcall_create');
        $data['lp_cd'] = $lp_cd;

        $this->load->view('header_rollcall', $data);
        $this->load->view('rollcall_create');
        $this->load->view('footer_rollcall');
    }

    function syncatt()
    {

        date_default_timezone_set('Asia/Jakarta');
        
        $success = false;
        $message = 'No Attendance data. Last Sync: ' . date('Y-m-d H:i:s');
        
        $dt = date('Y-m-d');
        $tt = '';
        $arrival_plan = '';
        $cashier = '';

        // step 1. cari attendance clock_in yang hari Ini dan belum Rollcall
        $sql = "
            SELECT
                a.attendance_id,
                a.driver_cd,
                a.attendance_dt,
                TIME_FORMAT(a.clock_in,'%H:%i') as departure_actual,
                TIME_FORMAT(a.clock_out,'%H:%i') as arrival_actual,
                a.rollcall_id,
                a.clock_in,
                b.lp_cd
            FROM 
                tb_r_attendance a
                INNER JOIN tb_r_timetable_detail b ON b.driver_cd = a.driver_cd 
                INNER JOIN tb_m_logistic_point c ON c.lp_cd = b.lp_cd AND c.empty_plan_flg = '1'
            WHERE 
                a.attendance_dt = '".$dt."' 
                    AND a.rollcall_id IS NULL
                    AND b.departure_dt = '".$dt."' 
                    AND b.departure_plan IS NOT NULL
                    AND b.lp_cd = '".$_POST['lp_cd']."'
            ORDER BY clock_in 
            LIMIT 1
        ";
        $att = $this->db->query($sql)->result();

        if (count($att) > 0)
        {
            // step 2. Cek ke table tb_r_timetable_detail

            // update 20-Juli-2020 update kondisi where arrival_actual is null (untuk memastikan driver yg jalan 2x rute)

            $att = $att[0];

            $sql = "
            SELECT                 
                a.timetable_detail_id,
                a.timetable_id,
                DATE_FORMAT(a.timetable_dt,'%d/%m/%Y') as timetable_dt,
                b.business,
                b.customer,
                a.route,
                a.cycle,
                DATE_FORMAT(a.departure_dt,'%d/%m/%Y') as departure_dt,
                DATE_FORMAT(DATE_ADD(DATE_FORMAT(CONCAT(a.departure_dt,' ', a.departure_plan),'%Y-%m-%d %H:%i:%s'), INTERVAL -20 MINUTE), '%H:%i') AS departure_plan,
                a.driver_cd,
                a.driver_name,
                d.smartphone,
                DATE_FORMAT(d.driving_license_val,'%d/%m/%Y') as driving_license_val,
                DATE_FORMAT(d.forklift_license_val,'%d/%m/%Y') as forklift_license_val,
                a.vehicle_cd,
                DATE_FORMAT(e.license_stnk,'%d/%m/%Y') as license_stnk,
                DATE_FORMAT(e.license_keur,'%d/%m/%Y') as license_keur,
                DATE_FORMAT(e.license_sipa,'%d/%m/%Y') as license_sipa,
                DATE_FORMAT(DATE_ADD(DATE_FORMAT(CONCAT(a.departure_dt,' ', a.departure_plan),'%Y-%m-%d %H:%i:%s'), INTERVAL -20 MINUTE), '%H:%i') AS arrival_plan_driver
            FROM 
                tb_r_timetable_detail a 
                INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                INNER JOIN tb_m_logistic_point c ON c.lp_cd = a.lp_cd AND c.empty_plan_flg = '1'
                INNER JOIN tb_m_driver d ON d.driver_cd = a.driver_cd
                INNER JOIN tb_m_vehicle e ON e.vehicle_cd = a.vehicle_cd
            WHERE 
                a.departure_dt = '" . $dt . "' 
                AND a.departure_plan IS NOT null
                AND a.driver_cd = '" . $att->driver_cd . "'
                AND a.lp_cd = '" . $_POST['lp_cd'] . "'
                AND a.arrival_plan IS NULL
                AND CONCAT(a.timetable_dt,a.route,a.cycle) NOT IN 
                (
                	SELECT CONCAT(timetable_dt, route,CYCLE) FROM tb_r_rollcall WHERE departure_dt = '" . $dt . "' AND driver_cd = '".$att->driver_cd."'
                )   
            ORDER BY a.driver_name, a.departure_plan
            LIMIT 1
            ;
            ";  // echo $sql;
            $timetable = $this->db->query($sql)->result();                        

            if (count($timetable) > 0)
            {
                // cek apakah dia salah FINGER, udah pernah Rollcall Tapi finger lagi di mesin IN
                // cek rollcall nya berdasarkan driver_cd, tgl, dan route-cycle di tb_r_rollcall

                // TODO
                $sql = "
                    select count(a.rollcall_id) AS cnt from tb_r_rollcall a
                    WHERE 
                        a.timetable_dt = '" . $timetable[0]->timetable_dt . "'
                        AND a.driver_cd = '" . $att->driver_cd . "'
                        AND a.route = '" . $timetable[0]->route . "'
                        AND a.cycle = '" . $timetable[0]->cycle . "'
                        AND a.arrival_actual is null
                    ";
                $cek_jml_rc = $this->db->query($sql)->row()->cnt;
                if ($cek_jml_rc == 0)
                {
                    // Hanya boleh rollcall jika 30 menit sebelum Departure Plan.
                    date_default_timezone_set('Asia/Jakarta');

                    $now = date('Y-m-d H:i:s');
                    $depart = date('Y-m-d', strtotime( str_replace('/', '-', $timetable[0]->departure_dt))) . ' ' . $timetable[0]->departure_plan . ':00';

                    // cari selisihnya brp lama
                    $now = time(); // or your date as well
                    $your_date = strtotime($depart);
                    $datediff = $your_date - $now;

                    $selisihMenit = round($datediff / 60);

                    // echo $selisihMenit; die();

                    if ($selisihMenit < 15)
                    {
                        // ada datanya.
                        // step 3. Cari Plan Arrival nya
                        $tt = $timetable[0];

                        $sql = "
                        SELECT 
                            -- TIME_FORMAT(a.arrival_plan,'%H:%i') as arrival_plan
                            TIME_FORMAT(DATE_ADD(a.arrival_plan, INTERVAL 20 MINUTE), '%H:%i') as arrival_plan
                        FROM 
                            tb_r_timetable_detail a 
                            INNER JOIN tb_m_logistic_point b ON b.lp_cd = a.lp_cd AND b.empty_plan_flg = '1' 
                        WHERE 
                            a.timetable_id = '" . $tt->timetable_id . "' AND 
                            a.arrival_plan IS NOT NULL AND 
                            a.departure_plan IS NULL 
                            AND a.route = '" . $tt->route . "'
                            AND a.cycle = '" . $tt->cycle . "'
                            and a.driver_cd = '" . $tt->driver_cd . "'
                        LIMIT 1
                        ";

                        $arrival_plan = '';

                        $ap = $this->db->query($sql)->result();
                        if (count($ap) > 0)
                        {
                            $arrival_plan = $ap[0]->arrival_plan;
                        }

                        // step 4. Cari Cashier Summary
                        $sql = "
                        SELECT 
                            SUM(a.km) AS km, 
                            SUM(a.fuel) AS fuel, 
                            GROUP_CONCAT(distinct a.spbu) AS spbu, 
                            SUM(a.tol) AS tol , 
                            SUM(a.others) AS others
                        FROM 
                            tb_r_timetable_detail a 
                        WHERE 
                            a.timetable_id = '" . $tt->timetable_id . "' AND 
                            a.route = '" . $tt->route . "' AND 
                            a.driver_cd = '" . $tt->driver_cd . "' AND 
                            a.vehicle_cd = '" . $tt->vehicle_cd . "'
                        GROUP BY
                            a.timetable_id, a.route, a.driver_cd, a.vehicle_cd
                        ";
                        $cashier = $this->db->query($sql)->row();

                        // update arrival_actual di timetable
                        $data_arrival['arrival_actual'] = $att->clock_in;
                        $agap = $this->get_time_difference($tt->arrival_plan_driver, date('H:i:s', strtotime($att->clock_in)));
                        $data_arrival['arrival_gap'] = $agap;
                        $data_arrival['arrival_eval'] = $this->get_time_eval($tt->arrival_plan_driver, date('H:i:s', strtotime($att->clock_in)));

                        $this->db->where('timetable_detail_id', $tt->timetable_detail_id);
                        $this->db->update('tb_r_timetable_detail', $data_arrival);

                        $success = true;
                        $message = 'Attendance Ditemukan';// . $tt->arrival_plan_driver . ' == ' . date('H:i:s', strtotime($att->clock_in));
                    }
                    else
                    {
                        // driver absen belum < 45 menit.
                        $data['rollcall_id'] = '-1';
                        $this->db->where('attendance_id', $att->attendance_id);
                        $this->db->update('tb_r_attendance', $data);
                        $message = 'Attendance dan Timetable ditemukan [' . $timetable[0]->driver_name . ' - ' . $timetable[0]->route . '], namun belum < 15 Menit waktu Rollcall';
                    }
                }
                else
                {
                    // minuskan. salah absen pulang
                    $data['rollcall_id'] = '-1';
                    $this->db->where('attendance_id', $att->attendance_id);
                    $this->db->update('tb_r_attendance', $data);
                    $message = 'Attendance dan Timetable ditemukan [' . $timetable[0]->driver_name . ' - ' . $timetable[0]->route . '], namun Salah Finger OUT di Mesin IN ';
                }
            }
            else
            {
                // Ada driver tapi tidak ada timetable.. update rollcall_id jadi -1, (Skip and find next driver finger)

                // update rollcall_id = -1 hanya jika lp code nya sesuai.

                $sql = "
                    SELECT                 
                        a.timetable_detail_id                     
                    FROM 
                        tb_r_timetable_detail a 
                        INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                        INNER JOIN tb_m_logistic_point c ON c.lp_cd = a.lp_cd AND c.empty_plan_flg = '1'
                        INNER JOIN tb_m_driver d ON d.driver_cd = a.driver_cd
                        INNER JOIN tb_m_vehicle e ON e.vehicle_cd = a.vehicle_cd
                    WHERE 
                        a.timetable_dt = '" . $dt . "' 
                        AND a.departure_plan IS NOT null
                        AND a.driver_cd = '" . $att->driver_cd . "' 
                        AND a.arrival_actual IS null
                    ORDER BY a.driver_name
                    LIMIT 1
                    ;
                    "; // echo $sql;
                $timetable = $this->db->query($sql)->result();
                if (count($timetable) == 0)
                {
                    // hanya delete jika benar2 tidak ada
                    $data['rollcall_id'] = '-1';
                    $this->db->where('attendance_id', $att->attendance_id);
                    $this->db->update('tb_r_attendance', $data);

                }

                $message = 'Attendance Ditemukan : ' . $att->driver_cd . ', namun Tidak ada timetable untuk ' . $_POST['lp_cd'];
            }
        }

        $result['success']      = $success;
        $result['message']      = $message;
        $result['data_att']     = $att;
        $result['data_tt']      = $tt;
        $result['data_cashier'] = $cashier;
        $result['arrival_plan'] = $arrival_plan;

        echo json_encode($result);
    }

    function saveRc()
    {
        date_default_timezone_set('Asia/Jakarta');

        // -- 02-Juli-2020
        // validasi tidak boleh ada Rollcall yg sama di Hari, Driver, Rute yg sama
        $sql = "
        select count(a.rollcall_id) AS cnt from tb_r_rollcall a
        WHERE 
            a.departure_dt = '" . date('Y-m-d', strtotime( str_replace('/', '-', $_POST['tt']['departure_dt']))) . "'
            AND a.driver_cd = '" . $_POST['tt']['driver_cd'] . "'
            AND a.route = '" . $_POST['tt']['route'] . "'
            AND a.cycle = '" . $_POST['tt']['cycle'] . "'
        ";
        $cek_jml_rc = $this->db->query($sql)->row()->cnt;
        if ($cek_jml_rc == 0)
        {
            // untuk update rollcall_id
            $attendance_id = $_POST['att']['attendance_id'];

            // generate nomor voucher
            $arrayBulan['01'] = 'A';
            $arrayBulan['02'] = 'B';
            $arrayBulan['03'] = 'C';
            $arrayBulan['04'] = 'D';
            $arrayBulan['05'] = 'E';
            $arrayBulan['06'] = 'F';
            $arrayBulan['07'] = 'G';
            $arrayBulan['08'] = 'H';
            $arrayBulan['09'] = 'I';
            $arrayBulan['10'] = 'J';
            $arrayBulan['11'] = 'K';
            $arrayBulan['12'] = 'L';
            $voucher = $arrayBulan[date('m', strtotime( str_replace('/', '-', $_POST['tt']['timetable_dt'])))] . date('d', strtotime( str_replace('/', '-', $_POST['tt']['timetable_dt'])));

            // find jum of transaction rollcall today
            $q = "select count(rollcall_id) as cnt from tb_r_rollcall where timetable_dt = '" . date('Y-m-d', strtotime( str_replace('/', '-', $_POST['tt']['timetable_dt']))) . "'";
            $cnt = $this->db->query($q)->row()->cnt;
            $cnt = (int)$cnt + 1;
            $voucher .= str_pad($cnt,3,"0",STR_PAD_LEFT);;        

            $voucher = 'DR' . date('Y', strtotime( str_replace('/', '-', $_POST['tt']['timetable_dt']))) . $voucher;

            $data['timetable_dt']       = date('Y-m-d', strtotime( str_replace('/', '-', $_POST['tt']['timetable_dt'])));
            $data['departure_dt']       = date('Y-m-d', strtotime( str_replace('/', '-', $_POST['tt']['departure_dt'])));
            $data['departure_plan']     = $_POST['tt']['departure_plan'];
            $data['departure_actual']   = $_POST['att']['departure_actual'];
            $data['arrival_plan']       = $_POST['arrival_plan'];
            $data['arrival_actual']     = $_POST['att']['arrival_actual'];
            $data['business']           = $_POST['tt']['business'];
            $data['customer']           = $_POST['tt']['customer'];
            $data['route']              = $_POST['tt']['route'];
            $data['cycle']              = $_POST['tt']['cycle'];
            $data['driver_cd']          = $_POST['tt']['driver_cd'];
            $data['vehicle_cd']         = $_POST['tt']['vehicle_cd'];
            $data['clock_in']           = $_POST['att']['departure_actual'];
            $data['clock_out']          = $_POST['att']['arrival_actual'];
            $data['driver_nik']         = $_POST['driver_cd_check'];
            $data['driver_sim']         = $_POST['driving_license_val_check'];
            $data['driver_sio']         = $_POST['forklift_license_val_check'];
            
            $data['driver_blood_pres_sistole']      = $_POST['driver_blood_pres_sistole'];
            $data['driver_blood_pres_diastole']     = $_POST['driver_blood_pres_diastole'];
            $data['driver_body_temp']               = $_POST['driver_body_temp'];
            $data['driver_sleep_time']              = $_POST['driver_sleep_time'];

            $data['cashier_fuel_plan']      = $_POST['cashier_fuel_plan'];
            $data['cashier_etoll_plan']     = $_POST['cashier_etoll_plan'];
            $data['cashier_others_plan']    = $_POST['cashier_others_plan'];
            $data['cashier_etoll_spbu']    = $_POST['cashier_etoll_spbu'];

            $data['lp_cd']  = $_POST['lp_cd'];

            // created by created dt
            $data['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
            $data['created_by'] = date('Y-m-d H:i:s');

            $this->db->insert('tb_r_rollcall', $data);
            $rollcall_id = $this->db->insert_id();

            // update attendance
            $data_att['rollcall_id'] = $rollcall_id;
            $this->db->where('attendance_id', $attendance_id);
            $this->db->update('tb_r_attendance', $data_att);

            // update voucher
            // $voucher .= $rollcall_id;
            $data_voucher['voucher'] = $voucher;
            $this->db->where('rollcall_id', $rollcall_id);
            $this->db->update('tb_r_rollcall', $data_voucher);

            $success = true;
            $message = 'Roll Call Saved';
        }
        else
        {
            /*
             a.departure_dt = '" . date('Y-m-d', strtotime( str_replace('/', '-', $_POST['tt']['departure_dt']))) . "'
            AND a.driver_cd = '" . $_POST['tt']['driver_cd'] . "'
            AND a.route = '" . $_POST['tt']['route'] . "'
            AND a.cycle = '" . $_POST['tt']['cycle'] . "'
            */
            $success = false;
            $message = 'Sudah ada Rollcall di Tanggal ' . date('Y-m-d', strtotime( str_replace('/', '-', $_POST['tt']['departure_dt'])));
            $message .= '. Driver : ' . $_POST['tt']['driver_cd'];
            $message .= '. Route : ' . $_POST['tt']['route'];
            $message .= '. Cycle : ' . $_POST['tt']['cycle'];
        }


        

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }
    
    function id($rollcall_id = '')
    {
        // rollcall_id - Mandatory
        if ($rollcall_id == '')        
        {
            $this->session->set_flashdata('notif_danger', '<strong>Error.</strong> Roll Call Tidak ditemukan.');
            redirect('rollcall');
        }
        else
        {
            // cek rollcall nya ada nggak di DB
            $cek = $this->db->query("select rollcall_id from tb_r_rollcall where rollcall_id = '" . $rollcall_id . "'")->row()->rollcall_id;
            if ($cek == '')
            {
                $this->session->set_flashdata('notif_danger', '<strong>Error.</strong> Roll Call Tidak ditemukan');
                redirect('rollcall');
            }
        }

        $data['stitle'] = 'Roll Call View';
        $data['jsapp'] = array('rollcall_id');
        $data['rollcall'] = $this->db->query("
            select 
                a.*,
                b.driver_name,
                b.smartphone,
                b.driving_license_val,
                b.forklift_license_val,
                c.license_stnk,
                c.license_keur,
                c.license_sipa
            from 
                tb_r_rollcall a 
                inner join tb_m_driver b on b.driver_cd = a.driver_cd
                inner join tb_m_vehicle c on c.vehicle_cd = a.vehicle_cd
            where a.rollcall_id = '" . $rollcall_id . "'")->row();

        $this->load->view('header_rollcall', $data);
        $this->load->view('rollcall_id');
        $this->load->view('footer_rollcall');

    }

    function saveRc2()
    {
        $rollcall_id = $_POST['rollcall_id'];
        // $data['cashier_fuel_actual'] = $_POST['cashier_fuel_actual'];
        // $data['cashier_fuel_remark'] = $_POST['cashier_fuel_remark'];
        $data['cashier_etoll_cardno'] = $_POST['cashier_etoll_cardno'];
        $data['cashier_etoll_amount_start'] = $_POST['cashier_etoll_amount_start'];
        // $data['cashier_etoll_balance'] = $_POST['cashier_etoll_balance'];
        // $data['cashier_etoll_amount_end'] = $_POST['cashier_etoll_amount_end'];
        // $data['cashier_etoll_remark'] = $_POST['cashier_etoll_remark'];
        // $data['cashier_others_actual'] = $_POST['cashier_others_actual'];
        // $data['cashier_others_remark'] = $_POST['cashier_others_remark'];
        $data['status'] = '1';

        $this->db->where('rollcall_id', $rollcall_id);
        $this->db->update('tb_r_rollcall', $data);

        $success = true;
        $message = 'Roll Call Updated';

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }    

    function saveRc22()
    {
        $rollcall_id = $_POST['rollcall_id'];
        $data['cashier_fuel_actual'] = $_POST['cashier_fuel_actual'];
        $data['cashier_fuel_remark'] = $_POST['cashier_fuel_remark'];
        // $data['cashier_etoll_cardno'] = $_POST['cashier_etoll_cardno'];
        // $data['cashier_etoll_amount_start'] = $_POST['cashier_etoll_amount_start'];
        $data['cashier_etoll_balance'] = $_POST['cashier_etoll_balance'];
        $data['cashier_etoll_amount_end'] = $_POST['cashier_etoll_amount_end'];
        $data['cashier_etoll_remark'] = $_POST['cashier_etoll_remark'];
        $data['cashier_others_actual'] = $_POST['cashier_others_actual'];
        $data['cashier_others_remark'] = $_POST['cashier_others_remark'];
        $data['status'] = '3';

        $this->db->where('rollcall_id', $rollcall_id);
        $this->db->update('tb_r_rollcall', $data);

        $success = true;
        $message = 'Roll Call Updated';

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }

    function saveRc3()
    {
        $rollcall_id = $_POST['rollcall_id'];
        $data['controller_apd_departure'] = $_POST['controller_apd_departure'];
        // $data['controller_apd_arrival'] = $_POST['controller_apd_arrival'];
        $data['controller_basket_departure'] = $_POST['controller_basket_departure'];
        // $data['controller_basket_arrival'] = $_POST['controller_basket_arrival'];
        $data['controller_key_departure'] = $_POST['controller_key_departure'];
        // $data['controller_key_arrival'] = $_POST['controller_key_arrival'];
        $data['status'] = '2';

        $this->db->where('rollcall_id', $rollcall_id);
        $this->db->update('tb_r_rollcall', $data);

        $success = true;
        $message = 'Roll Call Updated';

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }

    function saveRc31()
    {
        $rollcall_id = $_POST['rollcall_id'];
        // $data['controller_apd_departure'] = $_POST['controller_apd_departure'];
        $data['controller_apd_arrival'] = $_POST['controller_apd_arrival'];
        // $data['controller_basket_departure'] = $_POST['controller_basket_departure'];
        $data['controller_basket_arrival'] = $_POST['controller_basket_arrival'];
        // $data['controller_key_departure'] = $_POST['controller_key_departure'];
        $data['controller_key_arrival'] = $_POST['controller_key_arrival'];
        $data['status'] = '4';

        $this->db->where('rollcall_id', $rollcall_id);
        $this->db->update('tb_r_rollcall', $data);

        $success = true;
        $message = 'Roll Call Updated';

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }

    function print($rollcall_id)
    {
        // rollcall_id - Mandatory
        if ($rollcall_id == '')        
        {
            $this->session->set_flashdata('notif_danger', '<strong>Error.</strong> Roll Call Tidak ditemukan.');
            redirect('rollcall');
        }
        else
        {
            // cek rollcall nya ada nggak di DB
            $cek = $this->db->query("select rollcall_id from tb_r_rollcall where rollcall_id = '" . $rollcall_id . "'")->row()->rollcall_id;
            if ($cek == '')
            {
                $this->session->set_flashdata('notif_danger', '<strong>Error.</strong> Roll Call Tidak ditemukan');
                redirect('rollcall');
            }
        }

        $rollcall = $this->db->query("
            select 
                a.*,
                b.driver_name,
                b.smartphone,
                b.driving_license_val,
                b.forklift_license_val,
                c.license_stnk,
                c.license_keur,
                c.license_sipa,
                c.vehicle_number                
            from 
                tb_r_rollcall a 
                inner join tb_m_driver b on b.driver_cd = a.driver_cd
                inner join tb_m_vehicle c on c.vehicle_cd = a.vehicle_cd
            where a.rollcall_id = '" . $rollcall_id . "'")->row();

        $data['rollcall'] = $rollcall;

        // bugs fix.
        // cari dulu timetable_id nya jika lintas hari.
        $timetable_id = $this->db->query("
            select 
                a.timetable_id 
            from 
                tb_r_timetable_detail a 
            where 
                a.departure_dt = '" . $rollcall->departure_dt."'
                AND a.driver_cd = '". $rollcall->driver_cd ."'
                AND a.vehicle_cd = '" . $rollcall->vehicle_cd . "'
                AND a.route = '" . $rollcall->route . "'
                AND a.cycle = '" . $rollcall->cycle . "'
            order by a.arrival_dt, a.arrival_plan
            LIMIT 1
            "         
        )->row()->timetable_id;


        $data['timetable'] = $this->db->query("
            SELECT 
                a.lp_cd,
                a.arrival_plan,
                a.departure_plan            
            FROM 
                tb_r_timetable_detail a
            WHERE
                a.timetable_id = '" . $timetable_id ."'
                AND a.driver_cd = '". $rollcall->driver_cd ."'
                AND a.vehicle_cd = '" . $rollcall->vehicle_cd . "'
                and a.route = '" . $rollcall->route . "'
                and a.cycle = '" . $rollcall->cycle . "'
            ORDER BY
                a.arrival_dt, a.arrival_plan 
        ")->result();
        
        // update jumlah print nya;
        $print_driver_report = $rollcall->print_driver_report;
        $print_driver_report++;
        $data_update['print_driver_report'] = $print_driver_report;
        $this->db->where('rollcall_id', $rollcall_id);
        $this->db->update('tb_r_rollcall', $data_update);

        $data['print_driver_report'] = $print_driver_report;

        $this->load->view('report_driver_print2', $data);
    }

    function barcode($voucher = '123')
    {
        $this->zend->load('Zend/Barcode');
        Zend_Barcode::render('code128', 'image', array('text' => $voucher));
    }

    function get_time_difference($time1, $time2) {
        $time1 = strtotime("1980-01-01 $time1");
        $time2 = strtotime("1980-01-01 $time2");
        
        if ($time2 < $time1) {
            $time2 += 86400;
        }
        
        return date("H:i", strtotime("1980-01-01 00:00:00") + ($time2 - $time1));
    }

    function get_time_eval($time1, $time2) {
        $time1 = strtotime("1980-01-01 $time1");
        $time2 = strtotime("1980-01-01 $time2");
        
        $eval = 'Ontime';
        if ($time2 < $time1) {
            $eval = 'Advance';
        }else if ($time2 > $time1)
        {
            $eval = 'Delay';
        }
        
        return $eval;;
    }

    function dateDifference($date_1 , $date_2 , $differenceFormat = '%i' )
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);
    
        $interval = date_diff($datetime1, $datetime2);
    
        return $interval->format($differenceFormat);
    
    }

    function download($ds, $de, $status)
    {
        ini_set('memory_limit', '-1');
        
        date_default_timezone_set('Asia/Jakarta');
        $this->load->library('Excel');
		
		$tpl_path = './assets/files/template-rollcall.xlsx';
		
		//$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($tpl_path);
		$sheet = $objPHPExcel->getActiveSheet();
		$sheet->setTitle('Driver');
		
		// create data..
        $row = 7;
        
        $statustext['0'] = 'DEPARTURE CASHIER';
        $statustext['1'] = 'DEPARTURE CCR';
        $statustext['2'] = 'ARRIVAL CASHIER';
        $statustext['3'] = 'ARRIVAL CCR';
        $statustext['4'] = 'FINISHED';

        $controller_label['1'] = 'OK';
        $controller_label['0'] = 'NG';
        $controller_label[''] = '-';

        $driver_sio['1'] = 'OK';
        $driver_sio['0'] = 'NG';
        $driver_sio[''] = '-';

        $sheet->setCellValue('A3', $ds . ' - ' . $de);
        		
		$data = $this->Rollcall_model->getRollcallForDownload($ds, $de, $status);		
        $i = 1;
        foreach($data as $d) {
			$sheet->setCellValue('A'.$row, $d->rollcall_id);
			$sheet->setCellValue('B'.$row, $d->voucher);
			$sheet->setCellValue('C'.$row, $statustext[$d->status]);
            $sheet->setCellValue('D'.$row, $d->driver_cd);
            $sheet->setCellValue('E'.$row, $d->driver_name);
            $sheet->setCellValue('F'.$row, $d->vehicle_cd);
			$sheet->setCellValue('G'.$row, $d->vehicle_number);
			$sheet->setCellValue('H'.$row, $d->route);
			$sheet->setCellValue('I'.$row, $d->cycle);
			$sheet->setCellValue('J'.$row, $d->departure_dt);
			$sheet->setCellValue('K'.$row, substr($d->departure_plan,0,5));
			$sheet->setCellValue('L'.$row, substr($d->departure_actual,0,5));
            $sheet->setCellValue('M'.$row, substr($d->arrival_plan,0,5));
            $sheet->setCellValue('N'.$row, substr($d->arrival_actual,0,5));
            $sheet->setCellValue('O'.$row, $d->business);
            $sheet->setCellValue('P'.$row, $d->customer);
            // $sheet->setCellValue('O'.$row, substr($d->clock_in,0,5));
            // $sheet->setCellValue('P'.$row, substr($d->clock_out,0,5));
            $sheet->setCellValue('Q'.$row, 'OK');
            $sheet->setCellValue('R'.$row, 'OK');            
            $sheet->setCellValue('S'.$row, $driver_sio[$d->driver_sio]);
            $sheet->setCellValue('T'.$row, $d->driver_blood_pres_sistole . '/' . $d->driver_blood_pres_diastole);
            $sheet->setCellValue('U'.$row, $d->driver_body_temp);
            $sheet->setCellValue('V'.$row, ($d->driver_sleep_time == '1') ? 'OK' : 'NG');
            $sheet->setCellValue('W'.$row, $d->cashier_fuel_plan);
            $sheet->setCellValue('X'.$row, $d->cashier_fuel_actual);
            $sheet->setCellValue('Y'.$row, $d->cashier_fuel_remark);
            $sheet->setCellValue('Z'.$row, $d->cashier_etoll_plan);
            $sheet->setCellValueExplicit('AA'.$row, $d->cashier_etoll_cardno, PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValue('AB'.$row, $d->cashier_etoll_amount_start);
            $sheet->setCellValue('AC'.$row, $d->cashier_etoll_amount_end);
            $sheet->setCellValue('AD'.$row, $d->cashier_etoll_balance);
            $sheet->setCellValue('AE'.$row, $d->cashier_etoll_remark);
            $sheet->setCellValue('AF'.$row, $d->cashier_others_plan);
            $sheet->setCellValue('AG'.$row, $d->cashier_others_actual);
            $sheet->setCellValue('AH'.$row, $d->cashier_etoll_remark);
            $sheet->setCellValue('AI'.$row, $controller_label[$d->controller_apd_departure]);
            $sheet->setCellValue('AJ'.$row, $controller_label[$d->controller_basket_departure]);
            $sheet->setCellValue('AK'.$row, $controller_label[$d->controller_key_departure]);
            $sheet->setCellValue('AL'.$row, $controller_label[$d->controller_apd_arrival]);
            $sheet->setCellValue('AM'.$row, $controller_label[$d->controller_basket_arrival]);
            $sheet->setCellValue('AN'.$row, $controller_label[$d->controller_key_arrival]);            

            $row++;
            $i++;
        }        
        		
		$file_name = "Rollcall_".date('YmdHis').".xlsx";
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment;filename=$file_name");
		header("Cache-Control: max-age=0");
		$objWriter= PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

		unset($sheet, $objPHPExcel);
    }

    function cekrcmanual()
    {
        date_default_timezone_set('Asia/Jakarta');
        $departure_dt = date('Y-m-d');
        $driver_cd = $_POST['driver_cd'];
        $route = $_POST['route'];
        $cycle = $_POST['cycle'];

        // cari rollcallnya
        $sql = "
            select count(a.rollcall_id) AS cnt from tb_r_rollcall a
            WHERE 
                a.departure_dt = '" . $departure_dt . "'
                AND a.driver_cd = '" . $driver_cd . "'
                AND a.route = '" . $route . "'
                AND a.cycle = '" . $cycle . "'
            ";
        $cek_jml_rc = $this->db->query($sql)->row()->cnt;

        $success = false;
        $message = 'Sudah ada rollcall untuk Driver, Route, Cycle ini.';
        if ($cek_jml_rc == 0)
        {
            $success = true;
            $message = 'Belum ada rollcall. Rollcall akan dimulai';
        }

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }

    function create_manual($driver_cd, $route, $cycle, $lp_cd)
    {
        // lp_cd - Mandatory
        if ($lp_cd == '' || $driver_cd == '' || $route == '' || $cycle == '')
        {
            $this->session->set_flashdata('notif_danger', '<strong>Error.</strong> Rollcall Manual Tidak bisa dilakukan.');
            redirect('rollcall');
        }
        else
        {
            // ambil data untuk di rollcall manual
        }

        $data['stitle'] = 'RollCall Manual';
        $data['jsapp'] = array('rollcall_create_manual');
        $data['lp_cd'] = $lp_cd;

        $this->load->view('header_rollcall', $data);
        $this->load->view('rollcall_create_manual');
        $this->load->view('footer_rollcall');
    }

    function syncmanual()
    {
        // get data manual post
        $m_driver_cd = $_POST['driver_cd'];
        $m_route = $_POST['route'];
        $m_cycle = $_POST['cycle'];
        $m_lp_cd = $_POST['lp_cd'];

        date_default_timezone_set('Asia/Jakarta');
        
        $success = false;
        $message = 'No Attendance data. Last Sync: ' . date('Y-m-d H:i:s');
        
        $dt = date('Y-m-d');
        $tt = '';
        $arrival_plan = '';
        $cashier = '';

        // manual-step 1. cari attendance clock_in hari ini dan sudah rollcall ? 
        // jika sudah pernah finger == asumsi ini route selanjutnya.
        // maka ambil clock-in nya dari yang pertama.
        // jika belum pernah finger == rollcall manual ini sebagai clockin nya ? (Asumsi mesin finger mati);        

        // step 1. cari attendance clock_in yang hari Ini dan belum Rollcall
        $sql = "
            SELECT
                a.attendance_id,
                a.driver_cd,
                a.attendance_dt,
                TIME_FORMAT(a.clock_in,'%H:%i') as departure_actual,
                TIME_FORMAT(a.clock_out,'%H:%i') as arrival_actual,
                a.rollcall_id,
                a.clock_in,
                b.lp_cd
            FROM 
                tb_r_attendance a
                INNER JOIN tb_r_timetable_detail b ON b.driver_cd = a.driver_cd 
                INNER JOIN tb_m_logistic_point c ON c.lp_cd = b.lp_cd AND c.empty_plan_flg = '1'
            WHERE 
                a.attendance_dt = '" . $dt . "' 
                AND a.rollcall_id IS NULL
                AND b.departure_dt = '" . $dt . "' 
                AND b.departure_plan IS NOT NULL
                AND b.lp_cd = '" . $m_lp_cd . "'
                
                AND b.route = '" . $m_route . "'
                AND b.cycle = '" . $m_cycle . "'
            ORDER BY clock_in 
            LIMIT 1
        ";        
        $att = $this->db->query($sql)->result();

        if (count($att) == 0)
        {
            // isikan attendance nya
            $data_att['driver_cd'] = $m_driver_cd;
            $data_att['attendance_dt'] = $dt;
            $data_att['clock_in'] = date('Y-m-d H:i:s');
            $data_att['created_by'] = $this->session->userdata(S_EMPLOYEE_NAME);
            $this->db->insert('tb_r_attendance', $data_att);
        }

        // select lagi;
        $sql = "
            SELECT
                a.attendance_id,
                a.driver_cd,
                a.attendance_dt,
                TIME_FORMAT(a.clock_in,'%H:%i') as departure_actual,
                TIME_FORMAT(a.clock_out,'%H:%i') as arrival_actual,
                a.rollcall_id,
                a.clock_in,
                b.lp_cd
            FROM 
                tb_r_attendance a
                INNER JOIN tb_r_timetable_detail b ON b.driver_cd = a.driver_cd 
                INNER JOIN tb_m_logistic_point c ON c.lp_cd = b.lp_cd AND c.empty_plan_flg = '1'
            WHERE 
                a.attendance_dt = '" . $dt . "' 
                AND a.rollcall_id IS NULL
                AND b.departure_dt = '" . $dt . "' 
                AND b.departure_plan IS NOT NULL
                AND b.lp_cd = '" . $m_lp_cd . "'
                
                AND b.route = '" . $m_route . "'
                AND b.cycle = '" . $m_cycle . "'
            ORDER BY clock_in 
            LIMIT 1
        ";
        $att = $this->db->query($sql)->result();


        if (count($att) > 0)
        {
            // step 2. Cek ke table tb_r_timetable_detail
            $att = $att[0];

            $sql = "
            SELECT                 
                a.timetable_detail_id,
                a.timetable_id,
                DATE_FORMAT(a.timetable_dt,'%d/%m/%Y') as timetable_dt,
                b.business,
                b.customer,
                a.route,
                a.cycle,
                DATE_FORMAT(a.departure_dt,'%d/%m/%Y') as departure_dt,
                DATE_FORMAT(DATE_ADD(DATE_FORMAT(CONCAT(a.departure_dt,' ', a.departure_plan),'%Y-%m-%d %H:%i:%s'), INTERVAL -20 MINUTE), '%H:%i') AS departure_plan,
                a.driver_cd,
                a.driver_name,
                d.smartphone,
                DATE_FORMAT(d.driving_license_val,'%d/%m/%Y') as driving_license_val,
                DATE_FORMAT(d.forklift_license_val,'%d/%m/%Y') as forklift_license_val,
                a.vehicle_cd,
                DATE_FORMAT(e.license_stnk,'%d/%m/%Y') as license_stnk,
                DATE_FORMAT(e.license_keur,'%d/%m/%Y') as license_keur,
                DATE_FORMAT(e.license_sipa,'%d/%m/%Y') as license_sipa,
                DATE_FORMAT(DATE_ADD(DATE_FORMAT(CONCAT(a.departure_dt,' ', a.departure_plan),'%Y-%m-%d %H:%i:%s'), INTERVAL -20 MINUTE), '%H:%i') AS arrival_plan_driver
            FROM 
                tb_r_timetable_detail a 
                INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                INNER JOIN tb_m_logistic_point c ON c.lp_cd = a.lp_cd AND c.empty_plan_flg = '1'
                INNER JOIN tb_m_driver d ON d.driver_cd = a.driver_cd
                INNER JOIN tb_m_vehicle e ON e.vehicle_cd = a.vehicle_cd
            WHERE 
                a.departure_dt = '" . $dt . "' 
                AND a.departure_plan IS NOT null
                AND a.driver_cd = '" . $att->driver_cd . "'
                AND a.lp_cd = '" . $_POST['lp_cd'] . "'

                AND a.route = '" . $m_route . "'
                AND a.cycle = '" . $m_cycle . "'
            ORDER BY a.driver_name
            LIMIT 1
            ;
            "; // echo $sql;
            $timetable = $this->db->query($sql)->result();                        

            if (count($timetable) > 0)
            {
                // cek apakah dia salah FINGER, udah pernah Rollcall Tapi finger lagi di mesin IN
                // cek rollcall nya berdasarkan driver_cd, tgl, dan route-cycle di tb_r_rollcall

                // TODO
                $sql = "
                    select count(a.rollcall_id) AS cnt from tb_r_rollcall a
                    WHERE 
                        a.departure_dt = '" . $dt . "'
                        AND a.driver_cd = '" . $att->driver_cd . "'
                        AND a.route = '" . $timetable[0]->route . "'
                        AND a.cycle = '" . $timetable[0]->cycle . "'
                    ";
                $cek_jml_rc = $this->db->query($sql)->row()->cnt;
                if ($cek_jml_rc == 0)
                {
                    // Hanya boleh rollcall jika 30 menit sebelum Departure Plan.
                    date_default_timezone_set('Asia/Jakarta');

                    $now = date('Y-m-d H:i:s');
                    $depart = date('Y-m-d', strtotime( str_replace('/', '-', $timetable[0]->departure_dt))) . ' ' . $timetable[0]->departure_plan . ':00';

                    // cari selisihnya brp lama
                    $now = time(); // or your date as well
                    $your_date = strtotime($depart);
                    $datediff = $your_date - $now;

                    $selisihMenit = round($datediff / 60);

                    // echo $selisihMenit; die();

                    if ($selisihMenit < 15)
                    {
                        // ada datanya.
                        // step 3. Cari Plan Arrival nya
                        $tt = $timetable[0];

                        $sql = "
                        SELECT 
                            -- TIME_FORMAT(a.arrival_plan,'%H:%i') as arrival_plan
                            TIME_FORMAT(DATE_ADD(a.arrival_plan, INTERVAL 20 MINUTE), '%H:%i') as arrival_plan
                        FROM 
                            tb_r_timetable_detail a 
                            INNER JOIN tb_m_logistic_point b ON b.lp_cd = a.lp_cd AND b.empty_plan_flg = '1' 
                        WHERE 
                            a.timetable_id = '" . $tt->timetable_id . "' AND 
                            a.arrival_plan IS NOT NULL AND 
                            a.departure_plan IS NULL 
                            AND a.route = '" . $tt->route . "'
                            AND a.cycle = '" . $tt->cycle . "'
                            and a.driver_cd = '" . $tt->driver_cd . "'
                        LIMIT 1
                        ";

                        $arrival_plan = '';

                        $ap = $this->db->query($sql)->result();
                        if (count($ap) > 0)
                        {
                            $arrival_plan = $ap[0]->arrival_plan;
                        }

                        // step 4. Cari Cashier Summary
                        $sql = "
                        SELECT 
                            SUM(a.km) AS km, 
                            SUM(a.fuel) AS fuel, 
                            GROUP_CONCAT(distinct a.spbu) AS spbu, 
                            SUM(a.tol) AS tol , 
                            SUM(a.others) AS others
                        FROM 
                            tb_r_timetable_detail a 
                        WHERE 
                            a.timetable_id = '" . $tt->timetable_id . "' AND 
                            a.route = '" . $tt->route . "' AND 
                            a.driver_cd = '" . $tt->driver_cd . "' AND 
                            a.vehicle_cd = '" . $tt->vehicle_cd . "'
                        GROUP BY
                            a.timetable_id, a.route, a.driver_cd, a.vehicle_cd
                        ";
                        $cashier = $this->db->query($sql)->row();

                        // update arrival_actual di timetable
                        $data_arrival['arrival_actual'] = $att->clock_in;
                        $agap = $this->get_time_difference($tt->arrival_plan_driver, date('H:i:s', strtotime($att->clock_in)));
                        $data_arrival['arrival_gap'] = $agap;
                        $data_arrival['arrival_eval'] = $this->get_time_eval($tt->arrival_plan_driver, date('H:i:s', strtotime($att->clock_in)));

                        $this->db->where('timetable_detail_id', $tt->timetable_detail_id);
                        $this->db->update('tb_r_timetable_detail', $data_arrival);

                        $success = true;
                        $message = 'Attendance Ditemukan';// . $tt->arrival_plan_driver . ' == ' . date('H:i:s', strtotime($att->clock_in));
                    }
                    else
                    {
                        // driver absen belum < 45 menit.
                        $data['rollcall_id'] = '-1';
                        $this->db->where('attendance_id', $att->attendance_id);
                        $this->db->update('tb_r_attendance', $data);
                        $message = 'Attendance dan Timetable ditemukan [' . $timetable[0]->driver_name . ' - ' . $timetable[0]->route . '], namun belum < 15 Menit waktu Rollcall';
                    }
                }
                else
                {
                    // minuskan. salah absen pulang
                    $data['rollcall_id'] = '-1';
                    $this->db->where('attendance_id', $att->attendance_id);
                    $this->db->update('tb_r_attendance', $data);
                    $message = 'Attendance dan Timetable ditemukan [' . $timetable[0]->driver_name . ' - ' . $timetable[0]->route . '], namun Salah Finger OUT di Mesin IN ';
                }
            }
            else
            {
                // Ada driver tapi tidak ada timetable.. update rollcall_id jadi -1, (Skip and find next driver finger)

                // update rollcall_id = -1 hanya jika lp code nya sesuai.

                $sql = "
                    SELECT                 
                        a.timetable_detail_id                     
                    FROM 
                        tb_r_timetable_detail a 
                        INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
                        INNER JOIN tb_m_logistic_point c ON c.lp_cd = a.lp_cd AND c.empty_plan_flg = '1'
                        INNER JOIN tb_m_driver d ON d.driver_cd = a.driver_cd
                        INNER JOIN tb_m_vehicle e ON e.vehicle_cd = a.vehicle_cd
                    WHERE 
                        a.timetable_dt = '" . $dt . "' 
                        AND a.departure_plan IS NOT null
                        AND a.driver_cd = '" . $att->driver_cd . "'                        
                    ORDER BY a.driver_name
                    LIMIT 1
                    ;
                    "; // echo $sql;
                $timetable = $this->db->query($sql)->result();
                if (count($timetable) == 0)
                {
                    // hanya delete jika benar2 tidak ada
                    $data['rollcall_id'] = '-1';
                    $this->db->where('attendance_id', $att->attendance_id);
                    $this->db->update('tb_r_attendance', $data);

                }

                $message = 'Attendance Ditemukan : ' . $att->driver_cd . ', namun Tidak ada timetable untuk ' . $_POST['lp_cd'];
            }
        }

        $result['success']      = $success;
        $result['message']      = $message;
        $result['data_att']     = $att;
        $result['data_tt']      = $tt;
        $result['data_cashier'] = $cashier;
        $result['arrival_plan'] = $arrival_plan;

        echo json_encode($result);
    }

    function accessmanual()
    {
        $success = false;
        $message = 'Hak akses tidak bisa diberikan. Username/Password Salah atau bukan Supervisor/Admin.';

        $manual_rollcall_username = $_POST['manual_rollcall_username'];
        $manual_rollcall_password = $_POST['manual_rollcall_password'];

        $sql = "
            SELECT
                b.user_group_description
            FROM 
                tb_m_users a
                INNER JOIN tb_m_user_group b ON b.user_group_id = a.user_group_id
            WHERE 
                a.user_email = " . $this->db->escape($manual_rollcall_username) . "
                AND a.user_password = MD5(" . $this->db->escape($manual_rollcall_password) . "); 
        ";
        // echo $sql;
        $user = $this->db->query($sql)->result();

        if (count($user) > 0)
        {
            $role_nm = strtolower(str_replace(' ', '', $user[0]->user_group_description));
            if ($role_nm == 'administrator' || $role_nm == 'supervisor')
            {
                $success = true;
                $message = 'Hak akses berhasil';
            }    
        }


        $result['success']      = $success;
        $result['message']      = $message;
        echo json_encode($result);
    }
}
