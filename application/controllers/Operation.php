<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 06 Maret 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operation extends MY_Controller {

    function __construct() {
        parent:: __construct();

        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }

        $this->load->model('Operation_model');
        $this->load->model('Timetable_model');
    }

    function index() 
    {
        date_default_timezone_set('Asia/Jakarta');
        $timetable_dt = date('Y-m-d');
        $this->id($timetable_dt);
    }

    function id($timetable_dt, $nya = '')
    {
        $data['stitle'] = 'Operation';
        $data['jsapp'] = array('operation');
        $data['nya'] = $nya;

        $data['business'] = $this->db->query("
            SELECT DISTINCT business FROM tb_r_timetable WHERE timetable_dt = '" . $timetable_dt . "';
        ")->result();

        $data['customers'] = $this->db->query("
            SELECT DISTINCT a.customer, a.business, b.system_value_txt FROM tb_r_timetable a
            INNER JOIN tb_m_system b ON b.system_code = a.customer AND b.system_type = 'customer'
            WHERE timetable_dt = '" . $timetable_dt . "' ORDER BY a.customer
        ")->result();

        $data['customer_lp_cds'] = $this->db->query("
            SELECT DISTINCT customer_lp_cd, customer, business FROM tb_r_timetable 
            WHERE timetable_dt = '" . $timetable_dt . "' ORDER BY customer_lp_cd;
        ")->result();

        $data['routes'] = $this->db->query("
            SELECT 
                DISTINCT a.route, b.business, b.customer, b.customer_lp_cd
            FROM 
                tb_r_timetable_detail a
                INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
            WHERE 
                a.timetable_dt = '" . $timetable_dt . "'
            ORDER BY a.route
        ")->result();

        $data['cycles'] = $this->db->query("
            SELECT 
                DISTINCT a.cycle, a.route, b.business, b.customer, b.customer_lp_cd
            FROM 
                tb_r_timetable_detail a
                INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
            WHERE 
                a.timetable_dt = '" . $timetable_dt . "'
            ORDER BY b.business, b.customer, b.customer_lp_cd, a.route, a.cycle
        ")->result();

        $data['timetable_dt'] = $timetable_dt;

        $data['drivers'] = $this->Timetable_model->getDrivers();
        $data['vehicles'] = $this->Timetable_model->getVehicles();
        $data['lps'] = $this->Timetable_model->getCustomerLpCds(); 

        $this->load->view('header', $data);
        $this->load->view('operation');
        $this->load->view('footer');
    }

    function getSummary()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
        $sv		= $this->input->post('search')['value'];
        $timetable_dt	= $this->input->post('timetable_dt', true);
        $business	= $this->input->post('business', true);
        $customer	= $this->input->post('customer', true);
        $customer_lp_cd	= $this->input->post('customer_lp_cd', true);
        $route	= $this->input->post('route', true);
        $cycle	= $this->input->post('cycle', true);

        $nya = $_POST['nya'];
		
        $results 			= $this->Operation_model->getTimetableSummary($timetable_dt, $business, $customer, $customer_lp_cd, $route, $cycle, $start, $length, $sv, $order, $columns, $nya);
        $recordsTotal       = (int)$this->Operation_model->getCountOfTimetableSummary($timetable_dt, $business, $customer, $customer_lp_cd, $route, $cycle, $sv, $nya);
		        
        $data = array(); $no = 1;
		foreach ($results as $r) 
		{
            $row = array();
            
            $row[] = $no;
            $row[] = $r->business;
            $row[] = $r->customer;
            $row[] = $r->customer_lp_cd;
            $row[] = $r->route;
            $row[] = $r->cycle;
            $row[] = $r->vehicle_cd;
            $row[] = $r->driver_name;
            $row[] = $r->total;
            $row[] = $r->done;
            $row[] = $r->remain;

            $siz = ($r->percentage == '0') ? 'style="color: red;"' : '';
            $progress = '
                <div class="progress progress-lg pb-tt">
                    <div '.$siz.' class="progress-bar bg-primary" role="progressbar" aria-valuenow="'.$r->percentage.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$r->percentage.'%;">
                        '.$r->percentage.'%
                    </div>
                </div>
            ';

            $row[] = $progress;

            $btn = '        
            <button type="button" class="btn btn-purple waves-effect waves-light btn-sm" title="Change Truck" onclick="changeTruck(\'truck\',\''.$r->timetable_id.'\',\''.$r->route.'\',\''.$r->cycle.'\',\''.$r->vehicle_cd.'\',\''.$r->driver_cd.'\')"><i class="mdi mdi-truck"></i></button>
            <button type="button" class="btn btn-purple waves-effect waves-light btn-sm" title="Change Driver" onclick="changeTruck(\'driver\',\''.$r->timetable_id.'\',\''.$r->route.'\',\''.$r->cycle.'\',\''.$r->vehicle_cd.'\',\''.$r->driver_cd.'\')"><i class="mdi mdi-account"></i></button>
            <button id="btn-' . str_replace(' ', '_', $r->customer_lp_cd) .'-'.  str_replace(' ','_', $r->route).'-'.$r->cycle.'" type="button" class="btn btn-purple waves-effect waves-light btn-sm" title="View Detail" onclick="viewDetail(\''.$r->timetable_id.'\', \''.$r->route.'\',\''.$r->cycle.'\',\''.$r->business.'\',\''.$r->customer.'\',\''.$r->customer_lp_cd.'\')"><i class=" mdi mdi-magnify"></i></button>
            <button id="btn-del-' . str_replace(' ', '_', $r->customer_lp_cd) .'-' . str_replace(' ','_', $r->route).'-'.$r->cycle.'" type="button" class="btn btn-danger waves-effect waves-light btn-sm" title="Delete" onclick="delPerRC(\''.$r->timetable_id.'\', \''.$r->route.'\',\''.$r->cycle.'\')"><i class=" mdi mdi-recycle"></i></button>
            ';

            $row[] = $btn;
          
            $data[] = $row;
            
            $no++;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }

    function getDetails()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
        $sv		= $this->input->post('search')['value'];
        $timetable_id	= $this->input->post('timetable_id', true);
        $route	= $this->input->post('route', true);
        $cycle	= $this->input->post('cycle', true);
		
        $results 			= $this->Timetable_model->getTimetableDetails(md5($timetable_id), $route, $cycle, $start, $length, $sv, $order, $columns);
        $recordsTotal       = (int)$this->Timetable_model->getCountOfTimetableDetail(md5($timetable_id), $route, $cycle, $sv);
		        
        $data = array();
		foreach ($results as $r) 
		{
            $row = array();			
                        
            $row[] = $r->route;
            $row[] = $r->cycle;
            $row[] = $r->vehicle_cd;
            $row[] = $r->vehicle_number;            
            $row[] = $r->driver_cd;
            $row[] = $r->driver_name;
            $row[] = $r->lp_cd;
            $row[] = date('d-m-Y', strtotime($r->arrival_dt));
            $row[] = substr($r->arrival_plan, 0, 5);
            $row[] = ($r->arrival_actual != '') ? date('H:i', strtotime($r->arrival_actual)) : '-';

            $agap = '-';
            if ($r->arrival_gap != '')
            {
                if ($r->arrival_actual > ($r->arrival_dt . ' ' .$r->arrival_plan))
                {
                    $agap = $this->get_time_difference($r->arrival_plan, date('H:i:s', strtotime($r->arrival_actual)));
                }
                else
                {
                    $agap = $this->get_time_difference(date('H:i:s', strtotime($r->arrival_actual)), $r->arrival_plan);                    
                }
                
            }

            // jika keberangkatan
            if ($r->arrival_plan == '')
            {
                $agap = $r->arrival_gap;
            }

            $row[] = $agap;
            $row[] = $r->arrival_eval;
            $row[] = date('d-m-Y', strtotime($r->departure_dt));
            $row[] = substr($r->departure_plan, 0, 5);
            $row[] = ($r->departure_actual != '') ? date('H:i', strtotime($r->departure_actual)) : '-';

            $dgap = '-';
            if ($r->departure_gap != '')
            {
                if ($r->departure_actual > ($r->departure_dt . ' ' .$r->departure_plan))
                {
                    $dgap = $this->get_time_difference($r->departure_plan, date('H:i:s', strtotime($r->departure_actual)));
                }
                else
                {
                    $dgap = $this->get_time_difference(date('H:i:s', strtotime($r->departure_actual)), $r->departure_plan);                    
                }
            }
            $row[] = $dgap;
            $row[] = $r->departure_eval;
            $row[] = $r->remark;
            $row[] = $r->arrival_problem;
            $row[] = $r->departure_problem;
            $row[] = $r->timetable_detail_id;
            $row[] = $r->arrival_next_day;
            $row[] = $r->arrival_manual_by;
            $row[] = date('d-M-Y H:i:s', strtotime($r->arrival_manual_dt));
            $row[] = $r->departure_manual_by;
            $row[] = date('d-M-Y H:i:s', strtotime($r->departure_manual_dt));

            // addition 25-Mei-2020
            $row[] = $r->km;
            $row[] = $r->fuel;
            $row[] = $r->spbu;
            $row[] = $r->tol;
            $row[] = $r->others;
            $row[] = $r->pallet;
            $row[] = $r->rack;
            $row[] = $r->division;

			$data[] = $row;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }

    function get_time_difference($time1, $time2) {
        $time1 = strtotime("1980-01-01 $time1");
        $time2 = strtotime("1980-01-01 $time2");
        
        if ($time2 < $time1) {
            $time2 += 86400;
        }
        
        return date("H:i", strtotime("1980-01-01 00:00:00") + ($time2 - $time1));
    }
}
