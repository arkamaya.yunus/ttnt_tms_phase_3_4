<?php

/*
 * @author: irfan.satriadarma@gmail.com
 * @created: 2016-12-19
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class App extends CI_controller 
{
    function __construct() 
	{
        parent:: __construct();
		
		if ($this->session->userdata(S_COMPANY_ID) == null) 
		{
            redirect('login');
        }
		
        $this->load->model('app_model');
		$this->load->model('company_model');
    }

    function index() 
	{
		$data['stitle'] 	= '';
		$data['apps']		= $this->app_model->get_apps();
		$data['company']	= $this->company_model->get_company();
		
		$this->load->view('header', $data);
		$this->load->view('app');
        $this->load->view('footer');
	}
	
	function get_shortcut()
	{
		$results = $this->app_model->get_shortcut($_GET['query']);
		
		$suggestions = array();
		foreach ($results as $r)
		{
			$suggestions[] = array(
				'value' => $r->feature_name,
				'data' => $r->feature_action
			);
		}
		
        echo json_encode(array(
			'query' => 'Unit'
			, 'suggestions' => $suggestions
		));
	}
}
?>