<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 06 Maret 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cashier extends MY_Controller {

    function __construct() {
        parent:: __construct();

        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index() {

        $data['stitle'] = 'Rollcall';
        $this->load->view('header', $data);
        $this->load->view('uc');
        $this->load->view('footer');
    }
}
