<?php

/*
 * @author: irfan.satriadarma@gmail.com
 * @created: 2018-11-19
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Backup extends CI_controller 
{
    function __construct() 
	{
        parent:: __construct();
    }
    
    function index()
    {
        // echo '<pre>'; print_r($_SERVER); echo '</pre>';
        // die('tes');
        
        // Load the DB utility class
        $this->load->dbutil();
        
        // Backup your entire database and assign it to a variable
        $backup = $this->dbutil->backup();
        
        // Load the file helper and write the file to your server
        $this->load->helper('file');
        $fname = $_SERVER['SERVER_NAME'] . '__' . date('YmdHis') . '.gz';
        write_file($fname , $backup);
        
        // send email
        $this->load->library('email');
        
        $message = 'fyi, db backup for ' . $_SERVER['SERVER_NAME'];// $this->load->view('register_mail_confirm', $data, true);
            
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
		$config['validation'] = TRUE;

        $this->email->initialize($config);
        $this->email->from(SMTP_USER, EMAIL_ALIAS);
        $this->email->to('personalia.id@gmail.com');
        $this->email->bcc('admin@arkamaya.co.id');
        $this->email->subject('Daily backup database ' . $_SERVER['SERVER_NAME']);
        $this->email->message($message);
        $this->email->attach($fname);

        if ($this->email->send()) 
		{
		    unlink($fname);
            echo 'backup db success';
            
        } 
		else 
		{
            echo 'backup db failed';
        }
        
        // Load the download helper and send the file to your desktop
        //$this->load->helper('download');
        //force_download('mybackup.gz', $backup);
        
    }
}