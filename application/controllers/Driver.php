<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 16 Maret 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Driver extends MY_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->model('Driver_model');

        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index() {

        $data['stitle'] = 'Driver';
        $data['jsapp'] = array('driver');
        $data['logistic_partners'] = $this->Driver_model->getLogisticPartners();
        
        $this->load->view('header', $data);
        $this->load->view('driver');
        $this->load->view('footer');
    }

    function gets()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
		$sv		= $this->input->post('search')['value'];
        $logistic_partner = $this->input->post('logistic_partner', true);
        $active = $this->input->post('active', true);
        $business = $this->input->post('business', true);
		
        $results 			= $this->Driver_model->getDrivers($logistic_partner, $active, $start, $length, $sv, $order, $columns, $business);
        $recordsTotal       = (int)$this->Driver_model->getCountOfDriver($logistic_partner, $active, $sv, $business);
		        
        $data = array();
		foreach ($results as $r) 
		{
            $row = array();
            
            $filep = COMPANY_ASSETS_PATH. 'cid' . $this->session->userdata(S_COMPANY_ID) . '/profile/thumbs/' . $r->photo;
            $photo = 'assets/images/user_photo.png';										
            if ($r->photo != '') {
                if (file_exists($filep)) {
                    $photo = $filep;
                }
            }
            
            $row[] = '<img src="'.base_url().$photo.'" class="img-thumbnail" alt="'.$r->photo.'" style="max-height: 40px; height: 40px; padding: 0; min-width: 50px" />';
            $row[] = $r->driver_cd;
            $row[] = '<a href="'.site_url('driver/id/' . md5($r->driver_cd)).'" title="View Detail '.$r->driver_name.'">' . $r->driver_name . '</a>';
			$row[] = $r->logistic_partner;
            $row[] = $r->jobdesc;
            $row[] = $r->smartphone;
            // $row[] = $r->phone;
            $row[] = ($r->driving_license_val != '' && $r->driving_license_val != '0000-00-00') ? date('d-M-Y', strtotime($r->driving_license_val)) : '-';
            $row[] = $r->driving_license_type;
            $row[] = ($r->forklift_license_val != '' && $r->forklift_license_val != '0000-00-00') ? date('d-M-Y', strtotime($r->forklift_license_val)) : '-';
            $row[] = $r->forklift_license_type;
            $row[] = ($r->join_dt != '' && $r->join_dt != '0000-00-00') ? date('d-M-Y', strtotime($r->join_dt)) : '-';
            $row[] = ($r->join_dt != '' && $r->join_dt != '0000-00-00') ? $this->getLengthOfWorking($r->join_dt) : '-'; //$r->length_of_working;
            $row[] = $r->num_of_accident;
            $row[] = ($r->active == '1') ? 'Yes' : 'No';
            $row[] = ($r->resign_dt != '' && $r->resign_dt != '0000-00-00') ? date('d-M-Y', strtotime($r->resign_dt)) : '-';
            $row[] = $r->att_id;
            $row[] = $r->dob;
            $row[] = $r->business;

			$data[] = $row;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }

    function id()
    {
        // deletion
        if (isset($_POST['driver_cd']))
        {
            $this->Driver_model->delete($_POST['driver_cd']);
			$this->session->set_flashdata('notif_success', '<strong>Success.</strong> Driver deleted.');
			redirect('driver');
        }

        $driver_cd = $this->uri->segment(3);
        if ($driver_cd == '') redirect('driver');

        $driver = $this->Driver_model->getDriver($driver_cd);
        if (count($driver) == 0) redirect('driver');

        $data['driver'] = $driver[0];
        $data['stitle'] = $driver[0]->driver_name;
        $data['jsapp'] = array('driver_id');
        $data['low'] = ($driver[0]->join_dt != '') ? $this->getLengthOfWorking($driver[0]->join_dt) : '-';

        $this->load->view('header', $data);
        $this->load->view('driver_id');
        $this->load->view('footer');
    }

    function create()
	{
        $save_sts = '';
        $driver_cd = $driver_name = $logistic_partner = $jobdesc = $smartphone = $phone = '';
        $driving_license_val = $driving_license_type = $forklift_license_val = $forklift_license_type = '';
        $length_of_working = $num_of_accident = $join_dt = '';
        $photo = $active = $resign_dt = $att_id = $dob = $business = '';

		if (isset($_POST['driver_cd']))
		{
            $save_sts = $this->Driver_model->save() ;
            if ($save_sts == '1')
            {
                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> Driver Created.');						
			    redirect('driver/id/' . md5($this->input->post('driver_cd', true)), 'refresh');
            }
            else
            {
                $driver_cd = $this->input->post('driver_cd', true);
                $driver_name = $this->input->post('driver_name', true);
                $logistic_partner = $this->input->post('logistic_partner', true);
                $jobdesc = $this->input->post('jobdesc', true);
                $smartphone = $this->input->post('smartphone', true);
                $phone = $this->input->post('phone', true);
                $driving_license_val = $this->input->post('driving_license_val', true);
                $driving_license_type = $this->input->post('driving_license_type', true);
                $forklift_license_val = $this->input->post('forklift_license_val', true);
                $forklift_license_type = $this->input->post('forklift_license_type', true);
                $length_of_working = $this->input->post('length_of_working', true);
                $num_of_accident = $this->input->post('num_of_accident', true);
                $join_dt = $this->input->post('join_dt', true);
                $active = $this->input->post('active', true);
                $resign_dt = $this->input->post('resign_dt', true);
                $att_id = $this->input->post('att_id', true);
                $dob = $this->input->post('dob', true);
                $business = $this->input->post('business', true);
            }
		}
		
		$data['jsapp'] 		= array('driver_create');
        $data['stitle'] = 'Create Driver';
        $data['save_sts'] = $save_sts;

        // filled the field
        $data['driver_cd'] = $driver_cd;
        $data['driver_name'] = $driver_name;
        $data['logistic_partner'] = $logistic_partner;
        $data['jobdesc'] = $jobdesc;
        $data['smartphone'] = $smartphone;
        $data['phone'] = $phone;
        $data['driving_license_val'] = $driving_license_val;
        $data['driving_license_type'] = $driving_license_type;
        $data['forklift_license_val'] = $forklift_license_val;
        $data['forklift_license_type'] = $forklift_license_type;
        $data['length_of_working'] = $length_of_working;
        $data['num_of_accident'] = $num_of_accident;
        $data['join_dt'] = $join_dt;
        $data['photo'] = $photo;
        $data['active'] = $active;
        $data['resign_dt'] = $resign_dt;
        $data['att_id'] = $att_id;
        $data['dob'] = $dob;
        $data['business'] = $business;        
        
		$this->load->view('header', $data);
		$this->load->view('driver_create');
		$this->load->view('footer');
	}

    function get_logistic_partners()
	{
		$results = $this->Driver_model->getLogisticPartners($_GET['query']);
		
		$suggestions = array();
		foreach ($results as $r)
		{
			$suggestions[] = array(
				'value' => $r->logistic_partner,
				'data' => $r->logistic_partner
			);
		}
		
        echo json_encode(array(
			'query' => 'Unit'
			, 'suggestions' => $suggestions
		));
    }
    
    function edit()
	{
        $save_sts = '';

        $driver_cd = $this->uri->segment(3);
        if ($driver_cd == '') redirect('driver');

        $driver = $this->Driver_model->getDriver($driver_cd);
        if (count($driver) == 0) redirect('driver');
        $driver = $driver[0];

        $driver_cd = $driver->driver_cd;
        $driver_name = $driver->driver_name;
        $logistic_partner = $driver->logistic_partner;
        $jobdesc = $driver->jobdesc;
        $smartphone = $driver->smartphone;
        $phone = $driver->phone;
        $driving_license_val = $driver->driving_license_val;
        $driving_license_type = $driver->driving_license_type;
        $forklift_license_val = $driver->forklift_license_val;
        $forklift_license_type = $driver->forklift_license_type;
        $length_of_working = $driver->length_of_working;
        $num_of_accident = $driver->num_of_accident;
        $join_dt = $driver->join_dt;
        $photo = $driver->photo;
        $active = $driver->active;
        $resign_dt = $driver->resign_dt;
        $att_id = $driver->att_id;
        $dob = $driver->dob;
        $business = $driver->business;

		if (isset($_POST['driver_cd']))
		{
            $save_sts = $this->Driver_model->save('1') ;
            if ($save_sts == '2')
            {
                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> Driver Updated.');						
			    redirect('driver/id/' . md5($this->input->post('driver_cd', true)), 'refresh');
            }
            else
            {
                $driver_cd = $this->input->post('driver_cd', true);
                $driver_name = $this->input->post('driver_name', true);
                $logistic_partner = $this->input->post('logistic_partner', true);
                $jobdesc = $this->input->post('jobdesc', true);
                $smartphone = $this->input->post('smartphone', true);
                $phone = $this->input->post('phone', true);
                $driving_license_val = $this->input->post('driving_license_val', true);
                $driving_license_type = $this->input->post('driving_license_type', true);
                $forklift_license_val = $this->input->post('forklift_license_val', true);
                $forklift_license_type = $this->input->post('forklift_license_type', true);
                $length_of_working = $this->input->post('length_of_working', true);
                $num_of_accident = $this->input->post('num_of_accident', true);
                $join_dt = $this->input->post('join_dt', true);
                $active = $this->input->post('active', true);
                $resign_dt = $this->input->post('resign_dt', true);
                $att_id = $this->input->post('att_id', true);
                $dob = $this->input->post('dob', true);
                $business = $this->input->post('business', true);
            }
		}
		
		$data['jsapp'] 		= array('driver_edit');
        $data['stitle'] = 'Edit Driver';
        $data['save_sts'] = $save_sts;

        // filled the field
        $data['driver_cd'] = $driver_cd;
        $data['driver_name'] = $driver_name;
        $data['logistic_partner'] = $logistic_partner;
        $data['jobdesc'] = $jobdesc;
        $data['smartphone'] = $smartphone;
        $data['phone'] = $phone;
        $data['driving_license_val'] = $driving_license_val;
        $data['driving_license_type'] = $driving_license_type;
        $data['forklift_license_val'] = $forklift_license_val;
        $data['forklift_license_type'] = $forklift_license_type;
        $data['length_of_working'] = $length_of_working;
        $data['num_of_accident'] = $num_of_accident;
        $data['join_dt'] = $join_dt;
        $data['photo'] = $photo;
        $data['active'] = $active;
        $data['resign_dt'] = $resign_dt;
        $data['att_id'] = $att_id;
        $data['dob'] = $dob;
        $data['business'] = $business;
        
		$this->load->view('header', $data);
		$this->load->view('driver_edit');
		$this->load->view('footer');
    }
    
    function getLengthOfWorking($join_dt)
    {
        // $timeago = $this->time_elapsed_string($r->start_working_dt, true);
        $full = true;
        $now = new DateTime;
        $ago = new DateTime($join_dt);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'Years',
            'm' => 'Months',
            'd' => 'Days'
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v;
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        $result = $string ? implode(' ', $string) : 'New';
        return $result;
    }

    function download($logistic_partner, $active, $business)
    {
        $this->load->library('Excel');
		
		$tpl_path = './assets/files/template-driver.xlsx';
		
		//$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($tpl_path);
		$sheet = $objPHPExcel->getActiveSheet();
		$sheet->setTitle('Driver');
		
		// create data..
		$row = 5;
		
		$data = $this->Driver_model->getDriversForDownload($logistic_partner, $active, $business);
		// No	NIK	Driver Name	Logistic Partner	Jobdesc	Phone Number	Driving License Validation	Type of Driver License	SIO	SIO Type	Join Date	Length Of Working	No Of Accident
        $i = 1;
        foreach($data as $d) {
			$sheet->setCellValue('A'.$row, $i);
			$sheet->setCellValue('B'.$row, $d->driver_cd);
			$sheet->setCellValue('C'.$row, $d->driver_name);
			$sheet->setCellValue('D'.$row, $d->logistic_partner);
			$sheet->setCellValue('E'.$row, $d->jobdesc);
			$sheet->setCellValue('F'.$row, $d->smartphone);
			$sheet->setCellValue('G'.$row, $d->driving_license_val);
			$sheet->setCellValue('H'.$row, $d->driving_license_type);
			$sheet->setCellValue('I'.$row, $d->forklift_license_val);
			$sheet->setCellValue('J'.$row, $d->forklift_license_type);
            $sheet->setCellValue('K'.$row, $d->join_dt);
            $sheet->setCellValue('L'.$row, ($d->join_dt != '') ? $this->getLengthOfWorking($d->join_dt) : '');
            $sheet->setCellValue('M'.$row, $d->num_of_accident);

            $active = ($d->active == '1') ? 'Yes' : 'No';
            $sheet->setCellValue('N'.$row, $active);
            $sheet->setCellValue('O'.$row, $d->resign_dt);
            $sheet->setCellValue('P'.$row, $d->att_id);
            $sheet->setCellValue('Q'.$row, $d->dob);
            $sheet->setCellValue('R'.$row, $d->business);
            $row++;
            $i++;
		}
				
		$file_name = "Master_Data_Driver_".date('YmdHis').".xlsx";
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment;filename=$file_name");
		header("Cache-Control: max-age=0");
		$objWriter= PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

		unset($sheet, $objPHPExcel);
    }
    
    function upload()
    {
        $success = true;
        $message = '';
        
        
        if(!empty($_FILES['file_attach']['name']))
        {
            $config['upload_path'] = './assets/timetables';
            $config['allowed_types'] = 'xlsx';
            $config['remove_spaces'] = TRUE;

            $filename = '';

            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('file_attach'))
            {
                $success = false;
                $message = $this->upload->display_errors();
            }
            else
            {
                $dataUpload = $this->upload->data();
                $filename   = $dataUpload["file_name"];
            }

            if(!empty($filename))
            {
                $real_path= getcwd() . "/assets/timetables/";        
                
                $this->load->library('excel');
                $inputFileType  = 'Excel2007';
                $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel    = $objReader->load($real_path . $filename);
                $activeSheet    = $objPHPExcel->getActiveSheet();
                $arr_spread     = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $total_row      = count($arr_spread);
                $sheetData      = $activeSheet->toArray(null, true, true, true); 

                // validasi isi header template nya
                if(
                    strtolower(trim($sheetData['4']["A"])) != 'no' || 
                    strtolower(trim($sheetData['4']["B"])) != 'nik' || 
                    strtolower(trim($sheetData['4']["E"])) != 'jobdesc'
                )
                {
                    $success = false;
                    $message = 'Invalid Excel Template. Please download from Download Template link.';
                }
                else
                {
                    $values = '';
                    
                    for($row = 5; $row <= $total_row; $row++)
                    {
                        $is_valid = 1;
                        $error_detail = '';// "<b>Status data : </b><br/>";
                        $arr = array();

                        if(
                            !empty(trim($sheetData[$row]["B"])))
                        {
                            $driver_cd              = $sheetData[$row]["B"];
                            $driver_name            = $sheetData[$row]["C"];
                            $logistic_partner       = $sheetData[$row]["D"];
                            $jobdesc                = $sheetData[$row]["E"];
                            $smartphone             = $sheetData[$row]["F"];
                            $driving_license_val    = $sheetData[$row]["G"];
                            $driving_license_val    = ($driving_license_val != '') ? "'" . $driving_license_val . "'" : 'null';

                            $driving_license_type   = $sheetData[$row]["H"];
                            $forklift_license_val   = $sheetData[$row]["I"];
                            $forklift_license_val   = ($forklift_license_val != '') ? "'" . $forklift_license_val . "'" : 'null';

                            $forklift_license_type  = $sheetData[$row]["J"];
                            $join_dt                = $sheetData[$row]["K"];
                            $join_dt                = ($join_dt != '') ? "'" . $join_dt . "'" : 'null';

                            $num_of_accident        = $sheetData[$row]["M"];
                            $active                 = $sheetData[$row]["N"];
                            $resign_dt              = $sheetData[$row]["O"];
                            $att_id                 = $sheetData[$row]["P"];
                            $dob                    = $sheetData[$row]["Q"];
                            $business               = $sheetData[$row]["R"];

                            $active = (strtolower($active) == 'yes') ? '1' : '0';
                            
                            $values .= "
                                (
                                    '" . $driver_cd . "',
                                    " . $this->db->escape($driver_name) . ",
                                    '" . $logistic_partner . "',
                                    '" . $jobdesc . "',
                                    '" . $smartphone . "',
                                    " . $driving_license_val . ",
                                    '" . $driving_license_type . "',
                                    " . $forklift_license_val . ",
                                    '" . $forklift_license_type . "',
                                    " . $join_dt . ",
                                    '" . $num_of_accident . "',
                                    '" . $active . "',
                                    '" . $resign_dt . "',
                                    '" . $att_id . "',
                                    '" . $dob . "',
                                    '" . $business . "',
                                    '" . $this->session->userdata(S_USER_EMAIL) . "',
                                    '" . date('Y-m-d H:i:s') . "',
                                    '" . $this->session->userdata(S_USER_EMAIL) . "',
                                    '" . date('Y-m-d H:i:s') . "'
                                )
                            ";

                            $values .= ($row < $total_row) ? ', ' : '';
                        }
                    }

                    // remove the file!
                    unlink($real_path . $filename);

                    if ($values != '')
                    {
                        // insert into temporary detail
                        $sql = "insert into tb_m_driver
                            (
                                tb_m_driver.driver_cd, 
                                tb_m_driver.driver_name,
                                tb_m_driver.logistic_partner,
                                tb_m_driver.jobdesc,
                                tb_m_driver.smartphone,
                                tb_m_driver.driving_license_val,
                                tb_m_driver.driving_license_type,
                                tb_m_driver.forklift_license_val,
                                tb_m_driver.forklift_license_type,
                                tb_m_driver.join_dt,
                                tb_m_driver.num_of_accident,
                                tb_m_driver.active,
                                tb_m_driver.resign_dt,
                                tb_m_driver.att_id,
                                tb_m_driver.dob,
                                tb_m_driver.business,
                                tb_m_driver.created_by,tb_m_driver.created_dt,
                                tb_m_driver.changed_by,tb_m_driver.changed_dt
                            )
                            VALUES 
                                " . $values . "                                    
                            ON DUPLICATE KEY UPDATE 
                                tb_m_driver.logistic_partner = VALUES(tb_m_driver.logistic_partner),
                                tb_m_driver.jobdesc = VALUES(tb_m_driver.jobdesc),
                                tb_m_driver.smartphone = VALUES(tb_m_driver.smartphone),
                                tb_m_driver.driving_license_val = VALUES(tb_m_driver.driving_license_val),
                                tb_m_driver.driving_license_type = VALUES(tb_m_driver.driving_license_type),
                                tb_m_driver.forklift_license_val = VALUES(tb_m_driver.forklift_license_val),
                                tb_m_driver.forklift_license_type = VALUES(tb_m_driver.forklift_license_type),
                                tb_m_driver.join_dt = VALUES(tb_m_driver.join_dt),
                                tb_m_driver.num_of_accident = VALUES(tb_m_driver.num_of_accident),
                                tb_m_driver.active = VALUES(tb_m_driver.active),
                                tb_m_driver.resign_dt = VALUES(tb_m_driver.resign_dt),
                                tb_m_driver.att_id = VALUES(tb_m_driver.att_id),
                                tb_m_driver.dob = VALUES(tb_m_driver.dob),
                                tb_m_driver.business = VALUES(tb_m_driver.business),
                                tb_m_driver.changed_by = VALUES(tb_m_driver.changed_by),
                                tb_m_driver.changed_dt = VALUES(tb_m_driver.changed_dt)
                        ";

                        $this->db->query($sql);
                        
                        $message = 'Upload Success. You can double check the data';   
                    }

                                                                    
                }
            }
        }                       

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }
}
