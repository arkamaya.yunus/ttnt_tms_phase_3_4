<?php

/*
* @author: irfan.satriadarma@gmail.com_addref
* @created: 06 Maret 2020
*/

class Dashboard_timeline extends MY_controller
{
    var $menu = array();

    function __construct()
	{
        parent:: __construct();

        $this->load->model('Dashboard_timeline_model');

        if ($this->session->userdata(S_COMPANY_ID) == null)
		{
            redirect('login');
        }
        $this->menu = $this->permission->get_access_menu();
    }

    function index()
	  {
        $check_dtx = $this->input->get('dtx');
        $check_dty = $this->input->get('dty');
        if(empty($check_dtx) && empty($check_dty)){
          $dtx = date('d/m/Y');
          $dty = date('d/m/Y');
        }else {
          $dtx = date('d/m/Y', strtotime( str_replace('-', '/', $this->input->get('dtx'))));
          $dty = date('d/m/Y', strtotime( str_replace('-', '/', $this->input->get('dty'))));
        }

        $get_from = date('Y-m-d', strtotime( str_replace('/', '-', $dtx)));
        $get_to = date('Y-m-d', strtotime( str_replace('/', '-', $dty)));

        $data['get_from'] = $dtx;
        $data['get_to'] = $dty;
    		$data['stitle'] 	= 'Dashboard Timeline';
        $data['jsapp'] = array('dashboard_timeline');
    		$data['externaljs'] = array(); array('https://www.google.com/jsapi', 'https://cdn.datatables.net/fixedcolumns/3.3.1/js/dataTables.fixedColumns.min.js');
    		$data['timeline'] = $this->Dashboard_timeline_model->getTimeline($get_from, $get_to);
    		$this->load->view('header', $data);
    		$this->load->view('dashboard_timeline');
    		$this->load->view('footer');
    }

    function get_detail()
    {
        $from = $this->input->post('from');
        $to = $this->input->post('to');
        $from_db = date('Y-m-d', strtotime( str_replace('/', '-', $from)));
        $to_db = date('Y-m-d', strtotime( str_replace('/', '-', $to)));

        $data = $this->Dashboard_timeline_model->getTimelineDetail($from_db, $to_db);
        echo json_encode($data);
    }
}
