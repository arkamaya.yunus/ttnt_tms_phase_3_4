<?php

/*
* @author: irfan.satriadarma@gmail.com_addref
* @created: 06 Maret 2020
*/

class Dashboard_summary extends MY_controller
{
    var $menu = array();

    function __construct()
	{
        parent:: __construct();

        $this->load->model('Dashboard_model');

        if ($this->session->userdata(S_COMPANY_ID) == null)
		{
            redirect('login');
        }
    }

    function index()
	{
        date_default_timezone_set('Asia/Jakarta');

		$data['stitle'] 	= 'Dashboard Summary';
        $data['jsapp'] = array('dashboard_chart','dashboard_summary');
        $data['customer_lp_cds'] = $this->Dashboard_model->getCustomerLpCds();

        $data['business'] = $this->db->query("
            select system_code, system_value_txt from tb_m_system where system_type = 'business' order by system_value_num
        ")->result();

		$this->load->view('header', $data);
        $this->load->view('dashboard_summary');
		$this->load->view('footer');
    }

    function getSummary()
    {
        $customer_lp_cd = $this->input->post('customer_lp_cd', true);
        $timetable_dt = date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('timetable_dt', true))));

        // where Customer lp
        $wlp = ($customer_lp_cd != 'all') ? " and b.customer_lp_cd = '" . $customer_lp_cd . "'" : '';
        $wlp_a = ($customer_lp_cd != 'all') ? " and a.customer_lp_cd = '" . $customer_lp_cd . "'" : '';

        // total planning
        $sqlPlanning = "
            SELECT a.route, a.cycle FROM tb_r_timetable_detail a
            INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
            WHERE a.timetable_dt = '".$timetable_dt."' " . $wlp . "  and a.vehicle_cd <> 'STANDBY' and a.vehicle_cd <> 'FIELDMAN'
            GROUP BY b.business, b.customer, b.customer_lp_cd, a.route, a.cycle
        ";
        $total_planning = count($this->db->query($sqlPlanning)->result());

        // total arrival done
        // $sqlArrival = "
        //     SELECT COUNT(a.timetable_detail_id) as cnt FROM tb_r_timetable_detail a
        //     INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id and b.customer_lp_cd = a.lp_cd
        //     WHERE a.timetable_dt = '".$timetable_dt."' and a.departure_actual is not null " . $wlp . "  and a.vehicle_cd <> 'STANDBY' and a.vehicle_cd <> 'FIELDMAN'
        // ";
        $sqlArrival = "
            SELECT a.route, a.cycle FROM tb_r_timetable_detail a
            INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id AND b.customer_lp_cd = a.lp_cd
            WHERE
            a.timetable_dt = '".$timetable_dt."' " . $wlp . "
            and a.arrival_actual is not null
            and a.vehicle_cd <> 'STANDBY' and a.vehicle_cd <> 'FIELDMAN'
            GROUP BY b.business, b.customer, b.customer_lp_cd, a.route, a.cycle
        ";
        // $total_arrival = $this->db->query($sqlArrival)->row()->cnt;
        $total_arrival = count($this->db->query($sqlArrival)->result());

        // total truck on-duty
        $sqlTruck = "
            SELECT DISTINCT a.vehicle_cd, a.vehicle_number, c.vehicle_owner FROM tb_r_timetable_detail a
            INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
            inner join tb_m_vehicle c on c.vehicle_cd = a.vehicle_cd
            WHERE a.timetable_dt = '".$timetable_dt."' " . $wlp . " and a.vehicle_cd <> 'STANDBY' and a.vehicle_cd <> 'FIELDMAN'
            ORDER BY vehicle_cd;
        ";
        $data_truck = $this->db->query($sqlTruck)->result();
        $total_truck = count($data_truck);

        $sqlTruckActive = "
            select count(vehicle_cd) as cnt from tb_m_vehicle where active = '1'
        ";
        $total_truck_active = $this->db->query($sqlTruckActive)->row()->cnt;

        // total driver on-duty
        $sqlDriver = "
            SELECT DISTINCT a.driver_cd, a.driver_name, c.logistic_partner FROM tb_r_timetable_detail a
            INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
            INNER JOIN tb_m_driver c ON c.driver_cd = a.driver_cd
            WHERE
                a.timetable_dt = '".$timetable_dt."' " . $wlp . " and a.vehicle_cd <> 'STANDBY' and a.vehicle_cd <> 'FIELDMAN'
                and c.jobdesc = 'DRIVER'
            ORDER BY a.driver_name;
        ";
        $data_driver = $this->db->query($sqlDriver)->result();
        $total_driver = count($data_driver);

        $sqlDriverActive = "
            select count(driver_cd) as cnt from tb_m_driver where active = '1' and jobdesc = 'DRIVER'
        ";
        $total_driver_active = $this->db->query($sqlDriverActive)->row()->cnt;

        $total_business_truck = $this->db->query("
            SELECT b.business, COUNT(distinct vehicle_cd) AS total_truck FROM tb_r_timetable_detail a
            INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
            WHERE
                a.timetable_dt = '".$timetable_dt."' and a.vehicle_cd <> 'STANDBY' and a.vehicle_cd <> 'FIELDMAN'
            GROUP BY b.business;
        ")->result();

        $total_business_driver = $this->db->query("
            SELECT b.business, COUNT(DISTINCT a.driver_cd) AS total_driver FROM tb_r_timetable_detail a
            INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
            INNER JOIN tb_m_driver c ON c.driver_cd = a.driver_cd
            WHERE
                a.timetable_dt = '".$timetable_dt."' and a.vehicle_cd <> 'STANDBY' and a.vehicle_cd <> 'FIELDMAN' and c.jobdesc = 'DRIVER'
            GROUP BY b.business;
        ")->result();

        $total_business_cycle = $this->db->query("
        SELECT b.business, count(distinct a.route, a.cycle) AS total_cycle FROM tb_r_timetable_detail a
        INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
        WHERE a.timetable_dt = '".$timetable_dt."' and a.vehicle_cd <> 'STANDBY' and a.vehicle_cd <> 'FIELDMAN'
        GROUP BY
            b.business
        ")->result();

        // total delay
        $sql = "
            SELECT
                'MILKRUN' AS business, COUNT(a.timetable_id) AS total_delay
            FROM tb_r_timetable a
                INNER JOIN tb_r_timetable_detail b ON b.timetable_id = a.timetable_id
            WHERE
                a.business = 'MILKRUN'
                AND b.timetable_dt = '".$timetable_dt."'
                AND b.lp_cd = a.customer_lp_cd
                AND b.arrival_actual IS null
                AND CAST(CONCAT(b.arrival_dt, ' ', b.arrival_plan) AS DATETIME) < NOW()
                AND b.vehicle_cd <> 'STANDBY' and b.vehicle_cd <> 'FIELDMAN'

            UNION ALL

            SELECT
                'REGULER' AS business, COUNT(a.timetable_id) AS total_delay
            FROM tb_r_timetable a
                INNER JOIN tb_r_timetable_detail b ON b.timetable_id = a.timetable_id
            WHERE
                a.business = 'REGULER'
                AND b.timetable_dt = '".$timetable_dt."'
                AND b.lp_cd = a.customer_lp_cd
                AND b.arrival_actual IS null
                AND CAST(CONCAT(b.arrival_dt, ' ', b.arrival_plan) AS DATETIME) < NOW()
                AND b.vehicle_cd <> 'STANDBY'  and b.vehicle_cd <> 'FIELDMAN'
        ";
        $total_delay = $this->db->query($sql)->result();

        $output = array(
            // 'departure' => $dataDeparture,
            // 'arrival' => $dataArrival,
            'total_planning' => $total_planning,
            'total_arrival' => $total_arrival,
            'total_truck' => $total_truck,
            'total_truck_active' => $total_truck_active,
            'data_truck' => $data_truck,
            'total_driver' => $total_driver,
            'total_driver_active' => $total_driver_active,
            'data_driver' => $data_driver,
            'total_business_truck' => $total_business_truck,
            'total_business_driver' => $total_business_driver,
            'total_business_cycle' => $total_business_cycle,
            'total_delay' => $total_delay,
            // 'data_donut' => $data_donut,
            // 'data_bar' => $data_bar
        );
        echo json_encode($output);
    }
}
