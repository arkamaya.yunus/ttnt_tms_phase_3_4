<?php

/*
 * @author: irfan.satriadarma@gmail.com
 * @created: 2019-09-28
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Company extends CI_controller 
{
    function __construct() 
	{
        parent:: __construct();
		
		if ($this->session->userdata(S_USER_EMAIL) == null) 
		{
            redirect('login');
        }
		
        $this->load->model('Shared_model');
        $this->load->model('Company_model');
        $this->load->model('Register_model');
		$this->load->model('Login_model');	
    }

    function index() 
	{
        $data['stitle'] 	= 'Your Company'; 
        $data['jsapp'] = array('company');

        $data['cnt'] = (int) $this->Company_model->get_companies_count();
		
		$this->load->view('header', $data);
		$this->load->view('company');
        $this->load->view('footer');
    }
    
    function get_companies()
    {
        $order = $this->input->post('order');
        $column = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'start' => $this->input->post('start'),
        );

        $start = isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length = isset($_POST['length']) ? intval($_POST['length']) : 25;
        
        $sv = $this->input->post('search')['value'];
        $results = $this->Company_model->get_companies($start, $length, $sv);
        $recordsTotal = (int) $this->Company_model->get_companies_count($sv);

        $data = array();
        foreach ($results as $r) {
            $row = array();
            $row[] = '<a href="' . site_url('company/launch/' . $r->company_id) . '" title="Gunakan Perusahaan" style="font-weight: bold">' . $r->company_name . '</a>';
            $row[] = $r->address;
            $row[] = $r->company_industry;
            $row[] = '<a href="' . site_url('company/id/' . $r->company_id) . '" title="Lihat Perusahaan">Lihat Detil</a>';            
            $data[] = $row;
        }

        $output = array
            (
            "draw" => $def['draw'],
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsTotal,
            "data" => $data
        );
        echo json_encode($output);
    }

    function create()
    {
        if (isset($_POST['company_id'])) 
		{
            $register 			= $this->Register_model->get_register_by_user_email();
            if ($register != false)
		    {
                $trial_days = $this->Shared_model->get_system_value('app_config', 'trial_days', 'system_value_num');
                $trial_days = ($trial_days != '') ? $trial_days : '99';
                $dt 		= date_create(date("Y-m-d H:i:s"));
                date_add($dt, date_interval_create_from_date_string($trial_days . ' years'));
                
                $exp_dt 	= date_format($dt, "Y-m-d H:i:s");

                //insert company
                $company = array
                (
                    'user_email' 				=> $register->email
                    , 'email' 					=> $this->input->post('email', true)
                    , 'company_name' 			=> $this->input->post('company_name', true)
                    , 'company_industry'		=> $this->input->post('company_industry', true)
                    , 'address'		            => $this->input->post('address', true)
                    , 'phone' 					=> $this->input->post('phone', true)
                    , 'fax' 					=> $this->input->post('fax', true)
                    , 'tax_number' 				=> $this->input->post('tax_number', true)
                    , 'website' 				=> $this->input->post('company_website', true)
                    , 'accounting_period_start' => date('Y')."-01-01"
                    , 'accounting_period_end' 	=> date('Y')."-12-31"
                    , 'trial_expired' 			=> $exp_dt
                    , 'package_id' 				=> '1'
                    , 'created_by' 				=> $register->email
                    , 'created_dt' 				=> date('Y-m-d H:i:s')
                );

                $company_id = $this->Register_model->generate_data($company, $register);

                $default_company_assets_dir = S_COMPANY_PATH;
				
				// create neccesary directory
				if (!is_dir($default_company_assets_dir . 'cid'.$company_id)) {
					mkdir($default_company_assets_dir . 'cid' . $company_id, 0777, TRUE);
					copy($default_company_assets_dir . 'index.html', $default_company_assets_dir . 'cid' . $company_id.'/index.html');
				}
				
				$folders_to_create = array(
					'profile'
					, 'profile/thumbs'
					, 'purchase_order'
					, 'purchase_payment'
					, 'purchase_receive'
					, 'purchase_request'
					, 'sales_delivery'
					, 'sales_invoice'
					, 'sales_orders'
					, 'sales_payment'
					, 'sales_quotation'
				);
				
				foreach ($folders_to_create as $f)
				{
					if (!is_dir($default_company_assets_dir . 'cid'.$company_id . '/' . $f)) 
					{
						mkdir($default_company_assets_dir . 'cid' . $company_id . '/' . $f , 0777, TRUE);
						copy($default_company_assets_dir . 'index.html', $default_company_assets_dir . 'cid' . $company_id . '/' . $f . '/index.html');
					}
                }
                                
                $this->LaunchCompany();
            }

            
        }

        $data['jsapp'] = array('company_create');

        $this->load->view('header', $data);
        $this->load->view('company_create');
        $this->load->view('footer');
    }

    function LaunchCompany()
    {
        // langsung loginkan
        //	-------------------------------------------------------------
        $user_data = $this->Login_model->auth_user($this->session->userdata(S_USER_EMAIL));
        if (count($user_data) > 0) 
        {
            $user_data = $user_data[0];
            
            $this->Login_model->last_logged_in($user_data->user_email);
                        
            $employee_name = $user_data->full_name;

            // company info
            $this->session->set_userdata(S_COMPANY_ID		, $user_data->company_id);
            $this->session->set_userdata(S_COMPANY_NAME		, $user_data->company_name);
            $this->session->set_userdata(S_COMPANY_EXPIRED	, $user_data->is_expired);
            
            // user information
            // $this->session->set_userdata(S_USER_EMAIL		, $user_data->user_email);
            $this->session->set_userdata(S_USER_GROUP_ID	, $user_data->user_group_id);
            $this->session->set_userdata(S_USER_IS_ADMIN	, $user_data->is_admin);
            $this->session->set_userdata(S_USER_LANDING		, $user_data->default_landing);
            
            // user - employee information
            $this->session->set_userdata(S_EMPLOYEE_ID		, $user_data->user_email);
            $this->session->set_userdata(S_EMPLOYEE_NAME	, $employee_name);
            $this->session->set_userdata(S_EMPLOYEE_PHOTO	, '');										
        }

        redirect($user_data->default_landing, 'refresh');
    }

    function edit() {

        if (isset($_POST['company_id'])) {
            $this->Company_model->update_company();
			$this->session->set_flashdata('notif_company_success', '<strong>Berhasil.</strong> Perusahaan berhasil diubah.');
			redirect('company/id/' . $_POST['company_id']);
        }

        $company_id = $this->uri->segment(3);
        if ($company_id == '')
            redirect('company');

        $company = $this->Company_model->get_company($company_id);
        if (!$company)
            redirect('company');

        $data['company'] = $company;

        $data['jsapp'] = array('company_edit');
        
        $this->load->view('header', $data);
        $this->load->view('company_edit');
        $this->load->view('footer');
    }

    function id() {
        if (isset($_POST['company_id'])) {
            $this->Company_model->delete($_POST['company_id']);

            // remove directory
            $path= S_COMPANY_PATH . 'cid' . $_POST['company_id'] ;
            $this->load->helper("file"); // load the helper
            delete_files($path, true); // delete all files/folders
            rmdir($path);

            $this->session->set_flashdata('notif_company_success', '<strong>Berhasil.</strong> Perusahaan berhasil dihapus.');
            redirect('company');
        }

        $company_id = $this->uri->segment(3);
        if ($company_id == '')
            redirect('company');

        $company = $this->Company_model->get_company($company_id);
        if (!$company)
            redirect('company');

        $data['company'] = $company;
        $data['jsapp'] = array('company_id');
		
        $this->load->view('header', $data);
        $this->load->view('company_id');
        $this->load->view('footer');
        
    }

    function launch()
    {
        $company_id = $this->uri->segment(3);
        if ($company_id == '')
            redirect('company');

        $company = $this->Company_model->get_company($company_id);
        if (!$company)
            redirect('company');

        $this->Company_model->switchCompany($company_id);
        $this->LaunchCompany();
    
    }

    function changepwd()
    {
        $cpwd_old = $_POST['cpwd_old'];
        $cpwd_new = $_POST['cpwd_new'];

        $success = false;
        $message = '';
        
        // get user
        $cek = $this->db->query("
            select
                user_email
            from 
                tb_m_users 
            where 
                user_email = '" . $this->session->userdata(S_USER_EMAIL) . "'
                and user_password = '" . md5($cpwd_old) . "'
        ")->result();

        if (count($cek) > 0)
        {
            // update password
            $user['user_password'] = md5($cpwd_new);
            $this->db->where('user_email', $this->session->userdata(S_USER_EMAIL));
            $this->db->update('tb_m_users', $user);

            $success = true;
            $message = 'Success. Password already changed.';
        }
        else
        {
            $message = 'Warning. Wrong old password';
        }
        
        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }
}
?>