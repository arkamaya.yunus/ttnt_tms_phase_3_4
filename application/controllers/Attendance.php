<?php

/*
 * @author: Rakhman@arkamaya.co.id
 * @created: 12 Mei 2020
 */

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Attendance extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->load->model('Attendance_model');
	}


	public function getIpAddress()
	{
		$appId = $this->input->get("appid", true);

		$result = $this->Attendance_model->getIpAddress($appId);
		if ($result) {
			$resultIp = "";
			foreach ($result as $key => $value) {

				$resultIp = $resultIp . $value['system_code'] . '~' . $value['system_value_txt'] . ";";
			}
			echo $resultIp;
			//echo $result->system_code .";". $result->system_value_txt;
		} else {
			echo 0;
		}
	}

	public function getAutoReconTime()
	{

		$result = $this->Attendance_model->getAuroReconTime();
		if ($result) {
			$resultIp = "";
			foreach ($result as $key => $value) {
				$resultIp = $resultIp  . $value['system_value_txt'] ;
			}
			echo $resultIp;
			//echo $result->system_code .";". $result->system_value_txt;
		} else {
			echo 0;
		}
	}

	public function getLog()
	{

		$appId = $this->input->get("appid", true);

		$result = $this->Attendance_model->getLog($appId);
		if ($result) {
			$resultIp = "";
			foreach ($result as $key => $value) {
				//DateTime, app_id, ipaddress, message,status
				$resultIp = $resultIp . $value['dttm'] . '*' . $value['app_id'] . '*' . $value['ipaddress'] . '*' . $value['message'] . '*' . $value['STATUS'] . ";";
			}
			echo $resultIp;
			//echo $result->system_code .";". $result->system_value_txt;
		} else {
			echo 0;
		}
	}

	function dupes($array, $d,$in) {
		$res = false;
		foreach ($array as $val) {
			if (( $val['driver_cd'] == $d) && ($val['clock_in'] == $in)){
				
				$res= true;
			}
		}
		
		if ($this->Attendance_model->cekAttendance($d,$in)>0){
			$res= true;
		}

		return $res;
	}

	public function addLog(){
		//"?appid=113&ipaddress=192.168.4.16&message=Host unreachable&status=Error"
	
		$log = array(
			'datetime' => date('Y-m-d H:i:s'),
			'app_id' => $this->input->get("appid", true),
			'ipaddress' => $this->input->get("ipaddress", true),
			'message' => $this->input->get("message", true),
			'status' => $this->input->get("status", true),
		);

		$this->Attendance_model->saveLog($log);
	}

	public function pushData()
	{
		date_default_timezone_set('Asia/Jakarta');
		
		$data = $this->input->post("data", true);
		$audit = $this->Attendance_model->getSystemMaster("attendance", "log")->system_value_txt;
		
		if ($data != '') {

			if ($audit=="1"){
				$log = array(
					'datetime' => date('Y-m-d H:i:s'),
					'app_id' => "",
					'ipaddress' => "",
					'message' => $data,
					'status' => "",
				);
		
				$this->Attendance_model->saveLog($log);
			}

			$arr = explode(";", $data);

			$currentDate = date('Y-m-d H:i:s');
			//$processId = date('YmdHis');

			$max = sizeof($arr);

			$insertIn = array();
			$updateOut = array();

			for ($i = 0; $i < $max; $i++) {
				$d = explode("~", $arr[$i]);
				//echo $d[3];
				$inOut = $this->Attendance_model->getSystemMaster("attendance", $d[3])->system_value_txt;

				$driverCd = $this->Attendance_model->getDriverCd($d[0]);
				

				if ($driverCd) {
					if ($inOut == "in") {
						// cek dl ada duplicate ga di array in
						if ($this->dupes($insertIn,$driverCd->driver_cd, $d[1] ) == false){
							
							// cek dulu duplicate gak? klo gak, masukin ke array
							$row = array(
								'driver_cd' 		=> $driverCd->driver_cd, 
								'attendance_dt' 	=> $d[1], 
								'clock_in'			=> $d[1], 
								'source' 			=> $d[3], 
								'created_by'		=> $d[3], 
								'created_dt'		=> $currentDate
							);

							$cek = $this->Attendance_model->cekDuplicate($driverCd->driver_cd, substr($d[1], 0, 10));
							// 1. jika blm ada data finger di hari ini, insert ke tb_r_attendance (clockin)
							// 2. jika sudah ada data finger di hari ini dan clockout nya sudah terisi dan rollcall_id is not null and rollcall_id <> -1, 
							// maka boleh insert lagi ke tb_r_attendance (clockin - yg kedua )dst. <-- Berarti ini kasus dia harus jalan 2 kali di hari yg sama.

							if (count($cek) > 0) { // ini artinya duplicate
								// klo ada duplicate, cek rollcall_id -1 gak? klo == -1, update set clock_in = data baru, rollcall_id = null
								if (($cek[0]['rollcall_id'] == '-1') && ($cek[0]['clock_out'] == null)) {
									//numpang array $updateOut // buat update data nya
									$newRow = array(
										'driver_cd' 		=> $driverCd->driver_cd, 
										'attendance_id' 	=> $cek[0]['attendance_id'], 
										'attendance_dt' 	=> $cek[0]['attendance_dt'], 
										'clock_in'			=> $d[1], 
										'rollcall_id' 		=> null, 
										'changed_by'		=> $d[3], 
										'changed_dt'		=> $currentDate
									);

									$updateOut[] = $newRow;
								} elseif (($cek[0]['rollcall_id'] != null) && ($cek[0]['clock_out'] != null) && ($cek[0]['rollcall_id'] != '-1')) {
									// insert attendance baru
									$insertIn[] = $row;
								}
								// klo rollcall_id != -1, ignore, jgn di apa"in

							} else {
								$insertIn[] = $row;
							}
						}

					} else {
						//echo "out";

						$att = $this->Attendance_model->getAttId($driverCd->driver_cd);
						if ($att) {
							$row = array(
								'driver_cd' 		=> $driverCd->driver_cd, 
								'rollcall_id' 		=> $att->rollcall_id, 
								'attendance_id' 	=> $att->attendance_id, 
								'attendance_dt' 	=> $att->attendance_dt, 
								'clock_out' 		=> $d[1], 
								'changed_by'		=> $d[3], 
								'changed_dt'		=> $currentDate

							);

							$updateOut[] = $row;
						}
					}
				}
			}

			if (count($insertIn) > 0) {
				$this->Attendance_model->insertAttendanceQueue($insertIn);
			}

			if (count($updateOut) > 0) {

				$this->Attendance_model->updateAttendanceQueue($updateOut);
			}

			echo "1";
		} else {
			echo "0";
		}
	}
}
