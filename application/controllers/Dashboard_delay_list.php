<?php

/*
* @author: irfan.satriadarma@gmail.com_addref
* @created: 06 Maret 2020
*/

class Dashboard_delay_list extends MY_controller 
{
    var $menu = array();

    function __construct() 
	{
        parent:: __construct();

        $this->load->model('Dashboard_model');
		
        if ($this->session->userdata(S_COMPANY_ID) == null) 
		{
            redirect('login');
        }
    }

    function index() 
	{
        date_default_timezone_set('Asia/Jakarta');

		$data['stitle'] 	= 'Dashboard Delay List';
        $data['jsapp'] = array('dashboard_chart','dashboard_delaylist');
        $data['customer_lp_cds'] = $this->Dashboard_model->getCustomerLpCds();

        // $data['business'] = $this->db->query("
        //     select system_code, system_value_txt from tb_m_system where system_type = 'business' order by system_value_num
        // ")->result();
						
		$this->load->view('header', $data);
        $this->load->view('dashboard_delay_list');        
		$this->load->view('footer');
    }

    function getDepartureDelay()
    {        
        $customer_lp_cd = $this->input->post('customer_lp_cd', true);        
        $timetable_dt = date('Y-m-d', strtotime( str_replace('/', '-', $this->input->post('timetable_dt', true))));
		
        $resultDeparture = $this->Dashboard_model->getDepartureDelay($customer_lp_cd, $timetable_dt);
        $resultArrival = $this->Dashboard_model->getArrivalDelay($customer_lp_cd, $timetable_dt);
		        
        $dataDeparture = array();
		foreach ($resultDeparture as $r) 
		{
            $row = array();			
            
            $row[] = $r->customer_lp_cd;
            $row[] = $r->route;
            $row[] = $r->cycle;
            $row[] = md5($r->timetable_id);
            $row[] = $r->lp_cd;
            $row[] = $r->vehicle_cd;
            $row[] = $r->driver_name;
            $row[] = $r->delay;
            $row[] = ($r->remark != '' && $r->remark != null) ? $r->remark : '-';
			$dataDeparture[] = $row;
        }

        $dataArrival = array();
        foreach ($resultArrival as $r) 
		{
            $row = array();			
            
            $row[] = $r->customer_lp_cd;
            $row[] = $r->route;
            $row[] = $r->cycle;
            $row[] = md5($r->timetable_id);
            $row[] = $r->lp_cd;
            $row[] = $r->vehicle_cd;
            $row[] = $r->driver_name;
            $row[] = $r->delay;
            $row[] = ($r->remark != '' && $r->remark != null) ? $r->remark : '-';
			$dataArrival[] = $row;
        }

        // // where Customer lp
        // $wlp = ($customer_lp_cd != 'all') ? " and b.customer_lp_cd = '" . $customer_lp_cd . "'" : '';
        // $wlp_a = ($customer_lp_cd != 'all') ? " and a.customer_lp_cd = '" . $customer_lp_cd . "'" : '';

        // // total planning
        // $sqlPlanning = "
        //     SELECT COUNT(a.timetable_detail_id) as cnt FROM tb_r_timetable_detail a
        //     INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
        //     WHERE a.timetable_dt = '".$timetable_dt."' " . $wlp . "   
        // ";
        // $total_planning = $this->db->query($sqlPlanning)->row()->cnt;

        // // total arrival done
        // $sqlArrival = "
        //     SELECT COUNT(a.timetable_detail_id) as cnt FROM tb_r_timetable_detail a
        //     INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
        //     WHERE a.timetable_dt = '".$timetable_dt."' and a.arrival_actual is not null " . $wlp . "   
        // ";
        // $total_arrival = $this->db->query($sqlArrival)->row()->cnt;

        // // total truck on-duty
        // $sqlTruck = "
        //     SELECT DISTINCT a.vehicle_cd, a.vehicle_number, c.vehicle_owner FROM tb_r_timetable_detail a
        //     INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
        //     inner join tb_m_vehicle c on c.vehicle_cd = a.vehicle_cd
        //     WHERE a.timetable_dt = '".$timetable_dt."' " . $wlp . "
        //     ORDER BY vehicle_cd;
        // ";
        // $data_truck = $this->db->query($sqlTruck)->result();
        // $total_truck = count($data_truck);
        
        // // total driver on-duty
        // $sqlDriver = "
        //     SELECT DISTINCT a.driver_cd, a.driver_name, c.logistic_partner FROM tb_r_timetable_detail a
        //     INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
        //     INNER JOIN tb_m_driver c ON c.driver_cd = a.driver_cd
        //     WHERE a.timetable_dt = '".$timetable_dt."' " . $wlp . "
        //     ORDER BY a.driver_name;
        // ";
        // $data_driver = $this->db->query($sqlDriver)->result();
        // $total_driver = count($data_driver);

        // // donut
        // $sqlDonut = "
        //     SELECT 
        //         a.business
        //         , COUNT(b.timetable_detail_id) AS total
        //     FROM 
        //         tb_r_timetable a 
        //         INNER JOIN tb_r_timetable_detail b ON b.timetable_id = a.timetable_id
        //     WHERE a.timetable_dt = '".$timetable_dt."' " . $wlp_a . "
        //     GROUP BY
        //         a.business
        // ";
        // $data_donut = $this->db->query($sqlDonut)->result();

        // // bar
        // $sqlBar = "
        //     SELECT 
        //             t.customer_lp_cd
        //         , SUM(total) AS total
        //         , SUM(done) AS done
        //         , SUM(total) - SUM(done) AS notyet
        //     FROM
        //     (
        //     SELECT 
        //         a.timetable_id 
        //         , a.business
        //         , a.customer
        //         , a.customer_lp_cd
        //         , a.timetable_dt
        //         , COUNT(b.timetable_detail_id) AS total
        //         , 0 AS done
        //     FROM 
        //         tb_r_timetable a 
        //         INNER JOIN tb_r_timetable_detail b ON b.timetable_id = a.timetable_id
        //     WHERE a.timetable_dt = '".$timetable_dt."' " . $wlp_a . "
        //     GROUP BY
        //         a.timetable_id
            
        //     UNION ALL
            
        //     SELECT 
        //         a.timetable_id 
        //         , a.business
        //         , a.customer
        //         , a.customer_lp_cd
        //         , a.timetable_dt
        //         , 0 AS total
        //         , COUNT(b.timetable_detail_id) AS done	
        //     FROM 
        //         tb_r_timetable a 
        //         INNER JOIN tb_r_timetable_detail b ON b.timetable_id = a.timetable_id
        //     WHERE a.timetable_dt = '".$timetable_dt."' " . $wlp_a . "
        //         AND b.arrival_actual IS NOT null
        //     GROUP BY
        //         a.timetable_id
        //     ) t
        //     GROUP BY t.business, t.customer, t.customer_lp_cd
        //     ORDER BY t.business, t.customer, t.customer_lp_cd, t.timetable_dt
        // ";
        // // echo $sqlBar;
        // $data_bar = $this->db->query($sqlBar)->result();

        $output = array(
            'departure' => $dataDeparture,
            'arrival' => $dataArrival,
            // 'total_planning' => $total_planning,
            // 'total_arrival' => $total_arrival,
            // 'total_truck' => $total_truck,
            // 'data_truck' => $data_truck,
            // 'total_driver' => $total_driver,
            // 'data_driver' => $data_driver,
            // 'data_donut' => $data_donut,
            // 'data_bar' => $data_bar
        );
        echo json_encode($output);
    }    
}