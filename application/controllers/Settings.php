<?php
/*
* @author: eri.safari@arkamaya.co.id
* @created: 2019-08-07
*
* #TODO
* - Handle Setting Company	 (Create,update)
* - Handle Setting Sales
* - Handle Setting Purchase
* - Handle setting Items
* - Handle setting Account
* - Handle setting Users
* - Handle setting Billing
* -
*/

if (! defined('BASEPATH'))
    exit ('No direct script access allowed');

class Settings extends MY_controller{

    function __construct(){
        parent:: __construct();
        $this->load->model('company_model');
		$this->load->model('customer_model');
		$this->load->model('item_model');
        $this->load->model('settings_model');
		$this->load->library('user_agent');

		if ($this->session->userdata(S_COMPANY_ID) == null)
		{
			redirect('login');
		}
    }

    function index(){


        $data['jsapp']      = array('settings_company');
        $data['stitle']    = 'Settings';

        // get data company by id
        $data['company'] = $this->settings_model->get_company_id();

        $url_settings = $this->uri->segment('1');
        $data['menu'] = $this->settings_model->create_menu_pengaturan($url_settings);

        $this->load->view('header',$data);
        $this->load->view('settings_company');
        $this->load->view('footer');
    }

    function roles()
    {
        if (isset($_POST['user_group_description']))
        {
            $data['company_id'] = $this->session->userdata(S_COMPANY_ID);
            $data['user_group_description'] = $_POST['user_group_description'];
            $data['default_landing'] = 'dashboard_summary';
            $this->db->insert('tb_m_user_group', $data);
            $this->session->set_flashdata('notif_success', '<strong>Success.</strong> New Role created.');
            redirect('settings/roles');
        }

        if (isset($_POST['user_group_id_delete']))
        {
            $user_group_id = $_POST['user_group_id_delete'];

            $this->db->where('user_group_id', $user_group_id);
            $this->db->delete('tb_m_user_group_auth');

            $this->db->where('user_group_id', $user_group_id);
            $this->db->delete('tb_m_user_group');

            $this->session->set_flashdata('notif_success', '<strong>Success.</strong> Role deleted.');
            redirect('settings/roles');
        }


        $data['stitle']     = 'Roles';
        $url_settings = $this->uri->segment('1')."/".$this->uri->segment('2');
        $data['stitle']    = 'Roles Setting';
        $data['jsapp']      = array('settings_roles');

        $data['menu'] = $this->settings_model->create_menu_pengaturan($url_settings);

        $view = '';

        $id = $this->uri->segment(4);
        if ($id != '')
        {
            if (isset($_POST['user_group_description_edit']))
            {
                $user_group_id = $_POST['user_group_id'];
                $is_admin = $_POST['is_admin'];

                if (isset($_POST['uga']))
                {
                    $uga = $_POST['uga'];

                    $rows = array();
                    foreach ($uga as $u)
                    {
                        $ux = explode('~', $u);
                        $function_id = $ux[0];
                        $feature_id = $ux[1];

                        $feature = array(
                            'user_group_id' => $user_group_id,
                            'function_id' => $function_id,
                            'feature_id' => $feature_id
                        );
                        $rows[] = $feature;
                    }

                    // HACK for Admin only - user_group_id = '2'
                    if ($is_admin == '1')
                    {
                        $rows[] = array(
                            'user_group_id' => $user_group_id,
                            'function_id' => '171',
                            'feature_id' => '17101'
                        );
                        $rows[] = array(
                            'user_group_id' => $user_group_id,
                            'function_id' => '172',
                            'feature_id' => '17201'
                        );
                        $rows[] = array(
                            'user_group_id' => $user_group_id,
                            'function_id' => '173',
                            'feature_id' => '17301'
                        );
                    }

                    $this->db->where('user_group_id', $user_group_id);
                    $this->db->delete('tb_m_user_group_auth');

                    $this->db->insert_batch('tb_m_user_group_auth', $rows);
                }

                // update description if not empty
                $user_group_description = $_POST['user_group_description_edit'];
                if ($user_group_description != '')
                {
                    $role['user_group_description'] = $user_group_description;
                    $this->db->where('user_group_id', $user_group_id);
                    $this->db->update('tb_m_user_group', $role);
                }

                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> Authorization Updated.');
                redirect('settings/roles');
            }


            $role = $this->db->query("select * from tb_m_user_group where md5(user_group_id) = '" . $id . "'")->result();
            $data['role'] = $role[0];

            $view = 'settings_roles_id';
        }
        else
        {
            // get data roles
            $data['roles'] = $this->db->query("
                SELECT
                    a.user_group_id, a.user_group_description, a.is_admin, a.default_landing
                    , COUNT(b.user_email) AS num_of_users
                FROM
                    tb_m_user_group a
                LEFT JOIN tb_m_users b ON b.user_group_id = a.user_group_id
                GROUP BY a.user_group_id
                order by user_group_description
            ")->result();

            $view = 'settings_roles';

        }

        $this->load->view('header',$data);
        $this->load->view($view);
        $this->load->view('footer');
    }

    function users()
    {
        if (isset($_POST['user_email_delete']))
        {
            $user_email_delete = $_POST['user_email_delete'];
            $this->db->where('user_email', $user_email_delete);
            $this->db->delete('tb_m_users');

            $this->session->set_flashdata('notif_success', '<strong>Success.</strong> User deleted.');
            redirect('settings/users');
        }

        if (isset($_POST['user_email']))
        {
            $user_email = $_POST['user_email'];
            $full_name = $_POST['full_name'];
            $user_group_id = $_POST['user_group_id'];
            $user_password = $_POST['user_password'];



            if ($_POST['act'] == 'create')
            {
                $user['user_group_id'] = $user_group_id;
                $user['full_name'] = $full_name;
                $user['user_email'] = $user_email;
                $user['user_password'] = md5($user_password);
                $user['company_id'] = $this->session->userdata(S_COMPANY_ID);
                $user['email_confirmed'] = '1';

                $this->db->insert('tb_m_users', $user);
                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> New user created.');

            }
            else if ($_POST['act'] == 'edit')
            {
                $user['user_group_id'] = $user_group_id;
                $user['full_name'] = $full_name;

                $this->db->where('user_email', $user_email);
                $this->db->update('tb_m_users', $user);
                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> User updated.');

            }
            else if ($_POST['act'] == 'reset')
            {
                $user['user_password'] = md5($user_password);
                $this->db->where('user_email', $user_email);
                $this->db->update('tb_m_users', $user);
                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> Password Has been Reset.');
            }

            redirect('settings/users');
        }

        $data['stitle'] = 'Users Setting';
        $data['jsapp']      = array('settings_users');
        $url_settings = $this->uri->segment('1')."/".$this->uri->segment('2');
        $data['menu'] = $this->settings_model->create_menu_pengaturan($url_settings);


        $data['users'] = $this->db->query("
            select a.user_email, a.full_name, a.super_admin,
                a.user_group_id, b.user_group_description, b.is_admin
            from
                tb_m_users a inner join tb_m_user_group b on b.user_group_id = a.user_group_id
            order by
                a.user_group_id, a.full_name
        ")->result();

        // get data roles
        $data['roles'] = $this->db->query("
            SELECT
                a.user_group_id, a.user_group_description
            FROM
                tb_m_user_group a
            order by a.user_group_description
        ")->result();

        $this->load->view('header',$data);
        $this->load->view('settings_users');
        $this->load->view('footer');
    }

    function machine(){
      //create & edit
      if (isset($_POST['ip']))
      {
          $ip = $_POST['ip'];
          $mesin = $_POST['mesin'];
          $app_id = $_POST['app_id'];

          if ($_POST['act'] == 'create')
          {
              $create_machines['system_code'] = $ip;
              $create_machines['system_value_txt'] = $mesin;
              $create_machines['system_value_num'] = $app_id;
              $create_machines['system_type'] = 'attendance';

              $this->db->insert('tb_m_system', $create_machines);
              $this->session->set_flashdata('notif_success', '<strong>Success.</strong> New machine created.');
          }
          else if ($_POST['act'] == 'edit')
          {

              $update_machines['system_value_txt'] = $mesin;
              $update_machines['system_value_num'] = $app_id;

              $this->db->where('system_type', 'attendance');
              $this->db->where('system_code', $ip);
              $this->db->update('tb_m_system', $update_machines);
              $this->session->set_flashdata('notif_success', '<strong>Success.</strong> machine updated.');
          }

          redirect('settings/machine');
      }
      //delete
      if (isset($_POST['machine_delete']))
      {
          $machine_delete = $_POST['machine_delete'];
          $this->db->where('system_type', 'attendance');
          $this->db->where('system_code', $machine_delete);
          $this->db->delete('tb_m_system');

          $this->session->set_flashdata('notif_success', '<strong>Success.</strong> machine deleted.');
          redirect('settings/machine');
      }

      $data['stitle'] = 'Users Setting';
      $data['jsapp']      = array('settings_machine');
      $url_settings = $this->uri->segment('1')."/".$this->uri->segment('2');
      $data['menu'] = $this->settings_model->create_menu_pengaturan($url_settings);
      $data['machines'] = $this->settings_model->getMasters('attendance');

      // get data roles
      $data['roles'] = $this->db->query("
          SELECT
              a.user_group_id, a.user_group_description
          FROM
              tb_m_user_group a
          order by a.user_group_description
      ")->result();

      $this->load->view('header',$data);
      $this->load->view('settings_machine');
      $this->load->view('footer');
    }

    function create_company(){
        if(isset($_POST)){
            $nama_files = $this->_do_upload('logo');
            if($_FILES['logo']['size'] > 0){
                if($nama_files == $this->upload->display_errors() ){
                    $this->session->set_flashdata('notif_company_uploads_error', '<strong>Gagal.</strong> '.$this->upload->display_errors().'.');
                    redirect('settings');
                }else{
                    $data_company = $this->settings_model->save_company($nama_files);
                    $this->session->set_flashdata('notif_company_success', '<strong>Berhasil.</strong> Informasi Perusahaan berhasil disimpan.');
                    redirect('settings');
                }
            }else
            {
                $data_company = $this->settings_model->save_company($nama_files);
                $this->session->set_flashdata('notif_company_success', '<strong>Berhasil.</strong> Informasi Perusahaan berhasil disimpan.');
                redirect('settings');
            }
        }
    }

    function _do_upload($nama_file) {
        $company_id = $this->session->userdata(S_COMPANY_ID);
        $msg = '';
		if (!is_dir(FCPATH.'/assets/files/cid'.$company_id.'/profile')) {
			mkdir(FCPATH.'/assets/files/cid'.$company_id.'/profile',0755, TRUE);
		}
		$config['upload_path'] = './assets/files/cid'.$company_id.'/profile';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['max_size']	= '100000';

		$this->load->library('upload', $config);

		if(!$this->upload->do_upload($nama_file)){
            $msg = $this->upload->display_errors();
        }else{
            $data = $this->upload->data($nama_file);
            $image_data = $this->upload->data();

            $msg = $image_data['file_name'];
        }

		return $msg;

    }

    public function checkDuplicateIp($ip){
      $data = $this->settings_model->checkDuplicateIp($ip);
      echo json_encode($data);
    }


}

?>
