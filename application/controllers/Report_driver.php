<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 06 Maret 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report_driver extends MY_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->model('Report_driver_model');
        $this->load->model('Timetable_model');
        $this->load->library('Zend');

        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index()
    {
      date_default_timezone_set('Asia/Jakarta');

      $data['stitle'] = 'Report Driver';
      $data['today'] = date('d/m/Y');
      $data['jsapp'] = array('report_driver');

      $this->load->view('header', $data);
      $this->load->view('report_driver');
      $this->load->view('footer');
    }

    function gets()
    {
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;

        $sv		    = $this->input->post('search')['value'];
        $departure_dt_s = $this->input->post('departure_dt_s', true);
        $departure_dt_e = $this->input->post('departure_dt_e', true);

        $results 			= $this->Report_driver_model->getRollcall(
                              $start, $length, $sv, $order, $columns, $departure_dt_s, $departure_dt_e
                            );
        $recordsTotal       = (int)$this->Report_driver_model->getCountOfRollcall(
                                $departure_dt_s, $departure_dt_e, $sv
                            );

        $data = array();
    foreach ($results as $r)
    {
            $row = array();

            $row[] = $r->rollcall_id;
            $row[] = $r->voucher;
            $row[] = $r->driver_cd;
            $row[] = $r->driver_name;
            $row[] = $r->vehicle_cd;
            $row[] = $r->route;
            $row[] = $r->cycle;
            $row[] = $r->departure_plan;
            $row[] = '<a href="'.site_url('rollcall/print/'.$r->rollcall_id).'" target="_blank" class=""><span class="badge badge-primary">Print</span></a>';

      $data[] = $row;
        }

        $output = array
    (
      "draw" => $def['draw'],
      "recordsTotal" => $recordsTotal,
      "recordsFiltered" => $recordsTotal,
      "data" => $data
    );
        echo json_encode($output);
    }
}
