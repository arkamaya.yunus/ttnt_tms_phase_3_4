<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 06 Maret 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report_timetable_actual extends MY_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->helper('common');
        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index()
    {
        date_default_timezone_set('Asia/Jakarta');
        
        $timetable_dt = date('Y-m-d');
        $this->id($timetable_dt);
    }

    function id($timetable_dt) 
    {

        if ($timetable_dt == 'undefined-undefined-') redirect('report_driver');

        $data['stitle'] = 'Timetable Actual - Report';
        $data['jsapp'] = array('report_timetable_actual');

        $data['business'] = $this->db->query("
            SELECT DISTINCT business FROM tb_r_timetable WHERE timetable_dt = '" . $timetable_dt . "';
        ")->result();

        $data['customers'] = $this->db->query("
            SELECT DISTINCT a.customer, a.business, b.system_value_txt FROM tb_r_timetable a
            INNER JOIN tb_m_system b ON b.system_code = a.customer AND b.system_type = 'customer'
            WHERE timetable_dt = '" . $timetable_dt . "' ORDER BY a.customer
        ")->result();

        $data['customer_lp_cds'] = $this->db->query("
            SELECT DISTINCT customer_lp_cd, customer, business FROM tb_r_timetable 
            WHERE timetable_dt = '" . $timetable_dt . "' ORDER BY customer_lp_cd;
        ")->result();

        $data['routes'] = $this->db->query("
            SELECT 
                DISTINCT a.route, b.business, b.customer, b.customer_lp_cd
            FROM 
                tb_r_timetable_detail a
                INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
            WHERE 
                a.timetable_dt = '" . $timetable_dt . "'
            ORDER BY a.route
        ")->result();

        $data['cycles'] = $this->db->query("
            SELECT 
                DISTINCT a.cycle, a.route, b.business, b.customer, b.customer_lp_cd
            FROM 
                tb_r_timetable_detail a
                INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
            WHERE 
                a.timetable_dt = '" . $timetable_dt . "'
            ORDER BY b.business, b.customer, b.customer_lp_cd, a.route, a.cycle
        ")->result();

        $data['timetable_dt'] = $timetable_dt;

        $this->load->view('header', $data);
        $this->load->view('report_timetable_actual');
        $this->load->view('footer');
    }    

    function download($timetable_dt, $business, $customer, $customer_lp_cd, $route, $cycle)
    {
        ini_set('memory_limit', '-1');
        
        // get the data :D
        $w_business = ($business != 'all') ? " and b.business = '" . $business . "'" : '';
        $w_customer = ($customer != 'all') ? " and b.customer = '" . $customer . "'" : '';
        $w_customer_lp_cd = ($customer_lp_cd != 'all') ? " and b.customer_lp_cd = '" . urldecode($customer_lp_cd) . "'" : '';
        $w_route = ($route != 'all') ? " and a.route = '" . $route . "'" : '';
        $w_cycle = ($cycle != 'all') ? " and a.cycle = '" . $cycle . "'" : '';

        $sql = "
            select a.*, b.business, b.customer, c.smartphone from 
                tb_r_timetable_detail a 
                inner join tb_r_timetable b on b.timetable_id = a.timetable_id
                inner join tb_m_driver c on c.driver_cd = a.driver_cd
            where
                a.timetable_dt = '" . $timetable_dt . "'
                -- and a.arrival_plan is not null
                -- and a.departure_plan is not null
                " . $w_business . $w_customer . $w_customer_lp_cd . $w_route . $w_cycle . "
            order by a.route, a.cycle, a.arrival_plan
        ";

        $data = $this->db->query($sql)->result();
        if (count($data) == 0)
        {            
            $this->session->set_flashdata('notif_error', '<strong>Error.</strong> Timetable Not Found.');
            redirect('report_timetable_actual/id/' . $timetable_dt, 'refresh');
        }

        $this->load->library('Excel');
		
		$tpl_path = './assets/files/template-timetable_actual-new.xlsx';
		
		//$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($tpl_path);
		$sheet = $objPHPExcel->getActiveSheet();
		$sheet->setTitle('Truck');
		
		// create data..
        $row = 9;
        
        $sheet->setCellValue('B2', 'Daily Report Delivery ' . $data[0]->business . ' ' . $data[0]->customer);        
        $sheet->setCellValue('B6', urldecode($customer_lp_cd));
        $sheet->setCellValue('D4', date('d-m-Y', strtotime($timetable_dt)));
		
        $i = 1;
        $key = '';
        $lastKnownRow = 0;
        foreach($data as $d) {

            $key_temp = $d->route . $d->cycle . $d->vehicle_cd . $d->driver_cd;

            if ($key != $key_temp)
            {
                $sheet->setCellValue('B'.$row, $i);
                $sheet->setCellValue('C'.$row, $d->route);
                $sheet->setCellValue('D'.$row, $d->cycle);
                $sheet->setCellValue('E'.$row, $d->vehicle_cd);
                $sheet->setCellValue('G'.$row, $d->driver_name);
                $sheet->setCellValue('H'.$row, $d->smartphone);               

                // merge cells row before
                if ($i > 1)
                {
                    $rowUntil = $row - 1;
                    $sheet->mergeCells('B' . $lastKnownRow.':B' . $rowUntil);
                    $sheet->mergeCells('C' . $lastKnownRow.':C' . $rowUntil);
                    $sheet->mergeCells('D' . $lastKnownRow.':D' . $rowUntil);
                    $sheet->mergeCells('E' . $lastKnownRow.':E' . $rowUntil);
                    $sheet->mergeCells('F' . $lastKnownRow.':F' . $rowUntil);
                    $sheet->mergeCells('G' . $lastKnownRow.':G' . $rowUntil);
                    $sheet->mergeCells('H' . $lastKnownRow.':H' . $rowUntil);
                }

                $lastKnownRow = $row;
                
                $key = $key_temp;
                $i++;
            }

            // border-thin
            $sheet->getStyle("B" . $row . ":W" . $row)->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                )
            );

            // border-dash ~ only in customer_lp, plan-actual
            $styleArray = array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_DOTTED,
                        'color' => array('argb' => '000000'),
                    ),
                ),
            );            
            $sheet->getStyle("I" . $row . ":I" . $row)->applyFromArray($styleArray);
            $sheet->getStyle("L" . $row . ":M" . $row)->applyFromArray($styleArray);
            $sheet->getStyle("Q" . $row . ":R" . $row)->applyFromArray($styleArray);            

            // background-color kuning kalau lp_cd sama dengan customer_lp_cd
            if ($d->lp_cd == urldecode($customer_lp_cd))
            {
                $sheet->getStyle('I'.$row)->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'FFFF00')
                        )
                    )
                );    
            }

			$sheet->setCellValue('I'.$row, $d->lp_cd);
            
            $sheet->setCellValue('L'.$row, substr($d->arrival_plan, 0,5));
            $arrival_actual = ($d->arrival_actual != '') ? date('H:i:s', strtotime($d->arrival_actual)) : '';
			$sheet->setCellValue('M'.$row, substr($arrival_actual, 0, 5));
            
            $agap = '';
            if ($d->arrival_gap != '')
            {
                if ($d->arrival_actual > ($d->arrival_dt . ' ' .$d->arrival_plan))
                {
                    $agap = get_time_difference($d->arrival_plan, date('H:i:s', strtotime($d->arrival_actual)));
                }
                else
                {
                    $agap = '-' . get_time_difference(date('H:i:s', strtotime($d->arrival_actual)), $d->arrival_plan);
                }                
            }            
            $sheet->setCellValue('N'.$row, $agap);
            $sheet->setCellValue('O'.$row, $d->arrival_eval);
            $sheet->setCellValue('P'.$row, $d->arrival_problem);

            $sheet->setCellValue('Q'.$row, substr($d->departure_plan, 0, 5));
            $departure_actual = ($d->departure_actual != '') ? date('H:i:s', strtotime($d->departure_actual)) : '';
            $sheet->setCellValue('R'.$row, substr($departure_actual, 0, 5));
            
            $dgap = '';
            if ($d->departure_gap != '')
            {
                if ($d->departure_actual > ($d->departure_dt . ' ' .$d->departure_plan))
                {
                    $dgap = get_time_difference($d->departure_plan, date('H:i:s', strtotime($d->departure_actual)));
                }
                else
                {
                    $dgap = '-' . get_time_difference(date('H:i:s', strtotime($d->departure_actual)), $d->departure_plan);
                }
                
            }
            $sheet->setCellValue('S'.$row, $dgap);
            
            $sheet->setCellValue('T'.$row, $d->departure_eval);
            $sheet->setCellValue('U'.$row, $d->departure_problem);

            $sheet->setCellValue('V'.$row, $d->remark);
            $sheet->setCellValue('W'.$row, $d->division);
            $row++;            
        }

        // merge akhir loop
        $rowUntil = $row - 1;
        $sheet->mergeCells('B' . $lastKnownRow.':B' . $rowUntil);
        $sheet->mergeCells('C' . $lastKnownRow.':C' . $rowUntil);
        $sheet->mergeCells('D' . $lastKnownRow.':D' . $rowUntil);
        $sheet->mergeCells('E' . $lastKnownRow.':E' . $rowUntil);
        $sheet->mergeCells('F' . $lastKnownRow.':F' . $rowUntil);
        $sheet->mergeCells('G' . $lastKnownRow.':G' . $rowUntil);
        $sheet->mergeCells('H' . $lastKnownRow.':H' . $rowUntil);

        // border-Outline
        $styleArray2 = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );
        $rowAkhir = $row-1;
        $sheet->getStyle("B7:W" . $rowAkhir)->applyFromArray($styleArray2);
        
        $customer_lp_cd = ($customer_lp_cd != 'all') ? '-' . $customer_lp_cd : '';
        $route = ($route != 'all') ? '-' . $route : '';
        $cycle = ($cycle != 'all') ? '-' . $cycle : '';
				
		$file_name = "Timetable_Actual_". date('Ymd', strtotime($timetable_dt)) .  $customer_lp_cd . $route . $cycle . '-' . date('YmdHis').".xlsx";
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment;filename=$file_name");
		header("Cache-Control: max-age=0");
		$objWriter= PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

		unset($sheet, $objPHPExcel);
    }
}