<?php

/*
 * @author: irfan.satriadarma@gmail.com
 * @created: 2016-12-19
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_controller 
{
    function __construct() 
	{
        parent:: __construct();
        $this->load->model('Login_model');
    }

    function index() 
	{
        if ($this->session->userdata(S_COMPANY_ID) != null) 
		{
            $url = ($this->session->userdata(S_USER_LANDING) == '') ? 'company' : $this->session->userdata(S_USER_LANDING);
            redirect($url, 'refresh');
        }
		else
		{
			if (isset($_POST['user_email']))
			{
				if (filter_var($this->input->post('user_email', true), FILTER_VALIDATE_EMAIL)) 
				{
					$user_data = $this->Login_model->auth_user($this->input->post('user_email', true));
					
					if (count($user_data) > 0) 
					{
						$user_data = $user_data[0];
						
						if ($user_data->email_confirmed == '1')
						{
							if ($user_data->user_password == md5($this->input->post('user_password', true)))
							{							
								$this->Login_model->last_logged_in($user_data->user_email);
								
								$employee_name = $user_data->full_name; // ($user_data->employee_name != '') ? $user_data->employee_name : $user_data->full_name;

								// company info
								$this->session->set_userdata(S_COMPANY_ID		, $user_data->company_id);
								$this->session->set_userdata(S_COMPANY_NAME		, $user_data->company_name);
								$this->session->set_userdata(S_COMPANY_EXPIRED	, $user_data->is_expired);
								
								// user information
								$this->session->set_userdata(S_USER_EMAIL		, $user_data->user_email);
                                $this->session->set_userdata(S_USER_GROUP_ID	, $user_data->user_group_id);
                                $this->session->set_userdata(S_USER_GROUP_NM	, $user_data->user_group_description);
								$this->session->set_userdata(S_USER_IS_ADMIN	, $user_data->is_admin);
								$this->session->set_userdata(S_USER_LANDING		, $user_data->default_landing);
								
								// user - employee information
								$this->session->set_userdata(S_EMPLOYEE_ID		, $user_data->user_email);
								$this->session->set_userdata(S_EMPLOYEE_NAME	, $employee_name);
								$this->session->set_userdata(S_EMPLOYEE_PHOTO	, '');
																						
								$redirect_uri = $this->input->post('redirect_uri', true);
								if ($redirect_uri != '') 
								{
									redirect($redirect_uri);
								}
								else
								{
									// redirect('company');
                                    redirect($this->session->userdata(S_USER_LANDING));
								}
							}
							else
							{
								$this->session->set_flashdata('login_failed', 'Kata Sandi anda salah.');
							}
						}
						else
						{
							$this->session->set_flashdata('login_failed', 'Email anda belum dikonfirmasi. Silahkan cek Email anda.');
						}
					}
					else
					{
						$this->session->set_flashdata('login_failed', 'Email anda belum terdaftar.');
					}
				}
				else
				{
					$this->session->set_flashdata('login_failed', 'Email atau Kata Sandi salah.');					
				}
				redirect('login');
			}
			
            $this->load->view('login');
        }
    }

    function expired() 
	{
        $this->index();
    }

    function invalid_token() 
	{
        $this->index();
    }

    function reset_success() 
	{
        $this->index();
    }
	
	function misscode()
	{
		$this->index();
	}

    function out($next = '') 
	{
        $this->session->sess_destroy();
		redirect('login/' . $next);
    }

    function forgot_password() 
	{
        if (isset($_POST['email'])) 
		{
            $this->load->helper('string');
            $email 	= $this->input->post('email');
            $row 	= $this->Login_model->getEmployeeByUname($email);
			
            if ($row != false) 
			{
                $code 					= md5(random_string('alnum', 8));
                $data['employee_name'] 	= $row->full_name;
                $data['email'] 			= $email;
                $data['link'] 			= base_url() . 'reset_password/?k=' . $code;
                $emp['changed_by'] 		= $email;
                $emp['changed_dt'] 		= date('Y-m-d H:i:s');
                $emp['reset_password_code'] = $code;
				
                $this->Login_model->reset_password($emp, $email, false);

                $sent_email = $this->sent_email_reset_password($data, 'link_forgot');
                
                if($sent_email)
				{
                    $this->session->set_flashdata('notif_status', 'success');
                    $this->session->set_flashdata('notif_forgot_pass', '<strong>Berhasil. </strong> <i>Tautan</i> untuk ubah Kata Sandi telah dikirim ke e-mail.');
					redirect('forgot_password');
                }
				else
				{
                    $this->session->set_flashdata('notif_status', 'error');
                    $this->session->set_flashdata('notif_forgot_pass', '<strong> Gagal. </strong> Terjadi kesalahan saat mengirimkan email, coba beberapa saat lagi.');
					redirect('forgot_password');
                }
            }
			else 
			{
                $this->session->set_flashdata('notif_status', 'error');
                $this->session->set_flashdata('notif_forgot_pass', '<strong> </strong> Email anda tidak terdaftar.');
            }
        }

        $this->load->view('forgot_password');
    }

    function password_reset() {
        $email = $this->input->post('user_email', true);
        if ($email != '') {
            $reset_status = $this->Login_model->send_reset_password($email);
            redirect('login/forgot_password/' . $reset_status);
        } else {
            redirect('login/forgot_password/email_empty');
        }
    }

    function password_reset_token() {
        $data['valid_token'] = $this->Login_model->valid_token();
        $this->load->view('reset_password', $data);
    }

    function password_reset_do() {
        $p1 = $this->input->post('reset_password1', true);
        $p2 = $this->input->post('reset_password2', true);

        if ($p1 == $p2) {
            $this->Login_model->reset_password($p1, $this->input->post('user_email', true));
            redirect('login/reset_success');
        } else {
            redirect('login/password_reset_token/' . $this->input->post('reset_token', true) . '/not_match');
        }
    }

    public function check() 
	{
        $password = md5($this->input->post('user_password', true));
                
        if (filter_var($this->input->post('user_name', true), FILTER_VALIDATE_EMAIL)) 
		{
            $user_data = $this->Login_model->auth_user($this->input->post('user_name', true));			
            if (count($user_data) > 0) {
                $user_data = $user_data[0];
                
                if ($user_data->user_password == $password) 
				{
                    
                    $this->Login_model->last_logged_in($user_data->user_name);
					
					$employee_name = ($user_data->employee_name != '') ? $user_data->employee_name : 'Pengguna';

                    $this->session->set_userdata(S_COMPANY_ID, $user_data->company_id);
                    $this->session->set_userdata(S_COMPANY_NAME, $user_data->company_name);
                    $this->session->set_userdata(S_USER_NAME, $user_data->user_name);
                    $this->session->set_userdata(S_USER_GROUP_ID, $user_data->user_group_id);
                    $this->session->set_userdata(S_PHOTO, $user_data->photo);
                    $this->session->set_userdata(S_EMPLOYEE_ID, $user_data->employee_id);
                    $this->session->set_userdata(S_EMPLOYEE_NAME, $employee_name);
                    $this->session->set_userdata(S_IS_ADMIN, $user_data->is_admin);
                    $this->session->set_userdata(S_DEFAULT_LANDING, $user_data->default_landing);
                    $this->session->set_userdata(S_IS_EXPIRED, $user_data->is_expired);
                    
                    $redirect_uri = $this->input->post('redirect_uri', true);
                    if ($redirect_uri != '') 
					{
                        redirect($redirect_uri);
                    } 
					else 
					{
                        redirect($user_data->default_landing);
                    }
                }
				else 
				{
                    redirect('login/failed');
                }
            }
			else
			{
                redirect('login/failed');
            }
        }
		else 
		{
            redirect('login/failed');
        }
    }

    function reset_password() 
	{		
		$k = $this->input->get('k');
		if (!$k){
			$this->session->set_flashdata('notif_status', 'error');
                $this->session->set_flashdata('notif_forgot_pass', '<strong> Gagal. </strong> Kode untuk ubah kata sandi anda sudah pernah digunakan.');
				redirect('forgot_password');
		} 
		
		$um = $this->Login_model->get_email_by_reset_password_code($k);
		if (!$um) {
			$this->session->set_flashdata('notif_status', 'error');
                $this->session->set_flashdata('notif_forgot_pass', '<strong> Gagal. </strong> Kode untuk ubah kata sandi anda sudah pernah digunakan.');
				redirect('forgot_password');	
		}

        if (isset($_POST['email'])) {
            $email = $this->input->post('email');
            $newpassword = $this->input->post('newpassword');
            $key = $this->input->post('key');

            $data['user_password'] = md5($newpassword);
            $data['reset_password_code'] = null;
            $res = $this->Login_model->reset_password($data, $email, true, $key);

            if ($res > 0) {
                $this->session->set_flashdata('notif_status', 'success');
                $this->session->set_flashdata('notif_reset_pass', '<strong> Berhasil. </strong> Kata Sandi anda berhasil diubah. Sekarang anda dapat menggunakan akun anda kembali. Klik disini <a href="'.site_url('login').'">disini</a> untuk masuk ke halaman login.');
            } else {
                $this->session->set_flashdata('notif_status', 'error');
                $this->session->set_flashdata('notif_reset_pass', '<strong> Gagal. </strong> Kata Sandi anda gagal diubah. Kode untuk ubah kata sandi anda sudah pernah digunakan. Untuk Kembali ke halaman lupa kata sandi klik <a href="'.site_url('forgot_password').'">disini</a>');
            }
        }
        $data['key']	= $k;
		$data['um'] 	= $um;

        $this->load->view('reset_password', $data);
    }

    function sent_email_reset_password($data) 
	{
        $this->load->library('email');

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_timeout'] = '30';
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';

        $this->email->initialize($config);
        $this->email->from('admin@cpms.arkamaya.net', EMAIL_ALIAS);
        $this->email->to($data['email']);


        $message = $this->load->view('mail_reset_password', $data, true);
        $this->email->subject('Permintaan Ubah Kata Sandi ' . APP_NAME);
        $this->email->message($message);

        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }

}
