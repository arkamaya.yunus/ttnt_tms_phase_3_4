<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 28 April 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer extends MY_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->model('Customer_model');

        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index() {

        $data['stitle'] = 'Customer';
        $data['jsapp'] = array('customer');
        
        $this->load->view('header', $data);
        $this->load->view('customer');
        $this->load->view('footer');
    }

    function gets()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
		$sv		= $this->input->post('search')['value'];
		
        $results 			= $this->Customer_model->getCustomers($start, $length, $sv, $order, $columns);
        $recordsTotal       = (int)$this->Customer_model->getCountOfCustomer($sv);
		        
        $data = array();
		foreach ($results as $r) 
		{
            $row = array();			
			
            $row[] = $r->CustomerCode;
            $row[] = $r->CustomerName;

			$data[] = $row;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }

    function ondelete()
    {
        $success = false;

        $system_code = $this->input->post('system_code', true);

        // cek apakah sudah dipakai di timetable
        $cnt = $this->db->query("
            select count(timetable_id) as cnt from tb_r_timetable where customer = '" . $system_code . "'
        ")->row()->cnt;
        if ($cnt > 0)
        {
            $message = 'Customer already used in Timetable. Delete Failed.';
        }
        else
        {
            $this->db->where('system_type', 'customer');
            $this->db->where('system_code', $system_code);
            $this->db->delete('tb_m_system');

            $success = true;
            $message = 'Data already deleted';
        }

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }

    function save()
    {
        $todo = $this->input->post('todo', true);
        $system_code = $this->input->post('system_code', true);
        $system_value_txt = $this->input->post('system_value_txt', true);
        $message = "";

        $success = false;
        $data['system_value_txt'] = $system_value_txt;

        if ($todo == 'create')
        {
            // cek
            $cek = $this->db->query("select system_code from tb_m_system where system_type = 'customer' and system_code = '" . $system_code . "'")->result();
            if (count($cek) == 0)
            {
                $data['system_type'] = 'customer';
                $data['system_code'] = $system_code;
                $data['created_by'] = $this->session->userdata(S_USER_EMAIL);
                $data['created_dt'] = date('Y-m-d H:i:s');
    
                $this->db->insert('tb_m_system', $data);
    
                $success = true;
                $message = 'Customer saved';    
            }
            else
            {
                $message = 'Customer Code already Exist';

            }
        }
        else
        {
            $data['changed_by'] = $this->session->userdata(S_USER_EMAIL);
            $data['changed_dt'] = date('Y-m-d H:i:s');

            $this->db->where('system_type', 'customer');
            $this->db->where('system_code', $system_code);
            $this->db->update('tb_m_system', $data);

            $success = true;
            $message = 'Customer updated';
        }

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }
}