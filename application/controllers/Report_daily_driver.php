<?php

/*
 * @author: arka.gamal
 * @created: 07 07 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report_daily_driver extends MY_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->helper('common');
        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index()
    {
        date_default_timezone_set('Asia/Jakarta');

        $timetable_dt = date('Y-m-d');
        $timetable_df = date('Y-m-d');
        $this->id($timetable_dt, $timetable_df, $selected_customer="", $selected_departure_area="");
    }

    function id($timetable_dt, $timetable_df, $selected_customer, $selected_departure_area)
    {

        if ($timetable_dt == 'undefined-undefined-') redirect('report_driver');

        $data['stitle'] = 'Daily Driver - Report';
        $data['jsapp'] = array('report_daily_driver');
        $data['customer'] = $this->db->query("
            select system_code, system_value_txt from tb_m_system where system_type = 'customer' order by system_value_txt
        ")->result();
        $data['departure_area'] = $this->db->query("
            SELECT lp_cd FROM tb_m_logistic_point WHERE empty_plan_flg = '1' ORDER BY lp_cd
        ")->result();
        $data['timetable_dt'] = $timetable_dt;
        $data['timetable_df'] = $timetable_df;
        $data['selected_departure_area'] = $selected_departure_area;
        $data['selected_customer'] = $selected_customer;

        $this->load->view('header', $data);
        $this->load->view('report_daily_driver');
        $this->load->view('footer');
    }

    function download($timetable_dt, $timetable_df, $customer, $departure_area)
    {
        $w_customer = ($customer != 'all') ? " and b.customer = '" . $customer . "'" : '';
        $w_departure_area = ($departure_area != 'all') ? " and a.lp_cd = '" . $departure_area . "'" : '';
        $sql = "
        SELECT
        	a.vehicle_cd,
        	c.vehicle_number,
        	a.driver_cd,
        	a.driver_name,
        	b.customer_lp_cd,
        	a.departure_dt,
        	DATE_FORMAT(DATE_ADD(DATE_FORMAT(CONCAT(a.departure_dt,' ', a.departure_plan),'%Y-%m-%d %H:%i:%s'), INTERVAL -20 MINUTE), '%H:%i') AS departure_plan,
            a.lp_cd,
            a.route,
            a.cycle
        FROM
        	tb_r_timetable_detail a
        	INNER JOIN tb_r_timetable b ON b.timetable_id = a.timetable_id
        	INNER JOIN tb_m_vehicle c ON c.vehicle_cd = a.vehicle_cd
        	INNER JOIN tb_m_logistic_point d ON d.lp_cd = a.lp_cd AND d.empty_plan_flg = '1'
        WHERE
        	a.timetable_dt between '".$timetable_dt."' AND '".$timetable_df."'
            and a.departure_plan IS NOT null
            and a.arrival_plan IS null
          " . $w_customer . $w_departure_area . "
        ORDER BY
        	a.departure_dt
        	, a.departure_plan
        ";

        $data = $this->db->query($sql)->result();
        if (count($data) == 0)
        {
            $this->session->set_flashdata('notif_error', 'Data is not available on the current date range or filter, please try another date & filter.');
            redirect('report_daily_driver/id/' . $timetable_dt . '/' . $timetable_df . '/' . $customer . '/' . $departure_area, 'refresh');
        }
        // echo $timetable_dt; echo $timetable_df;
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        // die();

        $this->load->library('Excel');

    		$tpl_path = './assets/files/template-report-daily-driver.xlsx';
        $row = 8;
    		//$objPHPExcel = new PHPExcel();
    		$objPHPExcel = PHPExcel_IOFactory::load($tpl_path);
    		$sheet = $objPHPExcel->getActiveSheet();
    		$sheet->setTitle('Truck');
        // create data..
        $i = 1;
        $sheet->setCellValue('D4', date('d-m-Y', strtotime($timetable_dt)).' To '.date('d-m-Y', strtotime($timetable_df)));
        foreach($data as $d) {
          $sheet->setCellValue('B'.$row, $i);
          $sheet->setCellValue('C'.$row, $d->vehicle_cd);
          $sheet->setCellValue('D'.$row, $d->vehicle_number);
          $sheet->setCellValue('F'.$row, $d->driver_cd);
          $sheet->setCellValue('G'.$row, $d->driver_name);
          $sheet->setCellValue('I'.$row, $d->customer_lp_cd);
          $sheet->setCellValue('J'.$row, $d->route . ' - ' . $d->cycle);
          $sheet->setCellValue('K'.$row, $d->departure_plan);
          $row++;
          $i++;

          $sheet->getStyle("B" . ($row - 1) . ":K" . ($row - 1))->applyFromArray(
              array(
                  'borders' => array(
                      'allborders' => array(
                          'style' => PHPExcel_Style_Border::BORDER_THIN,
                          'color' => array('rgb' => '000000')
                      )
                  )
              )
          );

        }

        $file_name = "Report_Daily_Driver_".date('YmdHis').".xlsx";
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$file_name");
        header("Cache-Control: max-age=0");
        $objWriter= PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

        unset($sheet, $objPHPExcel);

    }
}
