<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 20 Maret 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Timetable extends MY_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->model('Timetable_model');

        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index() {
        date_default_timezone_set('Asia/Jakarta');
        $data['stitle'] = 'Time Table';
        $data['jsapp'] = array('timetable');
        $data['customer_lp_cds'] = $this->Timetable_model->getCustomerLpCds();
        $data['logistic_partners'] = $this->Timetable_model->getLogisticPartners();
        $data['timetable_dt_s'] = date('d/m/Y');
        $data['timetable_dt_e'] = date('d/m/Y');

        $data['business'] = $this->db->query("
            select system_code, system_value_txt from tb_m_system where system_type = 'business' order by system_value_num
        ")->result();

        $data['customer'] = $this->db->query("
            select system_code, system_value_txt from tb_m_system where system_type = 'customer' order by system_value_txt
        ")->result();

        $this->load->view('header', $data);
        $this->load->view('timetable');
        $this->load->view('footer');
    }

    function gets()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
		$sv		= $this->input->post('search')['value'];
        $customer_lp_cd = $this->input->post('customer_lp_cd', true);
        $logistic_partner = $this->input->post('logistic_partner', true);
        $timetable_dt_s = $this->input->post('timetable_dt_s', true);
        $timetable_dt_e = $this->input->post('timetable_dt_e', true);
        $business = $this->input->post('business', true);
        $customer = $this->input->post('customer', true);
		
        $results 			= $this->Timetable_model->getTimetables($business, $customer, $customer_lp_cd, $logistic_partner, $timetable_dt_s, $timetable_dt_e, $start, $length, $sv, $order, $columns);
        $recordsTotal       = (int)$this->Timetable_model->getCountOfTimetable($business, $customer, $customer_lp_cd, $logistic_partner, $timetable_dt_s, $timetable_dt_e, $sv);
		        
        $data = array();
		foreach ($results as $r) 
		{
            $row = array();			
            
            $row[] = md5($r->timetable_id);
            $row[] = $r->business;
            $row[] = $r->customer;
            $row[] = $r->customer_lp_cd;
            $row[] = date('d/m/Y', strtotime($r->timetable_dt));
            $row[] = $r->created_by;
            $row[] = '<a class="btn btn-purple waves-effect waves-light btn-xs" title="View Detail Timetable" href="'.site_url('timetable/id/' . md5($r->timetable_id)).'">View Detail</a>';
			$data[] = $row;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }

    function id()
    {
        // deletion
        if (isset($_POST['timetable_id']))
        {
            $this->Timetable_model->delete($_POST['timetable_id']);
            $this->session->set_flashdata('notif_success', '<strong>Success.</strong> Timetable deleted.');
            redirect('timetable');
        }

        $timetable_id = $this->uri->segment(3);
        if ($timetable_id == '') redirect('timetable');

        $timetable = $this->Timetable_model->getTimetable($timetable_id);
        if (count($timetable) == 0) {
            $this->session->set_flashdata('notif_danger', '<strong>Error.</strong> Timetable Not Found.');
            redirect('timetable');
        } 

        $timetable_detail = $this->Timetable_model->getTimeTableDetail($timetable_id);

        $data['timetable'] = $timetable[0];
        // $data['timetable_detail'] = $timetable_detail;
        $data['drivers'] = $this->Timetable_model->getDrivers();
        $data['vehicles'] = $this->Timetable_model->getVehicles();
        $data['lps'] = $this->Timetable_model->getCustomerLpCds(); 

        $data['stitle'] = 'Timetable Detail';
        $data['jsapp'] = array('timetable_id');

        // 12-April-2020 01:01
        // Pak Baqoh request to trigger on the sub-detail when coming from Dashboard Delay List
        $data['trigger_route'] = $this->uri->segment(4);
        $data['trigger_cycle'] = $this->uri->segment(5);

        $this->load->view('header', $data);
        $this->load->view('timetable_id');
        $this->load->view('footer');

    }

    function getDetails()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
        $sv		= $this->input->post('search')['value'];
        $timetable_id	= $this->input->post('timetable_id', true);
        $route	= $this->input->post('route', true);
        $cycle	= $this->input->post('cycle', true);
		
        $results 			= $this->Timetable_model->getTimetableDetails($timetable_id, $route, $cycle, $start, $length, $sv, $order, $columns);
        $recordsTotal       = (int)$this->Timetable_model->getCountOfTimetableDetail($timetable_id, $route, $cycle, $sv);
		        
        $data = array();
		foreach ($results as $r) 
		{
            $row = array();			
                        
            $row[] = $r->route;
            $row[] = $r->cycle;
            $row[] = $r->vehicle_cd;
            $row[] = $r->vehicle_number;            
            $row[] = $r->driver_cd;
            $row[] = $r->driver_name;
            $row[] = $r->lp_cd;
            $row[] = date('d-m-Y', strtotime($r->arrival_dt));
            $row[] = substr($r->arrival_plan, 0, 5);
            $row[] = ($r->arrival_actual != '') ? date('H:i', strtotime($r->arrival_actual)) : '-';

            $agap = '-';
            if ($r->arrival_gap != '')
            {
                if ($r->arrival_actual > ($r->arrival_dt . ' ' .$r->arrival_plan))
                {
                    $agap = $this->get_time_difference($r->arrival_plan, date('H:i:s', strtotime($r->arrival_actual)));
                }
                else
                {
                    $agap = $this->get_time_difference(date('H:i:s', strtotime($r->arrival_actual)), $r->arrival_plan);                    
                }
            }

            // jika keberangkatan
            if ($r->arrival_plan == '')
            {
                $agap = $r->arrival_gap;
            }

            $row[] = $agap;
            $row[] = $r->arrival_eval;
            $row[] = date('d-m-Y', strtotime($r->departure_dt));
            $row[] = substr($r->departure_plan, 0, 5);
            $row[] = ($r->departure_actual != '') ? date('H:i', strtotime($r->departure_actual)) : '-';

            $dgap = '-';
            if ($r->departure_gap != '')
            {
                if ($r->departure_actual > ($r->departure_dt . ' ' .$r->departure_plan))
                {
                    $dgap = $this->get_time_difference($r->departure_plan, date('H:i:s', strtotime($r->departure_actual)));
                }
                else
                {
                    $dgap = $this->get_time_difference(date('H:i:s', strtotime($r->departure_actual)), $r->departure_plan);                    
                }
            }
            $row[] = $dgap;
            $row[] = $r->departure_eval;
            $row[] = $r->remark;
            $row[] = $r->arrival_problem;
            $row[] = $r->departure_problem;
            $row[] = $r->timetable_detail_id;
            $row[] = $r->arrival_next_day;
            $row[] = $r->arrival_manual_by;
            $row[] = date('d-M-Y H:i:s', strtotime($r->arrival_manual_dt));
            $row[] = $r->departure_manual_by;
            $row[] = date('d-M-Y H:i:s', strtotime($r->departure_manual_dt));

            // addition 25-Mei-2020
            $row[] = $r->km;
            $row[] = $r->fuel;
            $row[] = $r->spbu;
            $row[] = $r->tol;
            $row[] = $r->others;
            $row[] = $r->pallet;
            $row[] = $r->rack;
            $row[] = $r->division;

			$data[] = $row;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }

    function get_time_difference($time1, $time2) {
        $time1 = strtotime("1980-01-01 $time1");
        $time2 = strtotime("1980-01-01 $time2");
        
        if ($time2 < $time1) {
            $time2 += 86400;
        }
        
        return date("H:i", strtotime("1980-01-01 00:00:00") + ($time2 - $time1));
    }

    function getSummary()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
        $sv		= $this->input->post('search')['value'];
        $timetable_id	= $this->input->post('timetable_id', true);
		
        $results 			= $this->Timetable_model->getTimetableSummary($timetable_id, $start, $length, $sv, $order, $columns);
        $recordsTotal       = (int)$this->Timetable_model->getCountOfTimetableSummary($timetable_id, $sv);
		        
        $data = array(); $no = 1;
		foreach ($results as $r) 
		{
            $row = array();
            
            $row[] = $no;
            $row[] = $r->route;
            $row[] = $r->cycle;
            $row[] = $r->vehicle_cd;
            $row[] = $r->driver_name;
            $row[] = $r->total;
            $row[] = $r->done;
            $row[] = $r->remain;

            $siz = ($r->percentage == '0') ? 'style="color: red;"' : '';
            $progress = '
                <div class="progress progress-lg pb-tt">
                    <div '.$siz.' class="progress-bar bg-primary" role="progressbar" aria-valuenow="'.$r->percentage.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$r->percentage.'%;">
                        '.$r->percentage.'%
                    </div>
                </div>
            ';

            $row[] = $progress;

            $btn = '
            <button type="button" class="btn btn-purple waves-effect waves-light btn-sm" title="Change Truck" onclick="changeTruck(\'truck\',\''.$r->route.'\',\''.$r->cycle.'\',\''.$r->vehicle_cd.'\',\''.$r->driver_cd.'\')"><i class="mdi mdi-truck"></i></button>
            <button type="button" class="btn btn-purple waves-effect waves-light btn-sm" title="Change Driver" onclick="changeTruck(\'driver\',\''.$r->route.'\',\''.$r->cycle.'\',\''.$r->vehicle_cd.'\',\''.$r->driver_cd.'\')"><i class="mdi mdi-account"></i></button>
            <button id="btn-'.$r->route.'-'.$r->cycle.'" type="button" class="btn btn-purple waves-effect waves-light btn-sm" title="View Detail" onclick="viewDetail(\''.$r->route.'\',\''.$r->cycle.'\')"><i class=" mdi mdi-magnify"></i> View Detail</button>
            <button id="btn-del-'.$r->route.'-'.$r->cycle.'" type="button" class="btn btn-danger waves-effect waves-light btn-sm" title="Delete" onclick="delPerRC(\''.$r->route.'\',\''.$r->cycle.'\')"><i class=" mdi mdi-recycle"></i> Delete</button>
            ';

            $row[] = $btn;
          
            $data[] = $row;
            
            $no++;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }

    function create()
    {        
        $data['stitle'] = 'Create Timetable';
        $data['jsapp'] = array('timetable_create');
        $data['customer_lp_cds'] = $this->Timetable_model->getCustomerLpCds();
        
        $data['business'] = $this->db->query("
            select system_code, system_value_txt from tb_m_system where system_type = 'business' order by system_value_num
        ")->result();

        $data['customer'] = $this->db->query("
            select system_code, system_value_txt from tb_m_system where system_type = 'customer' order by system_value_txt
        ")->result();

        // Delete Temp first for current user
        $this->Timetable_model->deleteTemp();

        $this->load->view('header', $data);
        $this->load->view('timetable_create');
        $this->load->view('footer');
    }

    function deletetemp()
    {
        // Delete Temp first for current user
        $this->Timetable_model->deleteTemp();

        $result['success']      = true;
        $result['message']      = 'Success Delete Temporary table';

        echo json_encode($result);
    }

    function do_upload()
    {
        $success = true;
        $message = '';
        $data = array();
        $err_cnt = 0;
        $proceed_step = 0;
        $process_id = '';

        // Delete Temp first for current user
        $this->Timetable_model->deleteTemp();

        // Cek sudah ada apa belum timetable untuk [Customer] dan [Tanggal] tersebut
        // $checkTimetable = $this->Timetable_model->checkTimetable(
        //     $this->input->post('customer_lp_cd', true),
        //     $this->input->post('timetable_dts', true)
        // );
        
        // 13-Apr-2020 Diloss kan dulu soalnya klo per Route itu level detail
        $checkTimetable = 0;

        if ($checkTimetable == 0)
        {
            if(!empty($_FILES['file_attach']['name']))
            {
                $config['upload_path'] = './assets/timetables';
                $config['allowed_types'] = 'xlsx';
                $config['remove_spaces'] = TRUE;

                $filename = '';

                $this->load->library('upload', $config);
                if(!$this->upload->do_upload('file_attach'))
                {
                    $success = false;
                    $message = $this->upload->display_errors();
                }
                else
                {
                    $dataUpload = $this->upload->data();
                    $filename   = $dataUpload["file_name"];
                }

                if(!empty($filename))
                {
                    $real_path= getcwd() . "/assets/timetables/";        
                    
                    $this->load->library('excel');
                    $inputFileType  = 'Excel2007';
                    $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel    = $objReader->load($real_path . $filename);
                    $activeSheet    = $objPHPExcel->getActiveSheet();
                    $arr_spread     = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $total_row      = count($arr_spread);
                    $sheetData      = $activeSheet->toArray(null, true, true, true); 

                    // validasi isi header template nya
                    if(
                        strtolower(trim($sheetData['1']["A"])) != 'route' || 
                        strtolower(trim($sheetData['1']["B"])) != 'cycle' || 
                        strtolower(trim($sheetData['1']["C"])) != 'truck' ||
                        strtolower(trim($sheetData['1']["D"])) != 'driver' ||

                        strtolower(trim($sheetData['1']["E"])) != 'km' ||
                        strtolower(trim($sheetData['1']["L"])) != 'division' ||

                        strtolower(trim($sheetData['1']["M"])) != 'logistic_point' ||
                        strtolower(trim($sheetData['1']["N"])) != 'arrival_next_day' ||
                        strtolower(trim($sheetData['1']["O"])) != 'arrival_plan' ||
                        strtolower(trim($sheetData['1']["P"])) != 'departure_next_day' ||
                        strtolower(trim($sheetData['1']["Q"])) != 'departure_plan'
                    )
                    {
                        $success = false;
                        $message = 'Invalid Excel Template. Please download from Download Template link.';
                    }
                    else
                    {
                        // $message = 'file sudah betul. tinggal diproses';
                        $process_id = date('YmdHisv').$this->session->userdata(S_USER_EMAIL);

                        // INSERT INTO TEMPORARY TABLE HEADER
                        $timetable_dts = explode(',', $this->input->post('timetable_dts', true));

                        $tempArrayH = array();
                        foreach ($timetable_dts as $timetable_dt)
                        {
                            $arrH['process_id']      = $process_id;
                            $arrH['business']  = $this->input->post('business', true);
                            $arrH['customer']  = $this->input->post('customer', true);
                            $arrH['customer_lp_cd']  = $this->input->post('customer_lp_cd', true);
                            $arrH['timetable_dt']     = $timetable_dt;

                            $tempArrayH[] = $arrH;
                        }
                        $this->Timetable_model->insertTempUploadH($tempArrayH);

                        $tempArrayD = array();
                        
                        for($row = 2; $row <= $total_row; $row++)
                        {
                            $is_valid = 1;
                            $error_detail = '';// "<b>Status data : </b><br/>";
                            $arr = array();

                            if(
                                !empty(trim($sheetData[$row]["A"])))
                            {
                                $route              = $sheetData[$row]["A"];
                                $cycle              = $sheetData[$row]["B"];
                                $truck              = $sheetData[$row]["C"];
                                $driver             = $sheetData[$row]["D"];

                                $km                 = $sheetData[$row]["E"];
                                $fuel               = $sheetData[$row]["F"];
                                $spbu               = $sheetData[$row]["G"];
                                $tol                = $sheetData[$row]["H"];
                                $others             = $sheetData[$row]["I"];
                                $pallet             = $sheetData[$row]["J"];
                                $rack               = $sheetData[$row]["K"];
                                $division           = $sheetData[$row]["L"];

                                $logistic_point     = $sheetData[$row]["M"];
                                $arrival_next_day   = $sheetData[$row]["N"];
                                $arrival_plan       = $sheetData[$row]["O"];
                                $departure_next_day = $sheetData[$row]["P"];
                                $departure_plan     = $sheetData[$row]["Q"];

                                // cek apakah Sudah ada di Tgl,Business,Customer,CLP, Route, Cycle tersebut
                                $cek = $this->db->query("
                                    select count(a.timetable_detail_id) as cnt
                                    from tb_r_timetable_detail a inner join tb_r_timetable b on b.timetable_id = a.timetable_id
                                    where
                                        b.business = '" . $this->input->post('business', true)."'
                                        and b.customer = '" . $this->input->post('customer', true). "'
                                        and b.customer_lp_cd = '" . $this->input->post('customer_lp_cd', true). "'
                                        and a.route = '" . $route . "'
                                        and a.cycle = '" . $cycle . "'
                                        and a.timetable_dt = '" . $timetable_dt . "'
                                ")->row()->cnt;
                                if ($cek > 0)
                                {
                                    $is_valid = 0;
                                    $error_detail .= "- <span class='text-danger'>Data Already Exists for same Route and Cycle</span><br/>";
                                }

                                // VALIDASI MANDATORY
                                if(empty(trim($route))){
                                    $is_valid = 0;
                                    $error_detail .= "- <span class='text-danger'>Route is required</span><br/>";
                                }
                                
                                if(empty(trim($cycle))){
                                    $is_valid = 0;
                                    $error_detail .= "- <span class='text-danger'>Cycle is required</span><br/>";
                                }

                                if(empty(trim($truck))){
                                    $is_valid = 0;
                                    $error_detail .= "- <span class='text-danger'>Truck is required</span><br/>";
                                }

                                if(empty(trim($driver))){
                                    $is_valid = 0;
                                    $error_detail .= "- <span class='text-danger'>driver is required</span><br/>";
                                }

                                if(empty(trim($logistic_point))){
                                    $is_valid = 0;
                                    $error_detail .= "- <span class='text-danger'>Logistic Point is required</span><br/>";
                                }

                                // if($arrival_next_day == '' || $arrival_next_day != '0'){
                                //     $is_valid = 0;
                                //     $error_detail .= "- <span class='text-danger'>Arrival Next Day is required</span><br/>";
                                // }

                                $cek_logistic_point = $this->Timetable_model->checkLogisticPoint($logistic_point);
                                if ($cek_logistic_point == 0)
                                {
                                    $is_valid = 0;
                                    $error_detail .= "- <span class='text-danger'>Logistic Point not registered in Master Data</span><br/>";
                                }
                                else
                                {
                                    // cek apakah lp_cd nya empty_flag = 1 jika iya maka arrival plan boleh kosong
                                    $empty_plan_flg = $this->db->query("select empty_plan_flg from tb_m_logistic_point where lp_cd = '" . $logistic_point . "'")->row()->empty_plan_flg;                                

                                    if(empty(trim($arrival_plan)) && ($empty_plan_flg == '0' || $empty_plan_flg == '') ){
                                        $is_valid = 0;
                                        $error_detail .= "- <span class='text-danger'>Arrival Plan is required</span><br/>";
                                    }
                                }
                                
                                // if($departure_next_day == '' || $departure_next_day != '0'){
                                //     $is_valid = 0;
                                //     $error_detail .= "- <span class='text-danger'>Departure Next Day is required</span><br/>";
                                // }

                                if(empty(trim($departure_plan)) && ($empty_plan_flg == '0' || $empty_plan_flg == '') ){
                                    $is_valid = 0;
                                    $error_detail .= "- <span class='text-danger'>Departure Plan is required</span><br/>";
                                }
                                //END

                                // VALIDASI DATA MASTER
                                $cek_truck = $this->Timetable_model->checkTruck($truck);
                                if ($cek_truck == 0)
                                {
                                    $is_valid = 0;
                                    $error_detail .= "- <span class='text-danger'>Truck not registered in Master Data</span><br/>";
                                }

                                $cek_driver = $this->Timetable_model->checkDriver($driver);
                                if ($cek_driver == 0)
                                {
                                    $is_valid = 0;
                                    $error_detail .= "- <span class='text-danger'>Driver not registered / not active in Master Data</span><br/>";
                                }

                                
                                //END
                                                                
                                // INSERT INTO TEMPORARY TABLE DETAIL
                                $arr['process_id']          = $process_id;
                                $arr['route']               = $route;
                                $arr['cycle']               = $cycle;
                                $arr['vehicle_cd']          = $truck;
                                $arr['driver_name']         = $driver;

                                // Additional - 25 Mei 2020 -- for rollcall driver report
                                $arr['km']                  = $km;
                                $arr['fuel']                = $fuel;
                                $arr['spbu']                = $spbu;
                                $arr['tol']                 = $tol;
                                $arr['others']              = $others;
                                $arr['pallet']              = $pallet;
                                $arr['rack']                = $rack;
                                $arr['division']            = $division;

                                $arr['lp_cd']               = $logistic_point;
                                $arr['arrival_next_day']    = $arrival_next_day;
                                $arr['arrival_plan']        = $arrival_plan;
                                $arr['departure_next_day']  = $departure_next_day;
                                $arr['departure_plan']      = $departure_plan;
                                $arr['is_valid']            = $is_valid;
                                $arr['error_detail']        = $error_detail;

                                $tempArrayD[] = $arr;

                                // increment error count
                                $err_cnt += ($is_valid == 0) ? 1 : 0;
                                
                            }
                        }

                        // remove the file!
                        unlink($real_path . $filename);

                        // insert into temporary detail
                        $this->Timetable_model->insertTempUploadD($tempArrayD);

                        // set message if error count more than one
                        if ($err_cnt > 0)
                        {                            
                            $message = 'Upload Success with some error. Please see details in data-table.';
                            $proceed_step = 1; // 1 means display datatable but can-not go further
                        }
                        else
                        {
                            $message = 'Upload Success. You can double check the data and go to the Process button';
                            $proceed_step = 2; // 1 means display datatable and can go further
                        }
                        
                        $data['customer_lp_cd'] = $this->input->post('customer_lp_cd', true);
                        $data['timetable_dts'] = $this->input->post('timetable_dts', true);
                        $data['process_id'] = $process_id;
                    }
                }
            }
        }
        else
        {
            $success = false;
            $message = 'Timetable Planning already set for Customer and selected date(s). Cancel the date or remove previous timetable planning';
        }                

        $result['success']      = $success;
        $result['message']      = $message;
        $result['data']         = $data;
        $result['proceed_step'] = $proceed_step;
        $result['process_id']   = $process_id;

        echo json_encode($result);
    }

    function get_temp_timetable()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
        $sv		= $this->input->post('search')['value'];
        $process_id = $this->input->post('process_id', true);
		
        $results 			= $this->Timetable_model->getTimetableUpload($process_id, $start, $length, $sv, $order, $columns);
        $recordsTotal       = (int)$this->Timetable_model->getCountOfTimetableUpload($process_id, $sv);
		        
        $data = array();
		foreach ($results as $r) 
		{
            $row = array();			
            
            $row[]  = $r->is_valid;
            $row[]  = ($r->is_valid == '0') ? $r->error_detail : '<span style="color: green">OK</span>';
            $row[]  = $r->route;
            $row[]  = $r->cycle;
            $row[]  = $r->vehicle_cd;
            $row[]  = $r->driver_name;
            $row[]  = $r->km;
            $row[]  = $r->fuel;
            $row[]  = $r->spbu;
            $row[]  = $r->tol;
            $row[]  = $r->others;
            $row[]  = $r->pallet;
            $row[]  = $r->rack;
            $row[]  = $r->division;
            $row[]  = $r->lp_cd;
            $row[]  = $r->arrival_next_day;
            $row[]  = $r->arrival_plan;
            $row[]  = $r->departure_next_day;
            $row[]  = $r->departure_plan;
			$data[] = $row;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }

    function do_process()
    {
        $process_id = $this->input->post('process_id', true);
        $process = $this->Timetable_model->doProcess2($process_id);

        $success = true;
        $message = '';

        if ($process)
        {
            $this->session->set_flashdata('notif_success', '<strong>Success.</strong> New Timetable Has been created.');
        }
        else
        {
            $success = false;
            $message = 'Error when processing timetable data';
        }

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);

    }

    function saveDetail()
    {        
        $process = $this->Timetable_model->saveDetail();

        $success = true;
        $message = 'Timetable already saved';

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }

    function saveTW()
    {        
        $process = $this->Timetable_model->saveTW();

        $success = true;
        $message = 'Data already saved';

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }

    function getTotalHours()
    {        
        $hours = $this->Timetable_model->getTotalHours();
        // print_r($hours)        ; die();

        $success = true;
        // $message = 'Data already saved';

        $result['success']                  = $success;
        $result['total_working_hours']      = $hours[0]->total_working_hours;
        $result['total_loading_unloading']  = $hours[0]->total_loading_unloading;
        $result['total_driver_hours']       = $hours[0]->total_driver_hours;

        $result['arrival_ontime']       = $hours[0]->arrival_ontime;
        $result['arrival_advance']       = $hours[0]->arrival_advance;
        $result['arrival_delay']       = $hours[0]->arrival_delay;
        $result['arrival_total']       = $hours[0]->arrival_total;

        $result['departure_ontime']       = $hours[0]->departure_ontime;
        $result['departure_advance']       = $hours[0]->departure_advance;
        $result['departure_delay']       = $hours[0]->departure_delay;
        $result['departure_total']       = $hours[0]->departure_total;
        $result['destination_result']       = $hours[0]->destination_result;

        echo json_encode($result);
    }

    function saveNLP()
    {        
        $success = $this->Timetable_model->saveNLP();

        $message = 'Data already saved';

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }

    function deleteNLP()
    {        
        $success = $this->Timetable_model->deleteNLP();

        $message = 'Data already deleted';

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }

    function deleteRC()
    {
        $this->db->where('md5(timetable_id)', $this->input->post('timetable_id', true));
        $this->db->where('route', $this->input->post('route', true));
        $this->db->where('cycle', $this->input->post('cycle', true));
        $this->db->delete('tb_r_timetable_detail');

        $success = true;

        $message = 'Data already deleted';

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }

    function deleteRCNotMd5()
    {
        $this->db->where('timetable_id', $this->input->post('timetable_id', true));
        $this->db->where('route', $this->input->post('route', true));
        $this->db->where('cycle', $this->input->post('cycle', true));
        $this->db->delete('tb_r_timetable_detail');

        $success = true;

        $message = 'Data already deleted';

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }
}
