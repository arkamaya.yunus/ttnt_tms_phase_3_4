<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 14 Maret 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vehicle extends MY_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->model('Vehicle_model');

        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index() {

        $data['stitle'] = 'Vehicle';
        $data['jsapp'] = array('vehicle');
        $data['owners'] = $this->Vehicle_model->getVehicleOwners();
        
        $this->load->view('header', $data);
        $this->load->view('vehicle');
        $this->load->view('footer');
    }

    function gets()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
		$sv		= $this->input->post('search')['value'];
        $vehicle_owner = $this->input->post('vehicle_owner', true);
        $active = $this->input->post('active', true);
        $business = $this->input->post('business', true);
		
        $results 			= $this->Vehicle_model->getVehicles($vehicle_owner, $active, $start, $length, $sv, $order, $columns, $business);
        $recordsTotal       = (int)$this->Vehicle_model->getCountOfVehicle($vehicle_owner, $active, $sv, $business);
		        
        $data = array();
		foreach ($results as $r) 
		{
            $row = array();			
			
            $row[] = '<a href="'.site_url('vehicle/id/' . md5($r->vehicle_cd)).'" title="View Detail '.$r->vehicle_cd.'">' . $r->vehicle_cd . '</a>';
            // $row[] = $r->vehicle_id;
            $row[] = $r->vehicle_number;            
			$row[] = nl2br(strip_tags(shorten_string($r->vehicle_type, 8)));
            $row[] = ($r->license_stnk != '') ? date('d-M-Y', strtotime($r->license_stnk)) : '-';
            $row[] = ($r->license_keur != '') ? date('d-M-Y', strtotime($r->license_keur)) : '-';
            $row[] = ($r->license_sipa != '') ? date('d-M-Y', strtotime($r->license_sipa)) : '-';
            $row[] = ($r->license_ibm != '') ? date('d-M-Y', strtotime($r->license_ibm)) : '-';
            $row[] = $r->vehicle_owner;
            $row[] = $r->brand;
            $row[] = ($r->active == '1') ? 'Yes' : 'No';
            $row[] = $r->business;
			$data[] = $row;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }

    function id()
    {
        // deletion
        if (isset($_POST['vehicle_cd']))
        {
            $this->Vehicle_model->delete($_POST['vehicle_cd']);
			$this->session->set_flashdata('notif_success', '<strong>Success.</strong> Vehicle deleted.');
			redirect('vehicle');
        }

        $vehicle_cd = $this->uri->segment(3);
        if ($vehicle_cd == '') redirect('vehicle');

        $vehicle = $this->Vehicle_model->getVehicle($vehicle_cd);
        if (count($vehicle) == 0) redirect('vehicle');

        $data['vehicle'] = $vehicle[0];
        $data['stitle'] = $vehicle[0]->vehicle_number;
        $data['jsapp'] = array('vehicle_id');

        $this->load->view('header', $data);
        $this->load->view('vehicle_id');
        $this->load->view('footer');
    }

    function create()
	{
        $save_sts = '';
        $vehicle_cd = $vehicle_id = $vehicle_number = $vehicle_type = $brand = '';
        $vehicle_owner = $insurance_no = $license_stnk = $license_keur = $license_sipa = $license_ibm = $business = '';
        $active = '1';

		if (isset($_POST['vehicle_cd']))
		{
            $save_sts = $this->Vehicle_model->save() ;
            if ($save_sts == '1')
            {
                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> Vehicle Created.');						
			    redirect('vehicle/id/' . md5($this->input->post('vehicle_cd', true)));
            }
            else
            {
                $vehicle_cd = $this->input->post('vehicle_cd', true);
                $vehicle_id = $this->input->post('vehicle_id', true);
                $vehicle_number = $this->input->post('vehicle_number', true);
                $vehicle_type = $this->input->post('vehicle_type', true);
                $vehicle_owner = $this->input->post('vehicle_owner', true);
                $insurance_no = $this->input->post('insurance_no', true);
                $license_stnk = $this->input->post('license_stnk', true);
                $license_keur = $this->input->post('license_keur', true);
                $license_sipa = $this->input->post('license_sipa', true);
                $license_ibm = $this->input->post('license_ibm', true);
                $brand = $this->input->post('brand', true);
                $active = $this->input->post('active', true);
                $business = $this->input->post('business', true);
            }
		}
		
		$data['jsapp'] 		= array('vehicle_create');
        $data['stitle'] = 'Create Vehicle';
        $data['save_sts'] = $save_sts;

        // filled the field
        $data['vehicle_cd'] = $vehicle_cd;
        $data['vehicle_id'] = $vehicle_id;
        $data['vehicle_number'] = $vehicle_number;
        $data['vehicle_type'] = $vehicle_type;
        $data['vehicle_owner'] = $vehicle_owner;
        $data['insurance_no'] = $insurance_no;
        $data['license_stnk'] = $license_stnk;
        $data['license_keur'] = $license_keur;
        $data['license_sipa'] = $license_sipa;
        $data['license_ibm'] = $license_ibm;
        $data['brand'] = $brand;
        $data['active'] = $active;
        $data['business'] = $business;
        
		$this->load->view('header', $data);
		$this->load->view('vehicle_create');
		$this->load->view('footer');
	}

    function get_vehicle_owners()
	{
		$results = $this->Vehicle_model->getVehicleOwners($_GET['query']);
		
		$suggestions = array();
		foreach ($results as $r)
		{
			$suggestions[] = array(
				'value' => $r->vehicle_owner,
				'data' => $r->vehicle_owner
			);
		}
		
        echo json_encode(array(
			'query' => 'Unit'
			, 'suggestions' => $suggestions
		));
    }
    
    function edit()
	{
        $save_sts = '';

        $vehicle_cd = $this->uri->segment(3);
        if ($vehicle_cd == '') redirect('vehicle');

        $vehicle = $this->Vehicle_model->getVehicle($vehicle_cd);
        if (count($vehicle) == 0) redirect('vehicle');
        $vehicle = $vehicle[0];

        $vehicle_cd = $vehicle->vehicle_cd;
        $vehicle_id = $vehicle->vehicle_id;
        $vehicle_number = $vehicle->vehicle_number;
        $vehicle_type = $vehicle->vehicle_type;
        $vehicle_owner = $vehicle->vehicle_owner;
        $brand = $vehicle->brand;
        $insurance_no = $vehicle->insurance_no;
        $license_stnk = $vehicle->license_stnk;
        $license_keur = $vehicle->license_keur;
        $license_sipa = $vehicle->license_sipa;
        $license_ibm = $vehicle->license_ibm;
        $active = $vehicle->active;
        $business = $vehicle->business;

		if (isset($_POST['vehicle_cd']))
		{
            $save_sts = $this->Vehicle_model->save('1') ;
            if ($save_sts == '2')
            {
                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> Vehicle Updated.');						
			    redirect('vehicle/id/' . md5($this->input->post('vehicle_cd', true)));
            }
            else
            {
                $vehicle_cd = $this->input->post('vehicle_cd', true);
                $vehicle_id = $this->input->post('vehicle_id', true);
                $vehicle_number = $this->input->post('vehicle_number', true);
                $vehicle_type = $this->input->post('vehicle_type', true);
                $vehicle_owner = $this->input->post('vehicle_owner', true);
                $insurance_no = $this->input->post('insurance_no', true);
                $license_stnk = $this->input->post('license_stnk', true);
                $license_keur = $this->input->post('license_keur', true);
                $license_sipa = $this->input->post('license_sipa', true);
                $license_ibm = $this->input->post('license_ibm', true);
                $brand = $this->input->post('brand', true);
                $active = $this->input->post('active', true);
                $business = $this->input->post('business', true);
            }
		}
		
		$data['jsapp'] 		= array('vehicle_edit');
        $data['stitle'] = 'Edit Vehicle';
        $data['save_sts'] = $save_sts;

        // filled the field
        $data['vehicle_cd'] = $vehicle_cd;
        $data['vehicle_id'] = $vehicle_id;
        $data['vehicle_number'] = $vehicle_number;
        $data['vehicle_type'] = $vehicle_type;
        $data['vehicle_owner'] = $vehicle_owner;
        $data['insurance_no'] = $insurance_no;
        $data['license_stnk'] = $license_stnk;
        $data['license_keur'] = $license_keur;
        $data['license_sipa'] = $license_sipa;
        $data['license_ibm'] = $license_ibm;
        $data['brand'] = $brand;
        $data['active'] = $active;
        $data['business'] = $business;
        
		$this->load->view('header', $data);
		$this->load->view('vehicle_edit');
		$this->load->view('footer');
    }
    
    function download($vehicle_owner, $active, $business)
    {
        $this->load->library('Excel');
		
		$tpl_path = './assets/files/template-vehicle.xlsx';
		
		//$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($tpl_path);
		$sheet = $objPHPExcel->getActiveSheet();
		$sheet->setTitle('Truck');
		
		// create data..
		$row = 6;
		
		$data = $this->Vehicle_model->getVehiclesForDownload($vehicle_owner, $active, $business);
		// No	Truck ID	Police Number	Truck Type	Brand	Owner	LICENSE				Based
        // STNK	KIR	SIPA	IBM	

        $i = 1;
        foreach($data as $d) {
			$sheet->setCellValue('A'.$row, $i);
			$sheet->setCellValue('B'.$row, $d->vehicle_cd);
			$sheet->setCellValue('C'.$row, $d->vehicle_number);
			$sheet->setCellValue('D'.$row, $d->vehicle_type);
			$sheet->setCellValue('E'.$row, $d->license_stnk);
			$sheet->setCellValue('F'.$row, $d->license_keur);
			$sheet->setCellValue('G'.$row, $d->license_sipa);
			$sheet->setCellValue('H'.$row, $d->license_ibm);
            // $sheet->setCellValue('K'.$row, $d->based);
            
            $active = ($d->active == '1') ? 'Yes' : 'No';
            $sheet->setCellValue('I'.$row, $d->vehicle_owner);
			$sheet->setCellValue('J'.$row, $d->brand);
			$sheet->setCellValue('K'.$row, $active);
            $sheet->setCellValue('L'.$row, $d->business);
            $row++;
            $i++;
		}
				
		$file_name = "Master_Data_Vehicle_".date('YmdHis').".xlsx";
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment;filename=$file_name");
		header("Cache-Control: max-age=0");
		$objWriter= PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

		unset($sheet, $objPHPExcel);
    }
    
    function upload()
    {
        $success = true;
        $message = '';
        
        
        if(!empty($_FILES['file_attach']['name']))
        {
            $config['upload_path'] = './assets/timetables';
            $config['allowed_types'] = 'xlsx';
            $config['remove_spaces'] = TRUE;

            $filename = '';

            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('file_attach'))
            {
                $success = false;
                $message = $this->upload->display_errors();
            }
            else
            {
                $dataUpload = $this->upload->data();
                $filename   = $dataUpload["file_name"];
            }

            if(!empty($filename))
            {
                $real_path= getcwd() . "/assets/timetables/";        
                
                $this->load->library('excel');
                $inputFileType  = 'Excel2007';
                $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel    = $objReader->load($real_path . $filename);
                $activeSheet    = $objPHPExcel->getActiveSheet();
                $arr_spread     = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $total_row      = count($arr_spread);
                $sheetData      = $activeSheet->toArray(null, true, true, true); 

                // validasi isi header template nya
                if(
                    strtolower(trim($sheetData['4']["A"])) != 'no' || 
                    strtolower(trim($sheetData['4']["I"])) != 'owner' ||
                    strtolower(trim($sheetData['4']["J"])) != 'brand'                     
                )
                {
                    $success = false;
                    $message = 'Invalid Excel Template. Please download from Download Template link.';
                }
                else
                {
                    $values = '';
                    
                    for($row = 6; $row <= $total_row; $row++)
                    {
                        $is_valid = 1;
                        $error_detail = '';// "<b>Status data : </b><br/>";
                        $arr = array();

                        if(
                            !empty(trim($sheetData[$row]["B"])))
                        {
                            $vehicle_cd     = $sheetData[$row]["B"];
                            $vehicle_number = $sheetData[$row]["C"];
                            $vehicle_type   = $sheetData[$row]["D"];
                            $license_stnk   = ($sheetData[$row]["E"] != '') ? "'" . $sheetData[$row]["E"] . "'" : 'null';;
                            $license_keur   = ($sheetData[$row]["F"] != '') ? "'" . $sheetData[$row]["F"] . "'" : 'null';
                            $license_sipa   = ($sheetData[$row]["G"] != '') ? "'" . $sheetData[$row]["G"] . "'" : 'null';;
                            $license_ibm    = ($sheetData[$row]["H"] != '') ? "'" . $sheetData[$row]["H"] . "'" : 'null';;
                            // $based          = $sheetData[$row]["K"];
                            $vehicle_owner  = $sheetData[$row]["I"];
                            $brand          = $sheetData[$row]["J"];
                            $active          = $sheetData[$row]["K"];
                            $business          = $sheetData[$row]["L"];
                            
                            $active = (strtolower($active) == 'yes') ? '1' : '0';

                            
                            $values .= "
                                (
                                    '" . $vehicle_cd . "',
                                    '" . $vehicle_number . "',
                                    '" . $vehicle_type . "',
                                    '" . $brand . "',
                                    '" . $vehicle_owner . "',
                                    " . $license_stnk . ",
                                    " . $license_keur . ",
                                    " . $license_sipa . ",
                                    " . $license_ibm . ",                                    
                                    '" . $active . "',
                                    '" . $business . "',
                                    '" . $this->session->userdata(S_USER_EMAIL) . "',
                                    '" . date('Y-m-d H:i:s') . "',
                                    '" . $this->session->userdata(S_USER_EMAIL) . "',
                                    '" . date('Y-m-d H:i:s') . "'
                                )
                            ";

                            $values .= ($row < $total_row) ? ', ' : '';
                        }
                    }

                    // remove the file!
                    unlink($real_path . $filename);

                    if ($values != '')
                    {
                        // insert into temporary detail
                        $sql = "insert into tb_m_vehicle
                            (
                                tb_m_vehicle.vehicle_cd, 
                                tb_m_vehicle.vehicle_number,
                                tb_m_vehicle.vehicle_type,
                                tb_m_vehicle.brand,
                                tb_m_vehicle.vehicle_owner,
                                tb_m_vehicle.license_stnk,
                                tb_m_vehicle.license_keur,
                                tb_m_vehicle.license_sipa,
                                tb_m_vehicle.license_ibm,                                
                                tb_m_vehicle.active,
                                tb_m_vehicle.business,
                                tb_m_vehicle.created_by,tb_m_vehicle.created_dt,
                                tb_m_vehicle.changed_by,tb_m_vehicle.changed_dt
                            )
                            VALUES 
                                " . $values . "                                    
                            ON DUPLICATE KEY UPDATE 
                                tb_m_vehicle.vehicle_number = VALUES(tb_m_vehicle.vehicle_number),
                                tb_m_vehicle.vehicle_type = VALUES(tb_m_vehicle.vehicle_type),
                                tb_m_vehicle.brand = VALUES(tb_m_vehicle.brand),
                                tb_m_vehicle.vehicle_owner = VALUES(tb_m_vehicle.vehicle_owner),
                                tb_m_vehicle.license_stnk = VALUES(tb_m_vehicle.license_stnk),
                                tb_m_vehicle.license_keur = VALUES(tb_m_vehicle.license_keur),
                                tb_m_vehicle.license_sipa = VALUES(tb_m_vehicle.license_sipa),
                                tb_m_vehicle.license_ibm = VALUES(tb_m_vehicle.license_ibm),                                
                                tb_m_vehicle.active = VALUES(tb_m_vehicle.active),
                                tb_m_vehicle.business = VALUES(tb_m_vehicle.business),
                                tb_m_vehicle.changed_by = VALUES(tb_m_vehicle.changed_by),
                                tb_m_vehicle.changed_dt = VALUES(tb_m_vehicle.changed_dt)
                        ";

                        $this->db->query($sql);
                        
                        $message = 'Upload Success. You can double check the data';   
                    }

                                                                    
                }
            }
        }                       

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }
    
}
