<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 06 Maret 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logistic_point extends MY_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->model('Logistic_point_type_model');
        $this->load->model('Logistic_point_model');

        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index() {

        $data['stitle'] = 'Logistic Point';
        $data['jsapp'] = array('logistic_point');
        $data['lpts'] = $this->Logistic_point_type_model->gets();
        $data['areas'] = $this->Logistic_point_model->getAreas();
        
        $this->load->view('header', $data);
        $this->load->view('logistic_point');
        $this->load->view('footer');
    }

    function gets()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
		$sv		= $this->input->post('search')['value'];				
        $lp_type_id = $this->input->post('lp_type_id', true);
        $lp_area = $this->input->post('lp_area', true);
		
        $results 			= $this->Logistic_point_model->getLogisticPoints($lp_type_id, $lp_area, $start, $length, $sv, $order, $columns);
        $recordsTotal       = (int)$this->Logistic_point_model->getCountOfLogisticPoint($lp_type_id, $lp_area, $sv);
		        
        $data = array();
		foreach ($results as $r) 
		{
            $row = array();			
			
            $row[] = $r->lp_cd;
			$row[] = '<a href="'.site_url('logistic_point/id/' . md5($r->lp_cd)).'" title="View Detail '.$r->lp_name.'">' . $r->lp_name . '</a>';
			$row[] = nl2br(strip_tags(shorten_string($r->lp_address, 8)));
			$row[] = $r->lp_area;
            $row[] = $r->lp_color;
            $row[] = $r->lp_remark;
            $row[] = $r->geofenceid;
            
            // $row[] = $r->lp_remark;
            // $row[] = $r->lp_business;
			$data[] = $row;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }

    function id()
    {
        // deletion
        if (isset($_POST['lp_cd']))
        {
            $this->Logistic_point_model->delete($_POST['lp_cd']);
			$this->session->set_flashdata('notif_success', '<strong>Success.</strong> Logistic Point deleted.');
			redirect('logistic_point');
        }

        $lp_cd = $this->uri->segment(3);
        if ($lp_cd == '') redirect('logistic_point');

        $lp = $this->Logistic_point_model->getLogisticPoint($lp_cd);
        if (count($lp) == 0) redirect('logistic_point');

        $data['lp'] = $lp[0];
        $data['stitle'] = $lp[0]->lp_name;
        $data['jsapp'] = array('logistic_point_id');

        $this->load->view('header', $data);
        $this->load->view('logistic_point_id');
        $this->load->view('footer');
    }

    function create()
	{
        $save_sts = '';
        $lp_cd = $lp_name = $lp_address = $lp_area = $lp_remark = $lp_business = $geofenceid = $lp_type_id = $lp_color = '';
        $empty_plan_flg = '0';

		if (isset($_POST['lp_cd']))
		{
            $save_sts = $this->Logistic_point_model->save() ;
            if ($save_sts == '1')
            {
                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> Logistic Point Created.');						
			    redirect('logistic_point/id/' . md5($this->input->post('lp_cd', true)));
            }
            else
            {
                $lp_cd = $this->input->post('lp_cd', true);
                $lp_name = $this->input->post('lp_name', true);
                $lp_address = $this->input->post('lp_address', true);
                $lp_area = $this->input->post('lp_area', true);
                $lp_remark = $this->input->post('lp_remark', true);
                $lp_business = $this->input->post('lp_business', true);
                $geofenceid = $this->input->post('geofenceid', true);
                $lp_type_id = $this->input->post('lp_type_id', true);
                $lp_color = $this->input->post('lp_color', true);
                $empty_plan_flg = $this->input->post('empty_plan_flg', true);
            }
		}
		
		$data['jsapp'] 		= array('logistic_point_create');
        $data['stitle'] = 'Create Logistic Point';
        $data['lpts'] = $this->Logistic_point_type_model->gets();
        $data['save_sts'] = $save_sts;

        // filled the field
        $data['lp_cd'] = $lp_cd;
        $data['lp_name'] = $lp_name;
        $data['lp_address'] = $lp_address;
        $data['lp_area'] = $lp_area;
        $data['lp_remark'] = $lp_remark;
        $data['lp_business'] = $lp_business;
        $data['geofenceid'] = $geofenceid;
        $data['lp_type_id'] = $lp_type_id;
        $data['lp_color'] = $lp_color;
        $data['empty_plan_flg'] = $empty_plan_flg;
        
        $data['areas'] = $this->Logistic_point_model->getMaterArea();

		$this->load->view('header', $data);
		$this->load->view('logistic_point_create');
		$this->load->view('footer');
	}

    function get_areas()
	{
		$results = $this->Logistic_point_model->getAreas($_GET['query']);
		
		$suggestions = array();
		foreach ($results as $r)
		{
			$suggestions[] = array(
				'value' => $r->lp_area,
				'data' => $r->lp_area
			);
		}
		
        echo json_encode(array(
			'query' => 'Unit'
			, 'suggestions' => $suggestions
		));
    }
    
    function edit()
	{
        $save_sts = '';

        $lp_cd = $this->uri->segment(3);
        if ($lp_cd == '') redirect('logistic_point');

        $lp = $this->Logistic_point_model->getLogisticPoint($lp_cd);
        if (count($lp) == 0) redirect('logistic_point');
        $lp = $lp[0];

        $lp_cd = $lp->lp_cd;
        $lp_name = $lp->lp_name;
        $lp_address = $lp->lp_address;
        $lp_area = $lp->lp_area;
        $lp_remark = $lp->lp_remark;
        $lp_business = $lp->lp_business;
        $geofenceid = $lp->geofenceid;
        $lp_type_id = $lp->lp_type_id;
        $lp_color = $lp->lp_color;
        $empty_plan_flg = $lp->empty_plan_flg;

		if (isset($_POST['lp_cd']))
		{
            $save_sts = $this->Logistic_point_model->save('1') ;
            if ($save_sts == '2')
            {
                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> Logistic Point Updated.');						
			    redirect('logistic_point/id/' . md5($this->input->post('lp_cd', true)));
            }
            else
            {
                $lp_cd = $this->input->post('lp_cd', true);
                $lp_name = $this->input->post('lp_name', true);
                $lp_address = $this->input->post('lp_address', true);
                $lp_area = $this->input->post('lp_area', true);
                $lp_remark = $this->input->post('lp_remark', true);
                $lp_business = $this->input->post('lp_business', true);
                $geofenceid = $this->input->post('geofenceid', true);
                $lp_type_id = $this->input->post('lp_type_id', true);
                $lp_color = $this->input->post('lp_color', true);
                $empty_plan_flg = $this->input->post('empty_plan_flg', true);
            }
        }                
		
		$data['jsapp'] 		= array('logistic_point_edit');
        $data['stitle'] = 'Edit Logistic Point';
        $data['lpts'] = $this->Logistic_point_type_model->gets();
        $data['save_sts'] = $save_sts;

        // filled the field
        $data['lp_cd'] = $lp_cd;
        $data['lp_name'] = $lp_name;
        $data['lp_address'] = $lp_address;
        $data['lp_area'] = $lp_area;
        $data['lp_remark'] = $lp_remark;
        $data['lp_business'] = $lp_business;
        $data['geofenceid'] = $geofenceid;
        $data['lp_type_id'] = $lp_type_id;
        $data['lp_color'] = $lp_color;
        $data['empty_plan_flg'] = $empty_plan_flg;

        $data['areas'] = $this->Logistic_point_model->getMaterArea();

		$this->load->view('header', $data);
		$this->load->view('logistic_point_edit');
		$this->load->view('footer');
    }
    
    function download()
    {
        $this->load->library('Excel');
		
		$tpl_path = './assets/files/template-logistic_point.xlsx';
		
		//$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($tpl_path);
		$sheet = $objPHPExcel->getActiveSheet();
		$sheet->setTitle('Logistic Point');
		
		// create data..
		$row = 5;
		
		$data = $this->Logistic_point_model->getLPForDownload();
		// No	Initial	GeofenceID	Supplier/Customer	Address	Area	Color	Remark	Logistic Flag

        $i = 1;
        foreach($data as $d) {
			$sheet->setCellValue('A'.$row, $i);
			$sheet->setCellValue('B'.$row, $d->lp_cd);
			$sheet->setCellValue('C'.$row, $d->geofenceid);
			$sheet->setCellValue('D'.$row, $d->lp_name);
			$sheet->setCellValue('E'.$row, $d->lp_address);
			$sheet->setCellValue('F'.$row, $d->lp_area);
			$sheet->setCellValue('G'.$row, $d->lp_color);
			$sheet->setCellValue('H'.$row, $d->lp_remark);
			$sheet->setCellValue('I'.$row, $d->empty_plan_flg);
            $row++;
            $i++;
		}
				
		$file_name = "Master_Data_LogisticPoint_".date('YmdHis').".xlsx";
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment;filename=$file_name");
		header("Cache-Control: max-age=0");
		$objWriter= PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

		unset($sheet, $objPHPExcel);
    }
    
    function upload()
    {
        $success = true;
        $message = '';
        
        
        if(!empty($_FILES['file_attach']['name']))
        {
            $config['upload_path'] = './assets/timetables';
            $config['allowed_types'] = 'xlsx';
            $config['remove_spaces'] = TRUE;

            $filename = '';

            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('file_attach'))
            {
                $success = false;
                $message = $this->upload->display_errors();
            }
            else
            {
                $dataUpload = $this->upload->data();
                $filename   = $dataUpload["file_name"];
            }

            if(!empty($filename))
            {
                $real_path= getcwd() . "/assets/timetables/";        
                
                $this->load->library('excel');
                $inputFileType  = 'Excel2007';
                $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel    = $objReader->load($real_path . $filename);
                $activeSheet    = $objPHPExcel->getActiveSheet();
                $arr_spread     = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $total_row      = count($arr_spread);
                $sheetData      = $activeSheet->toArray(null, true, true, true); 

                // validasi isi header template nya
                if(
                    strtolower(trim($sheetData['4']["A"])) != 'no' || 
                    strtolower(trim($sheetData['4']["B"])) != 'initial' || 
                    strtolower(trim($sheetData['4']["H"])) != 'remark'
                )
                {
                    $success = false;
                    $message = 'Invalid Excel Template. Please download from Download Template link.';
                }
                else
                {
                    $values = '';
                    
                    for($row = 5; $row <= $total_row; $row++)
                    {
                        $is_valid = 1;
                        $error_detail = '';// "<b>Status data : </b><br/>";
                        $arr = array();

                        if(
                            !empty(trim($sheetData[$row]["B"])))
                        {
                            $lp_cd          = $sheetData[$row]["B"];
                            $geofenceid     = $sheetData[$row]["C"];
                            $lp_name        = $sheetData[$row]["D"];
                            $lp_address     = $sheetData[$row]["E"];
                            $lp_area        = $sheetData[$row]["F"];
                            $lp_color       = $sheetData[$row]["G"];
                            $lp_remark      = $sheetData[$row]["H"];
                            $empty_plan_flg = $sheetData[$row]["I"];                                                                                     
                            
                            $values .= "
                                (
                                    '" . $lp_cd . "',
                                    '" . $geofenceid . "',
                                    '" . $lp_name . "',
                                    '" . $lp_address . "',
                                    '" . $lp_area . "',
                                    '" . $lp_color . "',
                                    '" . $lp_remark . "',
                                    '" . $empty_plan_flg . "',
                                    '" . $this->session->userdata(S_USER_EMAIL) . "',
                                    '" . date('Y-m-d H:i:s') . "',
                                    '" . $this->session->userdata(S_USER_EMAIL) . "',
                                    '" . date('Y-m-d H:i:s') . "'
                                )
                            ";

                            $values .= ($row < $total_row) ? ', ' : '';
                        }
                    }

                    // remove the file!
                    unlink($real_path . $filename);

                    if ($values != '')
                    {
                        // insert into temporary detail
                        $sql = "insert into tb_m_logistic_point
                            (
                                tb_m_logistic_point.lp_cd, 
                                tb_m_logistic_point.geofenceid,
                                tb_m_logistic_point.lp_name,
                                tb_m_logistic_point.lp_address,
                                tb_m_logistic_point.lp_area,
                                tb_m_logistic_point.lp_color,
                                tb_m_logistic_point.lp_remark,
                                tb_m_logistic_point.empty_plan_flg,
                                tb_m_logistic_point.created_by,tb_m_logistic_point.created_dt,
                                tb_m_logistic_point.changed_by,tb_m_logistic_point.changed_dt
                            )
                            VALUES 
                                " . $values . "                                    
                            ON DUPLICATE KEY UPDATE 
                                tb_m_logistic_point.geofenceid = VALUES(tb_m_logistic_point.geofenceid),
                                tb_m_logistic_point.lp_name = VALUES(tb_m_logistic_point.lp_name),
                                tb_m_logistic_point.lp_address = VALUES(tb_m_logistic_point.lp_address),
                                tb_m_logistic_point.lp_area = VALUES(tb_m_logistic_point.lp_area),
                                tb_m_logistic_point.lp_color = VALUES(tb_m_logistic_point.lp_color),
                                tb_m_logistic_point.lp_remark = VALUES(tb_m_logistic_point.lp_remark),
                                tb_m_logistic_point.empty_plan_flg = VALUES(tb_m_logistic_point.empty_plan_flg),
                                tb_m_logistic_point.changed_by = VALUES(tb_m_logistic_point.changed_by),
                                tb_m_logistic_point.changed_dt = VALUES(tb_m_logistic_point.changed_dt)
                        ";

                        $this->db->query($sql);
                        
                        $message = 'Upload Success. You can double check the data';   
                    }

                                                                    
                }
            }
        }                       

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }
    
}
