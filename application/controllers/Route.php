<?php

/*
 * @author: irfan@arkamaya.co.id
 * @created: 06 Maret 2020
 * @updated_by: Rakha@arkamaya.co.id
 * @updated_dt: 12 May 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Route extends MY_Controller {

    function __construct() {
        parent:: __construct();
		
		$this->load->model('Route_model');

        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index() {

        $data['stitle'] = 'Route';		
        $data['jsapp'] = array('route');
		
        $this->load->view('header', $data);
        $this->load->view('route');
        $this->load->view('footer');
    }
	
	function gets()
    {        
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;
		
		$sv		= $this->input->post('search')['value'];				
        //$param = $this->input->post('param', true);
		
        $results 			= $this->Route_model->getRoutes($start, $length, $sv, $order, $columns);
        $recordsTotal       = (int)$this->Route_model->getCountRoutes($sv);
		        
        $data = array();
		foreach ($results as $r) 
		{
            $row = array();						
			
            $row[] = $r->customer;
            $row[] = $r->route;
            $row[] = $r->cycle;
			$row[] = '<a href="'.site_url('route/id/' .$r->route.'/'.$r->cycle).' " title="View Detail '.$r->route.'"> View Detail </a>';
			$data[] = $row;
        }

        $output = array
		(
			"draw" => $def['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsTotal,
			"data" => $data
		);
        echo json_encode($output);
    }
	
	function id()
    {
		// deletion
        if (isset($_POST['route']))
        {
            $this->Route_model->delete($_POST['route'], $_POST['cycle']);
            $this->Route_model->delete_emoney($_POST['route'], $_POST['cycle']);
            $this->Route_model->delete_fuel($_POST['route'], $_POST['cycle']);
            $this->Route_model->delete_other($_POST['route'], $_POST['cycle']);
			
			$this->session->set_flashdata('notif_success', '<strong>Success.</strong> Route deleted.');
			redirect('route');
        }		
		
        $route = $this->uri->segment(3);
        $cycle = $this->uri->segment(4);
		
		if ($route == '') redirect('route');
		if ($cycle == '') redirect('route');

        $dataRoute = $this->Route_model->getRoute($route, $cycle);
        if (count($dataRoute) == 0) redirect('route');
		
		$emoney_detail 	= $this->Route_model->get_emoney_detail($route, $cycle);
		$fuel_detail 	= $this->Route_model->get_fuel_detail($route, $cycle);
		$other_detail 	= $this->Route_model->get_other_detail($route, $cycle);
		
		$data['stitle'] = $dataRoute[0]->route.'-'.$dataRoute[0]->cycle;		
		$data['dataRoute'] = $dataRoute[0];
		$data['emoney_detail'] = $emoney_detail;
		$data['fuel_detail'] = $fuel_detail;
		$data['other_detail'] = $other_detail;
		$data['jsapp'] = array('route_id');
		
		$this->load->view('header', $data);
        $this->load->view('route_id');
        $this->load->view('footer');
    }
	
	function create()
	{	
		$route = $this->input->post('route', true);
		$cycle = $this->input->post('cycle', true);
		if (!empty($route)) 
		{
			$count_route = count($this->Route_model->getRoute($route, $cycle));
			
			if( $count_route == 0) 
			{
				$dataRoute = array
				(
					'route' 	=> $this->input->post('route', true),			
					'cycle'		=> $this->input->post('cycle', true),			
					'customer'	=> $this->input->post('customer', true)			
				);
				
				$data['message'] = $this->Route_model->saveRoute($dataRoute) ;
				$this->session->set_flashdata('notif_route_success', '<strong>Berhasil.</strong> Route berhasil dibuat.');

				redirect("route/id/".$route."/".$cycle);
			} else {
				$this->session->set_flashdata('notif_route_error', '<strong>Gagal.</strong> Data Route sudah digunakan.');
			}
		}
		
		$data['jsapp'] 		= array('route_create');
        $data['stitle']		= 'Create Route';
		$data['customer'] 	= $this->Route_model->getMaterCustomer();

		$this->load->view('header', $data);
		$this->load->view('route_create');
		$this->load->view('footer');
	}
	
	function get_emoneys()
	{
		$sv = $this->input->get('search');

		$data = array(
			'emoneys' => (array) $this->Route_model->get_emoneys($sv)
		);
		
		echo json_encode($data);
	}
	
	function get_bensins()
	{
		$sv = $this->input->get('search');

		$data = array(
			'bensins' => (array) $this->Route_model->get_bensins($sv)
		);
		
		echo json_encode($data);
	}
	
	function edit() 
	{	
		$postRoute = $this->input->post('route', true);
		$postCycle = $this->input->post('cycle', true);
		
		if (!empty($postRoute)) 
		{
			$dataRoute = array
			(
				'route' 	=> $this->input->post('route', true),			
				'cycle'		=> $this->input->post('cycle', true),			
				'customer'	=> $this->input->post('old_customer', true)			
			);
		
			$data['message'] = $this->Route_model->editRoute($dataRoute) ;
			$this->session->set_flashdata('notif_route_success', '<strong>Berhasil.</strong> Route berhasil diubah.');

			redirect("route/id/".$postRoute."/".$postCycle);
		}
		
		$route = $this->uri->segment(3);
        $cycle = $this->uri->segment(4);
		
		if ($route == '') redirect('route');
		if ($cycle == '') redirect('route');

        $dataRoute = $this->Route_model->getRoute($route, $cycle);
        if (count($dataRoute) == 0) redirect('route');
		
		$emoney_detail 	= $this->Route_model->get_emoney_detail($route, $cycle);
		$fuel_detail 	= $this->Route_model->get_fuel_detail($route, $cycle);
		$other_detail 	= $this->Route_model->get_other_detail($route, $cycle);
		
		$data['stitle'] = 'Edit '.$dataRoute[0]->route.'-'.$dataRoute[0]->cycle;		
		$data['dataRoute'] = $dataRoute[0];
		$data['emoney_detail'] = $emoney_detail;
		$data['fuel_detail'] = $fuel_detail;
		$data['other_detail'] = $other_detail;		
		$data['customer'] 	= $this->Route_model->getMaterCustomer();
		$data['jsapp'] 		= array('route_edit');
		
        $this->load->view('header');
		$this->load->view('route_edit', $data);
		$this->load->view('footer');
	}
	
	function upload()
    {
        $success = true;
        $message = '';
        
        
        if(!empty($_FILES['file_attach']['name']))
        {
            $config['upload_path'] = './assets/files/route';
            $config['allowed_types'] = 'xlsx';
            $config['remove_spaces'] = TRUE;

            $filename = '';

            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('file_attach'))
            {
                $success = false;
                $message = $this->upload->display_errors();
            }
            else
            {
                $dataUpload = $this->upload->data();
                $filename   = $dataUpload["file_name"];
            }

            if(!empty($filename))
            {
                $real_path= getcwd() . "/assets/files/route/";        
                
                $this->load->library('excel');
                $inputFileType  = 'Excel2007';
                $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel    = $objReader->load($real_path . $filename);
                $activeSheet    = $objPHPExcel->getActiveSheet();
                $arr_spread     = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $total_row      = count($arr_spread);
                $sheetData      = $activeSheet->toArray(null, true, true, true); 

                // validasi isi header template nya
                if(
                    strtolower(trim($sheetData['4']["A"])) != 'no' || 
                    strtolower(trim($sheetData['4']["B"])) != 'route' || 
                    strtolower(trim($sheetData['4']["C"])) != 'cycle' || 
                    strtolower(trim($sheetData['4']["D"])) != 'customer' 
                )
                {
                    $success = false;
                    $message = 'Invalid Excel Template. Please download from Download Template link.';
                }
                else
                {
                    $values = '';
                    
                    for($row = 5; $row <= $total_row; $row++)
                    {
                        $is_valid = 1;
                        $error_detail = '';// "<b>Status data : </b><br/>";
                        $arr = array();

                        if(
                            !empty(trim($sheetData[$row]["B"])))
                        {
                            $route          = $sheetData[$row]["B"];
                            $cycle		    = $sheetData[$row]["C"];
                            $customer       = $sheetData[$row]["D"];                                        
                            
                            $values .= "
                                (
                                    '" . $route . "',
                                    '" . $cycle . "',
                                    '" . $customer . "',
                                    '" . $this->session->userdata(S_USER_EMAIL) . "',
                                    '" . date('Y-m-d H:i:s') . "',
                                    '" . $this->session->userdata(S_USER_EMAIL) . "',
                                    '" . date('Y-m-d H:i:s') . "'
                                )
                            ";

                            $values .= ($row < $total_row) ? ', ' : '';
                        }
                    }

                    // remove the file!
                    unlink($real_path . $filename);

                    if ($values != '')
                    {
                        // insert into route tabel
                        $sql = "insert into tb_m_route
                            (
                                tb_m_route.route, 
                                tb_m_route.cycle,
                                tb_m_route.customer,
                                tb_m_route.created_by,tb_m_route.created_dt,
                                tb_m_route.changed_by,tb_m_route.changed_dt
                            )
                            VALUES 
                                " . $values . "                                    
                            ON DUPLICATE KEY UPDATE 
                                tb_m_route.route = VALUES(tb_m_route.route),
                                tb_m_route.cycle = VALUES(tb_m_route.cycle),
                                tb_m_route.customer = VALUES(tb_m_route.customer),
                                tb_m_route.changed_by = VALUES(tb_m_route.changed_by),
                                tb_m_route.changed_dt = VALUES(tb_m_route.changed_dt)
                        ";

                        $this->db->query($sql);
                        
                        $message = 'Upload Success. You can double check the data';   
                    }    
                }
            }
        }                       

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }
	
	function download()
    {
        $this->load->library('Excel');
		
		$tpl_path = './assets/files/template-route.xlsx';
		
		//$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load($tpl_path);
		$sheet = $objPHPExcel->getActiveSheet();
		$sheet->setTitle('Route');
		
		// create data..
		$row = 5;
		
		$data = $this->Route_model->getRouteForDownload();
		// No	Route	Cycle	Customer

        $i = 1;
        foreach($data as $d) {
			$sheet->setCellValue('A'.$row, $i);
			$sheet->setCellValue('B'.$row, $d->route);
			$sheet->setCellValue('C'.$row, $d->cycle);
			$sheet->setCellValue('D'.$row, $d->customer);
            $row++;
            $i++;
		}
				
		$file_name = "Master_Data_Route_".date('YmdHis').".xlsx";
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment;filename=$file_name");
		header("Cache-Control: max-age=0");
		$objWriter= PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

		unset($sheet, $objPHPExcel);
    }
}
