<?php

/*
 * @author: arka.gamal
 * @created: 17-07-2020
 */

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class ApiInsertLoc extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		// $this->load->model('Attendance_model');
	}

	function index()
	{
	}

	public function insert()
	{
		$sql_str = $this->input->post("loc_str", true);

		$resultType = 'norm';
		$result = 'true';
		$message = 'Data saved';
		$data = array();
		$lastPositionId = '';

		if($sql_str != ''){
			try {
				$this->db->query($sql_str);
				$db_error = $this->db->error();
				// print_r($db_error);
				if ($db_error['code']!= 0) {
						throw new Exception('Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
						return false; // unreachable retrun statement !
				}
			}catch (Exception $e) {
				$result = 'false';
				$resultType = 'error';
				$message = 'Error when try to insert. Log:'.$e;
			}
		}else {
			$result = 'false';
			$resultType = 'error';
			$message = 'No loc_str found.';
		}

		try {
		$query = $this->db->query("
		SELECT PositionId FROM tb_r_log_location ORDER BY PositionId desc LIMIT 1
		");
		if ($query->num_rows() > 0){
			$lastPositionId = $query->row()->PositionId;
		}
		$db_error = $this->db->error();
		// print_r($db_error);
		if ($db_error['code']!= 0) {
				throw new Exception('Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
				return false; // unreachable retrun statement 1
		}
		}catch (Exception $e){
			$result = 'false';
			$resultType = 'error';
			$message = 'There is no data or something went wrong:'.$e;
		}

		header('Content-Type: application/json');
		$data['msg_type'] = $resultType;
		$data['result'] = $result;
		$data['msg'] = $message;
		$data['lastPositionId'] = $lastPositionId;

		echo json_encode($data);

	}

}
