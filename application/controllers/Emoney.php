<?php

/*
* @author: Arka.Gamal
* @created: 12 Mei 2020
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Emoney extends MY_Controller {

    function __construct() {
        parent:: __construct();

        $this->load->model('Emoney_model');

        if ($this->session->userdata(S_COMPANY_ID) == null) {
            redirect('login');
        }
    }

    function index() {

        $data['stitle'] = 'E-Money';
        $data['jsapp'] = array('emoney');
        $data['customers'] = $this->Emoney_model->getMasters('customer');
        $data['routes'] = $this->Emoney_model->getRoutes();
        $data['categories'] = $this->Emoney_model->getMasters('emoney_ctg');

        $this->load->view('header', $data);
        $this->load->view('emoney');
        $this->load->view('footer');
    }

    function gets()
    {
        $order = $this->input->post('order');
        $columns = $this->input->post('columns');
        $idx_cols = $order[0]['column'];
        $def = array (
            'draw'  => $this->input->post('draw'),
            'length'    => $this->input->post('length'),
            'start'     => $this->input->post('start'),
        );

        $start 		= isset($_POST['start']) ? intval($_POST['start']) : 0;
        $length 	= isset($_POST['length']) ? intval($_POST['length']) : 50;

        $sv		= $this->input->post('search')['value'];
        $route = $this->input->post('route', true);
        $customer = $this->input->post('customer', true);
        $category = $this->input->post('category', true);

        $results 			= $this->Emoney_model->getEmoneys($route, $customer, $category, $start, $length, $sv, $order, $columns);
        $recordsTotal       = (int)$this->Emoney_model->getCountOfEmoney($route, $customer, $category, $sv);

        $data = array();
    foreach ($results as $r)
    {
            $row = array();
            $row[] = '<a href="'.site_url('emoney/id/' . md5($r->emoney_id)).'" title="View Detail '.$r->emoney_id.'">' . $r->emoney_id . '</a>';
            $row[] = $r->bank;
            $row[] = ($r->balance != '') ? 'Rp. '.number_format($r->balance,2,",",".") : '';
            $row[] = $r->cust_name;
            $row[] = $r->emoney_ctg;
            $row[] = $r->route;
            $row[] = $r->cycle;
            $row[] = $r->gel;
      $data[] = $row;
        }

        $output = array
    (
      "draw" => $def['draw'],
      "recordsTotal" => $recordsTotal,
      "recordsFiltered" => $recordsTotal,
      "data" => $data
    );
        echo json_encode($output);
    }

    function id()
    {
        // deletion
        if (isset($_POST['emoney_id']))
        {
            $del = '';
            $del = $this->Emoney_model->delete($_POST['emoney_id']);
            if($del > 0){
              $this->session->set_flashdata('notif_success', '<strong>Success.</strong> E-Money deleted.');
              redirect('emoney');
            }else {
              $this->session->set_flashdata('notif_success', '<strong>Something went wrong.</strong> Deletion failed, please try again.');
              redirect('emoney');
            }
        }

        $emoney_id = $this->uri->segment(3);
        if ($emoney_id == '') redirect('emoney');

        $emoney = $this->Emoney_model->getEmoney($emoney_id);
        if (count($emoney) == 0) redirect('emoney');

        $data['history'] = $this->Emoney_model->getHistoryById($emoney_id);
        $data['checkTrans'] = $this->Emoney_model->checkTrans($emoney_id);
        $data['emoney'] = $emoney[0];
        $data['stitle'] = $emoney[0]->emoney_id;
        $data['jsapp'] = array('emoney_id');

        $this->load->view('header', $data);
        $this->load->view('emoney_id');
        $this->load->view('footer');
    }

    function create()
    {
        $save_sts = '';
        $emoney_id = $this->input->post('emoney_id', true);
        $bank = $this->input->post('bank', true);
        $balance = $this->input->post('balance', true);
        $category = $this->input->post('category', true);

    if (isset($_POST['emoney_id']))
    {
            $save_sts = $this->Emoney_model->save() ;
            if ($save_sts == '1')
            {
                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> E-Money Created.');
                redirect('emoney/id/' . md5($this->input->post('emoney_id', true)));
            }
            else
            {
              // filled the field
              $data['emoney_id'] = $emoney_id;
              $data['bank'] = $bank;
              $data['balance'] = $balance;
              $data['category'] = $category;
            }
    }

    $data['jsapp'] 		= array('emoney_create');
        $data['stitle'] = 'Create E-Money';
        $data['save_sts'] = $save_sts;
        $data['categories'] = $this->Emoney_model->getMasters('emoney_ctg');
        // filled the field
        $data['emoney_id'] = $emoney_id;
        $data['bank'] = $bank;
        $data['balance'] = $balance;

    $this->load->view('header', $data);
    $this->load->view('emoney_create');
    $this->load->view('footer');
    }

    function get_emoney_owners()
    {
    $results = $this->Emoney_model->getEmoneyOwners($_GET['query']);

    $suggestions = array();
    foreach ($results as $r)
    {
      $suggestions[] = array(
        'value' => $r->vehicle_owner,
        'data' => $r->vehicle_owner
      );
    }

        echo json_encode(array(
      'query' => 'Unit'
      , 'suggestions' => $suggestions
    ));
    }

    function edit()
    {
        $save_sts = '';

        $emoney_id = $this->uri->segment(3);
        if ($emoney_id == '') redirect('emoney');

        $emoney = $this->Emoney_model->getEmoney($emoney_id);
        if (count($emoney_id) == 0) redirect('emoney');
        $emoney_id = $emoney[0]->emoney_id;
        $bank = $emoney[0]->bank;
        $balance = $emoney[0]->balance;
        $category = $emoney[0]->emoney_ctg_raw;
        $customer = $emoney[0]->cust_name;
        $cycle = $emoney[0]->cycle;
        $gel = $emoney[0]->gel;

    if (isset($_POST['emoney_id']))
    {
            $save_sts = $this->Emoney_model->save('1') ;
            if ($save_sts == '2')
            {
                $this->session->set_flashdata('notif_success', '<strong>Success.</strong> E-Money Updated.');
                redirect('emoney/id/' . md5($this->input->post('emoney_id', true)));
            }
            else
            {
                $emoney_id = $this->input->post('emoney_id', true);
                $bank = $this->input->post('bank', true);
                $balance = $this->input->post('balance', true);
                $category = $this->input->post('category', true);
            }
    }

    $data['jsapp'] 		= array('emoney_edit');
        $data['stitle'] = 'Edit E-Money';
        $data['save_sts'] = $save_sts;
        $data['categories'] = $this->Emoney_model->getMasters('emoney_ctg');
        // filled the field
        $data['emoney_id'] = $emoney_id;
        $data['bank'] = $bank;
        $data['balance'] = $balance;
        $data['category'] = $category;
        $data['customer'] = $customer;
        $data['cycle'] = $cycle;
        $data['gel'] = $gel;

    $this->load->view('header', $data);
    $this->load->view('emoney_edit');
    $this->load->view('footer');
    }

    function download()
    {
        $this->load->library('Excel');

    $tpl_path = './assets/files/template-emoney.xlsx';

    //$objPHPExcel = new PHPExcel();
    $objPHPExcel = PHPExcel_IOFactory::load($tpl_path);
    $sheet = $objPHPExcel->getActiveSheet();
    $sheet->setTitle('E-Money');

    // create data..
    $row = 6;

    $data = $this->Emoney_model->getEmoneysForDownload();
    // No	Truck ID	Police Number	Truck Type	Brand	Owner	LICENSE				Based
        // STNK	KIR	SIPA	IBM

    $i = 1;
    foreach($data as $d) {
      $sheet->setCellValue('A'.$row, $i);
      $sheet->setCellValue('B'.$row, $d->emoney_id);
      $sheet->setCellValue('C'.$row, $d->emoney_ctg);
      $sheet->setCellValue('D'.$row, $d->bank);
      $sheet->setCellValue('E'.$row, $d->balance);
      // $sheet->setCellValue('E'.$row, $d->cust_name);
      // $sheet->setCellValue('F'.$row, $d->route);
      // $sheet->setCellValue('G'.$row, $d->cycle);
      // $sheet->setCellValue('H'.$row, $d->gel);
      $row++;
      $i++;
    }

    $file_name = "Master_Data_EMoney_".date('YmdHis').".xlsx";
    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment;filename=$file_name");
    header("Cache-Control: max-age=0");
    $objWriter= PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

    unset($sheet, $objPHPExcel);
    }

    function upload()
    {
        $success = true;
        $message = '';


        if(!empty($_FILES['file_attach']['name']))
        {
            $config['upload_path'] = './assets/timetables';
            $config['allowed_types'] = 'xlsx';
            $config['remove_spaces'] = TRUE;

            $filename = '';

            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('file_attach'))
            {
                $success = false;
                $message = $this->upload->display_errors();
            }
            else
            {
                $dataUpload = $this->upload->data();
                $filename   = $dataUpload["file_name"];
            }

            if(!empty($filename))
            {
                $real_path= getcwd() . "/assets/timetables/";

                $this->load->library('excel');
                $inputFileType  = 'Excel2007';
                $objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel    = $objReader->load($real_path . $filename);
                $activeSheet    = $objPHPExcel->getActiveSheet();
                $arr_spread     = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $total_row      = count($arr_spread);
                $sheetData      = $activeSheet->toArray(null, true, true, true);

                // validasi isi header template nya
                if(
                    strtolower(trim($sheetData['4']["A"])) != 'no' ||
                    strtolower(trim($sheetData['4']["B"])) != 'e-money id' ||
                    strtolower(trim($sheetData['4']["C"])) != 'category' ||
                    strtolower(trim($sheetData['4']["D"])) != 'bank' ||
                    strtolower(trim($sheetData['4']["E"])) != 'balance'
                )
                {
                    $success = false;
                    $message = 'Invalid Excel Template. Please download from Download Template link.';
                }
                else
                {
                    $values = '';

                    for($row = 5; $row <= $total_row; $row++)
                    {
                        $is_valid = 1;
                        $error_detail = '';// "<b>Status data : </b><br/>";
                        $arr = array();

                        if(
                            !empty(trim($sheetData[$row]["B"])))
                        {
                            $emoney_id    = $sheetData[$row]["B"];
                            $category = $sheetData[$row]["C"];
                            $bank = $sheetData[$row]["D"];
                            $balance   = str_replace(",", "", $sheetData[$row]["E"]);

                            $values .= "
                                (
                                    '" . $emoney_id . "',
                                    '" . $category . "',
                                    '" . $bank . "',
                                    '" . $balance . "',
                                    '" . $this->session->userdata(S_EMPLOYEE_NAME) . "',
                                    '" . date('Y-m-d H:i:s') . "',
                                    '" . $this->session->userdata(S_EMPLOYEE_NAME) . "',
                                    '" . date('Y-m-d H:i:s') . "'
                                )
                            ";

                            $values .= ($row < $total_row) ? ', ' : '';
                        }
                    }

                    // remove the file!
                    unlink($real_path . $filename);

                    if ($values != '')
                    {
                        // insert into temporary detail
                        $sql = "insert into tb_m_emoney
                            (
                                tb_m_emoney.emoney_id,
                                tb_m_emoney.emoney_ctg,
                                tb_m_emoney.bank,
                                tb_m_emoney.balance,
                                tb_m_emoney.created_by,tb_m_emoney.created_dt,
                                tb_m_emoney.changed_by,tb_m_emoney.changed_dt
                            )
                            VALUES
                                " . $values . "
                            ON DUPLICATE KEY UPDATE
                                tb_m_emoney.emoney_ctg = VALUES(tb_m_emoney.emoney_ctg),
                                tb_m_emoney.bank = VALUES(tb_m_emoney.bank),
                                tb_m_emoney.balance = VALUES(tb_m_emoney.balance),
                                tb_m_emoney.changed_by = VALUES(tb_m_emoney.changed_by),
                                tb_m_emoney.changed_dt = VALUES(tb_m_emoney.changed_dt)
                        ";

                        $this->db->query($sql);

                        $message = 'Upload Success. You can double check the data';
                    }


                }
            }
        }

        $result['success']      = $success;
        $result['message']      = $message;

        echo json_encode($result);
    }

}
