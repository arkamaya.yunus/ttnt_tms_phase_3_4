<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * @author: misbah@arkamaya.co.id
 * @created: 2017-02-16
 */

class Register extends CI_controller 
{
    function __construct() 
	{
        parent:: __construct();
        $this->load->model('Shared_model');
		$this->load->model('Register_model');
		$this->load->model('Login_model');
    }

    function index() 
	{
        $data['trial_days'] = $this->Shared_model->get_system_value('app_config', 'trial_days', 'system_value_num');
        $this->load->view('register', $data);
    }
	
	function process() 
	{
        $digits = 6;
        $activation_code = rand(pow(10, $digits-1), pow(10, $digits)-1);
        
        $dt = date_create(date("Y-m-d H:i:s"));
        date_add($dt, date_interval_create_from_date_string("2 days"));
        $exp_dt = date_format($dt, "Y-m-d H:i:s");
        
        // already registered email validation checking
        $email = $this->input->post('email', true);
        $res = $this->Register_model->check_email($email);
        
        if ($res == 'true')
		{
            $data = array(
                'name' 					=>  $this->input->post('name', true)
                , 'email' 				=> $email
                , 'phone' 				=> $this->input->post('phone', true)
                , 'password' 			=> md5($this->input->post('password', true))
                , 'company_name' 		=> $this->input->post('company_name', true)
                , 'industry_type' 		=> $this->input->post('industry_type', true)
                , 'package_id' 			=> $this->input->post('package_id', true)
                , 'activation_code' 	=> md5($activation_code)
                , 'activation_expired' 	=> $exp_dt
                , 'activation' 			=> 0
            );
        
            $res = $this->Register_model->register($data);
            if ($res)
			{
                $send_email = $this->send_email_confirmation($data);

                if ($send_email) 
				{
                    //return true;
                    $this->session->set_flashdata('notif_status', 'success');
                    $this->session->set_flashdata('notif_register', '<strong>Berhasil. </strong> Email untuk konfirmasi pendaftaran telah dikirim, harap cek email anda. Cek Folder <span style="font-weight: bold; color: red">SPAM</span> jika tidak ada dan masukkan Email kami kedalam whitelist.'
                            . '<p class="text-dark font-13 m-t-10"> Jika Anda Tidak Menerima Email konfirmasi, <a href="'.base_url().'register/resend_activation" class="text-primary">  <strong>klik disini</strong> </a> untuk kirim ulang email konfirmasi</p>');
                    redirect('register');
                } 
				else 
				{
                    //return false;
                    $this->session->set_flashdata('notif_status', 'warning');
                    $this->session->set_flashdata('notif_register', '<strong>Peringatan. </strong> Terjadi kesalahan saat mengirimkan link aktivasi ke email anda, silahkan <a href="'.base_url().'register/resend_activation" class="text-primary">  <strong class="text-dark">klik disini</strong> </a> untuk kirim ulang link aktivasi ke email anda.</p>');
                    redirect('register');
                }
            }
			else
			{
                $this->session->set_flashdata('notif_status', 'error');
                $this->session->set_flashdata('notif_register', '<strong>Gagal. </strong> terjadi kesalahan saat mencatat data registrasi, silahkan coba lagi.');
                redirect('register');
            }
        }
		else
		{
            $this->session->set_flashdata('notif_status', 'error');
            $this->session->set_flashdata('notif_register', '<strong>Gagal. </strong> email sudah terdaftar.');
            redirect('register');
        }
    }
	
	function send_email_confirmation($data)
	{        
        $this->load->library('email');
        
        $message = $this->load->view('register_mail_confirm', $data, true);
            
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
		$config['validation'] = TRUE;

        $this->email->initialize($config);
        $this->email->from(SMTP_USER, EMAIL_ALIAS);
        $this->email->to($this->input->post('email'));
		
		$this->email->bcc('irfan.satriadarma@gmail.com');

        $this->email->subject('Aktivasi Akun Anda');
        $this->email->message($message);

        if ($this->email->send()) 
		{
            return true;
        } 
		else 
		{
            return false;
        }
    }
    
    function resend_activation()
	{        
        if( isset($_POST['email']) ){
            $email 	= $this->input->post('email');
            $data 	= $this->Register_model->get_activation_code($email);
			
            if($data != false)
			{
                $send_email = $this->send_email_confirmation($data);
                if ($send_email) 
				{
                    //return true;
                    $this->session->set_flashdata('notif_status', 'success');
                    $this->session->set_flashdata('notif_resend', '<strong>Berhasil. </strong> Email untuk konfirmasi pendaftaran telah dikirim ulang, harap cek email anda. <br/><br/> Cek Folder <span style="font-weight: bold; color: red">SPAM</span> jika tidak ada dan masukkan Email kami kedalam whitelist.');
                    redirect('register/resend_activation');
                } 
				else 
				{
                    //return false;
                    $this->session->set_flashdata('notif_status', 'error');
                    $this->session->set_flashdata('notif_resend', '<strong>Gagal. </strong> terjadi kesalahan saat mengirimkan link aktivasi, silahkan coba lagi.');
                    redirect('register/resend_activation');
                }
            }
			else
			{
                $this->session->set_flashdata('notif_status', 'error');
                $this->session->set_flashdata('notif_resend', '<strong>Gagal. </strong> Email anda tidak terdaftar.');
            }
        }
        
        $this->load->view('register_resend_activation');
    }

    function activation()
	{
        $message	= "";
        $result 	= "Success";
        $email 		= "";
        
        $activation_code 	= $this->uri->segment(3);
        $register 			= $this->Register_model->get_register($activation_code);

        if ($register != false)
		{
            $this->Register_model->setupNewUser($register);
            $this->Register_model->update_register($activation_code, 1);
            
            $this->Login_model->last_logged_in($register->email);
            
            // user information
            $this->session->set_userdata(S_USER_EMAIL		, $register->email);
            $this->session->set_userdata(S_EMPLOYEE_NAME	,  $register->name);            
            
            $message = "Aktivasi data berhasil dilakukan";
            $result = "Success";

            /*
			$gen = $this->generate_data($register);

			if ($gen['result'] != false)
			{
				$this->Register_model->update_register($activation_code, 1);
				
				$default_company_assets_dir = S_COMPANY_PATH;
				
				// create neccesary directory
				if (!is_dir($default_company_assets_dir . 'cid'.$gen['result'])) {
					mkdir($default_company_assets_dir . 'cid' . $gen['result'], 0777, TRUE);
					copy($default_company_assets_dir . 'index.html', $default_company_assets_dir . 'cid' . $gen['result'].'/index.html');
				}
				
				$folders_to_create = array(
					'profile'
					, 'profile/thumbs'
					, 'purchase_order'
					, 'purchase_payment'
					, 'purchase_receive'
					, 'purchase_request'
					, 'sales_delivery'
					, 'sales_invoice'
					, 'sales_orders'
					, 'sales_payment'
					, 'sales_quotation'
				);
				
				foreach ($folders_to_create as $f)
				{
					if (!is_dir($default_company_assets_dir . 'cid'.$gen['result'] . '/' . $f)) 
					{
						mkdir($default_company_assets_dir . 'cid' . $gen['result'] . '/' . $f , 0777, TRUE);
						copy($default_company_assets_dir . 'index.html', $default_company_assets_dir . 'cid' . $gen['result'] . '/' . $f . '/index.html');
					}
				}
				
				// langsung loginkan
				//	-------------------------------------------------------------
				$user_data = $this->Login_model->auth_user($register->email);
				if (count($user_data) > 0) 
				{
					$user_data = $user_data[0];
					
					$this->Login_model->last_logged_in($user_data->user_email);
								
					$employee_name = $user_data->full_name; // ($user_data->employee_name != '') ? $user_data->employee_name : $user_data->full_name;

					// company info
					$this->session->set_userdata(S_COMPANY_ID		, $user_data->company_id);
					$this->session->set_userdata(S_COMPANY_NAME		, $user_data->company_name);
					$this->session->set_userdata(S_COMPANY_EXPIRED	, $user_data->is_expired);
					
					// user information
					$this->session->set_userdata(S_USER_EMAIL		, $user_data->user_email);
					$this->session->set_userdata(S_USER_GROUP_ID	, $user_data->user_group_id);
					$this->session->set_userdata(S_USER_IS_ADMIN	, $user_data->is_admin);
					$this->session->set_userdata(S_USER_LANDING		, $user_data->default_landing);
					
					// user - employee information
					$this->session->set_userdata(S_EMPLOYEE_ID		, $user_data->user_email);
					$this->session->set_userdata(S_EMPLOYEE_NAME	, $employee_name);
					$this->session->set_userdata(S_EMPLOYEE_PHOTO	, '');												
				}
				
				$message = "Aktivasi data berhasil dilakukan";
				$result = "Success";
			}
			else
			{
				$message = "Maaf, Terjadi kesalahan dalam mencatat data";
				$result = "Error";
            }
            */
        }
		else
		{
            $message = 'Maaf, Data Registrasi dengan kode <strong class="text-dark">'.$activation_code.'</strong> tidak terdaftar, atau sudah melalui batas kadaluarsa.';
            $result = "Error";
        }
        
        $data['result'] = $result;
        $data['message'] = $message;
        $data['email'] = $email;
        $this->load->view('register_activation_result', $data);
    }
    
    function generate_data($register) 
	{
        $ret = array(); 
        try 
		{
			$trial_days = $this->Shared_model->get_system_value('app_config', 'trial_days', 'system_value_num');
			$trial_days = ($trial_days != '') ? $trial_days : '15';
            $dt 		= date_create(date("Y-m-d H:i:s"));
            date_add($dt, date_interval_create_from_date_string($trial_days . ' days'));
            
			$exp_dt 	= date_format($dt, "Y-m-d H:i:s");

            //insert company
            $company = array
			(
                'user_email' 				=> $register->email
                , 'email' 					=> $register->email
                , 'company_name' 			=> $register->company_name
                , 'phone' 					=> $register->phone
                , 'accounting_period_start' => date('Y')."-01-01"
                , 'accounting_period_end' 	=> date('Y')."-12-31"
                , 'trial_expired' 			=> $exp_dt
                , 'package_id' 				=> $register->package_id
                , 'created_by' 				=> 'system'
                , 'created_dt' 				=> date('Y-m-d H:i:s')
            );

            $res = $this->Register_model->generate_data($company, $register);
            
            $ret['result'] 	= $res;
            $ret['message'] = "";
			
        }
		catch (Exception $e) 
		{
            $ret['result'] 	= false;
            $ret['message'] = $e->getMessage();
        }
        
        return $ret;
    }
    
    function check_email() 
	{
        $email = $this->input->post('email', true);
        $res = $this->Register_model->check_email($email);
        echo $res;
    }        
	
	function confirm_user_email()
	{
		$email_confirmed_code = $this->uri->segment(3);
		if (!$email_confirmed_code) redirect('login/out/misscode');
		
		$um = $this->Login_model->get_email_by_confirmed_password_code($email_confirmed_code);
		if (!$um) {
			redirect('login/out/misscode');
		}
		
		$data['key']	= $email_confirmed_code;
		$data['um'] 	= $um;
		
		if (isset($_POST['email'])) {
            $email = $this->input->post('email');
            $newpassword = $this->input->post('newpassword');
            $key = $this->input->post('key');

            $data['user_password'] 			= md5($newpassword);
            $data['email_confirmed_code'] 	= null;
            
			$this->db->query("
				update 
					tb_m_users 
				set 
					user_password = '" . md5($newpassword) . "'
					, email_confirmed = '1'
					, email_confirmed_code = null
					, reset_password_code = null
				where
					user_email = '" . $email . "' and email_confirmed_code = '" . $key . "'					
			");
			$res = $this->db->affected_rows();

            if ($res > 0) {
                // langsung loginkan
				$user_data = $this->Login_model->auth_user($email);
				if (count($user_data) > 0) {
					$user_data = $user_data[0];
					
					$this->Login_model->last_logged_in($user_data->user_email);
								
					$employee_name = ($user_data->employee_name != '') ? $user_data->employee_name : 'Pengguna';

					// company info
					$this->session->set_userdata(S_COMPANY_ID		, $user_data->company_id);
					$this->session->set_userdata(S_COMPANY_NAME		, $user_data->company_name);
					$this->session->set_userdata(S_COMPANY_EXPIRED	, $user_data->is_expired);
					
					// user information
					$this->session->set_userdata(S_USER_EMAIL		, $user_data->user_email);
					$this->session->set_userdata(S_USER_GROUP_ID	, $user_data->user_group_id);
					$this->session->set_userdata(S_USER_IS_ADMIN	, $user_data->is_admin);
					$this->session->set_userdata(S_USER_LANDING		, $user_data->default_landing);
					
					// user - employee information
					$this->session->set_userdata(S_EMPLOYEE_ID		, $user_data->employee_id);
					$this->session->set_userdata(S_EMPLOYEE_NAME	, $employee_name);
					$this->session->set_userdata(S_EMPLOYEE_PHOTO	, $user_data->employee_photo);
					
					$redirect_uri = $this->input->post('redirect_uri', true);
					if ($redirect_uri != '') {
						redirect($redirect_uri);
					} else {
						redirect($user_data->default_landing);
					}
				} else {
					redirect('login/failed');
				}
				
            } else {
                $this->session->set_flashdata('notif_status', 'error');
                $this->session->set_flashdata('notif_reset_pass', '<strong> Gagal. </strong> Kata Sandi anda gagal diubah. Kode untuk ubah kata sandi anda sudah pernah digunakan. Untuk Kembali ke halaman lupa kata sandi klik <a href="'.site_url('forgot_password').'">disini</a>');
            }
        }
		
		$this->load->view('confirm_password', $data);        
	}    
}
